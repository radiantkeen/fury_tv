// Tag Missile

function LoosenedArmorEffect::setup(%this)
{
    %this.duration = 30000;
    %this.stackable = false;
    %this.stackableDuration = 0;
    %this.ticking = false;
    %this.effectInvulnPeriod = 0;
}

function LooseArmorEffect::validateEffect(%this, %obj, %instigator)
{
    if(%obj.isMounted())
        return false;

    return true;
}

function LooseArmorEffect::startEffect(%this, %obj, %instigator)
{
    Parent::startEffect(%this, %obj, %instigator);

    %who = %obj.isPlayer() ? %obj.client : %obj.getControllingClient();
    %obj.damageReduceFactor += 0.5;
    
    messageClient(%who , 'MsgStartLooseArmor', "\c2Warning! Tag beacon detected, incoming damage increased by 50%!");
}

function LooseArmorEffect::endEffect(%this, %obj)
{
    if(Parent::endEffect(%this, %obj))
    {
        %who = %obj.isPlayer() ? %obj.client : %obj.getControllingClient();
        %obj.damageReduceFactor -= 0.5;
        
        messageClient(%who, 'MsgEndLooseArmor', '\c2Tag beacon overloaded, incoming damage restored to normal.');
    }
}

StatusEffect.registerEffect("LoosenedArmorEffect");
