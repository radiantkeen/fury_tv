// Burning Effect
datablock AudioProfile(BurningSound)
{
   filename = "fx/environment/icecrack2.wav";
   description = AudioClose3d;
   preload = true;
};

datablock ParticleData(BurnFlameParticle)
{
    dragCoefficient = 0;
    gravityCoefficient = 0.3;
    windCoefficient = 0;
    inheritedVelFactor = 0.362903;
    constantAcceleration = 0;
    lifetimeMS = 750;
    lifetimeVarianceMS = 749; //750; --- can't be > or = (ST)
    useInvAlpha = 0;
    spinRandomMin = -145.161;
    spinRandomMax = 133.065;
    textureName = "special/tracer00.PNG";
    times[0] = 0;
    times[1] = 1;
    colors[0] = "0.850394 0.304000 0.192000 0.451613";
    colors[1] = "0.000000 0.000000 0.000000 0.709677";
    sizes[0] = 1.97581;
    sizes[1] = 1.35484;
};

datablock ParticleEmitterData(BurnFlameEmitter)
{
    ejectionPeriodMS = 15;
    periodVarianceMS = 0;
    ejectionVelocity = 5.79032;
    velocityVariance = 2.30645;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 14.5161;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "BurnFlameParticle";
};

function BurnEffect::setup(%this)
{
    %this.duration = 4000;
    %this.stackable = true;
    %this.stackableDuration = 2000;
    %this.ticking = true;
    %this.tickTimeMultiplier = 1.0;
    %this.effectInvulnPeriod = 5000;

    %this.damagePerSecond = 1;
}

function BurnEffect::validateEffect(%this, %obj, %instigator)
{
    if(%obj.isWet || %obj.isShielded || %obj.isMounted()) // or is burn proof
        return false;
        
    return true;
}

function BurnEffect::startEffect(%this, %obj, %instigator)
{
    Parent::startEffect(%this, %obj, %instigator);
    
    if(%obj.isPlayer())
        messageClient(%obj.client, 'MsgStartBurnEffect' , '\c2Plasma fire detected, initiating suppression system...');
}

function BurnEffect::tickEffect(%this, %obj, %instigator)
{
    if(Parent::tickEffect(%this, %obj, %instigator))
    {
        if(%obj.isWet)
        {
            %this.forceEndEffect(%obj);
            return;
        }
        
        %self = %this.getName();
        
        if(%obj.tickCount[%self] % 2 == 0)
        {
            %obj.getDataBlock().damageObject(%obj, %instigator, %obj.position, 0.05, $DamageType::Plasma, "0 0 0", %instigator.client, 0, $DamageGroupMask::Plasma);
            %emitPos = %obj.getWorldBoxCenter(); //vectorAdd(%obj.position, "0 0 1");

            createLifeEmitter(%emitPos, "BurnFlameEmitter", $g_TickTime * 3);
            createLifeEmitter(%emitPos, "HeavyDamageSmoke", $g_TickTime * 3);
            %obj.setHeat(1);
        }
        
        if(%obj.tickCount[%self] % 7 == 0)
            %obj.play3D("BurningSound");

        if(%obj.tickCount[%self] % 32 == 0 && %obj.isPlayer())
        {
            if(getRandom() > 0.8)
                %this.forceEndEffect(%obj);
        }
    }
}

function BurnEffect::endEffect(%this, %obj)
{
    if(Parent::endEffect(%this, %obj) && %obj.isPlayer())
        messageClient(%obj.client, 'MsgEndBurnEffect', '\c2Plasma fire extinguished.');
}

StatusEffect.registerEffect("BurnEffect");
