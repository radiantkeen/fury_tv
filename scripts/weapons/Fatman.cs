//--------------------------------------
// Fat Man
//--------------------------------------

datablock AudioProfile(FatManMountSound)
{
   filename    = "fx/weapon/weapon_heavy_activate.wav";
   description = AudioExplosion3d;
   preload = true;
};

datablock AudioProfile(FatManFireSound)
{
   filename    = "fx/weapon/fatman_fire.wav";
   description = AudioAmbient3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------

datablock LinearProjectileData(FatManRocket)
{
   scale = "1.0 1.0 1.0";
   projectileShapeName = "vehicle_grav_scout.dts";
   emitterDelay        = -1;
   hasDamageRadius     = true;
   indirectDamage      = 32.5;
   damageRadius        = 35.0;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 7500;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Explosive;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "SatchelMainExplosion";
   underwaterExplosion = "UnderwaterSatchelMainExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = MissileFireEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 200;
   wetVelocity       = 200;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 4000;
   lifetimeMS        = 4000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 4000;

   sound = MissileProjectileSound;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 5.0;
   lightColor  = "0.2 0.05 0";
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(FatMan)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "vehicle_grav_scout.dts";
   image = FatManImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a fat man rocket";

   computeCRC = true;
   emap = true;
};

datablock ShapeBaseImageData(FatManImage)
{
   className = WeaponImage;
   shapeFile = "TR2weapon_mortar.dts";
   item = FatMan;
//   ammo = RocketLauncherAmmo;
   offset = "0 0 0";
//  armThread = lookms;
   emap = true;

   usesEnergy = true;
   minEnergy = 0;
   fireEnergy = 0;

   projectile = FatManRocket;
   projectileType = LinearProjectile;

   stateName[0]                     = "Activate";
   stateTransitionOnTimeout[0]      = "ActivateReady";
   stateTimeoutValue[0]             = 0.5;
   stateSequence[0]                 = "Activate";
   stateSound[0]                    = FatManMountSound;

   stateName[1]                     = "ActivateReady";
   stateTransitionOnLoaded[1]       = "Ready";
   stateTransitionOnNoAmmo[1]       = "NoAmmo";

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.01;
   stateFire[3]                     = true;
   stateRecoil[3]                   = LightRecoil;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "Fire";
   stateScript[3]                   = "onFire";
   stateSound[3]                    = FatManFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 5.0;
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";
//   stateSound[4]                    = MissileReloadSound;

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = MissileDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "ActivateReady";
   
   stateName[7]                     = "CheckTarget";
   stateTransitionOnNoTarget[7]     = "DryFire";
   stateTransitionOnTarget[7]       = "Fire";
   
   stateName[8]                     = "CheckWet";
   stateTransitionOnWet[8]          = "WetFire";
   stateTransitionOnNotWet[8]       = "CheckTarget";
   
   stateName[9]                     = "WetFire";
   stateTransitionOnNoAmmo[9]       = "NoAmmo";
   stateTransitionOnTimeout[9]      = "Reload";
   stateSound[9]                    = MissileFireSound;
   stateRecoil[3]                   = LightRecoil;
   stateTimeoutValue[9]             = 0.4;
   stateSequence[3]                 = "Fire";
   stateScript[9]                   = "onWetFire";
   stateAllowImageChange[9]         = false;
};

function FatManImage::onFire(%data, %obj, %slot)
{
    %p = Parent::onFire(%data, %obj, %slot);
    
    if(%p)
    {
        %obj.playAudio(0, FatManFireSound);
        %obj.unmountImage(0);
        %obj.setInventory("FatMan", 0);
    }
}
