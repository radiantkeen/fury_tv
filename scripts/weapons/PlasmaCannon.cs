//--------------------------------------
// Plasma Cannon
//--------------------------------------

datablock AudioProfile(PlasmaCannonSwitchSound)
{
   filename    = "fx/weapon/plasmacannon_activate.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(PlasmaCannonFireSound)
{
   filename    = "fx/weapon/plasmacannon_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(PlasmaCannonExpSound)
{
   filename    = "fx/explosion/plasma_exp.wav";
   description = AudioExplosion3d;
};

datablock ExplosionData(PlasmaCannonBoltExplosion) : FireballAtmosphereBoltExplosion
{
   soundProfile   = PlasmaCannonExpSound;
   shakeCamera = false;
};

datablock ParticleData(FireboltFireParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = -0.2;
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 350;
   lifetimeVarianceMS   = 0;

   textureName          = "particleTest";

   useInvAlpha = false;
   spinRandomMin = -180.0;
   spinRandomMax = 180.0;

   animateTexture = true;
   framesPerSec = 15;

   animTexName[0]       = "special/Explosion/exp_0016";
   animTexName[1]       = "special/Explosion/exp_0018";
   animTexName[2]       = "special/Explosion/exp_0020";
   animTexName[3]       = "special/Explosion/exp_0022";
   animTexName[4]       = "special/Explosion/exp_0024";
   animTexName[5]       = "special/Explosion/exp_0026";
   animTexName[6]       = "special/Explosion/exp_0028";
   animTexName[7]       = "special/Explosion/exp_0030";
   animTexName[8]       = "special/Explosion/exp_0032";

   colors[0]     = "1.0 0.7 0.5 1.0";
   colors[1]     = "1.0 0.5 0.2 1.0";
   colors[2]     = "1.0 0.25 0.1 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 4.0;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(FireboltFireEmitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 2;

   ejectionVelocity = 0.25;
   velocityVariance = 0.0;

   thetaMin         = 0.0;
   thetaMax         = 30.0;

   particles = "FireboltFireParticle";
};

datablock ParticleData(FireboltSmokeParticle)
{
   dragCoeffiecient     = 4.0;
   gravityCoefficient   = -0.00;   // rises slowly
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 100;   // ...more or less

   textureName          = "particleTest";

   useInvAlpha =     true;

   spinRandomMin = -50.0;
   spinRandomMax = 50.0;

   colors[0]     = "0.3 0.3 0.3 0.0";
   colors[1]     = "0.3 0.3 0.3 1.0";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 2;
   sizes[1]      = 3;
   sizes[2]      = 5;
   times[0]      = 0.0;
   times[1]      = 0.7;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(FireboltSmokeEmitter)
{
   ejectionPeriodMS = 12;
   periodVarianceMS = 4;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.5;

   thetaMin         = 10.0;
   thetaMax         = 30.0;

   useEmitterSizes = false;

   particles = "FireboltSmokeParticle";
};

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------
datablock LinearFlareProjectileData(PlasmaCannonBolt)
{
   projectileShapeName = "plasmabolt.dts";
   scale               = "4.0 4.0 4.0";
   faceViewer          = true;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 1.8;
   damageRadius        = 10.0;
   kickBackStrength    = 1500.0;
   radiusDamageType    = $DamageType::PlasmaCannon;

   explosion           = "PlasmaCannonBoltExplosion";
   splash              = PlasmaSplash;
   
   flags               = $Projectile::PlaysHitSound | $Projectile::Plasma;
   ticking             = false;
   headshotMultiplier  = 1.0;

   dryVelocity       = 250.0; // z0dd - ZOD, 7/20/02. Faster plasma projectile. was 55
   wetVelocity       = -1;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 2000;
   lifetimeMS        = 3000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   //activateDelayMS = 100;
   activateDelayMS = -1;

   size[0]           = 0.2;
   size[1]           = 0.5;
   size[2]           = 0.1;

   baseEmitter         = FireboltFireEmitter;
   delayEmitter        = FireboltSmokeEmitter;

   numFlares         = 35;
   flareColor        = "1 0.75 0.25";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

	sound        = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;
   
   hasLight    = true;
   lightRadius = 3.0;
   lightColor  = "1 0.75 0.25";
};


//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(PlasmaCannonAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_plasma.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some plasma cannon ammo";
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(PlasmaCannon)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "turret_fusion_large.dts";
   image = PlasmaCannonImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a plasma cannon";
};

datablock ShapeBaseImageData(PlasmaCannonImage)
{
   className = WeaponImage;
   shapeFile = "turret_fusion_large.dts";
   item = PlasmaCannon;
   ammo = PlasmaCannonAmmo;
   offset = "0 0 0";

   projectile = PlasmaCannonBolt;
   projectileType = LinearFlareProjectile;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Activate";
   stateSound[0] = PlasmaCannonSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "CheckWet";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.5;
   stateFire[3] = true;
   stateRecoil[3] = LightRecoil;
   stateAllowImageChange[3] = false;
   stateScript[3] = "onFire";
   stateEmitterTime[3] = 0.2;
   stateSound[3] = PlasmaCannonFireSound;

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 1.0;
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";
   stateSound[4] = PlasmaReloadSound;

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
   stateSound[6]      = PlasmaDryFireSound;
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";
   
   stateName[7]       = "WetFire";
   stateSound[7]      = PlasmaFireWetSound;
   stateTimeoutValue[7]        = 1.5;
   stateTransitionOnTimeout[7] = "Ready";
   
   stateName[8]               = "CheckWet";
   stateTransitionOnWet[8]    = "WetFire";
   stateTransitionOnNotWet[8] = "Fire"; 
};

