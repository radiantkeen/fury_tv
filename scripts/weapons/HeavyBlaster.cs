//--------------------------------------
// HeavyBlaster
//--------------------------------------

//--------------------------------------------------------------------------
// Sounds
//--------------------------------------

datablock AudioProfile(LNullifierFireSound)
{
   filename    = "fx/weapon/nullifier_turret_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(LNullifierExpSound)
{
   filename    = "fx/explosion/phasercannon_exp.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock ShockwaveData(LNullifierShockwave)
{
   width = 6;
   numSegments = 32;
   numVertSegments = 7;
   velocity = 3;
   acceleration = 32;
   lifetimeMS = 500;
   height = 0.65;
   verticalCurve = 0.35;

   mapToTerrain = false;
   renderBottom = true;
   orientToNormal = true;

//   texture[0] = "special/crescent4";
   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 3.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 0.85;
   times[3] = 1;

   colors[0] = "0.333 0.04 0.267 1.0";
   colors[1] = "0.333 0.04 0.267 1.0";
   colors[2] = "0.167 0.02 0.135 0.75";
   colors[3] = "0.0 0.0 0.0 0.0";
};

datablock ParticleData( LNullifierDebrisSmokeParticle )
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.01;   // rises slowly
   inheritedVelFactor   = 0.125;

   lifetimeMS           =  800;
   lifetimeVarianceMS   =  400;
   useInvAlpha          =  false;
   spinRandomMin        = -100.0;
   spinRandomMax        =  100.0;

   animateTexture = false;

   textureName = "flarebase"; //"special/Smoke/bigSmoke";

   colors[0]     = "0.9 0.9 0.9 0.5";
   colors[1]     = "0.333 0.04 0.267 0.8";
   colors[2]     = "0.167 0.02 0.135 0.345";
   colors[3]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 0.25;
   sizes[1]      = 0.35;
   sizes[2]      = 0.425;
   sizes[3]      = 0.65;
   times[0]      = 0.0;
   times[1]      = 0.45;
   times[2]      = 0.9;
   times[3]      = 1.0;
};

datablock ParticleEmitterData( LNullifierDebrisSmokeEmitter )
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 1;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.2;

   thetaMin         = 0.0;
   thetaMax         = 40.0;

   particles = "LNullifierDebrisSmokeParticle";
};

datablock DebrisData(LNullifierDebris)
{
   emitters[0] = LNullifierDebrisSmokeEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 0.85;
   lifetimeVariance = 0.25;

   numBounces = 1;
};

datablock ParticleData(LNullifierExplosionParticle)
{
    dragCoefficient = 0;
    gravityCoefficient = -0;
    windCoefficient = 1;
    inheritedVelFactor = 0.025;
    constantAcceleration = -0.8;
    lifetimeMS = 1500;
    lifetimeVarianceMS = 500;
    useInvAlpha = 0;
    spinRandomMin = -200;
    spinRandomMax = 200;
    textureName = "special/Smoke/smoke_003";
    times[0] = 0;
    times[1] = 0.5;
    times[2] = 1;
    colors[0] = "0.333 0.04 0.267 1.000000";
    colors[1] = "0.166667 0.02 0.133333 1.000000";
    colors[2] = "0.000000 0.000000 0.000000 0.000000";
    sizes[0] = 2.29839;
    sizes[1] = 3.87903;
    sizes[2] = 7.5;
};

datablock ParticleEmitterData(LNullifierExplosionEmitter)
{
    ejectionPeriodMS = 8;
    periodVarianceMS = 2;
    ejectionVelocity = 10.0;
    velocityVariance = 3.0;
    ejectionOffset =   0.25;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    lifeTimeMs = 500;
    particles = "LNullifierExplosionParticle";
};

datablock ParticleData(LNullifierSparksParticle)
{
    dragCoefficient = 1;
    gravityCoefficient = 0;
    windCoefficient = 1;
    inheritedVelFactor = 0.2;
    constantAcceleration = 0;
    lifetimeMS = 750;
    lifetimeVarianceMS = 250;
    useInvAlpha = false;
    spinRandomMin = 0;
    spinRandomMax = 0;
    textureName = "special/bigspark";
    times[0] = 0;
    times[1] = 0.540323;
    times[2] = 1;
    colors[0] = "1 0 1 0.500000";
    colors[1] = "1 0 1 0.500000";
    colors[2] = "0 0 0 0.500000";
    sizes[0] = 0.733871;
    sizes[1] = 1.2345;
    sizes[2] = 1.5;
};

datablock ParticleEmitterData(LNullifierSparksEmitter)
{
    ejectionPeriodMS = 6;
    periodVarianceMS = 1;
    ejectionVelocity = 12;
    velocityVariance = 5;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 1;
    orientOnVelocity = 1;
    lifeTimeMs = 375;
    particles = "LNullifierSparksParticle";
};

datablock ExplosionData(LNullifierExplosion)
{
   soundProfile   = LNullifierExpSound;
   shockwave      = LNullifierShockwave;

   emitter[0] = LNullifierExplosionEmitter;
   emitter[1] = LNullifierSparksEmitter;

   debris = LNullifierDebris;
   debrisThetaMin = 0;
   debrisThetaMax = 180;
   debrisNum = 6;
   debrisVelocity = 25.0;
   debrisVelocityVariance = 7.5;

   shakeCamera = true;
   camShakeFreq = "6.0 7.0 5.0";
   camShakeAmp = "35.0 35.0 35.0";
   camShakeDuration = 0.4;
   camShakeRadius = 10.0;
};

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------

datablock LinearFlareProjectileData(HeavyBlasterBolt)
{
   projectileShapeName = "Fireball_projectile.dts";
   scale               = "2.0 2.0 2.0";
   directDamage        = 1.2;
   directDamageType    = $DamageType::HeavyBlaster;

   explosion           = "HeavyBlasterExplosion";

   dryVelocity       = 300.0;
   wetVelocity       = 300.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 100;

   numFlares         = 10;
   size              = 0.65;
   flareColor        = "1 0.8 0.1";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   sound = SentryTurretProjectileSound;

   hasLight    = true;
   lightRadius = 1.5;
   lightColor  = "1.0 0.8 0.1";
};

//------------------------------------------------------------------------------

datablock LinearFlareProjectileData(PhaserCannonBolt)
{
   scale               = "2 2 2";
   faceViewer          = true;
   directDamage        = 0.0;
//   directDamageType    = $DamageType::Nullifier;
   hasDamageRadius     = true;
   indirectDamage      = 1.25;
   damageRadius        = 1.0;
   radiusDamageType    = $DamageType::PhaserCannon;

   kickBackStrength    = 500.0;

   explosion           = "LNullifierExplosion";
   splash              = PlasmaSplash;
   flags               = $Projectile::PlaysHitSound | $Projectile::PartialShieldPhase;
   ticking             = false;
   headshotMultiplier  = 1.0;

   dryVelocity       = 425;
   wetVelocity       = 425;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   activateDelayMS = 1;

   numFlares         = 35;
   flareColor        = "0.333 0.04 0.267";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   size[0]           = 0.5;
   size[1]           = 1.25;
   size[2]           = 2.0;

	sound        = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 3.0;
   lightColor  = "0.333 0.04 0.267";
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ShapeBaseImageData(HeavyBlasterImage)
{
   className = WeaponImage;
   shapeFile = "turret_mortar_large.dts";
   item = HeavyBlaster;
   projectile = PhaserCannonBolt;
   projectileType = LinearFlareProjectile;

   usesEnergy = true;
   fireEnergy = 32;
   minEnergy = 40;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "deploy";
   stateSound[0] = BlasterSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.5;
   stateFire[3] = true;
   stateRecoil[3] = NoRecoil;
   stateAllowImageChange[3] = false;
   stateSequence[3] = "Fire";
   stateSound[3] = LNullifierFireSound;
   stateScript[3] = "onFire";

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateAllowImageChange[4] = false;
   stateTimeoutValue[4] = 1.0;
   stateSequence[4] = "deploy";

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";
   
   stateName[6] = "DryFire";
   stateTimeoutValue[6] = 0.3;
   stateSound[6] = BlasterDryFireSound;
   stateTransitionOnTimeout[6] = "Ready";
};

datablock ItemData(HeavyBlaster)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "turret_mortar_large.dts";
   image = HeavyBlasterImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a phaser cannon";
};

