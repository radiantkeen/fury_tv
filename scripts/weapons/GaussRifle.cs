//--------------------------------------
// Gauss rifle
//--------------------------------------

datablock AudioProfile(GaussRifleFireSound)
{
   filename    = "fx/weapon/gaussrifle_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(GaussExpSound)
{
   filename    = "fx/explosion/gauss_exp.wav";
   description = AudioExplosion3d;
};

//--------------------------------------------------------------------------
// Explosion
//--------------------------------------
datablock ParticleData(SpikeSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.3;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 150;
   textureName          = "special/bigspark";
   colors[0]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 1.0";
   colors[1]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.5";
   colors[2]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.75;
   sizes[2]      = 1.0;
   times[0]      = 0.25;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(SpikeSparksEmitter)
{
   ejectionPeriodMS = 2;
   periodVarianceMS = 1;
   ejectionVelocity = 10;
   velocityVariance = 4.375;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 180;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "SpikeSparks";
};

datablock ExplosionData(SpikeCoreExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   faceViewer           = true;

   delayMS = 150;

   offset = 0;

   playSpeed = 1.5;

   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(GaussExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   soundProfile   = GaussExpSound;

   emitter[0] = SpikeSparksEmitter;
   subExplosion[0] = SpikeCoreExplosion;

   shakeCamera = true;
   camShakeFreq = "5.0 5.0 5.0";
   camShakeAmp = "5.0 5.0 5.0";
   camShakeDuration = 0.35;
   camShakeRadius = 4.0;

   faceViewer           = true;
};

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------

datablock TracerProjectileData(GaussBolt)
{
   doDynamicClientHits = true;

   projectileShapeName = "";
   directDamage        = 0.0;
   directDamageType    = $DamageType::Gauss;
   hasDamageRadius     = true;
   indirectDamage      = 0.75;
   damageRadius        = 4.0;
   kickBackStrength    = 500.0;
   radiusDamageType    = $DamageType::Gauss;
   sound          	   = BlasterProjectileSound;
   explosion           = GaussExplosion;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Kinetic;
   ticking             = false;
   headshotMultiplier  = 1.0;

   dryVelocity       = 500.0;
   wetVelocity       = 500.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 2000;
   lifetimeMS        = 2000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   activateDelayMS = -1;

   tracerLength    = 5;
   tracerAlpha     = false;
   tracerMinPixels = 3;
   tracerColor     = "1 0 0 1";
	tracerTex[0]  	 = "special/landSpikeBolt";
	tracerTex[1]  	 = "special/landSpikeBoltCross";
	tracerWidth     = 0.35;
   crossSize       = 0.79;
   crossViewAng    = 0.990;
   renderCross     = true;
   emap = true;
};

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(GaussRifleAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_plasma.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some gauss rifle ammo";
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(GaussRifle)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_assault.dts";
   image = GaussRifleImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a gauss rifle";
};

datablock ShapeBaseImageData(GaussRifleImage)
{
   className = WeaponImage;
   shapeFile = "weapon_assault.dts";
   item = GaussRifle;
   ammo = GaussRifleAmmo;
   offset = "0 0 0";
   armThread = looksn;
   
   projectile = GaussBolt;
   projectileType = TracerProjectile;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Activate";
   stateSound[0] = PlasmaSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.3;
   stateFire[3] = true;
   stateRecoil[3] = LightRecoil;
   stateAllowImageChange[3] = false;
   stateScript[3] = "onFire";
   stateSound[3] = GaussRifleFireSound;
   stateSequence[3] = "Fire_Vis";
   
   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 0.7;
   stateAllowImageChange[4] = false;

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
   stateSound[6]      = PlasmaDryFireSound;
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";
   
   stateName[7]       = "WetFire";
   stateSound[7]      = PlasmaFireWetSound;
   stateTimeoutValue[7]        = 1.5;
   stateTransitionOnTimeout[7] = "Ready";
   
   stateName[8]               = "CheckWet";
   stateTransitionOnWet[8]    = "WetFire";
   stateTransitionOnNotWet[8] = "Fire"; 
};

