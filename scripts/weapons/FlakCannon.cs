//--------------------------------------
// Flak Cannon
//--------------------------------------

datablock AudioProfile(FlakCannonFireSound)
{
   filename    = "fx/weapon/flak_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(FlakExplosionSound)
{
   filename    = "fx/explosion/flak_exp.wav";
   description = AudioAmbient3d;
   preload = true;
};

datablock ExplosionData(FlakShellExplosion) : HandGrenadeExplosion
{
   soundProfile   = FlakExplosionSound;

   debris = GrenadeDebris;
   debrisThetaMin = 1;
   debrisThetaMax = 180;
   debrisNum = 10;
   debrisVelocity = 26.0;
   debrisVelocityVariance = 7.0;
};

datablock LinearFlareProjectileData(FlakShellBurst)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.6;
   damageRadius        = 20.0;
   radiusDamageType    = $DamageType::Flak;
   kickBackStrength    = 500;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Explosive;
   ticking             = false;
   headshotMultiplier  = 1.25;

   sound               = GrenadeProjectileSound;
   explosion           = "FlakShellExplosion";
   underwaterExplosion = "UnderwaterGrenadeExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 32;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock LinearProjectileData(FlakBullet)
{
   scale = "1.0 1.0 1.0";
   projectileShapeName = "grenade_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 0;
   directDamageType    = $DamageType::Flak;
   hasDamageRadius     = true;
   indirectDamage      = 0.6;
   damageRadius        = 20;
   radiusDamageType    = $DamageType::Flak;
   kickBackStrength    = 500;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Explosive;
   ticking             = true;
   headshotMultiplier  = 1.0;

   explosion           = "FlakShellExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

//   baseEmitter         = MissileSmokeEmitter;
//   delayEmitter        = WildcatJetEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 300;
   wetVelocity       = 200;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 500;
   lifetimeMS        = 600;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 600;

   sound = "ChaingunProjectile";

   activateDelayMS = -1;

   hasLight    = false;
   lightRadius = 7.0;
   lightColor  = "0.4 0.15 0.1";
};

function FlakBullet::onTick(%this, %proj)
{
    Parent::onTick(%this, %proj);

    InitContainerRadiusSearch(%proj.position, %this.damageRadius * 0.675, $TypeMasks::VehicleObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::ProjectileObjectType);

    while((%int = ContainerSearchNext()) != 0)
    {
        if(%int.client.team == %proj.instigator.team || %int.team == %proj.instigator.team)
            continue;

        if(%int.getType() & $TypeMasks::ProjectileObjectType)
        {
            if(%int.getDatablock().getName() $= "FlareGrenadeProj")
                return %this.detonate(%proj);
        }
        else
        {
            if(%int.getHeat() > 0.75)
                return %this.detonate(%proj);
        }
    }
}

function FlakBullet::detonate(%this, %proj)
{
    transformProjectile(%proj, "LinearFlareProjectile", "FlakShellBurst", %proj.position, %proj.initialDirection);
}

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(FlakCannonAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_grenade.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some flak cannon ammo";
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(FlakCannon)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_flamer.dts";
   image = FlakCannonImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a flak cannon";
};

datablock ShapeBaseImageData(FlakCannonImage)
{
   className = WeaponImage;
   shapeFile = "weapon_flamer.dts";
   item = FlakCannon;
   ammo = FlakCannonAmmo;
   offset = "0 0 0";

   projectile = FlakBullet;
   projectileType = LinearProjectile;

   projectileSpread = 5.0;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
//   stateSequence[0] = "Activate";
   stateSound[0] = PlasmaSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "CheckWet";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.4;
   stateFire[3] = true;
   stateRecoil[3] = LightRecoil;
   stateAllowImageChange[3] = false;
   stateScript[3] = "onFire";
   stateEmitterTime[3] = 0.1;
   stateSound[3] = FlakCannonFireSound;
   stateSequence[3] = "Fire";
   
   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 0.233;
   stateAllowImageChange[4] = false;
   stateSequence[4] = "activate";
//   stateSound[4] = PlasmaReloadSound;

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
   stateSound[6]      = PlasmaDryFireSound;
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";
   
   stateName[7]       = "WetFire";
   stateSound[7]      = PlasmaFireWetSound;
   stateTimeoutValue[7]        = 1.5;
   stateTransitionOnTimeout[7] = "Ready";
   
   stateName[8]               = "CheckWet";
   stateTransitionOnWet[8]    = "WetFire";
   stateTransitionOnNotWet[8] = "Fire"; 
};
