//--------------------------------------
// Dual Blaster Turret
//--------------------------------------
datablock AudioProfile(BolterFireSound)
{
   filename    = "fx/weapon/beamer_bolter_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(BolterImpact)
{
   filename    = "fx/weapons/cg_metal2.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(ShrikeBlasterFire)
{
   filename    = "fx/vehicles/shrike_blaster.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(ShrikeBlasterProjectile)
{
   filename    = "fx/weapons/shrike_blaster_projectile.wav";
   description = ProjectileLooping3d;
   preload = true;
};

datablock ParticleData(BolterExplosionParticle1)
{
   dragCoefficient      = 0.65;
   gravityCoefficient   = 0.3;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 500;
   lifetimeVarianceMS   = 150;
   textureName          = "particleTest";
   colors[0]     = "0.56 0.36 0.26 1.0";
   colors[1]     = "0.56 0.36 0.26 0.0";
   sizes[0]      = 0.0625;
   sizes[1]      = 0.2;
};

datablock ParticleEmitterData(BolterExplosionEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 0.75;
   velocityVariance = 0.25;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "BolterExplosionParticle1";
};

datablock ParticleData(BolterImpactSmokeParticle)
{
   dragCoefficient      = 0.0;
   gravityCoefficient   = -0.2;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 200;
   useInvAlpha          = true;
   spinRandomMin        = -90.0;
   spinRandomMax        = 90.0;
   textureName          = "particleTest";
   colors[0]     = "0.7 0.7 0.7 0.0";
   colors[1]     = "0.7 0.7 0.7 0.4";
   colors[2]     = "0.7 0.7 0.7 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.5;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(BolterImpactSmoke)
{
   ejectionPeriodMS = 8;
   periodVarianceMS = 1;
   ejectionVelocity = 1.0;
   velocityVariance = 0.5;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 35;
   overrideAdvances = false;
   particles = "BolterImpactSmokeParticle";
   lifetimeMS       = 50;
};

datablock ParticleData(BolterSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 350;
   lifetimeVarianceMS   = 0;
   textureName          = "special/spark00";
   colors[0]     = "0.56 0.36 0.26 1.0";
   colors[1]     = "0.56 0.36 0.26 1.0";
   colors[2]     = "1.0 0.36 0.26 0.0";
   sizes[0]      = 0.6;
   sizes[1]      = 0.2;
   sizes[2]      = 0.05;
   times[0]      = 0.0;
   times[1]      = 0.7;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(BolterSparkEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 16;
   velocityVariance = 4.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 50;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 150;
   particles = "BolterSparks";
};

datablock ExplosionData(BolterExplosion)
{
   soundProfile   = ""; // BolterImpact

   emitter[0] = BolterImpactSmoke;
   emitter[1] = BolterSparkEmitter;

   faceViewer           = false;
};

datablock LinearProjectileData(BolterBullet)
{
   scale = "2.333 13 2.333";
   projectileShapeName = "chaingun_shot.dts";
   emitterDelay        = -1;
   directDamage        = 0.45;
   directDamageType    = $DamageType::Bolter;
   hasDamageRadius     = false;
   kickBackStrength    = 3200;

   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   playRandomChaingunSound = true;
   
   explosion           = "BolterExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

//   baseEmitter         = MissileSmokeEmitter;
//   delayEmitter        = WildcatJetEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 375;
   wetVelocity       = 225;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 3000;
   lifetimeMS        = 3000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 3000;

   sound = "ChaingunProjectile";

   activateDelayMS = -1;

   hasLight    = false;
   lightRadius = 7.0;
   lightColor  = "0.4 0.15 0.1";
};

//--------------------------------------------------------------------------
// Sounds
//--------------------------------------

datablock TracerProjectileData(LightAABlasterBolt)
{
   doDynamicClientHits = true;

   directDamage        = 0.3;
   explosion           = "ScoutChaingunExplosion";
   splash              = ChaingunSplash;

   directDamageType    = $DamageType::Blaster;
   kickBackStrength  = 0.0;

   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   sound = ShrikeBlasterProjectileSound;

   dryVelocity       = 425.0;
   wetVelocity       = 100.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 3000;

   tracerLength    = 20;
   tracerAlpha     = false;
   tracerMinPixels = 3;
   tracerColor     = "1 1 1 1";
	tracerTex[0]  	 = "special/shrikeBolt";
	tracerTex[1]  	 = "special/shrikeBoltCross";
	tracerWidth     = 0.55;
   crossSize       = 0.99;
   crossViewAng    = 0.990;
   renderCross     = true;
   emap = true;
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ShapeBaseImageData(BolterImage)
{
   className = WeaponImage;
   shapeFile = "weapon_energy1.dts";
   item = Bolter;
   projectile = LightAABlasterBolt;
   projectileType = TracerProjectile;

//   armThread = looksn;

   usesEnergy = true;
   fireEnergy = 8;
   minEnergy = 10;

   projectileSpread = 0.0;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Activate";
   stateSound[0] = BlasterSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.2;
   stateFire[3] = true;
   stateRecoil[3] = NoRecoil;
   stateAllowImageChange[3] = false;
   stateSequence[3] = "Fire_Vis";
   stateSound[3] = ShrikeBlasterFire;
   stateScript[3] = "onFire";

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";
   
   stateName[6] = "DryFire";
   stateTimeoutValue[6] = 0.3;
   stateSound[6] = BlasterDryFireSound;
   stateTransitionOnTimeout[6] = "Ready";
};

datablock ItemData(Bolter)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_energy1.dts";
   image = BolterImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "an anti-air blaster";
};

