//--------------------------------------
// Mortar
//--------------------------------------

datablock AudioProfile(HowitzerFireSound)
{
   filename    = "fx/weapon/howitzer_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(HowitzerExplosionSound)
{
   filename    = "fx/explosion/howitzer_shell_exp.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock ExplosionData(HowitzerExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   playSpeed = 1.5;
   soundProfile   = HowitzerExplosionSound;
   faceViewer = true;

   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "0.5 0.5 0.5";
   sizes[2] = "0.5 0.5 0.5";

   emitter[0] = MissileExplosionSmokeEmitter;

   debris = MissileSpikeDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 170;
   debrisNum = 8;
   debrisNumVariance = 6;
   debrisVelocity = 22.5;
   debrisVelocityVariance = 2.5;

   shakeCamera = true;
   camShakeFreq = "6.0 7.0 7.0";
   camShakeAmp = "70.0 70.0 70.0";
   camShakeDuration = 1.0;
   camShakeRadius = 7.0;
};

//---------------------------------------------------------------------------
// Smoke particles
//---------------------------------------------------------------------------
datablock ParticleData(HowitzerSmokeParticle)
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = 0.0;   // rises slowly
   inheritedVelFactor   = 0.25;

   lifetimeMS           =  600;
   lifetimeVarianceMS   =  200;
   useInvAlpha          =  true;
   spinRandomMin        = -100.0;
   spinRandomMax        =  100.0;

   animateTexture = false;

   textureName = "special/Smoke/bigSmoke";

   colors[0]     = "1.0 1.0 1.0 0.5";
   colors[1]     = "0.7 0.7 0.7 0.8";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 2.0;
   sizes[2]      = 4.5;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(HowitzerSmokeEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 3;

   ejectionVelocity = 2.25;
   velocityVariance = 0.55;

   thetaMin         = 0.0;
   thetaMax         = 40.0;

   particles = "HowitzerSmokeParticle";
};

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------

datablock LinearProjectileData(HowitzerShot)
{
   scale = "4.0 4.0 4.0";
   projectileShapeName = "weapon_missile_casement.dts";
   emitterDelay        = -1;
   hasDamageRadius     = true;
   indirectDamage      = 3.0;
   damageRadius        = 3.0;
   radiusDamageType    = $DamageType::Howitzer;
   kickBackStrength    = 6000;
   
   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Kinetic;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "HowitzerExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   baseEmitter         = HowitzerSmokeEmitter;
//   delayEmitter        = MissileFireEmitter;
   bubbleEmitter       = MortarBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 200;
   wetVelocity       = 150;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 2000;
   lifetimeMS        = 2000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 4000;

   sound = MissileProjectileSound;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 5.0;
   lightColor  = "0.2 0.05 0";
};

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(HowitzerAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_mortar.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some howitzer ammo";

   computeCRC = true;

};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(Howitzer)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "MiniCannon2.dts";
   image = HowitzerImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a howitzer";

   computeCRC = true;
   emap = true;
};

datablock ShapeBaseImageData(HowitzerImage)
{
   className = WeaponImage;
   shapeFile = "MiniCannon2.dts";
   item = Howitzer;
   ammo = HowitzerAmmo;
   offset = "0 0 0";
   emap = true;

   projectile = HowitzerShot;
   projectileType = LinearProjectile;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Activate";
   stateSound[0] = MortarSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";
   //stateSound[2] = MortarIdleSound;

   stateName[3] = "Fire";
   stateSequence[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.8;
   stateFire[3] = true;
   stateRecoil[3] = LightRecoil;
   stateAllowImageChange[3] = false;
   stateScript[3] = "onFire";
   stateSound[3] = HowitzerFireSound;

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 2.2;
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";
   stateSound[4] = MortarReloadSound;

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
   stateSound[6]      = MortarDryFireSound;
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";
};
