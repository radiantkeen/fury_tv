datablock AudioProfile(DeployablesExplosionSound)
{
   filename = "fx/explosions/deployables_explosion.wav";
   description = AudioExplosion3d;
   preload = true;
};

datablock ExplosionData(SmallMiscExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   playSpeed      = 0.5;
   soundProfile   = DeployablesExplosionSound;
   faceViewer     = true;

   emitter[0] = MineExplosionSmokeEmitter;
   emitter[1] = MineCrescentEmitter;

   shakeCamera = false;
   camShakeFreq = "8.0 7.0 9.0";
   camShakeAmp = "10.0 10.0 10.0";
   camShakeDuration = 1.0;
   camShakeRadius = 15.0;
};

datablock ExplosionData(UnderwaterSmallMiscExplosion)
{
   explosionShape = "disc_explosion.dts";
   playSpeed      = 1.0;
   sizes[0] = "0.4 0.4 0.4";
   sizes[1] = "0.4 0.4 0.4";
   soundProfile   = DeployablesExplosionSound;
   faceViewer     = true;

   emitter[0] = UnderwaterMineExplosionSmokeEmitter;
   emitter[1] = UnderwaterMineCrescentEmitter;
   emitter[2] = MineExplosionBubbleEmitter;

   shakeCamera = false;
   camShakeFreq = "8.0 7.0 9.0";
   camShakeAmp = "10.0 10.0 10.0";
   camShakeDuration = 1.0;
   camShakeRadius = 10.0;
};

datablock ShockwaveData(MagIonOLShockwave)
{
   width = 8.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 30;
   acceleration = 50.0;
   lifetimeMS = 1250;
   height = 1.0;
   verticalCurve = 0.5;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1 1 1 0.50";
   colors[1] = "1 1 1 0.25";
   colors[2] = "1 1 1 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ExplosionData(MagIonOLExplosion)
{
   soundProfile   = HugeExplosionSound;

   shockwave = MagIonOLShockwave;

   emitter[0] = MortarExplosionSmokeEmitter;
   emitter[1] = MortarCrescentEmitter;

   subExplosion[0] = ImpactSubExplosion1;
   subExplosion[1] = ImpactSubExplosion2;
   subExplosion[2] = ImpactSubExplosion3;

   debris = MortarDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 50;
   debrisNum = 8;
   debrisVelocity = 26.0;
   debrisVelocityVariance = 7.0;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "100.0 100.0 100.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;
};

datablock ExplosionData(MagIonOLUnderwaterExplosion)
{
   soundProfile   = HugeExplosionUWSound;

   shockwave = UnderwaterMortarShockwave;
   shockwaveOnTerrain = true;

   subExplosion[0] = UnderwaterMortarSubExplosion1;
   subExplosion[1] = UnderwaterMortarSubExplosion2;
   subExplosion[2] = UnderwaterMortarSubExplosion3;

   emitter[0] = MortarExplosionBubbleEmitter;
   emitter[1] = UnderwaterExplosionSparksEmitter;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "100.0 100.0 100.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;
};

datablock LinearFlareProjectileData(MagIonOLAreaCharge)
{
   projectileShapeName = "turret_muzzlepoint.dts";
   faceViewer          = false;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.2;
   damageRadius        = 6.0;
   radiusDamageType    = $DamageType::MagIonReactor;
   kickBackStrength    = 0;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::MagIonReactor;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 20;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.25;

   explosion           = "SmallMiscExplosion";
   underwaterExplosion = "UnderwaterSmallMiscExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock LinearFlareProjectileData(MagIonOLDeathCharge)
{
   projectileShapeName = "turret_muzzlepoint.dts";
   faceViewer          = false;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 2.0;
   damageRadius        = 15.0;
   radiusDamageType    = $DamageType::MagIonReactor;
   kickBackStrength    = 3000;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::MagIonReactor;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 250;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.25;

   explosion           = "MagIonOLExplosion";
   underwaterExplosion = "MagIonOLUnderwaterExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock LinearFlareProjectileData(TurbochargerDeathCharge)
{
   projectileShapeName = "turret_muzzlepoint.dts";
   faceViewer          = false;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 1.5;
   damageRadius        = 10.0;
   radiusDamageType    = $DamageType::Turbocharger;
   kickBackStrength    = 1000;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Turbocharger;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 150;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlaysHitSound | $Projectile::CountMAs;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "EMPulseMortarExplosion";
   underwaterExplosion = "EMPulseMortarExplosion";

   radialStatusEffect  = "EMPulseEffect";
   statusEffectChance  = 0.9;
   statusEffectMask    = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType | $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType;

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock LinearFlareProjectileData(StarHammerAreaCharge)
{
   projectileShapeName = "turret_muzzlepoint.dts";
   faceViewer          = false;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.2;
   damageRadius        = 8.0;
   radiusDamageType    = $DamageType::StarHammer;
   kickBackStrength    = 0;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::StarHammer;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 25;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "SmallMiscExplosion";
   underwaterExplosion = "UnderwaterSmallMiscExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock ParticleData(ExtendedAAExplosionParticle1)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/crescent4";
   olors[0]     = "0.8 0.5 0.5 0.5";
   colors[1]     = "0.7 0.4 0.4 0.8";
   colors[2]     = "0.6 0.3 0.3 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 2.0;
   sizes[2]      = 4.5;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(ExtendedAAExplosionEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 2;
   velocityVariance = 1.5;
   ejectionOffset   = 0.0;
   thetaMin         = 70;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "ExtendedAAExplosionParticle1";
};

datablock ParticleData(ExtendedAAExplosionParticle2)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/blasterHit";
   olors[0]     = "0.8 0.5 0.5 0.5";
   colors[1]     = "0.7 0.4 0.4 0.8";
   colors[2]     = "0.6 0.3 0.3 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 2.0;
   sizes[2]      = 4.5;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(ExtendedAAExplosionEmitter2)
{
   ejectionPeriodMS = 30;
   periodVarianceMS = 0;
   ejectionVelocity = 1;
   velocityVariance = 0.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = false;
   lifetimeMS       = 200;
   particles = "ExtendedAAExplosionParticle2";
};

datablock ExplosionData(ExtendedAAExplosion)
{
   soundProfile   = TurboBlasterExpSound;
   emitter[0]     = ExtendedAAExplosionEmitter;
   emitter[1]     = ExtendedAAExplosionEmitter2;
};

datablock ShockwaveData(MeteorTrailShockwave)
{
   width = 2.5;
   numSegments = 16;
   numVertSegments = 6;
   velocity = 10;
   acceleration = -20;
   lifetimeMS = 500;
   height = 1.0;
   verticalCurve = 0.5;
//   is2D = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1.0 1.0 1.0 0.75";
   colors[1] = "1.0 1.0 0.5 0.5";
   colors[2] = "1.0 1.0 0.1 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ExplosionData(MeteorTrailExplosion)
{
   shockwave = MeteorTrailShockwave;

   faceViewer = true;
};

datablock LinearFlareProjectileData(MeteorTrailCharge)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.0001;
   damageRadius        = 1;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 750;

   explosion           = "MeteorTrailExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock ShockwaveData(BluePowerShockwave)
{
   width = 5.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 15;
   acceleration = -30;
   lifetimeMS = 500;
   height = 0.75;
   verticalCurve = 0.4;
//   is2D = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.3 0.8 1.0 0.75";
   colors[1] = "0.3 0.4 1.0 0.5";
   colors[2] = "0.3 0.2 1.0 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ExplosionData(BluePowerDisplayExplosion)
{
//   soundProfile   = MeteorShockwaveSound;
   shockwave = BluePowerShockwave;

   faceViewer = true;
};

datablock LinearFlareProjectileData(BluePowerDisplayCharge)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.0001;
   damageRadius        = 5;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 1250;

   explosion           = "BluePowerDisplayExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock ParticleData(MeteorExplosionParticle)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 750;
   lifetimeVarianceMS   = 150;
   textureName          = "particleTest";
   colors[0]     = "0.15 0.2 1.0 1.0";
   colors[1]     = "0.6 0.1 1.0 0.0";
   sizes[0]      = 2;
   sizes[1]      = 5;
};

datablock ParticleEmitterData(MeteorExplosionEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 5;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "MeteorExplosionParticle";
};

datablock ParticleData(MeteorTrailParticle)
{
   dragCoefficient      = 2.75;
   gravityCoefficient   = 0.1;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 900;
   lifetimeVarianceMS   = 0;
   textureName          = "particleTest";
   colors[0]     = "0.15 0.2 1.0 1.0";
   colors[1]     = "0.6 0.1 1.0 0.0";
   sizes[0]      = 2.0;
   sizes[1]      = 1.5;
};

datablock ParticleEmitterData(MeteorTrailEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 10;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 12;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvance  = true;
   particles = "MeteorTrailParticle";
};

datablock AudioProfile(MeteorExplosionSound)
{
   filename    = "fx/explosion/meteorcannon_exp.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock ExplosionData(MeteorExplosion)
{
   explosionShape = "disc_explosion.dts";
   soundProfile   = MeteorExplosionSound;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "25.0 25.0 25.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;

   particleEmitter = MeteorExplosionEmitter;
   particleDensity = 150;
   particleRadius = 3.5;
   faceViewer = true;
};

datablock ShockwaveData(PowerShockwave)
{
   width = 5.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 15;
   acceleration = -30;
   lifetimeMS = 500;
   height = 0.75;
   verticalCurve = 0.4;
//   is2D = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1.0 1.0 1.0 0.75";
   colors[1] = "1.0 1.0 0.5 0.5";
   colors[2] = "1.0 1.0 0.1 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ExplosionData(PowerDisplayExplosion)
{
   soundProfile   = plasmaExpSound;
   shockwave = PowerShockwave;

   faceViewer = true;
};

datablock LinearFlareProjectileData(PowerDisplayCharge)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.0001;
   damageRadius        = 5;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 1250;

   explosion           = "PowerDisplayExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock LinearFlareProjectileData(MeteorCannonBlast)
{
   scale               = "4.5 4.5 4.5";
   faceViewer          = true;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 2.5;
   damageRadius        = 6.0;
   kickBackStrength    = 3000;
   radiusDamageType    = $DamageType::MeteorCannon;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::MeteorCannon;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 250;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[1]   = 100;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = true;
   headshotMultiplier  = 1.5;

   explosion           = "MeteorExplosion";
   underwaterExplosion = "MeteorExplosion";
   splash              = PlasmaSplash;
   baseEmitter         = MeteorTrailEmitter;

   dryVelocity       = 237.0;
   wetVelocity       = 237.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 2000;
   lifetimeMS        = 2000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 15000;

   activateDelayMS = -1;
   numFlares         = 35;
   flareColor        = "0.15 0.2 1";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

	sound       = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 8.0;
   lightColor  = "0.15 0.2 1";
};

function MeteorCannonBlast::onTick(%this, %proj)
{
    Parent::onTick(%this, %proj);

    if(%proj.tickCount % 8 == 0)
        createRemoteProjectile("LinearFlareProjectile", "MeteorTrailCharge", %proj.initialDirection, %proj.position, 0, %proj.instigator);
}

datablock ParticleData(StreakTrailParticle)
{
   dragCoefficient      = 1.5;
   gravityCoefficient   = 0;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 800;
   lifetimeVarianceMS   = 0;
   textureName          = "particleTest";
   colors[0]     = "0.6 0.6 0.6 0.5";
   colors[1]     = "0.2 0.2 0.2 0";
   sizes[0]      = 0.6;
   sizes[1]      = 0.8;
};

datablock ParticleEmitterData(StreakTrailEmitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 0;
   ejectionVelocity = 1;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 10;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "StreakTrailParticle";
};

datablock AudioProfile(HeavyPlasmaExpSound)
{
   filename    = "fx/explosion/plasma_medium_exp.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock ExplosionData(HeavyPlasmaExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   soundProfile   = HeavyPlasmaExpSound;
   particleEmitter = PlasmaExplosionEmitter;
   particleDensity = 150;
   particleRadius = 3.25;
   faceViewer = true;

   sizes[0] = "2.0 2.0 2.0";
   sizes[1] = "2.0 2.0 2.0";
   times[0] = 0.0;
   times[1] = 1.0;
};
