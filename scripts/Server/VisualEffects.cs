// Global Visual Effects library
$g_FXOrigin = "0 0 -1000 1 0 0 0";

datablock StaticShapeData(FXPoint)
{
   shapeFile = "turret_muzzlepoint.dts";
};

function getFXPoint()
{
    if(isObject(System.g_FXPoint))
        return System.g_FXPoint;
        
    %fxPoint = new StaticShape()
    {
        dataBlock        = FXPoint;
    };

    MissionCleanup.add(%fxPoint);
    System.g_FXPoint = %fxPoint;
      
    %fxPoint.startFade(1, 0, true);
    %fxPoint.setTransform($g_FXOrigin);
      
    return System.g_FXPoint;
}

function getFXPhantom()
{
    if(isObject(System.g_FXPhantom))
        return System.g_FXPhantom;

    %fxPhantom = new Player()
    {
        dataBlock = "LightMaleHumanArmor";
    };

    MissionCleanup.add(%fxPhantom);

    %fxPhantom.setInvincible(true);
    %fxPhantom.startFade(1, 0, true);
    %fxPhantom.setTransform($g_FXOrigin);
    %fxPhantom.isZapper = true;
    System.g_FXPhantom = %fxPhantom;
    
    return System.g_FXPhantom;
}

datablock ShockLanceProjectileData(FXZap)
{
   directDamage        = 0;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 0;
   velInheritFactor    = 0;
   sound               = "";

   zapDuration = 1.0;
   impulse = 0;
   boltLength = 14.0;
   extension = 14.0;            // script variable indicating distance you can shock people from
   lightningFreq = 50.0;
   lightningDensity = 18.0;
   lightningAmp = 0.25;
   lightningWidth = 0.05;


   boltSpeed[0] = 2.0;
   boltSpeed[1] = -0.5;

   texWrap[0] = 1.5;
   texWrap[1] = 1.5;

   startWidth[0] = 0.3;
   endWidth[0] = 0.6;
   startWidth[1] = 0.3;
   endWidth[1] = 0.6;

   texture[0] = "special/shockLightning01";
   texture[1] = "special/shockLightning02";
   texture[2] = "special/shockLightning03";
   texture[3] = "special/ELFBeam";
};

datablock ShockLanceProjectileData(FXPulse)
{
   directDamage        = 0;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 0;
   velInheritFactor    = 0;
   sound               = "";

   zapDuration = 1.0;
   impulse = 0;
   boltLength = 14.0;
   extension = 14.0;            // script variable indicating distance you can shock people from
   lightningFreq = 50.0;
   lightningDensity = 18.0;
   lightningAmp = 0.25;
   lightningWidth = 0.05;


   boltSpeed[0] = 2.0;
   boltSpeed[1] = -0.5;

   texWrap[0] = 1.5;
   texWrap[1] = 1.5;

   startWidth[0] = 0.3;
   endWidth[0] = 0.6;
   startWidth[1] = 0.3;
   endWidth[1] = 0.6;

   texture[0] = "special/nonlingradient";
   texture[1] = "special/nonlingradient";
   texture[2] = "special/nonlingradient";
   texture[3] = "special/nonlingradient";
};

datablock ShockLanceProjectileData(FXHealGreen)
{
   directDamage        = 0;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 0;
   velInheritFactor    = 0;
   sound               = "";

   zapDuration = 1.5;
   impulse = 0;
   boltLength = 14.0;
   extension = 14.0;            // script variable indicating distance you can shock people from
   lightningFreq = 50.0;
   lightningDensity = 18.0;
   lightningAmp = 0.25;
   lightningWidth = 0.05;

   boltSpeed[0] = 6.0;
   boltSpeed[1] = -3.0;

   texWrap[0] = 1.5;
   texWrap[1] = 1.5;

   startWidth[0] = 0.3;
   endWidth[0] = 0.6;
   startWidth[1] = 0.3;
   endWidth[1] = 0.6;

   texture[0] = "liquidTiles/LushWater01_Algae";
   texture[1] = "liquidTiles/LushWater01";
   texture[2] = "liquidTiles/LushWater01_Algae";
   texture[3] = "liquidTiles/LushWater01";
};

datablock ShockLanceProjectileData(FXBlueShift)
{
   directDamage        = 0;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 0;
   velInheritFactor    = 0;
   sound               = "";

   zapDuration = 0.6;
   impulse = 0;
   boltLength = 14.0;
   extension = 14.0;            // script variable indicating distance you can shock people from
   lightningFreq = 50.0;
   lightningDensity = 18.0;
   lightningAmp = 0.25;
   lightningWidth = 0.05;


   boltSpeed[0] = 2.0;
   boltSpeed[1] = -0.5;

   texWrap[0] = 1.5;
   texWrap[1] = 1.5;

   startWidth[0] = 0.3;
   endWidth[0] = 0.6;
   startWidth[1] = 0.3;
   endWidth[1] = 0.6;

   texture[0] = "special/shieldenvmap";
   texture[1] = "special/shieldmap";
   texture[2] = "special/shieldenvmap";
   texture[3] = "special/shieldmap";
};

datablock ShockLanceProjectileData(FXRedShift)
{
   directDamage        = 0;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 0;
   velInheritFactor    = 0;
   sound               = "";

   zapDuration = 1.0;
   impulse = 0;
   boltLength = 14.0;
   extension = 14.0;            // script variable indicating distance you can shock people from
   lightningFreq = 50.0;
   lightningDensity = 18.0;
   lightningAmp = 0.25;
   lightningWidth = 0.05;


   boltSpeed[0] = 2.0;
   boltSpeed[1] = -0.5;

   texWrap[0] = 1.5;
   texWrap[1] = 1.5;

   startWidth[0] = 0.3;
   endWidth[0] = 0.6;
   startWidth[1] = 0.3;
   endWidth[1] = 0.6;

   texture[0] = "special/redbump2";
   texture[1] = "special/redbump2";
   texture[2] = "special/redbump2";
   texture[3] = "special/redbump2";
};

datablock ShockLanceProjectileData(FXWhiteNoise)
{
   directDamage        = 0;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 0;
   velInheritFactor    = 0;
   sound               = "";

   zapDuration = 1.0;
   impulse = 0;
   boltLength = 14.0;
   extension = 14.0;            // script variable indicating distance you can shock people from
   lightningFreq = 50.0;
   lightningDensity = 18.0;
   lightningAmp = 0.25;
   lightningWidth = 0.05;


   boltSpeed[0] = 2.0;
   boltSpeed[1] = -0.5;

   texWrap[0] = 1.5;
   texWrap[1] = 1.5;

   startWidth[0] = 0.3;
   endWidth[0] = 0.6;
   startWidth[1] = 0.3;
   endWidth[1] = 0.6;

   texture[0] = "special/cloaktexture";
   texture[1] = "special/cloaktexture";
   texture[2] = "special/cloaktexture";
   texture[3] = "special/cloaktexture";
};

function zapVehicle(%obj, %lanceData)
{
    %pilot = %obj.getMountNodeObject(0);
    %zapper = !isObject(%pilot) ? getFXPhantom() : %pilot;
    %pos = %pilot ? %pilot.getPosition() : %obj.getPosition();

    if(!%pilot)
    {
        %zapper.setPosition(vectorAdd(%pos, "0 0 -1"));
        %zapper.setVelocity("0 0 0");
    }

    %zap = new ShockLanceProjectile()
    {
        dataBlock        = %lanceData;
        initialDirection = VectorRand();
        initialPosition  = vectorAdd(%pos, "0 0 0.6");
        sourceObject     = %zapper;
        sourceSlot       = 0;
        targetId         = %obj;
    };
    
    MissionCleanup.add(%zap);

    if(!%pilot)
        %zapper.setTransform($g_FXOrigin);
}

// Uncomment this line if zapObject is being fiaky
function zapObject(%obj, %lanceData)
{
//    return zapEffect(%obj, %lanceData);

    %zap = new ShockLanceProjectile()
    {
        dataBlock        = %lanceData;
        initialDirection = "0 0 1";
        initialPosition  = %obj.position;
        sourceObject     = %obj;
        sourceSlot       = 1;
        targetId         = %obj;
    };

    MissionCleanup.add(%zap);
}

function zapEffect(%obj, %lanceData)
{
    if(%obj.isVehicle())
        return zapVehicle(%obj, %lanceData);

    %pos = vectorAdd(%obj.position, "0 0 -0.25");

    %point = getFXPoint();
    %point.setPosition(%pos);

    %zap = new ShockLanceProjectile()
    {
        dataBlock        = %lanceData;
        initialDirection = "0 0 1"; // VectorRand()
        initialPosition  = %pos;
        sourceObject     = %point;
        sourceSlot       = 0;
        targetId         = %obj;
    };

    MissionCleanup.add(%zap);

    %point.setTransform($g_FXOrigin);
}

// todo: is this necessary?
function deleteSlotExtension(%obj, %slot)
{
   if(!%slot) // f00lpr00f
      %slot = 0;

   %ext = %obj.getMountNodeObject(%slot);
   if(!%ext)
      return;

   if(%ext.isExtension)
      %ext.delete();

   %obj.slotExtension[%slot] = "";
}

function isSlotExtension(%obj)
{
   if(isObject(%obj))
      if(%obj.isExtension)
         return true;

   return false;
}

function ShapeBase::createSlotExtension(%obj, %slot)
{
     createSlotExtension(%obj, %slot);
}

function ShapeBase::deleteSlotExtension(%obj, %slot)
{
     deleteSlotExtension(%obj, %slot);
}

datablock ShapeBaseImageData(MainAPEImage)
{
   className = WeaponImage;
   shapeFile = "turret_muzzlepoint.dts";
   offset = "0 0 0";
   rotation = "0 0 1 0";
   usesEnergy = true;
   minEnergy = -1; // fool the energy system into firing constant.

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "Idle";
   stateTimeoutValue[0] = 0.01;

   stateName[2] = "Idle";
   stateTransitionOnTriggerDown[2] = "Emit";

   stateName[3] = "Emit";
   stateTransitionOnTimeout[3] = "Idle";
   stateTimeoutValue[3] = 0.032; // min 32ms recharge time
   stateFire[3] = true;

   stateEmitter[3]       = "ChaingunFireSmoke"; // emitter
   stateEmitterTime[3]       = 0.1; //100ms
   stateEmitterNode[3]       = 0; // model node (most models dont come with any nodes)
//   stateScript[3]           = "onFire";
};

datablock ShapeBaseImageData(EngineAPEImage)
{
   className = WeaponImage;
   shapeFile = "turret_muzzlepoint.dts";
   offset = "0 0 0";
   rotation = "0 0 1 0";
   usesEnergy = true;
   minEnergy = -1; // fool the energy system into firing constant.

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "Idle";
   stateTimeoutValue[0] = 0.01;

   stateName[1] = "Idle";
   stateTransitionOnTriggerDown[1] = "JetOn";

   stateName[2] = "JetOn";
   stateTransitionOnTriggerDown[2] = "JetMaintain";
   stateSequence[2] = "ActivateBack";
   stateDirection[2] = true;
   stateTimeoutValue[2] = 0.5;

   stateName[3] = "JetMaintain";
   stateTransitionOnTriggerUp[3] = "JetOff";
   stateTransitionOnTimeout[3]   = "JetMaintain";
   stateTimeoutValue[3] = 0.01;
   stateSequence[3] = "MaintainBack";
//   stateFire[3] = true;

   stateName[4] = "JetOff";
   stateTransitionOnTriggerDown[4] = "JetOn";
   stateSequence[4] = "ActivateBack";
   stateDirection[4] = false;
   stateTimeoutValue[4] = 0.5;
   stateWaitForTimeout[4]          = false;
   stateTransitionOnTimeout[4]     = "Idle";
};

datablock ShapeBaseImageData(TriggerAPEImage)
{
   className = WeaponImage;
   shapeFile = "turret_muzzlepoint.dts";
   offset = "0 0 0";
   rotation = "0 0 1 0";
//   item = Plasma; // hehe
   usesEnergy = true;
   minEnergy = -1; // fool the energy system into firing constant.

//   casing              = ReaverShellDebris;
//   shellExitDir        = "1.0 0.3 1.0";
//   shellExitOffset     = "0.15 -0.56 -0.1";
//   shellExitVariance   = 15.0;
//   shellVelocity       = 3.0;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "Idle";
   stateTimeoutValue[0] = 0.01;

   stateName[2] = "Idle";
   stateTransitionOnTriggerDown[2] = "Emit";

   stateName[3] = "Emit";
   stateTransitionOnTriggerUp[3] = "Idle";
   stateTransitionOnTimeout[3]   = "Emit";
   stateTimeoutValue[3] = 0.032; // min 32ms recharge time
   stateFire[3] = true;

//   stateEmitter[3]       = "ChaingunFireSmoke"; // emitter
//   stateEmitterTime[3]       = 0.1; //100ms
//   stateEmitterNode[3]       = 0; // model node (most models dont come with any nodes)
//   stateScript[3]           = "onFire";
//   stateEjectShell[3]       = true;
};

// Datablock Cache
//

datablock ShockLanceProjectileData(FXZap)
{
   directDamage        = 0;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 0;
   velInheritFactor    = 0;
   sound               = "";

   zapDuration = 1.0;
   impulse = 0;
   boltLength = 14.0;
   extension = 14.0;            // script variable indicating distance you can shock people from
   lightningFreq = 50.0;
   lightningDensity = 18.0;
   lightningAmp = 0.25;
   lightningWidth = 0.05;


   boltSpeed[0] = 2.0;
   boltSpeed[1] = -0.5;

   texWrap[0] = 1.5;
   texWrap[1] = 1.5;

   startWidth[0] = 0.3;
   endWidth[0] = 0.6;
   startWidth[1] = 0.3;
   endWidth[1] = 0.6;

   texture[0] = "special/shockLightning01";
   texture[1] = "special/shockLightning02";
   texture[2] = "special/shockLightning03";
   texture[3] = "special/ELFBeam";
};

datablock ShockLanceProjectileData(FXPulse)
{
   directDamage        = 0;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 0;
   velInheritFactor    = 0;
   sound               = "";

   zapDuration = 1.0;
   impulse = 0;
   boltLength = 14.0;
   extension = 14.0;            // script variable indicating distance you can shock people from
   lightningFreq = 50.0;
   lightningDensity = 18.0;
   lightningAmp = 0.25;
   lightningWidth = 0.05;


   boltSpeed[0] = 2.0;
   boltSpeed[1] = -0.5;

   texWrap[0] = 1.5;
   texWrap[1] = 1.5;

   startWidth[0] = 0.3;
   endWidth[0] = 0.6;
   startWidth[1] = 0.3;
   endWidth[1] = 0.6;

   texture[0] = "special/nonlingradient";
   texture[1] = "special/nonlingradient";
   texture[2] = "special/nonlingradient";
   texture[3] = "special/nonlingradient";
};

datablock ParticleData(TeleporterParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = -0.5;
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 100;
   lifetimeVarianceMS   = 50;

   textureName          = "particleTest";

   useInvAlpha = false;
   spinRandomMin = -160.0;
   spinRandomMax = 160.0;

   animateTexture = true;
   framesPerSec = 15;


   animTexName[0]       = "special/Explosion/exp_0016";
   animTexName[1]       = "special/Explosion/exp_0018";
   animTexName[2]       = "special/Explosion/exp_0020";
   animTexName[3]       = "special/Explosion/exp_0022";
   animTexName[4]       = "special/Explosion/exp_0024";
   animTexName[5]       = "special/Explosion/exp_0026";
   animTexName[6]       = "special/Explosion/exp_0028";
   animTexName[7]       = "special/Explosion/exp_0030";
   animTexName[8]       = "special/Explosion/exp_0032";

   colors[0]     = "1 1 1 1";
   colors[1]     = "0.75  0.75 1.0";
   colors[2]     = "0.5 0.5 0.5 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 1;
   sizes[2]      = 2.5;
   times[0]      = 0.0;
   times[1]      = 0.7;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(TeleporterEmitter)
{
   ejectionPeriodMS = 3;
   periodVarianceMS = 0;
   ejectionVelocity = 6;
   velocityVariance = 2.9;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 5;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "TeleporterParticle";
};

function createEmitter(%pos, %emitter, %rot)
{
    %dummy = new ParticleEmissionDummy()
    {
          position = %pos;
          rotation = %rot;
          scale = "1 1 1";
          dataBlock = defaultEmissionDummy;
          emitter = %emitter;
          velocity = "1";
    };
    MissionCleanup.add(%dummy);

    if(isObject(%dummy))
       return %dummy;
}

function createLifeEmitter(%pos, %emitter, %lifeMS, %rot)
{
    %dummy = createEmitter(%pos, %emitter, %rot);
    %dummy.schedule(%lifeMS, delete);
    return %dummy;
}

function projectileTrail(%obj, %time, %projType, %proj, %bSkipFirst, %offset)
{
   if(isObject(%obj) && isObject(%obj.sourceObject))
   {
      if(!%obj.lastBlinkPos)
         %obj.lastBlinkPos = "65536 65536 65536";

      if(%bSkipFirst)
      {
         schedule(%time, 0, "projectileTrail", %obj, %time, %projType, %proj, false, %offset);
         return;
      }

      if(%offset)
         %pos = vectorAdd(%obj.position, vectorScale(%obj.initialDirection, %offset));
      else
         %pos = %obj.position;

      if( %obj.exploded )	// +soph
         return;		// +soph

      if(vectorCompare(%pos, %obj.lastBlinkPos)) //same position...BUG!
         return;

      %p = new (%projType)()
      {
         dataBlock        = %proj;
         initialDirection = %obj.initialDirection;
         initialPosition  = %pos;
         sourceObject     = %obj.sourceObject;
         sourceSlot       = 0;
      };
      MissionCleanup.add(%p);
      %p.exploded = true;	// +soph

      schedule(%time, 0, projectileTrail, %obj, %time, %projType, %proj, false, %offset);
      %obj.lastBlinkPos = %pos;
   }
}

function spawnShockwave(%data, %pos, %vec) // originally found by Lt. Earthworm
{
   %wave = new Shockwave()
   {
      dataBlock = %data;
      pos = %pos;
      normal = %vec;
   };
   MissionCleanup.add(%wave);

   return %wave;
}

function spawnShockwaveTrail(%obj, %time, %data)
{
   if(isObject(%obj))
   {
      %pos = %obj.position;

      if(vectorCompare(%pos, %obj.lastShockBlinkPos)) //same position...BUG!
         return;

      %wave = spawnShockwave(%data, %pos, %obj.initialDirection);

      schedule(%time, 0, spawnShockwaveTrail, %obj, %time, %data);
      %obj.lastShockBlinkPos = %pos;
   }
}

// todo: convert to $g_tickTime fluid motion, find a way to get all attached
// and/or nearby objects to follow suit
function moveObjectInSteps(%obj, %startPos, %endPos, %steps, %stepTime)
{
   if(isObject(%obj))
   {
      if(%steps < 1)
         %steps = 2;

      // vectorDist()
      %dist = getDistance3D(%startPos, %endPos);
      if(%dist > 1)
      {
         // vectorFromPoints()
         %vector = getVectorFromPoints(%startPos, %endPos);
         %stepLen = vectorScale(%vector, (1/%steps));
         %obj.stepVector = %vector;
         moveStepLoop(%obj, %stepLen, %steps, %stepTime);
      }
      else
         error("moveObjectInSteps: min step size = 1");
   }
}

function moveStepLoop(%obj, %moveVecChunk, %iterations, %time)
{
   if(isObject(%obj))
   {
      if(!%obj.loopBreak[moveObjectInSteps])
      {
         if(%iterations > 0)
         {
            %pos = %obj.getPosition();
            %endPos = vectorAdd(%pos, %moveVecChunk);
            %obj.setPosition(%endPos);
            schedule(%time, 0, "moveStepLoop", %obj, %moveVecChunk, %iterations--, %time);
         }
      }
   }
}

function createFlash(%pos, %radius, %intensity, %disableFlashOverlap)
{
   InitContainerRadiusSearch(%pos, %radius, $TypeMasks::PlayerObjectType | $TypeMasks::TurretObjectType);

   while((%flashMe = containerSearchNext()) != 0)
   {
      %eyePointVec = getVectorFromPoints(%flashMe.getEyePoint(), %pos);
      %eyeVec = %flashMe.getEyeVector();
      %eyeDot = (%d = VectorDot(%eyeVec, %eyePointVec)) <= 0 ? 0 : %d;
      %eyeDistPct = getDistance3D(%flashMe.getEyePoint(), %pos) / %radius;
      %eyeFlashPct = (%intensity * %eyeDistPct) / 2 + (%intensity * %eyeDot) / 2;
      %whiteoutVal = %disableFlashOverlap == true ? %eyeFlashPct : %flashMe.getWhiteOut() + %eyeFlashPct;
      %flashMe.setWhiteOut(%whiteoutVal);
   }
}

function createRealisticFlash(%pos, %radius, %intensity, %disableFlashOverlap)
{
   InitContainerRadiusSearch(%pos, %radius, $TypeMasks::PlayerObjectType | $TypeMasks::TurretObjectType);

   while((%flashMe = containerSearchNext()) != 0)
   {
      %eyePointVec = getVectorFromPoints(%flashMe.getEyePoint(), %pos);
      %vec = VectorScale(%eyePointVec, %radius);
      %end = VectorAdd(%pos, %vec);
      %canFlash = containerRayCast(%pos, %end, $TypeMasks::NonTransparent);

      if(%canFlash)
      {
         %eyeVec = %flashMe.getEyeVector();
         %eyeDistPct = getDistance3D(%flashMe.getEyePoint(), %pos) / %radius;
         %eyeDot = VectorDot(%eyeVec, %eyePointVec);
         %eyeDot = %eyeDot > 0 ? %eyeDot : 0;
         %eyeFlashPct = %intensity * %eyeDot * %eyeDistPct;

         if($FuncDebugMode) // example of FuncDebugMode in action
         {
            echo("Flash Obj  :" SPC %flashMe);
            echo("Intensity  :" SPC %intensity);
            echo("Max Radius :" SPC %radius);
            echo("DistPct    :" SPC %eyeDistPct);
            echo("Dot        :" SPC %eyeDot);
            echo("FlashPct   :" SPC %eyeFlashPct);
         }

         %whiteoutVal = %disableFlashOverlap == true ? %eyeFlashPct : %flashMe.getWhiteOut() + %eyeFlashPct;
         %flashMe.setWhiteOut(%whiteoutVal);
      }
   }
}

function createWind(%pos, %area, %force, %time)
{
   if(%time <= 0)
      return;

   %time -= 100;

   InitContainerRadiusSearch(%pos, %area, $TypeMasks::VehicleObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::ItemObjectType | $TypeMasks::CorpseObjectType); // );

   %numTargets = 0;
   while ((%targetObject = containerSearchNext()) != 0)
   {
      %dist = containerSearchCurrRadDamageDist();

      if (%dist > %area)
         continue;

      %targets[%numTargets]     = %targetObject;
      %targetDists[%numTargets] = %dist;
      %numTargets++;
   }

   for (%i = 0; %i < %numTargets; %i++)
   {
      %targetObject = %targets[%i];
      %dist = %targetDists[%i];
      %distPct = %dist / %area;

      %coverage = calcExplosionCoverage(%pos, %targetObject,
                                        ($TypeMasks::InteriorObjectType |
                                         $TypeMasks::TerrainObjectType |
                                         $TypeMasks::ForceFieldObjectType));
      if (%coverage == 0)
         continue;

      %tgtPos = %targetObject.getWorldBoxCenter();
      %vec = vectorNeg(vectorNormalize(vectorSub(%pos, %tgtPos)));
      %nForce = %force * %distPct * 10;
      %forceVector = vectorScale(%vec, %nForce);

      if(!%targetObject.inertialDampener && !%targetObject.getDatablock().forceSensitive)
          %targetObject.applyImpulse(%pos, %forceVector);
   }
   schedule(100, 0, "createWind", %pos, %area, %force, %time);
}

// keen: WARNING! Lightning objects are not cleaned up properly (or at all)
// on the server side, creating too many lightning objects can run a server
// out of memory... though much more relevant when servers had less than
// 512mb of ram...
function createLightning(%pos, %rad, %strikesPerMin, %hitPercentage, %scale1000)
{
   %obj = new Lightning() // new Lightning(Lightning)? necessary?
   {
      position = %pos;
     	rotation = "1 0 0 0";
     	scale = %scale1000 ? %rad SPC %rad SPC (%rad+1000) : %rad SPC %rad SPC (%rad + 1000);
    	dataBlock = "DefaultStorm";
    	lockCount = "0";
    	homingCount = "0";
    	strikesPerMinute = %strikesPerMin;
    	strikeWidth = "2.5";
    	chanceToHitTarget = %hitPercentage;
    	strikeRadius = "20";
    	boltStartRadius = "20";
    	color = "1.000000 1.000000 1.000000 1.000000";
    	fadeColor = "0.100000 0.100000 1.000000 1.000000";
    	useFog = "0";
  	};

   return %obj;
}

function createMeteorShower(%pos, %rad, %dropsPerMin, %hitPercentage)
{
   %obj = new FireballAtmosphere()
   {
      position = %pos;
     	rotation = "1 0 0 0";
     	scale = "1 1 1";
     	dataBlock = "fireball";
     	lockCount = "0";
     	homingCount = "0";
     	dropRadius = %rad;
     	dropsPerMinute = %dropsPerMin;
     	minDropAngle = "0";
     	maxDropAngle = "30";
     	startVelocity = "300";
     	dropHeight = "1000";
     	dropDir = "0.212 0.212 -0.953998";
  	};

   return %obj;
}

// todo: refactor (missing functions)
function loopDamageFX(%obj, %radius, %count, %rate, %blocktype, %block) // default; for displaying random explosions around a player
{
   if(!isObject(%obj))
      return;

   if(%blocktype $= "" || %block $= "")
      return;

   if(%rate > 31)
   {
      error("loopDamageFX::rate cannot exceed 31 iterations/second due to engine limitations");
      %rate = 31;
   }

   if(%rate < 1)
   {
      error("loopDamageFX::rate cannot be lower than 1 iteration/second");
      %rate = 1;
   }

   if(%obj.DFX_Count <= %count && %obj)
   {
      %pos = %obj.getPosition();
      %px = getWord(%pos, 0) + getRandomT(%radius);
      %py = getWord(%pos, 1) + getRandomT(%radius);
      %pz = getWord(%pos, 2) + getRandomT(%radius);
      %npos = %px SPC %py SPC %pz;

      %p = new (%blocktype)() {
         dataBlock        = %block;
         initialDirection = "0 0 0";
         initialPosition  = %npos;
         sourceObject     = %obj;
         sourceSlot       = 0;
      };
      %p.ignoreReflections = true ;	// +soph
      MissionCleanup.add(%p);

      %obj.DFX_Count++;

      schedule((1/%rate)*1000, 0, "loopDamageFX", %obj, %radius, %count, %rate, %blocktype, %block);
   }
   else
      %obj.DFX_Count = 0;
}

$DFX_ID = 0;

function loopDamageFX2(%obj, %pos, %radius, %count, %rate, %blocktype, %block, %id) // enhanced; for displaying random explosions around a position
{
   if(!isObject(%obj))
      return;

   if(%blocktype $= "" || %block $= "")
      return;

   if(%rate > 31)
   {
      error("loopDamageFX::rate cannot exceed 31 iterations/second due to engine limitations");
      %rate = 31;
   }

   if(%rate < 1)
   {
      error("loopDamageFX::rate cannot be lower than 1 iteration/second");
      %rate = 1;
   }

   if(%id $= "")
   {
      $DFX_ID++;
      %id = $DFX_ID;
   }

   if(%obj.proj[%id, DFX_Count] <= %count && %obj)
   {
      %px = getWord(%pos, 0) + getRandomT(%radius);
      %py = getWord(%pos, 1) + getRandomT(%radius);
      %pz = getWord(%pos, 2) + getRandomT(%radius);
      %npos = %px SPC %py SPC %pz;

      %p = new (%blocktype)() {
         dataBlock        = %block;
         initialDirection = "0 0 0";
         initialPosition  = %npos;
         sourceObject     = %obj;
         sourceSlot       = 0;
      };
      MissionCleanup.add(%p);

      %obj.proj[%id, DFX_Count]++;
      schedule((1/%rate)*1000, 0, "loopDamageFX2", %obj, %pos, %radius, %count, %rate, %blocktype, %block, %id);
   }
   else
      %obj.DFX_Count = 0;
}

//------------------------------------------------------------------------------
// Various Effect Datablocks

datablock ParticleData(OnFireParticle)
{
    dragCoefficient = 0;
    gravityCoefficient = 0.3;
    windCoefficient = 0;
    inheritedVelFactor = 0.862903;
    constantAcceleration = 0;
    lifetimeMS = 750;
    lifetimeVarianceMS = 749; //750; --- can't be > or = (ST)
    useInvAlpha = 0;
    spinRandomMin = -145.161;
    spinRandomMax = 133.065;
    textureName = "special/tracer00.PNG";
//    textureName = "flarebase.png";
    times[0] = 0;
    times[1] = 1;
    colors[0] = "0.850394 0.304000 0.192000 0.709677";
    colors[1] = "0.000000 0.000000 0.000000 0.451613";
    sizes[0] = 1.97581;
    sizes[1] = 1.35484;
};

datablock ParticleEmitterData(OnFireEmitter)
{
    ejectionPeriodMS = 7;
    periodVarianceMS = 0;
    ejectionVelocity = 5.79032;
    velocityVariance = 2.30645;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 14.5161;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "OnFireParticle";
};

datablock ParticleData(TurboJetParticle)
{
   dragCoeffiecient     = 1.5;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.4;

   lifetimeMS           = 100;
   lifetimeVarianceMS   = 25;

   textureName          = "particleTest";

   useInvAlpha = false;
   spinRandomMin = -160.0;
   spinRandomMax = 160.0;

   animateTexture = true;
   framesPerSec = 15;

   animTexName[0]       = "special/Explosion/exp_0016";
   animTexName[1]       = "special/Explosion/exp_0018";
   animTexName[2]       = "special/Explosion/exp_0020";
   animTexName[3]       = "special/Explosion/exp_0022";
   animTexName[4]       = "special/Explosion/exp_0024";
   animTexName[5]       = "special/Explosion/exp_0026";
   animTexName[6]       = "special/Explosion/exp_0028";
   animTexName[7]       = "special/Explosion/exp_0030";
   animTexName[8]       = "special/Explosion/exp_0032";

   colors[0]     = "1.0 0.7 0.5 1.0";
   colors[1]     = "1.0 0.5 0.2 1.0";
   colors[2]     = "1.0 0.25 0.1 0.0";
   sizes[0]      = 2;
   sizes[1]      = 5;
   sizes[2]      = 6;
   times[0]      = 0.0;
   times[1]      = 0.7;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(TurboJetEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 20;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 10;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "TurboJetParticle";
};

datablock ParticleData(BlueJetParticle)
{
    dragCoefficient = 0;
    gravityCoefficient = -0.01;
    windCoefficient = 1;
    inheritedVelFactor = 0.624194;
    constantAcceleration = -1.6129;
    lifetimeMS = 325;
    lifetimeVarianceMS = 96;
    useInvAlpha = 0;
    spinRandomMin = -64.5161;
    spinRandomMax = 381.048;
    textureName = "particleTest.png";
    times[0] = 0;
    times[1] = 0.112903;
    times[2] = 1;
    colors[0] = "1.000000 1.000000 1.000000 0.209677";
    colors[1] = "0.000000 0.304000 1.000000 0.314516";
    colors[2] = "0.464567 0.608000 1.000000 0.000000";
    sizes[0] = 6;
    sizes[1] = 6;
    sizes[2] = 2;
};

datablock ParticleEmitterData(BlueJetEmitter)
{
    ejectionPeriodMS = 3;
    periodVarianceMS = 2;
    ejectionVelocity = 9.78226;
    velocityVariance = 3.17742;
    ejectionOffset =   1;
    thetaMin = 0;
    thetaMax = 0;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
   particles = "BlueJetParticle";
};

datablock ParticleData(RedJetParticle)
{
    dragCoefficient = 2;
    gravityCoefficient = 0.2;
    windCoefficient = 0;
    inheritedVelFactor = 0.9;
    constantAcceleration = -1.1129;
    lifetimeMS = 250;
    lifetimeVarianceMS = 125;
    useInvAlpha = 0;
    spinRandomMin = -64.5161;
    spinRandomMax = 381.048;
    textureName = "special/cloudflash2.png";
    times[0] = 0;
    times[1] = 1;
    colors[0] = "1.000000 0.104000 0.000000 0.758065";
    colors[1] = "1.000000 0.648000 0.384000 0.000000";
    sizes[0] = 6;
    sizes[1] = 2;
};

datablock ParticleEmitterData(RedJetEmitter)
{
    ejectionPeriodMS = 7;
    periodVarianceMS = 2;
    ejectionVelocity = 12;
    velocityVariance = 3.17742;
    ejectionOffset =   2;
    thetaMin = 0;
    thetaMax = 2.90323;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
   particles = "RedJetParticle";
};

datablock ParticleData(RealFlyerJetParticle)
{
    dragCoefficient = 0.292683;
    gravityCoefficient = -0.01;
    windCoefficient = 0;
    inheritedVelFactor = 0.8;
    constantAcceleration = -1.32258;
    lifetimeMS = 200;
    lifetimeVarianceMS = 50;
    useInvAlpha = 0;
    spinRandomMin = -200;
    spinRandomMax = 0;
    textureName = "flarebase.png";
    times[0] = 0;
    times[1] = 0.274194;
    times[2] = 1;
    colors[0] = "0.000000 0.000000 1.000000 0.354839";
    colors[1] = "0.354331 0.024000 0.000000 0.782258";
    colors[2] = "1.000000 0.184000 0.000000 0.620968";
    sizes[0] = 6;
    sizes[1] = 4;
    sizes[2] = 2;
};

datablock ParticleEmitterData(RealFlyerJetEmitter)
{
    ejectionPeriodMS = 7;
    periodVarianceMS = 6;
    ejectionVelocity = 8;
    velocityVariance = 4;
    ejectionOffset = 1;
    thetaMin = 0;
    thetaMax = 0;
    phiReferenceVel = 360;
    phiVariance = 11.6129;
    overrideAdvances = 0;
    orientParticles = 0;
    orientOnVelocity = 1;
   particles = "RealFlyerJetParticle";
};

datablock ParticleData(GreenFJetParticle)
{
    dragCoefficient = 2;
    gravityCoefficient = 0.2;
    windCoefficient = 0;
    inheritedVelFactor = 0.9;
    constantAcceleration = -1.1129;
    lifetimeMS = 250;
    lifetimeVarianceMS = 125;
    useInvAlpha = 0;
    spinRandomMin = -64.5161;
    spinRandomMax = 381.048;
    textureName = "special/chuteTexture.png";
    times[0] = 0;
    times[1] = 1;
    colors[0] = "0.133858 1.000000 0.000000 0.758065";
    colors[1] = "0.086614 1.000000 0.384000 0.000000";
    sizes[0] = 6;
    sizes[1] = 3;
};

datablock ParticleEmitterData(GreenFJetEmitter)
{
    ejectionPeriodMS = 7;
    periodVarianceMS = 2;
    ejectionVelocity = 12;
    velocityVariance = 3.17742;
    ejectionOffset =   1;
    thetaMin = 0;
    thetaMax = 2.90323;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "GreenFJetParticle";
};


datablock ParticleData(UnderwaterPhaserExplosionSmoke)
{
   dragCoeffiecient     = 80.0;
   gravityCoefficient   = -0.3;   // rises slowly
   inheritedVelFactor   = 0.025;

   constantAcceleration = -0.75;

   lifetimeMS           = 1250;
   lifetimeVarianceMS   = 0;

   textureName          = "particleTest";

   useInvAlpha =  false;
   spinRandomMin = -200.0;
   spinRandomMax =  200.0;

   textureName = "special/Smoke/smoke_001";

   colors[0]     = "0.2 0.9 0.1 1.0";
   colors[1]     = "0.2 1.0 0.1 0.5";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 3.0;
   sizes[2]      = 5.0;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 1.0;
};

datablock ParticleData( PhaserDebrisSmokeParticle )
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.01;   // rises slowly
   inheritedVelFactor   = 0.125;

   lifetimeMS           =  800;
   lifetimeVarianceMS   =  400;
   useInvAlpha          =  false;
   spinRandomMin        = -100.0;
   spinRandomMax        =  100.0;

   animateTexture = false;

   textureName = "flarebase"; //"special/Smoke/bigSmoke";

   colors[0]     = "0.9 0.9 0.9 0.5";
   colors[1]     = "0.8 0.0 0.6 0.8";
   colors[2]     = "0.4 0.0 0.3 0.345";
   colors[3]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 0.25;
   sizes[1]      = 0.35;
   sizes[2]      = 0.425;
   sizes[3]      = 0.65;
   times[0]      = 0.0;
   times[1]      = 0.45;
   times[2]      = 0.9;
   times[3]      = 1.0;
};

datablock ParticleEmitterData( PhaserDebrisSmokeEmitter )
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 1;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.2;

   thetaMin         = 0.0;
   thetaMax         = 40.0;

   particles = "PhaserDebrisSmokeParticle";
};

datablock DebrisData(PhaserDebris)
{
   emitters[0] = PhaserDebrisSmokeEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 2.5;
   lifetimeVariance = 0.8;

   numBounces = 1;
};

datablock ParticleData(PhaserExplosionParticle)
{
    dragCoefficient = 0;
    gravityCoefficient = -0;
    windCoefficient = 1;
    inheritedVelFactor = 0.025;
    constantAcceleration = -0.8;
    lifetimeMS = 1500;
    lifetimeVarianceMS = 500;
    useInvAlpha = 0;
    spinRandomMin = -200;
    spinRandomMax = 200;
    textureName = "special/Smoke/smoke_001";
    times[0] = 0;
    times[1] = 0.5;
    times[2] = 1;
    colors[0] = "1.000000 1.000000 1.000000 1.000000";
    colors[1] = "0.8 0.0 0.6 1.000000";
    colors[2] = "0.000000 0.000000 0.000000 0.000000";
    sizes[0] = 2.29839;
    sizes[1] = 3.87903;
    sizes[2] = 7.5;
};

datablock ParticleEmitterData(PhaserExplosionEmitter)
{
    ejectionPeriodMS = 8;
    periodVarianceMS = 2;
    ejectionVelocity = 8.0;
    velocityVariance = 3.0;
    ejectionOffset =   0.25;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    lifeTimeMs = 500;
    particles = "PhaserExplosionParticle";
};

datablock ParticleData(PhaserSparksParticle)
{
    dragCoefficient = 1;
    gravityCoefficient = 0;
    windCoefficient = 1;
    inheritedVelFactor = 0.2;
    constantAcceleration = 0;
    lifetimeMS = 750;
    lifetimeVarianceMS = 250;
    useInvAlpha = 0;
    spinRandomMin = 0;
    spinRandomMax = 0;
    textureName = "special/bigspark";
    times[0] = 0;
    times[1] = 0.540323;
    times[2] = 1;
    colors[0] = "0.8 0.0 0.6 1.000000";
    colors[1] = "0.8 0.0 0.6 0.524231";
    colors[2] = "0.8 0.0 0.6 0.000000";
    sizes[0] = 0.733871;
    sizes[1] = 0.733871;
    sizes[2] = 0.75;
};

datablock ParticleEmitterData(PhaserSparksEmitter)
{
    ejectionPeriodMS = 6;
    periodVarianceMS = 1;
    ejectionVelocity = 18;
    velocityVariance = 5;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 1;
    orientOnVelocity = 1;
    lifeTimeMs = 250;
    particles = "PhaserSparksParticle";
};

datablock AudioProfile(PhaserExpSound)
{
   filename    = "fx/explosion/disruptor_exp.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(PhaserCExpSound)
{
   filename    = "fx/explosion/heavy_blaster_exp.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock ExplosionData(PhaserExplosion)
{
   soundProfile   = PhaserExpSound;

   emitter[0] = PhaserExplosionEmitter;
   emitter[1] = PhaserSparksEmitter;

   debris = PhaserDebris;
   debrisThetaMin = 25;
   debrisThetaMax = 80;
   debrisNum = 6;
   debrisVelocity = 14.0;
   debrisVelocityVariance = 3.0;

   shakeCamera = false;
   camShakeFreq = "12.0 13.0 11.0";
   camShakeAmp = "35.0 35.0 35.0";
   camShakeDuration = 0.5;
   camShakeRadius = 12.0;
};

datablock AudioProfile(PCRFireSound)
{
   filename    = "fx/weapon/phasedblaster_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock ParticleData(PCRSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0;
   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 500;
   textureName          = "special/bigspark";
   colors[0]     = "0.0 0.4 0.4 1.0";
   colors[1]     = "0.1 0.3 0.5 0.5";
   colors[2]     = "0.05 0.2 0.15 0.0";
   sizes[0]      = 1;
   sizes[1]      = 2;
   times[0]      = 0.0;
   times[1]      = 1.0;
};

datablock ParticleEmitterData(PCRSparksEmitter)
{
   ejectionPeriodMS = 1;
   periodVarianceMS = 0;
   ejectionVelocity = 13;
   velocityVariance = 5;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 180;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "PCRSparks";
};

datablock ParticleData(UnderwaterPhaserCExplosionSmoke)
{
   dragCoeffiecient     = 80.0;
   gravityCoefficient   = -0.3;   // rises slowly
   inheritedVelFactor   = 0.025;

   constantAcceleration = -0.75;

   lifetimeMS           = 1250;
   lifetimeVarianceMS   = 0;

   textureName          = "particleTest";

   useInvAlpha =  false;
   spinRandomMin = -200.0;
   spinRandomMax =  200.0;

   textureName = "special/Smoke/smoke_001";

   colors[0]     = "0.2 0.9 1.0 1.0";
   colors[1]     = "0.4 1.0 0.9 0.5";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 3.0;
   sizes[2]      = 5.0;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(UnderwaterPhaserCExplosionSmokeEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionVelocity = 5.25;
   velocityVariance = 0.25;

   thetaMin         = 0.0;
   thetaMax         = 180.0;

   lifetimeMS       = 250;

   particles = "UnderwaterPhaserCExplosionSmoke";
};

datablock ParticleData(UnderwaterPhaserCSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 500;
   lifetimeVarianceMS   = 350;
   textureName          = "special/droplet";
   colors[0]     = "0.2 0.9 1.0 1.0";
   colors[1]     = "0.2 0.9 1.0 1.0";
   colors[2]     = "0.2 0.9 1.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.25;
   sizes[2]      = 0.25;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(UnderwaterPhaserCSparkEmitter)
{
   ejectionPeriodMS = 3;
   periodVarianceMS = 0;
   ejectionVelocity = 20;
   velocityVariance = 10;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 180;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "UnderwaterPhaserCSparks";
};

datablock ExplosionData(UnderwaterPhaserCSubExplosion1)
{
   offset = 1.0;
   emitter[0] = UnderwaterPhaserCExplosionSmokeEmitter;
   emitter[1] = UnderwaterPhaserCSparkEmitter;
};

datablock ExplosionData(UnderwaterPhaserCSubExplosion2)
{
   offset = 1.0;
   emitter[0] = UnderwaterPhaserCExplosionSmokeEmitter;
   emitter[1] = UnderwaterPhaserCSparkEmitter;
};

datablock ExplosionData(UnderwaterPhaserCExplosion)
{
//   soundProfile   = GrenadeExplosionSound;

   soundProfile = PhaserCExpSound;
//   emitter[0] = PCRSparksEmitter;

   emitter[0] = UnderwaterPhaserCExplosionSmokeEmitter;
   emitter[1] = UnderwaterPhaserCSparkEmitter;

   subExplosion[0] = UnderwaterPhaserCSubExplosion1;
   subExplosion[1] = UnderwaterPhaserCSubExplosion2;

   shakeCamera = true;
   camShakeFreq = "12.0 13.0 11.0";
   camShakeAmp = "35.0 35.0 35.0";
   camShakeDuration = 0.4;
   camShakeRadius = 10.0;
};

datablock ParticleData( PhaserCDebrisSmokeParticle )
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.01;   // rises slowly
   inheritedVelFactor   = 0.125;

   lifetimeMS           =  800;
   lifetimeVarianceMS   =  400;
   useInvAlpha          =  false;
   spinRandomMin        = -100.0;
   spinRandomMax        =  100.0;

   animateTexture = false;

   textureName = "flarebase"; //"special/Smoke/bigSmoke";

   colors[0]     = "0.9 0.9 0.9 0.5";
   colors[1]     = "0.2 0.5 0.1 0.8";
   colors[2]     = "0.1 0.3 0.05 0.345";
   colors[3]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 0.25;
   sizes[1]      = 0.35;
   sizes[2]      = 0.425;
   sizes[3]      = 0.65;
   times[0]      = 0.0;
   times[1]      = 0.45;
   times[2]      = 0.9;
   times[3]      = 1.0;
};

datablock ParticleEmitterData( PhaserCDebrisSmokeEmitter )
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 1;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.2;

   thetaMin         = 0.0;
   thetaMax         = 40.0;

   particles = "PhaserCDebrisSmokeParticle";
};

datablock DebrisData(PhaserCDebris)
{
   emitters[0] = PhaserCDebrisSmokeEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 2.5;
   lifetimeVariance = 0.8;

   numBounces = 1;
};

datablock ParticleData(PhaserCExplosionParticle)
{
    dragCoefficient = 0;
    gravityCoefficient = -0;
    windCoefficient = 1;
    inheritedVelFactor = 0.025;
    constantAcceleration = -0.8;
    lifetimeMS = 1500;
    lifetimeVarianceMS = 500;
    useInvAlpha = 0;
    spinRandomMin = -200;
    spinRandomMax = 200;
    textureName = "special/Smoke/smoke_003";
    times[0] = 0;
    times[1] = 0.5;
    times[2] = 1;
    colors[0] = "1.000000 1.000000 1.000000 1.000000";
    colors[1] = "0.192000 0.700787 0.088000 1.000000";
    colors[2] = "0.000000 0.000000 0.000000 0.000000";
    sizes[0] = 2.29839;
    sizes[1] = 3.87903;
    sizes[2] = 7.5;
};

datablock ParticleEmitterData(PhaserCExplosionEmitter)
{
    ejectionPeriodMS = 8;
    periodVarianceMS = 2;
    ejectionVelocity = 10.0;
    velocityVariance = 3.0;
    ejectionOffset =   0.25;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    lifeTimeMs = 500;
    particles = "PhaserCExplosionParticle";
};

datablock ParticleData(PhaserCSparksParticle)
{
    dragCoefficient = 1;
    gravityCoefficient = 0;
    windCoefficient = 1;
    inheritedVelFactor = 0.2;
    constantAcceleration = 0;
    lifetimeMS = 750;
    lifetimeVarianceMS = 250;
    useInvAlpha = 0;
    spinRandomMin = 0;
    spinRandomMax = 0;
    textureName = "special/bigspark";
    times[0] = 0;
    times[1] = 0.540323;
    times[2] = 1;
    colors[0] = "0.253749 1.000000  0.253534 1.000000";
    colors[1] = "0.253749 1.000000  0.253534 0.524231";
    colors[2] = "0.253749 1.000000  0.253534 0.000000";
    sizes[0] = 0.733871;
    sizes[1] = 0.733871;
    sizes[2] = 0.75;
};

datablock ParticleEmitterData(PhaserCSparksEmitter)
{
    ejectionPeriodMS = 6;
    periodVarianceMS = 1;
    ejectionVelocity = 18;
    velocityVariance = 5;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 1;
    orientOnVelocity = 1;
    lifeTimeMs = 250;
    particles = "PhaserCSparksParticle";
};

datablock ExplosionData(PhaserCExplosion)
{
   soundProfile   = PhaserCExpSound;

   emitter[0] = PhaserCExplosionEmitter;
   emitter[1] = PhaserCSparksEmitter;

   debris = PhaserCDebris;
   debrisThetaMin = 0;
   debrisThetaMax = 180;
   debrisNum = 6;
   debrisVelocity = 25.0;
   debrisVelocityVariance = 7.5;

   shakeCamera = true;
   camShakeFreq = "6.0 7.0 5.0";
   camShakeAmp = "35.0 35.0 35.0";
   camShakeDuration = 0.4;
   camShakeRadius = 10.0;
};

datablock ExplosionData(PCRExplosion)
{
   explosionShape = "mortar_explosion.dts";
   playSpeed = 1.5;
   soundProfile = PhaserExpSound;
   emitter[0] = PCRSparksEmitter;
   faceViewer = true;

   sizes[0]      = 0.25;
   sizes[1]      = 0.5;
   times[0]      = 0.0;
   times[1]      = 1.0;

   shakeCamera = true;
   camShakeFreq = "10.0 6.0 9.0";
   camShakeAmp = "10.0 10.0 10.0";
   camShakeDuration = 0.5;
   camShakeRadius = 10.0;
};


// Satchel Charge
//--------------------------------------------------------------------------
// Sounds

datablock AudioProfile(SatchelChargeActivateSound)
{
   filename    = "fx/packs/satchel_pack_activate.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(SatchelChargeExplosionSound)
{
   filename = "fx/packs/satchel_pack_detonate.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock AudioProfile(SatchelChargePreExplosionSound)
{
   filename    = "fx/explosion/fatman_exp.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock AudioProfile(UnderwaterSatchelChargeExplosionSound)
{
   filename    = "fx/weapons/mortar_explode_UW.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

//----------------------------------------------------------------------------
// Satchel Debris
//----------------------------------------------------------------------------
datablock ParticleData( SDebrisSmokeParticle )
{
   dragCoeffiecient     = 1.0;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 3000;
   lifetimeVarianceMS   = 300;

   textureName          = "particleTest";

   useInvAlpha =     true;

   spinRandomMin = -60.0;
   spinRandomMax = 60.0;

   colors[0]     = "0.4 0.4 0.4 1.0";
   colors[1]     = "0.3 0.3 0.3 0.5";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 0.0;
   sizes[1]      = 2.0;
   sizes[2]      = 3.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( SDebrisSmokeEmitter )
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 1;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.2;

   thetaMin         = 0.0;
   thetaMax         = 40.0;

   particles = "SDebrisSmokeParticle";
};

datablock DebrisData( SatchelDebris )
{
   emitters[0] = SDebrisSmokeEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 0.3;
   lifetimeVariance = 0.02;
};

//----------------------------------------------------------------------------
// Bubbles
//----------------------------------------------------------------------------
datablock ParticleData(SatchelBubbleParticle)
{
   dragCoefficient      = 0.0;
   gravityCoefficient   = -0.25;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 1500;
   lifetimeVarianceMS   = 600;
   useInvAlpha          = false;
   textureName          = "special/bubbles";

   spinRandomMin        = -100.0;
   spinRandomMax        =  100.0;

   colors[0]     = "0.7 0.8 1.0 0.0";
   colors[1]     = "0.7 0.8 1.0 0.4";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 2.0;
   sizes[1]      = 2.0;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.8;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(SatchelBubbleEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 1.0;
   ejectionOffset   = 7.0;
   velocityVariance = 0.5;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "SatchelBubbleParticle";
};

//--------------------------------------------------------------------------
// Satchel Explosion Particle effects
//--------------------------------------
datablock ParticleData(SatchelExplosionSmoke)
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = 0.0;   // rises slowly
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 4000;
   lifetimeVarianceMS   = 500;

   textureName          = "particleTest";

   useInvAlpha =  true;
   spinRandomMin = -200.0;
   spinRandomMax =  200.0;

   textureName = "special/Smoke/smoke_001";

   colors[0]     = "1.0 0.7 0.0 1.0";
   colors[1]     = "0.2 0.2 0.2 0.5";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 7.0;
   sizes[1]      = 17.0;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.4;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(SatchelExplosionSmokeEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionVelocity = 7.0;
   velocityVariance = 1.5;

   thetaMin         = 0.0;
   thetaMax         = 180.0;

   lifetimeMS       = 200;

   particles = "SatchelExplosionSmoke";
};

datablock ParticleData(UnderwaterSatchelExplosionSmoke)
{
   dragCoeffiecient     = 105.0;
   gravityCoefficient   = -0.0;
   inheritedVelFactor   = 0.025;

   constantAcceleration = -1.0;

   lifetimeMS           = 1500;
   lifetimeVarianceMS   = 0;

   textureName          = "particleTest";

   useInvAlpha =  false;
   spinRandomMin = -200.0;
   spinRandomMax =  200.0;

   textureName = "special/Smoke/smoke_001";

   colors[0]     = "0.4 0.4 1.0 1.0";
   colors[1]     = "0.4 0.4 1.0 0.5";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 7.0;
   sizes[1]      = 17.0;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(UnderwaterSatchelExplosionSmokeEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionVelocity = 14.25;
   velocityVariance = 2.25;

   thetaMin         = 0.0;
   thetaMax         = 180.0;

   lifetimeMS       = 200;

   particles = "UnderwaterSatchelExplosionSmoke";
};


datablock ParticleData(SatchelSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 500;
   lifetimeVarianceMS   = 150;
   textureName          = "special/bigSpark";
   colors[0]     = "0.56 0.36 0.26 1.0";
   colors[1]     = "0.56 0.36 0.26 1.0";
   colors[2]     = "1.0 0.36 0.26 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.5;
   sizes[2]      = 0.75;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;

};

datablock ParticleEmitterData(SatchelSparksEmitter)
{
   ejectionPeriodMS = 1;
   periodVarianceMS = 0;
   ejectionVelocity = 40;
   velocityVariance = 20.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 180;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "SatchelSparks";
};

datablock ParticleData(UnderwaterSatchelSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 500;
   lifetimeVarianceMS   = 350;
   textureName          = "special/underwaterSpark";
   colors[0]     = "0.6 0.6 1.0 1.0";
   colors[1]     = "0.6 0.6 1.0 1.0";
   colors[2]     = "0.6 0.6 1.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.5;
   sizes[2]      = 0.75;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;

};

datablock ParticleEmitterData(UnderwaterSatchelSparksEmitter)
{
   ejectionPeriodMS = 2;
   periodVarianceMS = 0;
   ejectionVelocity = 30;
   velocityVariance = 5.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 70;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "UnderwaterSatchelSparks";
};


//---------------------------------------------------------------------------
// Explosion
//---------------------------------------------------------------------------

datablock ExplosionData(SatchelSubExplosion)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   explosionScale = "0.5 0.5 0.5";

   debris = SatchelDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 80;
   debrisNum = 8;
   debrisVelocity = 60.0;
   debrisVelocityVariance = 15.0;

   lifetimeMS = 1000;
   delayMS = 0;

   emitter[0] = SatchelExplosionSmokeEmitter;
   emitter[1] = SatchelSparksEmitter;

   offset = 0.0;

   playSpeed = 1.5;

   sizes[0] = "1.5 1.5 1.5";
   sizes[1] = "3.0 3.0 3.0";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(SatchelSubExplosion2)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   explosionScale = "0.7 0.7 0.7";

   debris = SatchelDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 170;
   debrisNum = 8;
   debrisVelocity = 60.0;
   debrisVelocityVariance = 15.0;

   lifetimeMS = 1000;
   delayMS = 50;

   emitter[0] = SatchelExplosionSmokeEmitter;
   emitter[1] = SatchelSparksEmitter;

   offset = 9.0;

   playSpeed = 1.5;

   sizes[0] = "1.5 1.5 1.5";
   sizes[1] = "1.5 1.5 1.5";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(SatchelSubExplosion3)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   explosionScale = "1.0 1.0 1.0";

   debris = SatchelDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 170;
   debrisNum = 8;
   debrisVelocity = 60.0;
   debrisVelocityVariance = 15.0;

   lifetimeMS = 2000;
   delayMS = 100;

   emitter[0] = SatchelExplosionSmokeEmitter;
   emitter[1] = SatchelSparksEmitter;

   offset = 9.0;

   playSpeed = 2.5;

   sizes[0] = "1.0 1.0 1.0";
   sizes[1] = "1.0 1.0 1.0";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(SatchelMainExplosion)
{
   soundProfile = SatchelChargePreExplosionSound;

   subExplosion[0] = SatchelSubExplosion;
   subExplosion[1] = SatchelSubExplosion2;
   subExplosion[2] = SatchelSubExplosion3;
};

//---------------------------------------------------------------------------
// Underwater Explosion
//---------------------------------------------------------------------------

datablock ExplosionData(UnderwaterSatchelSubExplosion)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   explosionScale = "0.5 0.5 0.5";


   lifetimeMS = 1000;
   delayMS = 0;

   emitter[0] = UnderwaterSatchelExplosionSmokeEmitter;
   emitter[1] = UnderwaterSatchelSparksEmitter;
   emitter[2] = SatchelBubbleEmitter;

   offset = 0.0;

   playSpeed = 0.75;

   sizes[0] = "1.5 1.5 1.5";
   sizes[1] = "2.5 2.5 2.5";
   sizes[2] = "2.0 2.0 2.0";
   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;
};

datablock ExplosionData(UnderwaterSatchelSubExplosion2)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   explosionScale = "0.7 0.7 0.7";


   lifetimeMS = 1000;
   delayMS = 50;

   emitter[0] = UnderwaterSatchelExplosionSmokeEmitter;
   emitter[1] = UnderwaterSatchelSparksEmitter;
   emitter[2] = SatchelBubbleEmitter;

   offset = 9.0;

   playSpeed = 0.75;

   sizes[0] = "1.5 1.5 1.5";
   sizes[1] = "1.0 1.0 1.0";
   sizes[2] = "0.75 0.75 0.75";
   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;
};

datablock ExplosionData(UnderwaterSatchelSubExplosion3)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   explosionScale = "1.0 1.0 1.0";


   lifetimeMS = 2000;
   delayMS = 100;

   emitter[0] = UnderwaterSatchelExplosionSmokeEmitter;
   emitter[1] = UnderwaterSatchelSparksEmitter;
   emitter[2] = SatchelBubbleEmitter;

   offset = 9.0;

   playSpeed = 1.25;

   sizes[0] = "1.0 1.0 1.0";
   sizes[1] = "1.0 1.0 1.0";
   sizes[2] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;
};

datablock ExplosionData(UnderwaterSatchelMainExplosion)
{
   soundProfile = UnderwaterSatchelChargeExplosionSound;

   subExplosion[0] = UnderwaterSatchelSubExplosion;
   subExplosion[1] = UnderwaterSatchelSubExplosion2;
   subExplosion[2] = UnderwaterSatchelSubExplosion3;
};

