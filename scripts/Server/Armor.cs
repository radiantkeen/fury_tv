// Armor-specific variables
$InvincibleTime = 6;

//Damage Rate for entering Liquid
$DamageLava       = 0.0325;
$DamageHotLava    = 0.0325;
$DamageCrustyLava = 0.0325;

//----------------------------------------------------------------------------

function Armor::onAdd(%data, %obj)
{
   Parent::onAdd(%data, %obj);

   %obj.setRechargeRate(%data.rechargeRate); // * %obj.rechargeRateFactor);
   %obj.setRepairRate(0);
//   %obj.setSelfPowered(); // keen: necessary? not really sure, part of base player code
   %obj.selfPower = true; // ^ if the above is needed, this should really be the only necessary part from it

   %obj.mountVehicle = true;
   %obj.tickCount = 0;
   %obj.isDead = false;
   %obj.isShielded = false;
   %obj.deathTriggered = false;
   %obj.scriptKilled = false;
   %obj.isDestroyer = false;
   %obj.destroyerLaunchSlot = 0;
   %obj.destroyerDualLaunch = false;
   %obj.dualFireOverride = false;
   
   %data.initTick(%obj);
}

// Called when new armor is bought
function Player::onNewArmor(%obj, %dbOld, %dbNew)
{
    if(%dbNew.isDestroyer == true)
    {
        // Same armor, bail
        if(%dbOld.isDestroyer == true)
            return;

        %obj.isDestroyer = true;
        
        if(%obj.client.race $= "Bioderm")
        {
            %obj.mountImage(DualLauncherDImage, $ShoulderSlot);
            %obj.mountImage(DualLauncher2DImage, $ShoulderSlot2);
        }
        else
        {
            %obj.mountImage(DualLauncherImage, $ShoulderSlot);
            %obj.mountImage(DualLauncher2Image, $ShoulderSlot2);
        }
    }
    else if(%dbOld.isDestroyer == true)
    {
        %obj.unmountImage($ShoulderSlot);
        %obj.unmountImage($ShoulderSlot2);

        %obj.isDestroyer = false;
    }
}

$ArmorType::Light = 1;
$ArmorType::Medium = 2;
$ArmorType::Heavy = 3;

function Player::getArmorType(%obj)
{
    %wc = %obj.getDatablock().maxEnergy;
    
    if(%wc == LightMaleHumanArmor.maxEnergy)
        return $ArmorType::Light;
    else if(%wc == MediumMaleHumanArmor.maxEnergy)
        return $ArmorType::Medium;
    else if(%wc == HeavyMaleHumanArmor.maxEnergy)
        return $ArmorType::Heavy;
    else
        return $ArmorType::Light;
}

// Shield Flash time (sec)
$g_ShieldFlashTime = 0.5;

function Player::shieldFlash(%obj)
{
//    if(%obj.shieldFlash > 0)
//        cancel(%obj.shieldFlash);

    %obj.setInvincibleMode(0.333, 0.05);

//    %obj.shieldFlash = %obj.schedule($g_ShieldFlashTime * 1000, "endShieldFlash");
}

function Player::endShieldFlash(%obj)
{
    if(%obj.shieldFlash > 0)
        cancel(%obj.shieldFlash);

    %obj.shieldFlash = 0;
}

function Armor::initTick(%data, %obj)
{
    if(%obj.ticking == true)
        return;

    %obj.ticking = true;
    %obj.tickThread = 0;
    %obj.isBot = isObject(%obj.client) ? %obj.client.isAIControlled() : false;
    %data.schedule($g_TickTime, "onTick", %obj);
}

function Armor::onTick(%this, %obj)
{
    if(!isObject(%obj) || %obj.isDead)
        return false;

    // keen: soon
//    if(%obj.tickCount % 32 == 0)
//        %this.updateTeamStats(%obj);

    // Ground detection
    if($Host::GroundIsDeadly && %obj.tickCount % 16 == 0)
        %this.scanForGround(%obj);

    %obj.tickCount++;
    %obj.tickThread = %this.schedule($g_TickTime, "onTick", %obj);

    return true;
}

function Armor::stopTick(%data, %obj)
{
    if(%obj.tickThread == 0)
        return;
        
    cancel(%obj.tickThread);
    %obj.tickThread = 0;
    %obj.ticking = false;
}

$FTVMultiRaycast = true;
$GroundKillRayCount = 0;
$GroundKillRays[$GroundKillRayCount] = "0 0 -0.75";
$GroundKillRayCount++;
$GroundKillRays[$GroundKillRayCount] = "0 1 0";
$GroundKillRayCount++;
$GroundKillRays[$GroundKillRayCount] = "0 -1 0";
$GroundKillRayCount++;
$GroundKillRays[$GroundKillRayCount] = "1 0 0";
$GroundKillRayCount++;
$GroundKillRays[$GroundKillRayCount] = "-1 0 0";
$GroundKillRayCount++;

function Armor::scanForGround(%data, %obj)
{
    if(%obj.isMounted() || %obj.isShielded)
        return;

    %pos = %obj.getPosition();
    %z = getTerrainHeight(%pos);
    %terrainPos = getWords(%pos, 0, 2) SPC %z;
    %dist = vectorDist(%pos, %terrainPos);

    if(%dist < 2.0)
    {
        %count = $FTVMultiRaycast ? $GroundKillRayCount : 1;
        
        for(%i = 0; %i < %count; %i++)
        {
            %end = VectorAdd(%pos, $GroundKillRays[%i]);
            %searchResult = containerRayCast(%pos, %end, $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticShapeObjectType, %obj);

            if(%searchResult)
            {
                if(%searchResult.getType() & $TypeMasks::TerrainObjectType)
                {
                    zapObject(%obj, FXZap);
                    %obj.setInvincible(false);
                    %obj.damage(0, %obj.getPosition(), 10000, $DamageType::Ground);
                }
                
                break;
            }
        }
    }
}

function Armor::updateTeamStats(%data, %obj)
{
    messageClient(%obj.client, 'MsgSPCurrentObjective1', "", '%1: %2 | %3: %4', $teamName[1], $teamScore[1], $teamName[2], $teamScore[2]);
}

function Player::gib(%obj)
{
    if(%obj.isFragmented)
        return;

    %obj.isFragmented = true;
    %obj.isDead = true;
    %obj.blowUp();
}

function deathTrigger(%player)
{
    %player.deathTriggered = !%player.deathTriggered;
    %player.setImageTrigger(0, %player.deathTriggered);

    if(%player.deathTriggered)
        schedule(5000 + getRandom(5000), %player, "deathTrigger", %player);
}

function Player::ejectFlag(%obj)
{
   if(%obj.holdingFlag)
   {
      %flag = %obj.holdingFlag;
      %flag.setVelocity("0 0 0");
      %flag.setTransform(%obj.getWorldBoxCenter());
      %flag.setCollisionTimeout(%obj);
      %obj.throwObject(%flag);
   }
}

function Armor::onDisabled(%this, %obj, %state)
{
    // keen: just for extra reinforcing
    %obj.isDead = true;

   if($MatchStarted)
   {
      %obj.startFade( 1000, $CorpseTimeoutValue - 1000, true );
      %obj.schedule( $CorpseTimeoutValue, "delete" );
   }
   else
      %obj.schedule( 150, "delete" );
}

function Armor::onEnterLiquid(%data, %obj, %coverage, %type)
{
   switch(%type)
   {
      case 0:
         //Water
        %obj.isWet = true;
      case 1:
         //Ocean Water
        %obj.isWet = true;
      case 2:
         //River Water
        %obj.isWet = true;
      case 3:
         //Stagnant Water
        %obj.isWet = true;
      case 4:
         //Lava
         %obj.liquidDamage(%data, $DamageLava, $DamageType::Lava);
      case 5:
         //Hot Lava
         %obj.liquidDamage(%data, $DamageHotLava, $DamageType::Lava);
      case 6:
         //Crusty Lava
         %obj.liquidDamage(%data, $DamageCrustyLava, $DamageType::Lava);
      case 7:
         //Quick Sand
   }
}

function Armor::onLeaveLiquid(%data, %obj, %type)
{
   switch(%type)
   {
      case 0:
         //Water
         %obj.isWet = false;
      case 1:
         //Ocean Water
         %obj.isWet = false;
      case 2:
         //River Water
         %obj.isWet = false;
      case 3:
         //Stagnant Water
         %obj.isWet = false;
      case 4:
         //Lava
      case 5:
         //Hot Lava
      case 6:
         //Crusty Lava
      case 7:
         //Quick Sand
   }

   if(%obj.lDamageSchedule !$= "")
   {
      cancel(%obj.lDamageSchedule);
      %obj.lDamageSchedule = "";
   }
}

function Armor::onMount(%this,%obj,%vehicle,%node)
{
   if (%node == 0)
   {
      // Node 0 is the pilot's pos.
      %obj.setTransform("0 0 0 0 0 1 0");
      %obj.setActionThread(%vehicle.getDatablock().mountPose[%node],true,true);

      if(!%obj.inStation)
         %obj.lastWeapon = (%obj.getMountedImage($WeaponSlot) == 0 ) ? "" : %obj.getMountedImage($WeaponSlot).getName().item;

       %obj.unmountImage($WeaponSlot);

//      if(!%obj.client.isAIControlled())
//      {
         %obj.setControlObject(%vehicle);
         %obj.client.setObjectActiveImage(%vehicle, 2);
//      }

      //E3 respawn...

      if(%obj == %obj.lastVehicle.lastPilot && %obj.lastVehicle != %vehicle)
      {
         schedule(15000, %obj.lastVehicle,"vehicleAbandonTimeOut", %obj.lastVehicle);
          %obj.lastVehicle.lastPilot = "";
      }
      if(%vehicle.lastPilot !$= "" && %vehicle == %vehicle.lastPilot.lastVehicle)
            %vehicle.lastPilot.lastVehicle = "";

      %vehicle.abandon = false;
      %vehicle.lastPilot = %obj;
      %obj.lastVehicle = %vehicle;

      // update the vehicle's team
      if((%vehicle.getTarget() != -1) && %vehicle.getDatablock().cantTeamSwitch $= "")
      {
         setTargetSensorGroup(%vehicle.getTarget(), %obj.client.getSensorGroup());
         if( %vehicle.turretObject > 0 )
            setTargetSensorGroup(%vehicle.turretObject.getTarget(), %obj.client.getSensorGroup());
      }

      // Send a message to the client so they can decide if they want to change view or not:
      commandToClient( %obj.client, 'VehicleMount' );

   }
   else
   {
      // tailgunner/passenger positions
      if(%vehicle.getDataBlock().mountPose[%node] !$= "")
         %obj.setActionThread(%vehicle.getDatablock().mountPose[%node]);
      else
         %obj.setActionThread("root", true);
   }
   // z0dd - ZOD, 6/27/02. announce to any other passengers that you've boarded
   if(%vehicle.getDatablock().numMountPoints > 1)
   {
      %nodeName = findNodeName(%vehicle, %node); // function in vehicle.cs
      for(%i = 0; %i < %vehicle.getDatablock().numMountPoints; %i++)
      {
         if (%vehicle.getMountNodeObject(%i) > 0)
         {
            if(%vehicle.getMountNodeObject(%i).client != %obj.client)
            {
               %team = (%obj.team == %vehicle.getMountNodeObject(%i).client.team ? 'Teammate' : 'Enemy');
               messageClient( %vehicle.getMountNodeObject(%i).client, 'MsgShowPassenger', '\c2%3: \c3%1\c2 has boarded in the \c3%2\c2 position.', %obj.client.name, %nodeName, %team );
            }
            commandToClient( %vehicle.getMountNodeObject(%i).client, 'showPassenger', %node, true);
         }
      }
   }
   //make sure they don't have any packs active
//    if ( %obj.getImageState( $BackpackSlot ) $= "activate")
//       %obj.use("Backpack");
   if ( %obj.getImageTrigger( $BackpackSlot ) )
      %obj.setImageTrigger( $BackpackSlot, false );

   //AI hooks
   %obj.client.vehicleMounted = %vehicle;
   AIVehicleMounted(%vehicle);
   if(%obj.client.isAIControlled())
      %this.AIonMount(%obj, %vehicle, %node);
}

function Armor::onUnmount( %this, %obj, %vehicle, %node )
{
    %dataBlock = %vehicle.getDatablock();
    %obj.vehicleTurret = 0;
    
   if ( %node == 0 )
   {
      commandToClient( %obj.client, 'VehicleDismount' );
      commandToClient(%obj.client, 'removeReticle');

      if(%obj.inv[%obj.lastWeapon])
         %obj.use(%obj.lastWeapon);

      if(%obj.getMountedImage($WeaponSlot) == 0)
         %obj.selectWeaponSlot( 0 );

      //Inform gunner position when pilot leaves...
      //if(%vehicle.getDataBlock().showPilotInfo !$= "")
      //   if((%gunner = %vehicle.getMountNodeObject(1)) != 0)
      //      commandToClient(%gunner.client, 'PilotInfo', "PILOT EJECTED", 6, 1);
   }
   // z0dd - ZOD, 6/27/02. announce to any other passengers that you've left
   if(%dataBlock.numMountPoints > 1)
   {
      %nodeName = findNodeName(%vehicle, %node); // function in vehicle.cs
      for(%i = 0; %i < %dataBlock.numMountPoints; %i++)
      {
         if (%vehicle.getMountNodeObject(%i) > 0)
         {
            if(%vehicle.getMountNodeObject(%i).client != %obj.client)
            {
               %team = (%obj.team == %vehicle.getMountNodeObject(%i).client.team ? 'Teammate' : 'Enemy');
               messageClient( %vehicle.getMountNodeObject(%i).client, 'MsgShowPassenger', '\c2%3: \c3%1\c2 has ejected from the \c3%2\c2 position.', %obj.client.name, %nodeName, %team );
            }
            commandToClient( %vehicle.getMountNodeObject(%i).client, 'showPassenger', %node, false);
         }
      }
   }
   //AI hooks
   %obj.client.vehicleMounted = "";
   if(%obj.client.isAIControlled())
      %this.AIonUnMount(%obj, %vehicle, %node);
      
    if(%dataBlock.hidePlayerOnMount[%node] == true)
        %obj.startFade(1, 0, false);
}

function Armor::onCollision(%this,%obj,%col,%forceVehicleNode)
{
   if (%obj.getState() $= "Dead")
      return;

   %dataBlock = %col.getDataBlock();
   %className = %dataBlock.className;
   %client = %obj.client;
   // player collided with a vehicle
   %node = -1;
   %bIsAI = %obj.client.isAIControlled();
   
//   error("col:" SPC %obj SPC %obj.getClassName() SPC %col SPC %col.getClassName());
   
   if(%forceVehicleNode !$= "" || %col.isVehicle() && %obj.mountVehicle && %obj.getState() $= "Move" && %col.mountable && !%obj.inStation && %col.getDamageState() !$= "Destroyed")
   {
      //if the player is an AI, he should snap to the mount points in node order,
      //to ensure they mount the turret before the passenger seat, regardless of where they collide...
      if (%bIsAI)
      {
         %transform = %col.getTransform();

         //either the AI is *required* to pilot, or they'll pick the first available passenger seat
//         if (%client.pilotVehicle)
//         {
            //make sure the bot is in light armor
//            if(%this.canPilot == true)
//            {
               //make sure the pilot seat is empty
               if (!%col.getMountNodeObject(0))
                  %node = 0;
//            }
//         }
         else
            %node = findAIEmptySeat(%col, %obj);
      }
      else
         %node = findEmptySeat(%col, %obj, %forceVehicleNode);

      //now mount the player in the vehicle
      if(%node >= 0)
      {
         // players can't be pilots, bombardiers or turreteers if they have
         // "large" packs -- stations, turrets, turret barrels
         if(hasLargePack(%obj)) {
            // check to see if attempting to enter a "sitting" node
            if(nodeIsSitting(%datablock, %node)) {
               // send the player a message -- can't sit here with large pack
               if(!%obj.noSitMessage)
               {
                  %obj.noSitMessage = true;
                  %obj.schedule(2000, "resetSitMessage");
                  messageClient(%obj.client, 'MsgCantSitHere', '\c2Pack too large, can\'t occupy this seat.~wfx/misc/misc.error.wav');
               }
               return;
            }
         }
         if(%col.noEnemyControl && %obj.team != %col.team)
            return;

         commandToClient(%obj.client,'SetDefaultVehicleKeys', true);
         //If pilot or passenger then bind a few extra keys
         if(%node == 0)
            commandToClient(%obj.client,'SetPilotVehicleKeys', true);
         else
            commandToClient(%obj.client,'SetPassengerVehicleKeys', true);

         if(!%obj.inStation)
            %col.lastWeapon = ( %col.getMountedImage($WeaponSlot) == 0 ) ? "" : %col.getMountedImage($WeaponSlot).getName().item;
         else
            %col.lastWeapon = %obj.lastWeapon;

         %col.mountObject(%obj,%node);
         %col.playAudio(0, MountVehicleSound);
         %obj.mVehicle = %col;

			// if player is repairing something, stop it
			if(%obj.repairing)
				stopRepairing(%obj);

         //this will setup the huds as well...
         %dataBlock.playerMounted(%col,%obj, %node);
         
         if(%dataBlock.hidePlayerOnMount[%node] == true)
            %obj.startFade(1, 0, true);

        // keen: debug sitting in seat message for now
         if($Host::DebugMode && %node > 0)
             messageClient(%obj.client, 'MsgDebugSitAt', '\c2Sitting in seat %1 - vehicle %2', %node, %col);
      }
   }
   else if (%className $= "Armor") {
      // player has collided with another player
      if(%col.getState() $= "Dead") {
         %gotSomething = false;
         // it's corpse-looting time!
         // weapons -- don't pick up more than you are allowed to carry!
         for(%i = 0; ( %obj.weaponCount < %obj.getDatablock().maxWeapons ) && $InvWeapon[%i] !$= ""; %i++)
         {
            %weap = $NameToInv[$InvWeapon[%i]];
            if ( %col.hasInventory( %weap ) )
            {
               if ( %obj.incInventory(%weap, 1) > 0 )
               {
                  %col.decInventory(%weap, 1);
                  %gotSomething = true;
                  messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', %weap.pickUpName);
               }
            }
         }
         // targeting laser:
         if ( %col.hasInventory( "TargetingLaser" ) )
         {
            if ( %obj.incInventory( "TargetingLaser", 1 ) > 0 )
            {
               %col.decInventory( "TargetingLaser", 1 );
               %gotSomething = true;
               messageClient( %obj.client, 'MsgItemPickup', '\c0You picked up a targeting laser.' );
            }
         }
         // ammo
         for(%j = 0; $ammoType[%j] !$= ""; %j++)
         {
            %ammoAmt = %col.inv[$ammoType[%j]];
            if(%ammoAmt)
            {
               // incInventory returns the amount of stuff successfully grabbed
               %grabAmt = %obj.incInventory($ammoType[%j], %ammoAmt);
               if(%grabAmt > 0)
               {
                  %col.decInventory($ammoType[%j], %grabAmt);
                  %gotSomething = true;
                  messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', $ammoType[%j].pickUpName);
                  %obj.client.setWeaponsHudAmmo($ammoType[%j], %obj.getInventory($ammoType[%j]));
               }
            }
         }
         // figure out what type, if any, grenades the (live) player has
         %playerGrenType = "None";
         for(%x = 0; $InvGrenade[%x] !$= ""; %x++) {
            %gren = $NameToInv[$InvGrenade[%x]];
            %playerGrenAmt = %obj.inv[%gren];
            if(%playerGrenAmt > 0)
            {
               %playerGrenType = %gren;
               break;
            }
         }
         // grenades
         for(%k = 0; $InvGrenade[%k] !$= ""; %k++)
         {
            %gren = $NameToInv[$InvGrenade[%k]];
            %corpseGrenAmt = %col.inv[%gren];
            // does the corpse hold any of this grenade type?
            if(%corpseGrenAmt)
            {
               // can the player pick up this grenade type?
               if((%playerGrenType $= "None") || (%playerGrenType $= %gren))
               {
                  %taken = %obj.incInventory(%gren, %corpseGrenAmt);
                  if(%taken > 0)
                  {
                     %col.decInventory(%gren, %taken);
                     %gotSomething = true;
                     messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', %gren.pickUpName);
                     %obj.client.setInventoryHudAmount(%gren, %obj.getInventory(%gren));
                  }
               }
               break;
            }
         }
         // figure out what type, if any, mines the (live) player has
         %playerMineType = "None";
         for(%y = 0; $InvMine[%y] !$= ""; %y++)
         {
            %mType = $NameToInv[$InvMine[%y]];
            %playerMineAmt = %obj.inv[%mType];
            if(%playerMineAmt > 0)
            {
               %playerMineType = %mType;
               break;
            }
         }
         // mines
         for(%l = 0; $InvMine[%l] !$= ""; %l++)
         {
            %mine = $NameToInv[$InvMine[%l]];
            %mineAmt = %col.inv[%mine];
            if(%mineAmt) {
               if((%playerMineType $= "None") || (%playerMineType $= %mine))
               {
                  %grabbed = %obj.incInventory(%mine, %mineAmt);
                  if(%grabbed > 0)
                  {
                     %col.decInventory(%mine, %grabbed);
                     %gotSomething = true;
                     messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', %mine.pickUpName);
                     %obj.client.setInventoryHudAmount(%mine, %obj.getInventory(%mine));
                  }
               }
               break;
            }
         }
         // beacons
         %beacAmt = %col.inv[Beacon];
         if(%beacAmt)
         {
            %bTaken = %obj.incInventory(Beacon, %beacAmt);
            if(%bTaken > 0)
            {
               %col.decInventory(Beacon, %bTaken);
               %gotSomething = true;
               messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', Beacon.pickUpName);
               %obj.client.setInventoryHudAmount(Beacon, %obj.getInventory(Beacon));
            }
         }
         // repair kit
         %rkAmt = %col.inv[RepairKit];
         if(%rkAmt)
         {
            %rkTaken = %obj.incInventory(RepairKit, %rkAmt);
            if(%rkTaken > 0)
            {
               %col.decInventory(RepairKit, %rkTaken);
               %gotSomething = true;
               messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', RepairKit.pickUpName);
               %obj.client.setInventoryHudAmount(RepairKit, %obj.getInventory(RepairKit));
            }
         }
      }
      if(%gotSomething)
         %col.playAudio(0, CorpseLootingSound);
   }
}

// todo: dynamic method for vehicle to define sitting nodes
function nodeIsSitting(%vehDBlock, %node)
{
   // pilot == always a "sitting" node
   if(%node == 0)
      return true;
   else {
      switch$ (%vehDBlock.getName())
      {
         // note: for assault tank -- both nodes are sitting
         // for any single-user vehicle -- pilot node is sitting
         case "BomberFlyer":
            // bombardier == sitting; tailgunner == not sitting
            if(%node == 1)
               return true;
            else
               return false;
         case "HAPCFlyer":
            // only the pilot node is sitting
            return false;
         default:
            return true;
      }
   }
}

function Armor::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType, %momVec, %mineSC, %srcProj)
{
   if(%targetObject.invincible || %targetObject.getState() $= "Dead") // magic constant time!
      return;

   //----------------------------------------------------------------
   // z0dd - ZOD, 6/09/02. Check to see if this vehicle is destroyed,
   // if it is do no damage. Fixes vehicle ghosting bug. We do not
   // check for isObject here, destroyed objects fail it even though
   // they exist as objects, go figure.
   if(%damageType == $DamageType::Impact)
      if(%sourceObject.getDamageState() $= "Destroyed")
         return;

   if(%targetObject.lastHitFlags & $Projectile::RepairProjectile)
   {
       %targetObject.lastHitFlags = 0;
       %targetObject.setDamageLevel(%targetObject.getDamageLevel() - %amount);
       zapObject(%targetObject, "FXRedShift");
       return;
   }

   if(%targetObject.isMounted() && !%targetObject.scriptKilled && ((%targetObject.lastHitFlags & $Projectile::Piercing) == 0))
   {
      %mount = %targetObject.getObjectMount();
      if(%mount.team == %targetObject.team)
      {
         %found = -1;
         for (%i = 0; %i < %mount.getDataBlock().numMountPoints; %i++)
         {
            if (%mount.getMountNodeObject(%i) == %targetObject)
            {
               %found = %i;
               break;
            }
         }

         if (%found != -1)
         {
            if (%mount.getDataBlock().isProtectedMountPoint[%found])
            {
               %mount.getDataBlock().damageObject(%mount, %sourceObject, %position, %amount, %damageType);
               return;
            }
         }
      }
   }

   %targetClient = %targetObject.getOwnerClient();
   if(isObject(%mineSC))
      %sourceClient = %mineSC;
   else
      %sourceClient = isObject(%sourceObject) ? %sourceObject.getOwnerClient() : 0;

   %targetTeam = %targetClient.team;

   //if the source object is a player object, player's don't have sensor groups
   // if it's a turret, get the sensor group of the target
   // if its a vehicle (of any type) use the sensor group
   if (%sourceClient)
      %sourceTeam = %sourceClient.getSensorGroup();
   else if(%damageType == $DamageType::Suicide)
      %sourceTeam = 0;
   //--------------------------------------------------------------------------------------------------------------------
   // z0dd - ZOD, 4/8/02. Check to see if this turret has a valid owner, if not clear the variable.
   else if(isObject(%sourceObject) && %sourceObject.getClassName() $= "Turret")
   {
      %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
      if(%sourceObject.owner !$="" && (%sourceObject.owner.team != %sourceObject.team || !isObject(%sourceObject.owner)))
      {
         %sourceObject.owner = "";
      }
   }
   //--------------------------------------------------------------------------------------------------------------------
   else if( isObject(%sourceObject) && %sourceObject.isVehicle())
      %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
   else
   {
      if (isObject(%sourceObject) && %sourceObject.getTarget() >= 0 )
      {
         %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
      }
      else
      {
         %sourceTeam = -1;
      }
   }

   // if teamdamage is off, and both parties are on the same team
   // (but are not the same person), apply no damage
   if(!$teamDamage && (%targetClient != %sourceClient) && (%targetTeam == %sourceTeam))
      return;

    // ShapeBaseExtension handles shields for armors
   if(%targetObject.isShielded && (%targetObject.lastHitFlags & $Projectile::IgnoreShields) == 0)
   {
      if(%targetObject.shieldSource > 0)
          %amount = %data.onShieldDamage(%targetObject.shieldSource, %position, %amount, %damageType);
      else
          %amount = %data.onShieldDamage(%targetObject, %position, %amount, %damageType);
   }

   if(%targetObject.lastHitFlags & $Projectile::Nullifier)
       %amount = 0;

   if(%targetObject.lastHitFlags & $Projectile::Energy)
       %amount *= 0.75;

   %amount *= %targetObject.damageReduceFactor;

   if(%targetObject.lastHitFlags & $Projectile::ArmorOnly)
       %amount *= 2;
       
   if(%amount == 0)
       return;

//   if(%targetObject.ammoInventory == true && %targetObject.hasAmmoPack == true)
//   {
//      if(getRandom() > 0.96 )
//          createRemoteProjectile("LinearFlareProjectile", "MagIonOLDeathCharge", vectorRand(), %obj.getWorldBoxCenter(), 0, %obj);
//   }

        // Set the damage flash
        if(%amount > 3)
            %flash = %targetObject.getDamageFlash() + (%amount * 0.01);
        else
            %flash = %amount * 0.1;

        if(%flash > 0.75)
           %flash = 0.75;

        %previousDamage = %targetObject.getDamagePercent();
        %targetObject.setDamageFlash(%flash);

        %targetObject.lastDamageTime = %time;
        %targetObject.applyDamage(%amount);

        Game.onClientDamaged(%targetClient, %sourceClient, %damageType, %sourceObject);

        if(%damagingClient != %targetClient.lastDamagedBy)
        {
            %targetClient.assistDamagedBy = %targetClient.lastDamagedBy;
            %targetClient.lastDamagedBy = %damagingClient;
        }

        %targetClient.lastDamaged = getSimTime();
        %targetClient.lastDamageType = %damageType;
    
   //now call the "onKilled" function if the client was... you know...
   if(%targetObject.getState() $= "Dead")
   {
      // prevent multi-looping death code (prevents straight-to-observer bugs)
      if(%targetObject.didDeathSequence == true)
        return;

      %targetObject.didDeathSequence = true;

      // where did this guy get it?
      %damLoc = %targetObject.getDamageLocation(%position);

      // should this guy be blown apart?
      // keen: let projectile determine that
      if(%targetObject.lastHitFlags & $Projectile::PlayerFragment)
      {
        %targetObject.setMomentumVector(%momVec);
        %targetObject.gib();
      }
      else if(%damageType == $DamageType::VehicleSpawn)
      {
         %targetObject.setMomentumVector("0 0 1");
         %targetObject.gib();
      }
      else if(!%targetObject.isFragmented && getRandom() > 0.475)
          deathTrigger(%targetObject);

      // If we were killed, max out the flash
      %targetObject.setDamageFlash(0.75);

      Game.onClientKilled(%targetClient, %sourceClient, %damageType, %sourceObject, %damLoc);
   }
//   else if ( %amount > 0.1 )
//   {
//      if( %targetObject.station $= "" && %targetObject.isCloaked() )
//      {
//         %targetObject.setCloaked( false );
//         %targetObject.reCloak = %targetObject.schedule( 500, "setCloaked", true );
//      }

    if(getRandom() > 0.85)
        playPain(%targetObject);

    // keen: reset so we don't get other damage bleedover
//    %targetObject.lastHitFlags = 0;
//   }
}

// reserved for future deadly ground changes
function Armor::onImpact(%data, %playerObject, %collidedObject, %vec, %vecLen)
{
   %data.damageObject(%playerObject, 0, VectorAdd(%playerObject.getPosition(),%vec),
      %vecLen * %data.speedDamageScale, $DamageType::Ground);
}

function Player::isPilot(%this)
{
    if((%vehicle = %this.getObjectMount()))
        return %vehicle.getMountNodeObject(0) == %this;
        
    return false;
}

function Player::isWeaponOperator(%this)
{
    if((%turret = %this.getControlObject()))
        return %turret.isTurret();
        
    return false;
}

function Player::use( %this,%data )
{
   // If player is in a station then he can't use any items
   if(%this.station !$= "")
      return false;

   // Convert the word "Backpack" to whatever is in the backpack slot.
   if(%data $= "Backpack")
   {
      if(%this.inStation)
         return false;

      // keen: assume we only mount vehicles
      if((%vehicle = %this.getObjectMount()))
      {
        if(%this.isPilot())
            %vehicle.getDatablock().triggerPack(%vehicle, %this);
        else if(%this.isWeaponOperator())
            %vehicle.getDatablock().turretTriggerPack(%vehicle, %this, %this.getControlObject());
          
        return;
      }

      %image = %this.getMountedImage($BackpackSlot);

      if(%image)
         %data = %image.item;
   }

   // Can't use some items when piloting or your a weapon operator
   if ( %this.isPilot() || %this.isWeaponOperator() )
      if ( %data.getName() !$= "RepairKit" )
         return false;

   return ShapeBase::use( %this, %data );
}

function Armor::onTrigger(%data, %player, %triggerNum, %val)
{
    if (%triggerNum == 4)
    {
      if(%val && isObject((%veh = %player.getObjectMount())) && %veh.isVehicle() && %veh.getMountNodeObject(0) == %player)
      {
          %veh.getDatablock().triggerGrenade(%veh, %player);
          return;
      }
      else if (%val == 1) // Throw grenade
      {
         %player.grenTimer = 1;
      }
      else
      {
         if (%player.grenTimer == 0)
         {
            // Bad throw for some reason
         }
         else
         {
            for(%i = 0; %i < $GrenadeCount; %i++)
            {
                if(%player.getInventory($NameToInv[$InvGrenade[%i]]))
                {
                    %player.use($NameToInv[$InvGrenade[%i]]);
                    break;
                }
            }
//            %player.use(Grenade);
            %player.grenTimer = 0;
         }
      }
   }
   else if (%triggerNum == 5)
   {
      %veh = %player.getObjectMount();
      if(isObject(%veh) && %veh.isVehicle())
      {
          %veh.getDatablock().triggerMine(%veh, %player, %veh.getObjectSlot(%player), %val);
          return;
      }

      // Throw mine
      if (%val == 1)
      {
         %player.mineTimer = 1;
      }
      else
      {
         if (%player.mineTimer == 0)
         {
            // Bad throw for some reason
         }
         else
         {
            for(%i = 0; %i < $MineCount; %i++)
            {
                if(%player.getInventory($NameToInv[$InvMine[%i]]))
                {
                    %player.use($NameToInv[$InvMine[%i]]);
                    break;
                }
            }

            %player.mineTimer = 0;
         }
      }
   }
   else if (%triggerNum == 3)
   {
      // val = 1 when jet key (LMB) first pressed down
      // val = 0 when jet key released
      // MES - do we need this at all any more?
      %player.isJetting = %val;
   }
}

function Armor::applyConcussion( %this, %dist, %radius, %sourceObject, %targetObject )
{
   %percentage = 1 - ( %dist / %radius );
   %random = getRandom();

   if( %sourceObject == %targetObject )
   {
      %flagChance = 1.0;
      %itemChance = 1.0;
   }
   else
   {
      %flagChance = 0.7;
      %itemChance = 0.7;
   }

   %probabilityFlag = %flagChance * %percentage;
   %probabilityItem = %itemChance * %percentage;

   if( %random <= %probabilityFlag )
      Game.applyConcussion( %targetObject );

    // no more throwing items from conc while mounted
    if(!%targetObject.isMounted())
    {
       if(%random <= %probabilityItem)
       {
            %targetObject.throwPack();
            %targetObject.throwWeapon();
       }
    }
}

function serverCmdSelectWeaponSlot(%client, %data)
{
    // keen: context-based - switching back for now until an option is built in
    %pl = %client.player;
    
    if((%mount = %pl.getObjectMount()) > 0)
        %mount.getDatablock().onTriggerKeys(%mount, %pl, %data);
    else
        %client.getControlObject().selectWeaponSlot( %data );
//        %pl.getDatablock().onTriggerKeys(%pl, %data);
}

function Armor::onTriggerKeys(%data, %obj, %slot)
{
    if(%slot > %data.armorSpecialCount)
        return;

    %obj.unmountImage(0);
    %obj.mountImage(%data.armorSpecial[%slot], 0);

    commandToClient(%obj.client, 'setRepairReticle');
}
