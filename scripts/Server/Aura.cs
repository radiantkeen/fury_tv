// Aura system

$AuraType::Instance = 0;
$AuraType::Persistent = 1;

function Aura::__construct(%this)
{
    Aura.Version = 1.0;
    Aura.auraCount = 0;
}

function Aura::__destruct(%this)
{
   // Thou shalt not spam
}

if(!isObject(Aura))
   System.addClass(Aura);

// create a list so we can go through all effects if need be
function Aura::registerAura(%this, %aura, %type)
{
    if(Aura.registeredAura[%aura] == true)
        return;

    Aura.auras[Aura.auraCount] = %aura;
    Aura.auraType[%aura] = %type;
    Aura.registeredAura[%aura] = true;
    Aura.auraCount++;
}

function Aura::create(%this, %auraClass)
{
    if(Aura.registeredAura[%auraClass] != true)
    {
        error("Aura::createAura - Class not found:" SPC %auraClass);
        return 0;
    }

    %aura = new ScriptObject("Aura"@getSimTime())
    {
        class = %auraClass;
        superClass = Aura;
    };
    
    MissionCleanup.add(%aura);

    %aura.type = Aura.auraType[%auraClass];
    %aura.aoe = %aura.type == $AuraType::Instance ? 0 : createRandomSimSet();
    %aura.signature = %auraClass;
    %aura.state = false;
    %aura.detectMask = 0;
    %aura.blockMask = 0;
    %aura.radius = 0;
    %aura.position = "0 0 0";
    %aura.source = 0;
    %aura.tickCount = 0;
    %aura.noPersistentTick = false;
    %aura.setTickRate($g_TickTime);
    
    %aura.init();
    
    return %aura;
}

function Aura::destroy(%this)
{
    %this.stop();
    %this.schedule(%this.tickRate * 1.5, "delete");
}

function Aura::start(%this)
{
    %this.state = true;
    %this.schedule(%this.tickRate, "onTick");
}

function Aura::stop(%this)
{
    %this.state = false;
}

function Aura::setTickRate(%this, %time)
{
    %this.tickRate = %time;
    %this.tickDelta = %time / 1000;
}

function Aura::getPosition(%this)
{
    return %this.source ? %this.source.position : %this.position;
}

function Aura::setPosition(%this, %pos)
{
    %this.position = %pos;
    %this.source = 0;
}

function Aura::attachToObject(%this, %obj)
{
    %this.source = isObject(%obj) ? %obj : 0;
}

function Aura::validateAuraTick(%this)
{
    return true;
}

function Aura::removeFromAura(%this, %obj)
{
    if(%obj.inAura[%this.signature] == true)
    {
        %obj.inAura[%this.signature] = false;
        %obj.inAuraTimeout[%this.signature] = getSimTime() + 256;
        
        %this.aoe.remove(%obj);
    }
}

function Aura::onTick(%this)
{
    if(%this.state)
    {
        if(!%this.validateAuraTick())
        {
            %this.tickCount++;
            %this.schedule(%this.tickRate, "onTick");
            
            return;
        }
        
        %pos = %this.getPosition();
        %time = getSimTime();
        
        if(%this.type == $AuraType::Instance)
        {
            InitContainerRadiusSearch(%pos, %this.radius, %this.detectMask);

            while((%found = containerSearchNext()) != 0)
            {
                if(%this.blockMask)
                {
                    %coverage = calcExplosionCoverage(%pos, %found, %this.blockMask);

                    if(%coverage == 0)
                        continue;
                }

                %this.forEachInAura(%found);
             }
        }
        else if(%this.type == $AuraType::Persistent)
        {
            for(%i = 0; %i < %this.aoe.getCount(); %i++)
            {
                %obj = %this.aoe.getObject(%i);

                // Optimization suggested by DarkDragonDX
                if(%obj.inAura[%this.signature] != true)
                    continue;

                if(vectorDist(%pos, %obj.position) > %this.radius)
                {
                    %obj.inAura[%this.signature] = false;
                    %this.aoe.schedule(1, "remove", %obj);
                    %this.onLeave(%obj);
                }
                else if(%this.blockMask)
                {
                    %coverage = calcExplosionCoverage(%pos, %obj, %this.blockMask);

                    if(%coverage == 0)
                    {
                        %obj.inAura[%this.signature] = false;
                        %this.aoe.schedule(1, "remove", %obj);
                        %this.onLeave(%obj);
                    }
                }
            }

            InitContainerRadiusSearch(%pos, %this.radius, %this.detectMask);

            while((%found = containerSearchNext()) != 0)
            {
                if(%this.aoe.isMember(%found))
                    continue;
                    
                if(%found.inAuraTimeout[%this.signature] > %time)
                    continue;

                if(%this.blockMask)
                {
                    %coverage = calcExplosionCoverage(%pos, %found, %this.blockMask);

                    if(%coverage == 0)
                        continue;
                }

                %found.inAura[%this.signature] = true;
                %found.inAuraTimeout[%this.signature] = %time + 256;
                
                %this.aoe.add(%found);
                %this.onEnter(%found);
            }

            if(!%this.noPersistentTick)
                for(%i = 0; %i < %this.aoe.getCount(); %i++)
                {
                    %thing = %this.aoe.getObject(%i);

                    %this.forEachInAura(%thing);
                }
        }

        %this.tickCount++;
        %this.schedule(%this.tickRate, "onTick");
    }
    else
    {
        %this.tickCount = 0;

        if(%this.aoe)
        {
            for(%i = 0; %i < %this.aoe.getCount(); %i++)
            {
                %obj = %this.aoe.getObject(%i);

                %obj.inAura[%this.signature] = false;
                %this.aoe.schedule(1, "remove", %obj);
                %this.onLeave(%obj);
            }
        }
    }
}

// Virtual functions

function Aura::init(%this)
{

}

function Aura::onEnter(%this, %obj)
{

}

function Aura::onLeave(%this, %obj)
{

}

function Aura::forEachInAura(%this, %obj)
{

}

execDir("Auras");
