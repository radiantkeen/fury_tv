// Global Sound Effects library

datablock AudioProfile(MILSwitchSound)
{
   filename    = "fx/powered/turret_missile_activate.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(MILFireSound)
{
   filename    = "fx/powered/turret_missile_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(FFReflectedProjectile)
{
   filename    = "fx/misc/repulse.wav";
   description = AudioDefault3d;
   preload = true;
};

// Chaingun sound library
$BulletImpactDrySoundCount = 0;
$BulletImpactWetSoundCount = 0;

// Wet
datablock AudioProfile(ChaingunWaterImpact)
{
   filename    = "fx/weapons/cg_water1.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactWetSound[$BulletImpactWetSoundCount] = "ChaingunWaterImpact";
$BulletImpactWetSoundCount++;

datablock AudioProfile(ChaingunWaterImpact2)
{
   filename    = "fx/weapons/cg_water2.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactWetSound[$BulletImpactWetSoundCount] = "ChaingunWaterImpact2";
$BulletImpactWetSoundCount++;

datablock AudioProfile(ChaingunWaterImpact3)
{
   filename    = "fx/weapons/cg_water3.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactWetSound[$BulletImpactWetSoundCount] = "ChaingunWaterImpact3";
$BulletImpactWetSoundCount++;

// Dry
datablock AudioProfile(ChaingunImpact)
{
   filename    = "fx/weapons/chaingun_impact.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactDrySound[$BulletImpactDrySoundCount] = "ChaingunImpact";
$BulletImpactDrySoundCount++;

datablock AudioProfile(ChaingunImpact1)
{
   filename    = "fx/weapons/cg_hard1.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactDrySound[$BulletImpactDrySoundCount] = "ChaingunImpact1";
$BulletImpactDrySoundCount++;

datablock AudioProfile(ChaingunImpact2)
{
   filename    = "fx/weapons/cg_hard2.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactDrySound[$BulletImpactDrySoundCount] = "ChaingunImpact2";
$BulletImpactDrySoundCount++;

datablock AudioProfile(ChaingunImpact3)
{
   filename    = "fx/weapons/cg_metal1.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactDrySound[$BulletImpactDrySoundCount] = "ChaingunImpact3";
$BulletImpactDrySoundCount++;

datablock AudioProfile(ChaingunImpact4)
{
   filename    = "fx/weapons/cg_metal2.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactDrySound[$BulletImpactDrySoundCount] = "ChaingunImpact4";
$BulletImpactDrySoundCount++;

datablock AudioProfile(ChaingunImpact5)
{
   filename    = "fx/weapons/cg_metal3.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactDrySound[$BulletImpactDrySoundCount] = "ChaingunImpact5";
$BulletImpactDrySoundCount++;

datablock AudioProfile(ChaingunImpact6)
{
   filename    = "fx/weapons/cg_metal4.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactDrySound[$BulletImpactDrySoundCount] = "ChaingunImpact6";
$BulletImpactDrySoundCount++;

datablock AudioProfile(ChaingunImpact7)
{
   filename    = "fx/misc/Ricoche1.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactDrySound[$BulletImpactDrySoundCount] = "ChaingunImpact7";
$BulletImpactDrySoundCount++;

datablock AudioProfile(ChaingunImpact8)
{
   filename    = "fx/misc/Ricoche2.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactDrySound[$BulletImpactDrySoundCount] = "ChaingunImpact8";
$BulletImpactDrySoundCount++;

datablock AudioProfile(ChaingunImpact9)
{
   filename    = "fx/misc/Ricoche3.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactDrySound[$BulletImpactDrySoundCount] = "ChaingunImpact9";
$BulletImpactDrySoundCount++;

datablock AudioProfile(ChaingunImpact10)
{
   filename    = "fx/misc/bullet_ricochet1.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactDrySound[$BulletImpactDrySoundCount] = "ChaingunImpact10";
$BulletImpactDrySoundCount++;

datablock AudioProfile(ChaingunImpact11)
{
   filename    = "fx/misc/bullet_ricochet2.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactDrySound[$BulletImpactDrySoundCount] = "ChaingunImpact11";
$BulletImpactDrySoundCount++;

function playRandomChaingunSound(%pos)
{
    InitContainerRadiusSearch(%pos, 0.1, $TypeMasks::WaterObjectType);

    %snd = ContainerSearchNext() ? $BulletImpactWetSound[getRandom($BulletImpactWetSoundCount - 1)] : $BulletImpactDrySound[getRandom($BulletImpactDrySoundCount - 1)];
    
    serverPlay3D(%snd, %pos);
}
