// Item Utils: Registration factory
// Consolidate changes here

// Found and overriden in item.cs - load after that for permanent change
$WeaponSlot = 0;
$ShoulderSlot = 1;
$BackpackSlot = 2;
$FlagSlot = 3;
$ArmorWeightSlot = 3;
$WeaponSubImage1 = 4;
$WeaponSubImage2 = 5;
$WeaponSubImage3 = 6;
$WeightImageSlot = 7;
$ShoulderSlot2 = 7;

// Projectile flags
$Projectile::One                        = 1 << 0;
$Projectile::IgnoreShields              = 1 << 1;   // ex: blaster, ignores shield and damages armor
$Projectile::IgnoreReflections          = 1 << 2;   // not affected by reflectors (ex. forcefields)
$Projectile::IgnoreReflectorField       = 1 << 3;   // not affected by repulsor fields
$Projectile::PlayerFragment             = 1 << 4;   // will gib the player if killed by this projectile
$Projectile::CanHeadshot                = 1 << 5;   // if hit in the head, will use headshotMultipler to increase damage
$Projectile::CountMAs                   = 1 << 6;
$Projectile::Piercing                   = 1 << 7;
$Projectile::PlaysHitSound              = 1 << 8;
$Projectile::InternalDamage             = 1 << 9;
$Projectile::ShieldsOnly                = 1 << 10;
$Projectile::ArmorOnly                  = 1 << 11;
$Projectile::InternalHealthDead         = 1 << 12;  // Left over from MD3
$Projectile::Nullifier                  = 1 << 13;
$Projectile::PartialShieldPhase         = 1 << 14;
$Projectile::Explosive                  = 1 << 15;
$Projectile::RepairProjectile           = 1 << 16;
$Projectile::TAGProjectile              = 1 << 17;
$Projectile::Phaser                     = 1 << 18;
$Projectile::Kinetic                    = 1 << 19;
$Projectile::Energy                     = 1 << 20;
$Projectile::Corrosive                  = 1 << 21;
$Projectile::Plasma                     = 1 << 22;

//------------------------------------------------------------------------------
// Armors
$ArmorCount = 0;

// Pilot
$InvArmor[$ArmorCount] = "Scout";
$NameToInv["Scout"] = "Light";
$ArmorCount++;

// Soldier
$InvArmor[$ArmorCount] = "Assault";
$NameToInv["Assault"] = "Medium";
$ArmorCount++;

// Gunner
$InvArmor[$ArmorCount] = "Juggernaut";
$NameToInv["Juggernaut"]  = "Heavy";
$ArmorCount++;

//------------------------------------------------------------------------------
// Weapons
$WeaponCount = 0;

$InvWeapon[$WeaponCount] = "Blaster";
$NameToInv["Blaster"] = "Blaster";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_blaster";
$WeaponsHudData[$WeaponCount, itemDataName] = "Blaster";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_blaster";
$WeaponsHudData[$WeaponCount, visible] = "true";
$WeaponsListID["Blaster"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Directed energy rifle, dangerous and precise";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 30 | Speed: 300 | Spread: 2 | Energy: 5";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Plasma Rifle";
$NameToInv["Plasma Rifle"] = "Plasma";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_plasma";
$WeaponsHudData[$WeaponCount, itemDataName] = "Plasma";
$WeaponsHudData[$WeaponCount, ammoDataName] = "PlasmaAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_plasma";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[PlasmaAmmo] = 10;
$WeaponsListID["Plasma"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Fires magnetically contained plasma fireballs";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 90 | Speed: 100 | Force: 500";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Chaingun";
$NameToInv["Chaingun"] = "Chaingun";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_chaingun";
$WeaponsHudData[$WeaponCount, itemDataName] = "Chaingun";
$WeaponsHudData[$WeaponCount, ammoDataName] = "ChaingunAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_chaingun";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[ChaingunAmmo] = 25;
$WeaponsListID["Chaingun"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Motor powered gattling weapon, very accurate";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 25 | Speed: 600 | Force: 0 | Spread: 3";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Spinfusor";
$NameToInv["Spinfusor"] = "Disc";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_disc";
$WeaponsHudData[$WeaponCount, itemDataName] = "Disc";
$WeaponsHudData[$WeaponCount, ammoDataName] = "DiscAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_disc";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[DiscAmmo] = 5;
$WeaponsListID["Disc"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Maglev-powered high explosive skeet launcher";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 100 | Speed: 250 | Force: 2500 | Style Points: +1";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Grenade Launcher";
$NameToInv["Grenade Launcher"] = "GrenadeLauncher";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_grenlaunch";
$WeaponsHudData[$WeaponCount, itemDataName] = "GrenadeLauncher";
$WeaponsHudData[$WeaponCount, ammoDataName] = "GrenadeLauncherAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_grenade";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[GrenadeLauncherAmmo] = 5;
$WeaponsListID["GrenadeLauncher"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Shoots your currently equipped hand grenade";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: variable | Speed: 200 | Force: variable";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Laser Rifle";
$NameToInv["Laser Rifle"] = "SniperRifle";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_sniper";
$WeaponsHudData[$WeaponCount, itemDataName] = "SniperRifle";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/hud_ret_sniper";
$WeaponsHudData[$WeaponCount, visible] = "false";
$WeaponsListID["SniperRifle"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Precision pulse laser rifle, best used close range";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 150 | Optimal Range: 75 | Maximum Range: 150 | Energy: 75";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "ELF Projector";
$NameToInv["ELF Projector"] = "ELFGun";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_elfgun";
$WeaponsHudData[$WeaponCount, itemDataName] = "ELFGun";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_elf";
$WeaponsHudData[$WeaponCount, visible] = "true";
$WeaponsListID["ELFGun"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Danger danger! High voltage! When we touch...";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 64/sec | Maximum Range: 25 | Energy: 32/sec";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Mortar";
$NameToInv["Mortar"] = "Mortar";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_mortor";
$WeaponsHudData[$WeaponCount, itemDataName] = "Mortar";
$WeaponsHudData[$WeaponCount, ammoDataName] = "MortarAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_mortor";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[MortarAmmo] = 5;
$WeaponsListID["Mortar"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Launches impact sensitive high explosives over long distances";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 300 | Speed: 125 | Force: 5000";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Missile Launcher";
$NameToInv["Missile Launcher"] = "MissileLauncher";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_missiles";
$WeaponsHudData[$WeaponCount, itemDataName] = "MissileLauncher";
$WeaponsHudData[$WeaponCount, ammoDataName] = "MissileLauncherAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_missile";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[MissileLauncherAmmo] = 2;
$WeaponsListID["MissileLauncher"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Launches heat seeking missiles";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 250 | Speed: 250 | Force: 3000 | Lifetime: 10";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Shocklance";
$NameToInv["Shocklance"] = "ShockLance";
$WeaponsHudData[$WeaponCount, bitmapName]   = "gui/hud_shocklance";
$WeaponsHudData[$WeaponCount, itemDataName] = "ShockLance";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/hud_ret_shocklance";
$WeaponsHudData[$WeaponCount, visible] = "false";
$WeaponsListID["ShockLance"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Reach out and torch someone.";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 150 (x2 from behind) | Maximum Range: 20 | Force: 4000 | Energy: 40";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Targeting Laser";
//$NameToInv["Targeting Laser"] = "TargetingLaser";
// WARNING!!! If you change the weapon index of the targeting laser,
// you must change the HudWeaponInvBase::addWeapon function to test
// for the new value!
// 9
$WeaponsHudData[$WeaponCount, bitmapName]   = "gui/hud_targetlaser";
$WeaponsHudData[$WeaponCount, itemDataName] = "TargetingLaser";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/hud_ret_targlaser";
$WeaponsHudData[$WeaponCount, visible] = "false";
$WeaponsListID["TargetingLaser"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Targeting Laser Pointer";
$WeaponsListData[$WeaponCount, "stats"] = "";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Nullifier Rifle";
$NameToInv["Nullifier Rifle"] = "Nullifier";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_blaster";
$WeaponsHudData[$WeaponCount, itemDataName] = "Nullifier";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_blaster";
$WeaponsHudData[$WeaponCount, visible] = "true";
$WeaponsListID["Nullifier"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Energy nullifier, only does damage to shields";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 120 (shields only) | Speed: 300 | Energy: 8";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Phaser Cannon";
$NameToInv["Phaser Cannon"] = "HeavyBlaster";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_blaster";
$WeaponsHudData[$WeaponCount, itemDataName] = "HeavyBlaster";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_blaster";
$WeaponsHudData[$WeaponCount, visible] = "true";
$WeaponsListID["HeavyBlaster"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Penetrates shields with 50% of it's damage.";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 125 | Speed: 150 | Force: 750 | Energy: 32";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Plasma Cannon";
$NameToInv["Plasma Cannon"] = "PlasmaCannon";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_plasma";
$WeaponsHudData[$WeaponCount, itemDataName] = "PlasmaCannon";
$WeaponsHudData[$WeaponCount, ammoDataName] = "PlasmaCannonAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_plasma";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[PlasmaCannonAmmo] = 10;
$WeaponsListID["PlasmaCannon"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Turret-sized version of the Plasma Rifle";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 240 | Speed: 250 | Force: 1500";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Howitzer";
$NameToInv["Howitzer"] = "Howitzer";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_mortor";
$WeaponsHudData[$WeaponCount, itemDataName] = "Howitzer";
$WeaponsHudData[$WeaponCount, ammoDataName] = "HowitzerAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_mortor";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[HowitzerAmmo] = 5;
$WeaponsListID["Howitzer"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "High velocity kinetic charge, trades explosive area for damage";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 450 | Speed: 400 | Force: 6000";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Minigun";
$NameToInv["Minigun"] = "Minigun";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_chaingun";
$WeaponsHudData[$WeaponCount, itemDataName] = "Minigun";
$WeaponsHudData[$WeaponCount, ammoDataName] = "MinigunAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_chaingun";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[MinigunAmmo] = 25;
$WeaponsListID["Minigun"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "High rate of fire damage per bullet but wide spread";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 25 | Speed: 600 | Force: 0 | Spread: 12";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Rocket Launcher";
$NameToInv["Rocket Launcher"] = "RocketLauncher";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_missiles";
$WeaponsHudData[$WeaponCount, itemDataName] = "RocketLauncher";
$WeaponsHudData[$WeaponCount, ammoDataName] = "RocketLauncherAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_missile";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[RocketLauncherAmmo] = 2;
$WeaponsListID["RocketLauncher"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Launches dumbfire rockets, does more damage than missiles";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 250 | Speed: 300 | Force: 3750";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Anti-Air Blaster";
$NameToInv["Anti-Air Blaster"] = "Bolter";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_blaster";
$WeaponsHudData[$WeaponCount, itemDataName] = "Bolter";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_blaster";
$WeaponsHudData[$WeaponCount, visible] = "true";
$WeaponsListID["Bolter"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Rapid fire anti-air energy weapon";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 30 | Speed: 400 | Force: 0 | Spread: 0 | Energy: 8";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Spike Rifle";
$NameToInv["Spike Rifle"] = "GaussRifle";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_plasma";
$WeaponsHudData[$WeaponCount, itemDataName] = "GaussRifle";
$WeaponsHudData[$WeaponCount, ammoDataName] = "GaussRifleAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_plasma";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[GaussRifleAmmo] = 10;
$WeaponsListID["GaussRifle"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Fires high velocity landspike bolts";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 75 | Speed: 500 | Force: 750";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Scattergun";
$NameToInv["Scattergun"] = "Scattergun";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_plasma";
$WeaponsHudData[$WeaponCount, itemDataName] = "Scattergun";
$WeaponsHudData[$WeaponCount, ammoDataName] = "ScattergunAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_plasma";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[ScattergunAmmo] = 10;
$WeaponsListID["Scattergun"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "12-bullet wide spread of pain";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 360 (30x12) | Speed: 425 | Force: 0 | Spread: 21";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Laser Cannon";
$NameToInv["Laser Cannon"] = "PulseLaserCannon";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_sniper";
$WeaponsHudData[$WeaponCount, itemDataName] = "PulseLaserCannon";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/hud_ret_sniper";
$WeaponsHudData[$WeaponCount, visible] = "false";
$WeaponsListID["PulseLaserCannon"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Constant beam of laser energy";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 40/sec | Optimal Range: 100m | Max Range: 200m | Energy: 5/sec";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "The Fatman";
$NameToInv["The Fatman"] = "Fatman";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_missiles";
$WeaponsHudData[$WeaponCount, itemDataName] = "Fatman";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_missile";
$WeaponsHudData[$WeaponCount, visible] = "true";
$WeaponsListID["Fatman"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Single use cruise missile";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 3250 | Speed: 200 | Force: 7500";
$WeaponCount++;

$WeaponsHudCount = $WeaponCount;

//------------------------------------------------------------------------------
// Packs
// todo: make like weapons ^

$InvPack[0] = "Energy Pack";
$InvPack[1] = "Repair Pack";
$InvPack[2] = "Shield Pack";
$InvPack[3] = "Cloak Pack";
$InvPack[4] = "Sensor Jammer Pack";
$InvPack[5] = "Ammunition Pack";
$InvPack[6] = "Satchel Charge";
$InvPack[7] = "Motion Sensor Pack";
$InvPack[8] = "Pulse Sensor Pack";
$InvPack[9] = "Inventory Station";
$InvPack[10] = "Landspike Turret";
$InvPack[11] = "Spider Clamp Turret";
$InvPack[12] = "ELF Turret Barrel";
$InvPack[13] = "Mortar Turret Barrel";
$InvPack[14] = "Plasma Turret Barrel";
$InvPack[15] = "AA Turret Barrel";
$InvPack[16] = "Missile Turret Barrel";
$InvPack[17] = "TR2 Energy Pack"; // z0dd - ZOD, 9/12/02. TR2 need

$NameToInv["Energy Pack"] = "EnergyPack";
$NameToInv["Repair Pack"] = "RepairPack";
$NameToInv["Shield Pack"] = "ShieldPack";
$NameToInv["Cloak Pack"] = "CloakingPack";
$NameToInv["Sensor Jammer Pack"] = "SensorJammerPack";
$NameToInv["Ammunition Pack"] = "AmmoPack";
$NameToInv["Satchel Charge"] = "SatchelCharge";
$NameToInv["Motion Sensor Pack"] = "MotionSensorDeployable";
$NameToInv["Pulse Sensor Pack"] = "PulseSensorDeployable";
$NameToInv["Inventory Station"] = "InventoryDeployable";
$NameToInv["Landspike Turret"] = "TurretOutdoorDeployable";
$NameToInv["Spider Clamp Turret"] = "TurretIndoorDeployable";
$NameToInv["ELF Turret Barrel"] = "ELFBarrelPack";
$NameToInv["Mortar Turret Barrel"] = "MortarBarrelPack";
$NameToInv["Plasma Turret Barrel"] = "PlasmaBarrelPack";
$NameToInv["AA Turret Barrel"] = "AABarrelPack";
$NameToInv["Missile Turret Barrel"] = "MissileBarrelPack";

// non-team mission pack choices (DM, Hunters, Rabbit)

$NTInvPack[0] = "Energy Pack";
$NTInvPack[1] = "Repair Pack";
$NTInvPack[2] = "Shield Pack";
$NTInvPack[3] = "Cloak Pack";
$NTInvPack[4] = "Sensor Jammer Pack";
$NTInvPack[5] = "Ammunition Pack";
$NTInvPack[6] = "Satchel Charge";
$NTInvPack[7] = "Motion Sensor Pack";
$NTInvPack[8] = "Pulse Sensor Pack";
$NTInvPack[9] = "Inventory Station";

//------------------------------------------------------------------------------
// Inventory Defines
$InventoryHudCount = 0;

//------------------------------------------------------------------------------
// Grenades
$GrenadeCount = 0;

$InvGrenade[$GrenadeCount] = "Grenade";
$NameToInv["Grenade"] = "Grenade";
$AmmoIncrement[Grenade]             = 5;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_handgren";
$InventoryHudData[$InventoryHudCount, itemDataName] = Grenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = Grenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

$InvGrenade[$GrenadeCount] = "Whiteout Grenade";
$NameToInv["Whiteout Grenade"] = "FlashGrenade";
$AmmoIncrement[FlashGrenade]        = 5;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_whiteout_gren";
$InventoryHudData[$InventoryHudCount, itemDataName] = FlashGrenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = FlashGrenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

$InvGrenade[$GrenadeCount] = "Concussion Grenade";
$NameToInv["Concussion Grenade"] = "ConcussionGrenade";
$AmmoIncrement[ConcussionGrenade]   = 5;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_concuss_gren";
$InventoryHudData[$InventoryHudCount, itemDataName] = ConcussionGrenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = ConcussionGrenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

$InvGrenade[$GrenadeCount] = "Flare Grenade";
$NameToInv["Flare Grenade"] = "FlareGrenade";
$AmmoIncrement[FlareGrenade]        = 5;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_handgren";
$InventoryHudData[$InventoryHudCount, itemDataName] = FlareGrenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = FlareGrenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

$InvGrenade[$GrenadeCount] = "Deployable Camera";
$NameToInv["Deployable Camera"] = "CameraGrenade";
$AmmoIncrement[CameraGrenade]       = 2; // z0dd - ZOD, 4/17/02. Camera ammo pickup fix.
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_handgren";
$InventoryHudData[$InventoryHudCount, itemDataName] = CameraGrenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = CameraGrenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

//------------------------------------------------------------------------------
// Mines
$MineCount = 0;

$InvMine[$MineCount] = "Mine";
$NameToInv["Mine"] = "Mine";
$AmmoIncrement[Mine] = 3;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_mine";
$InventoryHudData[$InventoryHudCount, itemDataName] = Mine;
$InventoryHudData[$InventoryHudCount, ammoDataName] = Mine;
$InventoryHudData[$InventoryHudCount, slot]         = 1;
$InventoryHudCount++;
$MineCount++;

//-----------------------------------------------------------------------------
// Misc
$AmmoIncrement[RepairKit]           = 1;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_medpack";
$InventoryHudData[$InventoryHudCount, itemDataName] = RepairKit;
$InventoryHudData[$InventoryHudCount, ammoDataName] = RepairKit;
$InventoryHudData[$InventoryHudCount, slot]         = 3;
$InventoryHudCount++;

$AmmoIncrement[Beacon]              = 1; // z0dd - ZOD, 4/17/02. Beacon ammo pickup fix.
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_beacon";
$InventoryHudData[$InventoryHudCount, itemDataName] = Beacon;
$InventoryHudData[$InventoryHudCount, ammoDataName] = Beacon;
$InventoryHudData[$InventoryHudCount, slot]         = 2;
$InventoryHudCount++;

//-----------------------------------------------------------------------------
// ShapeBase - Inventory Management

function ShapeBase::clearInventory(%this)
{
    for(%i = 0; %i < $WeaponCount; %i++)
    {
        %this.setInventory($WeaponsHudData[%i, itemDataName], 0);
        %this.setInventory($WeaponsHudData[%i, ammoDataName], 0);
    }
    
    for(%j = 0; %j < $InventoryHudCount; %j++)
        %this.setInventory($InventoryHudData[%j, itemDataName], 0);

    // take away any pack the player has
    %curPack = %this.getMountedImage($BackpackSlot);

    if(%curPack > 0)
        %this.setInventory(%curPack.item, 0);
}

// Weapons system transplant - must be loaded before any image
// Trigger Proxy Image
datablock ShapeBaseImageData(TriggerProxyImage)
{
   className = WeaponImage;

   shapeFile = "turret_muzzlepoint.dts";
   item = TargetingLaser;
   offset = "0 0 0";
   triggerProxySlot = $WeaponSubImage1;

   usesEnergy = true;
   minEnergy = 0;

   stateName[0]                     = "Activate";
   stateTimeoutValue[0]             = 0.5;
   stateTransitionOnTimeout[0]      = "ActivateReady";

   stateName[1]                     = "ActivateReady";
   stateTransitionOnAmmo[1]         = "Ready";
   stateTransitionOnNoAmmo[1]       = "NoAmmo";

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Trigger";

   stateName[3]                     = "Trigger";
   stateAllowImageChange[3]         = false;
   stateScript[3]                   = "onTriggerDown";
   stateTransitionOnTriggerUp[3]    = "Release";
   stateTransitionOnNoAmmo[3]       = "Release";

   stateName[4]                     = "NoAmmo";
   stateTransitionOnAmmo[4]         = "Ready";

   stateName[5]                     = "Release";
   stateScript[5]                   = "onTriggerUp";
   stateTransitionOnTimeout[5]      = "Ready";
};

function ShapeBaseImageData::onTriggerDown(%data, %obj, %slot)
{
    %obj.setImageTrigger(%data.triggerProxySlot, true);
}

function ShapeBaseImageData::onTriggerUp(%data, %obj, %slot)
{
    %obj.setImageTrigger(%data.triggerProxySlot, false);
}

//-----------------------------------------------------------------------------
// Misc Inventory Overrides

function ShapeBase::maxInventory(%this, %data)
{
   return %this.getDatablock().max[%data.getName()];
}

function ShapeBase::maxBaseInventory(%this, %data)
{
   return %this.getDatablock().max[%data.getName()];
}
