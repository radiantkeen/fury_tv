
// HUD Enhancements
//-----------------------------------------------------------------------------
function clientCmdShowDeploySensor(%color, %time)
{
    if(FuryClient.deploySensorSchedule > 0)
        cancel(FuryClient.deploySensorSchedule);

    deploySensor.color = %color;
    deploySensor.setVisible(true);

    if(%time > 0)
        FuryClient.deploySensorSchedule = schedule(%time, 0, "FuryClientClearDeploySensor");
}

function clientCmdClearDeploySensor()
{
    deploySensor.setVisible(false);
}

function FuryClientClearDeploySensor()
{
    FuryClient.deploySensorSchedule = 0;
    deploySensor.setVisible(false);
}

function clientCmdShowAmmoCounter()
{
    ammoHud.setVisible(true);
}

function clientCmdChangeReticle(%tex, %bFrame)
{
    reticleHud.setBitmap(%tex);
    reticleFrameHud.setVisible(%bFrame);
}

function clientCmdSetVehHighlight(%slot)
{
    if($F_lastVHighlight $= "")
        $F_lastVHighlight = 0;

    %oldHilite = "vWeap"@ $F_lastVHighlight @"Hilite";
    %oldHilite.setVisible(false);

    %newHilite = "vWeap"@ %slot @"Hilite";
    %newHilite.setVisible(true);

    $F_lastVHighlight = %slot;
}

// Base T2 HUD overrides
//-----------------------------------------------------------------------------
function GuiControl::updateAltitude(%this)
{
   %alt = getControlObjectAltitude();
   vAltitudeText.setValue(%alt);
   %this.altitudeCheck = %this.schedule(100, "updateAltitude");
}

function GuiControl::updateSpeed(%this)
{
   %vel = getControlObjectSpeed();
   // convert from m/s to km/h
   %cVel = mFloor(%vel * 3.6); // m/s * (3600/1000) = km/h
   vSpeedText.setValue(%cVel);
   %this.speedCheck = %this.schedule(100, "updateSpeed");
}

function clientCmdToggleDashHud(%val)
{
   if(!%val) {
      if(isObject(vDiagramHud))
      {
         vDiagramHud.delete();
         cancel(dashboardHud.speedCheck);
         vSpeedBox.delete();
      }
      if(isObject(vOverheadHud))
         vOverheadHud.delete();
      if(isObject(vEnergyFrame))
         vEnergyFrame.delete();
      if(isObject(vDamageFrame))
         vDamageFrame.delete();
      if(isObject(vAltitudeBox))
      {
         cancel(dashboardHud.altitudeCheck);
         vAltitudeBox.delete();
      }
      if(isObject(vWeaponOne))
         vWeaponOne.delete();
      if(isObject(vWeaponTwo))
         vWeaponTwo.delete();
      if(isObject(vWeaponThree))
         vWeaponThree.delete();
      if(isObject(vWeapHiliteOne))
         vWeapHiliteOne.delete();
      if(isObject(vWeapHiliteTwo))
         vWeapHiliteTwo.delete();
      if(isObject(vWeapHiliteThree))
         vWeapHiliteThree.delete();
      if(isObject(vPassengerHud))
         vPassengerHud.delete();
      if(isObject(bombardierHud))
         bombardierHud.delete();
      if(isObject(turreteerHud))
         turreteerHud.delete();
      // reset in case of vehicle-specific reticle
      //reticleHud.setBitmap("");
      //reticleFrameHud.setVisible(false);
   }
   dashboardHud.setVisible(%val);
}

function addEnergyGauge( %vehType )
{
   switch$ (%vehType)
   {
      case "Assault" or "Bomber":
      dashboardHud.nrgBar = new HudBitmapCtrl(vEnergyFrame) {
         profile = "GuiDashBoxProfile";
         horizSizing = "right";
         vertSizing = "top";
         position = "160 80";
         extent = "118 19";
         minExtent = "8 8";
         visible = "1";
         bitmap = "gui/hud_veh_new_dashpiece_5";
         opacity = "0.8";

         new HudEnergy(vEnergyBar) {
            profile = "GuiDefaultProfile";
            horizSizing = "right";
            vertSizing = "top";
            position = "0 0";
            extent = "118 19";
            minExtent = "8 8";
            visible = "1";
            fillColor = "0.353000 0.373000 0.933000 0.800000";
            frameColor = "0.000000 1.000000 0.000000 1.000000";
            autoCenter = "0";
            autoResize = "0";
            displayMounted = true;
            bitmap = "gui/hud_veh_new_dashpiece_5";
            verticalFill = false;
            subRegion = "4 5 98 5";
            pulseRate = "500";
            pulseThreshold = "0.3";
            //modColor = "1.000000 0.500000 0.000000 1.000000";
         };

         new HudCapacitor(vCapBar) {
            profile = "GuiDefaultProfile";
            horizSizing = "right";
            vertSizing = "top";
            position = "0 8";
            extent = "118 8";
            minExtent = "8 8";
            visible = "1";
            fillColor = "1.000 0.729 0.301 0.800000";
            frameColor = "0.000000 1.000000 0.000000 1.000000";
            autoCenter = "0";
            autoResize = "0";
            displayMounted = true;
            bitmap = "gui/hud_veh_new_dashpiece_5";
            verticalFill = false;
            subRegion = "4 5 98 5";
            pulseRate = "500";
            pulseThreshold = "0.3";
            //modColor = "1.000000 0.500000 0.000000 1.000000";
         };
      };
      dashboardHud.add(dashboardHud.nrgBar);

      default:
      dashboardHud.nrgBar = new HudBitmapCtrl(vEnergyFrame) {
         profile = "GuiDashBoxProfile";
         horizSizing = "right";
         vertSizing = "top";
         position = "160 80";
         extent = "118 19";
         minExtent = "8 8";
         visible = "1";
         bitmap = "gui/hud_veh_new_dashpiece_5";
         opacity = "0.8";

         new HudEnergy(vEnergyBar) {
            profile = "GuiDefaultProfile";
            horizSizing = "right";
            vertSizing = "top";
            position = "0 0";
            extent = "118 19";
            minExtent = "8 8";
            visible = "1";
            fillColor = "0.353000 0.373000 0.933000 0.800000";
            frameColor = "0.000000 1.000000 0.000000 1.000000";
            autoCenter = "0";
            autoResize = "0";
            displayMounted = true;
            bitmap = "gui/hud_veh_new_dashpiece_5";
            verticalFill = false;
            subRegion = "4 5 98 10";
            pulseRate = "500";
            pulseThreshold = "0.3";
            //modColor = "1.000000 0.500000 0.000000 1.000000";
         };
      };
      dashboardHud.add(dashboardHud.nrgBar);
   }
}

function clientCmdShowVehicleGauges(%vehType, %node)
{
   //if(!((%vehType $= "Bomber" || %vehType $= "Assault") && %node > 0))
   if(%node == 0)
   {
      // common elements that show up on all vehicle pilot HUDs
      dashboardHud.diagram = new HudBitmapCtrl(vDiagramHud) {
         profile = "GuiDashBoxProfile";
         horizSizing = "right";
         vertSizing = "top";
         position = "200 10";
         extent = "176 108";
         minExtent = "8 8";
         visible = "1";
         bitmap = "gui/hud_veh_new_dash";
         opacity = "0.8";
      };
      dashboardHud.add(dashboardHud.diagram);

      dashboardHud.vehDiagram = new HudBitmapCtrl(vOverheadHud) {
         profile = "GuiDashBoxProfile";
         horizSizing = "right";
         vertSizing = "top";
         position = "256 0";
         extent = "128 128";
         minExtent = "8 8";
         visible = "1";
         bitmap = "";
         opacity = "1.0";
      };
      dashboardHud.add(dashboardHud.vehDiagram);

      addEnergyGauge( %vehType );

      dashboardHud.dmgBar = new HudBitmapCtrl(vDamageFrame) {
         profile = "GuiDashBoxProfile";
         horizSizing = "right";
         vertSizing = "top";
         position = "361 80";
         extent = "118 19";
         minExtent = "8 8";
         visible = "1";
         bitmap = "gui/hud_veh_new_dashpiece_4";
         opacity = "0.8";

         new HudDamage(vDamageBar) {
            profile = "GuiDefaultProfile";
            horizSizing = "right";
            vertSizing = "top";
            position = "0 0";
            extent = "118 19";
            minExtent = "8 8";
            visible = "1";
            fillColor = "0.000000 1.0000 0.000000 0.800000";
            frameColor = "0.000000 1.000000 0.000000 0.000000";
            bitmap = "gui/hud_veh_new_dashpiece_4";
            verticalFill = false;
            displayMounted = true;
            opacity = "0.8";
            subRegion = "18 5 97 10";
            pulseRate = "500";
            pulseThreshold = "0.3";
            //modColor = "1.000000 0.500000 0.000000 1.000000";
         };
      };
      dashboardHud.add(dashboardHud.dmgBar);

      dashboardHud.speedBox = new GuiControl(vSpeedBox) {
         profile = "GuiDashBoxProfile";
         horizSizing = "right";
         vertSizing = "top";
         position = "210 47";
         extent = "40 40";
         minExtent = "8 8";
         visible = "1";

         new GuiTextCtrl(vSpeedText) {
            profile = "GuiDashTextProfile";
            horizSizing = "right";
            vertSizing = "top";
            position = "3 15";
            extent = "18 15";
            minExtent = "8 8";
            visible = "1";
            text = "test";
         };
         new GuiTextCtrl(vSpeedTxtLbl) {
            profile = "GuiDashTextProfile";
            horizSizing = "right";
            vertSizing = "top";
            position = "18 15";
            extent = "27 15";
            minExtent = "8 8";
            visible = "1";
            text = "KPH";
         };
      };
      dashboardHud.add(dashboardHud.speedBox);

      dashboardHud.updateSpeed();
   }

   switch$ (%vehType) {
      case "Shrike" :
         vOverheadHud.setBitmap("gui/hud_veh_icon_shrike");
         // add altitude box for flying vehicles
         dashboardHud.altBox = new HudBitmapCtrl(vAltitudeBox) {
            profile = "GuiDashBoxProfile";
            horizSizing = "right";
            vertSizing = "top";
            position = "371 56";
            extent = "68 22";
            minExtent = "8 8";
            bitmap = "gui/hud_veh_new_dashpiece_1";
            visible = "1";
            opacity = "0.8";

            new GuiTextCtrl(vAltitudeText) {
               profile = "GuiDashTextProfile";
               horizSizing = "right";
               vertSizing = "top";
               position = "19 5";
               extent = "18 15";
               minExtent = "8 8";
               visible = "1";
               text = "test";
            };
            new GuiTextCtrl(vAltitudeTxtLbl) {
               profile = "GuiDashTextProfile";
               horizSizing = "right";
               vertSizing = "top";
               position = "40 5";
               extent = "12 15";
               minExtent = "8 8";
               visible = "1";
               text = "M";
            };
         };
         dashboardHud.add(dashboardHud.altBox);
         dashboardHud.updateAltitude();
         // add right-hand weapons box and highlight
         dashboardHud.weapon = new GuiControl(vWeapHiliteOne) {
            profile = "GuiDashBoxProfile";
            horizSizing = "right";
            vertSizing = "top";
            position = "358 22";
            extent = "80 33";
            minExtent = "8 8";
            visible = "1";

            new HudBitmapCtrl(vWeapBkgdOne) {
               profile = "GuiDashBoxProfile";
               horizSizing = "right";
               vertSizing = "top";
               position = "0 0";
               extent = "82 40";
               minExtent = "8 8";
               bitmap = "gui/hud_veh_new_dashpiece_2";
               visible = "1";
               opacity = "0.8";

               new HudBitmapCtrl(vWeapIconOne) {
                  profile = "GuiDashBoxProfile";
                  horizSizing = "right";
                  vertSizing = "top";
                  position = "28 6";
                  extent = "25 25";
                  minExtent = "8 8";
                  bitmap = "gui/hud_blaster";
                  visible = "1";
                  opacity = "0.8";
               };
            };
         };
         dashboardHud.add(dashboardHud.weapon);
         // change to shrike reticle
//         reticleHud.setBitmap("gui/hud_ret_shrike");
//         reticleFrameHud.setVisible(false);

      case "Bomber" :
         if(%node == 1)
         {
            // bombardier hud
            dashboardHud.bHud = new GuiControl(bombardierHud) {
               profile = "GuiDefaultProfile";
               horizSizing = "center";
               vertSizing = "top";
               position = "200 75";
               extent = "240 50";
               minExtent = "8 8";
               visible = "1";

               new HudBitmapCtrl(vWeap1Hilite) {
                  profile = "GuiDashBoxProfile";
                  horizSizing = "right";
                  vertSizing = "bottom";
                  position = "18 9";
                  extent = "80 44";
                  minExtent = "8 8";
                  visible = "1";
                  bitmap = "gui/hud_veh_new_hilite_left";
                  opacity = "0.3";
               };
               new HudBitmapCtrl(vWeap2Hilite) {
                  profile = "GuiDashBoxProfile";
                  horizSizing = "right";
                  vertSizing = "bottom";
                  position = "141 9";
                  extent = "80 44";
                  minExtent = "8 8";
                  visible = "0";
                  bitmap = "gui/hud_veh_new_hilite_right";
                  opacity = "0.3";
               };
               new HudBitmapCtrl(vWeap3Hilite) {
                  profile = "GuiDashBoxProfile";
                  horizSizing = "right";
                  vertSizing = "bottom";
                  position = "99 9";
                  extent = "40 44";
                  minExtent = "8 8";
                  visible = "0";
                  bitmap = "gui/hud_veh_new_hilite_middle";
                  opacity = "0.3";
               };

               new HudBitmapCtrl(bombardierFrame) {
                  profile = "GuiDashBoxProfile";
                  horizSizing = "center";
                  vertSizing = "bottom";
                  position = "20 8";
                  extent = "200 40";
                  minExtent = "8 8";
                  visible = "1";
                  bitmap = "gui/hud_veh_new_bombardier_dash";
                  opacity = "1.0";

                  new HudBitmapCtrl(vWeaponOne) {
                     profile = "GuiDashBoxProfile";
                     horizSizing = "right";
                     vertSizing = "bottom";
                     position = "28 5";
                     extent = "25 25";
                     minExtent = "8 8";
                     visible = "1";
                     bitmap = "gui/hud_blaster";
                  };

                  new HudBitmapCtrl(vWeaponTwo) {
                     profile = "GuiDashBoxProfile";
                     horizSizing = "right";
                     vertSizing = "bottom";
                     position = "87 6";
                     extent = "25 25";
                     minExtent = "8 8";
                     visible = "1";
                     bitmap = "gui/hud_targetlaser";
                  };

                  new HudBitmapCtrl(vWeaponThree) {
                     profile = "GuiDashBoxProfile";
                     horizSizing = "right";
                     vertSizing = "bottom";
                     position = "147 6";
                     extent = "25 25";
                     minExtent = "8 8";
                     visible = "1";
                     bitmap = "gui/hud_veh_bomb";
                  };
               };
            };
            dashboardHud.add(dashboardHud.bHud);

            dashboardHud.nrgBar = new HudBitmapCtrl(vEnergyFrame) {
               profile = "GuiDashBoxProfile";
               horizSizing = "right";
               vertSizing = "top";
               position = "110 95";
               extent = "118 19";
               minExtent = "8 8";
               visible = "1";
               flipVertical = true;
               bitmap = "gui/hud_veh_new_dashpiece_5";
               opacity = "0.8";

               new HudEnergy(vEnergyBar) {
                  profile = "GuiDefaultProfile";
                  horizSizing = "right";
                  vertSizing = "top";
                  position = "0 0";
                  extent = "118 19";
                  minExtent = "8 8";
                  visible = "1";
                  fillColor = "0.353000 0.373000 0.933000 0.800000";
                  frameColor = "0.000000 1.000000 0.000000 1.000000";
                  autoCenter = "0";
                  autoResize = "0";
                  displayMounted = true;
                  bitmap = "gui/hud_veh_new_dashpiece_5";
                  verticalFill = false;
                  subRegion = "4 5 98 5";
                  pulseRate = "500";
                  pulseThreshold = "0.3";
                  //modColor = "1.000000 0.500000 0.000000 1.000000";
               };
               new HudCapacitor(vCapBar) {
                  profile = "GuiDefaultProfile";
                  horizSizing = "right";
                  vertSizing = "top";
                  position = "0 8";
                  extent = "118 8";
                  minExtent = "8 8";
                  visible = "1";
                  fillColor = "1.000 0.729 0.301 0.800000";
                  frameColor = "0.000000 1.000000 0.000000 1.000000";
                  autoCenter = "0";
                  autoResize = "0";
                  displayMounted = true;
                  bitmap = "gui/hud_veh_new_dashpiece_5";
                  verticalFill = false;
                  subRegion = "4 5 98 5";
                  pulseRate = "500";
                  pulseThreshold = "0.3";
                  //modColor = "1.000000 0.500000 0.000000 1.000000";
               };
            };
            dashboardHud.add(dashboardHud.nrgBar);

            dashboardHud.dmgBar = new HudBitmapCtrl(vDamageFrame) {
               profile = "GuiDashBoxProfile";
               horizSizing = "right";
               vertSizing = "top";
               position = "410 95";
               extent = "118 19";
               minExtent = "8 8";
               visible = "1";
               flipVertical = true;
               bitmap = "gui/hud_veh_new_dashpiece_4";
               opacity = "0.8";

               new HudDamage(vDamageBar) {
                  profile = "GuiDefaultProfile";
                  horizSizing = "right";
                  vertSizing = "top";
                  position = "0 0";
                  extent = "118 19";
                  minExtent = "8 8";
                  visible = "1";
                  fillColor = "0.000000 1.0000 0.000000 0.800000";
                  frameColor = "0.000000 1.000000 0.000000 0.000000";
                  bitmap = "gui/hud_veh_new_dashpiece_4";
                  verticalFill = false;
                  displayMounted = true;
                  opacity = "0.8";
                  subRegion = "18 5 97 10";
                  pulseRate = "500";
                  pulseThreshold = "0.3";
                  //modColor = "1.000000 0.500000 0.000000 1.000000";
               };
            };
            dashboardHud.add(dashboardHud.dmgBar);
            $numVWeapons = 3;
//            reticleHud.setBitmap("gui/hud_ret_shrike");
//            reticleFrameHud.setVisible(false);
         }
         else if(%node == 0)
         {
            // pilot dashboard hud
            vOverheadHud.setBitmap("gui/hud_veh_icon_bomber");
            // add altitude box for flying vehicles
            dashboardHud.altBox = new HudBitmapCtrl(vAltitudeBox) {
               profile = "GuiDashBoxProfile";
               horizSizing = "right";
               vertSizing = "top";
               position = "371 56";
               extent = "68 22";
               minExtent = "8 8";
               bitmap = "gui/hud_veh_new_dashpiece_1";
               visible = "1";
               opacity = "0.8";

               new GuiTextCtrl(vAltitudeText) {
                  profile = "GuiDashTextProfile";
                  horizSizing = "right";
                  vertSizing = "top";
                  position = "19 5";
                  extent = "18 15";
                  minExtent = "8 8";
                  visible = "1";
                  text = "test";
               };
               new GuiTextCtrl(vAltitudeTxtLbl) {
                  profile = "GuiDashTextProfile";
                  horizSizing = "right";
                  vertSizing = "top";
                  position = "40 5";
                  extent = "12 15";
                  minExtent = "8 8";
                  visible = "1";
                  text = "M";
               };
            };
            dashboardHud.add(dashboardHud.altBox);
            dashboardHud.updateAltitude();
         }
         else
         {
            // tailgunner hud
            dashboardHud.vehDiagram = new HudBitmapCtrl(vOverheadHud) {
               profile = "GuiDashBoxProfile";
               horizSizing = "right";
               vertSizing = "top";
               position = "256 0";
               extent = "128 128";
               minExtent = "8 8";
               visible = "1";
               bitmap = "gui/hud_veh_icon_bomber";
               opacity = "1.0";
            };
            dashboardHud.add(dashboardHud.vehDiagram);

            dashboardHud.nrgBar = new HudBitmapCtrl(vEnergyFrame) {
               profile = "GuiDashBoxProfile";
               horizSizing = "right";
               vertSizing = "top";
               position = "177 50";
               extent = "118 19";
               minExtent = "8 8";
               visible = "1";
               bitmap = "gui/hud_veh_new_dashpiece_5";
               flipVertical = true;
               opacity = "0.8";

               new HudEnergy(vEnergyBar) {
                  profile = "GuiDefaultProfile";
                  horizSizing = "right";
                  vertSizing = "top";
                  position = "0 0";
                  extent = "118 19";
                  minExtent = "8 8";
                  visible = "1";
                  fillColor = "0.353000 0.373000 0.933000 0.800000";
                  frameColor = "0.000000 1.000000 0.000000 1.000000";
                  autoCenter = "0";
                  autoResize = "0";
                  displayMounted = true;
                  bitmap = "gui/hud_veh_new_dashpiece_5";
                  verticalFill = false;
                  subRegion = "4 5 98 10";
                  pulseRate = "500";
                  pulseThreshold = "0.3";
               };
            };
            dashboardHud.add(dashboardHud.nrgBar);

            dashboardHud.dmgBar = new HudBitmapCtrl(vDamageFrame) {
               profile = "GuiDashBoxProfile";
               horizSizing = "right";
               vertSizing = "top";
               position = "345 50";
               extent = "118 19";
               minExtent = "8 8";
               visible = "1";
               bitmap = "gui/hud_veh_new_dashpiece_4";
               flipVertical = true;
               opacity = "0.8";

               new HudDamage(vDamageBar) {
                  profile = "GuiDefaultProfile";
                  horizSizing = "right";
                  vertSizing = "top";
                  position = "0 0";
                  extent = "118 19";
                  minExtent = "8 8";
                  visible = "1";
                  fillColor = "0.000000 1.0000 0.000000 0.800000";
                  frameColor = "0.000000 1.000000 0.000000 0.000000";
                  bitmap = "gui/hud_veh_new_dashpiece_4";
                  verticalFill = false;
                  displayMounted = true;
                  opacity = "0.8";
                  subRegion = "18 5 97 10";
                  pulseRate = "500";
                  pulseThreshold = "0.3";
               };
            };
            dashboardHud.add(dashboardHud.dmgBar);
         }
         if(%node != 1)
         {
            // passenger slot "dots"
            vOverheadHud.passengerHud = new GuiControl(vPassengerHud) {
               profile = "GuiDashBoxProfile";
               horizSizing = "right";
               vertSizing = "top";
               position = "0 0";
               extent = "101 101";
               minExtent = "8 8";
               visible = "1";

               new GuiBitmapCtrl(vPassenger0Slot) {
                  profile = "GuiDashBoxProfile";
                  horizSizing = "right";
                  vertSizing = "top";
                  position = "59 24";
                  extent = "10 10";
                  minExtent = "3 3";
                  visible = "0";
                  bitmap = "gui/hud_veh_seatdot";
               };
               new GuiBitmapCtrl(vPassenger1Slot) {
                  profile = "GuiDashBoxProfile";
                  horizSizing = "right";
                  vertSizing = "top";
                  position = "59 39";
                  extent = "10 10";
                  minExtent = "3 3";
                  visible = "0";
                  bitmap = "gui/hud_veh_seatdot";
               };
               new GuiBitmapCtrl(vPassenger2Slot) {
                  profile = "GuiDashBoxProfile";
                  horizSizing = "right";
                  vertSizing = "top";
                  position = "59 84";
                  extent = "10 10";
                  minExtent = "3 3";
                  visible = "0";
                  bitmap = "gui/hud_veh_seatdot";
               };
            };
            vOverheadHud.add(vOverheadHud.passengerHud);
         }
      case "HAPC" :
         if(%node == 0)
         {
            vOverheadHud.setBitmap("gui/hud_veh_icon_hapc");
            // add altitude box for flying vehicles
            dashboardHud.altBox = new HudBitmapCtrl(vAltitudeBox) {
               profile = "GuiDashBoxProfile";
               horizSizing = "right";
               vertSizing = "top";
               position = "371 56";
               extent = "68 22";
               minExtent = "8 8";
               bitmap = "gui/hud_veh_new_dashpiece_1";
               visible = "1";
               opacity = "0.8";

               new GuiTextCtrl(vAltitudeText) {
                  profile = "GuiDashTextProfile";
                  horizSizing = "right";
                  vertSizing = "top";
                  position = "19 5";
                  extent = "18 15";
                  minExtent = "8 8";
                  visible = "1";
                  text = "test";
               };
               new GuiTextCtrl(vAltitudeTxtLbl) {
                  profile = "GuiDashTextProfile";
                  horizSizing = "right";
                  vertSizing = "top";
                  position = "40 5";
                  extent = "12 15";
                  minExtent = "8 8";
                  visible = "1";
                  text = "M";
               };
            };
            dashboardHud.add(dashboardHud.altBox);
            updateVehicleAltitude();
            dashboardHud.updateAltitude();

         }
         else
         {
            // passenger hud
            dashboardHud.vehDiagram = new HudBitmapCtrl(vOverheadHud) {
               profile = "GuiDashBoxProfile";
               horizSizing = "right";
               vertSizing = "top";
               position = "256 0";
               extent = "128 128";
               minExtent = "8 8";
               visible = "1";
               bitmap = "gui/hud_veh_icon_hapc";
               opacity = "1.0";
            };
            dashboardHud.add(dashboardHud.vehDiagram);

            dashboardHud.nrgBar = new HudBitmapCtrl(vEnergyFrame) {
               profile = "GuiDashBoxProfile";
               horizSizing = "right";
               vertSizing = "top";
               position = "180 30";
               extent = "118 19";
               minExtent = "8 8";
               visible = "1";
               bitmap = "gui/hud_veh_new_dashpiece_5";
               flipVertical = true;
               opacity = "0.8";

               new HudEnergy(vEnergyBar) {
                  profile = "GuiDefaultProfile";
                  horizSizing = "right";
                  vertSizing = "top";
                  position = "0 0";
                  extent = "118 19";
                  minExtent = "8 8";
                  visible = "1";
                  fillColor = "0.353000 0.373000 0.933000 0.800000";
                  frameColor = "0.000000 1.000000 0.000000 1.000000";
                  autoCenter = "0";
                  autoResize = "0";
                  displayMounted = true;
                  bitmap = "gui/hud_veh_new_dashpiece_5";
                  verticalFill = false;
                  subRegion = "4 5 98 10";
                  pulseRate = "500";
                  pulseThreshold = "0.3";
               };
            };
            dashboardHud.add(dashboardHud.nrgBar);

            dashboardHud.dmgBar = new HudBitmapCtrl(vDamageFrame) {
               profile = "GuiDashBoxProfile";
               horizSizing = "right";
               vertSizing = "top";
               position = "342 30";
               extent = "118 19";
               minExtent = "8 8";
               visible = "1";
               bitmap = "gui/hud_veh_new_dashpiece_4";
               flipVertical = true;
               opacity = "0.8";

               new HudDamage(vDamageBar) {
                  profile = "GuiDefaultProfile";
                  horizSizing = "right";
                  vertSizing = "top";
                  position = "0 0";
                  extent = "118 19";
                  minExtent = "8 8";
                  visible = "1";
                  fillColor = "0.000000 1.0000 0.000000 0.800000";
                  frameColor = "0.000000 1.000000 0.000000 0.000000";
                  bitmap = "gui/hud_veh_new_dashpiece_4";
                  verticalFill = false;
                  displayMounted = true;
                  opacity = "0.8";
                  subRegion = "18 5 97 10";
                  pulseRate = "500";
                  pulseThreshold = "0.3";
               };
            };
            dashboardHud.add(dashboardHud.dmgBar);
         }
         // passenger slot "dots"
         vOverheadHud.passengerHud = new GuiControl(vPassengerHud) {
            profile = "GuiDashBoxProfile";
            horizSizing = "right";
            vertSizing = "top";
            position = "0 0";
            extent = "101 101";
            minExtent = "8 8";
            visible = "1";
            setFirstResponder = "0";
            modal = "1";
            helpTag = "0";

            new GuiBitmapCtrl(vPassenger0Slot) {
               profile = "GuiDashBoxProfile";
               horizSizing = "right";
               vertSizing = "top";
               position = "59 65";
               extent = "10 10";
               minExtent = "3 3";
               visible = "0";
               setFirstResponder = "0";
               modal = "1";
               bitmap = "gui/hud_veh_seatdot";
               wrap = "0";
            };
            new GuiBitmapCtrl(vPassenger1Slot) {
               profile = "GuiDashBoxProfile";
               horizSizing = "right";
               vertSizing = "top";
               position = "59 84";
               extent = "10 10";
               minExtent = "3 3";
               visible = "0";
               setFirstResponder = "0";
               modal = "1";
               bitmap = "gui/hud_veh_seatdot";
               wrap = "0";
            };
            new GuiBitmapCtrl(vPassenger2Slot) {
               profile = "GuiDashBoxProfile";
               horizSizing = "right";
               vertSizing = "top";
               position = "38 29";
               extent = "10 10";
               minExtent = "3 3";
               visible = "0";
               setFirstResponder = "0";
               modal = "1";
               bitmap = "gui/hud_veh_seatdot";
               wrap = "0";
            };
            new GuiBitmapCtrl(vPassenger3Slot) {
               profile = "GuiDashBoxProfile";
               horizSizing = "right";
               vertSizing = "top";
               position = "38 50";
               extent = "10 10";
               minExtent = "3 3";
               visible = "0";
               setFirstResponder = "0";
               modal = "1";
               bitmap = "gui/hud_veh_seatdot";
               wrap = "0";
            };
            new GuiBitmapCtrl(vPassenger4Slot) {
               profile = "GuiDashBoxProfile";
               horizSizing = "right";
               vertSizing = "top";
               position = "80 50";
               extent = "10 10";
               minExtent = "3 3";
               visible = "0";
               setFirstResponder = "0";
               modal = "1";
               bitmap = "gui/hud_veh_seatdot";
               wrap = "0";
            };
            new GuiBitmapCtrl(vPassenger5Slot) {
               profile = "GuiDashBoxProfile";
               horizSizing = "right";
               vertSizing = "top";
               position = "80 29";
               extent = "10 10";
               minExtent = "3 3";
               visible = "0";
               setFirstResponder = "0";
               modal = "1";
               bitmap = "gui/hud_veh_seatdot";
               wrap = "0";
            };
         };
         vOverheadHud.add(vOverheadHud.passengerHud);

      case "Assault" :
         if(%node == 1)
         {
            // turreteer hud
            dashboardHud.tHud = new GuiControl(turreteerHud) {
               profile = "GuiDefaultProfile";
               horizSizing = "center";
               vertSizing = "top";
               position = "225 70";
               extent = "240 50";
               minExtent = "8 8";
               visible = "1";

               new HudBitmapCtrl(vWeap1Hilite) {
                  profile = "GuiDashBoxProfile";
                  horizSizing = "right";
                  vertSizing = "bottom";
                  position = "40 11";
                  extent = "80 44";
                  minExtent = "8 8";
                  visible = "1";
                  bitmap = "gui/hud_veh_new_hilite_left";
                  opacity = "0.4";
               };
               new HudBitmapCtrl(vWeap2Hilite) {
                  profile = "GuiDashBoxProfile";
                  horizSizing = "right";
                  vertSizing = "bottom";
                  position = "118 11";
                  extent = "80 44";
                  minExtent = "8 8";
                  visible = "0";
                  bitmap = "gui/hud_veh_new_hilite_right";
                  opacity = "0.4";
               };

               new HudBitmapCtrl(turreteerFrame) {
                  profile = "GuiDashBoxProfile";
                  horizSizing = "center";
                  vertSizing = "bottom";
                  position = "20 8";
                  extent = "152 36";
                  minExtent = "8 8";
                  visible = "1";
                  bitmap = "gui/hud_veh_new_tankgunner_dash";
                  opacity = "0.8";

                  new HudBitmapCtrl(vWeaponOne) {
                     profile = "GuiDashBoxProfile";
                     horizSizing = "right";
                     vertSizing = "bottom";
                     position = "25 8";
                     extent = "25 25";
                     minExtent = "8 8";
                     visible = "1";
                     bitmap = "gui/hud_chaingun";
                  };

                  new HudBitmapCtrl(vWeaponTwo) {
                     profile = "GuiDashBoxProfile";
                     horizSizing = "right";
                     vertSizing = "bottom";
                     position = "99 8";
                     extent = "25 25";
                     minExtent = "8 8";
                     visible = "1";
                     bitmap = "gui/hud_mortor";
                  };
               };
            };
            dashboardHud.add(dashboardHud.tHud);

            dashboardHud.nrgBar = new HudBitmapCtrl(vEnergyFrame) {
               profile = "GuiDashBoxProfile";
               horizSizing = "right";
               vertSizing = "top";
               position = "134 95";
               extent = "118 19";
               minExtent = "8 8";
               visible = "1";
               flipVertical = true;
               bitmap = "gui/hud_veh_new_dashpiece_5";
               opacity = "0.8";

               new HudEnergy(vEnergyBar) {
                  profile = "GuiDefaultProfile";
                  horizSizing = "right";
                  vertSizing = "top";
                  position = "0 0";
                  extent = "118 19";
                  minExtent = "8 8";
                  visible = "1";
                  fillColor = "0.353000 0.373000 0.933000 0.800000";
                  frameColor = "0.000000 1.000000 0.000000 1.000000";
                  autoCenter = "0";
                  autoResize = "0";
                  displayMounted = true;
                  bitmap = "gui/hud_veh_new_dashpiece_5";
                  verticalFill = false;
                  subRegion = "4 5 98 5";
                  pulseRate = "500";
                  pulseThreshold = "0.3";
                  //modColor = "1.000000 0.500000 0.000000 1.000000";
               };
               new HudCapacitor(vCapBar) {
                  profile = "GuiDefaultProfile";
                  horizSizing = "right";
                  vertSizing = "top";
                  position = "0 8";
                  extent = "118 8";
                  minExtent = "8 8";
                  visible = "1";
                  fillColor = "1.000 0.729 0.301 0.800000";
                  frameColor = "0.000000 1.000000 0.000000 1.000000";
                  autoCenter = "0";
                  autoResize = "0";
                  displayMounted = true;
                  bitmap = "gui/hud_veh_new_dashpiece_5";
                  verticalFill = false;
                  subRegion = "4 5 98 5";
                  pulseRate = "500";
                  pulseThreshold = "0.3";
                  //modColor = "1.000000 0.500000 0.000000 1.000000";
               };
            };
            dashboardHud.add(dashboardHud.nrgBar);

            dashboardHud.dmgBar = new HudBitmapCtrl(vDamageFrame) {
               profile = "GuiDashBoxProfile";
               horizSizing = "right";
               vertSizing = "top";
               position = "390 95";
               extent = "118 19";
               minExtent = "8 8";
               visible = "1";
               flipVertical = true;
               bitmap = "gui/hud_veh_new_dashpiece_4";
               opacity = "0.8";

               new HudDamage(vDamageBar) {
                  profile = "GuiDefaultProfile";
                  horizSizing = "right";
                  vertSizing = "top";
                  position = "0 0";
                  extent = "118 19";
                  minExtent = "8 8";
                  visible = "1";
                  fillColor = "0.000000 1.0000 0.000000 0.800000";
                  frameColor = "0.000000 1.000000 0.000000 0.000000";
                  bitmap = "gui/hud_veh_new_dashpiece_4";
                  verticalFill = false;
                  displayMounted = true;
                  opacity = "0.8";
                  subRegion = "18 5 97 10";
                  pulseRate = "500";
                  pulseThreshold = "0.3";
                  //modColor = "1.000000 0.500000 0.000000 1.000000";
               };
            };
            dashboardHud.add(dashboardHud.dmgBar);

            $numVWeapons = 2;
            // add tank chaingun reticle
//            reticleHud.setBitmap("gui/hud_ret_tankchaingun");
//            reticleFrameHud.setVisible(false);
         }
         else
         {
            // node 0 == driver
            vOverheadHud.setBitmap("gui/hud_veh_icon_assault");
            // passenger slot "dots"
            vOverheadHud.passengerHud = new GuiControl(vPassengerHud) {
               profile = "GuiDashBoxProfile";
               horizSizing = "right";
               vertSizing = "top";
               position = "0 0";
               extent = "101 101";
               minExtent = "8 8";
               visible = "1";

               new GuiBitmapCtrl(vPassenger0Slot) {
                  profile = "GuiDashBoxProfile";
                  horizSizing = "right";
                  vertSizing = "top";
                  position = "64 30";
                  extent = "10 10";
                  minExtent = "3 3";
                  visible = "0";
                  bitmap = "gui/hud_veh_seatdot";
               };
               new GuiBitmapCtrl(vPassenger1Slot) {
                  profile = "GuiDashBoxProfile";
                  horizSizing = "right";
                  vertSizing = "top";
                  position = "53 53";
                  extent = "10 10";
                  minExtent = "3 3";
                  visible = "0";
                  bitmap = "gui/hud_veh_seatdot";
               };
            };
            vOverheadHud.add(vOverheadHud.passengerHud);
         }

      case "Hoverbike" :
         vOverheadHud.setBitmap("gui/hud_veh_icon_hoverbike");

      case "MPB" :
         vOverheadHud.setBitmap("gui/hud_veh_icon_mpb");

   }
   if(%node == 0)
      vDiagramHud.setVisible(true);
   else
      if(isObject(vDiagramHud))
         vDiagramHud.setVisible(false);
}

function clientCmdMissionStartPhase1(%seq, %missionName, %musicTrack)
{
   echo( "got client StartPhase1..." );

   // Reset the loading progress controls:
   LoadingProgress.setValue( 0 );
   DB_LoadingProgress.setValue( 0 );
   LoadingProgressTxt.setValue( "Downloading Datablocks..." );
   DB_LoadingProgressTxt.setValue( "Downloading Datablocks..." );

   clientCmdPlayMusic(%musicTrack);
   commandToServer('MissionStartPhase1Done', %seq);
   clientCmdResetCommandMap();
}

function ghostAlwaysStarted(%ghostCount)
{
   echo("Beginning scene object render thread for " @ %ghostCount @ " server objects....");

   LoadingProgress.setValue( 0.5 );
   DB_LoadingProgress.setValue( 0.5 );
   LoadingProgressTxt.setValue( "Loading Datablocks..." );
   DB_LoadingProgressTxt.setValue( "Loading Datablocks..." );
   Canvas.repaint();
   $ghostCount = %ghostCount;
   $ghostsRecvd = 0;
}

function ghostAlwaysObjectReceived()
{
   $ghostsRecvd++;
   %pct = ($ghostsRecvd / $ghostCount) / 2;

   if(%pct <= 0.5)
        %pct += 0.5;

   %lpct = mFloor(($ghostsRecvd / $ghostCount)*100);
   LoadingProgress.setValue( %pct );
   DB_LoadingProgress.setValue( %pct );
   LoadingProgressTxt.setValue( "Compiling active scene objects: "@$ghostsRecvd@"/"@$ghostCount@" ("@%lpct@"%)" );
   DB_LoadingProgressTxt.setValue( "Compiling active scene objects: "@$ghostsRecvd@"/"@$ghostCount@" ("@%lpct@"%)" );

    if(%pct == 1)
    {
        LoadingProgressTxt.setValue( "- Objects Loaded -" );
        DB_LoadingProgressTxt.setValue( "- Objects Loaded -" );
        LoadingProgress.setValue( 1 );
        DB_LoadingProgress.setValue( 1 );
    }

   Canvas.repaint();
}

function ClientReceivedDataBlock(%index, %total)
{
   %pct = (%index / %total) / 2;
   %dpct = mFloor((%index / %total)*100);
   LoadingProgress.setValue( %pct );
   LoadingProgress.setValue( %pct );
   LoadingProgressTxt.setValue( "Downloading Datablocks... "@%index@"/"@%total@" ("@%dpct@"%)" );
   DB_LoadingProgressTxt.setValue( "Downloading Datablocks... "@%index@"/"@%total@" ("@%dpct@"%)" );

    if(%pct == 0.5)
    {
        LoadingProgressTxt.setValue( "- Download Complete -" );
        DB_LoadingProgressTxt.setValue( "- Download Complete -" );
    }

   Canvas.repaint();
}

function clientCmdMissionStartPhase3(%seq, %missionName)
{
   $MSeq = %seq;

   //Reset Inventory Hud...
   if($Hud['inventoryScreen'] !$= "")
   {
      %favList = $Hud['inventoryScreen'].data[0, 1].type TAB $Hud['inventoryScreen'].data[0, 1].getValue();
      for ( %i = 1; %i < $Hud['inventoryScreen'].count; %i++ )
         if($Hud['inventoryScreen'].data[%i, 1].getValue() $= invalid)
            %favList = %favList TAB $Hud['inventoryScreen'].data[%i, 1].type TAB "EMPTY";
         else
            %favList = %favList TAB $Hud['inventoryScreen'].data[%i, 1].type TAB $Hud['inventoryScreen'].data[%i, 1].getValue();
      commandToServer( 'setClientFav', %favList );
   }
   else
      commandToServer( 'setClientFav', $pref::Favorite[$pref::FavCurrentSelect]);

   // needed?
   $MissionName = %missionName;
   //commandToServer( 'getScores' );

   // only show dialog if actually lights
   if(lightScene("sceneLightingComplete", $LaunchMode $= "SceneLight" ? "forceWritable" : ""))
   {
      error("beginning SceneLighting....");
      schedule(1, 0, "updateLightingProgress");
      $lightingMission = true;
      LoadingProgress.setValue( 0.0 );
      DB_LoadingProgress.setValue( 0.0 );
      LoadingProgressTxt.setValue( "Performing global illumination..." );
      DB_LoadingProgressTxt.setValue( "Performing global illumination..." );
      $missionLightStarted = true;
      Canvas.repaint();
   }
}

function sceneLightingComplete()
{
   LoadingProgress.setValue( 1 );
   DB_LoadingProgress.setValue( 1 );

   LoadingProgressTxt.setValue( "Loading sequence complete, starting renderer." );
   DB_LoadingProgressTxt.setValue( "Loading sequence complete, starting renderer." );

   echo("Scenelighting done...");
   $lightingMission = false;

   cleanUpHuds();

   if($LaunchMode $= "SceneLight")
   {
      quit();
      return;
   }

   clientCmdResetHud();
   commandToServer('SetVoiceInfo', $pref::Audio::voiceChannels, $pref::Audio::decodingMask, $pref::Audio::encodingLevel);
   commandToServer('EnableVehicleTeleport', $pref::Vehicle::pilotTeleport );
   commandToServer('MissionStartPhase3Done', $MSeq);
}

// Fury Clientside Graphing
//-----------------------------------------------------------------------------
function createColorGraph(%symbol, %maxLines, %pct, %color1, %color2, %color3, %noFillColor, %firstPct, %secondPct)
{
     %color = %pct > %firstPct ? (%pct > %secondPct ? %color3 : %color2) : %color1;

     // Could go higher, but mech uses 53 max
     if(%maxLines > 53)
          %maxLines = 53;

     %displayLines = mCeil(%pct * %maxLines);
     %leftLines = %maxLines - %displayLines;
     %level = %color;

     for(%i = 0; %i < %displayLines; %i++)
          %level = %level@%symbol;

     %level = %level@%noFillColor;

     for(%i = 0; %i < %leftLines; %i++)
          %level = %level@%symbol;

     %graph = "["@%level@"<color:42dbea>]";

     return %graph;
}

function createMonoGraph(%symbol, %maxLines, %pct, %color1, %color2, %color3, %noFillColor, %firstPct, %secondPct)
{
     %color = %pct > %firstPct ? (%pct > %secondPct ? %color3 : %color2) : %color1;

     // Could go higher, but mech uses 53 max
     if(%maxLines > 53)
          %maxLines = 53;

     %displayLines = mCeil(%pct * %maxLines);
     %leftLines = %maxLines - %displayLines;
     %level = %color;

     for(%i = 0; %i < %displayLines; %i++)
          %level = %level@%symbol;

     %level = %level@%noFillColor;

     for(%i = 0; %i < %leftLines; %i++)
          %level = %level@%symbol;

     %graph = "["@%level@"<color:42dbea>]";

     return %graph;
}

function createGraph(%symbol, %maxLines, %pct, %blankSymbol)
{
     // Could go higher, but mech uses 53 max
     if(%maxLines > 53)
          %maxLines = 53;

     %displayLines = mCeil(%pct * %maxLines);
     %leftLines = %maxLines - %displayLines;
     %level = "";

     for(%i = 0; %i < %displayLines; %i++)
          %level = %level@%symbol;

     for(%i = 0; %i < %leftLines; %i++)
          %level = %level@%blankSymbol;

     %graph = "["@%level@"]";

     return %graph;
}

function clientCmdGraphPilot(%hpPct, %shieldPct, %missiles, %weapon, %module, %headTracking, %stallState)
{
    %hpGraph = createColorGraph("|", 23, %hpPct, "<color:FF0000>", "<color:FFFF00>", "<color:00FF00>", "<color:777777>", 0.3, 0.6);
    %shGraph = createColorGraph("|", 23, %shieldPct, "<color:FF0000>", "<color:FFFF00>", "<color:00FF00>", "<color:777777>", 0.3, 0.6);

    if(%stallState)
        clientCmdBottomPrint("<just:left>Armor "@mFloor(%hpPct * 100)@"% "@%hpGraph@" <just:right>Shield "@mFloor(%shieldPct * 100)@"% "@%shGraph@"\n<just:center>Speed: "@mFloor(getControlObjectSpeed() * 3.6)@" KPH | Missiles: "@%missiles@" | Weapon: "@%weapon@"\n<color:FF6633>>> STALLING, INCREASE SPEED <<", 1, 3);
    else if(%headTracking)
        clientCmdBottomPrint("<just:left>Armor "@mFloor(%hpPct * 100)@"% "@%hpGraph@" <just:right>Shield "@mFloor(%shieldPct * 100)@"% "@%shGraph@"\n<just:center>Speed: "@mFloor(getControlObjectSpeed() * 3.6)@" KPH | Missiles: "@%missiles@" | Weapon [Mouselook]: "@%weapon@"\nModule: "@%module, 1, 3);
    else
        clientCmdBottomPrint("<just:left>Armor "@mFloor(%hpPct * 100)@"% "@%hpGraph@" <just:right>Shield "@mFloor(%shieldPct * 100)@"% "@%shGraph@"\n<just:center>Speed: "@mFloor(getControlObjectSpeed() * 3.6)@" KPH | Missiles: "@%missiles@" | Weapon: "@%weapon@"\nModule: "@%module, 1, 3);
}

function clientCmdGraphGunner(%hpPct, %shieldPct, %missiles, %weapon, %module)
{
    %hpGraph = createColorGraph("|", 23, %hpPct, "<color:FF0000>", "<color:FFFF00>", "<color:00FF00>", "<color:777777>", 0.3, 0.6);
    %shGraph = createColorGraph("|", 23, %shieldPct, "<color:FF0000>", "<color:FFFF00>", "<color:00FF00>", "<color:777777>", 0.3, 0.6);

    clientCmdBottomPrint("<just:left>Armor "@mFloor(%hpPct * 100)@"% "@%hpGraph@" <just:right>Shield "@mFloor(%shieldPct * 100)@"% "@%shGraph@"\n<just:center>Speed: "@mFloor(getControlObjectSpeed() * 3.6)@" KPH | Missiles: "@%missiles@" | Weapon: "@%weapon@"\nModule: "@%module, 1, 3);
}

function clientCmdGraphPassenger(%hpPct, %shieldPct, %module)
{
    %hpGraph = createColorGraph("|", 22, %hpPct, "<color:FF0000>", "<color:FFFF00>", "<color:00FF00>", "<color:777777>", 0.3, 0.6);
    %shGraph = createColorGraph("|", 22, %shieldPct, "<color:FF0000>", "<color:FFFF00>", "<color:00FF00>", "<color:777777>", 0.3, 0.6);

    clientCmdBottomPrint("<just:left>Armor "@mFloor(%hpPct * 100)@"% "@%hpGraph@" <just:right>Shield "@mFloor(%shieldPct * 100)@"% "@%shGraph@"\n<just:center>Speed: "@mFloor(getControlObjectSpeed() * 3.6)@" KPH | Module: "@%module, 1, 2);
}
