//**************************************************************
// HoverJeep
//**************************************************************

datablock FlyingVehicleData(HoverJeep) : UniversalDamageProfile
{
   spawnOffset = "0 0 2";

   catagory = "Vehicles";
   shapeFile = "hover_jeep.dts";
   multipassenger = true;
   computeCRC = true;

   weaponNode = 1;

   debrisShapeName = "vehicle_jeep_debris.dts";
   debris = ShapeDebris;
   renderWhenDestroyed = false;

   drag    = 0.2;
   density = 1.0;

   mountPose[0] = sitting;
   mountPose[1] = sitting;
   numMountPoints = 2;
   isProtectedMountPoint[0] = true;
   isProtectedMountPoint[1] = true;

   cameraDefaultFov = 90.0;
   cameraMinFov = 5.0;
   cameraMaxFov = 120.0;
   
   cameraMaxDist = 22;
   cameraOffset = 5;
   cameraLag = 1.0;
   explosion = LargeAirVehicleExplosion;
	explosionDamage = 10.0;
	explosionRadius = 15.0;

   maxDamage = 40.00;
   destroyedLevel = 40.00;

   isShielded = true;
   energyPerDamagePoint = 150;
   maxEnergy = 500;      // Afterburner and any energy weapon pool
   minDrag = 60;           // Linear Drag (eventually slows you down when not thrusting...constant drag)
   rotationalDrag = 1400;        // Angular Drag (dampens the drift after you stop moving the mouse...also tumble drag)
   rechargeRate = 95 / 32;

   // Auto stabilize speed
   maxAutoSpeed = 15;       // Autostabilizer kicks in when less than this speed. (meters/second)
   autoAngularForce = 1500;      // Angular stabilizer force (this force levels you out when autostabilizer kicks in)
   autoLinearForce = 300;        // Linear stabilzer force (this slows you down when autostabilizer kicks in)
   autoInputDamping = 0.95;      // Dampen control input so you don't whack out at very slow speeds
   
   // Maneuvering
   maxSteerinAngle = $g_PI * 0.9;    // Placeholder value for now, T2 v12 engine actually uses this value and it defaults to pi
   maxSteeringAngle = $g_PI * 0.9;    // Max radians you can rotate the wheel. Smaller number is more maneuverable. Defaults to pi
   horizontalSurfaceForce = 5;   // Horizontal center "wing" (provides "bite" into the wind for climbing/diving and turning)
   verticalSurfaceForce = 7;     // Vertical center "wing" (controls side slip. lower numbers make MORE slide.)
   maneuveringForce = 5600;      // Horizontal jets (W,S,D,A key thrust) // z0dd - ZOD, 9/8/02. Was 4700
   steeringForce = 1200;         // Steering jets (force applied when you move the mouse)
   steeringRollForce = 300;      // Steering jets (how much you heel over when you turn)
   rollForce = 8;                // Auto-roll (self-correction to right you after you roll/invert)
   hoverHeight = 5;        // Height off the ground at rest
   createHoverHeight = 3;  // Height off the ground when created
   maxForwardSpeed = 166.667;  // speed in which forward thrust force is no longer applied (meters/second) // z0dd - ZOD, 9/8/02. Was 85

   // Turbo Jet
   jetForce = 8800;      // Afterburner thrust (this is in addition to normal thrust) // z0dd - ZOD, 9/8/02. Was 3000
   minJetEnergy = 10.0;     // Afterburner can't be used if below this threshhold.
   jetEnergyDrain = 160 / 32;       // Energy use of the afterburners (low number is less drain...can be fractional)
   vertThrustMultiple = 2.5; // z0dd - ZOD, 9/8/02. Was 3.0

   // Reduction of impulse effects on sensitive shapes
   shapeMaxForce = 1500.0;
   
   dustEmitter = LargeVehicleLiftoffDustEmitter;
   triggerDustHeight = 4.0;
   dustHeight = 2.0;

   damageEmitter[0] = SmallLightDamageSmoke; //LightDamageSmoke;
   damageEmitter[1] = OnFireEmitter;
   damageEmitter[2] = DamageBubbles;
   damageEmitterOffset[0] = "3.0 -3.0 0.0";
   damageEmitterOffset[1] = "-3.0 -3.0 0.0";
   damageLevelTolerance[0] = 0.6;
   damageLevelTolerance[1] = 0.8;
   numDmgEmitterAreas = 2;

   // Rigid body
   mass = 320;        // Mass of the vehicle
   bodyFriction = 0;     // Don't mess with this.
   bodyRestitution = 0.5;   // When you hit the ground, how much you rebound. (between 0 and 1)
   minRollSpeed = 0;     // Don't mess with this.
   softImpactSpeed = 20;       // Sound hooks. This is the soft hit.
   hardImpactSpeed = 25;    // Sound hooks. This is the hard hit.

   // Ground Impact Damage (uses DamageType::Ground)
   minImpactSpeed = 28;      // If hit ground at speed above this then it's an impact. Meters/second
   speedDamageScale = 1.0;

   // Object Impact Damage (uses DamageType::Impact)
   collDamageThresholdVel = 27.78;
   collDamageMultiplier   = 1.0;

   //
   minTrailSpeed = 24;      // The speed your contrail shows up at.
   trailEmitter = ContrailEmitter;
   forwardJetEmitter = BlueJetEmitter;
   downJetEmitter = BlueJetEmitter;

   //
   jetSound = Engine1ThrustSound;
   engineSound = HoverJeepEngineSound;
   softImpactSound = SoftImpactSound;
   hardImpactSound = HardImpactSound;
   //wheelImpactSound = WheelImpactSound;

   //
   softSplashSoundVelocity = 15.0; 
   mediumSplashSoundVelocity = 20.0;   
   hardSplashSoundVelocity = 30.0;   
   exitSplashSoundVelocity = 10.0;
   
   exitingWater      = VehicleExitWaterHardSound;
   impactWaterEasy   = VehicleImpactWaterSoftSound;
   impactWaterMedium = VehicleImpactWaterMediumSound;
   impactWaterHard   = VehicleImpactWaterHardSound;
   waterWakeSound    = VehicleWakeHardSplashSound; 
  
   minMountDist = 4;

   splashEmitter[0] = VehicleFoamDropletsEmitter;
   splashEmitter[1] = VehicleFoamEmitter;

   shieldImpact = VehicleShieldImpact;

   cmdCategory = "Tactical";
   cmdIcon = CMDFlyingBomberIcon;
   cmdMiniIconName = "commander/MiniIcons/com_bomber_grey";
   targetNameTag = 'Hummingbird';
   targetTypeTag = 'Hover Jeep';
   sensorData = VehiclePulseSensor;
   sensorRadius = VehiclePulseSensor.detectRadius; // z0dd - ZOD, 4/25/02. Allows sensor to be shown on CC

   checkRadius = 7.1895;
   observeParameters = "1 10 10";
   shieldEffectScale = "0.75 0.975 0.375";
   showPilotInfo = 1;
   
   physicsType = $VehiclePhysics::ForwardsOnly;

   checkMinVelocity = 25;
   checkMinHeight = 30;
   checkMinReverseSpeed = 10;
};

datablock ShapeBaseImageData(JeepPilotTurret) : EngineAPEImage
{
   shapeFile = "turret_belly_base.dts";
   offset = "1.0 4.8 -1.6";
   rotation = "0 0 1 0"; //degreesToRotation("0 -45 180"); //"0 1 0 -45"; // 90

   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
};

datablock TurretData(HoverJeepTurret) : UniversalDamageProfile
{
   className      = VehicleTurret;
   catagory       = "Turrets";
   shapeFile      = "jeep_turret.dts";
   preload        = true;

   mass           = 1.0;  // Not really relevant

   maxEnergy               = 1;
   maxDamage               = HoverJeep.maxDamage;
   destroyedLevel          = HoverJeep.destroyedLevel;
   repairRate              = 0;

   // capacitor
   maxCapacitorEnergy      = 1000;
   capacitorRechargeRate   = 25.0;

   thetaMin = 10;
   thetaMax = 140;

   inheritEnergyFromMount = true;
   firstPersonOnly = true;
   useEyePoint = true;
   numWeapons = 2;

   cameraDefaultFov = 90.0;
   cameraMinFov = 5.0;
   cameraMaxFov = 120.0;

   targetNameTag = 'Hover Jeep';
   targetTypeTag = 'Turret';
};

function HoverJeep::onAdd(%this, %obj)
{
    Parent::onAdd(%this, %obj);

    %obj.mountImage(JeepPilotTurret, 4);
    %obj.schedule(5500, "playThread", $ActivateThread, "activate");
   
    %main = %this.installTurret(%obj, HoverJeepTurret, 2);
    %main.mountImage(GenericParamImage, 0);
}

function HoverJeep::playerMounted(%data, %obj, %player, %node)
{
   if(%node == 0)
   {
      // pilot position
	   commandToClient(%player.client, 'setHudMode', 'Pilot', "HAPC", %node);

       %data.onPilotSeated(%obj, %player);
   }
   else if(%node == 1)
      %data.onGunnerSeated(%obj, %player, 2);
   else
	   commandToClient(%player.client, 'setHudMode', 'Passenger', "HAPC", %node);

   %data.updatePassengerString(%obj);

   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}
