//**************************************************************
// Shrike X
//**************************************************************

datablock FlyingVehicleData(ShrikeX) : UniversalDamageProfile
{
   spawnOffset = "0 0 2";

   catagory = "Vehicles";
   shapeFile = "shrikemx.dts";
   multipassenger = false;
   computeCRC = true;

   debrisShapeName = "vehicle_air_scout_debris.dts";
   debris = ShapeDebris;
   renderWhenDestroyed = false;

   drag    = 0.15;
   density = 1.0;

   mountPose[0] = sitting;
   numMountPoints = 1;  
   isProtectedMountPoint[0] = true;
   cameraMaxDist = 15;
   cameraOffset = 2.5;
   cameraLag = 0.9;
   explosion = LargeAirVehicleExplosion;
	explosionDamage = 5.625;
	explosionRadius = 10.0;

   maxDamage = 22.50;
   destroyedLevel = 22.50;

   isShielded = false;
   energyPerDamagePoint = 160;
   maxEnergy = 800;      // Afterburner and any energy weapon pool
   rechargeRate = 125 / 32;

   minDrag = 30;           // Linear Drag (eventually slows you down when not thrusting...constant drag)
   rotationalDrag = 900;        // Anguler Drag (dampens the drift after you stop moving the mouse...also tumble drag)

   maxAutoSpeed = 26;       // Autostabilizer kicks in when less than this speed. (meters/second)
   autoAngularForce = 400;       // Angular stabilizer force (this force levels you out when autostabilizer kicks in)
   autoLinearForce = 300;        // Linear stabilzer force (this slows you down when autostabilizer kicks in)
   autoInputDamping = 0.95;      // Dampen control input so you don't` whack out at very slow speeds
   
   // Maneuvering
   maxSteerinAngle = $g_PI * 0.9;    // Placeholder value for now, T2 v12 engine actually uses this value and it defaults to pi
   maxSteeringAngle = $g_PI * 0.9;    // Max radians you can rotate the wheel. Smaller number is more maneuverable. Defaults to pi
   horizontalSurfaceForce = 6;   // Horizontal center "wing" (provides "bite" into the wind for climbing/diving and turning)
   verticalSurfaceForce = 4;     // Vertical center "wing" (controls side slip. lower numbers make MORE slide.)
   maneuveringForce = 2925;      // Horizontal jets (W,S,D,A key thrust)
   steeringForce = 1200;         // Steering jets (force applied when you move the mouse)
   steeringRollForce = 400;      // Steering jets (how much you heel over when you turn)
   rollForce = 4;                // Auto-roll (self-correction to right you after you roll/invert)
   hoverHeight = 5;        // Height off the ground at rest
   createHoverHeight = 3;  // Height off the ground when created
   maxForwardSpeed = 208.334;  // speed in which forward thrust force is no longer applied (meters/second)

   // Turbo Jet
   jetForce = 6000;      // Afterburner thrust (this is in addition to normal thrust)
   minJetEnergy = 40;     // Afterburner can't be used if below this threshhold.
   jetEnergyDrain = 325 / 32;       // Energy use of the afterburners (low number is less drain...can be fractional)                                                                                                                                                                                                                                                                                          // Auto stabilize speed
   vertThrustMultiple = 0.5; // z0dd - ZOD, 9/8/02. Was 3.0

   // Reduction of impulse effects on sensitive shapes
   impulseDampeningFactor = 7.0;

   // Rigid body
   mass = 175;        // Mass of the vehicle // 430
   bodyFriction = 0;     // Don't mess with this.
   bodyRestitution = 0.5;   // When you hit the ground, how much you rebound. (between 0 and 1)
   minRollSpeed = 0;     // Don't mess with this.
   softImpactSpeed = 14;       // Sound hooks. This is the soft hit.
   hardImpactSpeed = 25;    // Sound hooks. This is the hard hit.

   // Ground Impact Damage (uses DamageType::Ground)
   minImpactSpeed = 28;      // If hit ground at speed above this then it's an impact. Meters/second
   speedDamageScale = 1.0;

   // Object Impact Damage (uses DamageType::Impact)
   collDamageThresholdVel = 27.78;
   collDamageMultiplier   = 1.0;

   //
   minTrailSpeed = 28;      // The speed your contrail shows up at.
   trailEmitter = ContrailEmitter;
   forwardJetEmitter = RedJetEmitter;
   downJetEmitter = RedJetEmitter;

   //
   jetSound = ScoutFlyerThrustSound;
   engineSound = ScoutFlyerEngineSound;
   softImpactSound = SoftImpactSound;
   hardImpactSound = HardImpactSound;
   //wheelImpactSound = WheelImpactSound;
   
   //
   softSplashSoundVelocity = 10.0; 
   mediumSplashSoundVelocity = 15.0;   
   hardSplashSoundVelocity = 20.0;   
   exitSplashSoundVelocity = 10.0;
   
   exitingWater      = VehicleExitWaterMediumSound;
   impactWaterEasy   = VehicleImpactWaterSoftSound;
   impactWaterMedium = VehicleImpactWaterMediumSound;
   impactWaterHard   = VehicleImpactWaterMediumSound;
   waterWakeSound    = VehicleWakeMediumSplashSound; 
    
   dustEmitter = VehicleLiftoffDustEmitter;
   triggerDustHeight = 4.0;
   dustHeight = 1.0;

   damageEmitter[0] = SmallLightDamageSmoke;
   damageEmitter[1] = OnFireEmitter;
   damageEmitter[2] = DamageBubbles;
   damageEmitterOffset[0] = "0.0 -3.0 0.0 ";
   damageLevelTolerance[0] = 0.6;
   damageLevelTolerance[1] = 0.8;
   numDmgEmitterAreas = 1;

   //
   max[chaingunAmmo] = 1000;

   minMountDist = 4;

   splashEmitter[0] = VehicleFoamDropletsEmitter;
   splashEmitter[1] = VehicleFoamEmitter;

   shieldImpact = VehicleShieldImpact;

   cmdCategory = "Tactical";
   cmdIcon = CMDFlyingScoutIcon;
   cmdMiniIconName = "commander/MiniIcons/com_scout_grey";
   targetNameTag = 'Shrike X';
   targetTypeTag = 'Prototype Fighter';
   sensorData = VehiclePulseSensor;
   sensorRadius = VehiclePulseSensor.detectRadius;
   sensorColor = "0 212 45";
   
   checkRadius = 5.5;
   observeParameters = "1 10 10";

//   runningLight[0] = ShrikeLight1;
//   runningLight[1] = ShrikeLight2;

   shieldEffectScale = "0.937 1.125 0.60";
   
   physicsType = $VehiclePhysics::ConstantMomentum;

   checkMinVelocity = 30;
   checkMinHeight = 15;
   checkMinReverseSpeed = 10;
};

function ShrikeX::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);

   %obj.schedule(5500, "playThread", $ActivateThread, "activate");
}

function ShrikeX::playerMounted(%data, %obj, %player, %node)
{
   commandToClient(%player.client, 'setHudMode', 'Pilot', "Shrike", %node);

   %data.onPilotSeated(%obj, %player);
}
