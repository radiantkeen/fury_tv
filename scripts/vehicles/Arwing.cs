//**************************************************************
// Arwing
//**************************************************************

datablock FlyingVehicleData(Arwing) : UniversalDamageProfile
{
   spawnOffset = "0 0 2";

   catagory = "Vehicles";
   shapeFile = "Arwing.dts";
   multipassenger = false;

   debrisShapeName = "vehicle_grav_scout_debris.dts";
   debris = ShapeDebris;
   renderWhenDestroyed = false;

   drag    = 0.15;
   density = 1.0;

   lightOnly = true;
   mountPose[0] = sitting; //scoutRoot;
   cameraMaxDist = 7;
   cameraOffset = 1.1;
   cameraLag = 0.8;
   numMountPoints = 1;
   isProtectedMountPoint[0] = true;
   explosion = VehicleExplosion;
	explosionDamage = 1.25;
	explosionRadius = 10.0;

   maxDamage = 5.0;
   destroyedLevel = 5.0;

   isShielded = false;
   energyPerDamagePoint = 55;
   maxEnergy = 300;      // Afterburner and any energy weapon pool
   minDrag = 30;           // Linear Drag (eventually slows you down when not thrusting...constant drag)
   rotationalDrag = 900;        // Anguler Drag (dampens the drift after you stop moving the mouse...also tumble drag)
   rechargeRate = 80 / 32;

   maxAutoSpeed = 8;       // Autostabilizer kicks in when less than this speed. (meters/second)
   autoAngularForce = 360;       // Angular stabilizer force (this force levels you out when autostabilizer kicks in)
//   autoAngularForce = 678;       // Angular stabilizer force (this force levels you out when autostabilizer kicks in)
   autoLinearForce = 150;        // Linear stabilzer force (this slows you down when autostabilizer kicks in)
   autoInputDamping = 0.9;      // Dampen control input so you don't` whack out at very slow speeds

   // Maneuvering
   maxSteeringAngle = 2.5;    // Max radiens you can rotate the wheel. Smaller number is more maneuverable.
   horizontalSurfaceForce = 8;   // Horizontal center "wing" (provides "bite" into the wind for climbing/diving and turning)
   verticalSurfaceForce = 6;     // Vertical center "wing" (controls side slip. lower numbers make MORE slide.)
   maneuveringForce = 3350;      // Horizontal jets (W,S,D,A key thrust)
   steeringForce = 1900;         // Steering jets (force applied when you move the mouse)
   steeringRollForce = 2400;      // Steering jets (how much you heel over when you turn)
   rollForce = 12;                // Auto-roll (self-correction to right you after you roll/invert)
   hoverHeight = 4;        // Height off the ground at rest
   createHoverHeight = 5;  // Height off the ground when created
   maxForwardSpeed = 250;

   // Turbo Jet
   jetForce = 7000;      // Afterburner thrust (this is in addition to normal thrust)
   minJetEnergy = 10;     // Afterburner can't be used if below this threshhold.
   jetEnergyDrain = 145 / 32;       // Energy use of the afterburners (low number is less drain...can be fractional)                                                                                                                                                                                                                                                                                          // Auto stabilize speed
   vertThrustMultiple = 0.5;
   
   // Reduction of impulse effects on sensitive shapes
   impulseDampeningFactor = 5.0;

   // Rigid body
   mass = 100;        // Mass of the vehicle
   bodyFriction = 0;     // Don't mess with this.
   bodyRestitution = 0.5;   // When you hit the ground, how much you rebound. (between 0 and 1)
   minRollSpeed = 0;     // Don't mess with this.
   softImpactSpeed = 40;       // Sound hooks. This is the soft hit.
   hardImpactSpeed = 80;    // Sound hooks. This is the hard hit.

   // Ground Impact Damage (uses DamageType::Ground)
   minImpactSpeed = 20;      // If hit ground at speed above this then it's an impact. Meters/second
   speedDamageScale = 0.01;

   // Object Impact Damage (uses DamageType::Impact)
   collDamageThresholdVel = 20.0;
   collDamageMultiplier   = 0.0446;

   //
   minTrailSpeed = 25;      // The speed your contrail shows up at.
   trailEmitter = ContrailEmitter;
   forwardJetEmitter = WildcatJetEmitter;
   downJetEmitter = WildcatJetEmitter;

   //
   jetSound         = Engine2ThrustSound;
   engineSound      = InterceptorEngineSound;
   softImpactSound = SoftImpactSound;
   hardImpactSound = HardImpactSound;
   //wheelImpactSound = WheelImpactSound;

   //
   softSplashSoundVelocity = 10.0;
   mediumSplashSoundVelocity = 15.0;
   hardSplashSoundVelocity = 20.0;
   exitSplashSoundVelocity = 10.0;

   exitingWater      = VehicleExitWaterMediumSound;
   impactWaterEasy   = VehicleImpactWaterSoftSound;
   impactWaterMedium = VehicleImpactWaterMediumSound;
   impactWaterHard   = VehicleImpactWaterMediumSound;
   waterWakeSound    = VehicleWakeMediumSplashSound;

   dustEmitter = VehicleLiftoffDustEmitter;
   triggerDustHeight = 4.0;
   dustHeight = 1.0;

   damageEmitter[0] = SmallLightDamageSmoke;
   damageEmitter[1] = OnFireEmitter;
   damageEmitter[2] = DamageBubbles;
   damageEmitterOffset[0] = "0.0 -1.5 0.5";
   damageLevelTolerance[0] = 0.6;
   damageLevelTolerance[1] = 0.8;
   numDmgEmitterAreas = 1;

   minMountDist = 4;

   splashEmitter[0] = VehicleFoamDropletsEmitter;
   splashEmitter[1] = VehicleFoamEmitter;

   shieldImpact = VehicleShieldImpact;

   sensorColor = "8 212 0";
   sensorData = InterceptorPulseSensor;
   sensorRadius = InterceptorPulseSensor.detectRadius;
   cmdCategory = "Tactical";
   cmdIcon = CMDHoverScoutIcon;
   cmdMiniIconName = "commander/MiniIcons/com_landscout_grey";
   targetNameTag = 'Arwing';
   targetTypeTag = 'Interdictor';

   shieldEffectScale = "0.937 1.125 0.60";

   checkMinVelocity = 35;
   checkMinHeight = 10;
   checkMinReverseSpeed = 15;
};

function Arwing::playerMounted(%data, %obj, %player, %node)
{
   commandToClient(%player.client, 'setHudMode', 'Pilot', "Shrike", %node);

   %data.onPilotSeated(%obj, %player);
}
