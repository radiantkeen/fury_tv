// VehiclePart System
function VehiclePart::__construct(%this)
{
    VehiclePart.Version = 1.0;
}

function VehiclePart::__destruct(%this)
{
   // Thou shalt not spam
}

if(!isObject(VehiclePart))
   System.addClass(VehiclePart);

// Classes
$VehicleClass::Flying               = 0;
$VehicleClass::Hovering             = 1;
$VehicleClass::Ground               = 2;

// Vehicle Part Types
$VehiclePartType::Armor             = 0; // not used in FuryTV
$VehiclePartType::Module            = 1;
$VehiclePartType::Shield            = 2;
$VehiclePartType::Weapon            = 3;
$VehiclePartType::Weapon1           = 3;
$VehiclePartType::Weapon2           = 4;
$VehiclePartType::Weapon3           = 5;
$VehiclePartType::Weapon4           = 6;
$VehiclePartType::Weapon5           = 7;
$VehiclePartType::Weapon6           = 8;

// Sizes
$VHardpointSize::Placeholder        = 0;
$VHardpointSize::Missile            = 1;
$VHardpointSize::Gun                = 2;
$VHardpointSize::Cannon             = 3;
$VHardpointSize::Siege              = 4;
$VHardpointSize::Internal           = 5;

// Missile sizes/sound bindings
$VHardpointSize::Small              = 6;
$VHardpointSize::Medium             = 7;
$VHardpointSize::Large              = 8;
$VHardpointSize::XLarge             = 9;

// Types
$VHardpointType::Placeholder        = 0;
$VHardpointType::Energy             = 1 << 0;
$VHardpointType::Ballistic          = 1 << 1;
$VHardpointType::Pod                = 1 << 2;
$VHardpointType::Bay                = 1 << 3;
$VHardpointType::Missile            = 1 << 4;
$VHardpointType::Standard           = 1 << 5;
$VHardpointType::Hybrid             = $VHardpointType::Standard | $VHardpointType::Energy | $VHardpointType::Ballistic; // vehicles use Hybrid, weapons that require both use Standard
$VHardpointType::AmmoOnly           = $VHardpointType::Ballistic | $VHardpointType::Pod;
$VHardpointType::Omni               = $VHardpointType::Energy | $VHardpointType::Ballistic | $VHardpointType::Standard | $VHardpointType::Pod;
$VHardpointType::Anything           = -1;

// Trigger Types
$VMTrigger::Push = 0;
$VMTrigger::Toggle = 1;
$VMTrigger::Passive = 2;

// Vehicle Types
$VehicleSize::Interceptor = 1;
$VehicleSize::Fighter = 2;
$VehicleSize::Gunship = 3;
$VehicleSize::Battleship = 4;
$VehicleSize::Carrier = 5;

// AI Defs
// These define the type of vehicle for additional logic - bitmask
$AIVehicleRole::None                = 1 << 0;
$AIVehicleRole::SingleSeater        = 1 << 1; // Optimize usage for only one pilot and/or pretend to escort
$AIVehicleRole::HasTurrets          = 1 << 2; // Has turrets, conserve energy so they can still fire if possible
$AIVehicleRole::Bomber              = 1 << 3; // Try to attack unshielded targets
$AIVehicleRole::Scout               = 1 << 4; // Vehicle is very fast or has a large radar, intended for finding other vehicles
$AIVehicleRole::Escort              = 1 << 5; // Primary role is to defend larger (Support/Carrier) vehicles
$AIVehicleRole::Support             = 1 << 6; // Slower moving battleships, potentially command/call for escorts
$AIVehicleRole::Carrier             = 1 << 7; // Primary complement is players instead of turrets
$AIVehicleRole::Fighter             = 1 << 8; // Seek out targets to attack more than defending, more aggressive and less worried about fleeing on low damage

// These define everything the specific seat does/can do
$SeatProperties::Default            = 1 << 0;
$SeatProperties::Pilot              = 1 << 1; // This is the pilot seat, controls vehicle module, restocking, and movement
$SeatProperties::Gunner             = 1 << 2; // Shoot at the enemy
$SeatProperties::ControlsTurret     = 1 << 3; // This seat controls a turret, does the AI turn off here?
$SeatProperties::Tailgunner         = 1 << 4; // This seat offers optimal viewing angles for the top half of the vehicle, keep an eye on the rear of the vehicle occasionally
$SeatProperties::Underbelly         = 1 << 5; // This seat offers optimal viewing angles of the bottom half of the vehicle, useful for bombing
$SeatProperties::LaunchMissiles     = 1 << 6; // This seat has a missile lock indicator - pilot with this flag will use the grenade key, while gunners will use the missile bay attached to the turret

// Vehicle Firegroups
//----------------------------------------------------------------------------

$VehicleMaxFireGroups = 0;

$VehicleFiregroup::Alpha = 1 << $VehicleMaxFireGroups;
$VehicleFiregroupMask[$VehicleMaxFireGroups] = $VehicleFiregroup::Alpha;
$VehicleFiregroupID[$VehicleFiregroup::Alpha] = $VehicleMaxFireGroups;
$VehicleFiregroupName[$VehicleMaxFireGroups] = "Alpha";
$VehicleMaxFireGroups++;

$VehicleFiregroup::Beta = 1 << $VehicleMaxFireGroups;
$VehicleFiregroupMask[$VehicleMaxFireGroups] = $VehicleFiregroup::Beta;
$VehicleFiregroupID[$VehicleFiregroup::Beta] = $VehicleMaxFireGroups;
$VehicleFiregroupName[$VehicleMaxFireGroups] = "Beta";
$VehicleMaxFireGroups++;

$VehicleFiregroup::Gamma = 1 << $VehicleMaxFireGroups;
$VehicleFiregroupMask[$VehicleMaxFireGroups] = $VehicleFiregroup::Gamma;
$VehicleFiregroupID[$VehicleFiregroup::Gamma] = $VehicleMaxFireGroups;
$VehicleFiregroupName[$VehicleMaxFireGroups] = "Gamma";
$VehicleMaxFireGroups++;

//------------------------------------------------------------------------------
// Vehicles
$VehicleCount = 0;

// Skycutter Hoverbike
// -----------------------------------------------------------------------------
$VehicleListID["ScoutVehicle"] = $VehicleCount;
$VehicleList[$VehicleCount] = "ScoutVehicle";
$VehicleListData[$VehicleCount, "block"] = "ScoutVehicle";
$VehicleListData[$VehicleCount, "name"] = "Skycutter Turbobike";
$VehicleListData[$VehicleCount, "desc"] = "The fastest and smallest target in the skies thanks to it's light weight and raw thrust";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "role"] = $AIVehicleRole::SingleSeater | $AIVehicleRole::Scout;
$VehicleListData[$VehicleCount, "group"] = $VehicleGroup::Interceptors;
$VehicleListData[$VehicleCount, "hardpoints"] = 1;
$VehicleListData[$VehicleCount, "suffixcode"] = "SKY";
$VehicleList::Skycutter = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Skycutter = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[G] Nose Mount";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 1;

$VehicleHardpoints[$VehicleCount, 0, "position"] = "0 1.75 -0.35";
$VehicleHardpoints[$VehicleCount, 0, "rotation"] = "1 0 0 0";

$VehicleMax["ScoutVehicle"] = 3;

$VehicleSeatProperties[$VehicleCount, 0] = $SeatProperties::Pilot | $SeatProperties::Gunner;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "VShieldNone";
$DefaultVehicleWep[$VehicleCount, 0] = "GVulcan";
$VehicleCount++;

// Airhawk
// -----------------------------------------------------------------------------
$VehicleListID["Airhawk"] = $VehicleCount;
$VehicleList[$VehicleCount] = "Airhawk";
$VehicleListData[$VehicleCount, "block"] = "Airhawk";
$VehicleListData[$VehicleCount, "name"] = "Airhawk Jetbike";
$VehicleListData[$VehicleCount, "desc"] = "Trading some raw speed for more firepower, the hawk will swoop in for the kill";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "role"] = $AIVehicleRole::SingleSeater | $AIVehicleRole::Scout;
$VehicleListData[$VehicleCount, "group"] = $VehicleGroup::Interceptors;
$VehicleListData[$VehicleCount, "hardpoints"] = 2;
$VehicleListData[$VehicleCount, "suffixcode"] = "AIR";
$VehicleList::Airhawk = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Airhawk = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[G] Dual Nose Mounts";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Hybrid;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "mountSuffix"] = "NUL";
$VehicleHardpoints[$VehicleCount, 1, "name"] = "[M] Underbelly Missile";
$VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 7;
$VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 1, "mountSuffix"] = "NUL";

$VehicleHardpoints[$VehicleCount, 2, "position"] = "0.65 1.1 -0.41";
$VehicleHardpoints[$VehicleCount, 2, "rotation"] = "1 0 0 0";
$VehicleHardpoints[$VehicleCount, 3, "position"] = "-0.65 1.1 -0.41";
$VehicleHardpoints[$VehicleCount, 3, "rotation"] = "1 0 0 0";
$VehicleHardpoints[$VehicleCount, 7, "position"] = "0 0 -1.125";
$VehicleHardpoints[$VehicleCount, 7, "rotation"] = "1 0 0 0";

$VehicleMax["Airhawk"] = 3;

$VehicleSeatProperties[$VehicleCount, 0] = $SeatProperties::Pilot | $SeatProperties::LaunchMissiles | $SeatProperties::Gunner;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "VShieldNone";
$DefaultVehicleWep[$VehicleCount, 0] = "GBlaster";
$DefaultVehicleWep[$VehicleCount, 1] = "MBreacher";
$VehicleCount++;

// Wildcat
// -----------------------------------------------------------------------------
$VehicleListID["Wildcat"] = $VehicleCount;
$VehicleList[$VehicleCount] = "Wildcat";
$VehicleListData[$VehicleCount, "block"] = "Wildcat";
$VehicleListData[$VehicleCount, "name"] = "Wildcat Hoverbike";
$VehicleListData[$VehicleCount, "desc"] = "Well rounded on offense and defense, this is the only bike with a shield emitter";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "role"] = $AIVehicleRole::SingleSeater | $AIVehicleRole::Scout;
$VehicleListData[$VehicleCount, "group"] = $VehicleGroup::Interceptors;
$VehicleListData[$VehicleCount, "hardpoints"] = 3;
$VehicleListData[$VehicleCount, "suffixcode"] = "AIR"; // WIL // is actually WIL but shares mounts with AIR
$VehicleList::Wildcat = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Wildcat = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[G] Dual Nose Mounts";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Hybrid;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "mountSuffix"] = "NUL";
$VehicleHardpoints[$VehicleCount, 1, "name"] = "[M] Right Side Missile";
$VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 6;
$VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 1, "mountSuffix"] = "NUL";
$VehicleHardpoints[$VehicleCount, 2, "name"] = "[M] Left Side Missile";
$VehicleHardpoints[$VehicleCount, 2, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 2, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 2, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 2, "imageStart"] = 7;
$VehicleHardpoints[$VehicleCount, 2, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 2, "mountSuffix"] = "NUL";

$VehicleHardpoints[$VehicleCount, 2, "position"] = "2.5 3.9 -1.25";
$VehicleHardpoints[$VehicleCount, 2, "rotation"] = "1 0 0 0";
$VehicleHardpoints[$VehicleCount, 3, "position"] = "-2.5 3.9 -1.25";
$VehicleHardpoints[$VehicleCount, 3, "rotation"] = "1 0 0 0";
$VehicleHardpoints[$VehicleCount, 6, "position"] = "0.75 0 -0.65";
$VehicleHardpoints[$VehicleCount, 6, "rotation"] = "1 0 0 0";
$VehicleHardpoints[$VehicleCount, 7, "position"] = "-0.75 0 -0.65";
$VehicleHardpoints[$VehicleCount, 7, "rotation"] = "1 0 0 0";

$VehicleMax["Wildcat"] = 3;

$VehicleSeatProperties[$VehicleCount, 0] = $SeatProperties::Pilot | $SeatProperties::LaunchMissiles | $SeatProperties::Gunner;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "FWildcatStandard";
$DefaultVehicleWep[$VehicleCount, 0] = "GPlasma";
$DefaultVehicleWep[$VehicleCount, 1] = "MStinger";
$DefaultVehicleWep[$VehicleCount, 2] = "MStinger";
$VehicleCount++;

// Ameri
// -----------------------------------------------------------------------------
$VehicleListID["Ameri"] = $VehicleCount;
$VehicleList[$VehicleCount] = "Ameri";
$VehicleListData[$VehicleCount, "block"] = "Ameri";
$VehicleListData[$VehicleCount, "name"] = "Ameri Enforcer";
$VehicleListData[$VehicleCount, "desc"] = "This bike was converted from a police bike and upgraded with some serious military hardware";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "role"] = $AIVehicleRole::SingleSeater | $AIVehicleRole::Fighter;
$VehicleListData[$VehicleCount, "group"] = $VehicleGroup::Interceptors;
$VehicleListData[$VehicleCount, "hardpoints"] = 3;
$VehicleListData[$VehicleCount, "suffixcode"] = "AMI";
$VehicleList::Ameri = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Ameri = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[C] Nose Cannon";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Cannon;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Hybrid;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 0, "mountSuffix"] = "NUL";
$VehicleHardpoints[$VehicleCount, 1, "name"] = "[M] Right Wing Missile";
$VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 6;
$VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 1, "mountSuffix"] = "NUL";
$VehicleHardpoints[$VehicleCount, 2, "name"] = "[M] Left Wing Missile";
$VehicleHardpoints[$VehicleCount, 2, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 2, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 2, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 2, "imageStart"] = 7;
$VehicleHardpoints[$VehicleCount, 2, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 2, "mountSuffix"] = "NUL";

$VehicleHardpoints[$VehicleCount, 2, "position"] = "0 1.75 -0.55";
$VehicleHardpoints[$VehicleCount, 2, "rotation"] = "1 0 0 0";
$VehicleHardpoints[$VehicleCount, 6, "position"] = "1.05 0 0.65";
$VehicleHardpoints[$VehicleCount, 6, "rotation"] = "1 0 0 0";
$VehicleHardpoints[$VehicleCount, 7, "position"] = "-1.05 0 0.65";
$VehicleHardpoints[$VehicleCount, 7, "rotation"] = "1 0 0 0";

$VehicleMax["Ameri"] = 3;

$VehicleSeatProperties[$VehicleCount, 0] = $SeatProperties::Pilot | $SeatProperties::LaunchMissiles | $SeatProperties::Gunner;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "VShieldNone";
$DefaultVehicleWep[$VehicleCount, 0] = "LBlaster";
$DefaultVehicleWep[$VehicleCount, 1] = "MStinger";
$DefaultVehicleWep[$VehicleCount, 2] = "MStinger";
$VehicleCount++;

// F/A-73 Shrike Fighter
// -----------------------------------------------------------------------------
$VehicleListID["ScoutFlyer"] = $VehicleCount;
$VehicleList[$VehicleCount] = "ScoutFlyer";
$VehicleListData[$VehicleCount, "block"] = "ScoutFlyer";
$VehicleListData[$VehicleCount, "name"] = "Shrike Multirole Fighter";
$VehicleListData[$VehicleCount, "desc"] = "Standard Fighter/Aviator, jack of all trades";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "role"] = $AIVehicleRole::Fighter | $AIVehicleRole::SingleSeater;
$VehicleListData[$VehicleCount, "group"] = $VehicleGroup::Fighters;
$VehicleListData[$VehicleCount, "hardpoints"] = 4;
$VehicleListData[$VehicleCount, "suffixcode"] = "SHR";
$VehicleList::Shrike = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Shrike = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[G] Dual Nose Mounts";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Energy;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "mountSuffix"] = "NUL";
$VehicleHardpoints[$VehicleCount, 1, "name"] = "[G] Dual Wing Mounts";
$VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 4;
$VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 1, "mountSuffix"] = "NUL";
$VehicleHardpoints[$VehicleCount, 2, "name"] = "[M] Right Wing Missile";
$VehicleHardpoints[$VehicleCount, 2, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 2, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 2, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 2, "imageStart"] = 6;
$VehicleHardpoints[$VehicleCount, 2, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 2, "mountSuffix"] = "NUL";
$VehicleHardpoints[$VehicleCount, 3, "name"] = "[M] Left Wing Missile";
$VehicleHardpoints[$VehicleCount, 3, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 3, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 3, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 3, "imageStart"] = 7;
$VehicleHardpoints[$VehicleCount, 3, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 3, "mountSuffix"] = "NUL";

$VehicleHardpoints[$VehicleCount, 2, "position"] = "0.9 1.85 -1";
$VehicleHardpoints[$VehicleCount, 2, "rotation"] = "1 0 0 0";
$VehicleHardpoints[$VehicleCount, 3, "position"] = "-0.9 1.85 -1";
$VehicleHardpoints[$VehicleCount, 3, "rotation"] = "1 0 0 0";
$VehicleHardpoints[$VehicleCount, 4, "position"] = "3.2 -0.5 -2.5";
$VehicleHardpoints[$VehicleCount, 4, "rotation"] = "0 1 0 -125";
$VehicleHardpoints[$VehicleCount, 5, "position"] = "-3.2 -0.5 -2.5";
$VehicleHardpoints[$VehicleCount, 5, "rotation"] = "0 1 0 125";
$VehicleHardpoints[$VehicleCount, 6, "position"] = "4.2 -1.75 -3.25";
$VehicleHardpoints[$VehicleCount, 6, "rotation"] = "1 0 0 0";
$VehicleHardpoints[$VehicleCount, 7, "position"] = "-4.2 -1.75 -3.25";
$VehicleHardpoints[$VehicleCount, 7, "rotation"] = "1 0 0 0";

$VehicleMax["ScoutFlyer"] = 3;

$VehicleSeatProperties[$VehicleCount, 0] = $SeatProperties::Pilot | $SeatProperties::LaunchMissiles | $SeatProperties::Gunner;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "FShrikeStandard";
$DefaultVehicleWep[$VehicleCount, 0] = "GBlaster";
$DefaultVehicleWep[$VehicleCount, 1] = "GGauss";
$DefaultVehicleWep[$VehicleCount, 2] = "MBreacher";
$DefaultVehicleWep[$VehicleCount, 3] = "MBreacher";
$VehicleCount++;

// F/A-75 Shrike X
// -----------------------------------------------------------------------------
$VehicleListID["ShrikeX"] = $VehicleCount;
$VehicleList[$VehicleCount] = "ShrikeX";
$VehicleListData[$VehicleCount, "block"] = "ShrikeX";
$VehicleListData[$VehicleCount, "name"] = "Shrike X Prototype Fighter";
$VehicleListData[$VehicleCount, "desc"] = "Experimental combat upgrade to the Shrike, fast and powerful";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "role"] = $AIVehicleRole::Fighter | $AIVehicleRole::SingleSeater;
$VehicleListData[$VehicleCount, "group"] = $VehicleGroup::Fighters;
$VehicleListData[$VehicleCount, "hardpoints"] = 3;
$VehicleListData[$VehicleCount, "suffixcode"] = "SHX";
$VehicleList::ShrikeX = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::ShrikeX = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[G] Quad Wing Mounts";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Hybrid;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 4;
$VehicleHardpoints[$VehicleCount, 1, "name"] = "[M] Left Nose Missile";
$VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 6;
$VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 2, "name"] = "[M] Right Nose Missile";
$VehicleHardpoints[$VehicleCount, 2, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 2, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 2, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 2, "imageStart"] = 7;
$VehicleHardpoints[$VehicleCount, 2, "imageCount"] = 1;

$VehicleHardpoints[$VehicleCount, 2, "position"] = "3.6 -1 2.15";
$VehicleHardpoints[$VehicleCount, 2, "rotation"] = "1 0 0 0";
$VehicleHardpoints[$VehicleCount, 3, "position"] = "-4.25 -1 -3.5";
$VehicleHardpoints[$VehicleCount, 3, "rotation"] = "1 0 0 0";
$VehicleHardpoints[$VehicleCount, 4, "position"] = "-3.6 -1 2.15";
$VehicleHardpoints[$VehicleCount, 4, "rotation"] = "0 1 0 -125";
$VehicleHardpoints[$VehicleCount, 5, "position"] = "4.25 -1 -3.5";
$VehicleHardpoints[$VehicleCount, 5, "rotation"] = "0 1 0 125";
$VehicleHardpoints[$VehicleCount, 6, "position"] = "1.4 0.75 -0.5";
$VehicleHardpoints[$VehicleCount, 6, "rotation"] = "1 0 0 0";
$VehicleHardpoints[$VehicleCount, 7, "position"] = "-1.4 0.75 -0.5";
$VehicleHardpoints[$VehicleCount, 7, "rotation"] = "1 0 0 0";

$VehicleMax["ShrikeX"] = 3;

$VehicleSeatProperties[$VehicleCount, 0] = $SeatProperties::Pilot | $SeatProperties::LaunchMissiles | $SeatProperties::Gunner;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "VShieldNone";
$DefaultVehicleWep[$VehicleCount, 0] = "GBlaster";
$DefaultVehicleWep[$VehicleCount, 1] = "MStinger";
$DefaultVehicleWep[$VehicleCount, 2] = "MStinger";
$VehicleCount++;

// F/A-79 Super Shrike
// -----------------------------------------------------------------------------
$VehicleListID["SuperShrike"] = $VehicleCount;
$VehicleList[$VehicleCount] = "SuperShrike";
$VehicleListData[$VehicleCount, "block"] = "SuperShrike";
$VehicleListData[$VehicleCount, "name"] = "Super Shrike Strike Fighter";
$VehicleListData[$VehicleCount, "desc"] = "2-Seater agile combat platform, able to attack from all angles";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "role"] = $AIVehicleRole::Escort | $AIVehicleRole::HasTurrets;
$VehicleListData[$VehicleCount, "group"] = $VehicleGroup::Fighters;
$VehicleListData[$VehicleCount, "hardpoints"] = 5;
$VehicleListData[$VehicleCount, "suffixcode"] = "SSH";
$VehicleList::SuperShrike = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::SuperShrike = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[C] Nose Mount";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Cannon;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Ballistic;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 1, "name"] = "[G] Dual Wings Mount";
$VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 4;
$VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 2, "name"] = "[M] Left Wing Missile";
$VehicleHardpoints[$VehicleCount, 2, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 2, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 2, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 2, "imageStart"] = 6;
$VehicleHardpoints[$VehicleCount, 2, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 3, "name"] = "[M] Right Wing Missile";
$VehicleHardpoints[$VehicleCount, 3, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 3, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 3, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 3, "imageStart"] = 7;
$VehicleHardpoints[$VehicleCount, 3, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 4, "name"] = "[TG] Dual Turret Mount";
$VehicleHardpoints[$VehicleCount, 4, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 4, "type"] = $VHardpointType::Hybrid;
$VehicleHardpoints[$VehicleCount, 4, "mountPoint"] = 10;
$VehicleHardpoints[$VehicleCount, 4, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 4, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 4, "mountSuffix"] = "UBT";

$VehicleHardpoints[$VehicleCount, 2, "position"] = "0 4 -0.75";
$VehicleHardpoints[$VehicleCount, 2, "rotation"] = "1 0 0 0";
$VehicleHardpoints[$VehicleCount, 4, "position"] = "3.1 -1 -2.5";
$VehicleHardpoints[$VehicleCount, 4, "rotation"] = "0 1 0 -125";
$VehicleHardpoints[$VehicleCount, 5, "position"] = "-3.1 -1 -2.5";
$VehicleHardpoints[$VehicleCount, 5, "rotation"] = "0 1 0 125";
$VehicleHardpoints[$VehicleCount, 6, "position"] = "4.2 -1.75 -3.25";
$VehicleHardpoints[$VehicleCount, 6, "rotation"] = "1 0 0 0";
$VehicleHardpoints[$VehicleCount, 7, "position"] = "-4.2 -1.75 -3.25";
$VehicleHardpoints[$VehicleCount, 7, "rotation"] = "1 0 0 0";

$VehicleMax["SuperShrike"] = 3;

$VehicleSeatProperties[$VehicleCount, 0] = $SeatProperties::Pilot | $SeatProperties::LaunchMissiles | $SeatProperties::Gunner;
$VehicleSeatProperties[$VehicleCount, 1] = $SeatProperties::Gunner | $SeatProperties::ControlsTurret | $SeatProperties::Underbelly;

$TurretNodes[$VehicleCount] = 0;

$TurretControlNode[$VehicleCount, 1] = 10;
$TurretNodes[$VehicleCount]++;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "FSuperShrikeStandard";
$DefaultVehicleWep[$VehicleCount, 0] = "LScattergun";
$DefaultVehicleWep[$VehicleCount, 1] = "GBlaster";
$DefaultVehicleWep[$VehicleCount, 2] = "MBreacher";
$DefaultVehicleWep[$VehicleCount, 3] = "MBreacher";
$DefaultVehicleWep[$VehicleCount, 4] = "GDisruptor";
$VehicleCount++;

// NX-18 Starhawk
// -----------------------------------------------------------------------------
$VehicleListID["Starhawk"] = $VehicleCount;
$VehicleList[$VehicleCount] = "Starhawk";
$VehicleListData[$VehicleCount, "block"] = "Starhawk";
$VehicleListData[$VehicleCount, "name"] = "Starhawk Assault Fighter";
$VehicleListData[$VehicleCount, "desc"] = "Experimental heavy fighter, a good mix between speed and damage";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "hardpoints"] = 4;
$VehicleListData[$VehicleCount, "suffixcode"] = "STA";
$VehicleListData[$VehicleCount, "role"] = $AIVehicleRole::SingleSeater | $AIVehicleRole::Support;
$VehicleListData[$VehicleCount, "group"] = $VehicleGroup::Fighters;
$VehicleList::Starhawk = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Starhawk = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[C] Nose Mount";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Cannon;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 1, "name"] = "[G] Dual Wings Mount";
$VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Hybrid;
$VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 4;
$VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 2, "name"] = "[M] Left Wing Missile";
$VehicleHardpoints[$VehicleCount, 2, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 2, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 2, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 2, "imageStart"] = 6;
$VehicleHardpoints[$VehicleCount, 2, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 3, "name"] = "[M] Right Wing Missile";
$VehicleHardpoints[$VehicleCount, 3, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 3, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 3, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 3, "imageStart"] = 7;
$VehicleHardpoints[$VehicleCount, 3, "imageCount"] = 1;

$VehicleHardpoints[$VehicleCount, 2, "position"] = "0 1.6 -2.35";
$VehicleHardpoints[$VehicleCount, 2, "rotation"] = "1 0 0 0";
$VehicleHardpoints[$VehicleCount, 4, "position"] = "3.5 -4.95 1.6";
$VehicleHardpoints[$VehicleCount, 4, "rotation"] = "0 1 0 0";
$VehicleHardpoints[$VehicleCount, 5, "position"] = "-3.5 -4.95 1.6";
$VehicleHardpoints[$VehicleCount, 5, "rotation"] = "0 1 0 0";
$VehicleHardpoints[$VehicleCount, 6, "position"] = "4.54 -5.5 1";
$VehicleHardpoints[$VehicleCount, 6, "rotation"] = "1 0 0 0";
$VehicleHardpoints[$VehicleCount, 7, "position"] = "-4.54 -5.5 1";
$VehicleHardpoints[$VehicleCount, 7, "rotation"] = "1 0 0 0";

$VehicleMax["Starhawk"] = 3;

$VehicleSeatProperties[$VehicleCount, 0] = $SeatProperties::Pilot | $SeatProperties::LaunchMissiles | $SeatProperties::Gunner;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "FStarhawkStandard";
$DefaultVehicleWep[$VehicleCount, 0] = "LDisruptor";
$DefaultVehicleWep[$VehicleCount, 1] = "GVulcan";
$DefaultVehicleWep[$VehicleCount, 2] = "MBreacher";
$DefaultVehicleWep[$VehicleCount, 3] = "MBreacher";
$VehicleCount++;

// Firebird Heavy Fighter
// -----------------------------------------------------------------------------
$VehicleListID["Firebird"] = $VehicleCount;
$VehicleList[$VehicleCount] = "Firebird";
$VehicleListData[$VehicleCount, "block"] = "Firebird";
$VehicleListData[$VehicleCount, "name"] = "Firebird Heavy Fighter";
$VehicleListData[$VehicleCount, "desc"] = "Heavy plaform capable of mounting heavy class weapons for alpha striking";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "role"] = $AIVehicleRole::SingleSeater | $AIVehicleRole::Fighter;
$VehicleListData[$VehicleCount, "group"] = $VehicleGroup::Fighters;
$VehicleListData[$VehicleCount, "hardpoints"] = 3;
$VehicleListData[$VehicleCount, "suffixcode"] = "FIB";
$VehicleList::Firebird = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Firebird = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[C] Nose Mount";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Cannon;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 1, "name"] = "[C] Dual Wings Mount";
$VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Cannon;
$VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Hybrid;
$VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 4;
$VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 2, "name"] = "[G] Dual Under Nose Mount";
$VehicleHardpoints[$VehicleCount, 2, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 2, "type"] = $VHardpointType::Hybrid;
$VehicleHardpoints[$VehicleCount, 2, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 2, "imageStart"] = 6;
$VehicleHardpoints[$VehicleCount, 2, "imageCount"] = 2;

$VehicleHardpoints[$VehicleCount, 2, "position"] = "0 4.0 -1.65";
$VehicleHardpoints[$VehicleCount, 2, "rotation"] = "0 1 0 0";
$VehicleHardpoints[$VehicleCount, 4, "position"] = "1.9 -1.85 -1.35";
$VehicleHardpoints[$VehicleCount, 4, "rotation"] = "0 1 0 0";
$VehicleHardpoints[$VehicleCount, 5, "position"] = "-1.9 -1.85 -1.5";
$VehicleHardpoints[$VehicleCount, 5, "rotation"] = "0 1 0 0";
$VehicleHardpoints[$VehicleCount, 6, "position"] = "0.75 3.0 -1";
$VehicleHardpoints[$VehicleCount, 6, "rotation"] = "0 1 0 -90";
$VehicleHardpoints[$VehicleCount, 7, "position"] = "-0.75 3.0 -1";
$VehicleHardpoints[$VehicleCount, 7, "rotation"] = "0 1 0 90";

$VehicleMax["Firebird"] = 3;

$VehicleSeatProperties[$VehicleCount, 0] = $SeatProperties::Pilot | $SeatProperties::Gunner;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "FFirebirdStandard";
$DefaultVehicleWep[$VehicleCount, 0] = "LHowitzer";
$DefaultVehicleWep[$VehicleCount, 1] = "LBlaster";
$DefaultVehicleWep[$VehicleCount, 2] = "GGauss";
$VehicleCount++;

// Hummingbird HoverJeep
// -----------------------------------------------------------------------------
$VehicleListID["HoverJeep"] = $VehicleCount;
$VehicleList[$VehicleCount] = "HoverJeep";
$VehicleListData[$VehicleCount, "block"] = "HoverJeep";
$VehicleListData[$VehicleCount, "name"] = "Hummingbird Hover Jeep";
$VehicleListData[$VehicleCount, "desc"] = "Fast attack platform aimed at escort operations on larger fleets";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "role"] = $AIVehicleRole::HasTurrets | $AIVehicleRole::Fighter;
$VehicleListData[$VehicleCount, "group"] = $VehicleGroup::Fighters;
$VehicleListData[$VehicleCount, "hardpoints"] = 5;
$VehicleListData[$VehicleCount, "suffixcode"] = "JEP";
$VehicleList::HoverJeep = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::HoverJeep = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[G] Nose Mounts";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Hybrid;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 1, "name"] = "[TC] Rear Turret Cannon";
$VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Cannon;
$VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 2;
$VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 1, "mountSuffix"] = "TUS";
$VehicleHardpoints[$VehicleCount, 2, "name"] = "[TG] Rear Turret Guns";
$VehicleHardpoints[$VehicleCount, 2, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 2, "type"] = $VHardpointType::Hybrid;
$VehicleHardpoints[$VehicleCount, 2, "mountPoint"] = 2;
$VehicleHardpoints[$VehicleCount, 2, "imageStart"] = 4;
$VehicleHardpoints[$VehicleCount, 2, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 2, "mountSuffix"] = "TUD";
$VehicleHardpoints[$VehicleCount, 3, "name"] = "[M] Left Side Missile";
$VehicleHardpoints[$VehicleCount, 3, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 3, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 3, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 3, "imageStart"] = 6;
$VehicleHardpoints[$VehicleCount, 3, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 4, "name"] = "[M] Right Side Missile";
$VehicleHardpoints[$VehicleCount, 4, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 4, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 4, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 4, "imageStart"] = 7;
$VehicleHardpoints[$VehicleCount, 4, "imageCount"] = 1;

$VehicleHardpoints[$VehicleCount, 2, "position"] = "1.45 4.7 -1.8";
$VehicleHardpoints[$VehicleCount, 2, "rotation"] = "0 1 0 0";
$VehicleHardpoints[$VehicleCount, 3, "position"] = "0.55 4.7 -1.8";
$VehicleHardpoints[$VehicleCount, 3, "rotation"] = "0 1 0 0";
$VehicleHardpoints[$VehicleCount, 6, "position"] = "3.5 0 -0.5";
$VehicleHardpoints[$VehicleCount, 6, "rotation"] = "0 1 0 0";
$VehicleHardpoints[$VehicleCount, 7, "position"] = "-1.9 0 -0.5";
$VehicleHardpoints[$VehicleCount, 7, "rotation"] = "0 1 0 0";

$VehicleMax["HoverJeep"] = 3;

$VehicleSeatProperties[$VehicleCount, 0] = $SeatProperties::Pilot | $SeatProperties::LaunchMissiles | $SeatProperties::Gunner;
$VehicleSeatProperties[$VehicleCount, 1] = $SeatProperties::Gunner | $SeatProperties::ControlsTurret | $SeatProperties::Tailgunner;

$TurretNodes[$VehicleCount] = 0;

$TurretControlNode[$VehicleCount, 1] = 2;
$TurretNodes[$VehicleCount]++;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "FHoverJeepStandard";
$DefaultVehicleWep[$VehicleCount, 0] = "GPlasma";
$DefaultVehicleWep[$VehicleCount, 1] = "LFlak";
$DefaultVehicleWep[$VehicleCount, 2] = "GDisruptor";
$DefaultVehicleWep[$VehicleCount, 3] = "MSLAM";
$DefaultVehicleWep[$VehicleCount, 4] = "MSLAM";
$VehicleCount++;

// B-86 Catapult Destroyer
// -----------------------------------------------------------------------------
$VehicleListID["Catapult"] = $VehicleCount;
$VehicleList[$VehicleCount] = "Catapult";
$VehicleListData[$VehicleCount, "block"] = "Catapult";
$VehicleListData[$VehicleCount, "name"] = "B-86 Catapult Bomber";
$VehicleListData[$VehicleCount, "desc"] = "Heavily armed bomber/torpedo platform";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "role"] = $AIVehicleRole::Bomber | $AIVehicleRole::Escort;
$VehicleListData[$VehicleCount, "group"] = $VehicleGroup::Gunships;
$VehicleListData[$VehicleCount, "hardpoints"] = 5;
$VehicleListData[$VehicleCount, "suffixcode"] = "CAT";
$VehicleList::Catapult = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Catapult = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[P] Dual Rocket Pods";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Pod;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 1, "name"] = "[M] Left Missile 1 Mount";
$VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 4;
$VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 2, "name"] = "[M] Left Missile 2 Mount";
$VehicleHardpoints[$VehicleCount, 2, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 2, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 2, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 2, "imageStart"] = 5;
$VehicleHardpoints[$VehicleCount, 2, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 3, "name"] = "[M] Right Missile 1 Mount";
$VehicleHardpoints[$VehicleCount, 3, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 3, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 3, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 3, "imageStart"] = 6;
$VehicleHardpoints[$VehicleCount, 3, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 4, "name"] = "[M] Right Missile 2 Mount";
$VehicleHardpoints[$VehicleCount, 4, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 4, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 4, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 4, "imageStart"] = 7;
$VehicleHardpoints[$VehicleCount, 4, "imageCount"] = 1;

$VehicleHardpoints[$VehicleCount, 2, "position"] = "0.85 3 -5";
$VehicleHardpoints[$VehicleCount, 2, "rotation"] = "0 1 0 0";
$VehicleHardpoints[$VehicleCount, 3, "position"] = "-0.85 3 -5";
$VehicleHardpoints[$VehicleCount, 3, "rotation"] = "0 1 0 0";
$VehicleHardpoints[$VehicleCount, 4, "position"] = "5.75 2.75 -3.75";
$VehicleHardpoints[$VehicleCount, 4, "rotation"] = "0 1 0 0";
$VehicleHardpoints[$VehicleCount, 5, "position"] = "3.5 2.75 -3";
$VehicleHardpoints[$VehicleCount, 5, "rotation"] = "0 1 0 0";
$VehicleHardpoints[$VehicleCount, 6, "position"] = "-3.5 2.75 -3";
$VehicleHardpoints[$VehicleCount, 6, "rotation"] = "0 1 0 0";
$VehicleHardpoints[$VehicleCount, 7, "position"] = "-5.75 2.75 -3.75";
$VehicleHardpoints[$VehicleCount, 7, "rotation"] = "0 1 0 0";

$VehicleMax["Catapult"] = 3;

$VehicleSeatProperties[$VehicleCount, 0] = $SeatProperties::Pilot | $SeatProperties::LaunchMissiles | $SeatProperties::Gunner;
$VehicleSeatProperties[$VehicleCount, 1] = $SeatProperties::Gunner | $SeatProperties::Tailgunner;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "FCatapultStandard";
$DefaultVehicleWep[$VehicleCount, 0] = "LDevistator";
$DefaultVehicleWep[$VehicleCount, 1] = "MSLAM";
$DefaultVehicleWep[$VehicleCount, 2] = "MSLAM";
$DefaultVehicleWep[$VehicleCount, 3] = "MSLAM";
$DefaultVehicleWep[$VehicleCount, 4] = "MSLAM";
$VehicleCount++;

// AH-77 Tomahawk
// -----------------------------------------------------------------------------
$VehicleListID["Tomahawk"] = $VehicleCount;
$VehicleList[$VehicleCount] = "Tomahawk";
$VehicleListData[$VehicleCount, "block"] = "Tomahawk";
$VehicleListData[$VehicleCount, "name"] = "Tomahawk Attack Chopper";
$VehicleListData[$VehicleCount, "desc"] = "Fast attack platform aimed at escort operations on larger fleets";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "role"] = $AIVehicleRole::HasTurrets | $AIVehicleRole::Escort;
$VehicleListData[$VehicleCount, "group"] = $VehicleGroup::Gunships;
$VehicleListData[$VehicleCount, "hardpoints"] = 6;
$VehicleListData[$VehicleCount, "suffixcode"] = "TOM";
$VehicleList::Tomahawk = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Tomahawk = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[C] Dual Nose Mount";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Cannon;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Hybrid;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 1, "name"] = "[TG] Dual Turret";
$VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 10;
$VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 1, "mountSuffix"] = "UBT";
$VehicleHardpoints[$VehicleCount, 2, "name"] = "[M] Left Missile 1 Mount";
$VehicleHardpoints[$VehicleCount, 2, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 2, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 2, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 2, "imageStart"] = 4;
$VehicleHardpoints[$VehicleCount, 2, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 3, "name"] = "[M] Left Missile 2 Mount";
$VehicleHardpoints[$VehicleCount, 3, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 3, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 3, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 3, "imageStart"] = 5;
$VehicleHardpoints[$VehicleCount, 3, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 4, "name"] = "[M] Right Missile 1 Mount";
$VehicleHardpoints[$VehicleCount, 4, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 4, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 4, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 4, "imageStart"] = 6;
$VehicleHardpoints[$VehicleCount, 4, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 5, "name"] = "[M] Right Missile 2 Mount";
$VehicleHardpoints[$VehicleCount, 5, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 5, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 5, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 5, "imageStart"] = 7;
$VehicleHardpoints[$VehicleCount, 5, "imageCount"] = 1;

$VehicleHardpoints[$VehicleCount, 2, "position"] = "0.75 0.75 -1";
$VehicleHardpoints[$VehicleCount, 2, "rotation"] = "0 1 0 0";
$VehicleHardpoints[$VehicleCount, 3, "position"] = "-0.75 0.75 -1";
$VehicleHardpoints[$VehicleCount, 3, "rotation"] = "0 1 0 0";
$VehicleHardpoints[$VehicleCount, 4, "position"] = "3.85 -6.5 -1.25";
$VehicleHardpoints[$VehicleCount, 4, "rotation"] = "0 1 0 0";
$VehicleHardpoints[$VehicleCount, 5, "position"] = "2.6 -6.5 -1.75";
$VehicleHardpoints[$VehicleCount, 5, "rotation"] = "0 1 0 0";
$VehicleHardpoints[$VehicleCount, 6, "position"] = "-2.6 -6.5 -1.75";
$VehicleHardpoints[$VehicleCount, 6, "rotation"] = "0 1 0 0";
$VehicleHardpoints[$VehicleCount, 7, "position"] = "-3.85 -6.5 -1.25";
$VehicleHardpoints[$VehicleCount, 7, "rotation"] = "0 1 0 0";

$VehicleMax["Tomahawk"] = 3;

$VehicleSeatProperties[$VehicleCount, 0] = $SeatProperties::Pilot | $SeatProperties::LaunchMissiles | $SeatProperties::Gunner;
$VehicleSeatProperties[$VehicleCount, 1] = $SeatProperties::Gunner | $SeatProperties::ControlsTurret | $SeatProperties::Underbelly;

$TurretNodes[$VehicleCount] = 0;

$TurretControlNode[$VehicleCount, 1] = 10;
$TurretNodes[$VehicleCount]++;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "FTomahawkStandard";
$DefaultVehicleWep[$VehicleCount, 0] = "LAutocannon";
$DefaultVehicleWep[$VehicleCount, 1] = "GBlaster";
$DefaultVehicleWep[$VehicleCount, 2] = "MBreacher";
$DefaultVehicleWep[$VehicleCount, 3] = "MBreacher";
$DefaultVehicleWep[$VehicleCount, 4] = "MBreacher";
$DefaultVehicleWep[$VehicleCount, 5] = "MBreacher";
$VehicleCount++;

// Guillotine Hover Tank
// -----------------------------------------------------------------------------
$VehicleListID["AssaultVehicle"] = $VehicleCount;
$VehicleList[$VehicleCount] = "AssaultVehicle";
$VehicleListData[$VehicleCount, "block"] = "AssaultVehicle";
$VehicleListData[$VehicleCount, "name"] = "Guillotine Hover Tank";
$VehicleListData[$VehicleCount, "desc"] = "Flying artillery platform, one of two vehicles able to field an XL mount";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Hovering;
$VehicleListData[$VehicleCount, "role"] = $AIVehicleRole::HasTurrets | $AIVehicleRole::Fighter;
$VehicleListData[$VehicleCount, "group"] = $VehicleGroup::Gunships;
$VehicleListData[$VehicleCount, "hardpoints"] = 5;
$VehicleListData[$VehicleCount, "suffixcode"] = "GUI";
$VehicleList::Guillotine = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Guillotine = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[C] Pilot Turret Mount";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Cannon;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 1, "name"] = "[M] Underbelly L Missile";
$VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 6;
$VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 2, "name"] = "[M] Underbelly R Missile";
$VehicleHardpoints[$VehicleCount, 2, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 2, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 2, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 2, "imageStart"] = 7;
$VehicleHardpoints[$VehicleCount, 2, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 3, "name"] = "[TS] Siege Cannon Barrel";
$VehicleHardpoints[$VehicleCount, 3, "size"] = $VHardpointSize::Siege;
$VehicleHardpoints[$VehicleCount, 3, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 3, "mountPoint"] = 10;
$VehicleHardpoints[$VehicleCount, 3, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 3, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 3, "mountSuffix"] = "TUT";
$VehicleHardpoints[$VehicleCount, 4, "name"] = "[TG] Siege Gunner Barrels";
$VehicleHardpoints[$VehicleCount, 4, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 4, "type"] = $VHardpointType::Hybrid;
$VehicleHardpoints[$VehicleCount, 4, "mountPoint"] = 10;
$VehicleHardpoints[$VehicleCount, 4, "imageStart"] = 4;
$VehicleHardpoints[$VehicleCount, 4, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 4, "mountSuffix"] = "TUT";

$VehicleHardpoints[$VehicleCount, 2, "position"] = "-1.5 1 1.7";
$VehicleHardpoints[$VehicleCount, 2, "rotation"] = "0 1 0 0";
$VehicleHardpoints[$VehicleCount, 6, "position"] = "1.55 -3 -2";
$VehicleHardpoints[$VehicleCount, 6, "rotation"] = "0 1 0 0";
$VehicleHardpoints[$VehicleCount, 7, "position"] = "-2.85 -3 -2";
$VehicleHardpoints[$VehicleCount, 7, "rotation"] = "0 1 0 0";

$VehicleMax["AssaultVehicle"] = 2;

$VehicleSeatProperties[$VehicleCount, 0] = $SeatProperties::Pilot | $SeatProperties::LaunchMissiles | $SeatProperties::Gunner;
$VehicleSeatProperties[$VehicleCount, 1] = $SeatProperties::Gunner | $SeatProperties::ControlsTurret | $SeatProperties::Tailgunner;

$TurretNodes[$VehicleCount] = 0;

$TurretControlNode[$VehicleCount, 1] = 10;
$TurretNodes[$VehicleCount]++;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "FGuillotineStandard";
$DefaultVehicleWep[$VehicleCount, 0] = "LBlaster";
$DefaultVehicleWep[$VehicleCount, 1] = "MSLAM";
$DefaultVehicleWep[$VehicleCount, 2] = "MSLAM";
$DefaultVehicleWep[$VehicleCount, 3] = "XLRailgun";
$DefaultVehicleWep[$VehicleCount, 4] = "GMinigun";

$VehicleCount++;

// Retaliator Gunship
//----------------------------------------------------
$VehicleListID["BomberFlyer"] = $VehicleCount;
$VehicleList[$VehicleCount] = "BomberFlyer";
$VehicleListData[$VehicleCount, "block"] = "BomberFlyer";
$VehicleListData[$VehicleCount, "name"] = "Retaliator Gunship";
$VehicleListData[$VehicleCount, "desc"] = "Light and fast gunship, a true jack of all trades";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "role"] = $AIVehicleRole::HasTurrets | $AIVehicleRole::Escort;
$VehicleListData[$VehicleCount, "group"] = $VehicleGroup::Gunships;
$VehicleListData[$VehicleCount, "hardpoints"] = 5;
$VehicleListData[$VehicleCount, "suffixcode"] = "RET";
$VehicleList::Retaliator = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Retaliator = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[C] Pilot Turret Mount";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Cannon;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 1, "name"] = "[TG] Dual Underbelly Turret";
$VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Hybrid;
$VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 10;
$VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 1, "mountSuffix"] = "UBT";
$VehicleHardpoints[$VehicleCount, 2, "name"] = "[TB] Underbelly Launch Bay";
$VehicleHardpoints[$VehicleCount, 2, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 2, "type"] = $VHardpointType::Bay;
$VehicleHardpoints[$VehicleCount, 2, "mountPoint"] = 10;
$VehicleHardpoints[$VehicleCount, 2, "imageStart"] = 4;
$VehicleHardpoints[$VehicleCount, 2, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 2, "mountSuffix"] = "UBT";
$VehicleHardpoints[$VehicleCount, 3, "name"] = "[M] Missile L Underbelly";
$VehicleHardpoints[$VehicleCount, 3, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 3, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 3, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 3, "imageStart"] = 6;
$VehicleHardpoints[$VehicleCount, 3, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 4, "name"] = "[M] Missile R Underbelly";
$VehicleHardpoints[$VehicleCount, 4, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 4, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 4, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 4, "imageStart"] = 7;
$VehicleHardpoints[$VehicleCount, 4, "imageCount"] = 1;

$VehicleHardpoints[$VehicleCount, 2, "position"] = "0 1.7 -2.25";
$VehicleHardpoints[$VehicleCount, 2, "rotation"] = "0 1 0 0";
$VehicleHardpoints[$VehicleCount, 6, "position"] = "1.75 -7 -1.75";
$VehicleHardpoints[$VehicleCount, 6, "rotation"] = "0 1 0 0";
$VehicleHardpoints[$VehicleCount, 7, "position"] = "-1.75 -7 -1.75";
$VehicleHardpoints[$VehicleCount, 7, "rotation"] = "0 1 0 0";

$VehicleMax["BomberFlyer"] = 2;

$VehicleSeatProperties[$VehicleCount, 0] = $SeatProperties::Pilot | $SeatProperties::LaunchMissiles | $SeatProperties::Gunner;
$VehicleSeatProperties[$VehicleCount, 1] = $SeatProperties::Gunner | $SeatProperties::ControlsTurret | $SeatProperties::Underbelly | $SeatProperties::LaunchMissiles;
$VehicleSeatProperties[$VehicleCount, 2] = $SeatProperties::Gunner | $SeatProperties::Tailgunner;

$TurretNodes[$VehicleCount] = 0;

$TurretControlNode[$VehicleCount, 1] = 10;
$TurretNodes[$VehicleCount]++;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "FRetaliatorStandard";
$DefaultVehicleWep[$VehicleCount, 0] = "LHowitzer";
$DefaultVehicleWep[$VehicleCount, 1] = "GDisruptor";
$DefaultVehicleWep[$VehicleCount, 2] = "LBreacherBay";
$DefaultVehicleWep[$VehicleCount, 3] = "MSLAM";
$DefaultVehicleWep[$VehicleCount, 4] = "MSLAM";
$VehicleCount++;

// Stryker Assault Ship
//----------------------------------------------------
$VehicleListID["Stryker"] = $VehicleCount;
$VehicleList[$VehicleCount] = "Stryker";
$VehicleListData[$VehicleCount, "block"] = "Stryker";
$VehicleListData[$VehicleCount, "name"] = "Stryker Cruiser";
$VehicleListData[$VehicleCount, "desc"] = "Escort cruiser with 360 degree fire coverage, ideal for flagship defense";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "role"] = $AIVehicleRole::HasTurrets | $AIVehicleRole::Fighter;
$VehicleListData[$VehicleCount, "group"] = $VehicleGroup::Battleships;
$VehicleListData[$VehicleCount, "hardpoints"] = 5;
$VehicleListData[$VehicleCount, "suffixcode"] = "STR";
$VehicleList::Stryker = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Stryker = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[C] Pilot Turret Mount";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Cannon;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 1, "name"] = "[TG] Dual Underbelly Turret";
$VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Hybrid;
$VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 10;
$VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 1, "mountSuffix"] = "UBT";
$VehicleHardpoints[$VehicleCount, 2, "name"] = "[TB] Underbelly Launch Bay";
$VehicleHardpoints[$VehicleCount, 2, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 2, "type"] = $VHardpointType::Bay;
$VehicleHardpoints[$VehicleCount, 2, "mountPoint"] = 10;
$VehicleHardpoints[$VehicleCount, 2, "imageStart"] = 4;
$VehicleHardpoints[$VehicleCount, 2, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 2, "mountSuffix"] = "UBT";
$VehicleHardpoints[$VehicleCount, 3, "name"] = "[TC] Rear Turret Cannon";
$VehicleHardpoints[$VehicleCount, 3, "size"] = $VHardpointSize::Cannon;
$VehicleHardpoints[$VehicleCount, 3, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 3, "mountPoint"] = 3;
$VehicleHardpoints[$VehicleCount, 3, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 3, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 3, "mountSuffix"] = "TUS";
$VehicleHardpoints[$VehicleCount, 4, "name"] = "[TG] Rear Turret Guns";
$VehicleHardpoints[$VehicleCount, 4, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 4, "type"] = $VHardpointType::Hybrid;
$VehicleHardpoints[$VehicleCount, 4, "mountPoint"] = 3;
$VehicleHardpoints[$VehicleCount, 4, "imageStart"] = 4;
$VehicleHardpoints[$VehicleCount, 4, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 4, "mountSuffix"] = "TUD";

$VehicleHardpoints[$VehicleCount, 2, "position"] = "0 1.7 -2.25";
$VehicleHardpoints[$VehicleCount, 2, "rotation"] = "0 1 0 0";

$VehicleMax["Stryker"] = 2;

$VehicleSeatProperties[$VehicleCount, 0] = $SeatProperties::Pilot | $SeatProperties::Gunner;
$VehicleSeatProperties[$VehicleCount, 1] = $SeatProperties::Gunner | $SeatProperties::ControlsTurret | $SeatProperties::Underbelly | $SeatProperties::LaunchMissiles;
$VehicleSeatProperties[$VehicleCount, 2] = $SeatProperties::Gunner | $SeatProperties::ControlsTurret | $SeatProperties::Tailgunner;

$TurretNodes[$VehicleCount] = 0;

$TurretControlNode[$VehicleCount, 1] = 10;
$TurretNodes[$VehicleCount]++;

$TurretControlNode[$VehicleCount, 2] = 3;
$TurretNodes[$VehicleCount]++;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "FStrykerStandard";
$DefaultVehicleWep[$VehicleCount, 0] = "LBlaster";
$DefaultVehicleWep[$VehicleCount, 1] = "GGauss";
$DefaultVehicleWep[$VehicleCount, 2] = "LStingerBay";
$DefaultVehicleWep[$VehicleCount, 3] = "LPlasma";
$DefaultVehicleWep[$VehicleCount, 4] = "GNullifier";
$VehicleCount++;

// Wolfhound Battlecruiser
//----------------------------------------------------
$VehicleListID["Wolfhound"] = $VehicleCount;
$VehicleList[$VehicleCount] = "Wolfhound";
$VehicleListData[$VehicleCount, "block"] = "Wolfhound";
$VehicleListData[$VehicleCount, "name"] = "Wolfhound Battlecruiser";
$VehicleListData[$VehicleCount, "desc"] = "Designed to be agile enough to take on enemy fleets and avoid some fire";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "role"] = $AIVehicleRole::HasTurrets | $AIVehicleRole::Support;
$VehicleListData[$VehicleCount, "group"] = $VehicleGroup::Battleships;
$VehicleListData[$VehicleCount, "hardpoints"] = 6;
$VehicleListData[$VehicleCount, "suffixcode"] = "WOL";
$VehicleList::Wolfhound = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Wolfhound = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[TC] Wing L Turret Cannon";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Cannon;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Hybrid;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 0, "mountSuffix"] = "TUS";
$VehicleHardpoints[$VehicleCount, 1, "name"] = "[TG] Wing L Turret Guns";
$VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 2;
$VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 4;
$VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 1, "mountSuffix"] = "TUD";
$VehicleHardpoints[$VehicleCount, 2, "name"] = "[TC] Wing R Turret Cannon";
$VehicleHardpoints[$VehicleCount, 2, "size"] = $VHardpointSize::Cannon;
$VehicleHardpoints[$VehicleCount, 2, "type"] = $VHardpointType::Hybrid;
$VehicleHardpoints[$VehicleCount, 2, "mountPoint"] = 5;
$VehicleHardpoints[$VehicleCount, 2, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 2, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 2, "mountSuffix"] = "TUS";
$VehicleHardpoints[$VehicleCount, 3, "name"] = "[TG] Wing R Turret Guns";
$VehicleHardpoints[$VehicleCount, 3, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 3, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 3, "mountPoint"] = 5;
$VehicleHardpoints[$VehicleCount, 3, "imageStart"] = 4;
$VehicleHardpoints[$VehicleCount, 3, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 3, "mountSuffix"] = "TUD";
$VehicleHardpoints[$VehicleCount, 4, "name"] = "[M] Wing L Missile";
$VehicleHardpoints[$VehicleCount, 4, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 4, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 4, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 4, "imageStart"] = 6;
$VehicleHardpoints[$VehicleCount, 4, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 5, "name"] = "[M] Wing R Missile";
$VehicleHardpoints[$VehicleCount, 5, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 5, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 5, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 5, "imageStart"] = 7;
$VehicleHardpoints[$VehicleCount, 5, "imageCount"] = 1;

$VehicleHardpoints[$VehicleCount, 6, "position"] = "1.5 6 -4.25";
$VehicleHardpoints[$VehicleCount, 6, "rotation"] = "0 1 0 0";
$VehicleHardpoints[$VehicleCount, 7, "position"] = "-1.5 6 -4.25";
$VehicleHardpoints[$VehicleCount, 7, "rotation"] = "0 1 0 0";

$VehicleMax["Wolfhound"] = 2;

$VehicleSeatProperties[$VehicleCount, 0] = $SeatProperties::Pilot | $SeatProperties::LaunchMissiles;
$VehicleSeatProperties[$VehicleCount, 1] = $SeatProperties::Gunner | $SeatProperties::Tailgunner;
$VehicleSeatProperties[$VehicleCount, 2] = 0;
$VehicleSeatProperties[$VehicleCount, 3] = $SeatProperties::Gunner | $SeatProperties::ControlsTurret;
$VehicleSeatProperties[$VehicleCount, 4] = $SeatProperties::Gunner | $SeatProperties::ControlsTurret;
$VehicleSeatProperties[$VehicleCount, 5] = 0;

$TurretNodes[$VehicleCount] = 0;

$TurretControlNode[$VehicleCount, 3] = 2;
$TurretNodes[$VehicleCount]++;

$TurretControlNode[$VehicleCount, 4] = 5;
$TurretNodes[$VehicleCount]++;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "FWolfhoundStandard";
$DefaultVehicleWep[$VehicleCount, 0] = "LImpactMortar";
$DefaultVehicleWep[$VehicleCount, 1] = "GBlaster";
$DefaultVehicleWep[$VehicleCount, 2] = "LAutoCannon";
$DefaultVehicleWep[$VehicleCount, 3] = "GPlasma";
$DefaultVehicleWep[$VehicleCount, 4] = "MJavelin";
$DefaultVehicleWep[$VehicleCount, 5] = "MJavelin";
$VehicleCount++;

// Annihilator Battleship
// -----------------------------------------------------------------------------
$VehicleListID["Annihilator"] = $VehicleCount;
$VehicleList[$VehicleCount] = "Annihilator";
$VehicleListData[$VehicleCount, "block"] = "Annihilator";
$VehicleListData[$VehicleCount, "name"] = "Annihilator Battleship";
$VehicleListData[$VehicleCount, "desc"] = "The assault platform of choice, heavily armed and armored for all purposes";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "role"] = $AIVehicleRole::HasTurrets | $AIVehicleRole::Fighter;
$VehicleListData[$VehicleCount, "group"] = $VehicleGroup::Battleships;
$VehicleListData[$VehicleCount, "hardpoints"] = 6;
$VehicleListData[$VehicleCount, "suffixcode"] = "ANH";
$VehicleList::Annihilator = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Annihilator = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[TC] R Wing Turret Cannon";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Cannon;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 5;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 0, "mountSuffix"] = "TUS";
$VehicleHardpoints[$VehicleCount, 1, "name"] = "[TG] R Wing Turret Guns";
$VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Hybrid;
$VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 5;
$VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 4;
$VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 1, "mountSuffix"] = "TUD";
$VehicleHardpoints[$VehicleCount, 2, "name"] = "[TC] L Wing Turret Cannon";
$VehicleHardpoints[$VehicleCount, 2, "size"] = $VHardpointSize::Cannon;
$VehicleHardpoints[$VehicleCount, 2, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 2, "mountPoint"] = 6;
$VehicleHardpoints[$VehicleCount, 2, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 2, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 2, "mountSuffix"] = "TUS";
$VehicleHardpoints[$VehicleCount, 3, "name"] = "[TG] L Wing Turret Guns";
$VehicleHardpoints[$VehicleCount, 3, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 3, "type"] = $VHardpointType::Hybrid;
$VehicleHardpoints[$VehicleCount, 3, "mountPoint"] = 6;
$VehicleHardpoints[$VehicleCount, 3, "imageStart"] = 4;
$VehicleHardpoints[$VehicleCount, 3, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 3, "mountSuffix"] = "TUD";
$VehicleHardpoints[$VehicleCount, 4, "name"] = "[TG] Dual Underbelly Turret";
$VehicleHardpoints[$VehicleCount, 4, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 4, "type"] = $VHardpointType::Hybrid;
$VehicleHardpoints[$VehicleCount, 4, "mountPoint"] = 10;
$VehicleHardpoints[$VehicleCount, 4, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 4, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 4, "mountSuffix"] = "UBT";
$VehicleHardpoints[$VehicleCount, 5, "name"] = "[TB] Underbelly Launch Bay";
$VehicleHardpoints[$VehicleCount, 5, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 5, "type"] = $VHardpointType::Bay;
$VehicleHardpoints[$VehicleCount, 5, "mountPoint"] = 10;
$VehicleHardpoints[$VehicleCount, 5, "imageStart"] = 4;
$VehicleHardpoints[$VehicleCount, 5, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 5, "mountSuffix"] = "UBT";

$VehicleMax["Annihilator"] = 2;

$VehicleSeatProperties[$VehicleCount, 0] = $SeatProperties::Pilot;
$VehicleSeatProperties[$VehicleCount, 1] = $SeatProperties::Gunner | $SeatProperties::Underbelly | $SeatProperties::ControlsTurret | $SeatProperties::LaunchMissiles;
$VehicleSeatProperties[$VehicleCount, 2] = $SeatProperties::Gunner | $SeatProperties::Tailgunner;
$VehicleSeatProperties[$VehicleCount, 3] = 0;
$VehicleSeatProperties[$VehicleCount, 4] = $SeatProperties::Gunner | $SeatProperties::ControlsTurret;
$VehicleSeatProperties[$VehicleCount, 5] = 0;
$VehicleSeatProperties[$VehicleCount, 6] = 0;
$VehicleSeatProperties[$VehicleCount, 7] = $SeatProperties::Gunner | $SeatProperties::ControlsTurret;

$TurretNodes[$VehicleCount] = 0;

$TurretControlNode[$VehicleCount, 1] = 10;
$TurretNodes[$VehicleCount]++;

$TurretControlNode[$VehicleCount, 4] = 5;
$TurretNodes[$VehicleCount]++;

$TurretControlNode[$VehicleCount, 7] = 6;
$TurretNodes[$VehicleCount]++;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "FAnnihilatorStandard";
$DefaultVehicleWep[$VehicleCount, 0] = "LHowitzer";
$DefaultVehicleWep[$VehicleCount, 1] = "GDisruptor";
$DefaultVehicleWep[$VehicleCount, 2] = "LLaserCannon";
$DefaultVehicleWep[$VehicleCount, 3] = "PHailstorm";
$DefaultVehicleWep[$VehicleCount, 4] = "GBlaster";
$DefaultVehicleWep[$VehicleCount, 5] = "LBreacherBay";
$VehicleCount++;

// Leviathan Dreadnought
// -----------------------------------------------------------------------------
$VehicleListID["Leviathan"] = $VehicleCount;
$VehicleList[$VehicleCount] = "Leviathan";
$VehicleListData[$VehicleCount, "block"] = "Leviathan";
$VehicleListData[$VehicleCount, "name"] = "Leviathan Dreadnought";
$VehicleListData[$VehicleCount, "desc"] = "Heavily armed and armored, uses a jump/warp drive to get in and out of range";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "role"] = $AIVehicleRole::HasTurrets | $AIVehicleRole::Carrier | $AIVehicleRole::Support;
$VehicleListData[$VehicleCount, "group"] = $VehicleGroup::Dreadnoughts;
$VehicleListData[$VehicleCount, "hardpoints"] = 6;
$VehicleListData[$VehicleCount, "suffixcode"] = "LEV";
$VehicleList::Leviathan = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Leviathan = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[TS] Forward Turret Cannon";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Siege;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Hybrid;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 8;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 0, "mountSuffix"] = "TUT";
$VehicleHardpoints[$VehicleCount, 1, "name"] = "[TC] Forward Turret Guns";
$VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Cannon;
$VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 8;
$VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 4;
$VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 1, "mountSuffix"] = "TUT";
$VehicleHardpoints[$VehicleCount, 2, "name"] = "[TS] L Rear Turret Cannon";
$VehicleHardpoints[$VehicleCount, 2, "size"] = $VHardpointSize::Siege;
$VehicleHardpoints[$VehicleCount, 2, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 2, "mountPoint"] = 9;
$VehicleHardpoints[$VehicleCount, 2, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 2, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 2, "mountSuffix"] = "TUT";
$VehicleHardpoints[$VehicleCount, 3, "name"] = "[TG] L Rear Turret Guns";
$VehicleHardpoints[$VehicleCount, 3, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 3, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 3, "mountPoint"] = 9;
$VehicleHardpoints[$VehicleCount, 3, "imageStart"] = 4;
$VehicleHardpoints[$VehicleCount, 3, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 3, "mountSuffix"] = "TUT";
$VehicleHardpoints[$VehicleCount, 4, "name"] = "[TS] R Rear Turret Cannon";
$VehicleHardpoints[$VehicleCount, 4, "size"] = $VHardpointSize::Siege;
$VehicleHardpoints[$VehicleCount, 4, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 4, "mountPoint"] = 10;
$VehicleHardpoints[$VehicleCount, 4, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 4, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 4, "mountSuffix"] = "TUT";
$VehicleHardpoints[$VehicleCount, 5, "name"] = "[TG] R Rear Turret Guns";
$VehicleHardpoints[$VehicleCount, 5, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 5, "type"] = $VHardpointType::Omni;
$VehicleHardpoints[$VehicleCount, 5, "mountPoint"] = 10;
$VehicleHardpoints[$VehicleCount, 5, "imageStart"] = 4;
$VehicleHardpoints[$VehicleCount, 5, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 5, "mountSuffix"] = "TUT";

$VehicleMax["Leviathan"] = 2;

$VehicleSeatProperties[$VehicleCount, 0] = $SeatProperties::Pilot;
$VehicleSeatProperties[$VehicleCount, 1] = $SeatProperties::Gunner | $SeatProperties::ControlsTurret;
$VehicleSeatProperties[$VehicleCount, 2] = $SeatProperties::Gunner | $SeatProperties::ControlsTurret | $SeatProperties::Tailgunner;
$VehicleSeatProperties[$VehicleCount, 3] = $SeatProperties::Gunner | $SeatProperties::ControlsTurret | $SeatProperties::Tailgunner;
$VehicleSeatProperties[$VehicleCount, 4] = $SeatProperties::Gunner;
$VehicleSeatProperties[$VehicleCount, 5] = $SeatProperties::Gunner;
$VehicleSeatProperties[$VehicleCount, 6] = $SeatProperties::Gunner;
$VehicleSeatProperties[$VehicleCount, 7] = $SeatProperties::Gunner;

$TurretNodes[$VehicleCount] = 0;

$TurretControlNode[$VehicleCount, 1] = 8;
$TurretNodes[$VehicleCount]++;

$TurretControlNode[$VehicleCount, 2] = 9;
$TurretNodes[$VehicleCount]++;

$TurretControlNode[$VehicleCount, 3] = 10;
$TurretNodes[$VehicleCount]++;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "FLeviathanStandard";
$DefaultVehicleWep[$VehicleCount, 0] = "XLParticleBeam";
$DefaultVehicleWep[$VehicleCount, 1] = "LPlasma";
$DefaultVehicleWep[$VehicleCount, 2] = "XLMeteor";
$DefaultVehicleWep[$VehicleCount, 3] = "GGauss";
$DefaultVehicleWep[$VehicleCount, 4] = "XLRailgun";
$DefaultVehicleWep[$VehicleCount, 5] = "GBlaster";
$VehicleCount++;

// RSS Bismark
// -----------------------------------------------------------------------------
$VehicleListID["Bismark"] = $VehicleCount;
$VehicleList[$VehicleCount] = "Bismark";
$VehicleListData[$VehicleCount, "block"] = "Bismark";
$VehicleListData[$VehicleCount, "name"] = "Bismark Scout Carrier";
$VehicleListData[$VehicleCount, "desc"] = "Fast moving 2-gunner carrier that can take severe punishment and keep up with the fleet";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "role"] = $AIVehicleRole::Carrier | $AIVehicleRole::Scout;
$VehicleListData[$VehicleCount, "group"] = $VehicleGroup::Carriers;
$VehicleListData[$VehicleCount, "hardpoints"] = 0;
$VehicleListData[$VehicleCount, "suffixcode"] = "BIS";
$VehicleList::Bismark = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Bismark = $VehicleCount;

$VehicleMax["Bismark"] = 2;

$VehicleSeatProperties[$VehicleCount, 0] = $SeatProperties::Pilot;
$VehicleSeatProperties[$VehicleCount, 1] = $SeatProperties::Gunner;
$VehicleSeatProperties[$VehicleCount, 2] = $SeatProperties::Gunner;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "FBismarkStandard";
$VehicleCount++;

// RSS Sky Mistress
// -----------------------------------------------------------------------------
$VehicleListID["SkyMistress"] = $VehicleCount;
$VehicleList[$VehicleCount] = "SkyMistress";
$VehicleListData[$VehicleCount, "block"] = "SkyMistress";
$VehicleListData[$VehicleCount, "name"] = "Sky Mistress Troop Carrier";
$VehicleListData[$VehicleCount, "desc"] = "Pirate's life for me matey! Lighter and faster with 5 Gunner positions";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "role"] = $AIVehicleRole::Carrier | $AIVehicleRole::Support;
$VehicleListData[$VehicleCount, "group"] = $VehicleGroup::Carriers;
$VehicleListData[$VehicleCount, "hardpoints"] = 0;
$VehicleListData[$VehicleCount, "suffixcode"] = "SMS";
$VehicleList::SkyMistress = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::SkyMistress = $VehicleCount;

$VehicleMax["SkyMistress"] = 2;

$VehicleSeatProperties[$VehicleCount, 0] = $SeatProperties::Pilot;
$VehicleSeatProperties[$VehicleCount, 1] = $SeatProperties::Gunner;
$VehicleSeatProperties[$VehicleCount, 2] = $SeatProperties::Gunner;
$VehicleSeatProperties[$VehicleCount, 3] = $SeatProperties::Gunner;
$VehicleSeatProperties[$VehicleCount, 4] = $SeatProperties::Gunner;
$VehicleSeatProperties[$VehicleCount, 5] = $SeatProperties::Gunner;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "FSkyMistressStandard";
$VehicleCount++;

// Rhino Heavy APC
// -----------------------------------------------------------------------------
$VehicleListID["HAPCFlyer"] = $VehicleCount;
$VehicleList[$VehicleCount] = "HAPCFlyer";
$VehicleListData[$VehicleCount, "block"] = "HAPCFlyer";
$VehicleListData[$VehicleCount, "name"] = "Rhino Heavy APC";
$VehicleListData[$VehicleCount, "desc"] = "Let your gunners do the talking, 5 gunner positions";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "role"] = $AIVehicleRole::Carrier | $AIVehicleRole::Escort;
$VehicleListData[$VehicleCount, "group"] = $VehicleGroup::Carriers;
$VehicleListData[$VehicleCount, "hardpoints"] = 0;
$VehicleListData[$VehicleCount, "suffixcode"] = "RHI";
$VehicleList::HAPCFlyer = $VehicleListData[$VehicleCount, "mask"];
$VehicleList::Rhino = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::HAPCFlyer = $VehicleCount;
$VehicleID::Rhino = $VehicleCount;

$VehicleMax["HAPCFlyer"] = 2;

$VehicleSeatProperties[$VehicleCount, 0] = $SeatProperties::Pilot;
$VehicleSeatProperties[$VehicleCount, 1] = $SeatProperties::Gunner | $SeatProperties::Tailgunner;
$VehicleSeatProperties[$VehicleCount, 2] = $SeatProperties::Gunner;
$VehicleSeatProperties[$VehicleCount, 3] = $SeatProperties::Gunner;
$VehicleSeatProperties[$VehicleCount, 4] = $SeatProperties::Gunner;
$VehicleSeatProperties[$VehicleCount, 5] = $SeatProperties::Gunner;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "FRhinoStandard";
$VehicleCount++;

// Scorpion Destroyer
//----------------------------------------------------
$VehicleListID["Scorpion"] = $VehicleCount;
$VehicleList[$VehicleCount] = "Scorpion";
$VehicleListData[$VehicleCount, "block"] = "Scorpion";
$VehicleListData[$VehicleCount, "name"] = "Scorpion Destroyer";
$VehicleListData[$VehicleCount, "desc"] = "Heavy hitting weapons platform that sacrifices defenses for all-out damage";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "role"] = $AIVehicleRole::HasTurrets | $AIVehicleRole::Fighter;
$VehicleListData[$VehicleCount, "group"] = $VehicleGroup::Battleships;
$VehicleListData[$VehicleCount, "hardpoints"] = 5;
$VehicleListData[$VehicleCount, "suffixcode"] = "WOL"; // SCO
$VehicleList::Scorpion = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Scorpion = $VehicleCount;

$VehicleHardpoints[$VehicleCount, 0, "name"] = "[TC] Wing L Dual Cannon";
$VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Cannon;
$VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Hybrid;
$VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 0, "mountSuffix"] = "TPD";
$VehicleHardpoints[$VehicleCount, 1, "name"] = "[TC] Wing R Dual Cannon";
$VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Cannon;
$VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Hybrid;
$VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 5;
$VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 2;
$VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 2;
$VehicleHardpoints[$VehicleCount, 1, "mountSuffix"] = "TPD";
$VehicleHardpoints[$VehicleCount, 2, "name"] = "[TG] Pilot Turret Quad Guns";
$VehicleHardpoints[$VehicleCount, 2, "size"] = $VHardpointSize::Gun;
$VehicleHardpoints[$VehicleCount, 2, "type"] = $VHardpointType::Hybrid;
$VehicleHardpoints[$VehicleCount, 2, "mountPoint"] = 1;
$VehicleHardpoints[$VehicleCount, 2, "imageStart"] = 4;
$VehicleHardpoints[$VehicleCount, 2, "imageCount"] = 4;
$VehicleHardpoints[$VehicleCount, 2, "mountSuffix"] = "TPQ";
$VehicleHardpoints[$VehicleCount, 3, "name"] = "[M] Wing L Missile";
$VehicleHardpoints[$VehicleCount, 3, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 3, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 3, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 3, "imageStart"] = 6;
$VehicleHardpoints[$VehicleCount, 3, "imageCount"] = 1;
$VehicleHardpoints[$VehicleCount, 4, "name"] = "[M] Wing R Missile";
$VehicleHardpoints[$VehicleCount, 4, "size"] = $VHardpointSize::Missile;
$VehicleHardpoints[$VehicleCount, 4, "type"] = $VHardpointType::Missile;
$VehicleHardpoints[$VehicleCount, 4, "mountPoint"] = 0;
$VehicleHardpoints[$VehicleCount, 4, "imageStart"] = 7;
$VehicleHardpoints[$VehicleCount, 4, "imageCount"] = 1;

$VehicleHardpoints[$VehicleCount, 6, "position"] = "1.5 6 -4.25";
$VehicleHardpoints[$VehicleCount, 6, "rotation"] = "0 1 0 0";
$VehicleHardpoints[$VehicleCount, 7, "position"] = "-1.5 6 -4.25";
$VehicleHardpoints[$VehicleCount, 7, "rotation"] = "0 1 0 0";

$VehicleMax["Scorpion"] = 2;

$VehicleSeatProperties[$VehicleCount, 0] = $SeatProperties::Pilot | $SeatProperties::LaunchMissiles;
$VehicleSeatProperties[$VehicleCount, 1] = 0;
$VehicleSeatProperties[$VehicleCount, 2] = 0;
$VehicleSeatProperties[$VehicleCount, 3] = $SeatProperties::Gunner | $SeatProperties::ControlsTurret;
$VehicleSeatProperties[$VehicleCount, 4] = $SeatProperties::Gunner | $SeatProperties::ControlsTurret;
$VehicleSeatProperties[$VehicleCount, 5] = 0;

$TurretNodes[$VehicleCount] = 0;

$TurretControlNode[$VehicleCount, 3] = 2;
$TurretNodes[$VehicleCount]++;

$TurretControlNode[$VehicleCount, 4] = 5;
$TurretNodes[$VehicleCount]++;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "VShieldNone";
$DefaultVehicleWep[$VehicleCount, 0] = "LDisruptor";
$DefaultVehicleWep[$VehicleCount, 1] = "LAutoCannon";
$DefaultVehicleWep[$VehicleCount, 2] = "GBlaster";
$DefaultVehicleWep[$VehicleCount, 3] = "MJavelin";
$DefaultVehicleWep[$VehicleCount, 4] = "MJavelin";
$VehicleCount++;

if($Host::EnableSpecialVehicles)
{
    // Viper
    // -----------------------------------------------------------------------------
    $VehicleListID["Viper"] = $VehicleCount;
    $VehicleList[$VehicleCount] = "Viper";
    $VehicleListData[$VehicleCount, "block"] = "Viper";
    $VehicleListData[$VehicleCount, "name"] = "Viper Interceptor";
    $VehicleListData[$VehicleCount, "desc"] = "Notice how there aren't any cylons? You're welcome.";
    $VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
    $VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
    $VehicleListData[$VehicleCount, "role"] = $AIVehicleRole::SingleSeater | $AIVehicleRole::Scout;
    $VehicleListData[$VehicleCount, "group"] = $VehicleGroup::Interceptors;
    $VehicleListData[$VehicleCount, "hardpoints"] = 3;
    $VehicleListData[$VehicleCount, "suffixcode"] = "VIP";
    $VehicleList::Viper = $VehicleListData[$VehicleCount, "mask"];
    $VehicleID::Viper = $VehicleCount;

    $VehicleHardpoints[$VehicleCount, 0, "name"] = "[G] Nose Mount";
    $VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Gun;
    $VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Omni;
    $VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 0;
    $VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 2;
    $VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 1;
    $VehicleHardpoints[$VehicleCount, 1, "name"] = "[G] Right Wing Missile";
    $VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Missile;
    $VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Missile;
    $VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 0;
    $VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 6;
    $VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 1;
    $VehicleHardpoints[$VehicleCount, 2, "name"] = "[M] Left Wing Missile";
    $VehicleHardpoints[$VehicleCount, 2, "size"] = $VHardpointSize::Missile;
    $VehicleHardpoints[$VehicleCount, 2, "type"] = $VHardpointType::Missile;
    $VehicleHardpoints[$VehicleCount, 2, "mountPoint"] = 0;
    $VehicleHardpoints[$VehicleCount, 2, "imageStart"] = 7;
    $VehicleHardpoints[$VehicleCount, 2, "imageCount"] = 1;

    $VehicleHardpoints[$VehicleCount, 2, "position"] = "0 9.5 -1.5";
    $VehicleHardpoints[$VehicleCount, 2, "rotation"] = "1 0 0 0";
    $VehicleHardpoints[$VehicleCount, 4, "position"] = "2.5 3.9 -1.25";
    $VehicleHardpoints[$VehicleCount, 4, "rotation"] = "1 0 0 0";
    $VehicleHardpoints[$VehicleCount, 5, "position"] = "-2.5 3.9 -1.25";
    $VehicleHardpoints[$VehicleCount, 5, "rotation"] = "1 0 0 0";
    $VehicleHardpoints[$VehicleCount, 6, "position"] = "3.35 -1.75 -1.85";
    $VehicleHardpoints[$VehicleCount, 6, "rotation"] = "1 0 0 0";
    $VehicleHardpoints[$VehicleCount, 7, "position"] = "-3.4 -1.75 -1.75";
    $VehicleHardpoints[$VehicleCount, 7, "rotation"] = "1 0 0 0";

    $VehicleMax["Viper"] = 3;

    $VehicleSeatProperties[$VehicleCount, 0] = $SeatProperties::Pilot | $SeatProperties::LaunchMissiles | $SeatProperties::Gunner;

    $DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
    $DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
    $DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "FViperStandard";
    $DefaultVehicleWep[$VehicleCount, 0] = "GBlaster";
    $DefaultVehicleWep[$VehicleCount, 1] = "MStinger";
    $DefaultVehicleWep[$VehicleCount, 2] = "MStinger";
    $VehicleCount++;

    // Pyro-GX Heavy Interceptor
    // -----------------------------------------------------------------------------
    $VehicleListID["PyroGX"] = $VehicleCount;
    $VehicleList[$VehicleCount] = "PyroGX";
    $VehicleListData[$VehicleCount, "block"] = "PyroGX";
    $VehicleListData[$VehicleCount, "name"] = "Pyro-GX Heavy Interceptor";
    $VehicleListData[$VehicleCount, "desc"] = "Large target with large armaments";
    $VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
    $VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
    $VehicleListData[$VehicleCount, "role"] = $AIVehicleRole::SingleSeater | $AIVehicleRole::Fighter;
    $VehicleListData[$VehicleCount, "group"] = $VehicleGroup::Interceptors;
    $VehicleListData[$VehicleCount, "hardpoints"] = 5;
    $VehicleListData[$VehicleCount, "suffixcode"] = "PGX";
    $VehicleList::PyroGX = $VehicleListData[$VehicleCount, "mask"];
    $VehicleID::PyroGX = $VehicleCount;

    $VehicleHardpoints[$VehicleCount, 0, "name"] = "[G] Dual Wing Mounts";
    $VehicleHardpoints[$VehicleCount, 0, "size"] = $VHardpointSize::Gun;
    $VehicleHardpoints[$VehicleCount, 0, "type"] = $VHardpointType::Energy;
    $VehicleHardpoints[$VehicleCount, 0, "mountPoint"] = 0;
    $VehicleHardpoints[$VehicleCount, 0, "imageStart"] = 2;
    $VehicleHardpoints[$VehicleCount, 0, "imageCount"] = 2;
    $VehicleHardpoints[$VehicleCount, 1, "name"] = "[M] Right Wing Missile 1";
    $VehicleHardpoints[$VehicleCount, 1, "size"] = $VHardpointSize::Missile;
    $VehicleHardpoints[$VehicleCount, 1, "type"] = $VHardpointType::Missile;
    $VehicleHardpoints[$VehicleCount, 1, "mountPoint"] = 0;
    $VehicleHardpoints[$VehicleCount, 1, "imageStart"] = 4;
    $VehicleHardpoints[$VehicleCount, 1, "imageCount"] = 1;
    $VehicleHardpoints[$VehicleCount, 2, "name"] = "[M] Left Wing Missile 1";
    $VehicleHardpoints[$VehicleCount, 2, "size"] = $VHardpointSize::Missile;
    $VehicleHardpoints[$VehicleCount, 2, "type"] = $VHardpointType::Missile;
    $VehicleHardpoints[$VehicleCount, 2, "mountPoint"] = 0;
    $VehicleHardpoints[$VehicleCount, 2, "imageStart"] = 5;
    $VehicleHardpoints[$VehicleCount, 2, "imageCount"] = 1;
    $VehicleHardpoints[$VehicleCount, 3, "name"] = "[M] Right Wing Missile 2";
    $VehicleHardpoints[$VehicleCount, 3, "size"] = $VHardpointSize::Missile;
    $VehicleHardpoints[$VehicleCount, 3, "type"] = $VHardpointType::Missile;
    $VehicleHardpoints[$VehicleCount, 3, "mountPoint"] = 0;
    $VehicleHardpoints[$VehicleCount, 3, "imageStart"] = 6;
    $VehicleHardpoints[$VehicleCount, 3, "imageCount"] = 1;
    $VehicleHardpoints[$VehicleCount, 4, "name"] = "[M] Left Wing Missile 2";
    $VehicleHardpoints[$VehicleCount, 4, "size"] = $VHardpointSize::Missile;
    $VehicleHardpoints[$VehicleCount, 4, "type"] = $VHardpointType::Missile;
    $VehicleHardpoints[$VehicleCount, 4, "mountPoint"] = 0;
    $VehicleHardpoints[$VehicleCount, 4, "imageStart"] = 7;
    $VehicleHardpoints[$VehicleCount, 4, "imageCount"] = 1;

    $VehicleHardpoints[$VehicleCount, 2, "position"] = "3.1 -0.5 -1.5";
    $VehicleHardpoints[$VehicleCount, 2, "rotation"] = "1 0 0 0";
    $VehicleHardpoints[$VehicleCount, 3, "position"] = "-3.1 -0.5 -1.5";
    $VehicleHardpoints[$VehicleCount, 3, "rotation"] = "1 0 0 0";
    $VehicleHardpoints[$VehicleCount, 4, "position"] = "3.75 1.75 -1.8";
    $VehicleHardpoints[$VehicleCount, 4, "rotation"] = "1 0 0 0";
    $VehicleHardpoints[$VehicleCount, 5, "position"] = "-3.75 1.75 -1.8";
    $VehicleHardpoints[$VehicleCount, 5, "rotation"] = "1 0 0 0";
    $VehicleHardpoints[$VehicleCount, 6, "position"] = "3.75 1.75 -1.6";
    $VehicleHardpoints[$VehicleCount, 6, "rotation"] = "1 0 0 0";
    $VehicleHardpoints[$VehicleCount, 7, "position"] = "-3.75 1.75 -1.6";
    $VehicleHardpoints[$VehicleCount, 7, "rotation"] = "1 0 0 0";

    $VehicleMax["PyroGX"] = 3;

    $VehicleSeatProperties[$VehicleCount, 0] = $SeatProperties::Pilot | $SeatProperties::LaunchMissiles | $SeatProperties::Gunner;

    $DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
    $DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
    $DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "VShieldNone";
    $DefaultVehicleWep[$VehicleCount, 0] = "GBlaster";
    $DefaultVehicleWep[$VehicleCount, 1] = "MStinger";
    $DefaultVehicleWep[$VehicleCount, 2] = "MStinger";
    $DefaultVehicleWep[$VehicleCount, 3] = "MStinger";
    $DefaultVehicleWep[$VehicleCount, 4] = "MStinger";
    $VehicleCount++;
}

// Goodship - placeholder
// -----------------------------------------------------------------------------
$VehicleListID["Goodship"] = $VehicleCount;
$VehicleList[$VehicleCount] = "Goodship";
$VehicleListData[$VehicleCount, "block"] = "";
$VehicleListData[$VehicleCount, "name"] = "Goodship";
$VehicleListData[$VehicleCount, "desc"] = "Test Vehicle";
$VehicleListData[$VehicleCount, "mask"] = 1 << $VehicleCount;
$VehicleListData[$VehicleCount, "class"] = $VehicleClass::Flying;
$VehicleListData[$VehicleCount, "group"] = $VehicleGroup::Dreadnoughts;
$VehicleListData[$VehicleCount, "hardpoints"] = 0;
$VehicleListData[$VehicleCount, "suffixcode"] = "GOO";
$VehicleList::Goodship = $VehicleListData[$VehicleCount, "mask"];
$VehicleID::Goodship = $VehicleCount;

$VehicleMax["Goodship"] = 3;

$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Armor] = "VArmorNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Module] = "VModuleNone";
$DefaultVehiclePart[$VehicleCount, $VehiclePartType::Shield] = "VShieldNone";
$VehicleCount++;

// Turret positions
$VehicleID::Underbelly = 90;
$VehicleListData[$VehicleID::Underbelly, "suffixcode"] = "UBT";
$VehicleHardpoints[$VehicleID::Underbelly, 2, "position"] = "0 0 0";
$VehicleHardpoints[$VehicleID::Underbelly, 2, "rotation"] = "1 0 0 0";
$VehicleHardpoints[$VehicleID::Underbelly, 3, "position"] = "0 0 0";
$VehicleHardpoints[$VehicleID::Underbelly, 3, "rotation"] = "1 0 0 0";
$VehicleHardpoints[$VehicleID::Underbelly, 4, "position"] = "2 -4 -0.5";
$VehicleHardpoints[$VehicleID::Underbelly, 4, "rotation"] = "1 0 0 0";
$VehicleHardpoints[$VehicleID::Underbelly, 5, "position"] = "-2 -4 -0.5";
$VehicleHardpoints[$VehicleID::Underbelly, 5, "rotation"] = "1 0 0 0";

$VehicleID::TurretPole = 91;
$VehicleListData[$VehicleID::TurretPole, "suffixcode"] = "TUS";
$VehicleHardpoints[$VehicleID::TurretPole, 2, "position"] = "0 0.6 0";
$VehicleHardpoints[$VehicleID::TurretPole, 2, "rotation"] = "1 0 0 0";
$VehicleHardpoints[$VehicleID::TurretPole, 4, "position"] = "0.5 0.75 0";
$VehicleHardpoints[$VehicleID::TurretPole, 4, "rotation"] = "0 1 0 -90";
$VehicleHardpoints[$VehicleID::TurretPole, 5, "position"] = "-0.5 0.75 0";
$VehicleHardpoints[$VehicleID::TurretPole, 5, "rotation"] = "0 1 0 90";

$VehicleID::TurretBase = 92;
$VehicleListData[$VehicleID::TurretBase, "suffixcode"] = "TUS";
$VehicleHardpoints[$VehicleID::TurretBase, 2, "position"] = "0 -0.75 0";
$VehicleHardpoints[$VehicleID::TurretBase, 2, "rotation"] = "1 0 0 0";
$VehicleHardpoints[$VehicleID::TurretBase, 4, "position"] = "0.45 0.1 0";
$VehicleHardpoints[$VehicleID::TurretBase, 4, "rotation"] = "0 1 0 -90";
$VehicleHardpoints[$VehicleID::TurretBase, 5, "position"] = "-0.45 0.1 0";
$VehicleHardpoints[$VehicleID::TurretBase, 5, "rotation"] = "0 1 0 90";

$VehicleID::TankTurret = 93;
$VehicleListData[$VehicleID::TankTurret, "suffixcode"] = "TUT";
$VehicleHardpoints[$VehicleID::TankTurret, 2, "position"] = "0 0 0";
$VehicleHardpoints[$VehicleID::TankTurret, 2, "rotation"] = "1 0 0 0";
$VehicleHardpoints[$VehicleID::TankTurret, 4, "position"] = "-0.15 0.1 0";
$VehicleHardpoints[$VehicleID::TankTurret, 4, "rotation"] = "1 0 0 0";
$VehicleHardpoints[$VehicleID::TankTurret, 5, "position"] = "0.15 0.1 0";
$VehicleHardpoints[$VehicleID::TankTurret, 5, "rotation"] = "1 0 0 0";

$VehicleID::TurretDualQuad = 94;
$VehicleListData[$VehicleID::TurretDualQuad, "suffixcode"] = "TPD";
$VehicleHardpoints[$VehicleID::TurretDualQuad, 2, "position"] = "0.4 0.75 0";
$VehicleHardpoints[$VehicleID::TurretDualQuad, 2, "rotation"] = "0 1 0 -90";
$VehicleHardpoints[$VehicleID::TurretDualQuad, 3, "position"] = "-0.4 0.75 0";
$VehicleHardpoints[$VehicleID::TurretDualQuad, 3, "rotation"] = "0 1 0 90";

$VehicleHardpoints[$VehicleID::TurretDualQuad, 4, "position"] = "0.15 0.25 0.25";
$VehicleHardpoints[$VehicleID::TurretDualQuad, 4, "rotation"] = "0 1 0 -90";
$VehicleHardpoints[$VehicleID::TurretDualQuad, 5, "position"] = "-0.15 0.25 0.25";
$VehicleHardpoints[$VehicleID::TurretDualQuad, 5, "rotation"] = "0 1 0 90";
$VehicleHardpoints[$VehicleID::TurretDualQuad, 6, "position"] = "0.15 0.25 -0.25";
$VehicleHardpoints[$VehicleID::TurretDualQuad, 6, "rotation"] = "0 1 0 -90";
$VehicleHardpoints[$VehicleID::TurretDualQuad, 7, "position"] = "-0.15 0.25 -0.25";
$VehicleHardpoints[$VehicleID::TurretDualQuad, 7, "rotation"] = "0 1 0 90";

// Loop through all vehicles and add +0 to the size/type lines to convert them to int

// Shortcut bitmasks
$VehicleList::All = -1;
$VehicleList::General = -1; // whoa how did this happen, let's get rid of this

// Group definitions
$VehicleList::Sizes = $VehicleList::Skycutter | $VehicleList::Airhawk | $VehicleList::Wildcat | $VehicleList::Ameri | $VehicleList::Shrike | $VehicleList::ShrikeX | $VehicleList::SuperShrike | $VehicleList::Starhawk | $VehicleList::Firebird | $VehicleList::HoverJeep | $VehicleList::Catapult | $VehicleList::Tomahawk | $VehicleList::Guillotine | $VehicleList::Retaliator | $VehicleList::Stryker | $VehicleList::Wolfhound | $VehicleList::Annihilator | $VehicleList::Leviathan | $VehicleList::Bismark | $VehicleList::SkyMistress | $VehicleList::HAPCFlyer | $VehicleList::Viper | $VehicleList::PyroGX | $VehicleList::Scorpion;
$VehicleList::AllFighters = $VehicleList::Skycutter | $VehicleList::Airhawk | $VehicleList::Wildcat | $VehicleList::Ameri | $VehicleList::Shrike | $VehicleList::ShrikeX | $VehicleList::Starhawk | $VehicleList::Firebird;
$VehicleList::Fighters = $VehicleList::ShrikeX | $VehicleList::Starhawk | $VehicleList::Firebird | $VehicleList::Shrike;
$VehicleList::FighterMulti = $VehicleList::Skycutter | $VehicleList::Airhawk | $VehicleList::Wildcat | $VehicleList::Ameri | $VehicleList::Shrike | $VehicleList::ShrikeX | $VehicleList::Starhawk | $VehicleList::Firebird | $VehicleList::SuperShrike | $VehicleList::Catapult;
$VehicleList::Gunships = $VehicleList::SuperShrike | $VehicleList::HoverJeep | $VehicleList::Catapult | $VehicleList::Retaliator | $VehicleList::Stryker | $VehicleList::Scorpion;
$VehicleList::Battleships = $VehicleList::Wolfhound | $VehicleList::Annihilator | $VehicleList::Leviathan;
$VehicleList::Carriers = $VehicleList::Bismark | $VehicleList::SkyMistress | $VehicleList::HAPCFlyer;
$VehicleList::Tanks = $VehicleList::Guillotine | $VehicleList::HoverJeep;
$VehicleList::VTOL = $VehicleList::HoverJeep | $VehicleList::Annihilator | $VehicleList::Leviathan | $VehicleList::Bismark | $VehicleList::HAPCFlyer;
$VehicleList::HasTurrets = $VehicleList::SuperShrike | $VehicleList::HoverJeep | $VehicleList::Guillotine | $VehicleList::Retaliator | $VehicleList::Stryker | $VehicleList::Wolfhound | $VehicleList::Annihilator | $VehicleList::Leviathan | $VehicleList::Scorpion;
$VehicleList::UnderbellyTurrets = $VehicleList::SuperShrike | $VehicleList::Retaliator;
$VehicleList::GunnerTurrets = $VehicleList::HoverJeep | $VehicleList::Guillotine | $VehicleList::Stryker | $VehicleList::Wolfhound | $VehicleList::Annihilator | $VehicleList::Leviathan | $VehicleList::Scorpion;

$VehicleList::HasShields = $VehicleList::Wildcat | $VehicleList::Shrike | $VehicleList::SuperShrike | $VehicleList::Starhawk | $VehicleList::Firebird | $VehicleList::HoverJeep | $VehicleList::Catapult | $VehicleList::Tomahawk | $VehicleList::Guillotine | $VehicleList::Retaliator | $VehicleList::Wolfhound | $VehicleList::Annihilator | $VehicleList::Leviathan | $VehicleList::Bismark | $VehicleList::SkyMistress | $VehicleList::HAPCFlyer | $VehicleList::Viper;
$VehicleList::HasCrew = $VehicleList::SuperShrike | $VehicleList::HoverJeep | $VehicleList::Catapult | $VehicleList::Tomahawk | $VehicleList::Guillotine | $VehicleList::Retaliator | $VehicleList::Stryker | $VehicleList::Wolfhound | $VehicleList::Annihilator | $VehicleList::Leviathan | $VehicleList::Bismark | $VehicleList::SkyMistress | $VehicleList::HAPCFlyer | $VehicleList::Scorpion;
$VehicleList::HasMissiles = $VehicleList::Airhawk | $VehicleList::Wildcat | $VehicleList::Ameri | $VehicleList::Shrike | $VehicleList::ShrikeX | $VehicleList::SuperShrike | $VehicleList::Starhawk | $VehicleList::HoverJeep | $VehicleList::Catapult | $VehicleList::Tomahawk | $VehicleList::Guillotine | $VehicleList::Retaliator | $VehicleList::Stryker | $VehicleList::Wolfhound | $VehicleList::Viper | $VehicleList::PyroGX | $VehicleList::Scorpion;
$VehicleList::HasAmmo =  $VehicleList::Skycutter | $VehicleList::Airhawk | $VehicleList::Wildcat | $VehicleList::Shrike | $VehicleList::ShrikeX | $VehicleList::SuperShrike | $VehicleList::Starhawk | $VehicleList::Firebird | $VehicleList::HoverJeep | $VehicleList::Catapult | $VehicleList::Tomahawk | $VehicleList::Guillotine | $VehicleList::Retaliator | $VehicleList::Stryker | $VehicleList::Wolfhound | $VehicleList::Annihilator | $VehicleList::Leviathan | $VehicleList::Viper | $VehicleList::PyroGX | $VehicleList::Scorpion;

// -----------------------------------------------------------------------------

function ShapeBase::vehicleCheckType(%obj, %typeMask)
{
    return $VehicleListData[%obj.vid, "mask"] & %typeMask;
}

function getRandomVehicleType(%typeMask)
{
    %vc = 0;

    for(%i = 0; %i < $VehicleCount; %i++)
    {
        if($VehicleListData[%i, "mask"] & %typeMask)
        {
            %va[%vc] = %i;
            %vc++;
        }
    }
    
    if(%vc)
    {
        %vc--;
        return %vc > 0 ? $VehicleListData[%va[mFloor(getRandom() * %vc)], "block"] : $VehicleListData[%va[0], "block"];
    }
    else
        return "ScoutFlier";
}

function testSlotCanMount(%vid, %slot, %wep)
{
    return ($VehicleHardpoints[%vid, %slot, "size"] == %wep.mountSize && $VehicleHardpoints[%vid, %slot, "type"] & %wep.mountType && $VehicleListData[%vid, "mask"] & %wep.wearableMask);
}

function getRandomWeapon(%vid, %slot)
{
    %vwep = "XEmptyHardpoint";
    %wc = 0;
    
    %numWeapons = VehiclePart.vehiclePartCount[$VehiclePartType::Weapon];

    for(%i = 0; %i < %numWeapons; %i++)
    {
        %potwep = VehiclePart.vehicleParts[$VehiclePartType::Weapon, %i].getName();
        
        if(%potwep $= "" || %potwep $= %vwep)
            continue;
            
        if(testSlotCanMount(%vid, %slot, %potwep))
        {
            %pw[%wc] = %potwep;
            %wc++;
        }
    }
    
    if(%wc)
    {
        %wc--;
        %vwep = %wc > 0 ? %pw[mFloor(getRandom() * %wc)] : %pw[0];
    }
    
    return %vwep;
}

function dbgRandomWeapon(%vid, %slot)
{
    %vwep = "XEmptyHardpoint";
    %wc = 0;

    echo("Vehicle:" SPC $VehicleList[%vid] SPC "slot:" SPC %slot SPC $VehicleHardpoints[%vid, %slot, "name"]);
    
    %numWeapons = VehiclePart.vehiclePartCount[$VehiclePartType::Weapon];

    for(%i = 0; %i < %numWeapons; %i++)
    {
        %potwep = VehiclePart.vehicleParts[$VehiclePartType::Weapon, %i].getName();

        if(%potwep $= "" || %potwep $= %vwep)
            continue;

//        echo("Wep" SPC %potwep SPC "slot size" SPC $VehicleHardpoints[%vid, %slot, "size"] SPC "weapon size" SPC %potwep.mountSize);
        
        if($VehicleHardpoints[%vid, %slot, "size"] == %potwep.mountSize)
        {
            %typematch = $VehicleHardpoints[%vid, %slot, "type"] & %potwep.mountType;
            %vehmatch = $VehicleListData[%vid, "mask"] & %potwep.wearableMask;

            echo("Found compatible size weapon" SPC %potwep SPC "slot type match:" SPC %typematch SPC "vehicle match" SPC %vehmatch);
            
            if(%typematch && %vehmatch)
            {
                %pw[%wc] = %potwep;
                %wc++;
            }
        }
    }

    if(%wc)
    {
        %wc--;
        %vwep = %wc > 0 ? %pw[mFloor(getRandom() * %wc)] : %pw[0];
    }

    echo("Found weapon:" SPC %vwep);
}

function serverCmdSendVehicleInventory(%client)
{
    %client.hasClient = true;
    %client.sendVehicleInventory();
}

function serverCmdVEditSetLoadout(%client, %vid, %vname, %weaponsList, %modulesList)
{
//    echo("VEditSetLoadout:" SPC %client SPC %vid SPC %vname SPC "|" SPC %weaponsList SPC "|" SPC %modulesList);
    
    // Convert vehicle back from datablock to internal ID
    %vid = $VehicleListID[%vid];
    
    // Preset name (capped to 24 chars)
    %vname = getSubStr(trim(%vname), 0, 24);
    %client.pendingVehicleName[%vid] = getWord(%vname, 0) !$= "Preset" ? %vname : "";
    
    // Convert weapon numbers back to their object names
    for(%i = 0; %i < 6; %i++)
    {
        %t = %i + 1; // Client sends initial tab in data, so 1-indexed
        %w = %i + 3; // Original MD3 weapon data setup - module, shield, armor, w1-6
        %wid = trim(getField(%weaponsList, %t));

        if(!isObject(%wid))
            %wid = "";
            
        %client.pendingVehicleMod[%w, %vid] = %wid;
    }
    
    // And the module
    %mid = trim(getField(%modulesList, 1));

    if(!isObject(%mid))
        %mid = "";

    %client.pendingVehicleMod[$VehiclePartType::Module, %vid] = %mid;

    // And the shield
    %sid = trim(getField(%modulesList, 2));
    
    if(!isObject(%sid))
        %sid = "";

    %client.pendingVehicleMod[$VehiclePartType::Shield, %vid] = %sid;

//    %temp = "";

//    for(%t = 0; %t < 8; %t++)
//        %temp = %temp SPC %t@":"@%client.pendingVehicleMod[%t, %vid];

//    echo("received items from client" SPC %client SPC ":"@%temp);
}

function GameConnection::sendVehicleInventory(%client) // C Component (module), F Field (Shield), S Small, M Medium, L Large, XL XLarge
{
    for(%i = 0; %i < $VehicleCount; %i++)
    {
        %block = $VehicleListData[%i, "block"];
        
        if(%block $= "")
            continue;
        
        %len = strlen(%block.shapeFile);
        %blockname = getSubStr(%block.shapeFile, 0, %len - 4);

        commandToClient(%client, 'RegisterVehicle', $VehicleListData[%i, "block"], $VehicleListData[%i, "name"], $VehicleListData[%i, "mask"], mFloor(%block.mass * 100), mFloor(%block.maxDamage * 100), 0, (%block.rechargeRate * $g_TickTime), %blockname, $VehicleListData[%i, "desc"]);

        for(%h = 0; %h < $VehicleListData[%i, "hardpoints"]; %h++)
            commandToClient(%client, 'RegisterSlot', $VehicleHardpoints[%i, %h, "type"], $VehicleHardpoints[%i, %h, "size"], $VehicleHardpoints[%i, %h, "name"]);
    }

    // start %i at 1 because 0 are placeholders which are needed for some weird reason
    %numWeapons = VehiclePart.vehiclePartCount[$VehiclePartType::Weapon];

    for(%i = 0; %i < %numWeapons; %i++)
    {
        %vwep = VehiclePart.vehicleParts[$VehiclePartType::Weapon, %i];

        commandToClient(%client, 'RegisterWeapon', %vwep.getName(), %vwep.name, %vwep.mountType, %vwep.mountSize, %vwep.wearableMask);
        commandToClient(%client, 'RegisterWeaponText', %vwep.description);
    }

    %numModules = VehiclePart.vehiclePartCount[$VehiclePartType::Module];

    for(%i = 0; %i < %numModules; %i++)
    {
        %vmod = VehiclePart.vehicleParts[$VehiclePartType::Module, %i];

        commandToClient(%client, 'RegisterModule', %vmod.getName(), %vmod.name, "Module", %vmod.wearableMask, %vmod.deltaMass, %vmod.deltaArmor, %vmod.deltaShield, %vmod.deltaRegen, %vmod.description);
    }

    %numShields = VehiclePart.vehiclePartCount[$VehiclePartType::Shield];

    for(%i = 0; %i < %numShields; %i++)
    {
        %vshield = VehiclePart.vehicleParts[$VehiclePartType::Shield, %i];

        commandToClient(%client, 'RegisterModule', %vshield.getName(), %vshield.name, "Shield", %vshield.wearableMask, %vshield.deltaMass, %vshield.deltaArmor, %vshield.deltaShield, %vshield.deltaRegen, %vshield.description);
    }

    commandToClient(%client, 'EndVehicleEditData');
}

function VehiclePart::registerVehiclePart(%this, %type, %part, %name, %desc, %mask, %size, %slottype)
{
    // No duplication!
    if(isObject(%part))
        return;

    %newPart = new ScriptObject(%part)
    {
        class = %part;
        superClass = VehiclePart;
    };

    if(%size $= "")
        %size = $VHardpointSize::Internal;
        
    if(%slottype $= "")
        %slottype = $VHardpointType::Anything;
        
    %newPart.name = %name;
    %newPart.description = %desc;
    %newPart.wearableMask = %mask;
    %newPart.mountSize = %size;
    %newPart.mountType = %slottype;
    %newPart.partType = %type;
    %newPart.onInit();

    // Store index of where this object is
    if(%this.vehiclePartCount[%type] $= "")
        %this.vehiclePartCount[%type] = 0;

    %this.vehicleParts[%type, %this.vehiclePartCount[%type]] = %newPart.getName();
    %this.partID[%type, %newPart.getName()] = %this.vehiclePartCount[%type];
    
    // Next data
    %this.vehiclePartCount[%type]++;
}

function VehiclePart::onInit(%this)
{
    // Set prefs here
}

function VehiclePart::validateUse(%this, %obj)
{
    return true;
}

// Default setup here
function VehiclePart::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    // Default weapon part installation script
    if(%this.partType == $VehiclePartType::Weapon)
    {
        %suffix = $VehicleListData[%vehicle.vid, "suffixcode"];
        %slot = $VehicleHardpoints[%vehicle.vid, %hardpoint, "imageStart"];
        %run = $VehicleHardpoints[%vehicle.vid, %hardpoint, "imageCount"];
        %mp = $VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"];
        %mount = %mp > 0 ? %vehicle.getMountNodeObject(%mp) : %vehicle;

        if(%mp > 0)
            %suffix = $VehicleHardpoints[%vehicle.vid, %hardpoint, "mountSuffix"];

        for(%i = 0; %i < %run; %i++)
        {
            %img = %this.getName()@%suffix@(%slot+%i);
            
            if(!isObject(%img))
                error("Vehicle Image construction failure:" SPC %img SPC $VehicleHardpoints[%vehicle.vid, %hardpoint, "mountSuffix"] SPC %vehicle.vid SPC %hardpoint);
                
            %mount.mountImage(%img, %slot+%i);
        }
    }
}

// Only for use with weapons for now
function VehiclePart::uninstallPart(%this, %vehicle, %hardpoint)
{
    %slot = $VehicleHardpoints[%vehicle.vid, %hardpoint, "imageStart"];
    %run = $VehicleHardpoints[%vehicle.vid, %hardpoint, "imageCount"];
//    %mp = $VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"];
//    %mount = %mp > 0 ? %vehicle.getMountNodeObject(%mp) : %vehicle;

    %vehicle.ammoCache[%this.ammo] -= mCeil(%this.ammoCount * $VehicleHardpoints[%vehicle.vid, %hardpoint, "imageCount"] * %vehicle.maxAmmoCapacityFactor);
    
    if(%vehicle.ammoCache[%this.ammo] < 0)
        %vehicle.ammoCache[%this.ammo] = 0;

//    for(%i = 0; %i < %run; %i++)
//        %mount.unmountImage(%slot+%i);
}

function VehiclePart::triggerToggle(%this, %data, %vehicle, %player, %state)
{
    // Toggle vehicle module on/off
}

function VehiclePart::triggerPush(%this, %data, %vehicle, %player)
{
    // Push once to activate
}

function VehiclePart::onSwitchWeapon(%this, %vehicle, %client, %wepNum)
{
    for(%i = 0; %i < 8; %i++)
        %vehicle.setImageTrigger(%i, false);
}

function VehicleData::installParts(%data, %obj, %player)
{
    if(%obj.partsInstalled)
        return;

    %mapSpawned = !isObject(%player);
    %obj.partsInstalled = true;
    %obj.rechargeTick = %data.rechargeRate;
    %obj.isShielded = false;

    %vid = $VehicleListID[%data];
    %obj.vid = %vid;
    
    if(%player.client.isAIControlled())
        %obj.nameOverride = AIGenerateRandomShipName();
    else
        %obj.nameOverride = %player.client.pendingVehicleName[%vid];
        
    if(%obj.nameOverride !$= "")
    {
        if(%obj.nameOverride $= "Random" || %obj.nameOverride $= "random")
            %obj.nameOverride = AIGenerateRandomShipName();
            
        freeTarget(%obj.target);
        %obj.target = createTarget(%obj, addTaggedString(%obj.nameOverride), "", "", %data.targetTypeTag, 0, 0);
//        echo("Added tagged string for vehicle:" SPC %obj SPC %obj.nameOverride);
    }
    else
        %obj.nameOverride = $VehicleListData[%obj.vid, "name"];
        
    %obj.installedParts = 0;
    
    // Two pass system - base vehicle enhancements
    for(%i = 0; %i < 3; %i++)
    {
        %partID = "";

        if(!%mapSpawned)
            %partID = %player.client.pendingVehicleMod[%i, %vid];

        %part = %partID $= "" ? $DefaultVehiclePart[%vid, %i] : VehiclePart.vehicleParts[%i, VehiclePart.partID[%i, %partID]];
        
        %obj.installedVehiclePart[%i] = %part;
        %obj.installedParts++;
        %part.installPart(%data, %obj, %player, %i);
    }

    // Then weapons
    for(%i = 0; %i < 6; %i++)
    {
        %partID = "";
        %w = %i + 3;
        
        if(!%mapSpawned)
        {
            if(%player.client.pendingVehicleMod[%w, %vid] !$= "")
            {
                %partID = %player.client.pendingVehicleMod[%w, %vid];
                
                if(!testSlotCanMount(%vid, %i, %partId))
                    continue;
            }
            else if(%player.client.isAIControlled()) // Bots get randomized loadouts if there are no pending presets set
                %partID = getRandomWeapon(%vid, %i);
        }

        %part = %partID !$= "" ? %partID : $DefaultVehicleWep[%vid, %i]; // $VehicleListID[%data]

        // simply not defined/empty hardpoint
        if(%part $= "" || %part $= "XEmptyHardpoint")
            continue;

        %obj.installedVehiclePart[%w] = %part;
        %obj.installedParts++;
        
        %tgtPtNum = $VehicleHardpoints[%vid, %i, "mountPoint"];

//        echo("Installing part:" SPC %part SPC %i SPC %w SPC "on hardpoint" SPC %tgtPtNum SPC $VehicleHardpoints[%vid, %i, "imageStart"]);
        
        if(%tgtPtNum > 0)
        {
            %tgtPtObj = %obj.getMountNodeObject(%tgtPtNum);

            if($VehicleHardpoints[%vid, %i, "type"] != $VHardpointType::Missile)
            {
                if(%tgtPtObj.wepCount $= "")
                    %tgtPtObj.wepCount = 0;

                %tgtPtObj.weapon[%tgtPtObj.wepCount] = %part;
                %tgtPtObj.hardpointRef[%tgtPtObj.wepCount] = %i;
                %tgtPtObj.wepCount++;
            }
            else
            {
                if(%tgtPtObj.installedMissiles $= "")
                    %tgtPtObj.installedMissiles = 0;

                %tgtPtObj.missile[%tgtPtObj.installedMissiles] = %part;
                %tgtPtObj.installedMissiles++;
            }
        }
        else if(%tgtPtNum == 0)
        {
            if($VehicleHardpoints[%vid, %i, "type"] != $VHardpointType::Missile)
            {
                if(%obj.wepCount $= "")
                    %obj.wepCount = 0;

                %obj.weapon[%obj.wepCount] = %part;
                %obj.hardpointRef[%obj.wepCount] = %i;
                %obj.wepCount++;
            }
            else
            {
                if(%obj.installedMissiles $= "")
                    %obj.installedMissiles = 0;
                    
                %obj.missile[%obj.installedMissiles] = %part;
                %obj.installedMissiles++;
            }
        }
        
        %part.installPart(%data, %obj, %player, %i);
        %part.reArm(%data, %obj, %player, %i);
    }

    // Add all default params if no seeking params are installed
    for(%i = 0; %i < %obj.turretCount; %i++)
    {
        if(!isObject(%obj.turrets[%i].getMountedImage(0)))
            %obj.turrets[%i].mountImage("GenericParamImage", 0);
    }

    // Apply weights - removed
//  %weight = %obj.bonusWeight;

//    if(%weight > 150)
//        %weight = 150;
//    if(%weight <= -100)
//        %weight = -100;

//    %weightImage = %weight ? getWeightName(%data, %obj, %weight) : false;

//    if(%weightImage)
//    {
//        %obj.mountImage(%weightImage, $VehicleWeightSlot);
//        %obj.bonusWeight = %weight;
//        %obj.totalWeight = %data.mass + %weight;
//    }
//    else
//    {
//        %obj.bonusWeight = 0;
//        %obj.totalWeight = %data.mass;
//    }
}

function VehiclePart::reArm(%this, %data, %vehicle, %player, %hardpoint)
{
    if($VehicleHardpoints[%vehicle.vid, %hardpoint, "type"] == $VHardpointType::Missile)
    {
        if(!isObject(%vehicle.getMountedImage(0)))
            %vehicle.mountImage("VehicleMissileLocker", 0);
            
        %this.reArmMissile(%vehicle, %hardpoint);
    }
    else if(%this.ammo !$= "")
    {
        %vehicle.ammoCache[%this.ammo] += mCeil(%this.ammoCount * $VehicleHardpoints[%vehicle.vid, %hardpoint, "imageCount"] * %vehicle.maxAmmoCapacityFactor);

        // Since we're not using ammo objects, set ammo state to true
        %turret = $VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"];
        
        if(%turret > 0)
        {
            %t = %vehicle.getMountNodeObject(%turret);

            for(%i = 0; %i < 8; %i++)
                %t.setImageAmmo(%i, true);
        }
        else
            for(%i = 0; %i < 8; %i++)
                %vehicle.setImageAmmo(%i, true);
    }
}

function VehiclePart::reArmMissile(%this, %vehicle, %hardpoint)
{
    %slot = $VehicleHardpoints[%vehicle.vid, %hardpoint, "imageStart"];
    %key = $VehicleListData[%vehicle.vid, "suffixcode"];
    %mp = $VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"];
    %mount = %mp > 0 ? %vehicle.getMountNodeObject(%mp) : %vehicle;
        
    if(%mp > 0)
        %key = $VehicleHardpoints[%vehicle.vid, %hardpoint, "mountSuffix"];

    %mount.addMissile(%slot, %this.getName()@%key@%slot, %this.missileName@"Deploy", %this.missileName, %this.rearmTime, %this.missileSize);
}

function VehiclePart::onTrigger(%this, %obj, %state, %start, %run)
{
    %offset = %start + %run;
    
    if(%this.fireTimeout > 0) // && %run > 1
    {
        if(%obj.sequentialTurretFire == true)
            return %this.sequentialFireTrigger(%obj, %start, %offset, %state);
        else if(%run == 2)
            return %this.setAlternateFireTrigger(%obj, %start, %state);
        else if(%run == 4)
            return %this.setQuadFireTrigger(%obj, %start, %state);
    }

    for(%s = %start; %s < %offset; %s++)
        %obj.setImageTrigger(%s, %state);
}

function VehiclePart::onTurretTriggerPack(%this, %vehicle, %player, %turret)
{
    // Abstract for modules to use
}

function VehiclePart::triggerGrenade(%this, %vehicle, %player)
{
    // Abstract for modules to use - by default, launch missile
    %vehicle.launchMissile();
}

function VehiclePart::onTriggerMine(%this, %vehicle, %player, %slot)
{
    // Abstract for modules to use
}

function ShapeBase::setOffsetImageTrigger(%obj, %offset, %slot)
{
    if(!%obj.fireTriggerIn[%slot])
        return;
        
    %obj.setImageTrigger(%offset, true);
}

function VehiclePart::setAlternateFireTrigger(%this, %obj, %slot, %state)
{
    // Necessary because T2 arbitrarily passes ID instead of object name
    %name = %this.getName();
    
    if(%obj.weaponTrigger[%name] $= "")
        %obj.weaponTrigger[%name] = %slot + mFloor(getRandom() + 0.5);

    %obj.weaponTriggerState[%name] = %state;

    if(%state)
        %this.triggerLoop(%obj, %slot, %name);
    else
    {
        %obj.setImageTrigger(%slot, false);
        %obj.setImageTrigger(%slot + 1, false);
    }
}

function VehiclePart::triggerLoop(%this, %obj, %slot, %name)
{
    if(!%obj.weaponTriggerState[%name])
        return;

    %time = getSimTime();
    %timeMS = %this.fireTimeout;
    
    if(%timeMS < 32)
    {
        %timeMS = 32;
        error("Warning! Weapon" SPC %this.getName() SPC " has fireTimeout < 32ms!");
    }
    
    %obj.weaponTrigger[%name] = %obj.weaponTrigger[%name] == %slot ? %slot + 1 : %slot;
    %obj.setImageTrigger(%obj.weaponTrigger[%name], true);
//    %obj.schedule(32, "setImageTrigger", %obj.weaponTrigger[%name], false);

//    %this.schedule(mFloor(%timeMS*1.3), "triggerLoop", %obj, %slot, %name); // technically not 2x fire rate due to server slowdowns interfering with image fire rate
    %this.schedule(%timeMS, "triggerLoop", %obj, %slot, %name); // technically not 2x fire rate due to server slowdowns interfering with image fire rate
}

function VehiclePart::setQuadFireTrigger(%this, %obj, %slot, %state)
{
    // Necessary because T2 arbitrarily passes ID instead of object name
    %name = %this.getName();

    if(%obj.weaponTrigger[%name] $= "")
        %obj.weaponTrigger[%name] = %slot + mFloor(getRandom() + 0.5);

    %obj.weaponTriggerState[%name] = %state;

    if(%state)
        %this.triggerLoopQuad(%obj, %slot, %name);
    else
    {
        %obj.setImageTrigger(%slot, false);
        %obj.setImageTrigger(%slot + 1, false);
        %obj.setImageTrigger(%slot + 2, false);
        %obj.setImageTrigger(%slot + 3, false);
    }
}

function VehiclePart::triggerLoopQuad(%this, %obj, %slot, %name)
{
    if(!%obj.weaponTriggerState[%name])
        return;

    %time = getSimTime();
    %timeMS = %this.fireTimeout;

    if(%timeMS < 32)
    {
        %timeMS = 32;
        error("Warning! Weapon" SPC %this.getName() SPC " has fireTimeout < 64ms!");
    }

    %obj.weaponTrigger[%name] = %obj.weaponTrigger[%name] == %slot ? %slot + 1 : %slot;

    if(%obj.weaponTrigger[%name] == %slot)
    {
        %obj.setImageTrigger(%slot, true);
        %obj.setImageTrigger(%slot + 1, true);
//        %obj.schedule(32, "setImageTrigger", %obj.weaponTrigger[%name], false);
//        %obj.schedule(32, "setImageTrigger", %obj.weaponTrigger[%name] + 1, false);
    }
    else
    {
        %obj.setImageTrigger(%slot + 2, true);
        %obj.setImageTrigger(%slot + 3, true);
//        %obj.schedule(32, "setImageTrigger", %obj.weaponTrigger[%name] + 2, false);
//        %obj.schedule(32, "setImageTrigger", %obj.weaponTrigger[%name] + 3, false);
    }

//    %this.schedule(mFloor(%timeMS*1.3), "triggerLoopQuad", %obj, %slot, %name);
    %this.schedule(%timeMS, "triggerLoopQuad", %obj, %slot, %name);
}

function VehiclePart::sequentialFireTrigger(%this, %obj, %slot, %offset, %state)
{
    // Necessary because T2 arbitrarily passes ID instead of object name
    %name = %this.getName();
    %obj.weaponTriggerState[%name] = %state;

    if(%state)
        %this.triggerSequentialFire(%obj, %slot, %slot, %offset, %name);
    else
    {
        for(%s = %start; %s < %offset; %s++)
            %obj.setImageTrigger(%s, false);
    }
}

function VehiclePart::triggerSequentialFire(%this, %obj, %start, %next, %offset, %name)
{
    if(!%obj.weaponTriggerState[%name])
        return;

    %time = getSimTime();
    %timeMS = mFloor(%this.fireTimeout / 2);

    if(%timeMS <= 32)
        %timeMS = 32;

    %obj.setImageTrigger(%next, true);
//    %obj.schedule(32, "setImageTrigger", %next, false);

    %next++;

    if(%next > %offset)
        %next = %start;

//    %this.schedule(mFloor(%timeMS*1.3), "triggerSequentialFire", %obj, %start, %next, %offset, %name);
    %this.schedule(%timeMS, "triggerSequentialFire", %obj, %start, %next, %offset, %name);
}

// Put these in first to bump 0-indexes
exec("scripts/vehicles/VPartPlaceholders.cs");

execDir("VehicleParts");
// Place empty slot on the last of every list
exec("scripts/vehicles/defaultVehiclePart.cs");
//exec("scripts/vehicles/VehicleWeight.cs"); // removing weights
