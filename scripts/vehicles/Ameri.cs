//**************************************************************
// Ameri
//**************************************************************

datablock FlyingVehicleData(Ameri) : UniversalDamageProfile
{
   spawnOffset = "0 0 2";

   catagory = "Vehicles";
   shapeFile = "vehicle_grav_scout_ameri.dts";
   multipassenger = false;

   debrisShapeName = "vehicle_grav_scout_debris.dts";
   debris = ShapeDebris;
   renderWhenDestroyed = false;

   drag    = 0.15;
   density = 1.0;

   lightOnly = true;
   mountPose[0] = scoutRoot;
   cameraMaxDist = 10;
   cameraOffset = 1.0;
   cameraLag = 0.8;
   numMountPoints = 1;
   isProtectedMountPoint[0] = true;
   explosion = LargeAirVehicleExplosion;
	explosionDamage = 2.5;
	explosionRadius = 7.5;

   maxDamage = 10.00;
   destroyedLevel = 10.00;

   isShielded = false;
   energyPerDamagePoint = 55;
   maxEnergy = 160;      // Afterburner and any energy weapon pool
   minDrag = 30;          // Linear Drag (eventually slows you down when not thrusting...constant drag)
   rotationalDrag = 900;        // Anguler Drag (dampens the drift after you stop moving the mouse...also tumble drag)
   rechargeRate = 70 / 32;

   maxAutoSpeed = 8;       // Autostabilizer kicks in when less than this speed. (meters/second)
   autoAngularForce = 360;       // Angular stabilizer force (this force levels you out when autostabilizer kicks in)
   autoLinearForce = 150;        // Linear stabilzer force (this slows you down when autostabilizer kicks in)
   autoInputDamping = 0.9;      // Dampen control input so you don't` whack out at very slow speeds

   // Maneuvering
   maxSteerinAngle = $g_PI * 0.9;    // Placeholder value for now, T2 v12 engine actually uses this value and it defaults to pi
   maxSteeringAngle = $g_PI * 0.9;    // Max radians you can rotate the wheel. Smaller number is more maneuverable. Defaults to pi
   horizontalSurfaceForce = 8;   // Horizontal center "wing" (provides "bite" into the wind for climbing/diving and turning)
   verticalSurfaceForce = 6;     // Vertical center "wing" (controls side slip. lower numbers make MORE slide.)
   maneuveringForce = 3500;      // Horizontal jets (W,S,D,A key thrust)
   steeringForce = 1500;         // Steering jets (force applied when you move the mouse)
   steeringRollForce = 550;      // Steering jets (how much you heel over when you turn)
   rollForce = 4;                // Auto-roll (self-correction to right you after you roll/invert)
   hoverHeight = 3;        // Height off the ground at rest
   createHoverHeight = 5;  // Height off the ground when created
   maxForwardSpeed = 250;

   // Turbo Jet
   jetForce = 7500;      // Afterburner thrust (this is in addition to normal thrust)
   minJetEnergy = 10;     // Afterburner can't be used if below this threshhold.
   jetEnergyDrain = 120 / 32;       // Energy use of the afterburners (low number is less drain...can be fractional)                                                                                                                                                                                                                                                                                          // Auto stabilize speed
   vertThrustMultiple = 0.0;

   // Reduction of impulse effects on sensitive shapes
   impulseDampeningFactor = 5.0;

   // Rigid body
   mass = 140;        // Mass of the vehicle
   bodyFriction = 0;     // Don't mess with this.
   bodyRestitution = 0.5;   // When you hit the ground, how much you rebound. (between 0 and 1)
   minRollSpeed = 0;     // Don't mess with this.
   softImpactSpeed = 40;       // Sound hooks. This is the soft hit.
   hardImpactSpeed = 80;    // Sound hooks. This is the hard hit.

   // Ground Impact Damage (uses DamageType::Ground)
   minImpactSpeed = 28;      // If hit ground at speed above this then it's an impact. Meters/second
   speedDamageScale = 5.0;

   // Object Impact Damage (uses DamageType::Impact)
   collDamageThresholdVel = 27.78;
   collDamageMultiplier   = 5.0;

   //
   minTrailSpeed = 28;      // The speed your contrail shows up at.
   trailEmitter = ContrailEmitter;
   forwardJetEmitter = TurboJetEmitter;
   downJetEmitter = TurboJetEmitter;

   //
   jetSound         = Engine2ThrustSound;
   engineSound      = InterceptorEngineSound;
   softImpactSound = SoftImpactSound;
   hardImpactSound = HardImpactSound;
   //wheelImpactSound = WheelImpactSound;

   //
   softSplashSoundVelocity = 10.0;
   mediumSplashSoundVelocity = 15.0;
   hardSplashSoundVelocity = 20.0;
   exitSplashSoundVelocity = 10.0;

   exitingWater      = VehicleExitWaterMediumSound;
   impactWaterEasy   = VehicleImpactWaterSoftSound;
   impactWaterMedium = VehicleImpactWaterMediumSound;
   impactWaterHard   = VehicleImpactWaterMediumSound;
   waterWakeSound    = VehicleWakeMediumSplashSound;

   dustEmitter = VehicleLiftoffDustEmitter;
   triggerDustHeight = 4.0;
   dustHeight = 1.0;

   damageEmitter[0] = SmallLightDamageSmoke;
   damageEmitter[1] = SmallLightDamageSmoke;
   damageEmitter[2] = DamageBubbles;
   damageEmitterOffset[0] = "0.0 -1.5 0.5";
   damageLevelTolerance[0] = 0.6;
   damageLevelTolerance[1] = 0.8;
   numDmgEmitterAreas = 1;

   minMountDist = 4;

   splashEmitter[0] = VehicleFoamDropletsEmitter;
   splashEmitter[1] = VehicleFoamEmitter;

   shieldImpact = VehicleShieldImpact;

   sensorColor = "8 212 0";
   sensorData = InterceptorPulseSensor;
   sensorRadius = InterceptorPulseSensor.detectRadius;
   cmdCategory = "Tactical";
   cmdIcon = CMDHoverScoutIcon;
   cmdMiniIconName = "commander/MiniIcons/com_landscout_grey";
   targetNameTag = 'Ameri';
   targetTypeTag = 'Enforcer';

   shieldEffectScale = "0.937 1.125 0.60";

   physicsType = $VehiclePhysics::ForwardsOnly;

   checkMinVelocity = 35;
   checkMinHeight = 15;
   checkMinReverseSpeed = 15;
};

function Ameri::playerMounted(%data, %obj, %player, %node)
{
   commandToClient(%player.client, 'setHudMode', 'Pilot', "Hoverbike", %node);

   %data.onPilotSeated(%obj, %player);
}
