//**************************************************************
// Leviathan
//**************************************************************

datablock FlyingVehicleData(Leviathan) : UniversalDamageProfile
{
   spawnOffset = "0 0 6";
   renderWhenDestroyed = false;
   preload        = true;
   
   catagory = "Vehicles";
   shapeFile = "vehicle_air_airbus.dts";
   multipassenger = true;
   computeCRC = true;

   debrisShapeName = "vehicle_air_hapc_debris.dts";
   debris = ShapeDebris;

   drag = 0.2;
   density = 1.0;

   numMountPoints = 8;
   
   mountPose[0] = sitting;
   mountPose[1] = sitting;
   mountPose[2] = sitting;
   mountPose[3] = sitting;
   
   hidePlayerOnMount[1] = true;
   hidePlayerOnMount[2] = true;
   hidePlayerOnMount[3] = true;

   isProtectedMountPoint[0] = true;
   isProtectedMountPoint[1] = true;
   isProtectedMountPoint[2] = true;
   isProtectedMountPoint[3] = true;
   isProtectedMountPoint[4] = true;
   isProtectedMountPoint[5] = true;
   isProtectedMountPoint[6] = true;
   isProtectedMountPoint[7] = true;

   cameraMaxDist = 30;
   cameraOffset = 5;
   cameraLag = 10;
   explosion = HugeVehicleExplosion;
	explosionDamage = 62.5;
	explosionRadius = 25.0;

   maxDamage = 250.00;
   destroyedLevel = 250.00;

   isShielded = true;
   rechargeRate = 150 / 32; // z0dd - ZOD, 4/16/02. Was 0.8
   energyPerDamagePoint = 150; // z0dd - ZOD, 4/16/02. Was 200
   maxEnergy = 1500; // z0dd - ZOD, 4/16/02. Was 550
   minDrag = 100;                // Linear Drag
   rotationalDrag = 3000;        // Anguler Drag

   // Auto stabilize speed
   maxAutoSpeed = 10;
   autoAngularForce = 3000;      // Angular stabilizer force
   autoLinearForce = 450;        // Linear stabilzer force
   autoInputDamping = 0.95;      //

   // Maneuvering
   maxSteerinAngle = $g_PI;    // Placeholder value for now, T2 v12 engine actually uses this value and it defaults to pi
   maxSteeringAngle = $g_PI;    // Max radians you can rotate the wheel. Smaller number is more maneuverable. Defaults to pi
   horizontalSurfaceForce = 8;  // Horizontal center "wing"
   verticalSurfaceForce = 9;    // Vertical center "wing"
   maneuveringForce = 6250;      // Horizontal jets // z0dd - ZOD, 4/25/02. Was 6000
   steeringForce = 1250;          // Steering jets
   steeringRollForce = 400;      // Steering jets
   rollForce = 12;               // Auto-roll
   hoverHeight = 8;         // Height off the ground at rest
   createHoverHeight = 6;   // Height off the ground when created
   maxForwardSpeed = 100;  // speed in which forward thrust force is no longer applied (meters/second) z0dd - ZOD, 4/25/02. Was 71

   // Turbo Jet
   jetForce = 11000; // z0dd - ZOD, 4/25/02. Was 5000
   minJetEnergy = 50;
   jetEnergyDrain = 320 / 32; // z0dd - ZOD, 4/16/02. Was 3.6
   vertThrustMultiple = 3.5;

   // Reduction of impulse effects on sensitive shapes
   shapeMaxForce = 5000.0;

   dustEmitter = LargeVehicleLiftoffDustEmitter;
   triggerDustHeight = 8.0;
   dustHeight = 2.0;

   damageEmitter[0] = SmallLightDamageSmoke; //LightDamageSmoke;
   damageEmitter[1] = OnFireEmitter;
   damageEmitter[2] = DamageBubbles;
   damageEmitterOffset[0] = "3.0 -3.0 0.0 ";
   damageEmitterOffset[1] = "-3.0 -3.0 0.0 ";
   damageLevelTolerance[0] = 0.6;
   damageLevelTolerance[1] = 0.8;
   numDmgEmitterAreas = 2;
   
   // Rigid body
   mass = 625;
   bodyFriction = 0;
   bodyRestitution = 0.5;
   minRollSpeed = 0;
   softImpactSpeed = 10;       // Sound hooks. This is the soft hit.
   hardImpactSpeed = 12;    // Sound hooks. This is the hard hit.

   // Ground Impact Damage (uses DamageType::Ground)
   minImpactSpeed = 28;      // If hit ground at speed above this then it's an impact. Meters/second
   speedDamageScale = 2.0;

   // Object Impact Damage (uses DamageType::Impact)
   collDamageThresholdVel = 27.78;
   collDamageMultiplier   = 2.0;

   //
   minTrailSpeed = 24;      // The speed your contrail shows up at.
   trailEmitter = ContrailEmitter;
   forwardJetEmitter = RedJetEmitter;
   downJetEmitter = RedJetEmitter;

   //
   jetSound = Engine3ThrustSound;
   engineSound = LeviathanEngineSound;
   softImpactSound = SoftImpactSound;
   hardImpactSound = HardImpactSound;
   //wheelImpactSound = WheelImpactSound;

   //
   softSplashSoundVelocity = 5.0; 
   mediumSplashSoundVelocity = 8.0;   
   hardSplashSoundVelocity = 12.0;   
   exitSplashSoundVelocity = 8.0;
   
   exitingWater      = VehicleExitWaterHardSound;
   impactWaterEasy   = VehicleImpactWaterSoftSound;
   impactWaterMedium = VehicleImpactWaterMediumSound;
   impactWaterHard   = VehicleImpactWaterHardSound;
   waterWakeSound    = VehicleWakeHardSplashSound; 
   
   minMountDist = 4;

   splashEmitter[0] = VehicleFoamDropletsEmitter;
   splashEmitter[1] = VehicleFoamEmitter;

   shieldImpact = VehicleShieldImpact;

   cmdCategory = "Tactical";
   cmdIcon = CMDFlyingHAPCIcon;
   cmdMiniIconName = "commander/MiniIcons/com_hapc_grey";
   targetNameTag = 'Leviathan';
   targetTypeTag = 'Dreadnought';
   sensorData = AWACPulseSensor;
   sensorRadius = AWACPulseSensor.detectRadius; // z0dd - ZOD, 4/25/02. Allows sensor to be shown on CC
   sensorColor = "255 10 10";

   checkRadius = 7.8115;
   observeParameters = "1 20 20";

   stuckTimerTicks = 32;   // If the hapc spends more than 1 sec in contact with something
   stuckTimerAngle = 80;   //  with a > 80 deg. pitch, BOOM!

   shieldEffectScale = "1.0 0.9375 0.45";

   physicsType = $VehiclePhysics::ForwardsOnly;

   checkMinVelocity = 15;
   checkMinHeight = 30;
   checkMinReverseSpeed = 10;
};

function Leviathan::hasDismountOverrides(%data, %obj)
{
   return true;
}

function Leviathan::getDismountOverride(%data, %obj, %mounted)
{
   %node = -1;
   for (%i = 0; %i < %data.numMountPoints; %i++)
   {
      if (%obj.getMountNodeObject(%i) == %mounted)
      {
         %node = %i;
         break;
      }
   }
   if (%node == -1)
   {
      warning("Couldn't find object mount point");
      return "0 0 1";
   }

   if (%node == 4 || %node == 5) // left side of vehicle
   {
      return "-1 0 1";
   }
   else if (%node == 6 || %node == 7) // right side of vehicle
   {
      return "1 0 1";
   }
   else
   {
      return "0 0 1";
   }
}

datablock TurretData(LeviathanTurret) : UniversalDamageProfile
{
   className      = VehicleTurret;
   catagory       = "Turrets";
   shapeFile      = "Turret_tank_base.dts";
   preload        = true;

   mass           = 1.0;  // Not really relevant

   maxEnergy               = 1;
   maxDamage               = Leviathan.maxDamage;
   destroyedLevel          = Leviathan.destroyedLevel;
   repairRate              = 0;

   // capacitor
   maxCapacitorEnergy      = 1000;
   capacitorRechargeRate   = 25.0;

   thetaMin = 0;
   thetaMax = 150;

   inheritEnergyFromMount = true;
   firstPersonOnly = true;
   useEyePoint = true;
   numWeapons = 2;

   cameraDefaultFov = 90.0;
   cameraMinFov = 5.0;
   cameraMaxFov = 120.0;

   targetNameTag = 'Leviathan';
   targetTypeTag = 'Turret';
};

datablock ShapeBaseImageData(LevGenerator1)
{
   mountPoint = 15;
   offset = "0.0 -0.25 -1.51";
   rotation = "0 0 1 0"; //calcThisRotD("0 0 0");
   shapeFile = "station_generator_large.dts";

   stateName[0]             = "Activate";
   stateSequence[0]         = "Power";
};

function Leviathan::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);
   
    %obj.mountImage(LevGenerator1, 1);

    %front = %this.installTurret(%obj, LeviathanTurret, 8);
    %front.mountImage(AssaultTurretParam, 0);

    %rear1 = %this.installTurret(%obj, LeviathanTurret, 9);
    %rear1.mountImage(AssaultTurretParam, 0);
    
    %rear2 = %this.installTurret(%obj, LeviathanTurret, 10);
    %rear2.mountImage(AssaultTurretParam, 0);
}

function Leviathan::playerMounted(%data, %obj, %player, %node)
{
   if(%node == 0)
   {
      // pilot position
	   commandToClient(%player.client, 'setHudMode', 'Pilot', "HAPC", %node);
    
       %data.onPilotSeated(%obj, %player);
   }
   else if(%node == 1)
      %data.onGunnerSeated(%obj, %player, 8);
   else if(%node == 2)
      %data.onGunnerSeated(%obj, %player, 9);
   else if(%node == 3)
      %data.onGunnerSeated(%obj, %player, 10);
   else
	   commandToClient(%player.client, 'setHudMode', 'Passenger', "HAPC", %node);

   %data.updatePassengerString(%obj);

   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}
