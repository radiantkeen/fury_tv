// These placeholders exist as the vehicle part system 0-indexed exhibit extremely
// strange behavior, such as partial function argument passing and even straight
// out ignoring function calls

function SHAPlaceholder::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    
    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = 0;
    %this.deltaRegen = 0;
}

function SHAPlaceholder::installPart(%this, %data, %vehicle, %player)
{
    // Try a function call in here, DO IT! JUST! DO IT!
    echo("test 123");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHAPlaceholder", "Sanity Check", "If this works I know something", 0, $VHardpointSize::Placeholder, $VHardpointType::Placeholder);
VehiclePart.registerVehiclePart($VehiclePartType::Module, "MOAPlaceHolder", "A Placeholder", "Because T2 is retarded sometimes", 0, $VHardpointSize::Placeholder, $VHardpointType::Placeholder);
VehiclePart.registerVehiclePart($VehiclePartType::Armor, "APAPlaceHolder", "A Placeholder", "Why does this not work", 0, $VHardpointSize::Placeholder, $VHardpointType::Placeholder);
VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "WPAPlaceHolder", "A Placeholder", "Uuuuuuugh", 0, $VHardpointSize::Placeholder, $VHardpointType::Placeholder);


// Global shield variables
$ShieldDefs::StandardPowerDrain = 40;
$ShieldDefs::StandardBleedthrough = 5;

$ShieldDefs::RegenerativePowerDrain = 50;
$ShieldDefs::RegenerativeBleedthrough = 2.5;

$ShieldDefs::HardenedPowerDrain = 50;
$ShieldDefs::HardenedBleedthrough = 0;

$ShieldDefs::CovariantPowerDrain = 60;
$ShieldDefs::CovariantBleedthrough = 5;

$ShieldDefs::LightPowerDrain = 20;
$ShieldDefs::LightBleedthrough = 10;

// Hailstorm definitions
datablock AudioProfile(RocketPodFireSound)
{
   filename    = "fx/weapon/rocketpod_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(HailstormFireSound)
{
   filename    = "fx/weapon/missile_turret_launch.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(RocketPodReloadSound)
{
   filename    = "fx/powered/turret_heavy_reload.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(RocketExplosionSound)
{
   filename    = "fx/explosion/devistator_exp.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock ExplosionData(RocketExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   playSpeed = 1.5;
   soundProfile   = RocketExplosionSound;
   faceViewer = true;

   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "0.5 0.5 0.5";
   sizes[2] = "0.5 0.5 0.5";

   emitter[0] = MissileExplosionSmokeEmitter;

   debris = MissileSpikeDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 170;
   debrisNum = 8;
   debrisNumVariance = 6;
   debrisVelocity = 15.0;
   debrisVelocityVariance = 2.0;

   shakeCamera = true;
   camShakeFreq = "6.0 7.0 7.0";
   camShakeAmp = "70.0 70.0 70.0";
   camShakeDuration = 1.0;
   camShakeRadius = 7.0;
};

datablock LinearProjectileData(HailstormRocket)
{
//   scale = "1.5 1.5 1.5";
   projectileShapeName = "vehicle_missile_red.dts";
   emitterDelay        = -1;
   directDamage        = 0;
   directDamageType    = $DamageType::Missile;
   hasDamageRadius     = true;
   indirectDamage      = 2.0;
   damageRadius        = 12;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 500;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Explosive;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "RocketExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = WildcatJetEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 350;
   wetVelocity       = 200;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 10000;
   lifetimeMS        = 10000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 6000;

   sound = "LargeMissileProjectileSound";

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 7.0;
   lightColor  = "0.4 0.15 0.1";
};
