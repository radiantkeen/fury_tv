// General datablocks for parts
datablock TurretImageData(SeekingTurretParam)
{
   mountPoint = 0;
   shapeFile = "turret_muzzlepoint.dts";

   projectile = AssaultChaingunBullet;
   projectileType = TracerProjectile;

   useCapacitor = true;
   usesEnergy = true;

   // Turret parameters
   activationMS      = 1000;
   deactivateDelayMS = 1500;
   thinkTimeMS       = 200;
   degPerSecTheta    = 500;
   degPerSecPhi      = 500;

   attackRadius      = 200;

   isSeeker     = true;
   seekRadius   = 400;
   maxSeekAngle = 30;
   seekTime     = 0.85;
   minSeekHeat  = 0.8;
   useTargetAudio = false;
   minTargetingDistance = 10;
};

// None armor type
/// ----------------------------------------------------------
function VArmorNone::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    
    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = 0;
    %this.deltaRegen = 0;
}

VehiclePart.registerVehiclePart($VehiclePartType::Armor, "VArmorNone", "None", "No Additional Armoring", $VehicleList::All, $VHardpointSize::Internal, $VHardpointType::Anything);

// Empty Module Slot
/// ----------------------------------------------------------
function VModuleNone::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Push;
    
    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = 0;
    %this.deltaRegen = 0;
}

function VModuleNone::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = false;
}

VehiclePart.registerVehiclePart($VehiclePartType::Module, "VModuleNone", "None", "No Module", $VehicleList::All, $VHardpointSize::Internal, $VHardpointType::Anything);

// None Shield Slot
/// ----------------------------------------------------------
function VShieldNone::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    
    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = 0;
    %this.deltaRegen = 0;
}

function VShieldNone::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 0, 0, 0, 0, 0, 1, "NoShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "VShieldNone", "None", "No Shield", $VehicleList::All, $VHardpointSize::Internal, $VHardpointType::Anything);

// Empty Hardpoint
/// ----------------------------------------------------------
function XEmptyHardpoint::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "";
    %this.ammoUse = 0;
    %this.ammoAmount = 0;
}

function XEmptyHardpoint::installPart(%this, %data, %vehicle, %player)
{

}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "XEmptyHardpoint", "Empty Hardpoint", "Empty Hardpoint", -1, -1, -1);

// Shield support functions
/// ----------------------------------------------------------
function Armor::installShield(%data, %obj, %maxStrength, %recDelay, %recRate, %powerDrain, %shieldblock)
{
    // not needed as module does this
}

function VehicleData::installShield(%data, %obj, %maxStrength, %recDelay, %resDelay, %recRate, %powerDrain, %bleedthrough, %shieldblock)
{
    %shield = new StaticShape()
    {
        dataBlock = %shieldblock;
        team = %obj.teamBought;
        scale = "1 1 1";
    };

   MissionCleanup.add(%shield);

   %obj.mountObject(%shield, 15);
   %shield.setCloaked(true);
   %obj.shieldSource = %shield;
   %obj.rechargeTick *= 1 - %powerDrain;

   %shield.sourceVehicle = %obj;
   %shield.init = true;
   %shield.maxStrength = %maxStrength;
   %shield.rechargeRate = %recRate;
   %shield.rechargeDelay = %recDelay;
   %shield.bleedthrough = %bleedthrough;
   %shield.restartDelay = %resDelay;

   %shield.getDatablock().startShield(%shield);
}

// NoShieldEmitter
// -----------------------------------------------------------

datablock StaticShapeData(NoShieldEmitter)
{
   shapeFile = "beacon.dts";
};

function NoShieldEmitter::startShield(%data, %shield)
{
    %shield.sourceVehicle.isShielded = false;
}

function NoShieldEmitter::onShieldDamage(%data, %shield, %obj, %position, %amount, %damageType)
{
    return %amount;
}

// VehicleShieldEmitter
/// ----------------------------------------------------------
datablock StaticShapeData(VehicleShieldEmitter)
{
   shapeFile = "turret_sentry.dts";
};

datablock AudioProfile(ShieldsDownSound)
{
   filename    = "fx/vehicle/shields_down.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(ShieldsUpSound)
{
   filename    = "fx/vehicle/shields_up.wav";
   description = AudioDefault3d;
   preload = true;
};

function VehicleShieldEmitter::startShield(%data, %shield)
{
    if(%shield.init == true)
    {
        %shield.init = false;
        %shield.strength = 0;
        %shield.tickRate = $g_TickTime;
    }

    if(%shield.maxStrength > 0)
    {
        if(%shield.rechargeThread > 0)
        {
            cancel(%shield.rechargeThread);
            %shield.rechargeThread = 0;
        }
    
        %data.startRecharge(%shield);
    }
    else
        %shield.sourceVehicle.isShielded = false;
}

function VehicleShieldEmitter::startRecharge(%data, %shield)
{
    if(!isObject(%shield.sourceVehicle))
    {
        %shield.schedule(1000, "delete");
        return;
    }

//    if(%shield.sourceVehicle.isShielded == true) // don't start loop twice
//        return;

    %shield.sourceVehicle.isShielded = true;
    %shield.sourceVehicle.playAudio(2, ShieldsUpSound);
    zapVehicle(%shield.sourceVehicle, FXBlueShift);

    if(%shield.strength == 0)
        %shield.strength = %shield.maxStrength * 0.25;

    %data.tickRecharge(%shield);
}

function VehicleShieldEmitter::restartRecharge(%data, %shield, %bDepleted)
{
    %shield.sourceVehicle.isShielded = (%shield.strength > 0);

    if(%shield.rechargeThread > 0)
        cancel(%shield.rechargeThread);

    if(%bDepleted)
    {
        %shield.sourceVehicle.playAudio(2, ShieldsDownSound);
        zapVehicle(%shield.sourceVehicle, FXPulse);
        %shield.rechargeThread = %data.schedule(%shield.restartDelay, "startRecharge", %shield);
    }
    else
        %shield.rechargeThread = %data.schedule(%shield.rechargeDelay, "startRecharge", %shield);
}

function VehicleShieldEmitter::tickRecharge(%data, %shield)
{
    if(%shield.strength < %shield.maxStrength)
    {
        %shield.strength += %shield.rechargeRate;

        if(%shield.strength > %shield.maxStrength)
            %shield.strength = %shield.maxStrength;

        %shield.rechargeThread = %data.schedule(%shield.tickRate, "tickRecharge", %shield);
    }
    else
        %shield.rechargeThread = 0;
}

function VehicleShieldEmitter::onShieldDamage(%data, %shield, %obj, %position, %amount, %damageType)
{
    %energy = %shield.strength;

    // Passthrough if no shields or if shield ignore
    if(%energy < 1 || %obj.lastHitFlags & ($Projectile::IgnoreShields | $Projectile::Phaser))
        return %amount;

    if(%obj.lastHitFlags & ($Projectile::ArmorOnly | $Projectile::Corrosive))
       return 0;

    %amount = mRound(%amount * 100);

    if(%obj.lastHitFlags & $Projectile::Nullifier)
        %amount *= 2;

    if(%obj.lastHitFlags & $Projectile::PartialShieldPhase)
        %amount *= 0.5;

    if(%obj.lastHitFlags & $Projectile::Explosive)
        %amount *= 0.2;

    if(%obj.lastHitFlags & $Projectile::Kinetic)
        %amount *= 0.75;

    %amount *= %obj.shieldAbsorbFactor;
    
//    %shieldScale = %obj.getDataBlock().shieldDamageScale[%damageType];

//    if(%shieldScale $= "")
//        %shieldScale = 1;

//    %amount *= %shieldScale;
//    %amount *= %obj.shieldDamageFactor[%element];
    %remainder = 0;

    if(%energy > %amount)
    {
        %obj.playShieldEffect("0.0 0.0 1.0");
        %shield.getDatablock().restartRecharge(%shield, false);

        if(%obj.lastHitFlags & $Projectile::PartialShieldPhase)
        {
            %shield.strength = %energy - (%amount * 0.5);
            %remainder = %amount * 0.5;
        }
        else
        {
            %shield.strength = %energy - %amount;
            %remainder = %amount * %shield.bleedthrough;
        }
    }
    else
    {
        %shield.strength = 0;
        %remainder = %amount - %energy;

        %shield.getDatablock().restartRecharge(%shield, true);
    }

    if(%obj.lastHitFlags & $Projectile::Nullifier)
        %remainder = 0;

    return %remainder * 0.01;
}
