// Physics constants
$VehiclePhysics::None = 0;
$VehiclePhysics::ConstantMomentum = 1;
$VehiclePhysics::Aerodynamic = 2;
$VehiclePhysics::ForwardsOnly = 3;

// Vehicle Count Groups
$VehicleGroup::Other = 0;
$VehicleGroup::Interceptors = 1;
$VehicleGroup::Fighters = 2;
$VehicleGroup::Gunships = 3;
$VehicleGroup::Battleships = 4;
$VehicleGroup::Carriers = 5;
$VehicleGroup::Dreadnoughts = 6;

// Vehicle sounds
datablock AudioProfile(InterceptorEngineSound)
{
   filename    = "fx/vehicle/skycutter_engine_base.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(FighterEngineSound)
{
   filename    = "fx/vehicles/mpb_thrust.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(CatapultEngineSound)
{
   filename    = "fx/vehicle/jetbike_engine.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(HoverJeepEngineSound)
{
   filename    = "fx/vehicle/hovercar_engine.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(TomahawkEngineSound)
{
   filename    = "fx/vehicle/firestorm_engine.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(GunshipEngineSound)
{
   filename    = "fx/vehicle/skycutter_engine_base.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(BattleshipEngineSound)
{
   filename    = "fx/vehicle/engine_1.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(SkyMistressEngineSound)
{
   filename    = "fx/vehicle/imperator_engine_base.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(LeviathanEngineSound)
{
   filename    = "fx/vehicle/trebuchet_engine.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(GoodshipEngineSound)
{
   filename    = "fx/vehicle/engine_2.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(Engine1ThrustSound)
{
   filename    = "fx/vehicle/skycutter_thrust.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(Engine2ThrustSound)
{
   filename    = "fx/vehicle/jetbike_thrust.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(Engine3ThrustSound)
{
   filename    = "fx/vehicle/trebuchet_boost.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(Engine4ThrustSound)
{
   filename    = "fx/vehicles/wake_shrike_n_tank.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(Engine5ThrustSound)
{
   filename    = "fx/vehicles/wake_wildcat.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

// Execute these vehicles in this order:
exec("scripts/vehicles/vehicle_shrike.cs");
exec("scripts/vehicles/vehicle_bomber.cs");
exec("scripts/vehicles/vehicle_havoc.cs");
//exec("scripts/vehicles/vehicle_bulldog.cs");
exec("scripts/vehicles/vehicle_wildcat.cs");
exec("scripts/vehicles/vehicle_tank.cs");
//exec("scripts/vehicles/vehicle_mpb.cs");

// Fury:TV Vehicles
exec("scripts/vehicles/Airhawk.cs");
exec("scripts/vehicles/Wildcat.cs");
exec("scripts/vehicles/Ameri.cs");
exec("scripts/vehicles/ShrikeX.cs");
exec("scripts/vehicles/SuperShrike.cs");
exec("scripts/vehicles/Starhawk.cs");
exec("scripts/vehicles/Firebird.cs");
exec("scripts/vehicles/Stryker.cs");
exec("scripts/vehicles/Tomahawk.cs");
exec("scripts/vehicles/Catapult.cs");
exec("scripts/vehicles/HoverJeep.cs");
exec("scripts/vehicles/Leviathan.cs");
exec("scripts/vehicles/Annihilator.cs");
exec("scripts/vehicles/Bismark.cs");
exec("scripts/vehicles/SkyMistress.cs");
exec("scripts/vehicles/Wolfhound.cs");
exec("scripts/vehicles/Scorpion.cs");

if($Host::EnableSpecialVehicles)
{
//    exec("scripts/vehicles/Goodship.cs");
//    exec("scripts/vehicles/Arwing.cs");
    exec("scripts/vehicles/Viper.cs");
    exec("scripts/vehicles/PyroGX.cs");
}
