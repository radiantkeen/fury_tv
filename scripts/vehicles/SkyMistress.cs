//**************************************************************
// SkyMistress
//**************************************************************

datablock FlyingVehicleData(SkyMistress) : UniversalDamageProfile
{
   spawnOffset = "0 0 6";
   renderWhenDestroyed = false;

   catagory = "Vehicles";
   shapeFile = "Airship.dts";
   multipassenger = true;
   computeCRC = true;

   debrisShapeName = "vehicle_air_hapc_debris.dts";
   debris = ShapeDebris;

   drag = 0.15;
   density = 1.0;

//   mountPose[0] = sitting;
//   mountPose[1] = sitting;
   numMountPoints = 6;
   isProtectedMountPoint[0] = true;
   isProtectedMountPoint[1] = true;
   isProtectedMountPoint[2] = true;
   isProtectedMountPoint[3] = true;
   isProtectedMountPoint[4] = true;
   isProtectedMountPoint[5] = true;

   cameraMaxDist = 17;
   cameraOffset = 2;
   cameraLag = 8.5;
   explosion = HugeVehicleExplosion;
	explosionDamage = 32.5;
	explosionRadius = 20.0;

   maxDamage = 130.00;
   destroyedLevel = 130.00;

   isShielded = true;
   rechargeRate = 50 / 32; // z0dd - ZOD, 4/16/02. Was 0.8
   energyPerDamagePoint = 150; // z0dd - ZOD, 4/16/02. Was 200
   maxEnergy = 500; // z0dd - ZOD, 4/16/02. Was 550
   minDrag = 100;                // Linear Drag
   rotationalDrag = 2700;        // Anguler Drag

   // Auto stabilize speed
   maxAutoSpeed = 10;
   autoAngularForce = 3000;      // Angular stabilizer force
   autoLinearForce = 450;        // Linear stabilzer force
   autoInputDamping = 0.95;      //

   // Maneuvering
   maxSteerinAngle = $g_PI;    // Placeholder value for now, T2 v12 engine actually uses this value and it defaults to pi
   maxSteeringAngle = $g_PI;    // Max radians you can rotate the wheel. Smaller number is more maneuverable. Defaults to pi
   horizontalSurfaceForce = 9;  // Horizontal center "wing"
   verticalSurfaceForce = 9;    // Vertical center "wing"
   maneuveringForce = 7400;      // Horizontal jets // z0dd - ZOD, 4/25/02. Was 6000
   steeringForce = 1250;          // Steering jets
   steeringRollForce = 400;      // Steering jets
   rollForce = 12;               // Auto-roll
   hoverHeight = 8;         // Height off the ground at rest
   createHoverHeight = 6;   // Height off the ground when created
   maxForwardSpeed = 125;  // speed in which forward thrust force is no longer applied (meters/second) z0dd - ZOD, 4/25/02. Was 71

   // Turbo Jet
   jetForce = 11000; // z0dd - ZOD, 4/25/02. Was 5000
   minJetEnergy = 15;
   jetEnergyDrain = 140 / 32; // z0dd - ZOD, 4/16/02. Was 3.6
   vertThrustMultiple = 3.5;

   // Reduction of impulse effects on sensitive shapes
   shapeMaxForce = 200.0;

   dustEmitter = LargeVehicleLiftoffDustEmitter;
   triggerDustHeight = 4.0;
   dustHeight = 2.0;

   damageEmitter[0] = SmallLightDamageSmoke;
   damageEmitter[1] = OnFireEmitter;
   damageEmitter[2] = DamageBubbles;
   damageEmitterOffset[0] = "3.0 -3.0 0.0 ";
   damageEmitterOffset[1] = "-3.0 -3.0 0.0 ";
   damageLevelTolerance[0] = 0.6;
   damageLevelTolerance[1] = 0.8;
   numDmgEmitterAreas = 2;

   // Rigid body
   mass = 550;
   bodyFriction = 0;
   bodyRestitution = 0.3;
   minRollSpeed = 0;
   softImpactSpeed = 12;       // Sound hooks. This is the soft hit.
   hardImpactSpeed = 15;    // Sound hooks. This is the hard hit.

   // Ground Impact Damage (uses DamageType::Ground)
   minImpactSpeed = 28;      // If hit ground at speed above this then it's an impact. Meters/second
   speedDamageScale = 2.0;

   // Object Impact Damage (uses DamageType::Impact)
   collDamageThresholdVel = 27.78;
   collDamageMultiplier   = 2.0;

   //
   minTrailSpeed = 24;      // The speed your contrail shows up at.
   trailEmitter = ContrailEmitter;
   forwardJetEmitter = FlyerJetEmitter;
   downJetEmitter = FlyerJetEmitter;

   //
   jetSound = Engine4ThrustSound;
   engineSound = SkyMistressEngineSound;
   softImpactSound = SoftImpactSound;
   hardImpactSound = HardImpactSound;
   //wheelImpactSound = WheelImpactSound;

   //
   softSplashSoundVelocity = 5.0; 
   mediumSplashSoundVelocity = 8.0;   
   hardSplashSoundVelocity = 12.0;   
   exitSplashSoundVelocity = 8.0;
   
   exitingWater      = VehicleExitWaterHardSound;
   impactWaterEasy   = VehicleImpactWaterSoftSound;
   impactWaterMedium = VehicleImpactWaterMediumSound;
   impactWaterHard   = VehicleImpactWaterHardSound;
   waterWakeSound    = VehicleWakeHardSplashSound; 
   
   minMountDist = 4;

   splashEmitter[0] = VehicleFoamDropletsEmitter;
   splashEmitter[1] = VehicleFoamEmitter;

   shieldImpact = VehicleShieldImpact;

   cmdCategory = "Tactical";
   cmdIcon = CMDFlyingHAPCIcon;
   cmdMiniIconName = "commander/MiniIcons/com_hapc_grey";
   targetNameTag = 'Sky Mistress';
   targetTypeTag = 'Troop Carrier';
   sensorData = VehiclePulseSensor;
   sensorRadius = VehiclePulseSensor.detectRadius; // z0dd - ZOD, 4/25/02. Allows sensor to be shown on CC

   checkRadius = 7.8115;
   observeParameters = "1 15 15";

   stuckTimerTicks = 32;   // If the hapc spends more than 1 sec in contact with something
   stuckTimerAngle = 80;   //  with a > 80 deg. pitch, BOOM!

   shieldEffectScale = "1.0 0.9375 0.45";

   physicsType = $VehiclePhysics::ForwardsOnly;

   checkMinVelocity = 20;
   checkMinHeight = 20;
   checkMinReverseSpeed = 10;
};

function SkyMistress::hasDismountOverrides(%data, %obj)
{
   return true;
}

function SkyMistress::getDismountOverride(%data, %obj, %mounted)
{
   %node = -1;
   for (%i = 0; %i < %data.numMountPoints; %i++)
   {
      if (%obj.getMountNodeObject(%i) == %mounted)
      {
         %node = %i;
         break;
      }
   }
   if (%node == -1)
   {
      warning("Couldn't find object mount point");
      return "0 0 1";
   }

   if (%node == 3 || %node == 2)
   {
      return "-1 0 1";
   }
   else if (%node == 5 || %node == 4)
   {
      return "1 0 1";
   }
   else
   {
      return "0 0 1";
   }
}

function SkyMistress::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);

   %obj.schedule(5500, "playThread", $ActivateThread, "ambient");
}

function SkyMistress::playerMounted(%data, %obj, %player, %node)
{
   if(%node == 0)
   {
      // pilot position
	   commandToClient(%player.client, 'setHudMode', 'Pilot', "HAPC", %node);

       %data.onPilotSeated(%obj, %player);
   }
   else
	   commandToClient(%player.client, 'setHudMode', 'Passenger', "HAPC", %node);

   %data.updatePassengerString(%obj);

   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}
