//**************************************************************
// Stryker
//**************************************************************

datablock FlyingVehicleData(Stryker) : UniversalDamageProfile
{
   spawnOffset = "0 0 2";

   catagory = "Vehicles";
   shapeFile = "vehicle_air_newbomber.dts";
   multipassenger = true;
   computeCRC = true;

   weaponNode = 1;

   debrisShapeName = "vehicle_air_bomber_debris.dts";
   debris = ShapeDebris;
   renderWhenDestroyed = false;

   drag    = 0.2;
   density = 1.0;

   mountPose[0] = sitting;
   mountPose[1] = sitting;
   mountPose[2] = sitting;
   numMountPoints = 4;
   isProtectedMountPoint[0] = true;
   isProtectedMountPoint[1] = true;
   isProtectedMountPoint[2] = true;
   isProtectedMountPoint[3] = true;

   cameraDefaultFov = 90.0;
   cameraMinFov = 5.0;
   cameraMaxFov = 120.0;
   
   cameraMaxDist = 22;
   cameraOffset = 5;
   cameraLag = 1.0;
   explosion = BigAirVehicleExplosion;
	explosionDamage = 18.75;
	explosionRadius = 15.0;

   maxDamage = 75.00;
   destroyedLevel = 75.00;

   isShielded = true;
   energyPerDamagePoint = 150;
   maxEnergy = 850;      // Afterburner and any energy weapon pool
   minDrag = 60;           // Linear Drag (eventually slows you down when not thrusting...constant drag)
   rotationalDrag = 1800;        // Angular Drag (dampens the drift after you stop moving the mouse...also tumble drag)
   rechargeRate = 112 / 32;

   // Auto stabilize speed
   maxAutoSpeed = 15;       // Autostabilizer kicks in when less than this speed. (meters/second)
   autoAngularForce = 1500;      // Angular stabilizer force (this force levels you out when autostabilizer kicks in)
   autoLinearForce = 300;        // Linear stabilzer force (this slows you down when autostabilizer kicks in)
   autoInputDamping = 0.95;      // Dampen control input so you don't whack out at very slow speeds

   // Maneuvering
   maxSteerinAngle = $g_PI;    // Placeholder value for now, T2 v12 engine actually uses this value and it defaults to pi
   maxSteeringAngle = $g_PI;    // Max radians you can rotate the wheel. Smaller number is more maneuverable. Defaults to pi
   horizontalSurfaceForce = 5;   // Horizontal center "wing" (provides "bite" into the wind for climbing/diving and turning)
   verticalSurfaceForce = 7;     // Vertical center "wing" (controls side slip. lower numbers make MORE slide.)
   maneuveringForce = 5000;      // Horizontal jets (W,S,D,A key thrust) // z0dd - ZOD, 9/8/02. Was 4700
   steeringForce = 1200;         // Steering jets (force applied when you move the mouse)
   steeringRollForce = 300;      // Steering jets (how much you heel over when you turn)
   rollForce = 8;                // Auto-roll (self-correction to right you after you roll/invert)
   hoverHeight = 5;        // Height off the ground at rest
   createHoverHeight = 3;  // Height off the ground when created
   maxForwardSpeed = 166.667;  // speed in which forward thrust force is no longer applied (meters/second) // z0dd - ZOD, 9/8/02. Was 85

   // Turbo Jet
   jetForce = 9500;      // Afterburner thrust (this is in addition to normal thrust) // z0dd - ZOD, 9/8/02. Was 3000
   minJetEnergy = 20.0;     // Afterburner can't be used if below this threshhold.
   jetEnergyDrain = 237 / 32;       // Energy use of the afterburners (low number is less drain...can be fractional)
   vertThrustMultiple = 2.5; // z0dd - ZOD, 9/8/02. Was 3.0

   // Reduction of impulse effects on sensitive shapes
   shapeMaxForce = 2000.0;

   dustEmitter = LargeVehicleLiftoffDustEmitter;
   triggerDustHeight = 4.0;
   dustHeight = 2.0;

   damageEmitter[0] = SmallLightDamageSmoke; //LightDamageSmoke;
   damageEmitter[1] = OnFireEmitter;
   damageEmitter[2] = DamageBubbles;
   damageEmitterOffset[0] = "3.0 -3.0 0.0 ";
   damageEmitterOffset[1] = "-3.0 -3.0 0.0 ";
   damageLevelTolerance[0] = 0.6;
   damageLevelTolerance[1] = 0.8;
   numDmgEmitterAreas = 2;

   // Rigid body
   mass = 425;        // Mass of the vehicle
   bodyFriction = 0;     // Don't mess with this.
   bodyRestitution = 0.5;   // When you hit the ground, how much you rebound. (between 0 and 1)
   minRollSpeed = 0;     // Don't mess with this.
   softImpactSpeed = 20;       // Sound hooks. This is the soft hit.
   hardImpactSpeed = 25;    // Sound hooks. This is the hard hit.

   // Ground Impact Damage (uses DamageType::Ground)
   minImpactSpeed = 28;      // If hit ground at speed above this then it's an impact. Meters/second
   speedDamageScale = 1.0;

   // Object Impact Damage (uses DamageType::Impact)
   collDamageThresholdVel = 27.78;
   collDamageMultiplier   = 1.0;

   //
   minTrailSpeed = 24;      // The speed your contrail shows up at.
   trailEmitter = ContrailEmitter;
   forwardJetEmitter = BlueJetEmitter;
   downJetEmitter = BlueJetEmitter;

   //
   jetSound = BomberFlyerThrustSound;
   engineSound = GunshipEngineSound;
   softImpactSound = SoftImpactSound;
   hardImpactSound = HardImpactSound;
   //wheelImpactSound = WheelImpactSound;

   //
   softSplashSoundVelocity = 15.0; 
   mediumSplashSoundVelocity = 20.0;   
   hardSplashSoundVelocity = 30.0;   
   exitSplashSoundVelocity = 10.0;
   
   exitingWater      = VehicleExitWaterHardSound;
   impactWaterEasy   = VehicleImpactWaterSoftSound;
   impactWaterMedium = VehicleImpactWaterMediumSound;
   impactWaterHard   = VehicleImpactWaterHardSound;
   waterWakeSound    = VehicleWakeHardSplashSound; 
  
   minMountDist = 4;

   splashEmitter[0] = VehicleFoamDropletsEmitter;
   splashEmitter[1] = VehicleFoamEmitter;

   shieldImpact = VehicleShieldImpact;

   cmdCategory = "Tactical";
   cmdIcon = CMDFlyingBomberIcon;
   cmdMiniIconName = "commander/MiniIcons/com_bomber_grey";
   targetNameTag = 'Stryker';
   targetTypeTag = 'Cruiser';
   sensorData = VehiclePulseSensor;
   sensorRadius = VehiclePulseSensor.detectRadius; // z0dd - ZOD, 4/25/02. Allows sensor to be shown on CC

   checkRadius = 7.1895;
   observeParameters = "1 10 10";
   shieldEffectScale = "0.75 0.975 0.375";
   showPilotInfo = 1;

   physicsType = $VehiclePhysics::ForwardsOnly;

   checkMinVelocity = 25;
   checkMinHeight = 20;
   checkMinReverseSpeed = 10;
};

datablock TurretData(StrykerTurret) : UniversalDamageProfile
{
   className      = VehicleTurret;
   catagory       = "Turrets";
   shapeFile      = "turret_base_large.dts";
   preload        = true;

   mass           = 1.0;  // Not really relevant

   maxEnergy               = 1;
   maxDamage               = Stryker.maxDamage;
   destroyedLevel          = Stryker.destroyedLevel;
   repairRate              = 0;

   // capacitor
   maxCapacitorEnergy      = 1000;
   capacitorRechargeRate   = 25.0;

   thetaMin = 0;
   thetaMax = 130;

   inheritEnergyFromMount = true;
   firstPersonOnly = true;
   useEyePoint = true;
   numWeapons = 2;

   cameraDefaultFov = 90.0;
   cameraMinFov = 5.0;
   cameraMaxFov = 120.0;

   targetNameTag = 'Stryker';
   targetTypeTag = 'Turret';
};

datablock TurretData(StrykerUnderTurret) : UniversalDamageProfile
{
   className               = VehicleTurret;
   catagory                = "Turrets";
   shapeFile               = "turret_belly_chopper.dts";
   preload                 = true;

   mass                    = 1.0;  // Not really relevant
   repairRate              = 0;
   maxDamage               = Stryker.maxDamage;
   destroyedLevel          = Stryker.destroyedLevel;

   thetaMin                = 60;
   thetaMax                = 180;

   // capacitor
   maxCapacitorEnergy      = 250;
   capacitorRechargeRate   = 1.0;

   inheritEnergyFromMount  = true;
   firstPersonOnly         = true;
   useEyePoint             = true;
   numWeapons              = 2;

   targetNameTag           = 'Stryker';
   targetTypeTag           = 'Belly Turret';
};

function Stryker::onAdd(%this, %obj)
{
    Parent::onAdd(%this, %obj);

    %obj.mountImage("FirestormPilotTurret", 3);

    %main = %this.installTurret(%obj, StrykerTurret, 3);
    %main.mountImage(GenericParamImage, 0);
//    %main.mountImage(PlasmaBarrelLarge, 2);

    %under = %this.installTurret(%obj, StrykerUnderTurret, 10);
    %under.mountImage(GenericParamImage, 0);
//    %under.mountImage(PlasmaBarrelLarge, 2);
}

function Stryker::playerMounted(%data, %obj, %player, %node)
{
   if(%node == 0)
   {
      // pilot position
	   commandToClient(%player.client, 'setHudMode', 'Pilot', "HAPC", %node);

       %data.onPilotSeated(%obj, %player);
   }
   else if(%node == 1)
      %data.onGunnerSeated(%obj, %player, 10);
   else if(%node == 2)
      %data.onGunnerSeated(%obj, %player, 3);
   else
	   commandToClient(%player.client, 'setHudMode', 'Passenger', "HAPC", %node);

   %data.updatePassengerString(%obj);

   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}
