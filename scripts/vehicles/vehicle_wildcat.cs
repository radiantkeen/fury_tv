//**************************************************************
// WILDCAT GRAV CYCLE
//**************************************************************
//**************************************************************
// SOUNDS
//**************************************************************
datablock AudioProfile(ScoutSqueelSound)
{
   filename    = "fx/vehicles/outrider_skid.wav";
   description = ClosestLooping3d;
   preload = true;
};

// Scout
datablock AudioProfile(ScoutEngineSound)
{
   filename    = "fx/vehicles/outrider_engine.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(ScoutThrustSound)
{
   filename    = "fx/vehicles/outrider_boost.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

//**************************************************************
// VEHICLE CHARACTERISTICS
//**************************************************************

datablock FlyingVehicleData(ScoutVehicle) : UniversalDamageProfile
{
   spawnOffset = "0 0 2";

   catagory = "Vehicles";
   shapeFile = "vehicle_grav_scout_Nova.dts";
   multipassenger = false;

   debrisShapeName = "vehicle_grav_scout_debris.dts";
   debris = ShapeDebris;
   renderWhenDestroyed = false;

   drag    = 0.15;
   density = 1.0;

   lightOnly = true;
   mountPose[0] = scoutRoot;
   cameraMaxDist = 7;
   cameraOffset = 1.1;
   cameraLag = 0.8;
   numMountPoints = 1;
   isProtectedMountPoint[0] = true;
   explosion = VehicleExplosion;
	explosionDamage = 1.0;
	explosionRadius = 2.5;

   maxDamage = 3.0;
   destroyedLevel = 3.0;

   isShielded = false;
   energyPerDamagePoint = 55;
   maxEnergy = 150;      // Afterburner and any energy weapon pool
   minDrag = 30;           // Linear Drag (eventually slows you down when not thrusting...constant drag)
   rotationalDrag = 900;        // Anguler Drag (dampens the drift after you stop moving the mouse...also tumble drag)
   rechargeRate = 25 / 32;

   maxAutoSpeed = 8;       // Autostabilizer kicks in when less than this speed. (meters/second)
   autoAngularForce = 360;       // Angular stabilizer force (this force levels you out when autostabilizer kicks in)
   autoLinearForce = 150;        // Linear stabilzer force (this slows you down when autostabilizer kicks in)
   autoInputDamping = 0.9;      // Dampen control input so you don't whack out at very slow speeds

   // Maneuvering
   maxSteerinAngle = $g_PI * 0.8;    // Placeholder value for now, T2 v12 engine actually uses this value and it defaults to pi
   maxSteeringAngle = $g_PI * 0.8;    // Max radians you can rotate the wheel. Smaller number is more maneuverable. Defaults to pi
   horizontalSurfaceForce = 8;   // Horizontal center "wing" (provides "bite" into the wind for climbing/diving and turning)
   verticalSurfaceForce = 6;     // Vertical center "wing" (controls side slip. lower numbers make MORE slide.)
   maneuveringForce = 4170;      // Horizontal jets (W,S,D,A key thrust)
   steeringForce = 1500;         // Steering jets (force applied when you move the mouse)
   steeringRollForce = 550;      // Steering jets (how much you heel over when you turn)
   rollForce = 4;                // Auto-roll (self-correction to right you after you roll/invert)
   hoverHeight = 3;        // Height off the ground at rest
   createHoverHeight = 5;  // Height off the ground when created
   maxForwardSpeed = 250;

   // Turbo Jet
   jetForce = 7000;      // Afterburner thrust (this is in addition to normal thrust)
   minJetEnergy = 10;     // Afterburner can't be used if below this threshhold.
   jetEnergyDrain = 65 / 32;       // Energy use of the afterburners (low number is less drain...can be fractional)                                                                                                                                                                                                                                                                                          // Auto stabilize speed
   vertThrustMultiple = 0;

   // Rigid body
   mass = 100;        // Mass of the vehicle
   bodyFriction = 0;     // Don't mess with this.
   bodyRestitution = 0.5;   // When you hit the ground, how much you rebound. (between 0 and 1)
   minRollSpeed = 0;     // Don't mess with this.
   softImpactSpeed = 40;       // Sound hooks. This is the soft hit.
   hardImpactSpeed = 80;    // Sound hooks. This is the hard hit.

   // Ground Impact Damage (uses DamageType::Ground)
   minImpactSpeed = 28;      // If hit ground at speed above this then it's an impact. Meters/second
   speedDamageScale = 5.0;

   // Object Impact Damage (uses DamageType::Impact)
   collDamageThresholdVel = 27.78;
   collDamageMultiplier   = 5.0;

   //
   minTrailSpeed = 28;      // The speed your contrail shows up at.
   trailEmitter = ContrailEmitter;
   forwardJetEmitter = TurboJetEmitter;
   downJetEmitter = TurboJetEmitter;

   //
   jetSound         = ScoutSqueelSound;
   engineSound      = ScoutEngineSound;
   softImpactSound = SoftImpactSound;
   hardImpactSound = HardImpactSound;
   //wheelImpactSound = WheelImpactSound;

   //
   softSplashSoundVelocity = 10.0;
   mediumSplashSoundVelocity = 15.0;
   hardSplashSoundVelocity = 20.0;
   exitSplashSoundVelocity = 10.0;

   exitingWater      = VehicleExitWaterMediumSound;
   impactWaterEasy   = VehicleImpactWaterSoftSound;
   impactWaterMedium = VehicleImpactWaterMediumSound;
   impactWaterHard   = VehicleImpactWaterMediumSound;
   waterWakeSound    = VehicleWakeMediumSplashSound;

   dustEmitter = VehicleLiftoffDustEmitter;
   triggerDustHeight = 4.0;
   dustHeight = 1.0;

   damageEmitter[0] = SmallLightDamageSmoke;
   damageEmitter[1] = SmallLightDamageSmoke;
   damageEmitter[2] = DamageBubbles;
   damageEmitterOffset[0] = "0.0 -1.5 0.5";
   damageLevelTolerance[0] = 0.6;
   damageLevelTolerance[1] = 0.8;
   numDmgEmitterAreas = 1;

   minMountDist = 4;

   splashEmitter[0] = VehicleFoamDropletsEmitter;
   splashEmitter[1] = VehicleFoamEmitter;

   shieldImpact = VehicleShieldImpact;

   sensorColor = "8 212 0";
   sensorData = InterceptorPulseSensor;
   sensorRadius = InterceptorPulseSensor.detectRadius;
   cmdCategory = "Tactical";
   cmdIcon = CMDHoverScoutIcon;
   cmdMiniIconName = "commander/MiniIcons/com_landscout_grey";
   targetNameTag = 'Skycutter';
   targetTypeTag = 'Turbobike';

//   runningLight[0] = WildcatLight1;
//   runningLight[1] = WildcatLight2;
//   runningLight[2] = WildcatLight3;

   shieldEffectScale = "0.937 1.125 0.60";

   physicsType = $VehiclePhysics::ForwardsOnly;

   checkMinVelocity = 35;
   checkMinHeight = 15;
   checkMinReverseSpeed = 15;
};

datablock ShapeBaseImageData(HBDecal1) : MainAPEImage
{
   offset = "0.8 0 -0.5";
   rotation = degreesToRotation("180 90 0"); //"1 0 0 180";
   shapeFile = "TR2weapon_mortar.dts";   // hjehje

   stateEmitter[3]       = "FlyerJetEmitter"; // emitter
   stateEmitterTime[3]       = 0.300; // 100ms
   stateEmitterNode[3]       = 0; // model node (most models dont come with any nodes)
};

datablock ShapeBaseImageData(HBDecal2) : MainAPEImage
{
   offset = "-0.8 0 -0.275";
   rotation = degreesToRotation("180 -90 0"); //"1 0 0 180";
   shapeFile = "TR2weapon_mortar.dts";   // hjehje

   stateEmitter[3]       = "FlyerJetEmitter"; // emitter // WildcatJetEmitter
   stateEmitterTime[3]       = 0.300;
   stateEmitterNode[3]       = 0; // model node (most models dont come with any nodes)
};

datablock ShapeBaseImageData(HBEngine1) : EngineAPEImage
{
   shapeFile = "turret_assaulttank_plasma.dts";
   offset = "0.53 -0.80 -0.144";
   rotation = degreesToRotation("0 -45 180"); //"0 1 0 -45"; // 90

//   stateName[0]             = "Activate";
//   stateSequence[0]         = "Activate";
   stateSequence[2] = "activate";

   stateDirection[3] = true;
   stateSequence[3] = "activate";

   stateSequence[4] = "activate";
};

datablock ShapeBaseImageData(HBEngine2) : EngineAPEImage
{
   shapeFile = "turret_assaulttank_plasma.dts";
   offset = "-0.53 -0.80 -0.144";
   rotation = degreesToRotation("0 45 180"); //"0 1 0 45";

//   stateName[0]             = "Activate";
//   stateSequence[0]         = "Activate";
   stateSequence[2] = "activate";

   stateDirection[3] = true;
   stateSequence[3] = "activate";

   stateSequence[4] = "activate";
};

datablock ShapeBaseImageData(HBEngineParam) : EngineAPEImage
{
   mountPoint = 1;
   shapeFile = "turret_assaulttank_plasma.dts";

   offset = "0 -2.75 0.2"; //.125 .075 -0.2
   rotation = "0 0 1 180"; //"1 0 45 90";

   stateName[2] = "JetOn";
   stateDirection[2] = true;
   stateSequence[2] = "activate";

   stateName[3] = "JetMaintain";
   stateDirection[3] = true;
   stateSequence[3] = "activate";

   stateName[4] = "JetOff";
   stateSequence[4] = "activate";
};
