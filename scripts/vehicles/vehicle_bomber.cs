//**************************************************************
// THUNDERSWORD BOMBER
//**************************************************************
//**************************************************************
// SOUNDS
//**************************************************************
datablock AudioProfile(BomberFlyerEngineSound)
{
   filename    = "fx/vehicles/bomber_engine.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(BomberFlyerThrustSound)
{
   filename    = "fx/vehicles/bomber_boost.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(FusionExpSound)
// Sound played when mortar impacts on target
{
   filename    = "fx/powered/turret_mortar_explode.wav";
   description = "AudioBIGExplosion3d";
   preload = true;
};

datablock AudioProfile(BomberTurretFireSound)
{
   filename    = "fx/vehicles/bomber_turret_fire.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(BomberTurretActivateSound)
{
   filename    = "fx/vehicles/bomber_turret_activate.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(BomberTurretReloadSound)
{
   filename    = "fx/vehicles/bomber_turret_reload.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(BomberTurretIdleSound)
{
   filename    = "fx/misc/diagnostic_on.wav";
   description = ClosestLooping3d;
   preload = true;
};

datablock AudioProfile(BomberTurretDryFireSound)
{
   filename    = "fx/vehicles/bomber_turret_dryfire.wav";
   description = AudioClose3d;
   preload = true;
};
   
datablock AudioProfile(BomberBombReloadSound)
{
   filename    = "fx/vehicles/bomber_bomb_reload.wav";
   description = AudioClose3d;
   preload = true;
   effect = BomberBombReloadEffect;
};

datablock AudioProfile(BomberBombProjectileSound)
{
   filename    = "fx/vehicles/bomber_bomb_projectile.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(BomberBombDryFireSound)
{
   filename    = "fx/vehicles/bomber_bomb_dryfire.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(BomberBombFireSound)
{
   filename    = "fx/vehicles/bomber_bomb_reload.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(BomberBombIdleSound)
{
   filename    = "fx/misc/diagnostic_on.wav";
   description = ClosestLooping3d;
   preload = true;
};

//**************************************************************
// VEHICLE CHARACTERISTICS
//**************************************************************

datablock FlyingVehicleData(BomberFlyer) : UniversalDamageProfile
{
   spawnOffset = "0 0 2";

   catagory = "Vehicles";
   shapeFile = "vehicle_air_bomber.dts";
   multipassenger = true;
   computeCRC = true;

   weaponNode = 1;

   debrisShapeName = "vehicle_air_bomber_debris.dts";
   debris = ShapeDebris;
   renderWhenDestroyed = false;

   drag    = 0.2;
   density = 1.0;

   mountPose[0] = sitting;
   mountPose[1] = sitting;
   numMountPoints = 3;  
   isProtectedMountPoint[0] = true;
   isProtectedMountPoint[1] = true;
   isProtectedMountPoint[2] = true;

   cameraDefaultFov = 90.0;
   cameraMinFov = 5.0;
   cameraMaxFov = 120.0;
   
   cameraMaxDist = 22;
   cameraOffset = 5;
   cameraLag = 1.0;
   explosion = BigAirVehicleExplosion;
	explosionDamage = 22.00;
	explosionRadius = 15.0;

   maxDamage = 88.00;
   destroyedLevel = 88.00;

   isShielded = true;
   energyPerDamagePoint = 150;
   maxEnergy = 700;      // Afterburner and any energy weapon pool
   minDrag = 60;           // Linear Drag (eventually slows you down when not thrusting...constant drag)
   rotationalDrag = 1800;        // Angular Drag (dampens the drift after you stop moving the mouse...also tumble drag)
   rechargeRate = 100 / 32;

   // Auto stabilize speed
   maxAutoSpeed = 15;       // Autostabilizer kicks in when less than this speed. (meters/second)
   autoAngularForce = 1500;      // Angular stabilizer force (this force levels you out when autostabilizer kicks in)
   autoLinearForce = 300;        // Linear stabilzer force (this slows you down when autostabilizer kicks in)
   autoInputDamping = 0.95;      // Dampen control input so you don't whack out at very slow speeds

   // Maneuvering
   maxSteerinAngle = $g_PI * 0.95;    // Placeholder value for now, T2 v12 engine actually uses this value and it defaults to pi
   maxSteeringAngle = $g_PI * 0.95;    // Max radians you can rotate the wheel. Smaller number is more maneuverable. Defaults to pi
   horizontalSurfaceForce = 5;   // Horizontal center "wing" (provides "bite" into the wind for climbing/diving and turning)
   verticalSurfaceForce = 7;     // Vertical center "wing" (controls side slip. lower numbers make MORE slide.)
   maneuveringForce = 5500;      // Horizontal jets (W,S,D,A key thrust) // z0dd - ZOD, 9/8/02. Was 4700
   steeringForce = 1200;         // Steering jets (force applied when you move the mouse)
   steeringRollForce = 300;      // Steering jets (how much you heel over when you turn)
   rollForce = 8;                // Auto-roll (self-correction to right you after you roll/invert)
   hoverHeight = 5;        // Height off the ground at rest
   createHoverHeight = 3;  // Height off the ground when created
   maxForwardSpeed = 166.667;  // speed in which forward thrust force is no longer applied (meters/second) // z0dd - ZOD, 9/8/02. Was 85

   // Turbo Jet
   jetForce = 9500;      // Afterburner thrust (this is in addition to normal thrust) // z0dd - ZOD, 9/8/02. Was 3000
   minJetEnergy = 20.0;     // Afterburner can't be used if below this threshhold.
   jetEnergyDrain = 225 / 32;       // Energy use of the afterburners (low number is less drain...can be fractional)
   vertThrustMultiple = 2.5; // z0dd - ZOD, 9/8/02. Was 3.0

   dustEmitter = LargeVehicleLiftoffDustEmitter;
   triggerDustHeight = 4.0;
   dustHeight = 2.0;

   damageEmitter[0] = SmallLightDamageSmoke; //LightDamageSmoke;
   damageEmitter[1] = OnFireEmitter;
   damageEmitter[2] = DamageBubbles;
   damageEmitterOffset[0] = "3.0 -3.0 0.0";
   damageEmitterOffset[1] = "-3.0 -3.0 0.0";
   damageLevelTolerance[0] = 0.6;
   damageLevelTolerance[1] = 0.8;
   numDmgEmitterAreas = 2;

   // Reduction of impulse effects on sensitive shapes
   shapeMaxForce = 2000.0;

   // Rigid body
   mass = 350;        // Mass of the vehicle
   bodyFriction = 0;     // Don't mess with this.
   bodyRestitution = 0.5;   // When you hit the ground, how much you rebound. (between 0 and 1)
   minRollSpeed = 0;     // Don't mess with this.
   softImpactSpeed = 20;       // Sound hooks. This is the soft hit.
   hardImpactSpeed = 25;    // Sound hooks. This is the hard hit.

   // Ground Impact Damage (uses DamageType::Ground)
   minImpactSpeed = 28;      // If hit ground at speed above this then it's an impact. Meters/second
   speedDamageScale = 1.5;

   // Object Impact Damage (uses DamageType::Impact)
   collDamageThresholdVel = 27.78;
   collDamageMultiplier   = 1.5;

   //
   minTrailSpeed = 24;      // The speed your contrail shows up at.
   trailEmitter = ContrailEmitter;
   forwardJetEmitter = RealFlyerJetEmitter;
   downJetEmitter = RealFlyerJetEmitter;

   //
   jetSound = BomberFlyerThrustSound;
   engineSound = BomberFlyerEngineSound;
   softImpactSound = SoftImpactSound;
   hardImpactSound = HardImpactSound;
   //wheelImpactSound = WheelImpactSound;

   //
   softSplashSoundVelocity = 15.0; 
   mediumSplashSoundVelocity = 20.0;   
   hardSplashSoundVelocity = 30.0;   
   exitSplashSoundVelocity = 10.0;
   
   exitingWater      = VehicleExitWaterHardSound;
   impactWaterEasy   = VehicleImpactWaterSoftSound;
   impactWaterMedium = VehicleImpactWaterMediumSound;
   impactWaterHard   = VehicleImpactWaterHardSound;
   waterWakeSound    = VehicleWakeHardSplashSound; 
  
   minMountDist = 4;

   splashEmitter[0] = VehicleFoamDropletsEmitter;
   splashEmitter[1] = VehicleFoamEmitter;

   shieldImpact = VehicleShieldImpact;

   cmdCategory = "Tactical";
   cmdIcon = CMDFlyingBomberIcon;
   cmdMiniIconName = "commander/MiniIcons/com_bomber_grey";
   targetNameTag = 'Retaliator';
   targetTypeTag = 'Gunship';
   sensorData = VehiclePulseSensor;
   sensorRadius = VehiclePulseSensor.detectRadius; // z0dd - ZOD, 4/25/02. Allows sensor to be shown on CC

   checkRadius = 7.1895;
   observeParameters = "1 10 10";
   shieldEffectScale = "0.75 0.975 0.375";
   showPilotInfo = 1;
   
   physicsType = $VehiclePhysics::ForwardsOnly;

   checkMinVelocity = 25;
   checkMinHeight = 20;
   checkMinReverseSpeed = 10;
};

datablock ShapeBaseImageData(FirestormPilotTurret) : EngineAPEImage
{
   shapeFile = "turret_belly_base.dts";
   offset = "0.0 1.675 -1.5";
   rotation = "0 0 1 0";
   

   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
};

datablock ShapeBaseImageData(RetaliatorPilotTurret) : EngineAPEImage
{
   shapeFile = "turret_belly_base.dts";
   offset = "0.0 4.85 -0.35";
   mountPoint = 1;
   rotation = "0 0 1 0"; //degreesToRotation("0 -45 180"); //"0 1 0 -45"; // 90


   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
};

datablock ShapeBaseImageData(AvengerEngine1) : MainAPEImage
{
   shapeFile = "turret_muzzlepoint.dts";
//   shapeFile = "turret_assaulttank_plasma.dts";
   offset = "-4.75 -11 0";
   rotation = "0 1 0 65"; //calcThisRotD("0 37.5 180");

//   stateName[0]             = "Activate";
//   stateSequence[0]         = "Activate";

   stateEmitter[3]       = "FlyerJetEmitter"; // emitter
   stateEmitterTime[3]       = 0.512; // 100ms
   stateEmitterNode[3]       = 0; // model node (most models dont come with any nodes)
};

datablock ShapeBaseImageData(AvengerEngine2) : MainAPEImage
{
   shapeFile = "turret_muzzlepoint.dts";
   offset = "4.75 -11 0";
   rotation = "0 1 0 -65"; //calcThisRotD("0 37.5 180");

   stateEmitter[3]       = "FlyerJetEmitter"; // emitter
   stateEmitterTime[3]       = 0.512; // 100ms
   stateEmitterNode[3]       = 0; // model node (most models dont come with any nodes)
};

datablock ShapeBaseImageData(AvengerEngine3) : MainAPEImage
{
   shapeFile = "turret_muzzlepoint.dts";
   offset = "-4.75 -11 -2";
   rotation = "0 1 0 115"; //calcThisRotD("0 37.5 180");

   stateEmitter[3]       = "FlyerJetEmitter"; // emitter
   stateEmitterTime[3]       = 0.512; // 100ms
   stateEmitterNode[3]       = 0; // model node (most models dont come with any nodes)
};

datablock ShapeBaseImageData(AvengerEngine4) : MainAPEImage
{
   shapeFile = "turret_muzzlepoint.dts";
   offset = "4.75 -11 -2";
   rotation = "0 1 0 -115"; //calcThisRotD("0 37.5 180");

   stateEmitter[3]       = "FlyerJetEmitter"; // emitter
   stateEmitterTime[3]       = 0.512; // 100ms
   stateEmitterNode[3]       = 0; // model node (most models dont come with any nodes)
};

datablock ShapeBaseImageData(AvengerDecal1) : EngineAPEImage
{
   shapeFile = "vehicle_grav_scout.dts";
   offset = "-4.75 -8 0";
   rotation = "0 1 0 65"; //calcThisRotD("0 37.5 180");
};

datablock ShapeBaseImageData(AvengerDecal2) : AvengerDecal1
{
   offset = "4.75 -8 0";
   rotation = "0 1 0 -65";
};

datablock ShapeBaseImageData(AvengerDecal3) : AvengerDecal1
{
   offset = "-4.75 -8 -2";
   rotation = "0 1 0 115";
};

datablock ShapeBaseImageData(AvengerDecal4) : AvengerDecal1
{
   offset = "4.75 -8 -2";
   rotation = "0 1 0 -115";
};

datablock StaticShapeData(AvengerStaticF)
{
   shapeFile = "vehicle_air_bomber.dts";

   dynamicType = $TypeMasks::VehicleObjectType;

   maxDamage = BomberFlyer.maxDamage;
   destroyedLevel = BomberFlyer.destroyedLevel;

   targetNameTag = 'Retaliator';
   targetTypeTag = 'Escort Gunship';
};

function AvengerStaticF::onDamage(%data, %obj)
{
   %newDamageVal = %obj.getDamageLevel();
   if(%obj.lastDamageVal !$= "")
      if(isObject(%obj.getObjectMount()) && %obj.lastDamageVal > %newDamageVal)
         %obj.getObjectMount().setDamageLevel(%newDamageVal);
   %obj.lastDamageVal = %newDamageVal;
}

function AvengerStaticF::damageObject(%this, %targetObject, %sourceObject, %position, %amount, %damageType ,%vec, %client, %projectile)
{
   //If vehicle turret is hit then apply damage to the vehicle
   %vehicle = %targetObject.getObjectMount();

   if(%vehicle)
      %vehicle.getDataBlock().damageObject(%vehicle, %sourceObject, %position, %amount, %damageType, %vec, %client, %projectile);
}

//-------------------------------------
// BOMBER BOMB PROJECTILE
//-------------------------------------

datablock BombProjectileData(BomberBomb)
{
   projectileShapeName  = "bomb.dts";
   emitterDelay         = -1;
   directDamage         = 0.0;
   hasDamageRadius      = true;
   indirectDamage       = 1.1;
   damageRadius         = 30;
   radiusDamageType     = $DamageType::BomberBombs;
   kickBackStrength     = 4500;  // z0dd - ZOD, 4/25/02. Was 2500

   explosion            = "VehicleBombExplosion";
   velInheritFactor     = 1.0;

   grenadeElasticity    = 0.25;
   grenadeFriction      = 0.4;
   armingDelayMS        = 2000;
   muzzleVelocity       = 0.1;
   drag                 = 0.3;
   gravityMod		= 20.0 / mabs($Classic::gravSetting); // z0dd - ZOD, 8/28/02. Compensate for our grav change. Math: base grav / our grav

   minRotSpeed          = "60.0 0.0 0.0";
   maxRotSpeed          = "80.0 0.0 0.0";
   scale                = "1.0 1.0 1.0";
   
   sound                = BomberBombProjectileSound;
};

//-------------------------------------
// BOMBER BOMB CHARACTERISTICS
//-------------------------------------

datablock StaticShapeData(DropBombs)
{
   catagory             = "Turrets";
   shapeFile            = "bombers_eye.dts";
   maxDamage            = 1.0;
   disabledLevel        = 0.6;
   destroyedLevel       = 0.8;
};

//-------------------------------------
// BOMBER BEACON
//-------------------------------------
datablock StaticShapeData(BomberBeacon)
{
   shapeFile = "turret_muzzlepoint.dts";
   targetNameTag = 'beacon';
   isInvincible = true;
   
   dynamicType = $TypeMasks::SensorObjectType;
};

datablock TurretData(BomberTurret) : TurretDamageProfile
{
   className               = VehicleTurret;
   catagory                = "Turrets";
   shapeFile               = "turret_belly_chopper.dts"; // turret_belly_base
   preload                 = true;

   mass                    = 1.0;  // Not really relevant
   repairRate              = 0;
   maxDamage               = BomberFlyer.maxDamage;
   destroyedLevel          = BomberFlyer.destroyedLevel;

   thetaMin                = 60;
   thetaMax                = 180;

   // capacitor
   maxCapacitorEnergy      = 250;
   capacitorRechargeRate   = 0.8;

   inheritEnergyFromMount  = true;
   firstPersonOnly         = true;
   useEyePoint             = true;
   numWeapons              = 3;

   targetNameTag           = 'Thundersword Belly';
   targetTypeTag           = 'Turret';
};

datablock ExplosionData(BomberFusionBoltExplosion)
{
   soundProfile   = blasterExpSound;
   shockwave      = BomberFusionShockwave;
   emitter[0]     = BomberFusionExplosionEmitter;
};

datablock LinearFlareProjectileData(BomberFusionBolt)
{
   projectileShapeName        = "";
   directDamage               = 0.35;
   directDamageType           = $DamageType::BellyTurret;
   hasDamageRadius            = false;
   explosion                  = BomberFusionBoltExplosion;
   sound                      = BlasterProjectileSound;

   dryVelocity                = 200.0;
   wetVelocity                = 200.0;
   velInheritFactor           = 1.0;
   fizzleTimeMS               = 2000;
   lifetimeMS                 = 3000;
   explodeOnDeath             = false;
   reflectOnWaterImpactAngle  = 0.0;
   explodeOnWaterImpact       = true;
   deflectionOnWaterImpact    = 0.0;
   fizzleUnderwaterMS         = -1;

   activateDelayMS            = 100;

   numFlares                  = 0;
   size                       = 0.15;
   flareColor                 = "0.7 0.3 1.0";
   flareModTexture            = "flaremod";
   flareBaseTexture           = "flarebase";
};

datablock TurretImageData(AIAimingTurretBarrel)
{
   shapeFile            = "turret_muzzlepoint.dts";
   mountPoint           = 3;

   projectile           = BomberFusionBolt;

   // Turret parameters
   activationMS         = 1000;
   deactivateDelayMS    = 1500;
   thinkTimeMS          = 200;
   degPerSecTheta       = 500;
   degPerSecPhi         = 800;

   attackRadius         = 75;
};
