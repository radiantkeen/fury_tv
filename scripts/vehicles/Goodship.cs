datablock FlyingVehicleData(Goodship) : UniversalDamageProfile
{
   spawnOffset = "0 0 2";

   catagory = "Vehicles";
   shapeFile = "vehicle_grav_tank.dts";
   multipassenger = true;
   computeCRC = true;

   weaponNode = 1;

   debrisShapeName = "vehicle_land_assault_debris.dts";
   debris = ShapeDebris;
   renderWhenDestroyed = false;

   drag    = 0.2;
   density = 1.0;

   mountPose[0] = sitting;
   mountPose[1] = sitting;
   numMountPoints = 2;
   isProtectedMountPoint[0] = true;
   isProtectedMountPoint[1] = true;

   cameraDefaultFov = 90.0;
   cameraMinFov = 5.0;
   cameraMaxFov = 120.0;

   cameraMaxDist = 22;
   cameraOffset = 5;
   cameraLag = 1.0;
   explosion = LargeAirVehicleExplosion;
	explosionDamage = 0.5;
	explosionRadius = 5.0;

   maxDamage = 50.00;
   destroyedLevel = 50.00;

   isShielded = true;
   energyPerDamagePoint = 150;
   maxEnergy = 400;      // Afterburner and any energy weapon pool
   minDrag = 60;           // Linear Drag (eventually slows you down when not thrusting...constant drag)
   rotationalDrag = 1800;        // Angular Drag (dampens the drift after you stop moving the mouse...also tumble drag)
   rechargeRate = 0.8;

   // Auto stabilize speed
   maxAutoSpeed = 15;       // Autostabilizer kicks in when less than this speed. (meters/second)
   autoAngularForce = 1500;      // Angular stabilizer force (this force levels you out when autostabilizer kicks in)
   autoLinearForce = 300;        // Linear stabilzer force (this slows you down when autostabilizer kicks in)
   autoInputDamping = 0.95;      // Dampen control input so you don't whack out at very slow speeds

   // Maneuvering
   maxSteeringAngle = 5;    // Max radiens you can rotate the wheel. Smaller number is more maneuverable.
   horizontalSurfaceForce = 5;   // Horizontal center "wing" (provides "bite" into the wind for climbing/diving and turning)
   verticalSurfaceForce = 8;     // Vertical center "wing" (controls side slip. lower numbers make MORE slide.)
   maneuveringForce = 4725;      // Horizontal jets (W,S,D,A key thrust) // z0dd - ZOD, 9/8/02. Was 4700
   steeringForce = 1100;         // Steering jets (force applied when you move the mouse)
   steeringRollForce = 300;      // Steering jets (how much you heel over when you turn)
   rollForce = 8;                // Auto-roll (self-correction to right you after you roll/invert)
   hoverHeight = 5;        // Height off the ground at rest
   createHoverHeight = 3;  // Height off the ground when created
   maxForwardSpeed = 87;  // speed in which forward thrust force is no longer applied (meters/second) // z0dd - ZOD, 9/8/02. Was 85

   // Turbo Jet
   jetForce = 3100;      // Afterburner thrust (this is in addition to normal thrust) // z0dd - ZOD, 9/8/02. Was 3000
   minJetEnergy = 40.0;     // Afterburner can't be used if below this threshhold.
   jetEnergyDrain = 3.0;       // Energy use of the afterburners (low number is less drain...can be fractional)
   vertThrustMultiple = 3.5; // z0dd - ZOD, 9/8/02. Was 3.0

   dustEmitter = LargeVehicleLiftoffDustEmitter;
   triggerDustHeight = 4.0;
   dustHeight = 2.0;

   damageEmitter[0] = LightDamageSmoke;
   damageEmitter[1] = HeavyDamageSmoke;
   damageEmitter[2] = DamageBubbles;
   damageEmitterOffset[0] = "3.0 -3.0 0.0 ";
   damageEmitterOffset[1] = "-3.0 -3.0 0.0 ";
   damageLevelTolerance[0] = 0.3;
   damageLevelTolerance[1] = 0.7;
   numDmgEmitterAreas = 2;

   // Rigid body
   mass = 350;        // Mass of the vehicle
   bodyFriction = 0;     // Don't mess with this.
   bodyRestitution = 0.5;   // When you hit the ground, how much you rebound. (between 0 and 1)
   minRollSpeed = 0;     // Don't mess with this.
   softImpactSpeed = 20;       // Sound hooks. This is the soft hit.
   hardImpactSpeed = 25;    // Sound hooks. This is the hard hit.

   // Ground Impact Damage (uses DamageType::Ground)
   minImpactSpeed = 20;      // If hit ground at speed above this then it's an impact. Meters/second
   speedDamageScale = 0.060;

   // Object Impact Damage (uses DamageType::Impact)
   collDamageThresholdVel = 25;
   collDamageMultiplier   = 0.020;

   //
   minTrailSpeed = 15;      // The speed your contrail shows up at.
   trailEmitter = ContrailEmitter;
   forwardJetEmitter = TankJetEmitter;
   downJetEmitter = TankJetEmitter;

   //
   jetSound = Engine3ThrustSound;
   engineSound = GoodshipEngineSound;
   softImpactSound = SoftImpactSound;
   hardImpactSound = HardImpactSound;
   //wheelImpactSound = WheelImpactSound;

   //
   softSplashSoundVelocity = 15.0;
   mediumSplashSoundVelocity = 20.0;
   hardSplashSoundVelocity = 30.0;
   exitSplashSoundVelocity = 10.0;

   exitingWater      = VehicleExitWaterHardSound;
   impactWaterEasy   = VehicleImpactWaterSoftSound;
   impactWaterMedium = VehicleImpactWaterMediumSound;
   impactWaterHard   = VehicleImpactWaterHardSound;
   waterWakeSound    = VehicleWakeHardSplashSound;

   minMountDist = 4;

   splashEmitter[0] = VehicleFoamDropletsEmitter;
   splashEmitter[1] = VehicleFoamEmitter;

   shieldImpact = VehicleShieldImpact;

   cmdCategory = "Tactical";
   cmdIcon = CMDFlyingBomberIcon;
   cmdMiniIconName = "commander/MiniIcons/com_bomber_grey";
   targetNameTag = 'Goodship';
   targetTypeTag = 'Lollipop';
   sensorData = VehiclePulseSensor;
   sensorRadius = VehiclePulseSensor.detectRadius; // z0dd - ZOD, 4/25/02. Allows sensor to be shown on CC

   checkRadius = 7.1895;
   observeParameters = "1 10 10";
   shieldEffectScale = "0.75 0.975 0.375";
   showPilotInfo = 1;
};

datablock TurretData(GoodshipTurret) : UniversalDamageProfile
{
   className      = VehicleTurret;
   catagory       = "Turrets";
   shapeFile      = "turret_base_large.dts";
   preload        = true;

   mass           = 1.0;  // Not really relevant

   maxEnergy               = 1;
   maxDamage               = Goodship.maxDamage;
   destroyedLevel          = Goodship.destroyedLevel;
   repairRate              = 0;

   // capacitor
   maxCapacitorEnergy      = 250;
   capacitorRechargeRate   = 1.0;

   thetaMin = 0;
   thetaMax = 180;

   inheritEnergyFromMount = true;
   firstPersonOnly = true;
   useEyePoint = true;
   numWeapons = 2;

   cameraDefaultFov = 90.0;
   cameraMinFov = 5.0;
   cameraMaxFov = 120.0;

   targetNameTag = 'Goodship Auto';
   targetTypeTag = 'Turret';
};

function GoodshipTurret::selectTarget(%this, %turret)
{
    %veh = %turret.thisVehicle;

    if(%veh.gunshipBeacon > 0)
        %turret.setTargetObject(%veh.gunshipBeacon);
    else
        %turret.clearTarget();
}

function Goodship::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);
//   %obj.schedule(5500, "playThread", $ActivateThread, "activate");

    %main = %this.installTurret(%obj, GoodshipTurret, 10);
//    %main.setScale("4 4 4");
    %main.schedule(5500, "playThread", $ActivateThread, "Deploy");
    %main.mountImage(AssaultTurretParam, 0);
//.    %main.mountImage(PlasmaBarrelLarge, 2);

    %obj.mainTurret = %main;
    %main.thisVehicle = %obj;
}

function Goodship::playerMounted(%data, %obj, %player, %node)
{
   if(%node == 0)
   {
      // pilot position
	   commandToClient(%player.client, 'setHudMode', 'Pilot', "HAPC", %node);

       %data.onPilotSeated(%obj, %player);
   }
   else
   {
      // all others
	   commandToClient(%player.client, 'setHudMode', 'Passenger', "HAPC", %node);
   }
   // build a space-separated string representing passengers
   // 0 = no passenger; 1 = passenger (e.g. "1 0 0 1 1 0 ")
   %passString = buildPassengerString(%obj);
	// send the string of passengers to all mounted players
	for(%i = 0; %i < %data.numMountPoints; %i++)
		if(%obj.getMountNodeObject(%i) > 0)
		   commandToClient(%obj.getMountNodeObject(%i).client, 'checkPassengers', %passString);

   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}

function Goodship::triggerMine(%data, %obj, %player, %slot, %state)
{
    if(!%state)
        return;

    if(%obj.gunshipBeacon > 0)
    {
        %obj.gunshipBeacon.delete();
        %obj.gunshipBeacon = 0;
//        %obj.turretR.setImageAmmo(0, true);
//        %obj.turretL.setImageAmmo(0, true);
        %obj.mainTurret.setAutoFire(false);
        return;
    }

    %obj.gunshipBeacon = new WayPoint()
    {
        position = "0 0 10000";
        rotation = "1 0 0 0";
        scale = "1 1 1";
        name = "";
        dataBlock = "WayPointMarker";
        lockCount = "0";
        homingCount = "0";
        team = %obj.team;
    };

    MissionCleanup.add(%obj.gunshipBeacon);
    %obj.mainTurret.setAutoFire(true);
//    %obj.turretR.setTargetObject(%obj.gunshipBeacon);
//    %obj.turretL.setTargetObject(%obj.gunshipBeacon);
//    %obj.turretR.setImageTrigger(0, false);
//    %obj.turretL.setImageTrigger(0, false);
    %obj.mainTurret.setImageAmmo(2, true);
    %data.trackBeaconPos(%obj, %player);
}

function Goodship::trackBeaconPos(%data, %obj, %pilot)
{
    if(%obj.gunshipBeacon > 0)
    {
        %muzzlePoint = %obj.getMuzzlePoint(0);
        %muzzleVector = %pilot.getEyeVector();
        %point = castRay(%muzzlePoint, %muzzleVector, 300, $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StaticShapeObjectType | $TypeMasks::TurretObjectType | $TypeMasks::ItemObjectType, %obj);

        if(%point)
            %obj.gunshipBeacon.setPosition(%point.hitPos);
        else
            %obj.gunshipBeacon.setPosition(vectorProject(%muzzlePoint, %muzzleVector, 300));

//        %obj.turretR.setTargetObject(%obj.gunshipBeacon);
//        %obj.turretL.setTargetObject(%obj.gunshipBeacon);
    }

    %data.schedule(32, "trackBeaconPos", %obj, %pilot);
}

function Goodship::playerDismounted(%data, %obj, %player)
{
    if(!isObject(%obj.getMountNodeObject(0)) && %obj.gunshipBeacon > 0)
    {
        %obj.gunshipBeacon.delete();
        %obj.gunshipBeacon = 0;

        %obj.mainTurret.setAutoFire(false);
//        %obj.turretR.setImageTrigger(0, false);
//        %obj.turretL.setImageTrigger(0, false);
//        %obj.turretR.setImageAmmo(0, true);
//        %obj.turretL.setImageAmmo(0, true);
    }

   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, true );
}
