//==============================================================================
// JSON Writer Implementation for TS
// Created by Branden Hall (radiantkeen)
// 02-22-15 for Fury:TV Core

if($JSONCount $= "")
    $JSONCount = 0;

function JSON_parse(%string)
{
    %inquote = false;
    %index = 0;
    %max = strlen(%string);
    
    %buf = getSubStr(%string, %index, 1);
}

function JSON_createInstance()
{
    %json = new ScriptObject("JSON"@$JSONCount)
    {
        class = JSON;
//        superClass = JSON;
        items = 0;
    };

    $JSONCount++;

    return %json;
}

//==============================================================================
// JSON Object Methods

$JSONType::Invalid = 0;
$JSONType::Object = 1;
$JSONType::Array = 2;
$JSONType::String = 3;
$JSONType::Number = 4;

function JSON::setType(%this, %type)
{
    %this.type = %type;
}

function JSON::push(%this, %type, %key, %value)
{
    %this.types[%this.items] = %type;
    %this.keys[%this.items] = %key;
    %this.values[%this.items] = %value;
    %this.items++;
}

function JSON::render(%this)
{
    %js = "";
    %je = "";

    if(%this.type == $JSONType::Object)
    {
        %js = "{";
        %je = "}";
    }
    else if(%this.type == $JSONType::Array)
    {
        %js = "[";
        %je = "]";
    }
    
    %string = %js;
    
    for(%i = 0; %i < %this.items; %i++)
    {
        if(%i > 0)
            %string = %string @ ",";

        if(%this.type == $JSONType::Object)
        {
            if(%this.types[%i] == $JSONType::Array || %this.types[%i] == $JSONType::Object)
                %string = %string @ "\"" @ %this.keys[%i] @ "\":" @ %this.values[%i].render();
            else if(%this.types[%i] == $JSONType::String)
                %string = %string @ "\"" @ %this.keys[%i] @ "\":\"" @ %this.values[%i] @ "\"";
            else if(%this.types[%i] == $JSONType::Number)
                %string = %string @ "\"" @ %this.keys[%i] @ "\":" @ %this.values[%i];
        }
        else
        {
            if(%this.types[%i] == $JSONType::Array || %this.types[%i] == $JSONType::Object)
                %string = %string @ %this.values[%i].render();
            else if(%this.types[%i] == $JSONType::String)
                %string = %string @ "\"" @ %this.values[%i] @ "\"";
            else if(%this.types[%i] == $JSONType::Number)
                %string = %string @ %this.values[%i];
        }
    }

    return %string @ %je;
}

function testJSON()
{
    %b = JSON_createInstance();
    %b.setType($JSONType::Array);
    %t = JSON_createInstance();
    %t.setType($JSONType::Object);
    %a = JSON_createInstance();
    %a.setType($JSONType::Array);
    %r = JSON_createInstance();
    %r.setType($JSONType::Array);
    %d = JSON_createInstance();
    %d.setType($JSONType::Object);
    %s = JSON_createInstance();
    %s.setType($JSONType::Object);
    
    %b.push($JSONType::Object, "object1", %t);
    %b.push($JSONType::Object, "object2", %d);
    %b.push($JSONType::Array, "list", %r);
    %b.push($JSONType::String, "whatsthis", "The End");
    
    %t.push($JSONType::String, "key1", "value1");
    %t.push($JSONType::Number, "key2", 2);
    %t.push($JSONType::Object, "subobject", %s);
    
    %s.push($JSONType::String, "lorem", "ipsum");
    %s.push($JSONType::String, "abc", "123");
    %s.push($JSONType::String, "what", "look at this - spacing!");
    
    %r.push($JSONType::String, "", "value1");
    %r.push($JSONType::Array, "", %a);
    %r.push($JSONType::String, "", "value2");
    
    %a.push($JSONType::Number, "", 1);
    %a.push($JSONType::Number, "", 2);
    %a.push($JSONType::Number, "", 3);
    %a.push($JSONType::Number, "", 4);
    %a.push($JSONType::Number, "", 5);
    %a.push($JSONType::Number, "", 6);
    %a.push($JSONType::String, "", "thing 1");
    %a.push($JSONType::String, "", "thing 2");
    %a.push($JSONType::String, "", "thing 3");
    
    echo(%b.render());
}
