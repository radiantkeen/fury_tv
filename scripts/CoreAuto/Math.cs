//-----------------------------------------------------------------------------
// Math Library
//--------------------------------------
$g_PI = 3.1415926;
$g_TAU = 6.283185;

function mClamp(%value, %max)
{
    if(%value > %max)
        return %max;
        
    return %value;
}

function mRound(%value)
{
    return mFloor(%value + 0.5);
}

function mRoundToNearest(%value, %nearest)
{
    %rem = mRound(%value) % %nearest;

    if(%rem == 0)
        return %value;
    else
        return mRound((%value + %nearest / 2) / %nearest) * %nearest;
}

function getRandomT(%rng)
{
    if(%rng)
        return getRandom(1) ? getRandom(%rng) : getRandom(%rng) * -1;
    else
        return getRandom(1) ? getRandom() : getRandom() * -1;
}

function VectorRand()
{
     return (getRandomT() SPC getRandomT() SPC getRandomT());
}

function VectorFromPoints(%posa, %posb)
{
     return VectorNormalize(VectorSub(%posa, %posb));
}

// absolute comparison, for gradual comparison use vectorDot()
function VectorCompare(%v1, %v2)
{
   if(getWord(%v1, 0) == getWord(%v2, 0))
      if(getWord(%v1, 1) == getWord(%v2, 1))
         if(getWord(%v1, 2) == getWord(%v2, 2))
            return true;
         return false;
      return false;
   return false;
}

function getPurgatoryPos()
{
     return getRandomT(10000) SPC getRandomT(10000) SPC "-10000";
}

function comparePositions(%pos1, %pos2)
{
   return mFloor(vectorDot(vectorNormalize(%pos1), vectorNormalize(%pos2)) * 100);
}

function VectorProject(%pos, %vec, %dist)
{
   return vectorAdd(%pos, vectorScale(vectorNormalize(%vec), %dist));
}

function VectorSpread(%vector, %spread)
{
   %euler = VectorScale(VectorRand(), $g_PI * (%spread / 1000));
   %mat = MatrixCreateFromEuler(%euler);

   return MatrixMulVector(%mat, %vector);
}

function VectorToRotation(%vec)
{
    %mat = MatrixCreateFromEuler(vectorScale(%vec, $g_TAU));

    return getWords(%mat, 3, 5) SPC mRadtoDeg(getWord(%mat, 6));
}

function vectorDiv(%one, %two)
{
    return getWord(%one, 0) / getWord(%two, 0) SPC
    getWord(%one, 1) / getWord(%two, 1) SPC
    getWord(%one, 2) / getWord(%two, 2);
}

function VectorToDegrees(%vec)
{
    %x = getWord(%vec, 0) * 360;
    %y = getWord(%vec, 1) * 360;
    %z = getWord(%vec, 2) * 360;

    return %x SPC %y SPC %z;
}

function degreesToRotation(%deg)
{
   %mat = MatrixCreateFromEuler(mDegToRad(getWord(%deg, 0)) SPC mDegToRad(getWord(%deg, 1)) SPC mDegToRad(getWord(%deg, 2)));

   return getWords(%mat, 3, 5) SPC mRadtoDeg(getWord(%mat, 6));
}

// lazy shortcut function
function energyPerSecond(%nrg)
{
    return %nrg / 32;
}

//function by Founder (Martin Hoover)
function getRotationFromPoints(%posOne, %posTwo)
{
   %vec = VectorSub(%posTwo, %posOne);

   // pull the values out of the vector
   %x = firstWord(%vec);
   %y = getWord(%vec, 1);
   %z = getWord(%vec, 2);

   //this finds the distance from origin to our point
   %len = vectorLen(%vec);

   //---------XY-----------------
   //given the rise and length of our vector this will give us the angle in radians
   %rotAngleXY = mATan( %z, %len );

   //---------Z-----------------
   //get the angle for the z axis
   %rotAngleZ = mATan( %x, %y );

   //create 2 matrices, one for the z rotation, the other for the x rotation
   %matrix = MatrixCreateFromEuler("0 0" SPC %rotAngleZ * -1);
   %matrix2 = MatrixCreateFromEuler(%rotAngleXY SPC "0 0");

   //now multiply them together so we end up with the rotation we want
   %finalMat = MatrixMultiply(%matrix, %matrix2);

   //we're done, send the proper numbers back
   //return getWords(%finalMat, 3, 6);
   %rt = getWords(%finalMat, 3, 6);

   return %rt; // Our rotation
}

//function by Founder (Martin Hoover)
function calcThisRot(%thisRot)
{
   %Mrot = MatrixCreateFromEuler(%thisRot);
   %rot = getWord(%Mrot, 3) SPC getWord(%Mrot, 4) SPC getWord(%Mrot, 5) SPC mCeil(mRadtoDeg(getWord(%Mrot, 6)));

   return %rot;
//   echo("calcThisRot:" SPC %thisRot SPC "rotation = \"" @ %rot @ "\"\;");
}

//function by Founder (Martin Hoover)
function calcThisRotD(%this)
{
   %rx = mDegToRad(getWord(%this, 0));
   %ry = mDegToRad(getWord(%this, 1));
   %rz = mDegToRad(getWord(%this, 2));

   %thisRot = %rx SPC %ry SPC %rz;

   %Mrot = MatrixCreateFromEuler(%thisRot);
   %rot = getWord(%Mrot, 3) SPC getWord(%Mrot, 4) SPC getWord(%Mrot, 5) SPC mCeil(mRadtoDeg(getWord(%Mrot, 6)));

   return %rot;
//   echo("calcThisRot:" SPC %thisRot SPC "rotation = \"" @ %rot @ "\"\;");
}

function convertMS(%vel, %type)
{
    switch$(%type)
    {
        case "kph":
            return %vel * 3.6;
            
        case "mph":
            return %vel * 2.23694;
    }
}

function getPosFromRaycast(%raycast)
{
    return getWords(%raycast, 1, 3);
}

function getNormalFromRaycast(%raycast)
{
    return getWords(%raycast, 4, 6);
}

function DecToHex(%num)
{
   %w[1] = mFloor(%num / 16);
   %w[2] = %num - (%w[1] * 16);

   for (%i = 1; %i < 3; %i++)
   {
      if (%w[%i] == 10) %w[%i] = "A";
      if (%w[%i] == 11) %w[%i] = "B";
      if (%w[%i] == 12) %w[%i] = "C";
      if (%w[%i] == 13) %w[%i] = "D";
      if (%w[%i] == 14) %w[%i] = "E";
      if (%w[%i] == 15) %w[%i] = "F";
   }
   
   return %w[1] @ %w[2];
}
