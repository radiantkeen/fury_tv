// Patches contributed by
// Ragora
// calvin_balke

// Remove 250ms limit on GrenadeProjectiles
memPatch("6339D0", "81BB4801000000000000");
memPatch("7ACFC6", "306D732900");
memPatch("6339EF", "C7834801000000000000");

// Auto-close T2 MFC Application if it UEs (for dedicated servers)
if($Host::Dedicated && !$Host::DebugMode)
    memPatch("006FF373", "906A01FF15F8C67D00C3");
    
memPatch("439DE7","85F67412834E1801E88CFFFFFF89F18B196A01FF53048D65F85E5B5DC3");

// Completely disables CRC checking I believe (otherwise known as the "Stop Wine from disconnecting" patch
memPatch("6A3059","EB");

// Enable datablock extension patch
activateDBPatch();

// Packet rate code - remove for now
//$g_PacketRateSet = true;
// make sure to keep the commented out line below commented out
// replace the first 20 in B820000000 in the two mempatches below, and it will change the number of loop iterations

//memPatch("A3A100","5052BA00000000B8200000005150526800000000E8C7D6B4FF5A585981C20100000039C27CE65A58E95F8CB8FF");
//memPatch("A3A200","5052BA00000000B8200000005150526800000000E8C7D5B4FF5A585981C20100000039C27CE65A58E9FC8AB8FF");
//memPatch("5C2D22","E9D97447009090");
//memPatch("5C2D85","E9767347009090");
// TGE waits for all the acks to be sent back before it sends more than 0x1e packets
// so this moves it up to 0x7E
//memPatch("43D72E","7E");
//memPatch("0058665C","9090909090909090");
//memPatch("00586682","90909090909090909090");
//memPatch("005866AB","90909090909090909090");
//memPatch("58781A","EB0C");
