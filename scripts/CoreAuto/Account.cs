//-----------------------------------------------------------------------------
// Account Handler
//--------------------------------------

// Match Flags
$MatchFlags::ValidMatch = 1;

// Player Flags
$PlayerFlags::Active = 1;

function GameConnection::loadAccountData(%client)
{
    if(%client.accountLoaded == true)
        return;

    if(!Net.isStatTracking)
    {
        %client.accountLoaded = true;
        return;
    }

    %conn = Net.createConnection();
    %request = %conn.createHTTPPacket(true, "/furyapi.php");
    %request.setPOSTField("code", $NET_STATUS_PLAYERDATA);
    %request.setPOSTField("authtoken", Net.authToken);
    %request.setPOSTField("guid", %client.guid);
    %request.setPOSTField("name", Base64_Encode(%client.nameBase));
    %request.setPOSTField("end", 1);

    Net.sendPacket(%conn, %request);

    // Send request for player data
//    Net.get("/login.php?playerdata=1&guid="@%client.guid@"&name="@Base64_Encode(%client.nameBase));
}

// [Net.cs->Net::processPacket] onLoadAccount()
function onLoadAccount(%acc)
{
    %guid = getField(%acc, 1);
    %flags = getField(%acc, 2);
    %client = GUIDToClientID(%guid);
    
    if((%flags & $PlayerFlags::Active) == 0)
    {
        %client.setDisconnectReason("You have been globally banned from all Fury: TV servers.");
        %client.disconnect();
        return;
    }

    %client.accountFlags = %flags;
    %client.accountLoaded = true;
    
    // Set default values
    %client.kicked = 0;
    %client.banned = 0;
    %client.score = 0;
    %client.kills = 0;
    %client.deaths = 0;
    %client.assists = 0;
    %client.suicides = 0;
    %client.teamkills = 0;
    %client.pilotkills = 0;
    %client.gunnerkills = 0;
}

function System::processGameOver(%this)
{
    if(!Net.isStatTracking)
        return;
        
    %matcharray = JSON_createInstance();
    %matcharray.setType($JSONType::Array);

    // Get score data here before gameOver() is sent
    for(%x = 0; %x < ClientGroup.getCount(); %x++)
    {
        %cl = ClientGroup.getObject(%x);
        %guid = %cl.isAIControlled() ? 1 : %cl.guid;

        %playerscore = JSON_createInstance();
        %playerscore.setType($JSONType::Object);
        %playerscore.push($JSONType::Number, "guid", %guid);
        %playerscore.push($JSONType::Number, "points", %cl.lastscore);
        %playerscore.push($JSONType::Number, "kills", %cl.lastkills);
        %playerscore.push($JSONType::Number, "deaths", %cl.lastdeaths);
        %playerscore.push($JSONType::Number, "assists", %cl.lastassists);
        %playerscore.push($JSONType::Number, "suicides", %cl.lastsuicides);
        %playerscore.push($JSONType::Number, "teamkills", %cl.lastteamkills);
        %playerscore.push($JSONType::Number, "pilotkills", %cl.lastpilotkills);
        %playerscore.push($JSONType::Number, "gunnerkills", %cl.lastgunnerkills);
        %playerscore.push($JSONType::String, "alias", %cl.namebase);
        
        %matcharray.push($JSONType::Object, "", %playerscore);
            
        // reset F2 menu on mission end
        %cl.scoreHudMenuState = $MenuState::Default;
        %cl.scoreHudMenu = $Menu::Main;
        %cl.looseScore = 0;
        
        // Clear and reset scores for next match
        %cl.score = 0;
        %cl.kills = 0;
        %cl.deaths = 0;
        %cl.assists = 0;
        %cl.suicides = 0;
        %cl.teamkills = 0;
        %cl.pilotkills = 0;
        %cl.gunnerkills = 0;
    }

    %matchdata = %matcharray.render();
    
    %conn = Net.createConnection();
    %request = %conn.createHTTPPacket(true, "/furyapi.php");
    %request.setPOSTField("code", $NET_STATUS_MATCHDATA);
    %request.setPOSTField("authtoken", Net.authToken);
    %request.setPOSTField("matchflags", $MatchFlags::ValidMatch);
    %request.setPOSTField("mapname", $SLastMission);
    %request.setPOSTField("gametype", $SLastMissionType);
    %request.setPOSTField("matchdata", %matchdata);
    %request.setPOSTField("end", 1);

    Net.sendPacket(%conn, %request);
}
