// Fury: Terminal Velocity
// Custom functions required for gameplay
$Pref::useImmersion = "1";

if(!isObject(FuryClient))
{
   new ScriptObject(FuryClient)
   {
      class = FuryClient;
      init = false;
      packetRateSet = false;
      version = 12;
   };
}

// Count to 3
if(FuryClient.init != true)
{
    FuryClient.init = true;

    schedule(3032, 0, "setMinimumPacketRate");
    schedule(3000, 0, "exec", "scripts/autoexec/FuryClient.cs"); // self exec here because for SOME reason the functions don't keep, maybe being overwritten?
    schedule(3000, 0, "exec", "gui/VehicleEditDlg.gui");
    
    $Pref::useImmersion = "1";                // Used for C++ interface
    loadMod("TSExtension");
}

// Fury Client Hooks
//-----------------------------------------------------------------------------
function clientCmdRegisterFuryClient()
{
    commandToServer('FuryClientAck', FuryClient.version);
}

function clientCmdClientEchoText(%text)
{
   echo(%text);
}

function clientCmdClientWarnText(%text)
{
   warn(%text);
}

function clientCmdClientErrorText(%text)
{
   error(%text);
}

function clientCmdCreateMessageBoxOK(%title, %text)
{
   MessageBoxOK(%title, %text);
}

// Connecting to server override
//-----------------------------------------------------------------------------
function JoinSelectedGame()
{
	$ServerInfo = GMJ_Browser.getServerInfoString();
 
    %modtype = getRecord($ServerInfo, 2);
    
    if(%modtype $= "FuryTV")
    {
        error("Applying Fury: Terminal Velocity customizations.");
        activateDBPatch();
        
        exec("scripts/FuryClientScripts.cs");
    }

	JoinGame($JoinGameAddress);
}

// Fury timing and network speed improvements
//-----------------------------------------------------------------------------
function setMinimumPacketRate()
{
    if($pref::Net::PacketRateToClient < 32)
        $pref::Net::PacketRateToClient = 32;

    if($pref::Net::PacketRateToServer < 32)
        $pref::Net::PacketRateToServer = 32;
        
    if($pref::Net::PacketSize < 450)
        $pref::Net::PacketSize = 450;
}
