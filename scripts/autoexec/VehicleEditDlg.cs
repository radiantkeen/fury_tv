//---------------------------------------------------------------------------------------------------------
//  VehicleEditDlg.cs
//  Main script file implementing the GUI logic and client API for the vehicle edit dialog, built for use
//  in Keen's SolConflict and Fury-TV modifications.
//
//  Copyright (c) 2016 Robert MacGregor
//  This software is licensed under the MIT license. Refer to LICENSE.txt for more information.
//---------------------------------------------------------------------------------------------------------

// Ensure that these are all 0-values, otherwise we end up using "" as keys.
if ($VehicleEdit::Vehicles::Count $= "")
    $VehicleEdit::Vehicles::Count = 0;
if ($VehicleEdit::Weapons::Count $= "")
    $VehicleEdit::Weapons::Count = 0;
if ($VehicleEdit::Modules::TypeCount $= "")
    $VehicleEdit::Modules::TypeCount = 0;

if (isFile("VehicleEditPresets.cs"))
    exec("vehicleEditPresets.cs");

function mVariate(%min, %max, %base, %val)
{
    if (%val == %base)
        return 0.5;
    else if (%val > %base)
        return (%val - %base) / (%max - %base) * 0.5 + 0.5;
    else if (%val < %base)
        return (%val - %min) / (%base - %min) * 0.5;
}

function ShellPopupMenu::getIDByText(%this, %text)
{
    for (%index = 0; %index < 100; %index++)
    {
        %indexText = %this.getTextByID(%index);
        if (%indexText $= %text)
            return %index;
        else if (%indexText $="")
            return -1;
    }

    return -1;
}

//---------------------------------------------------------------------------------------------------------
//  Description: Registers a new vehicle to the vehicle edit dialog.
//  Parameters:
//      %serverID - A server relevant identification number for this vehicle.
//      %name - The name of the vehicle.
//      %vehicleMask - The bitmask representing this vehicle. It is used for module compatibility testing.
//      %baseWeight - The weight of the vehicle with no alterations.
//      %baseArmor - The armor of the vehicle with no alterations.
//      %baseShield - The shield strength of the vehicle with no alterations.
//      %baseRegen - The energy regeneration rate of the vehicle with no alterations.
//      %shapeName - The shapename of the vehicle. This is displayed in a 3D preview on the client end.
//      %description - The description for the vehicle that is displayed on the HUD.
//      %vehicleCompatMask - If specified, this represents a bitmask that is used to do a straight comparison check for compatible weapons and modules.
//      Note that this parameter has exactly the same expected value as the shapefile parameter on a
//      datablock, except that the .dts extension must be omitted.
//---------------------------------------------------------------------------------------------------------
function clientCmdRegisterVehicle(%serverID, %name, %vehicleMask, %baseWeight, %baseArmor, %baseShield, %baseRegen, %shapename, %description)
{
    %name = strReplace(%name, " ", "_");

    $VehicleEdit::Vehicles::Name[$VehicleEdit::Vehicles::Count] = %name;
    $VehicleEdit::Vehicles::BaseArmor[$VehicleEdit::Vehicles::Count] = %baseArmor;
    $VehicleEdit::Vehicles::BaseShield[$VehicleEdit::Vehicles::Count] = %baseShield;
    $VehicleEdit::Vehicles::BaseRegen[$VehicleEdit::Vehicles::Count] = %baseRegen;
    $VehicleEdit::Vehicles::BaseWeight[$VehicleEdit::Vehicles::Count] = %baseWeight;
    $VehicleEdit::Vehicles::ShapeName[$VehicleEdit::Vehicles::Count] = %shapename;
    $VehicleEdit::Vehicles::Mask[$VehicleEdit::Vehicles::Count] = %vehicleMask;
    $VehicleEdit::Vehicles::ServerID[$VehicleEdit::Vehicles::Count] = %serverID;
    $VehicleEdit::Vehicles::Description[$VehicleEdit::Vehicles::Count] = %description;
    $VehicleEdit::Vehicles::NameToID[%name] = $VehicleEdit::Vehicles::Count;

    $VehicleEdit::Vehicles::Count++;
}

//---------------------------------------------------------------------------------------------------------
//  Description: Registers a weapon slot with the given compatibility mask to the last registered vehicle.
//  Parameters:
//      %mask - The bitmask representing
//---------------------------------------------------------------------------------------------------------
function clientCmdRegisterSlot(%typeMask, %slotSize, %name)
{
    %id = $VehicleEdit::Vehicles::Count - 1;

    if ($VehicleEdit::Vehicles::Slots::Count[%id] $= "")
        $VehicleEdit::Vehicles::Slots::Count[%id] = 0;

    $VehicleEdit::Vehicles::Slots::Name[%id, $VehicleEdit::Vehicles::Slots::Count[%id]] = %name;
    $VehicleEdit::Vehicles::Slots::TypeMask[%id, $VehicleEdit::Vehicles::Slots::Count[%id]] = %typeMask;
    $VehicleEdit::Vehicles::Slots::SlotSize[%id, $VehicleEdit::Vehicles::Slots::Count[%id]] = %slotSize;
    $VehicleEdit::Vehicles::Slots::Count[%id]++;
}

function clientCmdRegisterModule(%serverID, %name, %type, %vehicleMask, %relWeight, %relArmor, %relShield, %relRegen, %description)
{
    if ($VehicleEdit::Modules::Count[%type] $= "")
        $VehicleEdit::Modules::Count[%type] = 0;
    if ($VehicleEdit::Modules::TypeCount $= "")
        $VehicleEdit::Modules::TypeCount = 0;

    %id = $VehicleEdit::Modules::Count[%type];

    if ($VehicleEdit::Modules::TypeToID[%type] $= "")
    {
        $VehicleEdit::Modules::TypeToID[%type] = $VehicleEdit::Modules::TypeCount;
        $VehicleEdit::Modules::Types[$VehicleEdit::Modules::TypeCount] = %type;

        $VehicleEdit::Modules::TypeCount++;
    }

    $VehicleEdit::Modules::Name[%type, %id] = %name;
    $VehicleEdit::Modules::RelativeArmor[%type, %id] = %relArmor;
    $VehicleEdit::Modules::RelativeShield[%type, %id] = %relShield;
    $VehicleEdit::Modules::RelativeRegen[%type, %id] = %relRegen;
    $VehicleEdit::Modules::RelativeWeight[%type, %id] = %relWeight;
    $VehicleEdit::Modules::VehicleMask[%type, %id] = %vehicleMask;
    $VehicleEdit::Modules::ServerID[%type, %id] = %serverID;
    $VehicleEdit::Modules::Description[%type, %id] = %description;

    // Ensure the default count is 0.
    if ($VehicleEdit::Modules::NameToID::Count[%type, %name] $= "")
        $VehicleEdit::Modules::NameToID::Count[%type, %name] = 0;

    %currentCount = $VehicleEdit::Modules::NameToID::Count[%type, %name];
    $VehicleEdit::Modules::NameToID[%type, %name, %currentCount] = $VehicleEdit::Modules::Count[%type];
    $VehicleEdit::Modules::NameToID::Count[%type, %name]++;

    $VehicleEdit::Modules::Count[%type]++;
}

function clientCmdRegisterWeapon(%serverID, %name, %typeMask, %slotSize, %vehicleCompatibilityMask)
{
    $VehicleEdit::Weapons::ServerID[$VehicleEdit::Weapons::Count] = %serverID;
    $VehicleEdit::Weapons::Name[$VehicleEdit::Weapons::Count] = %name;
    $VehicleEdit::Weapons::TypeMask[$VehicleEdit::Weapons::Count] = %typeMask;
    $VehicleEdit::Weapons::SlotSize[$VehicleEdit::Weapons::Count] = %slotSize;
    $VehicleEdit::Weapons::VehicleCompatibilityMask[$VehicleEdit::Weapons::Count] = %vehicleCompatibilityMask;

    // Count weapons by name.
    if ($VehicleEdit::Weapons::Name::Count[%name] $= "")
        $VehicleEdit::Weapons::Name::Count[%name] = 0;

    %currentCount = $VehicleEdit::Weapons::Name::Count[%name];
    $VehicleEdit::Weapons::NameToID[%name, %currentCount] = $VehicleEdit::Weapons::Count;

    $VehicleEdit::Weapons::Count++;
    $VehicleEdit::Weapons::Name::Count[%name]++;
}

function clientCmdRegisterWeaponText(%text)
{
    $VehicleEdit::Weapons::WeaponTexts[$VehicleEdit::Weapons::Count - 1] = %text;
}

function VehicleEdit::lookupWeaponID(%weaponName, %weaponSlot, %vehicleID)
{
    for (%iteration = 0; %iteration < $VehicleEdit::Weapons::Name::Count[%weaponName]; %iteration++)
    {
        // Size and type must be good
        %typeTest = $VehicleEdit::Vehicles::Slots::TypeMask[%vehicleID, %weaponSlot] & $VehicleEdit::Weapons::TypeMask[$VehicleEdit::Weapons::NameToID[%weaponName, %iteration]];
        %sizeTest = $VehicleEdit::Vehicles::Slots::SlotSize[%vehicleID, %weaponSlot] == $VehicleEdit::Weapons::SlotSize[$VehicleEdit::Weapons::NameToID[%weaponName, %iteration]];

        if (%typeTest && %sizeTest)
            return $VehicleEdit::Weapons::NameToID[%weaponName, %iteration];
    }

    return -1;
}

function VehicleEdit::lookupModuleID(%type, %name, %vehicleID)
{
    for (%iteration = 0; %iteration < $VehicleEdit::Modules::NameToID::Count[%type, %name]; %iteration++)
    {
        // Verify if this is compatible.
        %currentModuleID = $VehicleEdit::Modules::NameToID[%type, %name, %iteration];
        if ($VehicleEdit::Modules::VehicleMask[%type, %currentModuleID] & $VehicleEdit::Vehicles::Mask[%vehicleID])
            return %currentModuleID;
    }

    return -1;
}

function VehicleEdit::resetInternalDatabase()
{
    // Reset vehicles.
    for (%vehicleID = 0; %vehicleID < $VehicleEdit::Vehicles::Count; %vehicleID++)
    {
        $VehicleEdit::Vehicles::CompatibleModules[%vehicleID] = "";
        $VehicleEdit::Vehicles::CompatibleWeapons[%vehicleID] = "";

        $VehicleEdit::Vehicles::MaximumArmor[%vehicleID] = $VehicleEdit::Vehicles::MinimumArmor[%vehicleID]
        = $VehicleEdit::Vehicles::BaseArmor[%vehicleID] = $VehicleEdit::Vehicles::MaximumShield[%vehicleID] =
        $VehicleEdit::Vehicles::MinimumShield[%vehicleID] = $VehicleEdit::Vehicles::BaseShield[%vehicleID] =
        $VehicleEdit::Vehicles::MaximumRegen[%vehicleID] = $VehicleEdit::Vehicles::MinimumRegen[%vehicleID] =
        $VehicleEdit::Vehicles::BaseRegen[%vehicleID] = $VehicleEdit::Vehicles::MaximumWeight[%vehicleID] =
        $VehicleEdit::Vehicles::MinimumWeight[%vehicleID] = $VehicleEdit::Vehicles::BaseWeight[%vehicleID] =
        $VehicleEdit::Vehicles::Description[%vehicleID] = $VehicleEdit::Vehicles::CompatibilityMask[%vehicleID] =
        $VehicleEdit::Vehicles::Name[%vehicleID] = "";

        for (%slotID = 0; %slotID < $VehicleEdit::Vehicles::Slots::Count[%vehicleID]; %slotID++)
            $VehicleEdit::Vehicles::Slots::SlotSize[%vehicleID, %slotID] = $VehicleEdit::Vehicles::Slots::TypeMask[%vehicleID, %slotID] = "";

        $VehicleEdit::Vehicles::Slots::Count[%vehicleID] = 0;
    }

    // Reset weapons.
    for (%weaponID = 0; %weaponID < $VehicleEdit::Weapons::Count; %weaponID++)
    {
        $VehicleEdit::Weapons::SlotSize[%weaponID] = $VehicleEdit::Weapons::TypeMask[%weaponID] = "";
        $VehicleEdit::Weapons::Name::Count[$VehicleEdit::Weapons::Name[%weaponID]] = 0;
    }

    // Reset modules.
    for (%moduleTypeID = 0; %moduleTypeID < $VehicleEdit::Modules::TypeCount; %moduleTypeID++)
    {
        %typeName = $VehicleEdit::Modules::Types[%moduleTypeID];
        %moduleName = $VehicleEdit::Modules::Name[%typeName, %moduleID];

        $VehicleEdit::Modules::MinimumWeight[%typeName] = $VehicleEdit::Modules::MaximumWeight[%typeName] =
        $VehicleEdit::Modules::MaximumShield[%typeName] = $VehicleEdit::Modules::MinimumShield[%typeName] =
        $VehicleEdit::Modules::MaximumArmor[%typeName] = $VehicleEdit::Modules::MinimumArmor[%typeName] =
        $VehicleEdit::Modules::MaximumRegen[%typeName] = $VehicleEdit::Modules::MinimumRegen[%typeName] = "";

        for (%moduleID = 0; %moduleID < $VehicleEdit::Modules::Count[%typeName]; %moduleID++)
        {
            $VehicleEdit::Modules::Name[%typeName, %moduleID] = $VehicleEdit::RelativeRegen[%typeName, %moduleID] =
            $VehicleEdit::Modules::RelativeArmor[%typeName, %moduleID] = $VehicleEdit::Modules::RelativeWeight[%typeName, %moduleID] =
            $VehicleEdit::Modules::VehicleCompatibilityMask[%typeName, %moduleID] = $VehicleEdit::Modules::RelativeShield[%typeName, %moduleID] = "";
        }

        %currentModuleCount = $VehicleEdit::Modules::NameToID::Count[%typeName, %moduleName];
        for (%subModuleID = 0; %subModuleID < %currentModuleCount; %subModuleID++)
            $VehicleEdit::Modules::NameToID[%typeName, %moduleName, %subModuleID] = 0;

        $VehicleEdit::Modules::Count[%typeName] = $VehicleEdit::Modules::NameToID::Count[%typeName, %moduleName] = 0;
        $VehicleEdit::Modules::TypeToID[%typeName] = $VehicleEdit::Modules::Types[%moduleTypeID] = "";
    }

    $VehicleEdit::Modules::TypeCount = 0;
    $VehicleEdit::Vehicles::Count = 0;
    $VehicleEdit::Weapons::Count = 0;
}

function clientCmdEndVehicleEditData()
{
    // FIXME: This is a potential server-sided DoS. Really shitty one, though.

    // We loop over vehicles and foreach compatible module in a given type, we figure the best and worst influences
    for (%vehicleID = 0; %vehicleID < $VehicleEdit::Vehicles::Count; %vehicleID++)
    {
        $VehicleEdit::Vehicles::CompatibleModules[%vehicleID] = "";
        $VehicleEdit::Vehicles::CompatibleWeapons[%vehicleID] = "";

        $VehicleEdit::Vehicles::MaximumArmor[%vehicleID] = $VehicleEdit::Vehicles::MinimumArmor[%vehicleID]
        = $VehicleEdit::Vehicles::BaseArmor[%vehicleID];

        $VehicleEdit::Vehicles::MaximumShield[%vehicleID] = $VehicleEdit::Vehicles::MinimumShield[%vehicleID]
        = $VehicleEdit::Vehicles::BaseShield[%vehicleID];

        $VehicleEdit::Vehicles::MaximumRegen[%vehicleID] = $VehicleEdit::Vehicles::MinimumRegen[%vehicleID]
        = $VehicleEdit::Vehicles::BaseRegen[%vehicleID];

        $VehicleEdit::Vehicles::MaximumWeight[%vehicleID] = $VehicleEdit::Vehicles::MinimumWeight[%vehicleID]
        = $VehicleEdit::Vehicles::BaseWeight[%vehicleID];

        %baseArmor = $VehicleEdit::Vehicles::BaseArmor[%vehicleID];
        %baseRegen = $VehicleEdit::Vehicles::BaseRegen[%vehicleID];
        %baseShield = $VehicleEdit::Vehicles::BaseShield[%vehicleID];
        %baseWeight = $VehicleEdit::Vehicles::BaseWeight[%vehicleID];

        // Process foreach weapon slot and find compatible weapons
        for (%slotID = 0; %slotID < $VehicleEdit::Vehicles::Slots::Count[%vehicleID]; %slotID++)
        {
            // Check each weapon against this slot
            for (%weaponID = 0; %weaponID < $VehicleEdit::Weapons::Count; %weaponID++)
                if ($VehicleEdit::Weapons::SlotSize[%weaponID] == $VehicleEdit::Vehicles::Slots::SlotSize[%vehicleID, %slotID] &&
                $VehicleEdit::Weapons::TypeMask[%weaponID] & $VehicleEdit::Vehicles::Slots::TypeMask[%vehicleID, %slotID])
                    $VehicleEdit::Vehicles::CompatibleWeapons[%vehicleID, %slotID] = $VehicleEdit::Vehicles::CompatibleWeapons[%vehicleID, %slotID] SPC %weaponID;

            $VehicleEdit::Vehicles::CompatibleWeapons[%vehicleID, %slotID] = trim($VehicleEdit::Vehicles::CompatibleWeapons[%vehicleID, %slotID]);
        }

        // Process minimum and maximum values on a per module-type basis as well as build compatible lists
        for (%moduleTypeID = 0; %moduleTypeID < $VehicleEdit::Modules::TypeCount; %moduleTypeID++)
        {
            %typeName = $VehicleEdit::Modules::Types[%moduleTypeID];

            // Now loop foreach module in this type
            for (%moduleID = 0; %moduleID < $VehicleEdit::Modules::Count[%typeName]; %moduleID++)
            {
                if ($VehicleEdit::Modules::VehicleMask[%typeName, %moduleID] & $VehicleEdit::Vehicles::Mask[%vehicleID])
                {
                    $VehicleEdit::Vehicles::CompatibleModules[%vehicleID, %typeName] = $VehicleEdit::Vehicles::CompatibleModules[%vehicleID, %typeName] SPC %moduleID;

                    %relArmor = $VehicleEdit::Modules::RelativeArmor[%typeName, %moduleID];
                    %newArmor = %baseArmor + %relArmor;

                    %relShield = $VehicleEdit::Modules::RelativeShield[%typeName, %moduleID];
                    %newShield = %baseShield + %relShield;

                    %relRegen = $VehicleEdit::Modules::RelativeRegen[%typeName, %moduleID];
                    %newRegen = %baseRegen + %relRegen;

                    %relWeight = $VehicleEdit::Modules::RelativeWeight[%typeName, %moduleID];
                    %newWeight = %baseWeight + %relWeight;

                    // Now grab the minimum and maximums
                    if (%newWeight > $VehicleEdit::Vehicles::MaximumWeight[%vehicleID])
                    {
                        $VehicleEdit::Vehicles::MaximumWeight[%vehicleID, %typeName] = %newWeight;
                        $VehicleEdit::Vehicles::MaximumRelativeWeight[%vehicleID, %typeName] = %relWeight;
                    }
                    else if (%newWeight < $VehicleEdit::Vehicles::MinimumWeight[%vehicleID])
                    {
                        $VehicleEdit::Vehicles::MinimumWeight[%vehicleID, %typeName] = %newWeight;
                        $VehicleEdit::Vehicles::MinimumRelativeWeight[%vehicleID, %typeName] = %relWeight;
                    }

                    if (%newShield > $VehicleEdit::Vehicles::MaximumShield[%vehicleID])
                    {
                        $VehicleEdit::Vehicles::MaximumShield[%vehicleID, %typeName] = %newShield;
                        $VehicleEdit::Vehicles::MaximumRelativeShield[%vehicleID, %typeName] = %relShield;
                    }
                    else if (%newWeight < $VehicleEdit::Vehicles::MinimumShield[%vehicleID])
                    {
                        $VehicleEdit::Vehicles::MinimumShield[%vehicleID, %typeName] = %newShield;
                        $VehicleEdit::Vehicles::MinimumRelativeShield[%vehicleID, %typeName] = %relShield;
                    }

                    if (%newRegen > $VehicleEdit::Vehicles::MaximumRegen[%vehicleID])
                    {
                        $VehicleEdit::Vehicles::MaximumRegen[%vehicleID, %typeName] = %newRegen;
                        $VehicleEdit::Vehicles::MaximumRelativeRegen[%vehicleID, %typeName] = %relRegen;
                    }
                    else if (%newRegen < $VehicleEdit::Vehicles::MinimumRegen[%vehicleID])
                    {
                        $VehicleEdit::Vehicles::MinimumRegen[%vehicleID, %typeName] = %newRegen;
                        $VehicleEdit::Vehicles::MinimumRelativeRegen[%vehicleID, %typeName] = %relRegen;
                    }

                    if (%newArmor > $VehicleEdit::Vehicles::MaximumArmor[%vehicleID])
                    {
                        $VehicleEdit::Vehicles::MaximumArmor[%vehicleID, %typeName] = %newArmor;
                        $VehicleEdit::Vehicles::MaximumRelativeArmor[%vehicleID, %typeName] = %relArmor;
                    }
                    else if (%newArmor < $VehicleEdit::Vehicles::MinimumArmor[%vehicleID])
                    {
                        $VehicleEdit::Vehicles::MinimumArmor[%vehicleID, %typeName] = %newArmor;
                        $VehicleEdit::Vehicles::MinimumRelativeArmor[%vehicleID, %typeName] = %relArmor;
                    }
                }
            }

            $VehicleEdit::Vehicles::CompatibleModules[%vehicleID, %typeName] = trim($VehicleEdit::Vehicles::CompatibleModules[%vehicleID, %typeName]);
        }

        // Figure out what the overall minimum and maximum values are for the vehicle

        // Technically isn't necessary the way TS works
        %currentMinRelArmor = %currentMaxRelArmor = %currentMinRelShield = %currentMaxRelShield = %currentMinRelRegen = %currentMaxRelRegen
        = %currentMinRelWeight = %currentMaxRelWeight = 0;

        for (%moduleTypeID = 0; %moduleTypeID < $VehicleEdit::Modules::TypeCount; %moduleTypeID++)
        {
            %typeName = $VehicleEdit::Modules::Types[%moduleTypeID];

            %currentMinRelArmor += $VehicleEdit::Vehicles::MinimumRelativeArmor[%vehicleID, %typeName];
            %currentMaxRelArmor += $VehicleEdit::Vehicles::MaximumRelativeArmor[%vehicleID, %typeName];

            %currentMinRelRegen += $VehicleEdit::Vehicles::MinimumRelativeRegen[%vehicleID, %typeName];
            %currentMaxRelRegen += $VehicleEdit::Vehicles::MaximumRelativeRegen[%vehicleID, %typeName];

            %currentMinRelShield += $VehicleEdit::Vehicles::MinimumRelativeShield[%vehicleID, %typeName];
            %currentMaxRelShield += $VehicleEdit::Vehicles::MaximumRelativeShield[%vehicleID, %typeName];

            %currentMinRelWeight += $VehicleEdit::Vehicles::MinimumRelativeWeight[%vehicleID, %typeName];
            %currentMaxRelWeight += $VehicleEdit::Vehicles::MaximumRelativeWeight[%vehicleID, %typeName];
        }

        // After all this, assign the overall minimums and maximums
        $VehicleEdit::Vehicles::MaximumArmor[%vehicleID] = %baseArmor + %currentMaxRelArmor;
        $VehicleEdit::Vehicles::MinimumArmor[%vehicleID] = %baseArmor + %currentMinRelArmor;

        $VehicleEdit::Vehicles::MaximumRegen[%vehicleID] = %baseRegen + %currentMaxRelRegen;
        $VehicleEdit::Vehicles::MinimumRegen[%vehicleID] = %baseRegen + %currentMinRelRegen;

        $VehicleEdit::Vehicles::MaximumWeight[%vehicleID] = %baseWeight + %currentMaxRelWeight;
        $VehicleEdit::Vehicles::MinimumWeight[%vehicleID] = %baseWeight + %currentMinRelWeight;

        $VehicleEdit::Vehicles::MaximumShield[%vehicleID] = %baseShield + %currentMaxRelShield;
        $VehicleEdit::Vehicles::MinimumShield[%vehicleID] = %baseShield + %currentMinRelShield;
    }

    // Now foreach vehicle we set preset defaults
    for (%vehicleID = 0; %vehicleID < $VehicleEdit::Vehicles::Count; %vehicleID++)
        for (%presetID = 1; %presetID < 6; %presetID++)
            if ($VehicleEdit::Presets::Name[%vehicleID, %presetID] $= "")
                $VehicleEdit::Presets::Name[%vehicleID, %presetID] = "Preset " @ %presetID;
}

function VehicleEdit::checkPresetsActive()
{
    %active = $VehicleEdit::WeaponSelectionActive || $VehicleEdit::ModuleSelectionActive;

    VehiclePreset1.setActive(%active);
    VehiclePreset2.setActive(%active);
    VehiclePreset3.setActive(%active);
    VehiclePreset4.setActive(%active);
    VehiclePreset5.setActive(%active);

    VehicleEditPresetName.setActive(%active);
}

function VehicleEdit::onTabSelect(%id)
{
    %selected = nameToID("VehiclePreset" @ %id);

    for (%iteration = 1; %iteration < 6; %iteration++)
    {
        %toggled = nameToID("VehiclePreset" @ %iteration);
        %toggled.setValue(false);
    }

    %selected.setValue(true);

    $VehicleEdit::SelectedPreset[$VehicleEdit::CurrentVehicleSelection] = %id;

    %vehicleID = $VehicleEdit::CurrentVehicleSelection;
    %vehicleName = $VehicleEdit::Vehicles::Name[%vehicleID];

    // Set the preset name text
    VehicleEditPresetName.setText($VehicleEdit::Presets::Name[%vehicleID, %id]);

    // Set default preset values and load them
    for (%weaponSlot = 0; %weaponSlot < $VehicleEdit::Vehicles::Slots::Count[%vehicleID]; %weaponSlot++)
    {
        if ($VehicleEdit::Presets::Weapon[%vehicleName, %id, %weaponSlot] $= "")
            $VehicleEdit::Presets::Weapon[%vehicleName, %id, %weaponSlot] = $VehicleEdit::Weapons::Name[getWord($VehicleEdit::Vehicles::CompatibleWeapons[%vehicleID, %weaponSlot], 0)];

        $VehicleEdit::CurrentSelectedFiregroup[%weaponSlot] = $VehicleEdit::Presets::Firegroup[%vehicleName, %id, %weaponSlot];
        $VehicleEdit::CurrentWeaponSelection[%weaponSlot] = VehicleEdit::lookupWeaponID($VehicleEdit::Presets::Weapon[%vehicleName, %id, %weaponSlot], %weaponSlot, %vehicleID);
    }

    for (%moduleTypeID = 0; %moduleTypeID < $VehicleEdit::Modules::TypeCount; %moduleTypeID++)
    {
        %typeName = $VehicleEdit::Modules::Types[%moduleTypeID];

        if ($VehicleEdit::Presets::Module[%vehicleName, %id, %typeName] $= "")
            $VehicleEdit::Presets::Module[%vehicleName, %id, %typeName] = $VehicleEdit::Modules::Name[%typeName, getWord($VehicleEdit::Vehicles::CompatibleModules[%vehicleID, %typeName], 0)];

        $VehicleEdit::CurrentModuleSelection[%typeName] = VehicleEdit::lookupModuleID(%typeName, $VehicleEdit::Presets::Module[%vehicleName, %id, %typeName], $VehicleEdit::CurrentVehicleSelection);
    }

    VehicleEdit::ModuleTypeChanged();
    VehicleEdit::ModuleSlotChanged();
    VehicleEdit::WeaponSlotChanged();
}

function VehicleEdit::setWeaponSelectionActive(%active)
{
    VehicleSlotSelection.setActive(%active);

    if (!%active)
        VehicleWeaponSelection.setText("");
    VehicleWeaponSelection.setActive(%active);

    WeaponSelectionText.setVisible(%active);

    if (!%active)
        VehicleSlotSelection.setValue("Not Applicable");

    $VehicleEdit::WeaponSelectionActive = %active;
    VehicleEdit::checkPresetsActive();
}

function VehicleEdit::setModuleSelectionActive(%active)
{
    if (!%active)
        VehicleModuleSelection.setText("");
    if (!%active)
        VehicleModuleType.setText("");

    VehicleModuleSelection.setActive(%active);
    VehicleModuleType.setActive(%active);

    VehicleModuleArmor.setActive(%active);
    VehicleModuleShield.setActive(%active);
    VehicleModuleEnergy.setActive(%active);
    VehicleModuleWeight.setActive(%active);
    VehicleModuleArmorText.setVisible(%active);
    VehicleModuleShieldText.setVisible(%active);
    VehicleModuleEnergyText.setVisible(%active);
    VehicleModuleWeightText.setVisible(%active);

    if (%active)
        ModuleTypeText.setText("Module Type");
    else
        ModuleTypeText.setText("Not Applicable");

    $VehicleEdit::ModuleSelectionActive = %active;
    VehicleEdit::checkPresetsActive();
}

function VehicleEdit::WeaponChanged()
{
    %weaponSlot = VehicleSlotSelection.getSelected();
    %id = VehicleEdit::lookupWeaponID(VehicleWeaponSelection.getValue(), %weaponSlot, $VehicleEdit::CurrentVehicleSelection);

    WeaponSelectionText.setText($VehicleEdit::Weapons::WeaponTexts[%id]);

    // Update the current selection
    $VehicleEdit::CurrentWeaponSelection[%weaponSlot] = %id;

    // Save the new weapon selection.
    VehicleEdit::Save();
}

function VehicleEdit::PresetNameChanged()
{
    VehicleEdit::Save();
}

function VehicleEdit::VehicleChanged()
{
    %name = VehicleDropdown.getValue();

    %name = strReplace(%name, " ", "_");
    %id = $VehicleEdit::Vehicles::NameToID[%name];
    $VehicleEdit::CurrentVehicleSelection = $VehicleEdit::Vehicles::NameToID[%name];

    if ($VehicleEdit::SelectedPreset[%id] $= "")
        $VehicleEdit::SelectedPreset[%id] = 1;

    VehicleEdit::onTabSelect($VehicleEdit::SelectedPreset[%id]);

    // Update slider values
    VehicleEdit::CurrentVehicleArmorChanged();
    VehicleEdit::CurrentVehicleShieldChanged();
    VehicleEdit::CurrentVehicleRegenChanged();
    VehicleEdit::CurrentVehicleWeightChanged();

    // Rename all of the tabs
    for (%presetID = 1; %presetID < 6; %presetID++)
    {
        %tab = nameToID("VehiclePreset" @ %presetID);
        %tab.setText($VehicleEdit::Presets::Name[%id, %presetID]);
    }

    // Update the slot slider
    if ($VehicleEdit::Vehicles::Slots::Count[%id] == 0)
        VehicleEdit::setWeaponSelectionActive(false);
    else
    {
        VehicleEdit::setWeaponSelectionActive(true);

        VehicleEdit::WeaponSlotChanged();
        %slotCount = $VehicleEdit::Vehicles::Slots::Count[%id];

        // Populate the dropdown with values
        VehicleSlotSelection.clear();
        if (%slotCount <= 0)
        {
            VehicleSlotSelection.setActive(false);
            VehicleSlotSelection.setText("No Slots Available");
        }
        else
        {
            VehicleSlotSelection.setActive(true);

            for (%iteration = 0; %iteration < %slotCount; %iteration++)
            {
                %slotName = $VehicleEdit::Vehicles::Slots::Name[%id, %iteration];
                %slotName = %slotName !$= "" ? %slotName : "Weapon Slot " @ (%iteration + 1);
                VehicleSlotSelection.add(%slotName, %iteration);
            }
            VehicleSlotSelection.setSelected(0);
        }
    }

    // Sync the preview model
    VehiclePreview.setModel($VehicleEdit::Vehicles::ShapeName[$VehicleEdit::CurrentVehicleSelection], "");

    // Populate the module types
    VehicleModuleType.clear();

    %moduleTypeCount = 0;
    for (%iteration = 0; %iteration < $VehicleEdit::Modules::TypeCount; %iteration++)
    {
        %type = $VehicleEdit::Modules::Types[%iteration];

        // And then for each type we check to see if there's any compatible Modules
        for (%moduleID = 0; %moduleID < $VehicleEdit::Modules::Count[%type]; %moduleID++)
        {
            if ($VehicleEdit::Modules::VehicleMask[%type, %moduleID] & $VehicleEdit::Vehicles::Mask[%id])
            {
                VehicleModuleType.add(%type, %moduleTypeCount);
                %moduleTypeCount++;
                break;
            }
        }
    }

    if (%moduleTypeCount == 0)
        VehicleEdit::setModuleSelectionActive(false);
    else
    {
        VehicleEdit::setModuleSelectionActive(true);

        VehicleModuleType.setSelected(0);
        VehicleEdit::ModuleTypeChanged();
        VehicleEdit::ModuleSlotChanged();
    }
}

function VehicleEdit::CurrentVehicleArmorChanged()
{
    VehicleCurrentArmor.range = "0" SPC $VehicleEdit::Vehicles::BaseArmor[$VehicleEdit::CurrentVehicleSelection];
    VehicleCurrentArmor.setValue(getWord(VehicleCurrentArmor.range, 1) / 2);

    VehicleCurrentArmorText.setText(VehicleCurrentArmor.getValue() SPC "Units");
}

function VehicleEdit::CurrentVehicleShieldChanged()
{
    VehicleCurrentShield.range = "0" SPC $VehicleEdit::Vehicles::BaseShield[$VehicleEdit::CurrentVehicleSelection];
    VehicleCurrentShield.setValue(getWord(VehicleCurrentShield.range, 1) / 2);

    VehicleCurrentShieldText.setText(VehicleCurrentShield.getValue() SPC "Units");
}

function VehicleEdit::CurrentVehicleRegenChanged()
{
    VehicleCurrentRegen.range = "0" SPC $VehicleEdit::Vehicles::BaseRegen[$VehicleEdit::CurrentVehicleSelection];
    VehicleCurrentRegen.setValue(getWord(VehicleCurrentRegen.range, 1) / 2);

    VehicleCurrentRegenText.setText(VehicleCurrentRegen.getValue() SPC "Units");
}

function VehicleEdit::CurrentVehicleWeightChanged()
{
    VehicleCurrentWeight.range = "0" SPC $VehicleEdit::Vehicles::BaseWeight[$VehicleEdit::CurrentVehicleSelection];
    VehicleCurrentWeight.setValue(getWord(VehicleCurrentWeight.range, 1) / 2);

    VehicleCurrentWeightText.setText(VehicleCurrentWeight.getValue() SPC "Lbs");
}

function VehicleEdit::WeaponSlotChanged()
{
    %selected = VehicleSlotSelection.getSelected();

    // Now we populate the weapon list with compatible weapons
    VehicleWeaponSelection.clear();

    %weaponCount = 0;
    %vehicleCompatMask = $VehicleEdit::Vehicles::Mask[$VehicleEdit::CurrentVehicleSelection];

    for (%iteration = 0; %iteration < $VehicleEdit::Weapons::Count; %iteration++)
    {
        // First check if the weapon is compatible with the vehicle. If there is no compatibility mask set for the weapon, it is always compatible on that raw compat check.
        %vehicleCompatResult = $VehicleEdit::Weapons::VehicleCompatibilityMask[%iteration] $= "" || $VehicleEdit::Weapons::VehicleCompatibilityMask[%iteration] & %vehicleCompatMask;
        %slotTypeResult = $VehicleEdit::Vehicles::Slots::TypeMask[$VehicleEdit::CurrentVehicleSelection, %selected] & $VehicleEdit::Weapons::TypeMask[%iteration];
        %slotSizeResult = $VehicleEdit::Vehicles::Slots::SlotSize[$VehicleEdit::CurrentVehicleSelection, %selected] == $VehicleEdit::Weapons::SlotSize[%iteration];

        if (%vehicleCompatResult && %slotTypeResult && %slotSizeResult)
        {
            VehicleWeaponSelection.add($VehicleEdit::Weapons::Name[%iteration], %weaponCount);
            %weaponCount++;
        }
    }
    // Set the weapon selection accordingly
    %weaponName = $VehicleEdit::Weapons::Name[$VehicleEdit::CurrentWeaponSelection[%selected]];
    %selectedID = VehicleWeaponSelection.getIDByText(%weaponName);

    // Check Fire Groups
    %vehicleName = $VehicleEdit::Vehicles::Name[$VehicleEdit::CurrentVehicleSelection];

    VehicleWeaponSelection.setSelected(%selectedID);
    VehicleEdit::WeaponChanged();
}

function VehicleEdit::ModuleTypeChanged()
{
    %type = VehicleModuleType.getValue();
    VehicleModuleSelection.clear();

    %weaponSlotID = VehicleSlotSelection.getSelected();

    %currentCount = 0;
    %vehicleCompatMask = $VehicleEdit::Vehicles::Mask[$VehicleEdit::CurrentVehicleSelection];
    for (%iteration = 0; %iteration < $VehicleEdit::Modules::Count[%type]; %iteration++)
        if (($VehicleEdit::Modules::VehicleMask[%type, %iteration] $= "" || $VehicleEdit::Modules::VehicleMask[%type, %iteration] & %vehicleCompatMask))
        {
            VehicleModuleSelection.add($VehicleEdit::Modules::Name[%type, %iteration], %currentCount);
            %currentCount++;
        }

    %selectedID = $VehicleEdit::CurrentModuleSelection[%type];
    %selectedName = $VehicleEdit::Modules::Name[%type, %selectedID];
    %selectedID = VehicleModuleSelection.getIDByText(%selectedName);

    VehicleModuleSelection.setSelected(%selectedID);
    VehicleEdit::ModuleSlotChanged();
}

function VehicleEdit::_processCombinedModules()
{
    // Loop foreach module type and calculate combined relative values
    for (%moduleTypeID = 0; %moduleTypeID < $VehicleEdit::Modules::TypeCount; %moduleTypeID++)
    {
        %typeName = $VehicleEdit::Modules::Types[%moduleTypeID];

        %selectedID = $VehicleEdit::CurrentModuleSelection[%typeName];

        %relArmor += $VehicleEdit::Modules::RelativeArmor[%typeName, %selectedID];
        %relShield += $VehicleEdit::Modules::RelativeShield[%typeName, %selectedID];
        %relWeight += $VehicleEdit::Modules::RelativeWeight[%typeName, %selectedID];
        %relRegen += $VehicleEdit::Modules::RelativeRegen[%typeName, %selectedID];
    }

    // Now update the sliders
    %vehicleID = $VehicleEdit::CurrentVehicleSelection;

    // Armor
    %baseArmor = $VehicleEdit::Vehicles::BaseArmor[%vehicleID];
    %minimumArmor = $VehicleEdit::Vehicles::MinimumArmor[%vehicleID];
    %maximumArmor = $VehicleEdit::Vehicles::MaximumArmor[%vehicleID];

    VehicleEdit::_processModuleAffects(VehicleCurrentArmorText, VehicleCurrentArmor, %minimumArmor, %maximumArmor, %baseArmor,
    %relArmor, false, true);

    // Regen
    %baseRegen = $VehicleEdit::Vehicles::BaseRegen[%vehicleID];
    %minimumRegen = $VehicleEdit::Vehicles::MinimumRegen[%vehicleID];
    %maximumRegen = $VehicleEdit::Vehicles::MaximumRegen[%vehicleID];

    VehicleEdit::_processModuleAffects(VehicleCurrentRegenText, VehicleCurrentRegen, %minimumRegen, %maximumRegen, %baseRegen,
    %relRegen, false, true);

    // Weight
    %baseWeight = $VehicleEdit::Vehicles::BaseRegen[%vehicleID];
    %minimumWeight = $VehicleEdit::Vehicles::MinimumWeight[%vehicleID];
    %maximumWeight = $VehicleEdit::Vehicles::MaximumWeight[%vehicleID];

    VehicleEdit::_processModuleAffects(VehicleCurrentWeightText, VehicleCurrentWeight, %minimumWeight, %maximumWeight, %baseWeight,
    %relWeight, true, true);

    // Shield
    %baseShield = $VehicleEdit::Vehicles::BaseShield[%vehicleID];
    %minimumShield = $VehicleEdit::Vehicles::MinimumShield[%vehicleID];
    %maximumShield = $VehicleEdit::Vehicles::MaximumShield[%vehicleID];

    VehicleEdit::_processModuleAffects(VehicleCurrentShieldText, VehicleCurrentShield, %minimumShield, %maximumShield, %baseShield,
    %relShield, false, true);
}

function VehicleEdit::_processModuleAffects(%text, %slider, %minVal, %maxVal, %baseVal, %relVal, %invert, %displayBase)
{
    %slider.range = "0 1";

    %newValue = %baseVal + %relVal;

    %sliderValue = mVariate(%minVal, %maxVal, %baseVal, %newValue);
    %slider.setValue(%sliderValue);

    if (%relVal == 0)
    {
        %displayed = "(No Change)";

        if (%displayBase)
            %displayed = %baseVal SPC %displayed;
    }
    else
    {
        %colors[0] = "<color:FF0000>";
        %colors[1] = "<color:00FF00>";
        %color = %colors[%relVal > 0 ^ %invert];

        %displayed = %relVal > 0 ? %color @ "+" @ %relVal SPC "Units" : %color @ %relVal SPC "Units";

        if (%displayBase)
            %displayed = (%baseVal + %relVal) SPC %color @ "(" @ %displayed @ ")";
    }

    %text.setText(%displayed);
}

function VehicleEdit::ModuleSlotChanged()
{
    %moduleType = VehicleModuleType.getValue();
    %vehicleID = $VehicleEdit::CurrentVehicleSelection;

    %moduleName = VehicleModuleSelection.getValue();
    %moduleID = VehicleEdit::lookupModuleID(%moduleType, %moduleName, %vehicleID);

    // Update the current selection
    $VehicleEdit::CurrentModuleSelection[%moduleType] = %moduleID;

    // Armor
    %baseArmor = $VehicleEdit::Vehicles::BaseArmor[%vehicleID];
    %relativeArmor = $VehicleEdit::Modules::RelativeArmor[%moduleType, %moduleID];
    %minimumArmor = $VehicleEdit::Vehicles::MinimumArmor[%vehicleID, %moduleType];
    %maximumArmor = $VehicleEdit::Vehicles::MaximumArmor[%vehicleID, %moduleType];

    VehicleEdit::_processModuleAffects(VehicleModuleArmorText, VehicleModuleArmor, %minimumArmor, %maximumArmor,
    %baseArmor, %relativeArmor, false);

    // Shield
    %baseShield = $VehicleEdit::Vehicles::BaseShield[%vehicleID];
    %relativeShield = $VehicleEdit::Modules::RelativeShield[%moduleType, %moduleID];
    %minimumShield = $VehicleEdit::Vehicles::MinimumShield[%vehicleID, %moduleType];
    %maximumShield = $VehicleEdit::Vehicles::MaximumShield[%vehicleID, %moduleType];

    VehicleEdit::_processModuleAffects(VehicleModuleShieldText, VehicleModuleShield, %minimumShield, %maximumShield,
    %baseShield, %relativeShield, false);

    // Energy
    %baseRegen = $VehicleEdit::Vehicles::BaseRegen[%vehicleID];
    %relativeRegen = $VehicleEdit::Modules::RelativeRegen[%moduleType, %moduleID];
    %minimumRegen = $VehicleEdit::Vehicles::MinimumRegen[%vehicleID, %moduleType];
    %maximumRegen = $VehicleEdit::Vehicles::MaximumRegen[%vehicleID, %moduleType];

    VehicleEdit::_processModuleAffects(VehicleModuleEnergyText, VehicleModuleEnergy, %minimumRegen, %maximumRegen,
    %baseRegen, %relativeRegen, false);

    // Weight
    %baseWeight = $VehicleEdit::Vehicles::BaseWeight[%vehicleID];
    %relativeWeight = $VehicleEdit::Modules::RelativeWeight[%moduleType, %moduleID];
    %minimumWeight = $VehicleEdit::Vehicles::MinimumWeight[%vehicleID, %moduleType];
    %maximumWeight = $VehicleEdit::Vehicles::MaximumWeight[%vehicleID, %moduleType];

    VehicleEdit::_processModuleAffects(VehicleModuleWeightText, VehicleModuleWeight, %minimumWeight, %maximumWeight,
    %baseWeight, %relativeWeight, true);

    VehicleEdit::_processCombinedModules();

    // Display the module description
    ModuleSelectionText.setText($VehicleEdit::Modules::Description[%moduleType, %moduleID]);

    // Save the current module selection.
    VehicleEdit::Save();
}

function VehicleEdit::Exit()
{
    canvas.popDialog();
}

function VehicleEdit::Save()
{
    // Save preset values
    %vehicleID = $VehicleEdit::CurrentVehicleSelection;
    %vehicleName = $VehicleEdit::Vehicles::Name[%vehicleID];
    %weapons = "";
    %modules = "";

    %presetID = $VehicleEdit::SelectedPreset[$VehicleEdit::CurrentVehicleSelection];
    for (%weaponSlot = 0; %weaponSlot < $VehicleEdit::Vehicles::Slots::Count[%vehicleID]; %weaponSlot++)
    {
        %weaponID = $VehicleEdit::CurrentWeaponSelection[%weaponSlot];
        $VehicleEdit::Presets::Weapon[%vehicleName, %presetID, %weaponSlot] = $VehicleEdit::Weapons::Name[%weaponID];

        %weapons = %weapons TAB $VehicleEdit::Weapons::ServerID[%weaponID];
    }

    for (%moduleTypeID = 0; %moduleTypeID < $VehicleEdit::Modules::TypeCount; %moduleTypeID++)
    {
        %moduleTypeName = $VehicleEdit::Modules::Types[%moduleTypeID];
        $VehicleEdit::Presets::Module[%vehicleName, %presetID, %moduleTypeName] = $VehicleEdit::Modules::Name[%moduleTypeName, $VehicleEdit::CurrentModuleSelection[%moduleTypeName]];

        %modules = %modules TAB $VehicleEdit::Modules::ServerID[%moduleTypeName, $VehicleEdit::CurrentModuleSelection[%moduleTypeName]];
    }

    $VehicleEdit::Presets::Name[%vehicleID, %presetID] = VehicleEditPresetName.getValue();
    nameToID("VehiclePreset" @ %presetID).setText($VehicleEdit::Presets::Name[%vehicleID, %presetID]);

    commandToServer('VEditSetLoadout', $VehicleEdit::Vehicles::ServerID[%vehicleID], $VehicleEdit::Presets::Name[%vehicleID, %presetID], %weapons, %modules);
    export("$VehicleEdit::Presets::*", "vehicleEditPresets.cs", false);
}

// Sends loadout data to server for current vehicle
function VehicleEdit::UpdateCurrentVehicleLoadout()
{
    return; // will have code in ::Save() ported over once finished tweaking
}


function VehicleEditDlg::onWake(%this)
{
    VehicleEditDlg.setVisible(true);

    VehicleDropdown.clear();

    // Repopulate the vehicles
    for (%iteration = 0; %iteration < $VehicleEdit::Vehicles::Count; %iteration++)
        VehicleDropdown.add(strReplace($VehicleEdit::Vehicles::Name[%iteration], "_", " "), %iteration);

    // Select the first vehicle in our list
    VehicleDropDown.setSelected(0);
    VehicleEdit::VehicleChanged();
}

function VehicleEditDlg::onSleep(%this)
{
    VehicleEditDlg.setVisible(false);
}

function ToggleVehicleEdit(%val)
{
    if (%val && VehicleEditDlg.isVisible())
        canvas.popDialog();
    else if (%val)
        canvas.pushDialog(VehicleEditDlg);
}

package VehicleEditHooks
{
    function OptionsDlg::onWake(%this)
    {
        if (!$VehicleEdit::BuiltBindings)
        {
            $RemapName[$RemapCount] = "Vehicle Loadout";
            $RemapCmd[$RemapCount] = "ToggleVehicleEdit";
            $RemapCount++;

            $VehicleEdit::BuiltBindings = true;
        }

        parent::onWake(%this);
    }

    function disconnect()
    {
        VehicleEdit::resetInternalDatabase();
        parent::disconnect();
    }
};

if (!isActivePackage(VehicleEditHooks))
    activatePackage(VehicleEditHooks);
