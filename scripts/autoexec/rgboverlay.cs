$RGBOverlay::Count = 4;

//With rgboverlay.cs loaded on the client and that texture being placed correctly:
//commandToClient(%client, 'ShowRGBOverlay', true, "0 255 0", 0.5, 1000);
//Shows a pulsing green overlay at half opacity.
//commandToClient(%client, 'HideRGBOverlay');
//Obviously hides it.
//First parameter is pulsing (whether or not it should pulse), second is color in R G B format, third is maximum opacity, and last is the time between oscillations (time between fully invisible and fully visible).
//If pulsing is off then it will remain static at the color and opacity you specified.
//It assumes the end user will not be playing on a resolution bigger than 4096 in width or height.
//Any bigger and they will notice it stops drawing at some point in the right and bottom portions of the screen.

function clientCmdShowRGBOverlay(%pulsing, %color, %opacity, %pulseRate)
{
    // Initialize the Elements
    if (!isObject(RGBOverlayElement1))
    {
        for (%iteration = 0; %iteration < $RGBOverlay::Count; %iteration++)
        {
            %container = new GuiControl("RGBOverlayContainer" @ %iteration)
            {
                position = "0" SPC (1024 * %iteration);
                extent = "4096 1024";
            };

            %element = new HUDPulsingBitmap("RGBOverlayElement" @ %iteration)
            {
                visible = false;
                position = "0" SPC (1024 * iteration);
                bitmap = "gui/customPulse";
                extent = "4096 1024";
                horizSizing = "relative";
                vertSizing = "relative";
            };

            %container.add(%element);
            PlayGUI.add(%container);
        }
    }

    // Process new settings and set visible
    for (%iteration = 0; %iteration < $RGBOverlay::Count; %iteration++)
    {
        %currentName = "RGBOverlayElement" @ %iteration;
        %currentName.color = %color;
        %currentName.pulseRate = %pulseRate;
        %currentName.opacity = %opacity;
        %currentName.pulse = %pulsing;

        %currentName.setVisible(true);
    }
}

function clientCmdHideRGBOverlay()
{
    for (%iteration = 0; %iteration < $RGBOverlay::Count; %iteration++)
    {
        %currentName = "RGBOverlayElement" @ %iteration;
        if (isObject(%currentName))
            %currentName.setVisible(false);
    }
}
