// #autoload
// #name = TabMenuHud
// #version = 1.3
// #date = 2/20/02
// #author = DEbig3
// #warrior =  DEbig3
// #email = debig3@attbi.com
// #web = http://debig3.tribes-universe.com
// #description = a hud thing similar to the t1 tab menu
// #credit = Advis, for writing this thing
// #status = release
// #readme =
// #category = ilys' Scripts

package TabMenuHud {

function TabMenuHudToggle(%val)
{
	if (%val)
	{
		if ($TabMenuHudOn)
		{
			Canvas.popDialog(TabMenuHudPlayerActionDlg);
			Canvas.popDialog(TabMenuHud);
		}
		else
		{
			Canvas.pushDialog(TabMenuHud);
		}
	}
}


function TabMenuHud::onAdd(%this)
{
   // Add the Player popup menu:
   new GuiControl(TabMenuHudPlayerActionDlg)
   {
      profile = "GuiModelessDialogProfile";
      horizSizing = "width";
      vertSizing = "height";
      position = "0 0";
      extent = "640 480";
      minExtent = "8 8";
      visible = "1";
      setFirstResponder = "0";
      modal = "1";

      new ShellPopupMenu(TabMenuHudPlayerPopup)
      {
         profile = "ShellPopupProfile";
         position = "0 0";
         extent = "0 0";
         minExtent = "0 0";
         visible = "1";
         maxPopupHeight = "200";
         noButtonStyle = "1";
      };
   };
}

function TabMenuHud::OnWake(%this)
{
	$TabMenuHudOn = true;

	if (!%this.init)
	{
		TabMenuHudList.setSortColumn($pref::TabMenu::SortColumnKey);
		TabMenuHudList.setSortIncreasing($pref::TabMenu::SortInc);
		TabMenuHudList2.setSortColumn($pref::TabMenu2::SortColumnKey);
		TabMenuHudList2.setSortIncreasing($pref::TabMenu2::SortInc);
		TabMenuHudList3.setSortColumn($pref::TabMenu3::SortColumnKey);
		TabMenuHudList3.setSortIncreasing($pref::TabMenu3::SortInc);

		%this.init = true;
	}

	TabMenuHudUpdate();
	TabMenuVoteFill();
	if (isobject(autoload)) {
		TabMenuHudScriptsButton.setVisible("1");
	}

    if (isObject(hudMap))
    {
        hudMap.pop();
        hudMap.delete();
    }
    new ActionMap(hudMap);
    hudMap.blockBind(moveMap, toggleInventoryHud);
    hudMap.blockBind(moveMap, toggleCommanderMap);
    hudMap.blockBind(moveMap, toggleScoreScreen);

    hudMap.bindCmd(keyboard, "escape", "", "TabMenuHudToggle(1);");
    hudMap.push();

    // Tab is usually used in options menus but we're overriding it
    if (getField(moveMap.getBinding(TabMenuHudToggle), 1) $= tab)
    {
        GlobalActionMap.copyBind(moveMap, TabMenuHudToggle);
    }
}

function TabMenuHud::OnSleep(%this)
{
	if (%this.schedule != 0) cancel(%this.schedule);

	TabMenuHudList.SaveColumns();
	TabMenuHudList2.SaveColumns();
	TabMenuHudList3.SaveColumns();

	TabMenuVoteMenu.clear();
	TabMenuVoteMenu.mode = "";
	TabMenuVoteCancel.setVisible(false);

    clearMapping(GlobalActionMap, TabMenuHudToggle);
    hudMap.pop();
    hudMap.delete();

	$TabMenuHudOn = false;
}

function TabMenuHudUpdate()
{
	if (TabMenuHud.schedule != 0) cancel(TabMenuHud.schedule);

	if ($TabMenuHudOn)
	{
		commandToServer('getScores');

		TabMenuHud.schedule = schedule(4000, 0, TabMenuHudUpdate);
	}
}

function TabMenuHudList::InitColumns(%this)
{
	%this.clear();
	%this.clearColumns();

    %this.addColumn(0, " ", $pref::TabMenu::Column0, 24, 24, "center");   // Flag column
	%this.addColumn(1, "Player", $pref::TabMenu::Column1, 50, 300);
	%this.addColumn(2, "Team", $pref::TabMenu::Column2, 50, 200);
	%this.addColumn(3, "Score", $pref::TabMenu::Column3, 25, 100, "numeric center");
	%this.addColumn(4, "Ping", $pref::TabMenu::Column4, 25, 100, "numeric center");
   	%this.addColumn(5, "PL", $pref::TabMenu::Column5, 25, 100, "numeric center");

	$TabMenuTeam1Count = 0;
	$TabMenuTeam2Count = 0;
	$TabMenuObservingCount = 0;
}

function TabMenuHudList2::InitColumns(%this)
{
	%this.clear();
	%this.clearColumns();

    %this.addColumn(0, " ", $pref::TabMenu2::Column0, 24, 24, "center");   // Flag column
	%this.addColumn(1, "Player", $pref::TabMenu2::Column1, 50, 300);
	%this.addColumn(2, "Team", $pref::TabMenu2::Column2, 50, 200);
	%this.addColumn(3, "Score", $pref::TabMenu2::Column3, 25, 100, "numeric center");
	%this.addColumn(4, "Ping", $pref::TabMenu2::Column4, 25, 100, "numeric center");
   	%this.addColumn(5, "PL", $pref::TabMenu2::Column5, 25, 100, "numeric center");
}

function TabMenuHudList3::InitColumns(%this) {
	%this.clear();
	%this.clearColumns();

    %this.addColumn(0, " ", $pref::TabMenu3::Column0, 24, 24, "center");
	%this.addColumn(1, "Player", $pref::TabMenu3::Column1, 50, 300);
	%this.addColumn(2, "Team", $pref::TabMenu3::Column2, 50, 200);
	%this.addColumn(3, "Ping", $pref::TabMenu3::Column3, 25, 100, "numeric center");
}

function TabMenuHudList::onColumnResize(%this, %col, %size)
{
   $pref::TabMenu::Column[%this.getColumnKey(%col)] = %size;
}

function TabMenuHudList2::onColumnResize(%this, %col, %size)
{
   $pref::TabMenu2::Column[%this.getColumnKey(%col)] = %size;
}

function TabMenuHudList3::onColumnResize(%this, %col, %size)
{
   $pref::TabMenu3::Column[%this.getColumnKey(%col)] = %size;
}

function TabMenuHudList::onSetSortKey(%this, %key, %increasing)
{
   $pref::TabMenu::SortColumnKey = %key;
   $pref::TabMenu::SortInc = %increasing;
}

function TabMenuHudList2::onSetSortKey(%this, %key, %increasing)
{
   $pref::TabMenu2::SortColumnKey = %key;
   $pref::TabMenu2::SortInc = %increasing;
}

function TabMenuHudList3::onSetSortKey(%this, %key, %increasing)
{
   $pref::TabMenu3::SortColumnKey = %key;
   $pref::TabMenu3::SortInc = %increasing;
}

function TabMenuHudList::onRightMouseDown(%this, %column, %row, %mousePos)
{
   // Open the action menu:
   %clientId = %this.getRowId(%row);
   TabMenuHudPlayerPopup.player = $PlayerList[%clientId];

   if (TabMenuHudPlayerPopup.player !$= "")
   {
      TabMenuHudPlayerPopup.position = %mousePos;
      Canvas.pushDialog(TabMenuHudPlayerActionDlg);
      TabMenuHudPlayerPopup.forceOnAction();
   }
}

function TabMenuHudList2::onRightMouseDown(%this, %column, %row, %mousePos)
{
   // Open the action menu:
   %clientId = %this.getRowId(%row);
   TabMenuHudPlayerPopup.player = $PlayerList[%clientId];

   if (TabMenuHudPlayerPopup.player !$= "")
   {
      TabMenuHudPlayerPopup.position = %mousePos;
      Canvas.pushDialog(TabMenuHudPlayerActionDlg);
      TabMenuHudPlayerPopup.forceOnAction();
   }
}

function TabMenuHudList3::onRightMouseDown(%this, %column, %row, %mousePos)
{
   // Open the action menu:
   %clientId = %this.getRowId(%row);
   TabMenuHudPlayerPopup.player = $PlayerList[%clientId];

   if (TabMenuHudPlayerPopup.player !$= "")
   {
      TabMenuHudPlayerPopup.position = %mousePos;
      Canvas.pushDialog(TabMenuHudPlayerActionDlg);
      TabMenuHudPlayerPopup.forceOnAction();
   }
}

function TabMenuHudList::SaveColumns(%this)
{
	%count = %this.getNumColumns();
	for (%col = 0; %col < %count; %col++)
	{
		$pref::TabMenu::Column[%this.getColumnKey(%col)] = %this.getColumnWidth(%col);
	}
}

function TabMenuHudList2::SaveColumns(%this)
{
	%count = %this.getNumColumns();
	for (%col = 0; %col < %count; %col++)
	{
		$pref::TabMenu2::Column[%this.getColumnKey(%col)] = %this.getColumnWidth(%col);
	}
}

function TabMenuHudList3::SaveColumns(%this)
{
	%count = %this.getNumColumns();
	for (%col = 0; %col < %count; %col++)
	{
		$pref::TabMenu3::Column[%this.getColumnKey(%col)] = %this.getColumnWidth(%col);
	}
}

function TabMenuHudUpdatePlayer(%clientId)
{
	if (!$TabMenuHudOn) return;

	%player = $PlayerList[%clientId];
	if (!isObject(%player)) return;

	// Build the text:
	if (%player.isSuperAdmin)
		%tag = "SA";
	else if (%player.isAdmin)
		%tag = "A";
	else if (%player.isBot)
		%tag = "B";
	else
		%tag = " ";

	if($clTeamCount > 1) {
		if (%player.teamId == 0) {
			%teamName = "Observer";
			%text = %tag TAB %player.name TAB %teamName TAB %player.ping;
		}
		else {
			%teamName = $clTeamScore[%player.teamId, 0] $= "" ? "-" : $clTeamScore[%player.teamId, 0];
			%text = %tag TAB %player.name TAB %teamName TAB %player.score TAB %player.ping TAB %player.packetLoss;
		}
	} else {
		if (%player.teamId == 0) {
			%teamName = "Observer";
			%text = %tag TAB %player.name TAB %teamName TAB %player.ping;
		}
		else {
			%teamName = "Playing";
			%text = %tag TAB %player.name TAB %teamName TAB %player.score TAB %player.ping TAB %player.packetLoss;
		}
	}


	if (%player.teamId == 1) {		// Let's assume this is the first team......
		$TeamHudTeamTop = getField(%text,2);
		if (TabMenuHudList.getRowNumById(%clientId) == -1) {
			TabMenuHudList.addRow(%clientId, %text);

			$TabMenuTeam1Count = $TabMenuTeam1Count + 1;
			TabMenuTeam1.setText($TeamHudTeamTop @ ":");
			TabMenuTeam1Number.setText($TabMenuTeam1Count);
   		} else
			TabMenuHudList.setRowById(%clientId, %text);
	}
	else if (%player.teamId == 2) {
		$TeamHudTeamBottom = getField(%text, 2);
		if (TabMenuHudList2.getRowNumById(%clientId) == -1) {
			TabMenuHudList2.addRow(%clientId, %text);

			$TabMenuTeam2Count = $TabMenuTeam2Count + 1;
			TabMenuTeam2.setText($TeamHudTeamBottom @ ":");
			TabMenuTeam2Number.setText($TabMenuTeam2Count);
   		} else
			TabMenuHudList2.setRowById(%clientId, %text);
	}
	else {
		if (TabMenuHudList3.getRowNumById(%clientId) == -1) {
			TabMenuHudList3.addRow(%clientId, %text);
			$TabMenuObservingCount = $TabMenuObservingCount + 1;
			TabMenuObservingNumber.setText($TabMenuObservingCount);
		}
		else
			TabMenuHudList3.setRowById(%clientId, %text);
	}

 	TabMenuHudList.sort();
	TabMenuHudList2.sort();
	TabMenuHudList3.sort();
}

function TabMenuHudRemovePlayer(%clientID) {

	%player = $PlayerList[%clientId];
	//if (!isObject(%player)) return;

	if($clTeamCount > 1) {
		if (%player.teamId == 0)
			%teamName = "Observer";
		else
			%teamName = $clTeamScore[%player.teamId, 0] $= "" ? "-" : $clTeamScore[%player.teamId, 0];
	}

	%oldTeam = getField(TabMenuHudList.getRowTextById(%clientId), 2);
	if(%oldTeam $= "")
		%oldTeam =  getField(TabMenuHudList2.getRowTextById(%clientId), 2);
	if (%oldTeam $= "")
		%oldTeam =  getField(TabMenuHudList3.getRowTextById(%clientId), 2);


	if(%oldTeam $= $TeamHudTeamTop) {
		TabMenuHudList.removeRowById(%clientId);
		if ($TabMenuTeam1Count > 0)
			$TabMenuTeam1Count = $TabMenuTeam1Count - 1;
		TabMenuTeam1.setText($TeamHudTeamTop @ ":");
		TabMenuTeam1Number.setText($TabMenuTeam1Count);
	}
	else if (%oldTeam $= $teamHudTeamBottom) {
		TabMenuHudList2.removeRowById(%clientId);
		if ($TabMenuTeam2Count > 0)
			$TabMenuTeam2Count = $TabMenuTeam2Count - 1;
		TabMenuTeam2.setText($TeamHudTeamBottom @ ":");
		TabMenuTeam2Number.setText($TabMenuTeam2Count);
	}
	else {
		TabMenuHudList3.removeRowById(%clientId);
		if ($TabMenuObserverCount > 0)
			$TabMenuObservingCount = $TabMenuObservingCount - 1;
		TabMenuObservingNumber.setText($TabMenuObservingCount);
	}
}

function TabMenuHudScoreInfo(%msgType, %msgString, %clientId, %score, %ping, %packetLoss)
{
	TabMenuHudUpdatePlayer(%clientId);
}

function TabMenuHudBWAdminRegister(%msgType, %msgString)
{
    $TabMenuHudBWClient = true;
}

function TabMenuHudClientJoin(%msgType, %msgString, %clientName, %clientId, %targetId, %isAI, %isAdmin, %isSuperAdmin, %isSmurf, %guid)
{
    if (strstr(%msgString, "Welcome to Tribes2") != -1)
    {
        $TabMenuHudClient = %clientId;

        // Register with the server to recieve the extra menus
        commandToServer('BWAdminRegister');
    }

	TabMenuHudUpdatePlayer(%clientId);
}

function TabMenuHudClientDrop(%msgType, %msgString, %clientName, %clientId)
{
	TabMenuHudRemovePlayer(%clientId);
}

function TabMenuHudClientJoinTeam(%msgType, %msgString, %clientName, %teamName, %clientId, %teamId)
{
	TabMenuHudRemovePlayer(%clientId);
	TabMenuHudUpdatePlayer(%clientId);
}

function TabMenuHudDefaultColumns()
{
	$pref::TabMenu::Column0 = 24;
	$pref::TabMenu::Column1 = 182;
	$pref::TabMenu::Column2 = 100;
	$pref::TabMenu::Column3 = 50;
	$pref::TabMenu::Column4 = 50;
	$pref::TabMenu::Column5 = 50;

	$pref::TabMenu2::Column0 = 24;
	$pref::TabMenu2::Column1 = 182;
	$pref::TabMenu2::Column2 = 100;
	$pref::TabMenu2::Column3 = 50;
	$pref::TabMenu2::Column4 = 50;
	$pref::TabMenu2::Column5 = 50;

	$pref::TabMenu3::Column0 = 24;
	$pref::TabMenu3::Column1 = 182;
	$pref::TabMenu3::Column2 = 100;
	$pref::TabMenu3::Column3 = 50;
	$pref::TabMenu3::Column4 = 50;
}

function TabMenuHudSetup()
{
	if (isObject(TabMenuHud))
	{
		if (TabMenuHud.playerDialogOpen) Canvas.popDialog(TabMenuHudPlayerActionDlg);
		Canvas.popDialog(TabMenuHud);
		TabMenuHud.delete();
	}

	exec("gui/TabMenuHud.gui");

	if ($pref::TabMenu::Column0 $= "")
	{
		TabMenuHudDefaultColumns();
		$pref::TabMenu::SortColumnKey = 3;
		$pref::TabMenu::SortInc = 0;

		$pref::TabMenu2::SortColumnKey = 3;
		$pref::TabMenu2::SortInc = 0;

		$pref::TabMenu3::SortColumnKey = 2;
		$pref::TabMenu3::SortColumnKey = 0;
	}
}

function TabMenuHudTeamListMessage(%msgType, %msgString, %teamCount, %teamList)			// CHANGED
{
	TabMenuHudList.InitColumns();
	TabMenuHudList2.InitColumns();
	TabMenuHudList3.InitColumns();
}

function TabMenudHudSuperAdminPlayer(%msgType, %msgString, %clientId)
{
    if (strstr(%msgString, " has become a Super Admin by force.") != -1)
    {
    	%player = $PlayerList[%clientId];
    	if (%player)
    	{
    		%player.isAdmin = true;
    		%player.isSuperAdmin = true;
    		TabMenuHudUpdatePlayer(%clientId);
    	}
    }
    else
    if (strstr(%msgString, " has become an Admin by force.") != -1)
    {
    	%player = $PlayerList[%clientId];
    	if (%player)
    	{
    		%player.isAdmin = true;
    		%player.isSuperAdmin = false;
    		TabMenuHudUpdatePlayer(%clientId);
    	}
    }
    else
    if (strstr(%msgString, " has become a Peon by force.") != -1)
    {
    	%player = $PlayerList[%clientId];
    	if (%player)
    	{
    		%player.isAdmin = false;
    		%player.isSuperAdmin = false;
    		TabMenuHudUpdatePlayer(%clientId);
    	}
    }
}

function TabMenuDisconnect()
{
   MessageBoxYesNo( "CONFIRM", "Are you sure you want to leave this game?", "TabMenuLeaveGame();", "" );
}

//------------------------------------------------------------------------------
function TabMenuLeaveGame()
{
   Canvas.popDialog( TabMenuHud );
   Disconnect();
}




//
// --- TabMenuVote ---
//

function TabMenuVoteFill()
{
   TabMenuVoteMenu.key++;
   TabMenuVoteMenu.clear();
   TabMenuVoteMenu.tourneyChoose = 0;
   commandToServer('GetVoteMenu', TabMenuVoteMenu.key);
}

function TabMenuVoteFillTeam()
{
   TabMenuVoteMenu.key++;
   TabMenuVoteMenu.clear();
   TabMenuVoteMenu.mode = "team";
   commandToServer('GetTeamList', TabMenuVoteMenu.key);
   TabMenuVoteCancel.setVisible(true);
}

function TabMenuVoteFillMissionType()
{
   TabMenuVoteMenu.key++;
   TabMenuVoteMenu.clear();
   TabMenuVoteMenu.mode = "type";
   commandToServer('GetMissionTypes', TabMenuVoteMenu.key);
   TabMenuVoteCancel.setVisible(true);
}

function TabMenuVoteFillMission(%type, %typeName)
{
   TabMenuVoteMenu.key++;
   TabMenuVoteMenu.clear();
   TabMenuVoteMenu.mode = "mission";
   TabMenuVoteMenu.missionType = %type;
   TabMenuVoteMenu.typeName = %typeName;
   commandToServer('GetMissionList', TabMenuVoteMenu.key, %type);
}

function TabMenuVoteFillTimeLimit()
{
   TabMenuVoteMenu.key++;
   TabMenuVoteMenu.clear();
   TabMenuVoteMenu.mode = "timeLimit";
   commandToServer('GetTimeLimitList', TabMenuVoteMenu.key);
   TabMenuVoteCancel.setVisible(true);
}

function TabMenuVoteItemMessage(%msgType, %msgString, %key, %voteName, %voteActionMsg, %voteText, %sort)
{
	if (!$TabMenuHudOn || %key != TabMenuVoteMenu.key) return;

	%index = TabMenuVoteMenu.rowCount();
	TabMenuVoteMenu.addRow(%index, detag(%voteText));
	if (%sort) TabMenuVoteMenu.sort(0);
	$clVoteCmd[%index] = detag(%voteName);
	$clVoteAction[%index] = detag(%voteActionMsg);
}

function TabMenuVoteControl()
{
   %id = TabMenuVoteMenu.getSelectedId();
   %text = TabMenuVoteMenu.getRowTextById(%id);

   switch$ (TabMenuVoteMenu.mode)
   {
      case "": // Default case...
         // Test for special cases:
         switch$ ($clVoteCmd[%id])
         {
            case "JoinGame":
               CommandToServer('clientJoinGame');
               schedule(100, 0, TabMenuHudToggle, true);
               return;

            case "ChooseTeam":
               TabMenuVoteFillTeam();
               return;

            case "VoteTournamentMode":
               TabMenuVoteMenu.tourneyChoose = 1;
               TabMenuVoteFillMissionType();
               return;

            case "VoteMatchStart":
               startNewVote("VoteMatchStart");
               schedule(100, 0, TabMenuHudToggle, true);
               return;

            case "MakeObserver":
               commandToServer('ClientMakeObserver');
               schedule(100, 0, TabMenuHudToggle, true);
               return;

            case "VoteChangeMission":
               TabMenuVoteFillMissionType();
               return;

            case "VoteChangeTimeLimit":
               TabMenuVoteFillTimeLimit();
               return;

            case "Addbot":
               commandToServer('addBot');
               return;
         }

      case "team":
         commandToServer('ClientJoinTeam', %id++);
         schedule(100, 0, TabMenuHudToggle, true);
         return;

      case "type":
         TabMenuVoteFillMission($clVoteCmd[%id], %text);
         return;

      case "mission":
         if(!TabMenuVoteMenu.tourneyChoose)
         {
            startNewVote("VoteChangeMission",
                  %text,                        // Mission display name
                  TabMenuVoteMenu.typeName,       // Mission type display name
                  $clVoteCmd[%id],              // Mission id
                  TabMenuVoteMenu.missionType);  // Mission type id
         }
         else
         {
            startNewVote("VoteTournamentMode",
                  %text,                        // Mission display name
                  TabMenuVoteMenu.typeName,       // Mission type display name
                  $clVoteCmd[%id],              // Mission id
                  TabMenuVoteMenu.missionType);  // Mission type id
            TabMenuVoteMenu.tourneyChoose = 0;
         }
         TabMenuVoteMenu.reset();
         return;

      case "timeLimit":
         startNewVote("VoteChangeTimeLimit", $clVoteCmd[%id]);
         TabMenuVoteMenu.reset();
         return;
   }

   startNewVote($clVoteCmd[%id], $clVoteAction[%id]);
   TabMenuVoteFill();
}

function TabMenuVoteMenu::reset(%this)
{
	%this.mode = "";
	%this.tourneyChoose = 0;
	TabMenuVoteCancel.setVisible(false);
	TabMenuVoteFill();
}

//
// --- PlayerActionDlg ---
//

function TabMenuHudPlayerActionDlg::onWake(%this)
{
	TabMenuHud.playerDialogOpen = true;
	TabMenuHudPlayerPopupMenu();
}

function TabMenuHudPlayerActionDlg::onSleep(%this)
{
	TabMenuHud.playerDialogOpen = false;
}

function TabMenuHudPlayerPopup::onSelect(%this, %id, %text)
{
	//the id's for these are used in DefaultGame::sendGamePlayerPopupMenu()...
	//mute:				1
	//admin:			2
	//kick:				3
	//ban:				4
	//force observer:	5
	//switch team:		6

	switch(%id)
	{
		case 1:  // Mute/Unmute
			togglePlayerMute(%this.player.clientId);

		case 2:  // Admin
			MessageBoxYesNo("CONFIRM", "Are you sure you want to make " @ %this.player.name @ " an admin?", "TabMenuHudPlayerVote(VoteAdminPlayer, " @ %this.player.clientId @ ");");

		case 3:  // Kick
			MessageBoxYesNo("CONFIRM", "Are you sure you want to kick " @ %this.player.name @ "?", "TabMenuHudPlayerVote(VoteKickPlayer, " @ %this.player.clientId @ ");");

		case 4:  // Ban
			MessageBoxYesNo("CONFIRM", "Are you sure you want to ban " @ %this.player.name @ "?", "TabMenuHudPlayerVote(BanPlayer, " @ %this.player.clientId @ ");");

		case 5: // force observer
			forceToObserver(%this.player.clientId);

		case 6: //change team 1
			changePlayersTeam(%this.player.clientId, 1);
			TabMenuVoteFill();

		case 7: //change team 2
			changePlayersTeam(%this.player.clientId, 2);
			TabMenuVoteFill();

		case 8:
			adminAddPlayerToGame(%this.player.clientId);

		case 9: // enable/disable voice communication
			togglePlayerVoiceCom(%this.player);

		case 10:
			confirmAdminListAdd(%this.player, false);

		case 11:
			confirmAdminListAdd(%this.player, true);

        //
        // --- TabMenuHud options ---
        //

        case 501: // Observe player
            commandToServer('ProcessGameLink', %this.player.clientId);

        //
        // --- bwadmin options ---
        //

		case 601: // Warn player
            commandToServer('AdminWarnPlayer', %this.player.clientId);

		case 602: // Observe player
            commandToServer('ProcessGameLink', %this.player.clientId);
    }

	Canvas.popDialog(TabMenuHudPlayerActionDlg);
}

function TabMenuHudPlayerPopup::onCancel(%this)
{
	Canvas.popDialog(TabMenuHudPlayerActionDlg);
}

function TabMenuHudPlayerPopupMenu()
{
    TabMenuHudPlayerPopup.key++;
    TabMenuHudPlayerPopup.clear();

    TabMenuHudPlayerPopup.add(TabMenuHudPlayerPopup.player.name, 0);

    if (!$TabMenuHudBWClient)
    {
        // Add the observe player line
        %ownTeamId = $PlayerList[$TabMenuHudClient].teamId;
        %selTeamId = TabMenuHudPlayerPopup.player.teamId;
        if (%ownTeamId == 0 && %selTeamId != 0)
        {
            %line = detag("Observe " @ TabMenuHudPlayerPopup.player.name);
            TabMenuHudPlayerPopup.add("     " @ %line, 501);
        }
    }

    commandToServer('GetPlayerPopupMenu', TabMenuHudPlayerPopup.player.clientId, TabMenuHudPlayerPopup.key);
}

function TabMenuHudPlayerPopupMessage(%msgType, %msgString, %key, %voteName, %voteActionMsg, %voteText, %popupEntryId)
{
    if (!$TabMenuHudOn || %key != TabMenuHudPlayerPopup.key) return;

    TabMenuHudPlayerPopup.add("     " @ detag(%voteText), %popupEntryId);
}

function TabMenuHudPlayerVote(%voteType, %playerId)
{
   startNewVote(%voteType, %playerId, 0, 0, 0, true);
   TabMenuVoteFill();
}

//
// --- Overrides ---
//

function DispatchLaunchMode()
{
	TabMenuHudSetup();

	// TabMenuHud
	addMessageCallback('MsgPlayerScore', TabMenuHudScoreInfo);
	addMessageCallback('MsgClientJoin', TabMenuHudClientJoin);
	addMessageCallback('MsgClientDrop', TabMenuHudClientDrop);
	addMessageCallback('MsgClientJoinTeam', TabMenuHudClientJoinTeam);
	addMessageCallback('MsgPlayerPopupItem', TabMenuHudPlayerPopupMessage );
	addMessageCallback('MsgTeamList', TabMenuHudTeamListMessage);
	addMessageCallback('MsgSuperAdminPlayer', TabMenudHudSuperAdminPlayer);
	addMessageCallback('MsgBWAdminRegister', TabMenuHudBWAdminRegister);

	// TabMenuVote
	addMessageCallback('MsgVoteItem', TabMenuVoteItemMessage);

	parent::DispatchLaunchMode();
}

function OptionsDlg::onWake(%this)
{
	if (!$TabMenuHudOptions)
	{
		$RemapName[$RemapCount] = "TabMenu HUD";
		$RemapCmd[$RemapCount]  = "TabMenuHudToggle";
		$RemapCount++;

		$TabMenuHudOptions = true;
	}

	parent::onWake(%this);
}

function OptionsDlg::onSleep(%this)
{
	parent::onSleep(%this);

	// Resolution could've changed
	TabMenuHudSetup();
	TabMenuHudList.InitColumns();
	TabMenuHudList2.InitColumns();
	TabMenuHudList3.InitColumns();
}

// Bind the toggle in vehicles so we can still use the hud
function clientCmdSetDefaultVehicleKeys(%inVehicle)
{
    parent::clientCmdSetDefaultVehicleKeys(%inVehicle);

    if (%inVehicle) passengerKeys.copyBind(moveMap, TabMenuHudToggle);
}

// Disables the TabMenuHud while the Inventory Screen is displayed.
function InventoryScreen::onWake(%this)
{
	parent::onWake(%this);
	hudMap.blockBind(moveMap, TabMenuHudToggle);
}

// Disables the TabMenuHud while the Score Screen is displayed.
function ScoreScreen::onWake(%this)
{
	parent::onWake(%this);
	hudMap.blockBind(moveMap, TabMenuHudToggle);
}


}; // END package TabMenuHud
activatePackage(TabMenuHud);

$TeamHudTeamTop = "";
$TeamHudTeamBottom = "";