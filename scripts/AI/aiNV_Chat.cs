function AIMessageThread(%msg, %clSpeaker, %clListener, %force, %index, %threadId)
{
	//abort if AI chat has been disabled
	if ($AIDisableChat)
		return;

	//initialize the params
	if (%index $= "")
		%index = 0;

	if (%clListener $= "")
		%clListener = 0;

	if (%force $= "")
		%force = false;

	//if this is the initial call, see if we're already in a thread, and if we should force this one
	if (%index == 0 && $AIMsgThreadActive && !%force)
	{
                AIEndMessageThread(%threadId);//added here to clear any message errors - Lagg...
		//error("DEBUG msg thread already in progress - aborting: " @ %msg @ " from client: " @ %clSpeaker);
                return;
	}

	//if this is an ongoing thread, make sure it wasn't pre-empted
	if (%index > 0 && %threadId != $AIMsgThreadId)
		return;

	//if this is a new thread, set a new thread id
	if (%index == 0)
	{
		$AIMsgThreadId++;
		$AIMsgThreadActive = true;
		%threadId = $AIMsgThreadId;
	}
	switch$ (%msg)
	{
		//this is an example of how to use the chat system without using the table...
		case "ChatHi":
			serverCmdCannedChat(%clSpeaker, 'ChatHi', true);
			%responsePending = false;
			if (%index == 0)
			{
				if (%clListener < 0)
					%clListener = AIFindCommanderAI(%clSpeaker);
				if (%clListener > 0 && (getRandom() < 0.5))
				{
					%responsePending = true;
               schedule(1000, 0, "AIMessageThread", %msg, %clListener, 0, false, 1, %threadId);
				}
			}
			if (! %responsePending)
				schedule(1000, 0, "AIEndMessageThread", $AIMsgThreadId);

		//this method of using the chat system sets up a table instead...
		//the equivalent is commented out below - same effect - table is much better.
		case "ChatSelfDefendGenerator":
			if (%index == 0)
			{
				//initialize the table
				$AIMsgThreadIndex = 0;
				$AIMsgThreadTable[1] = "";

				%commander = AIFindCommanderAI(%clSpeaker);
				if (%commander > 0)
				{
					$AIMsgThreadTable[0] = %commander @ " ChatCmdDefendBase " @ 1000;
					$AIMsgThreadTable[1] = %clSpeaker @ " ChatCmdAcknowledged " @ 1250;
					$AIMsgThreadTable[2] = %clSpeaker @ " " @ %msg @ " " @ 1000;
					$AIMsgThreadTable[3] = "";
				}
				else
					$AIMsgThreadTable[0] = %clSpeaker @ " " @ %msg @ " " @ 1000;

				//now process the table
				AIProcessMessageTable(%threadId);
			}
		
//		case "ChatSelfDefendGenerator":
//			%responsePending = false;
//			if (%index == 0)
//			{
//				//find the commander
//				%commander = AIFindCommanderAI(%clSpeaker);
//				if (%commander > 0)
//				{
//					%responsePending = true;
//					serverCmdCannedChat(%commander, "ChatCmdDefendBase", true);
//					schedule("AIMessageThread(" @ %msg @ ", " @ %clSpeaker @ ", 0, 1, " @ %type @ ", false, " @ %threadId @ ");", 1000);
//				}
//				else
//					serverCmdCannedChat(%commander, "ChatSelfDefendGenerator", true);
//			}
//			else if (%index == 1)
//			{
//				//make sure the client is still alive
//				if (AIClientIsAlive(%clSpeaker, 1000))
//				{
//					%responsePending = true;
//					serverCmdCannedChat(%clSpeaker, "ChatCmdAcknowledged", true);
//					schedule("AIMessageThread(" @ %msg @ ", " @ %clSpeaker @ ", 0, 2, " @ %type @ ", false, " @ %threadId @ ");", 1000);
//				}
//				else
//					AIEndMessageThread($AIMsgThreadId);
//			}
//			else if (%index == 2)
//			{
//				//make sure the client is still alive
//				if (AIClientIsAlive(%clSpeaker, 1000))
//					serverCmdCannedChat(%clSpeaker, "ChatSelfDefendGenerator", true);
//				else
//					AIEndMessageThread($AIMsgThreadId);
//			}
//			if (! %responsePending)
//				schedule("AIEndMessageThread(" @ $AIMsgThreadId @ ");", 1000);
			
		default:
         %tag = addTaggedString( %msg );
			serverCmdCannedChat( %clSpeaker, %tag, true );
         removeTaggedString( %tag );   // Don't keep incrementing the string ref count...
			schedule( 1500, 0, "AIEndMessageThread", $AIMsgThreadId );
	}
}