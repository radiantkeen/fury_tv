// AI Vehicle Defs

// These define what logic set to use for the vehicle AI
$AIVehicleMainTask::None = 0;
$AIVehicleMainTask::Attack = 1;
$AIVehicleMainTask::Escort = 2;
$AIVehicleMainTask::SkyPriest = 3;
$AIVehicleMainTask::Objective = 4;
$AIVehicleMainTask::Support = 5;
