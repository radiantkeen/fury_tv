//-----------------------------------//
//        AI SCRIPT FUNCTIONS        //
//-----------------------------------//
// Modified for FuryTV by Keen

//first, exec the supporting scripts

exec("scripts/AI/aiNV_DefaultTasks.cs");
exec("scripts/AI/aiNV_Objectives.cs");
exec("scripts/AI/aiNV_Inventory.cs");
exec("scripts/AI/aiNV_Chat.cs");
exec("scripts/AI/aiNV_ObjectiveBuilder.cs");
exec("scripts/AI/NV_serverTasks.cs");

$AIModeStop = 0;
$AIModeWalk = 1;
$AIModeGainHeight = 2;
$AIModeExpress = 3;
$AIModeMountVehicle = 4;

$AIClientLOSTimeout = 15000;	//how long a client has to remain out of sight of the bot
										//before the bot "can't see" the client anymore...
$AIClientMinLOSTime = 10000;	//how long a bot will search for a client


//-----------------------------------//
//Objective weights - level 1

$AIWeightCapFlag[1]           = 5000;	//range 5100 to 5320
$AIWeightKillFlagCarrier[1]   = 4800;	//range 4800 to 5120
$AIWeightReturnFlag[1]        = 5001;	//range 5101 to 5321
$AIWeightDefendFlag[1]        = 3900;	//range 4000 to 4220
$AIWeightGrabFlag[1]          = 3850;	//range 3950 to 4170

$AIWeightDefendFlipFlop[1]    = 3900;  //range 4000 to 4220
$AIWeightCaptureFlipFlop[1]   = 3850;  //range 3850 to 4170

$AIWeightAttackGenerator[1]   = 3100;	//range 3200 to 3520 
$AIWeightRepairGenerator[1]   = 3200;	//range 3300 to 3620
$AIWeightDefendGenerator[1]   = 3100;	//range 3200 to 3420

$AIWeightMortarTurret[1]      = 3400;	//range 3500 to 3600
$AIWeightMissileTurret[1]     = 3300;	//range 3400 to 3500 added - Lagg...
$AIWeightLazeObject[1]        = 3200;  //range 3300 to 3400
$AIWeightRepairTurret[1]      = 3100;  //range 3200 to 3420

$AIWeightAttackInventory[1]   = 3100;	//range 2800 to 2920//was 2900 - Lagg...
$AIWeightRepairInventory[1]   = 3050;	//range 2800 to 2920//was 2900 - Lagg...

$AIWeightEscortOffense[1]     = 3100;  //range 2800 to 2920//was 2900 - Lagg...
$AIWeightEscortCapper[1]      = 3250;	//range 3350 to 3470

//used to allow a bot to finish tasks once started.
$AIWeightContinueDeploying		= 4250;
$AIWeightContinueRepairing		= 4250;

//Objective weights from human
$AIWeightHumanIssuedCommand		= 5050;//was 4450 - Lagg...
$AIWeightHumanIssuedEscort			= 5025;//was 4425 - Lagg...

//Objective weights - level 2
$AIWeightCapFlag[2]           =    0;	//only one person can ever cap a flag
$AIWeightKillFlagCarrier[2]   = 4800;	//range 4800 to 5020
$AIWeightReturnFlag[2]        = 4100;	//range 4200 to 4320
$AIWeightDefendFlag[2]        = 2000;	//range 2100 to 2220
$AIWeightGrabFlag[2]          = 2000;	//range 2100 to 2220

$AIWeightDefendFlipFlop[2]		= 2000;  //range 2100 to 2220
$AIWeightDefendFlipFlop[3]		= 1500;  //range 1600 to 1720
$AIWeightDefendFlipFlop[4]		= 1000;  //range 1100 to 1220

$AIWeightAttackGenerator[2]   = 1600;	//range 1700 to 1920 
$AIWeightRepairGenerator[2]   = 1600;	//range 1700 to 1920
$AIWeightDefendGenerator[2]   = 1500;	//range 1600 to 1720

$AIWeightAttackInventory[2]   = 1400;	//range 1500 to 1720 
$AIWeightRepairInventory[2]   = 1400;	//range 1500 to 1720

$AIWeightMortarTurret[2]      =    0;	//range 1100 to 1320//was 1000 - Lagg...
$AIWeightLazeObject[2]        =    0;  //no need to have more than one targetter
$AIWeightRepairTurret[2]      = 1000;  //range 1100 to 1320

$AIWeightEscortOffense[2]     = 2900;  //range 3300 to 3420 
$AIWeightEscortCapper[2]      = 3000;	//range 3100 to 3220


//This function is designed to clear out the objective Q's, and clear the task lists from all the AIs
//added a few tricks to the ai so they must be dealt with here also - Lagg...

function AIMissionEnd()
{
	//disable the AI system
	AISystemEnabled(false);

	//loop through the client list, and clear the tasks of each bot
	%count = ClientGroup.getCount();
	for (%i = 0; %i < %count; %i++)
	{
		%client = ClientGroup.getObject(%i);
		if (%client.isAIControlled())
		{
			//cancel the respawn thread and the objective thread...
			cancel(%client.respawnThread);
			cancel(%client.objectiveThread);

         //reset the clients tasks, variables, etc...
	      AIUnassignClient(%client);
         %client.stop();
			%client.clearTasks();
         %client.clearStep();
         %client.lastDamageClient = -1;
         %client.lastDamageTurret = -1;
         %client.shouldEngage = -1;
         %client.setEngageTarget(-1);
         %client.setTargetObject(-1);
         %client.engageRemeq = -1;//added - Lagg...
         %client.pickUpItem = -1;//added - Lagg...
	      %client.pilotVehicle = false;
         %client.defaultTasksAdded = false;

	      //do the nav graph cleanup
	      %client.missionCycleCleanup();
		}
	}

	//clear the objective Q's
	for (%i = 0; %i <= Game.numTeams; %i++)
	{
		if (isObject($ObjectiveQ[%i]))
		{
			$ObjectiveQ[%i].clear();
			$ObjectiveQ[%i].delete();
		}
		$ObjectiveQ[%i] = "";
	}

	//now delete all the sets used by the AI system...
	if (isObject($AIBombLocationSet))
   	$AIBombLocationSet.delete();
	$AIBombLocationSet = "";

	if (isObject($AIInvStationSet))
   	$AIInvStationSet.delete();
	$AIInvStationSet = "";

	if (isObject($AIItemSet))
   	$AIItemSet.delete();
	$AIItemSet = "";

	if (isObject($AIGrenadeSet))
   	$AIGrenadeSet.delete();
	$AIGrenadeSet = "";

	if (isObject($AIWeaponSet))
   	$AIWeaponSet.delete();
	$AIWeaponSet = "";

	if (isObject($AIRemoteTurretSet))
   	$AIRemoteTurretSet.delete();
	$AIRemoteTurretSet = "";

	if (isObject($AIDeployedMineSet))
   	$AIDeployedMineSet.delete();
	$AIDeployedMineSet = "";

	if (isObject($AIVehicleSet))
   	$AIVehicleSet.delete();
	$AIVehicleSet = "";
}

//FUNCTIONS ON EACH OBJECT EXECUTED AT MISSION LOAD TIME

//function AIConnection::onAIConnect(%client, %name, %team, %skill, %offense, %voice, %voicePitch)
// above function modified in AI.cs to protect random name/skill level generation

function onAIRespawn(%client)
{
   %markerObj = Game.pickPlayerSpawn(%client, true);
   Game.createPlayer(%client, %markerObj);	

	//make sure the player object is the AI's control object - even during the mission warmup time
	//the function AISystemEnabled(true/false) will control whether they actually move...
	%client.setControlObject(%client.player);
   
   if (%client.objective)
      error("ERROR!!! " @ %client @ " is still assigned to objective: " @ %client.objective);
      
   //clear the objective and choose a new one
	AIUnassignClient(%client);
   %client.stop();
   %client.clearStep();
   %client.lastDamageClient = -1;
   %client.lastDamageTurret = -1;
   %client.shouldEngage = -1;
   %client.setEngageTarget(-1);
   %client.setTargetObject(-1);
   %client.engageRemeq = -1;//added - Lagg...
   %client.pickUpItem = -1;//added - Lagg...
	%client.pilotVehicle = false;

	//set the spawn time
	%client.spawnTime = getSimTime();
	%client.respawnThread = "";

	//timeslice the objective reassessment for the bots
	if (!isEventPending(%client.objectiveThread))
	{
		%curTime = getSimTime();
		%remainder = %curTime % 5000;
		%schedTime = $AITimeSliceReassess - %remainder;
		if (%schedTime <= 0)
			%schedTime += 5000;
		%client.objectiveThread = schedule(%schedTime, %client, "AIReassessObjective", %client);

		//set the next time slice "slot"
		$AITimeSliceReassess += 300;
		if ($AITimeSliceReassess > 5000)
			$AITimeSliceReassess -= 5000;
	}

	//call the game specific spawn function
	Game.onAIRespawn(%client);
}

//-----------------------------------------------------------------------------
//AI VEHICLE FUNCTIONS

//lets boost the vehicle turret skill if ai mounted :) - Lagg...
function Armor::AIonMount(%this, %obj, %vehicle, %node)
{
	//set the client var...
	%client = %obj.client;
	%client.turretMounted = -1;

	//make sure the AI was *supposed* to mount the vehicle
//	if (!%client.isMountingVehicle()) // keen - disabled because this caused AI not to pilot
//	{
//		AIDisembarkVehicle(%client);
//		return;
//	}

	//get the vehicle's pilot
   %pilot = %vehicle.getMountNodeObject(0);

	//make sure the bot is in node 0 if'f the bot is piloting the vehicle
	if ((%node == 0 && !%client.pilotVehicle) || (%node > 0 && %client.pilotVehicle))
	{
		AIDisembarkVehicle(%client);
		return;
	}

	//make sure the bot didn't is on the same team as the pilot
	if (%pilot > 0 && isObject(%pilot) && %pilot.client.team != %client.team)
	{
		AIDisembarkVehicle(%client);
		return;
	}

	//if we're supposed to pilot the vehicle, set the control object
	if (%client.pilotVehicle)
		%client.setControlObject(%vehicle);

	//each vehicle may be built differently...
   if (%vehicle.vehicleCheckType($VehicleList::Tanks))
   {
      //node 1 is this vehicle's turret seat
      if (%node == 1)
      {
         %turret = %vehicle.getMountNodeObject(10);
         //%skill = %client.getSkillLevel();
         %skill = 2.0; //that should do it :) - Lagg...
         %turret.setSkill(%skill);
			%client.turretMounted = %turret;
			%turret.setAutoFire(true);
      }
	}

   else if (%vehicle.vehicleCheckType($VehicleList::Gunships))
   {
      //node 1 is this vehicle's turret seat
      if (%node == 1)
      {
         %turret = %vehicle.getMountNodeObject(10);
         //%skill = %client.getSkillLevel();
         %skill = 2.0; //that should do it :) - Lagg...
         %turret.setSkill(%skill);
			%client.turretMounted = %turret;
			%client.setTurretMounted(%turret);
			%turret.setAutoFire(true);
      }
	}
}

execDir("LaggAI");
