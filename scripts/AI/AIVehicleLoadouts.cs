// Vehicle Loadouts

// Skycutter
$AIShipLoadoutCount = 0;
$AIShipLoadout[$VehicleID::Skycutter, $AIShipLoadoutCount, "Name"] = "Chaser";

$AIShipPart[$VehicleID::Skycutter, $AIShipLoadoutCount, $VehiclePartType::Armor] = "VArmorNone";
$AIShipPart[$VehicleID::Skycutter, $AIShipLoadoutCount, $VehiclePartType::Module] = "VModuleNone";
$AIShipPart[$VehicleID::Skycutter, $AIShipLoadoutCount, $VehiclePartType::Shield] = "VShieldNone";
$AIShipPart[$VehicleID::Skycutter, $AIShipLoadoutCount, $VehiclePartType::Weapon1] = "GVulcan";
$AIShipLoadoutCount++;

$AIShipLoadouts[$VehicleID::Skycutter] = $AIVehLoadoutCount;
