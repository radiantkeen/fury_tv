//All tasks for deathmatch, hunters, and tasks that coincide with the current objective task live here...

//Weights for tasks that override the objective task: must be between 4300 and 4700
$AIWeightVehicleMountedEscort	= 6000;//was 4700 - Lagg...

$AIWeightDetectRemeq				= 3500;//added - Lagg...
$AIWeightDetectVehicule				= 2700;//added - Lagg...

//------------------------------------------------------------------------------------------------------------
//modified pickupitem task to include repairpack as a health item and fixed some other bugs - Lagg... 4-3-2003
//------------------------------------------------------------------------------------------------------------
//AIPickupItemTask is responsible for anything to do with picking up an item

function AIPickupItemTask::init(%task, %client)
{
}

function AIPickupItemTask::assume(%task, %client)
{
	%task.setWeightFreq(10);
	%task.setMonitorFreq(10);

	%task.pickupItem = -1;
        %client.pickupItem = -1;
}

function AIPickupItemTask::retire(%task, %client)
{
        %task.pickupItem = -1;
        %client.pickupItem = -1;
}

function AIPickupItemTask::weight(%task, %client)
{
	//if we're already picking up an item, make sure it's still valid, then keep the weight the same...
	if (%task.pickupItem > 0 && %client.pickupItem > 0)
	{
		if (isObject(%task.pickupItem) && !%task.pickupItem.isHidden())
			return;
		else
                {
			%task.pickupItem = -1;
                        %client.pickupItem = -1;
                }
	}

	//otherwise, search for objects
        //first, see if we can pick up health
	%player = %client.player;
	if (!isObject(%player))
		return;

	%damage = %player.getDamagePercent();
	%healthRad = %damage * 100;
	%closestHealth = -1;
	%closestHealthDist = %healthRad;
	%closestHealthLOS = false;
	%closestItem = -1;
	%closestItemDist = 32767;
	%closestItemLOS = false;

	//loop through the item list, looking for things to pick up
	%itemCount = $AIItemSet.getCount();
	for (%i = 0; %i < %itemCount; %i++)
	{
		%item = $AIItemSet.getObject(%i);
		if (!%item.isHidden())
		{
                    %dist = %client.getPathDistance(%item.getWorldBoxCenter());
		    if (((%item.getDataBlock().getName() $= "RepairPack" && %player.getInventory("RepairPack") <= 0) ||
                           (%item.isCorpse && %item.getInventory("RepairKit") > 0) ||
                           %item.getDataBlock().getName() $= "RepairPatch" ||
                           %item.getDataBlock().getName() $= "RepairKit") &&
                           %player.getInventory("RepairKit") <= 0 && %damage > 0.3)
 		    {
		        if (%dist > 0 && %dist < %closestHealthDist)
		    	{
			    %closestHealth = %item;
			    %closestHealthDist = %dist;

			    //check for LOS
			    %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType;
			    %closestHealthLOS = !containerRayCast(%client.player.getWorldBoxCenter(), %item.getWorldBoxCenter(), %mask, 0);
			}
		    }
		    else
		    {
		        //only pick up stuff within 35m
			if (%dist < 45)
			{
			    if (AICouldUseItem(%client, %item))
			    {
		                if (%dist < %closestItemDist)
				{
				    %closestItem = %item;
				    %closestItemDist = %dist;

				    //check for LOS
				    %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType;
				    %closestItemLOS = !containerRayCast(%client.player.getWorldBoxCenter(), %item.getWorldBoxCenter(), %mask, 0);
				}
			    }
			}
		    }
		}
	}

	//now set the weight
	if (%closestHealth > 0)
	{
	    //only choose an item if it's at least 25 m closer than health...
	    //and we're not engageing someone or not that badly in need
	    %currentTarget = %client.getEngageTarget();
	    if (%closestItem > 0 && %closetItemDist < %closestHealthDist - 25 && (%damage < 0.6 || %currentTarget <= 0) && %closestItemLOS)
            {
	        %task.pickupItem = %closestItem;
                %client.pickupItem = %closestItem;
                if (AIEngageWeaponRating(%client) < 20)                    
	            %task.setWeight($AIWeightNeedItemBadly);
		else if (%closestItemDist < 10 && %closestItemLOS)
		    %task.setWeight($AIWeightNeedItem);
                else if (%closestItemLOS)
                    %task.setWeight($AIWeightFoundItem);
                else
		    %task.setWeight(0);
	    }
	    else
	    {
		if (%damage > 0.8)
		{
		    %task.pickupItem = %closestHealth;
                    %client.pickupItem = %closestHealth;
		    %task.setWeight($AIWeightNeedItemBadly + 50);
		}
		else if (%closestHealthLOS)
		{
		    %task.pickupItem = %closestHealth;
                    %client.pickupItem = %closestHealth;
		    %task.setWeight($AIWeightNeedItem + 50);
		}
		else
		    %task.setWeight(0);
		}
	}
	else if (%closestItem > 0)
	{
		%task.pickupItem = %closestItem;
                if (AIEngageWeaponRating(%client) < 20)
		    %task.setWeight($AIWeightNeedItemBadly);
		else if (%closestItemDist < 10 && %closestItemLOS)
		    %task.setWeight($AIWeightNeedItem);
		else if (%closestItemLOS)
		    %task.setWeight($AIWeightFoundItem);
		else
		    %task.setWeight(0);
	}
	else
		%task.setWeight(0);
}

function AIPickupItemTask::monitor(%task, %client)
{
	//move to the pickup location
	if (isObject(%task.pickupItem))
        {
           if (%task.pickupItem.getDataBlock().getName() $= "RepairPack")
           {
               %distToRep = %client.getPathDistance(%task.pickupItem.position);
	       if (%distToRep < 10 && %client.player.getMountedImage($BackpackSlot) > 0 && %client.player.getInventory("RepairPack") <= 0)
               {
	           %client.player.throwPack();
               }
           }
	   %client.stepMove(%task.pickupItem.getWorldBoxCenter(), 0.25);
        }

	//this call works in conjunction with AIEngageTask
	%client.setEngageTarget(%client.shouldEngage);
}

//-------------------------------------------------------------------------------------------------
//modified slightly -                                                            Lagg... - 4-4-2003
//AITauntCorpseTask is should happen only after an enemy is freshly killed

function AITauntCorpseTask::init(%task, %client)
{
}

function AITauntCorpseTask::assume(%task, %client)
{
	%task.setWeightFreq(11);
	%task.setMonitorFreq(11);
}

function AITauntCorpseTask::retire(%task, %client)
{
}

function AITauntCorpseTask::weight(%task, %client)
{
   %task.corpse = %client.getVictimCorpse();
   if (%task.corpse > 0 && getSimTime() - %client.getVictimTime() < 7500)
	{
		//see if we're already taunting, and if it's time to stop
		if ((%task.tauntTime > %client.getVictimTime()) && (getSimTime() - %task.tauntTime > 1000))
			%task.corpse = -1;
		else
		{
			//if the corpse is within 25m, taunt
         %distToCorpse = %client.getPathDistance(%task.corpse.getWorldBoxCenter());
			if (%dist < 0 || %distToCorpse > 25)
				%task.corpse = -1;
		}
	}
	else
		%task.corpse = -1;

	//set the weight
	if (%task.corpse > 0)
	{
		//don't taunt someone if there's an enemy right near by...
		%result = AIFindClosestEnemy(%client, 40, 15000);
      %closestEnemy = getWord(%result, 0);
      %closestdist = getWord(%result, 1);
      if (%closestEnemy > 0)
			%task.setWeight(0);
		else
			%task.setWeight($AIWeightTauntVictim);
	}
	else
		%task.setWeight(0);
}

$AITauntChat[0] = "gbl.aww";
$AITauntChat[1] = "gbl.brag";
$AITauntChat[2] = "gbl.obnoxious";
$AITauntChat[3] = "gbl.sarcasm";
$AITauntChat[4] = "gbl.when";
function AITauntCorpseTask::monitor(%task, %client)
{
	//make sure we still have a corpse, and are not fighting anyone
	if (%client.getEngageTarget() <= 0 && %task.corpse > 0 && isObject(%task.corpse))
	{
      %clientPos = %client.player.getWorldBoxCenter();
		%corpsePos = %task.corpse.getWorldBoxCenter();
		%distToCorpse = VectorDist(%clientPos, %corpsePos);
		if (%distToCorpse < 2.0)
		{
			//start the taunt!
			if (%task.tauntTime < %client.getVictimTime())
			{
				%task.tauntTime = getSimTime();
				%client.stop();

				if (getRandom() > 0.2)
				{
					//pick the sound and taunt cels
					%sound = $AITauntChat[mFloor(getRandom() * 3.99)];
					%minCel = 2;
					%maxCel = 8;
				   schedule(250, %client, "AIPlayAnimSound", %client, %corpsePos, %sound, %minCel, %maxCel, 0);
				}
				//say 'bye'  :)
				else
				   schedule(250, %client, "AIPlayAnimSound", %client, %corpsePos, "gbl.bye", 2, 2, 0);
			}
		}
		else
			%client.stepMove(%task.corpse.getWorldBoxCenter(), 1.75);
	}
}



//-----------------------------------------------------------------------------
//AIAvoidMineTask is responsible for detecting/destroying enemy mines...
//modified so bots only shoot at mines that they can see (sometimes opps!) - Lagg...

function AIDetectMineTask::init(%task, %client)
{
}

function AIDetectMineTask::assume(%task, %client)
{
	%task.setWeightFreq(7);
	%task.setMonitorFreq(7);
}

function AIDetectMineTask::retire(%task, %client)
{
	%task.engageMine = -1;
	%task.attackInitted = false;
   %client.setTargetObject(-1);
}

function AIDetectMineTask::weight(%task, %client)
{
	//crappy hack, but they need the proper weapon before they can destroy a mine...
	%player = %client.player;
	if (!isObject(%player))
		return;

   %hasPlasma = (%player.getInventory("Plasma") > 0) && (%player.getInventory("PlasmaAmmo") > 0);
   %hasDisc = (%player.getInventory("Disc") > 0) && (%player.getInventory("DiscAmmo") > 0);

	if (!%hasPlasma && !%hasDisc)
	{
		%task.setWeight(0);
		return;
	}

	//if we're already attacking a mine, 
	if (%task.engageMine > 0 && isObject(%task.engageMine))
	{
		%task.setWeight($AIWeightDetectMine);
		return;
	}
	//see if we're within the viscinity of a new (enemy) mine
	%task.engageMine = -1;
	%closestMine = -1;
	%closestDist = 15;	//initialize so only mines within 15 m will be detected...
	%mineCount = $AIDeployedMineSet.getCount();
	for (%i = 0; %i < %mineCount; %i++)
	{
		%mine = $AIDeployedMineSet.getObject(%i);
		%mineTeam = %mine.sourceObject.team;

		%minePos = %mine.getWorldBoxCenter();
		%clPos = %client.player.getWorldBoxCenter();

		//see if the mine is the closest...
		%mineDist = VectorDist(%minePos, %clPos);
		if (%mineDist < %closestDist)
		{
			//now see if we're more or less heading towards the mine...
			%clVelocity = %client.player.getVelocity();
			%clVelocity = getWord(%clVelocity, 0) SPC getWord(%clVelocity, 1) SPC "0";
			%mineVector = VectorSub(%minePos, %clPos);
			%mineVector = getWord(%mineVector, 0) SPC getWord(%mineVector, 1) SPC "0";
			if (VectorLen(%clVelocity) > 2.0 && VectorLen(%mineVector > 2.0))
			{
				%clNormal = VectorNormalize(%clVelocity);
				%mineNormal = VectorNormalize(%mineVector);
				if (VectorDot(%clNormal, %mineNormal) > 0.3)
				{
					%closestMine = %mine;
					%closestDist = %mineDist;
				}
			}
		}
	}

        //check for LOS
        if (isObject(%closestMine))
        {
	   %mask = $TypeMasks::TerrainObjectType |
             $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
	   %mineLOS = !containerRayCast(%player.getWorldBoxCenter(), %closestMine.getWorldBoxCenter(), %mask, 0);
        }

	//see if we found a mine to attack
	if (%closestMine > 0 && %mineLOS)
	{
		%task.engageMine = %closestMine;
		%task.attackInitted = false;
		%task.setWeight($AIWeightDetectMine);
	}
	else
		%task.setWeight(0);
}

function AIDetectMineTask::monitor(%task, %client)
{
	if (%task.engageMine > 0 && isObject(%task.engageMine))
	{
		if (!%task.attackInitted)
		{
		   %task.attackInitted = true;
		   %client.stepRangeObject(%task.engageMine, "DefaultRepairBeam", 6, 12);
		   %client.setEngageTarget(-1);
		   %client.setTargetObject(-1);
		}

      else if (%client.getStepStatus() $= "Finished")
		{
		   //%client.stop();
                   %client.setTargetObject(%task.engageMine);
                   %client.setDangerLocation(%task.engageMine.position, 15);
		}
	}
}

//------------------------------------------------------------------------------------
//New AIDefault Task                       ------                    Lagg... 3-20-2003
//AIDetectRemeqTask is responsible for finding and killing enemy remote inventories...
//------------------------------------------------------------------------------------

function AIDetectRemeqTask::init(%task, %client)
{

}
function AIDetectRemeqTask::assume(%task, %client)
{
	%task.setWeightFreq(7);
	%task.setMonitorFreq(7);
}

function AIDetectRemeqTask::retire(%task, %client)
{
	%task.engageRemeq = -1;
        %task.attackInitted = false;
	%client.setTargetObject(-1);
        %client.engageRemeq = -1;
}

function AIDetectRemeqTask::weight(%task, %client)
{
        %player = %client.player;
	if (!isObject(%player))
		return;

        //see if we're already attacking a remote inventory
        if (%task.engageRemeq > 0 && isObject(%task.engageRemeq) && %client.engageRemeq > 0)
        {
	   %task.setWeight($AIWeightDetectRemeq);
           return;
	}
        //see if we are still alive
        else if (!AIClientIsAlive(%client))
        {
           %task.engageRemeq = -1;
           %client.engageRemeq = -1;
	   //%client.setEngagetarget(-1);
        }
	else
	{
           //lets find some remote equipment (inventories)...
           %task.engageRemeq = -1;
	   %closestRemeq = -1;
	   %closestDist = 100;	//initialize so only remote invetoties within 100 m will be detected
           %depCount = 0;
           %depGroup = nameToID("MissionCleanup/Deployables");
           if (isObject(%depGroup))
           {	      
              %depCount = %depGroup.getCount();
	      for (%i = 0; %i < %depCount; %i++)
	      {
	         %obj = %depGroup.getObject(%i);
	         if (%obj.getDataBlock().getName() $= "DeployedStationInventory" && %obj.team != %client.team && %obj.isEnabled())
                 {
                    %remeqPos = %obj.getWorldBoxCenter();
		    %clPos = %client.player.getWorldBoxCenter();
                    %remeqDist = VectorDist(%remeqPos, %clPos);
		    if (%remeqDist < %closestDist)
                    {
                       //check for LOS
		       %mask = $TypeMasks::TerrainObjectType |
                              $TypeMasks::InteriorObjectType | $TypeMasks::StaticTSObjectType;
		       %remeqLOS = !containerRayCast(%client.player.getWorldBoxCenter(), %obj.getWorldBoxCenter(), %mask, 0);
                       if (%remeqLOS)
                       {
                          %task.engageRemeq = %obj;
                          %client.engageRemeq = %obj;
                       }
                       else
                       {
                          %task.engageRemeq = -1;
                          %client.engageRemeq = -1;
                          %task.setWeight(0);
                       }
                    }
                 }
              }              
           }                      
        }
}

function AIDetectRemeqTask::monitor(%task, %client)
{

        if (%task.engageRemeq > 0 && isObject(%task.engageRemeq) && AIClientIsAlive(%client) && %client.engageRemeq > 0)
	{
                %hasMortar = (%client.player.getInventory("Mortar") > 0) && (%client.player.getInventory("MortarAmmo") > 0);
		//set the AI to fire at the remote inventory
                //also made adjustments to Mortar function in aiinventory.cs
                if (%hasMortar)
		   %client.setTargetObject(%task.engageRemeq, 300, "Mortar");
                else
                   %client.setTargetObject(%task.engageRemeq, 100, "Destroy");

		//control the movement - shoot first ask questions later
		if (!%task.attackInitted)
		{
			%task.attackInitted = true;
                        %client.setEngageTarget(-1);
                        if (%hasMortar)
                           %client.stepRangeObject(%task.engageRemeq, "BasicTargeter", 8, 80);
                        else
                           %client.stepMove(%task.engageRemeq.getWorldBoxCenter(), 10.0);
		}  
	}
        //else we are done
        else
        {
            %task.engageRemeq = -1;
            %client.engageRemeq = -1;
	    %task.attackInitted = false;
            %client.setTargetObject(-1);
            %task.setWeight(0);
        }              
}

//------------------------------------------------------------------------------------
//New AIDefault Task                       ------                    Lagg... 3-20-2003
//AIDetectVehiculeTask is for detecting / destroying enemy vehicles manned or unmanned
//------------------------------------------------------------------------------------

function AIDetectVehiculeTask::init(%task, %client)
{

}

function AIDetectVehiculeTask::assume(%task, %client)
{
	    %task.setWeightFreq(15);
	    %task.setMonitorFreq(7);
}

function AIDetectVehiculeTask::retire(%task, %client)
{
	    %task.engageVehicule = -1;
	    %task.attackInitted = false;
            %client.setTargetObject(-1);        
}

function AIDetectVehiculeTask::weight(%task, %client)
{
	%player = %client.player;
	if (!isObject(%player))
		return;

        //only do this task at higher skill levels :) - Lagg...
        %skill = %client.getSkillLevel();
        if (%skill <= 0.7)
        {
           %task.setWeight(0);
           return;
        }

        //if we're already attacking a mine, 
	if (%task.engageVehicule > 0 && isObject(%task.engageVehicule))
	{
		%task.setWeight($AIWeightDetectRemeq);
		//return;
	}
	//see if we're within the viscinity of a new (enemy) vehicle
	%task.engageVehicule = -1;
	%closestVehicule = -1;
	%closestDist = 300;	//initialize so only vehicles within 300 m will be detected...
	%vehiculeCount = $AIVehicleSet.getCount();
	for (%i = 0; %i < %vehiculeCount; %i++)
	{
		%vehicule = $AIVehicleSet.getObject(%i);
                if (! %vehicule)//added to stop consol errors - Lagg... 2-18-2003
                {
                   echo("OPPS! Not a vehicle");
                   %task.setWeight(0);
                   return;
                }
		if(%vehicule.isEnabled() && %vehicule.team != %client.team)
                {
                            %vehiculePos = %vehicule.getWorldBoxCenter();
		            %clPos = %client.player.getWorldBoxCenter();
                            //check for LOS
			    %mask = $TypeMasks::TerrainObjectType | 
                                    $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
			    %vehiculeLOS = !containerRayCast(%client.player.getWorldBoxCenter(), %vehicule.getWorldBoxCenter(), %mask, 0);
                            //see if the vehicle is the closest...
		            %vehiculeDist = VectorDist(%vehiculePos, %clPos);
		            if (%vehiculeDist < %closestDist && %vehiculeLOS)
                            {
   		                %closestVehicule = %vehicule;
                                %closestDist = %vehiculeDist;
		            }
	          
                }
	            //see if we found a vehicle to attack
	            if (%closestVehicule > 0)
	            {
                            %task.engageVehicule = %closestVehicule;
		            %task.attackInitted = false;
		            %task.setWeight($AIWeightDetectRemeq);
                    }
	            else
                            %task.setWeight(0);
    }
}

function AIDetectVehiculeTask::monitor(%task, %client)
{

        %player = %client.player;
	if (!isObject(%player) || !isObject(%task.engageVehicule))
		return;

        //check for LOS
	%mask = $TypeMasks::TerrainObjectType |
                 $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
	%vehiculeLOS = !containerRayCast(%player.getWorldBoxCenter(), %task.engageVehicule.getWorldBoxCenter(), %mask, 0);

	if (%task.engageVehicule > 0 && isObject(%task.engageVehicule) && %vehiculeLOS)
	{
                %hasMortar = (%player.getInventory("Mortar") > 0) && (%player.getInventory("MortarAmmo") > 0);
                %hasMissile = (%player.getInventory("MissileLauncher") > 0) && (%player.getInventory("MissileLauncherAmmo") > 0);
        
                if (!%task.attackInitted)
		{
			%task.attackInitted = true;
                        if (%hasMissile || %hasMortar)
                           %client.stepRangeObject(%task.engageVehicule, "BasicTargeter", 80, 300);
                        else
                           %client.stepMove(%task.engageVehicule.getWorldBoxCenter(), 40.0);
                        //echo("-----------Attack inittiated step move to enemy vehicle ----------");
                        
			//%client.setEngageTarget(-1);
			%client.setTargetObject(-1);
		}

                if (%client.getStepStatus() $= "Finished")
		{
                        //%client.stop();
                        //echo("-----------We are killing a enemy vehicle ----------");
                        //%vehiculePos = %vehicule.getWorldBoxCenter();
		                //%clPos = %client.player.getWorldBoxCenter();
                        if (vectorDist(%client.player.getWorldBoxCenter(), %task.engageVehicule.getWorldBoxCenter()) > 40 && %hasMissile)
                           %client.setTargetObject(%task.engageVehicule, 300, "Missile");
                        else
                           %client.setTargetObject(%task.engageVehicule, 300, "Mortar");
                }
	}
        else
        {
            %task.engageVehicule = -1;
	    %task.attackInitted = false;
            %client.setTargetObject(-1);
            %task.setWeight(0);
        }
}

//-----------------------------------------------------------------------------
//AICouldUseInventoryTask will cause them to use an inv station if they're low
//on ammo.  This is a new version of AIUseInventory task for CTF      - Lagg...

function AICouldUseInventoryTask::init(%task, %client)
{
}

function AICouldUseInventoryTask::assume(%task, %client)
{
	%task.setWeightFreq(15);
	%task.setMonitorFreq(5);

	//mark the current time for the buy inventory state machine
	%task.buyInvTime = getSimTime();
}

function AICouldUseInventoryTask::retire(%task, %client)
{
	//reset the state machine time stamp...
	%task.buyInvTime = getSimTime();
}

function AICouldUseInventoryTask::weight(%task, %client)
{
   //first, see if we can pick up health
	%player = %client.player;
	if (!isObject(%player))
		return;

        //if we are picking up an item or health forget it - Lagg...
        if (%client.pickUpItem > 0)
                return;

	%damage = %player.getDamagePercent();
	%weaponry = AIEngageWeaponRating(%client);

   //if there's an inv station, and we haven't used an inv station since we
   //spawned, the bot should use an inv once regardless
   if (%client.spawnUseInv)
   {
      //see if we're already heading there
      if (%client.buyInvTime != %task.buyInvTime)
      {
	      //see if there's an inventory we can use
         %result = AIFindClosestInventory(%client, false);
         %closestInv = getWord(%result, 0);
	      if (isObject(%closestInv))
         {
            %task.setWeight($AIWeightNeedItem);
            return;
         }
         else
            %client.spawnUseInv = false;
      }
      else
      {
         %task.setWeight($AIWeightNeedItem);
         return;
      }
   }

	//first, see if we need equipment or health
	if (%damage < 0.3 && %weaponry >= 40)
	{
		%task.setWeight(0);
		return;
	}

	//don't use inv stations if we're not that badly damaged, and we're in the middle of a fight
	if (%damage < 0.6 && %client.getEngageTarget() > 0 && !AIEngageOutOfAmmo(%client))
	{
		%task.setWeight(0);
		return;
	}

	//if we're already buying, continue
	if (%task.buyInvTime == %client.buyInvTime)
	{
		//set the weight - if our damage is above 0.8 or our we're out of ammo
		if (%damage > 0.8 || AIEngageOutOfAmmo(%client))
			%task.setWeight($AIWeightNeedItemBadly);
		else
			%task.setWeight($AIWeightNeedItem);
		return;
	}

	//we need to search for an inv station near us...
	%result = AIFindClosestInventory(%client, false);
	%closestInv = getWord(%result, 0);
	%closestDist = getWord(%result, 1);

	//only use inv stations if we're right near them...  patrolTask will get us nearer if required
	if (%closestDist > 300)
	{
		%task.setWeight(0);
                return;
	}

        //check for LOS - this was not here - Lagg...
        %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType;
	%closestInvLOS = !containerRayCast(%client.player.getWorldBoxCenter(), %closestInv.getWorldBoxCenter(), %mask, 0);

	//set the weight...
	%task.closestInv = %closestInv;
	if (%damage > 0.8 || AIEngageOutOfAmmo(%client))
		%task.setWeight($AIWeightNeedItemBadly);
	else if (%closestDist < 200 && (AIEngageWeaponRating(%client) <= 30 || %damage > 0.4))
		%task.setWeight($AIWeightNeedItem);
	else if (%closestInvLOS)
		%task.setWeight($AIWeightFoundItem);
	else
		%task.setWeight(0);
}

function AICouldUseInventoryTask::monitor(%task, %client)
{
	//make sure we still need equipment
	%player = %client.player;
	if (!isObject(%player))
		return;

        //if we are picking up an item or health forget it - Lagg...
        if (%client.pickUpItem > 0)
                return;

	%damage = %player.getDamagePercent();
	%weaponry = AIEngageWeaponRating(%client);
	if (%damage < 0.3 && %weaponry >= 40 && !%client.spawnUseInv)
	{
		%task.buyInvTime = getSimTime();
		return;
	}

	//pick a random set based on armor...
	%randNum = getRandom();
	if (%randNum < 0.4) 
		%buySet = "LightEnergyDefault MediumEnergySet HeavyEnergySet";
	else if (%randNum < 0.6)
		%buySet = "LightShieldSet MediumShieldSet HeavyShieldOff"; 
	else if (%randNum < 0.8)
		%buySet = "LightCloakSet MediumMissileSet HeavyAmmoSet";
        else
                %buySet = "LightEnergySniper MediumEnergySet HeavyEnergySet";

	//process the inv buying state machine
   %result = AIBuyInventory(%client, "", %buySet, %task.buyInvTime);

   //if we succeeded, reset the spawn flag
   if (%result $= "Finished")
      %client.spawnUseInv = false;

   //if we succeeded or failed, reset the state machine...
	if (%result !$= "InProgress")
		%task.buyInvTime = getSimTime();
        
	//this call works in conjunction with AIEngageTask
	%client.setEngageTarget(%client.shouldEngage);
}

