//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ AIO Missile Object +++
// - Lagg... 4-12-2003

function AIOMissileObject::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   // if were playing CnH, check who owns this
   if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden() || %this.targetObjectId.team == %client.team)
      return 0;

   //don't attack team 0 stuffs
   if (%this.targetObjectId.team <= 0)
      return 0;

   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   //lets check the range on this one
   if (vectorDist(%client.Player.getWorldBoxCenter(), %this.getWorldBoxCenter()) < 50)
      return 0;

   //no need to attack if the object is already destroyed
   if (!isObject(%this.targetObjectId) || %this.targetObjectId.getDamageState() $= "Destroyed")
      return 0;
   else
   {
      //if this bot is linked to a human who has issued this command, up the weight
      if (%this.issuedByClientId == %client.controlByHuman)
      {
         //make sure we have the potential to reach the minWeight
         if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
         {
            if ($AIWeightHumanIssuedCommand < %minWeight)
               return 0;
            else
               %weight = $AIWeightHumanIssuedCommand;
         }
         else
         {
            // calculate the default...
            %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
            if (%weight < $AIWeightHumanIssuedCommand)
               %weight = $AIWeightHumanIssuedCommand;
         }
      }
      else
      {
         //make sure we have the potential to reach the minWeight
         if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
            return 0;

         // calculate the default...
         %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
      }
      //if we are playing DnD, check if objective was completed already, if not give some extra :) - Lagg... 3-29-2004
      if (Game.class $= "DnDGame")
      {
         if (%this.targetObjectId.objectiveCompleted)
            %weight -= 400;
         else
            %weight += 200;
      }

      return %weight;
   }
}

function AIOMissileObject::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIMissileObject);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIOMissileObject::unassignClient(%this, %client)
{
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//----------------------------------------------------------------------------------------------- AI Missile Object ---
// - Lagg... 4-12-2003

function AIMissileObject::initFromObjective(%task, %objective, %client)
{
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;
   %task.location = %objective.location;
   %task.issuedByClient = %objective.issuedByClientId;

   %task.buyEquipmentSet = %objective.buyEquipmentSet;
   %task.equipment = strReplace(%objective.equipment, "MissileLauncher", "Hornet");
   %task.desiredEquipment = strReplace(%objective.desiredEquipment, "MissileLauncher", "Hornet");
   %task.equipment = strReplace(%task.equipment, "Plasma", "Hornet");
   %task.desiredEquipment = strReplace(%task.desiredEquipment, "Plasma", "Hornet");

    if ((%objective.deathCountByTurret > 0) || (%objective.deathCountByLava > 0)) {
       if (%objective.deathCountByTurret >= %objective.deathCountByLava) {
         %task.equipment = "SensorJammerPack || AntiEnergyPack";
         %task.desiredEquipment = "SensorJammerPack RepairKit || AntiEnergyPack RepairKit";
         %task.buyEquipmentSet = "MediumSensorJammerTriumph FieldTechSensorJammer HeavySensorJammer LightSensorJammer";
       } else {
         %task.equipment = "ElementShieldPack";
         %task.desiredEquipment = "ElementShieldPack RepairKit";
         %task.buyEquipmentSet = "SniperElementShield MediumElementShield FieldTechElementShield HeavyElementShield TitanElementShield LightElementShield";
       }
    }

   //initialize other task vars
   %task.sendMsg = true;
   %task.sendMsgTime = 0;
   %task.useTeleport = true;
}

function AIMissileObject::assume(%task, %client)
{
   %task.setWeightFreq(15);
   %task.setMonitorFreq(5);
   %task.needToRangeTime = 0;
   %client.needEquipment = AINeedEquipment(%task.equipment, %client);

   //even if we don't *need* equipemnt, see if we should buy some...
   if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
   {
      //see if we could benefit from inventory
      %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      if (%closestInv > 0) {   // No functioning inv station around -> don't bother using inv anymore
//          %closestInv.location = %closestInv.trigger.getWorldBoxCenter();
//          %result = AICheckUseTeleport(%client, %closestInv);//new function in aiVehicle.cs
         %closestDist = getWord(%result, 1);
         if (AINeedEquipment(%task.desiredEquipment, %client) && %closestDist < 200)
            %client.needEquipment = true;
      }
   }

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();

   //--------------------------------------------------- assume telporter - start -
   //if the MPB Teleporter is online - Lagg... 9-30-2003
   // ZOD: Less code this way...
//    if(MPBTeleporterCheck(%client, %task))
      %task.useTeleport = true;
//    else
//       %task.useTeleport = false;
   //--------------------------------------------------- assume telporter - end -
}

function AIMissileObject::retire(%task, %client)
{
   %client.setTargetObject(-1);
}

function AIMissileObject::weight(%task, %client)
{
   if (VectorDist(%task.location, %client.Player.getWorldBoxCenter) < 41)
   {
      //echo(" Too Close to missile object set baseweight (0)");
      %task.baseWeight = 0;
   }
   else if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;

   //let the monitor decide when to stop attacking
   %task.setWeight(%task.baseWeight);
}

function AIMissileObject::monitor(%task, %client)
{
   //first, buy the equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;

      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime, %task);
      if (%result $= "InProgress")
      {
         //force a nervous reaction every 15 sec - Lagg...
         if (getSimTime() - %task.buyInvTime > 15000)
         {
            %client.setDangerLocation(%client.player.getWorldBoxCenter(), 15);
            %task.buyInvTime = getSimTime();
         }
         return;
      }
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(15);
         %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
         //if this task is the objective task, choose a new objective
         if (%task == %client.objectiveTask)
         {
            AIUnassignClient(%client);
            Game.AIChooseGameObjective(%client);
         }
         return;
      }
   }
   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   //chat
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
         {
            if (%task.chat !$= "")
            {
               %chatMsg = getWord(%task.chat, 0);
               %chatTemplate = getWord(%task.chat, 1);
               if (%chatTemplate !$= "")
                  AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
               else
                  AIMessageThread(%task.chat, %client, -1);
            }
            else if (%task.targetObject > 0)
            {
               %type = %task.targetObject.getDataBlock().getName();
               if (%type $= "GeneratorLarge")
                  AIMessageThreadTemplate("AttackBase", "ChatSelfAttackGenerator", %client, -1);
               else if (%type $= "SensorLargePulse")
                  AIMessageThreadTemplate("AttackBase", "ChatSelfAttackSensors", %client, -1);
               else if (%type $= "SensorMediumPulse")
                  AIMessageThreadTemplate("AttackBase", "ChatSelfAttackSensors", %client, -1);
               else if (%type $= "TurretBaseLarge")
                  AIMessageThreadTemplate("AttackBase", "ChatSelfAttackTurrets", %client, -1);
               else if (%type $= "StationVehicle")
                  AIMessageThreadTemplate("AttackBase", "ChatSelfAttackVehicle", %client, -1);
            }
         }
      }
   }

   //set the target object
   if (isObject(%task.targetObject) && %task.targetObject.getDamageState() !$= "Destroyed")
   {
      //make sure we still have equipment
		%client.needEquipment = AINeedEquipment(%task.equipment, %client);
      if (%client.needEquipment)
      {
         //if this task is the objective task, choose a new objective
         if (%task == %client.objectiveTask)
         {
            AIUnassignClient(%client);
            Game.AIChooseGameObjective(%client);
            return;
         }
      }

      //--------------------------------------------------- monitor telporter - start -
      // use triumph teleporter if appropriate:
      if (%task.useTeleport) if (AIUseTeleport(%client, %task)) return;
      //--------------------------------------------------- monitor telporter - end -

      %clientPos = %client.player.getWorldBoxCenter();
      %targetPos = %task.targetObject.getWorldBoxCenter();
      %distance = %client.getPathDistance(%targetPos);
      if (%distance < 0)
         %distance = 32767;

      //next move to within 300
      if (%distance > 300)
      {
         %client.setTargetObject(-1);
         %client.stepMove(%task.targetObject.getWorldBoxCenter(), 15);
      }
      else
      {
         //move to LOS location to objective marker(not to target)
         //(that makes the LOS location adjustable!)
         %firePos = %client.getLOSLocation(%task.location, 50, 290);
         %client.stepMove(%firePos);
         //check for LOS
         %missileLOS = "false";
         //%mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType | $TypeMasks::ForceFieldObjectType;
         %missileLOS = !containerRayCast(%client.player.getWorldBoxCenter(), %task.location, $LOSMask, 0);
         %inRange = %client.getPathDistance(%task.location);

         //modified here in case bot gets stuck trying to get LOS to target - Lagg... - 4-21-2003
         %currentTime = getSimTime();
         if (%currentTime > %task.needToRangeTime)
         {
            //force a rangeObject every 20 seconds...
            %task.needToRangeTime = %currentTime + 20000;
            %client.setDangerLocation(%firePos, 30);
            return;
         }

         if ((%inRange > 49) && (%inRange < 291) && %missileLOS)
         {
            %client.setTargetObject(%task.targetObject, 300, "Mortar");
            //dissolve the human control link
            if (%task == %client.objectiveTask)
            {
               aiReleaseHumanControl(%client.controlByHuman, %client);
               %client.stop();
            }
            return;
         }
         else if ((%inRange > 49) && (%inRange < 291) && !%missileLOS && (%client.getPathDistance(%client.player.getWorldBoxCenter(), %firePos) < 10))
         {
            //if this task is the objective task, choose a new objective
            if (%task == %client.objectiveTask)
            {
               %task.baseWeight = 0;
               %task.setWeight(%task.baseWeight);
               return;
            }
         }
         else if (%client.getStepStatus() !$= "Finished")
         {
            %client.stepMove(%firePos);
            return;
         }
      }
   }
   else
   {
      //if this task is the objective task, choose a new objective
      if (%task == %client.objectiveTask)
      {
         %client.setTargetObject(-1);
         %client.stop();
         AIUnassignClient(%client);
         Game.AIChooseGameObjective(%client);
      }
   }
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ AIO Defense Patrol +++
//this is a new objective in beta. bot will walk from location to location doing the default defense stuff.
//*** note to lagg redo this for more than 1 path like you did for vehicle objectives ***

//Requires a simgroup titled T<%team#>PatrolPath<#> (example: T1PatrolPath1)  including 'markers' to set as locations to defend.

function AIODefensePatrol::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   // if were playing CnH, check who owns this
   if (%this.targetObjectId > 0)
   {
   	if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden() || %this.targetObjectId.team != %client.team)
	      return 0;
   }

   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   //do a quick check to disqualify this objective if it can't meet the minimum weight
   if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
   {
      if (%this.targetObjectId > 0 && %this.issuedByClientId == %client.controlByHuman)
      {
         if ($AIWeightHumanIssuedCommand < %minWeight)
            return 0;
      }
      else
         return 0;
   }
   %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);

   //if the object has been destroyed, reduce the weight
   if (%this.targetObjectId > 0)
   {
      //see if we were forced on the objective
      if (%this.issuedByClientId == %client.controlByHuman && %weight < $AIWeightHumanIssuedCommand)
         %weight = $AIWeightHumanIssuedCommand;

      //else see if the object has been destroyed - slight modification here - Lagg... 11-7-2003
	//else if (!isObject(%this.targetObjectId) || %this.targetObjectId.getDamageState() $= "Destroyed")
      else if (!isObject(%this.targetObjectId) || %this.targetObjectId.isDisabled())
         %weight -= 320;
   }
   return %weight;
}

function AIODefensePatrol::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIDefensePatrol);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIODefensePatrol::unassignClient(%this, %client)
{
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//----------------------------------------------------------------------------------------------- AI Defense Patrol ---

function AIDefensePatrol::initFromObjective(%task, %objective, %client)
{
   //initialize the task vars from the objective
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;
   if (%objective.Location !$= "")
      %task.location = %objective.location;
   else
      %task.location = %objective.targetObjectId.getWorldBoxCenter();

   %task.equipment = %objective.equipment;
   %task.desiredEquipment = %objective.desiredEquipment;
   %task.buyEquipmentSet = %objective.buyEquipmentSet;

   if (%task.buyEquipmentSet $= "randomSet")
   {
      //pick a random equipment set...
	%randNum = getRandom();
	if (%randNum < 0.2)
      {
         %task.desiredEquipment = "Hornet HornetAmmo";
         %task.buyEquipmentSet = "HeavyEnergySet";
      }
	else if (%randNum < 0.4)
      {
         %task.desiredEquipment = "Hornet HornetAmmo";
         %task.buyEquipmentSet = "MediumEnergySet";
      }
	else if (%randNum < 0.6)
      {
         %task.desiredEquipment = "Shotgun ShotgunAmmo";
         %task.buyEquipmentSet = "LightCloakSet";
      }
      else if (%randNum < 0.8)
      {
         %task.desiredEquipment = "Hornet HornetAmmo";
         %task.buyEquipmentSet = "HeavyShieldSet";
      }
      else
      {
         %task.desiredEquipment = "Hornet HornetAmmo";
         %task.buyEquipmentSet = "HeavyRepairSet";
      }
   }
   %task.issuedByClient = %objective.issuedByClientId;
   %task.chat = %objective.chat;

   //initialize other task vars
   %task.sendMsg = true;
   %task.sendMsgTime = 0;
   %task.engageTarget = -1;
   %task.timeCheck = true;
}

function AIDefensePatrol::assume(%task, %client)
{
   %task.setWeightFreq(15);
   %task.setMonitorFreq(15);
   %client.inPerimeter = false;
   %client.needEquipment = AINeedEquipment(%task.desiredEquipment, %client);

   //even if we don't *need* equipemnt, see if we should buy some...
   if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
   {
      //see if we could benefit from inventory
      %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      if (%closestInv > 0) {   // No functioning inv station around -> don't bother using inv anymore
//          %closestInv.location = %closestInv.trigger.getWorldBoxCenter();
//          %result = AICheckUseTeleport(%client, %closestInv);//new function in aiVehicle.cs
         %closestDist = getWord(%result, 1);
         if (AINeedEquipment(%task.desiredEquipment, %client) && %closestDist < 200)
            %client.needEquipment = true;
      }
   }

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();

   //set a flag to determine if the objective should be re-aquired when the object is destroyed
   %task.reassignOnDestroyed = false;

   //--------------------------------------------------- assume telporter - start -
   //if the MPB Teleporter is online - Lagg... 9-30-2003
   // ZOD: Less code this way...
//    if(MPBTeleporterCheck(%client, %task))
      %task.useTeleport = true;
//    else
//       %task.useTeleport = false;
   //--------------------------------------------------- assume telporter - end -

   //error("AIDefensePatrol::assume - client = " @ getTaggedString(%client.name));
}

function AIDefensePatrol::retire(%task, %client)
{
   %task.engageVehicle = -1;
   %client.setTargetObject(-1);
}

function AIDefensePatrol::weight(%task, %client)
{
   //update the task weight
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;//this will reaccess on destroyed if targetobjectid > 0

   %player = %client.player;
   if (!isObject(%player))
      return;

   %hasMissile = (%player.getInventory("MissileLauncher") > 0) && (%player.getInventory("MissileLauncherAmmo") > 0);

   //if we're defending with a missile launcher, our first priority is to take out vehicles...
   //see if we're already attacking a vehicle...
   if (%task.engageVehicle > 0 && isObject(%task.engageVehicle) && %hasMissile)
   {
      //set the weight
      %task.setWeight(%task.baseWeight);
		return;
   }

   //search for a new vehicle to attack
   %task.engageVehicle = -1;
   %losTimeout = $AIClientMinLOSTime + ($AIClientLOSTimeout * %client.getSkillLevel());
   %result = AIFindClosestEnemyPilot(%client, 300, %losTimeout);
   %pilot = getWord(%result, 0);
   %pilotDist = getWord(%result, 1);

   //if we've got missiles, and a vehicle to attack...
   if (%hasMissile && AIClientIsAlive(%pilot))
   {
      %task.engageVehicle = %pilot.vehicleMounted;
      %client.needEquipment = false;
   }

   //otherwise look for a regular enemy to fight...
   else
   {
      %result = AIFindClosestEnemyToLoc(%client, %task.location, 100, %losTimeout);
      %closestEnemy = getWord(%result, 0);
      %closestdist = getWord(%result, 1);

      //see if we found someone
      if (%closestEnemy > 0)
         %task.engageTarget = %closestEnemy;
      else
      {
         %task.engageTarget = -1;

	 //see if someone is near me...
	 %result = AIFindClosestEnemy(%client, 100, %losTimeout);
		   %closestEnemy = getWord(%result, 0);
		   %closestdist = getWord(%result, 1);
			if (%closestEnemy <= 0 || %closestDist > 70)
				%client.setEngageTarget(-1);
      }
   }

   //set the weight
   %task.setWeight(%task.baseWeight);
}

function AIDefensePatrol::monitor(%task, %client)
{
   //first, buy the equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;

      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime, %task);
      if (%result $= "InProgress")
      {
         //force a nervous reaction every 15 sec - Lagg...
         if (getSimTime() - %task.buyInvTime > 15000)
         {
            %client.setDangerLocation(%client.player.getWorldBoxCenter(), 20);
            %task.buyInvTime = getSimTime();
            %client.buyInvTime = %task.buyInvTime;
         }
         return;
      }
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(15);
	 %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
         //if this task is the objective task, choose a new objective
	 if (%task == %client.objectiveTask)
	 {
	    AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	 }
	 return;
      }
   }
   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   //chat
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
	 {
	    if (%task.chat !$= "")
	    {
	       %chatMsg = getWord(%task.chat, 0);
	       %chatTemplate = getWord(%task.chat, 1);
	       if (%chatTemplate !$= "")
                  AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
	       else
		  AIMessageThread(%task.chat, %client, -1);
	    }
	    else if (%task.targetObject > 0)
	    {
	       %type = %task.targetObject.getDataBlock().getName();
	       if (%type $= "Flag")
	          AIMessageThreadTemplate("DefendBase", "ChatSelfDefendFlag", %client, -1);
	       else if (%type $= "GeneratorLarge")
		  AIMessageThreadTemplate("DefendBase", "ChatSelfDefendBase", %client, -1);
	       else if (%type $= "StationVehicle")
		  AIMessageThreadTemplate("DefendBase", "ChatSelfDefendVehicle", %client, -1);
	       else if (%type $= "SensorLargePulse")
		  AIMessageThreadTemplate("DefendBase", "ChatSelfDefendSensors", %client, -1);
	       else if (%type $= "SensorMediumPulse")
		  AIMessageThreadTemplate("DefendBase", "ChatSelfDefendSensors", %client, -1);
	       else if (%type $= "TurretBaseLarge")
		  AIMessageThreadTemplate("DefendBase", "ChatSelfDefendTurrets", %client, -1);
	    }
         }
      }
   }

   //--------------------------------------------------- monitor telporter - start -
   // use triumph teleporter if appropriate:
   if (%task.useTeleport) if (AIUseTeleport(%client, %task)) return;
   //--------------------------------------------------- monitor telporter - end -

   //if the defend location task has an object, set the "reset" flag
   if (%task == %client.objectiveTask && isObject(%task.targetObject))
   {
      //if (%task.targetObject.getDamageState() !$= "Destroyed")
      if (%task.targetObject.isDisabled())
         %task.reassignOnDestroyed = true;
      else
      {
         if (%task.reassignOnDestroyed)
         {
            AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	    return;
         }
      }
   }

   //first, check for a vehicle to engage
   if (%task.engageVehicle > 0 && isObject(%task.engageVehicle))
   {
      %client.stop();
      %client.clearStep();
      %client.setEngageTarget(-1);
      %client.setTargetObject(%task.engageVehicle, 300, "Missile");
   }
   else
   {
      //clear the target vehicle...
      %client.setTargetObject(-1);

      //see if we're engaging a player
      if (%client.getEngageTarget() > 0)
      {
         //too far, or killed the enemy - return home
	 if (%client.getStepStatus() !$= "InProgress" || %distance > 100)
	 {
	    %client.setEngageTarget(-1);
	    %client.stepMove(%task.location, 4.0);
	 }
      }

      //else see if we have a target to begin attacking
      else if (%task.engageTarget > 0)
         %client.stepEngage(%task.engageTarget);

      //else move to a random location around where we are defending
      else
      {
         %dist = VectorDist(%client.player.getWorldBoxCenter(), %task.location);
	 if (%dist < 6)
	 {
            //random time update task location - Lagg...
            if (%task.timeCheck)
            {
               %task.watTime = getSimTime();
               %task.ranTime = getRandom(0, 20) * 1000;//random time to stepidle at defense points
               %task.timeCheck = false;
            }
            if (getSimTime() > (%task.watTime + %task.ranTime))
            {
               if (Game.class !$= "SiegeGame")
                  %patrolSpots = nameToId("T" @ %client.team @ "PatrolPath1");
               else
                  %patrolSpots = nameToId("T2PatrolPath1");
               %count = %patrolSpots.getCount();
               %ranSpot = mFloor(getRandom(0, %count - 1));
               %task.location = %patrolSpots.getObject(%ranSpot).position;
               %task.timeCheck = true;
            }
            //dissolve the human control link and re-evaluate the weight
	    if (%task == %client.objectiveTask)
	    {
	       if (aiHumanHasControl(%task.issuedByClient, %client))
	       {
	          aiReleaseHumanControl(%client.controlByHuman, %client);

		  //should re-evaluate the current objective weight
		  %inventoryStr = AIFindClosestInventories(%client);
		  %client.objectiveWeight = %client.objective.weight(%client, %client.objectiveLevel, 0, %inventoryStr);
               }
            }
	    %client.stepIdle(%task.location);
	 }
	 else
            %client.stepMove(%task.location, 4.0, $AIModeWalk);
      }
   }

   //see if we're supposed to be engaging anyone...
   if (!AIClientIsAlive(%client.getEngageTarget()) && AIClientIsAlive(%client.shouldEngage))
      %client.setEngageTarget(%client.shouldEngage);
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ AIO Place Camera +++
// have to fix this so a repair object objective is created for the newly deployed thingy?? - ZOD, nah, no one repairs cameras
//rotate the objective marker, bot will place on the "Y" same as all other deploy objectives
//                                                                            - Lagg... 1-14-2003

function AIOPlaceCamera::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   //make sure the deploy objective is valid
   //if (%this.isInvalid)
   //return 0;

   //first, make sure we haven't deployed too many...
   if (%this.equipment $= "CameraGrenade")
      %maxAllowed = $TeamDeployableMax[DeployedCamera];
   else
      return 0;

   if ($TeamDeployedCount[%client.team, %this.equipment] >= %maxAllowed)
      return 0;

   //now make sure there are no other items in the way...
   InitContainerRadiusSearch(%this.location, 1, $TypeMasks::VehicleObjectType |
  	                                          $TypeMasks::MoveableObjectType |
  	                                          $TypeMasks::StaticShapeObjectType |
  	                                          $TypeMasks::TSStaticShapeObjectType |
  	                                          $TypeMasks::ForceFieldObjectType |
  	                                          $TypeMasks::ItemObjectType |
  	                                          $TypeMasks::PlayerObjectType | $TypeMasks::TurretObjectType);
   %objSearch = containerSearchNext();

   //make sure we're not invalidating the deploy location with the client's own player object
   if (%objSearch == %client.player)
      %objSearch = containerSearchNext();

   //did we find an object which would block deploying the equipment?
   if (isObject(%objSearch))
      return 0;



   //check equipment requirement
   %needEquipment = AINeedEquipment(%this.equipment, %client);

   //if don't need equipment, see if we've past the "point of no return", and should continue regardless
   if (! %needEquipment)
   {
      %needArmor = AIMustUseRegularInvStation(%this.equipment, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      %closestInv.location = %closestInv.trigger.getWorldBoxCenter();
      %result = AICheckUseTeleport(%client, %closestInv);//new function in aiVehicle.cs
      %closestDist = getWord(%result, 1);

      //if we're too far from the inv to go back, or we're too close to the deploy location, force continue
      if ((%closestDist > 50 && VectorDist(%client.player.getWorldBoxCenter(), %task.location) < 150) || %closestInv < 0)
      {
         %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
         if (%weight < $AIWeightContinueDeploying)
            %weight = $AIWeightContinueDeploying;

         return %weight;
       }
    }

   //if this bot is linked to a human who has issued this command, up the weight
   if (%this.issuedByClientId == %client.controlByHuman)
   {
      //make sure we have the potential to reach the minWeight
      if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
      {
         if ($AIWeightHumanIssuedCommand < %minWeight)
            return 0;
         else
            %weight = $AIWeightHumanIssuedCommand;
      }
      else
      {
         // calculate the default...
         %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
         if (%weight < $AIWeightHumanIssuedCommand)
            %weight = $AIWeightHumanIssuedCommand;
      }
   }
   else
   {
      //make sure we have the potential to reach the minWeight
      if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
         return 0;

      // calculate the default...
      %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
   }
   return %weight;
}

function AIOPlaceCamera::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIPlaceCamera);
   %task = %client.objectiveTask;
   %task.initFromObjective(%this, %client);
}

function AIOPlaceCamera::unassignClient(%this, %client)
{
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//----------------------------------------------------------------------------------------------- AI Place Camera ---

function AIPlaceCamera::initFromObjective(%task, %objective, %client)
{
   //initialize the task vars from the objective
   %task.baseWeight = %client.objectiveWeight;
   %task.location = %objective.location;
   %task.buyEquipmentSet = %objective.buyEquipmentSet;
   %task.equipment = strReplace(%objective.equipment, "Plasma", "Hornet");
   %task.desiredEquipment = strReplace(%objective.desiredEquipment, "Plasma", "Hornet");
   %task.issuedByClient = %objective.issuedByClientId;
   %task.chat = %objective.chat;

   //initialize other task vars
   %task.sendMsg = true;
   %task.sendMsgTime = 0;

   //use the Y-axis of the rotation as the desired direction of deployement,
   //and calculate a walk to point 3 m behind the deploy point.
   %task.deployDirection = MatrixMulVector("0 0 0 " @ getWords(%objective.getTransform(), 3, 6), "0 1 0");
   %task.deployDirection = VectorNormalize(%task.deployDirection);
}

function AIPlaceCamera::assume(%task, %client)
{
   %task.setWeightFreq(15);
   %task.setMonitorFreq(15);

   %client.needEquipment = AINeedEquipment(%task.equipment, %client);

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();

   %task.passes = 0;
   %task.deployAttempts = 0;
   %task.checkObstructed = false;
   %task.waitMove = 0;
   %task.useTeleport = true;
}

function AIPlaceCamera::retire(%task, %client)
{
}

function AIPlaceCamera::weight(%task, %client)
{
   //update the task weight
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;

   %task.setWeight(%task.baseWeight);
}

function AIPlaceCamera::monitor(%task, %client)
{
   //first, buy the equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;

      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime, %task);
      if (%result $= "InProgress")
      {
         //force a nervous reaction every 15 sec - Lagg...
         if (getSimTime() - %task.buyInvTime > 15000)
         {
            %client.setDangerLocation(%client.player.getWorldBoxCenter(), 20);
            %task.buyInvTime = getSimTime();
         }
         return;
      }
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(30);
         %client.needEquipment = false;
         //if we made it past the inventory buying, reset the inv time
	   %task.buyInvTime = getSimTime();
      }
      else if (%result $= "Failed")
      {
         //if this task is the objective task, choose a new objective
         if (%task == %client.objectiveTask)
         {
            AIUnassignClient(%client);
            Game.AIChooseGameObjective(%client);
         }
         return;
      }
   }

   //chat
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
         {
            if (%task.chat !$= "")
            {
               %chatMsg = getWord(%task.chat, 0);
               %chatTemplate = getWord(%task.chat, 1);
               if (%chatTemplate !$= "")
                  AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
               else
                  AIMessageThread(%task.chat, %client, -1);
            }
         }
      }
   }

   //see if we're supposed to be engaging anyone...
   if (AIClientIsAlive(%client.shouldEngage))
   {
      %hasLOS = %client.hasLOSToClient(%client.shouldEngage);
      %losTime = %client.getClientLOSTime(%client.shouldEngage);
      if (%hasLOS || %losTime < 1000)
         %client.setEngageTarget(%client.shouldEngage);
      else
         %client.setEngageTarget(-1);
   }
   else
      %client.setEngageTarget(-1);

   // use triumph teleporter if appropriate:
   if (%task.useTeleport) if (AIUseTeleport(%client, %task)) return;

   //calculate the deployFromLocation
   %factor = -4;// * (3 - (%task.passes * 0.5));
   %task.deployFromLocation = VectorAdd(%task.location,VectorScale(%task.deployDirection, %factor));

   //see if we're within range of the deploy location
   %clLoc = %client.player.position;
   %distance = VectorDist(%clLoc, %task.deployFromLocation);
   %dist2D = VectorDist(%clLoc, getWords(%task.deployFromLocation, 0, 1) SPC getWord(%clLoc, 2));

   //set the aim when we get near the target...  this will be overwritten when we're actually trying to deploy
   if (%distance < 10 && %dist2D < 10)
      %client.aimAt(%task.location, 1000);

   if ((%client.pathDistRemaining(20) > %distance + 0.25) || %dist2D > 0.5)
   {
      %task.deployAttempts = 0;
      %task.checkObstructed = false;
      %task.waitMove = 0;
      %client.stepMove(%task.deployFromLocation, 0.25);
      %task.setMonitorFreq(15);
      return;
   }

   if (%task.deployAttempts < 1 && %task.passes < 1 && !AIClientIsAlive(%client.getEngageTarget()))
   {
      //dissolve the human control link
      if (%task == %client.objectiveTask)
         aiReleaseHumanControl(%client.controlByHuman, %client);

      %task.setMonitorFreq(3);
      %client.stop();
      if (%task.deployAttempts == 0)
         %deployPoint = %task.location;
      else
         %deployPoint = findTurretDeployPoint(%client, %task.location, %task.deployAttempts);

      if(%deployPoint !$= "")
      {
         // we have possible point
         %task.deployAttempts++;
         %client.aimAt(%deployPoint, 2000);

         //try to place the camera
         //%client.deployPack = true;
         %client.lastThrownObject = -1;
         %client.player.throwStrength = 2;
         %client.player.use(CameraGrenade);

         // check if camera deployed
         if (isObject(%client.lastDeployedObject))
         {
            //see if there's a "repairObject" objective for the newly deployed thingy...
            if (%task == %client.objectiveTask)
            {
               %deployedObject = %client.lastDeployedObject;

               //search the current objective group and search for a "repair Object" task...
               %objective = %client.objective;

               //delete any previously associated "AIORepairObject" objective
               if (isObject(%objective.repairObjective))
               {
                  clearObjectiveFromTable(%objective.repairObjective); // New function - Lagg...
                  AIClearObjective(%objective.repairObjective);
                  %objective.repairObjective.delete();
                  %objective.repairObjective = "";
               }

               //add the repair objective
               %objective.repairObjective = new AIObjective(AIORepairObject)
               {
                  dataBlock = "AIObjectiveMarker";
                  weightLevel1 = %objective.weightLevel1 - 100;
                  weightLevel2 = 0;
                  description = "Repair the Deployed Camera";
                  targetObjectId = %deployedObject;
                  issuedByClientId = %client;
                  offense = false;
                  defense = true;
                  equipment = "RepairPack || RepairGun2";
                  buyEquipmentSet = "FieldTechRepairSet";
               };
               %objective.repairObjective.deployed = true;
               %objective.repairObjective.setTransform(%objective.getTransform());
               %objective.repairObjective.group = %objective.group;
               MissionCleanup.add(%objective.repairObjective);
               $ObjectiveQ[%client.team].add(%objective.repairObjective);

               //finally, unassign the client so he'll go do something else...
               AIUnassignClient(%client);
               Game.AIChooseGameObjective(%client);
            }
            //finished
            return;
         }
      }
   }
   else if (!%task.checkObstructed)
   {
      %task.checkObstructed = true;

      //see if anything is in our way
      InitContainerRadiusSearch(%task.location, 4, $TypeMasks::MoveableObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::PlayerObjectType);
      %objSrch = containerSearchNext();
      if (%objSrch == %client.player)
         %objSrch = containerSearchNext();

      if (%objSrch)
         AIMessageThread("ChatMove", %client, -1);
   }
   else if (%task.waitMove < 5 && %task.passes < 1)
   {
      %task.waitMove++;

      //try another pass at deploying
      if (%task.waitMove == 5)
      {
         %task.waitMove = 0;
         %task.passes++;
         %task.deployAttempts = 0;

         //see if we're *right* underneath the deploy point
         %deployDist2D = VectorDist(getWords(%client.player.position, 0, 1) @ "0", getWords(%task.location, 0, 1) @ "0");
         if (%deployDist2D < 0.25)
         {
            %client.pressjump();
            %client.player.throwStrength = 2;
            %client.player.use(CameraGrenade);

            // check if camera deployed
            //don't add a "repairObject" objective for ceiling turrets
            if (%task == %client.objectiveTask)
            {
               AIUnassignClient(%client);
               Game.AIChooseGameObjective(%client);
            }
         }
      }
   }
   else
   {
      //find a new assignment - and remove this one from the Queue
      if (%task == %client.objectiveTask)
      {
         //error(%client SPC "from team" SPC %client.team SPC "is invalidating objective:" SPC %client.objective SPC "UNABLE TO DEPLOY EQUIPMENT");
         %client.objective.isInvalid = false;
         AIUnassignClient(%client);
         Game.AIChooseGameObjective(%client);
      }
   }
}

//----------------------------------------------------------------------------------------------- AIO Destroy Object --- (Last Update 3-30-2004)
//place objective marker in spot you want bot to stand and destroy for sentry turrets and hard to
//reach spots. basically same as attackobject.                                                - Lagg... 3-26-2003

function AIODestroyObject::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   // if were playing CnH, check who owns this
   if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden() || %this.targetObjectId.team == %client.team)
      return 0;

   //don't attack team 0 stuffs
   if (%this.targetObjectId.team <= 0)
      return 0;

   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   //no need to attack if the object is already destroyed
   if (!isObject(%this.targetObjectId) || %this.targetObjectId.getDamageState() $= "Destroyed")
      return 0;
   else
   {
      //if this bot is linked to a human who has issued this command, up the weight
                if (%this.issuedByClientId == %client.controlByHuman)
                {
                        //make sure we have the potential to reach the minWeight
                        if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
                        {
                                if ($AIWeightHumanIssuedCommand < %minWeight)
                                        return 0;
                                else
                                        %weight = $AIWeightHumanIssuedCommand;
                        }
                        else
                        {
                                // calculate the default...
                                %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
                                if (%weight < $AIWeightHumanIssuedCommand)
                                        %weight = $AIWeightHumanIssuedCommand;
                        }
                }
                else
                {
                        //make sure we have the potential to reach the minWeight
                        if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
                                return 0;

                        // calculate the default...
                        %this.desiredEquipment = "Plasma PlasmaAmmo";
                        %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
                }

      //if we are playing DnD, check if objective was completed already, if not give some extra :) - Lagg... 3-29-2004
      if (Game.class $= "DnDGame")
      {
         if (%this.targetObjectId.objectiveCompleted)
            %weight -= 400;
         else
            %weight += 200;
      }

      return %weight;
   }
}
function AIODestroyObject::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIdestroyObject);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIODestroyObject::unassignClient(%this, %client)
{
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//------------------------------------------------------------------------------------------------ AI Destroy Object ---
function AIDestroyObject::initFromObjective(%task, %objective, %client)
{
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;
   %task.equipment = %objective.equipment;
   %task.buyEquipmentSet = %objective.buyEquipmentSet;
   %task.desiredEquipment = %objective.desiredEquipment;
   %task.issuedByClient = %objective.issuedByClientId;
   %task.location = %objective.position;

   //initialize other task vars
   %task.sendMsg = true;
   %task.sendMsgTime = 0;
   %task.useTeleport = false;
}

function AIDestroyObject::assume(%task, %client)
{
   %task.setWeightFreq(15);
   %task.setMonitorFreq(15);

   //added here so Bot buys a random set if no set was specified in objective dataBlock - Lagg... 2-14-2003
      if (%task.buyEquipmentSet $= "randomSet")
      {
         //pick a random equipment set...
         %randNum = getRandom();
         if (%randNum < 0.2)
         {
            %task.desiredEquipment = "Plasma PlasmaAmmo";
            %task.buyEquipmentSet = "HeavyEnergySet";
         }
         else if (%randNum < 0.4)
         {
            %task.desiredEquipment = "Plasma PlasmaAmmo";
            %task.buyEquipmentSet = "MediumEnergySet";
         }
         else if (%randNum < 0.6)
         {
            %task.desiredEquipment = "Plasma PlasmaAmmo";
            %task.buyEquipmentSet = "LightCloakSet";
         }
         else if (%randNum < 0.8)
         {
            %task.desiredEquipment = "Plasma PlasmaAmmo";
            %task.buyEquipmentSet = "HeavyShieldOff";
         }
         else
         {
            %task.desiredEquipment = "Plasma PlasmaAmmo";
            %task.buyEquipmentSet = "MediumMissileSet";
         }
      }

   if (%task.equipment !$= "")
      %equipmentList = %task.equipment;
   else
      %equipmentList = %task.desiredEquipment;
   %client.needEquipment = AINeedEquipment(%equipmentList, %client);


   //even if we don't *need* equipment, see if we should buy some...
   if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
   {
      //see if we could benefit from inventory
      %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      %closestDist = getWord(%result, 1);
      if (AINeedEquipment(%task.desiredEquipment, %client) && %closestInv > 0)
      {
         //find where we are
         %clientPos = %client.player.getWorldBoxCenter();
         %objPos = %task.targetObject.getWorldBoxCenter();
         %distToObject = %client.getPathDistance(%objPos);

         if (%distToObject < 0 || %closestDist < 50)// 50m to inventory in this case
         {
            //error("AIDestroyObject::assume - ! %needequipment but should buy some " @ gettaggedString(%client.name));
            %client.needEquipment = true;
         }
      }
   }

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();

   %task.useTeleport = true;
   //--------------------------------------------------- assume telporter - end -
}

function AIDestroyObject::retire(%task, %client)
{
   %client.setTargetObject(-1);
}

function AIDestroyObject::weight(%task, %client)
{
   //update the task weight
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;

   //let the monitor decide when to stop attacking
   %task.setWeight(%task.baseWeight);
}

function AIDestroyObject::monitor(%task, %client)
{

   //first, buy the equipment - if we really really need some :) - Lagg... - 4-6-2003
   %hasMortar = (%client.player.getInventory("Mortar") > 0) && (%client.player.getInventory("MortarAmmo") > 0);
   %hasPlasma = (%client.player.getInventory("Plasma") > 0) && (%client.player.getInventory("PlasmaAmmo") > 0);
   %hasDisc = (%client.player.getInventory("Disc") > 0) && (%client.player.getInventory("DiscAmmo") > 0) && (!%client.player.getInventory("Blaster") > 0);
   if (%hasMortar || %hasPlasma || %hasDisc)
      %client.needEquipment = false;

   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
                        %equipmentList = %task.equipment;
                else
                        %equipmentList = %task.desiredEquipment;
      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
         return;
      else if (%result $= "Finished")
                {
                   %task.setMonitorFreq(15);
                        %client.needEquipment = false;
                }
                else if (%result $= "Failed")
                {
              //if this task is the objective task, choose a new objective
              if (%task == %client.objectiveTask)
              {
                 AIUnassignClient(%client);
                 Game.AIChooseGameObjective(%client);
              }
                        return;
                }
   }
        //if we made it past the inventory buying, reset the inv time
        %task.buyInvTime = getSimTime();

        //chat
        if (%task.sendMsg)
        {
                if (%task.sendMsgTime == 0)
                        %task.sendMsgTime = getSimTime();
                else if (getSimTime() - %task.sendMsgTime > 7000)
                {
                        %task.sendMsg = false;
                   if (%client.isAIControlled())
                        {
                                if (%task.chat !$= "")
                                {
                                        %chatMsg = getWord(%task.chat, 0);
                                        %chatTemplate = getWord(%task.chat, 1);
                                        if (%chatTemplate !$= "")
                                                AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
                                        else
                                                AIMessageThread(%task.chat, %client, -1);
                                }
                                else if (%task.targetObject > 0)
                                {
                                        %type = %task.targetObject.getDataBlock().getName();
                                        if (%type $= "GeneratorLarge")
                                                AIMessageThreadTemplate("AttackBase", "ChatSelfAttackGenerator", %client, -1);
                                        else if (%type $= "SensorLargePulse")
                                                AIMessageThreadTemplate("AttackBase", "ChatSelfAttackSensors", %client, -1);
                                        else if (%type $= "SensorMediumPulse")
                                                AIMessageThreadTemplate("AttackBase", "ChatSelfAttackSensors", %client, -1);
                                        else if (%type $= "TurretBaseLarge")
                                                AIMessageThreadTemplate("AttackBase", "ChatSelfAttackTurrets", %client, -1);
                                        else if (%type $= "StationVehicle")
                                                AIMessageThreadTemplate("AttackBase", "ChatSelfAttackVehicle", %client, -1);
                                }
                        }
                }
        }
   //--------------------------------------------------- monitor telporter - start -
   // use triumph teleporter if appropriate:
   if (%task.useTeleport) if (AIUseTeleport(%client, %task)) return;
   //--------------------------------------------------- monitor telporter - end -


   //set the target object
   %outOfStuff = (AIAttackOutofAmmo(%client));
   if (isObject(%task.targetObject) && %task.targetObject.getDamageState() !$= "Destroyed" && !%outOfStuff)
   {
      %close = false;
      if (VectorDist(%task.targetObject.position, %client.player.position) < 4)
         %close = true;

      %type = %task.targetObject.getDataBlock().getName();
      if (%type $= "StationVehicle" || %type $= "MPBTeleporter")
      {
         //use the Y-axis of the rotation as the desired direction of approach,
         //and calculate a walk to point 1m in front of the trigger point - Lagg...

         %aprchDirection = MatrixMulVector("0 0 0 " @ getWords(%task.targetObject.getTransform(), 3, 6), "0 1 0");
         %aprchDirection = VectorNormalize(%aprchDirection);
         %factor = 10; //10m in front of station vehicle is the right spot to stand
         //%aprchFromLocation = VectorAdd(%task.targetObject.position,VectorScale(%aprchDirection, %factor));
         %task.location = VectorAdd(%task.targetObject.position,VectorScale(%aprchDirection, %factor));
         %client.stepMove(%task.location);

         if (VectorDist(%task.targetObject.position, %client.player.position) < 15)
            %client.setDangerLocation(%task.targetObject.position, 20);
      }
      //in case lazy people don't want to move aiobjective marker
      else if (VectorDist(%task.targetObject.getWorldBoxCenter(), %task.location) < 4)
      {
         if (! %client.targetInRange())
            %client.stepMove(%task.targetObject.getWorldBoxCenter(), 8.0);
      }
      else
         %client.stepMove(%task.location);

      %targetLOS = "false";
      %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::ForceFieldObjectType;
      //%targetLOS = !containerRayCast(%client.player.getWorldBoxCenter(), %task.targetObject.getWorldBoxCenter(), %mask, 0);
      %targetLOS = !containerRayCast(%client.player.getMuzzlePoint($WeaponSlot), %task.targetObject.getWorldBoxCenter(), %mask, 0);
      if (%targetLOS && !%close)
         %client.setTargetObject(%task.targetObject, 40, "Destroy");
      else
          %client.setTargetObject(-1);
   }
   else
   {
      %client.setTargetObject(-1);

      //aim at the floor if we need to repair self
      if (%client.player.getDamagePercent() > 0 && %client.player.getInventory(RepairPack) > 0)
      {
         %pos = %client.player.getWorldBoxCenter();
         %posX = getWord(%pos, 0);
         %posY = getWord(%pos, 1);
         %posZ = getWord(%pos, 2) - 1;
         %pos = %posX SPC %posY SPC %posZ;
         %client.aimAt(%pos, 500);
      }


      //if this task is the objective task, choose a new objective
      if (%task == %client.objectiveTask)
      {
         AIUnassignClient(%client);
         Game.AIChooseGameObjective(%client);
      }
   }
}


//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ AIO MAttack Location +++
//modified version of AIAttackLocation, could be called AIMortarSpamLocation :) - Lagg...

function AIOMAttackLocation::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   //now, if this bot is linked to a human who has issued this command, up the weight
   if (%this.issuedByClientId == %client.controlByHuman)
   {
      //make sure we have the potential to reach the minWeight
      if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
      {
         if ($AIWeightHumanIssuedCommand < %minWeight)
            return 0;
         else
            %weight = $AIWeightHumanIssuedCommand;
      }
      else
      {
         // calculate the default...
         %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
         if (%weight < $AIWeightHumanIssuedCommand)
            %weight = $AIWeightHumanIssuedCommand;
      }
   }
   else
   {
      //make sure we have the potential to reach the minWeight
      if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
         return 0;

      // calculate the default...
      %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
   }

   //Special cases to account for force fields up or down and if we are in the base or not
   if (nameToId("EntranceFFNorth").powerCount > 0 && nameToId("EntranceFFEast").powerCount > 0 && nameToId("EntranceFFWest").powerCount > 0 && !%client.inBase)
      return 0;

   if (%this.mode $= "SwitchGenDown")
   {
      if (nameToId("SwitchFFGenerator").isEnabled())
         return 0;

      if (isObject(%client.objectiveTask) && (%client.objectiveTask.getName() $= "AIDefendLocation" || %client.objectiveTask.getName() $= "AIDeployEquipment"))
      {
         //error("AIOMAttackLocation::weight - we are in base but we are " @ getTaggedString(%client.name) SPC %client.objectiveTask.getName());
         return 0;
      }
   }
   return %weight;
}

function AIOMAttackLocation::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIMAttackLocation);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIOMAttackLocation::unassignClient(%this, %client)
{
   //error("AIOMAttackLocation::unassignClient - " @ getTaggedString(%client.name));

   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//----------------------------------------------------------------------------------------------- AI MAttack Location ---

function AIMAttackLocation::initFromObjective(%task, %objective, %client)
{
        //initialize the task vars from the objective
   %task.baseWeight = %client.objectiveWeight;
   %task.location = %objective.location;
   %task.equipment = %objective.equipment;
        %task.buyEquipmentSet = %objective.buyEquipmentSet;
        %task.desiredEquipment = %objective.desiredEquipment;
        %task.issuedByClient = %objective.issuedByClientId;
        %task.chat = %objective.chat;

        //initialize other task vars
        %task.sendMsg = false;
        %task.sendMsgTime = 0;
   %task.engageTarget = -1;
}

function AIMAttackLocation::assume(%task, %client)
{
   %task.setWeightFreq(30);
   %task.setMonitorFreq(30);
        %client.needEquipment = AINeedEquipment(%task.equipment, %client);

        //even if we don't *need* equipemnt, see if we should buy some...
        if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
        {
                //see if we could benefit from inventory
                %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
                %result = AIFindClosestInventory(%client, %needArmor);
                %closestInv = getWord(%result, 0);
                %closestDist = getWord(%result, 1);
                if (AINeedEquipment(%task.desiredEquipment, %client) && %closestInv > 0)
                {
                        %result = AIFindClosestEnemyToLoc(%client, %task.location, 100, $AIClientLOSTimeout);
              %closestEnemy = getWord(%result, 0);

                        if (%closestEnemy <= 0 && %closestDist < 100)
                                %client.needEquipment = true;
                }
        }

        //mark the current time for the buy inventory state machine
        %task.buyInvTime = getSimTime();

        %task.snipeLocation = "";
        %task.hideLocation = "";
        %task.moveToPosition = true;
        %task.moveToSnipe = false;
        %task.nextSnipeTime = 0;
}

function AIMAttackLocation::retire(%task, %client)
{
}

function AIMAttackLocation::weight(%task, %client)
{
        //update the task weight
        if (%task == %client.objectiveTask)
                %task.baseWeight = %client.objectiveWeight;

        //if we're a sniper, we're going to cheat, and see if there are clients near the attack location
        %losTimeout = $AIClientMinLOSTime + ($AIClientLOSTimeout * %client.getSkillLevel());
        %distToLoc = VectorDist(%client.player.getWorldBoxCenter(), %task.location);
        if (((%client.player.getInventory(SniperRifle) > 0 && %client.player.getInventory(EnergyPack) > 0) ||
            (%client.player.getInventory(Mortar) > 0 && %client.player.getInventory(MortarAmmo) > 0)) && %distToLoc > 100)
                %result = AIFindClosestEnemyToLoc(%client, %task.location, 100, $AIClientLOSTimeout, true);

        //otherwise, do the search normally.  (cheat ignores LOS)...
        else
                %result = AIFindClosestEnemyToLoc(%client, %task.location, 100, %losTimeout, false);

   %closestEnemy = getWord(%result, 0);
   %closestdist = getWord(%result, 1);
   %task.setWeight(%task.baseWeight);

   //see if we found someone
   if (%closestEnemy > 0)
      %task.engageTarget = %closestEnemy;
   else
      %task.engageTarget = -1;
}

//---------------------------------------------------------------------
//modified range for bot sniper to attack location - Lagg... 4-12-2003
function AIMAttackLocation::monitor(%task, %client)
{
   //first, buy the equipment - if we really really need some :) - Lagg... - 4-6-2003
   %hasMortar = (%client.player.getInventory("Mortar") > 0) && (%client.player.getInventory("MortarAmmo") > 0);

   if (%hasMortar)
      %client.needEquipment = false;

   //first, buy the equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;
      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
         return;
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(30);
         %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
         //if this task is the objective task, choose a new objective
         if (%task == %client.objectiveTask)
         {
            AIUnassignClient(%client);
            Game.AIChooseGameObjective(%client);
         }
         return;
      }
   }
   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   //chat
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
         {
            if (%task.chat !$= "")
            {
               %chatMsg = getWord(%task.chat, 0);
               %chatTemplate = getWord(%task.chat, 1);
               if (%chatTemplate !$= "")
                  AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
               else
                  AIMessageThread(%task.chat, %client, -1);
            }
            else
               AIMessageThreadTemplate("AttackBase", "ChatSelfAttack", %client, -1);
         }
      }
   }

   //make sure we still have equipment
   %client.needEquipment = AINeedEquipment(%task.equipment, %client);
   if (%client.needEquipment)
   {
      //if this task is the objective task, choose a new objective
      if (%task == %client.objectiveTask)
      {
         AIUnassignClient(%client);
         Game.AIChooseGameObjective(%client);
         return;
      }
   }

   //how far are we from the location we're defending
   %myPos = %client.player.getWorldBoxCenter();
   %distance = %client.getPathDistance(%task.location);
   %hasSniper = %client.player.getInventory(SniperRifle) > 0 && %client.player.getInventory(EnergyPack) > 0;
   %hasMortar = %client.player.getInventory(Mortar) > 0 && %client.player.getInventory(MortarAmmo) > 0;
   if (%distance < 0)
      %distance = 32767;

   if (%hasSniper || %hasMortar)
   {
      //first, find an LOS location
      if (%task.snipeLocation $= "")
      {
         %task.snipeLocation = %client.getLOSLocation(%task.location, 150, 300);//was 150-200 - Lagg...
         %task.hideLocation = %client.getHideLocation(%task.location, VectorDist(%task.location, %task.snipeLocation), %task.snipeLocation, 1);
         %client.stepMove(%task.hideLocation, 4.0);
         %task.moveToPosition = true;
      }
      else
      {
         //see if we can acquire a target
         %energy = %client.player.getEnergyPercent();
         %distToSnipe = VectorDist(%task.snipelocation, %client.player.getWorldBoxCenter());
         %distToHide = VectorDist(%task.hidelocation, %client.player.getWorldBoxCenter());

         //until we're in position, we can move using the AIModeExpress, after that, we only want to walk...
         if (%task.moveToPosition)
         {
            if (%distToHide < 4.0)
            {
               //dissolve the human control link
               if (%task == %client.objectiveTask)
               {
                  if (aiHumanHasControl(%task.issuedByClient, %client))
                  {
                     aiReleaseHumanControl(%client.controlByHuman, %client);

                     //should re-evaluate the current objective weight
                     %inventoryStr = AIFindClosestInventories(%client);
                     %client.objectiveWeight = %client.objective.weight(%client, %client.objectiveLevel, 0, %inventoryStr);
                  }
               }
               %task.moveToPosition = false;
            }
         }
         else if (%task.moveToSnipe)
         {
            if (((%hasSniper && %energy > 0.75) || %hasMortar) && %client.getStepStatus() $= "Finished")
            {
               %client.stepMove(%task.snipeLocation, 4.0, $AIModeWalk);
               %client.setEngageTarget(%task.engageTarget);
            }
            else if (%hasSniper && %energy < 0.4)
            {
               %client.setEngageTarget(-1);
               %client.stepMove(%task.hideLocation, 4.0);
               //%task.nextSnipeTime = getSimTime() + 4000 + (getRandom() * 4000);
               %task.nextSnipeTime = getSimTime() + 1000 + (getRandom() * 2000);
               %task.moveToSnipe = false;
            }
         }
         else if (((%hasSniper && %energy > 0.5) || %hasMortar) && %task.engageTarget > 0 && getSimTime() > %task.nextSnipeTime)
         {
            %task.sendMsg = true;
            %client.stepRangeObject(%task.engageTarget.player.getWorldBoxCenter(), "BasicSniperShot", 150, 300, %task.snipelocation);
            if (! %hasMortar)
               %client.aimAt(%task.engageTarget.player.getWorldBoxCenter(), 8000);
            %task.moveToSnipe = true;
         }
      }
   }
   //else we don't have sniper or mortar stuff so attack regular like :( - Lagg...
   else
   {
      //else see if we have a target to begin attacking
      if (%client.getEngageTarget() <= 0 && %task.engageTarget > 0)
         %client.stepEngage(%task.engageTarget);

      //else move to the location we're attacking
      else if (%client.getEngageTarget() <= 0)
      {
         %client.stepMove(%task.location, 8.0);
         if (VectorDist(%client.player.position, %task.location) < 10)
         {
            //dissolve the human control link
            if (%task == %client.objectiveTask)
            {
               if (aiHumanHasControl(%task.issuedByClient, %client))
               {
                  aiReleaseHumanControl(%client.controlByHuman, %client);

                  //should re-evaluate the current objective weight
                  %inventoryStr = AIFindClosestInventories(%client);
                  %client.objectiveWeight = %client.objective.weight(%client, %client.objectiveLevel, 0, %inventoryStr);
               }
            }
         }
      }
   }

   //see if we're supposed to be engaging anyone...
   if (!AIClientIsAlive(%client.getEngageTarget()) && AIClientIsAlive(%client.shouldEngage))
      %client.setEngageTarget(%client.shouldEngage);
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ AIO MDestroy Object +++
//place objective marker in spot you want bot to stand and destroy for sentry turrets and hard to
//reach spots. basically same as destroy object but with mortars :)           - Lagg... 3-26-2003

function AIOMDestroyObject::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   // if were playing CnH, check who owns this
   if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden() || %this.targetObjectId.team == %client.team)
      return 0;

   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   //no need to attack if the object is already destroyed
   if (!isObject(%this.targetObjectId) || %this.targetObjectId.getDamageState() $= "Destroyed")
      return 0;
   else
        {
                //if this bot is linked to a human who has issued this command, up the weight
                if (%this.issuedByClientId == %client.controlByHuman)
                {
                        //make sure we have the potential to reach the minWeight
                        if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
                        {
                                if ($AIWeightHumanIssuedCommand < %minWeight)
                                        return 0;
                                else
                                        %weight = $AIWeightHumanIssuedCommand;
                        }
                        else
                        {
                                // calculate the default...
                                %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
                                if (%weight < $AIWeightHumanIssuedCommand)
                                        %weight = $AIWeightHumanIssuedCommand;
                        }
                }
                else
                {
                        //make sure we have the potential to reach the minWeight
                        if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
                                return 0;

                        // calculate the default...
                        %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
                }

      //if we are playing DnD, check if objective was completed already, if not give some extra :) - Lagg... 3-29-2004
      if (Game.class $= "DnDGame")
      {
         if (%this.targetObjectId.objectiveCompleted)
            %weight -= 400;
         else
            %weight += 200;
      }

      return %weight;
   }
}

function AIOMDestroyObject::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIMDestroyObject);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIOMDestroyObject::unassignClient(%this, %client)
{
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";

   //error("AIOMDestroyObject::unassignClient - " @ getTaggedString(%client.name));
}

//----------------------------------------------------------------------------------------------- AI MDestroy Object ---

function AIMDestroyObject::initFromObjective(%task, %objective, %client)
{
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;
   %task.equipment = %objective.equipment;
        %task.buyEquipmentSet = %objective.buyEquipmentSet;
        %task.desiredEquipment = %objective.desiredEquipment;
        %task.issuedByClient = %objective.issuedByClientId;
        %task.location = %objective.location;

        //initialize other task vars
        %task.sendMsg = true;
        %task.sendMsgTime = 0;
   %task.useTeleport = false;

   if (%task.equipment $= "")
   {
      //pick a random equipment set...
      %randNum = getRandom();
      if (%randNum < 0.2)
      {
         %task.desiredEquipment = "Plasma PlasmaAmmo";
         %task.buyEquipmentSet = "HeavyEnergySet";
      }
      else if (%randNum < 0.4)
      {
         %task.desiredEquipment = "Plasma PlasmaAmmo";
         %task.buyEquipmentSet = "HeavyAmmoSet";
      }
      else if (%randNum < 0.6)
      {
         %task.desiredEquipment = "Plasma PlasmaAmmo";
         %task.buyEquipmentSet = "HeavyEnergyDefault";
      }
      else if (%randNum < 0.8)
      {
         %task.desiredEquipment = "Plasma PlasmaAmmo";
         %task.buyEquipmentSet = "HeavyShieldOff";
      }
      else
      {
         %task.desiredEquipment = "Plasma PlasmaAmmo";
         %task.buyEquipmentSet = "HeavyRepairSet";
      }
   }
}

function AIMDestroyObject::assume(%task, %client)
{
   %task.setWeightFreq(20);
   %task.setMonitorFreq(20);

   %client.needEquipment = AINeedEquipment(%task.equipment, %client);

        //even if we don't *need* equipemnt, see if we should buy some...
        if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
        {
                //see if we could benefit from inventory
                %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
                %result = AIFindClosestInventory(%client, %needArmor);
                %closestInv = getWord(%result, 0);
                %closestDist = getWord(%result, 1);
                if (AINeedEquipment(%task.desiredEquipment, %client) && %closestInv > 0)
                {
                        //find where we are
                        %clientPos = %client.player.getWorldBoxCenter();
                        %objPos = %task.targetObject.getWorldBoxCenter();
                        %distToObject = %client.getPathDistance(%objPos);

                        if (%distToObject < 0 || %closestDist < 100)//%distToObject)
                                %client.needEquipment = true;
                }
        }

        //mark the current time for the buy inventory state machine
        %task.buyInvTime = getSimTime();

   %task.useTeleport = true;
   //--------------------------------------------------- assume telporter - end -
}

function AIMDestroyObject::retire(%task, %client)
{
   %client.setTargetObject(-1);
}

function AIMDestroyObject::weight(%task, %client)
{
        //update the task weight
        if (%task == %client.objectiveTask)
                %task.baseWeight = %client.objectiveWeight;

   //let the monitor decide when to stop attacking
   %task.setWeight(%task.baseWeight);
}

function AIMDestroyObject::monitor(%task, %client)
{
   //first, buy the equipment - if we really really need some :) - Lagg... - 4-6-2003
   %hasMortar = (%client.player.getInventory("Mortar") > 0) && (%client.player.getInventory("MortarAmmo") > 0);

   if (%hasMortar)
      %client.needEquipment = false;

   if (%client.needEquipment)
   {
           %task.setMonitorFreq(5);
                if (%task.equipment !$= "")
                        %equipmentList = %task.equipment;
                else
                        %equipmentList = %task.desiredEquipment;

      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
         return;
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(20);
         %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
         //if this task is the objective task, choose a new objective
         if (%task == %client.objectiveTask)
         {
            AIUnassignClient(%client);
            Game.AIChooseGameObjective(%client);
         }
         return;
      }
   }
        //if we made it past the inventory buying, reset the inv time
        %task.buyInvTime = getSimTime();

        //chat
        if (%task.sendMsg)
        {
                if (%task.sendMsgTime == 0)
                        %task.sendMsgTime = getSimTime();
                else if (getSimTime() - %task.sendMsgTime > 7000)
                {
                        %task.sendMsg = false;
                   if (%client.isAIControlled())
                        {
                                if (%task.chat !$= "")
                                {
                                        %chatMsg = getWord(%task.chat, 0);
                                        %chatTemplate = getWord(%task.chat, 1);
                                        if (%chatTemplate !$= "")
                                                AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
                                        else
                                                AIMessageThread(%task.chat, %client, -1);
                                }
                                else if (%task.targetObject > 0)
                                {
                                        %type = %task.targetObject.getDataBlock().getName();
                                        if (%type $= "GeneratorLarge")
                                                AIMessageThreadTemplate("AttackBase", "ChatSelfAttackGenerator", %client, -1);
                                        else if (%type $= "SensorLargePulse")
                                                AIMessageThreadTemplate("AttackBase", "ChatSelfAttackSensors", %client, -1);
                                        else if (%type $= "SensorMediumPulse")
                                                AIMessageThreadTemplate("AttackBase", "ChatSelfAttackSensors", %client, -1);
                                        else if (%type $= "TurretBaseLarge")
                                                AIMessageThreadTemplate("AttackBase", "ChatSelfAttackTurrets", %client, -1);
                                        else if (%type $= "StationVehicle")
                                                AIMessageThreadTemplate("AttackBase", "ChatSelfAttackVehicle", %client, -1);
                                }
                        }
                }
        }

   //--------------------------------------------------- monitor telporter - start -
   // use triumph teleporter if appropriate:
   if (%task.useTeleport) if (AIUseTeleport(%client, %task)) return;
   //--------------------------------------------------- monitor telporter - end -


   //set the target object
   %outOfStuff = (AIAttackOutofAmmo(%client));
   if (isObject(%task.targetObject) && %task.targetObject.getDamageState() !$= "Destroyed" && !%outOfStuff)
   {
      %dist = VectorDist(%task.targetObject.position, %client.player.position);
      if (%dist < 15)
      {
         if (%task.targetObject.getDataBlock().getName() $= "StationVehicle")
            %client.setDangerLocation(%task.targetObject.position, 20);
         else if (%task.targetObject.getDataBlock().getName() $= "MPBTeleporter")
            %client.setDangerLocation(%task.targetObject.position, 20);
      }
      //in case lazy people don't want to move aiobjective marker
      else if (VectorDist(%task.targetObject.getWorldBoxCenter(), %task.location) < 4)
      {
         if (! %client.targetInRange())
            %client.stepMove(%task.targetObject.getWorldBoxCenter(), 8.0);
      }
      else
         %client.stepMove(%task.location);

      %targetLOS = "false";
      %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::ForceFieldObjectType;
      %targetLOS = !containerRayCast(%client.player.getMuzzlePoint($WeaponSlot), %task.targetObject.getWorldBoxCenter(), %mask, 0);

      if (%targetLOS && %dist > 40 && %dist < 300)
      {
         %client.setTargetObject(%task.targetObject, 300, "Mortar");
         //echo("AIMDestroyObject::monitor - Should be set to MDestroy object here");
      }
      else if (%targetLOS && %dist < 40)
      {
         %client.setTargetObject(%task.targetObject, 100, "Destroy");
         //echo("AIMDestroyObject::monitor - Should be set to MDestroy object here");
      }
      else
      {
          %client.setTargetObject(-1);
          %client.stepMove(%task.location);
      }
   }
   else
   {
      %client.setTargetObject(-1);

      //aim at the floor if we need to repair self so we don't repair enemy equipment
      if (%client.player.getDamagePercent() > 0 && %client.player.getInventory(RepairPack) > 0)
      {
         %pos = %client.player.getWorldBoxCenter();
         %posX = getWord(%pos, 0);
         %posY = getWord(%pos, 1);
         %posZ = getWord(%pos, 2) - 1;
         %pos = %posX SPC %posY SPC %posZ;
         %client.aimAt(%pos, 500);
      }


      //if this task is the objective task, choose a new objective
      if (%task == %client.objectiveTask)
      {
         AIUnassignClient(%client);
         Game.AIChooseGameObjective(%client);
      }
   }
}

