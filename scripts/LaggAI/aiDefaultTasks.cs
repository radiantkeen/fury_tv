//All tasks for deathmatch, hunters, and tasks that coincide with the current objective task live here...

//Weights for tasks that override the objective task: must be between 4300 and 4700
$AIWeightVehicleMountedEscort	= 4850;//was 4700 - Lagg...
$AIWeightReturnTurretFire = 4675;
$AIWeightNeedItemBadly = 4650;
$AIWeightReturnFire = 4600;
$AIWeightDetectMine = 3700;
$AIWeightTauntVictim = 3600;
$AIWeightNeedItem = 4400;
$AIWeightDestroyTurret = 4300;
$AIWeightDetectVehicle = 3390;//added - Lagg...
$AIWeightDetectRemeq = 3200;//added - Lagg...
$AIWeightDetectFF = 3250;//added - Pinkpanther...
$AIWeightDetectDeplTurret = 3150;//added - Pinkpanther...
$AIWeightIgnoreEngagement = 4300;//added - Pinkpanther...
$AIWeightDetectDeplByHuman = 600;// Stuff deployed by humans gets higher weight
$AIWeightDetectEnemyStuffNearBase = 700;// Enemy stuff near our base gets higher weight

//Weights that allow the objective task to continue:  must be 3000 or less
$AIWeightFoundEnemy        	= 3000;
$AIWeightFoundItem    		= 2500;
$AIWeightFoundToughEnemy     	= 1000;
$AIWeightPatrolling        	= 2000;

//Hunters weights...
$AIHuntersWeightMustCap		= 4690;
$AIHuntersWeightNeedHealth	= 4625;
$AIHuntersWeightShouldCap	= 4425;
$AIHuntersWeightMustEngage	= 4450;
$AIHuntersWeightShouldEngage	= 4325;
$AIHuntersWeightPickupFlag	= 4425;

//Rabbit weights...
$AIRabbitWeightDefault		= 4625;
$AIRabbitWeightNeedInv		= 4325;

//Bounty weights...
$AIBountyWeightShouldEngage	= 4325;

//-----------------------------------------------------------------------------
//AIEngageTask is responsible for anything to do with engaging an enemy

function AIEngageWhoWillWin(%client1, %client2)
{
   //assume both clients are alive - gather some info
   if (%client1.isAIControlled())
      %skill1 = %client1.getSkillLevel();
   else
      %skill1 = 0.5;

   if (%client2.isAIControlled())
      %skill2 = %client2.getSkillLevel();
   else
      %skill2 = 0.5;

   %damage1 = %client1.player.getDamagePercent();
   %damage2 = %client2.player.getDamagePercent();

   //first compare health
   %tolerance1 = 0.5 + ((%skill1 - %skill2) * 0.3);
   %tolerance2 = 0.5 + ((%skill2 - %skill1) * 0.3);
   if (%damage1 - %damage2 > %tolerance1)
      return %client2;
   else if (%damage2 - %damage1 > %tolerance2)
      return %client1;

   //health not a problem, see how the equipment compares for the two...
   %weaponry1 = AIEngageWeaponRating(%client1);
   %weaponry2 = AIEngageWeaponRating(%client2);
   %effective = 20;
   if (%weaponry1 < %effective && %weaponry2 >= %effective)
      return %client2;
   else if (%weaponry1 >= %effective && %weaponry2 < %effective)
      return %client1;

   //no other criteria for now...  return -1 to indicate a tie...
   return -1;
}

function AIEngageTask::init(%task, %client)
{
}

function AIEngageTask::assume(%task, %client)
{
   %task.setWeightFreq(9);
   %task.setMonitorFreq(9);
   %task.searching = false;
   if (isObject(%client.shouldEngage.player))
      %task.searchLocation = %client.shouldEngage.player.getWorldBoxCenter();
}

function AIEngageTask::retire(%task, %client)
{
}

function AIEngageTask::weight(%task, %client)
{
   if (%client.objectiveWeight > $AIWeightIgnoreEngagement) {
      %client.shouldEngage = -1;
      %task.setWeight(0);
      return;
      }

   %player = %client.player;
   if (!isObject(%player))
      return;

   if (%player.isMounted())
      AIProcessVehicle(%client); //added this call to help vehicle mounted bots - Lagg...

   %clientPos = %player.getWorldBoxCenter();
   %currentTarget = %client.shouldEngage;
   if (!AIClientIsAlive(%currentTarget))
      %currentTarget = %client.getEngageTarget();

   %client.shouldEngage = -1;
   %mustEngage = false;
   %tougherEnemy = false;

   //first, make sure we actually can engage
   if (AIEngageOutOfAmmo(%client))
   {
      %client.shouldEngage = -1;
      %task.setWeight(0);
      return;
   }

   //see if anyone has fired on us recently...
   %losTimeout = $AIClientMinLOSTime + ($AIClientLOSTimeout * %client.getSkillLevel());
   if (AIClientIsAlive(%client.lastDamageClient, %losTimeout) && getSimTime() - %client.lastDamageTime < %losTimeout)
   {
      //see if we should turn on the new attacker
      if (AIClientIsAlive(%currentTarget))
      {
         %targPos = %currentTarget.player.getWorldBoxCenter();
         %curTargDist = %client.getPathDistance(%targPos);

         %newTargPos = %client.lastDamageClient.player.getWorldBoxCenter();
         %newTargDist = %client.getPathDistance(%newTargPos);

         //see if the new targ is no more than 30 m further
         if (%newTargDist > 0 && %newTargDist < %curTargDist + 30)
         {
            %client.shouldEngage = %client.lastDamageClient;
            %mustEngage = true;
         }
      }
      else
      {
         %client.shouldEngage = %client.lastDamageClient;
         %mustEngage = true;
      }
   }
   //no one has fired at us recently, see if we're near an enemy
   else
   {
      %result = AIFindClosestEnemy(%client, 100, %losTimeout);
      %closestEnemy = getWord(%result, 0);
      %closestdist = getWord(%result, 1);
      if (%closestEnemy > 0)
      {
         //see if we're right on top of them
         %targPos = %closestEnemy.player.getWorldBoxCenter();
         %dist = %client.getPathDistance(%targPos);

         if (%dist > 0 && %dist < 25) //was 20 - Lagg..
         {
            %client.shouldEngage = %closestEnemy;
            %mustEngage = true;
         }

         //else choose them only if we're not already attacking someone
         else if (%currentTarget <= 0)
         {
            %client.shouldEngage = %closestEnemy;
            %mustEngage = false;

            //Make sure the odds are not overwhelmingly in favor of the enemy...
            if (AIEngageWhoWillWin(%client, %closestEnemy) == %closestEnemy)
               %tougherEnemy = true;
         }
      }
   }

   //if we still haven't found a new target, keep fighting the old one
   if (%client.shouldEngage <= 0)
   {
      if (AIClientIsAlive(%currentTarget))
      {
         //see if we still have sight of the current target
         %hasLOS = %client.hasLOSToClient(%currentTarget);
         %losTime = %client.getClientLOSTime(%currentTarget);
         if (%hasLOS || %losTime < %losTimeout)
            %client.shouldEngage = %currentTarget;
         else
            %client.shouldEngage = -1;
      }
      else
         %client.shouldEngage = -1;

      %mustEngage = false;
   }

   //finally, set the weight
   if (%client.shouldEngage > 0)
   {
      if (%mustEngage)
         %task.setWeight($AIWeightReturnFire);
      else if (%tougherEnemy)
         %task.setWeight($AIWeightFoundToughEnemy);
      else
         %task.setWeight($AIWeightFoundEnemy);
   }
   else
      %task.setWeight(0);
}

function AIEngageTask::monitor(%task, %client)
{
   if (!AIClientIsAlive(%client.shouldEngage))
   {
      %client.stop();
      %client.clearStep();
      %client.setEngageTarget(-1);
      return;
   }

   %hasLOS = %client.hasLOSToClient(%client.shouldEngage);
   %losTime = %client.getClientLOSTime(%client.shouldEngage);
   //%detectLocation = %client.getDetectLocation(%client.shouldEngage);
   %detectPeriod = %client.getDetectPeriod();

   //if we can see the target, engage...
   if (%hasLOS || %losTime < %detectPeriod)
   {
      //added here so gen attackers go for gens, but not if they are deploying :) - Lagg... 12-7-2003
      if (isObject(%client.objectiveTask) && %client.objectiveTask.getName() !$= "AIAttackObject" && %client.objectiveTask.getName() !$= "AIDeployEquipment")
      {
         %client.stepEngage(%client.shouldEngage);
         %task.searching = false;
         %task.searchLocation = %client.shouldEngage.player.getWorldBoxCenter();
      }
      else
      {
         %client.setEngageTarget(%client.shouldEngage);
         %task.searching = true;
      }
   }
   //else if we haven't for approx 5 sec...  move to the last known location
   else
   {
      //clear the engage target
      %client.setEngageTarget(-1);

      if (! %task.searching)
      {
         %dist = VectorDist(%client.player.getWorldBoxCenter(), %task.searchLocation);
         if (%dist < 4)
         {
            %client.stepIdle(%task.searchLocation);
            %task.searching = true;
         }
         else
            %client.stepMove(%task.searchLocation, 4.0);
      }
   }
}

//-----------------------------------------------------------------------------
//AIPickupItemTask is responsible for anything to do with picking up an item
//modified pickupitem task to include repairpack as a health item and fixed some other bugs - Lagg... 4-3-2003

function AIPickupItemTask::init(%task, %client)
{
}

function AIPickupItemTask::assume(%task, %client)
{
   %task.setWeightFreq(10);
   %task.setMonitorFreq(10);

   %task.pickupItem = -1;
   %client.pickupItem = -1;
}

function AIPickupItemTask::retire(%task, %client)
{
   %task.pickupItem = -1;
   %client.pickupItem = -1;
}

function AIPickupItemTask::weight(%task, %client)
{
   //first, see if we can pick up health
   %player = %client.player;
   if (!isObject(%player))
      return;

   //if we're already picking up an item, make sure it's still valid, then keep the weight the same...
   if (%task.pickupItem > 0 && %client.pickupItem > 0)
   {
      if (isObject(%task.pickupItem) && !%task.pickupItem.isHidden())
         return;
      else
      {
         %task.pickupItem = -1;
         %client.pickupItem = -1;
      }
   }

   //otherwise, search for objects

   %damage = %player.getDamagePercent();
   %healthRad = %damage * 100;
   %closestHealth = -1;
   %closestHealthDist = %healthRad;
   %closestHealthLOS = false;
   %closestItem = -1;
   %closestItemDist = 32767;
   %closestItemLOS = false;

   //loop through the item list, looking for things to pick up
   %itemCount = $AIItemSet.getCount();
   for (%i = 0; %i < %itemCount; %i++)
   {
      %item = $AIItemSet.getObject(%i);
      if (!%item.isHidden())
      {
         %dist = %client.getPathDistance(%item.getWorldBoxCenter());

         if (isObject(%client.objectiveTask))
         {
            %watTask = %client.objectiveTask.getName();
            if (%watTask $= "AIODeployEquipment")
               echo("AIPickupItemTask::weight - watTask = " @ getTaggedString(%client.name));
         }

         if ((%item.getDataBlock().getName() $= "RepairPack" && %player.getInventory("RepairPack") <= 0 && %player.getInventory("RepairGun2") <= 0 && %damage > 0.4 && %watTask !$= "AIODeployEquipment")
            || (%item.isCorpse && %item.getInventory("RepairKit") > 0 && %damage > 0.2 && %player.getInventory("RepairKit") <= 0)
            || (%item.getDataBlock().getName() $= "RepairPatch" && %damage > 0.2) || (%item.getDataBlock().getName() $= "RepairKit" && %damage > 0.2 && %player.getInventory("RepairKit") <= 0))
         {
            if (%dist > 1 && %dist < %closestHealthDist)
            {
               %closestHealth = %item;
               %closestHealthDist = %dist;

               //if (%item.isCorpse && %item.getInventory("RepairKit") > 0)
               //{
                  //error("AIPickupItemTask::weight-item.isCorpse + hasRepairKit");
                  //error("client = " @ getTaggedString(%client.name));
               //}

	         //check for LOS
               %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::ForceFiledObjectType;
               %closestHealthLOS = !containerRayCast(%client.player.getWorldBoxCenter(), %item.getWorldBoxCenter(), %mask, 0);
            }
         }
         else
	   {
            //only pick up stuff within 35m
            // Greater than 1 as well - Lagg..
            if (%dist > 1 || %dist < 55)
            {
               if (AICouldUseItem(%client, %item))
               {
//echo (getTaggedString(%client.name) SPC "AICouldUseItem" SPC %dist SPC %item.getDataBlock().getName());
                  if (%dist < %closestItemDist)
                  {
                     %closestItem = %item;
                     %closestItemDist = %dist;

                     //check for LOS
                     %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType;
                     %closestItemLOS = !containerRayCast(%client.player.getWorldBoxCenter(), %item.getWorldBoxCenter(), %mask, 0);
                  }
               }
            }
         }
      }
   }

   //now set the weight
   if (%closestHealth > 0)
   {
      //only choose an item if it's at least 25 m closer than health...
      //and we're not engageing someone or not that badly in need
      %currentTarget = %client.getEngageTarget();
      if (%closestItem > 0 && %closetItemDist < %closestHealthDist - 25 && (%damage < 0.6 || %currentTarget <= 0) &&
        %closestItemLOS)
      {
         %task.pickupItem = %closestItem;
         %client.pickupItem = %closestItem;
         if (AIEngageWeaponRating(%client) < 20)
	      %task.setWeight($AIWeightNeedItemBadly);
	   else if (%closestItemDist < 10 && %closestItemLOS)
	      %task.setWeight($AIWeightNeedItem);
         else if (%closestItemLOS)
            %task.setWeight($AIWeightFoundItem);
         else
	     %task.setWeight(0);
      }
      else
      {
         if (%damage > 0.8)
         {
            %task.pickupItem = %closestHealth;
            %client.pickupItem = %closestHealth;
            %task.setWeight($AIWeightNeedItemBadly + 50);
         }
         else if (%closestHealthLOS)
         {
            %task.pickupItem = %closestHealth;
            %client.pickupItem = %closestHealth;
            %task.setWeight($AIWeightNeedItem + 50);
         }
         else
            %task.setWeight(0);
      }
   }
   else if (%closestItem > 0)
   {
      %task.pickupItem = %closestItem;
      if (AIEngageWeaponRating(%client) < 20)
         %task.setWeight($AIWeightNeedItemBadly);
      else if (%closestItemDist < 10 && %closestItemLOS)
	   %task.setWeight($AIWeightNeedItem);
      else if (%closestItemLOS)
	   %task.setWeight($AIWeightFoundItem);
      else
	   %task.setWeight(0);
   }
   else
      %task.setWeight(0);

   AISensorJammerActivation(%client);
}

function AIPickupItemTask::monitor(%task, %client)
{
   %distance = %client.getPathDistance(%task.pickupItem.position);
   //move to the pickup location
   if (isObject(%task.pickupItem))// && %distance > 2.2
   {
      if (%task.pickupItem.getDataBlock().getName() $= "RepairPack")
      {
         %distToRep = %client.getPathDistance(%task.pickupItem.position);
         if (%distToRep < 10 && %client.player.getMountedImage($BackpackSlot) > 0 && %client.player.getInventory("RepairPack") <= 0)
         {
            %client.player.throwPack();
         }
      }
      %client.stepMove(%task.pickupItem.getWorldBoxCenter(), 0.25);

      //make the bot stop acting like a bot - Lagg...
      %client.lastDamageClient = -1;
if ($debugAIPickupItemTask) error("AIPickupItemTask::monitor - " @ getTaggedString(%client.name) @ " %task.pickupItem:" SPC %task.pickupItem.getDataBlock().getName() SPC %distance);

	  //this call works in conjunction with AIEngageTask
	  %client.setEngageTarget(%client.shouldEngage);
   } else {
//       if (isObject(%task.pickupItem) && %distance < 0.8) {  // prevent daydreaming on pickup items   - pinkpanther
//          if (%task.pickupItem.getDataBlock().getName() !$= "FLAG") %client.setDangerLocation(%task.pickupItem.position, 15);
//       }
      %task.pickupItem = -1;
      %client.pickupItem = -1;
   }
}

//-----------------------------------------------------------------------------
//AIUseInventoryTask will cause them to use an inv station if they're low
//on ammo.  This task should be used only for DM and Hunters - most objectives
//have their own logic for when to use an inv station...

function AIUseInventoryTask::init(%task, %client)
{
}

function AIUseInventoryTask::assume(%task, %client)
{
   %task.setWeightFreq(15);
   %task.setMonitorFreq(5);

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();
}

function AIUseInventoryTask::retire(%task, %client)
{
   //reset the state machine time stamp...
   %task.buyInvTime = getSimTime();
}

function AIUseInventoryTask::weight(%task, %client)
{
   //first, see if we can pick up health
   %player = %client.player;
   if (!isObject(%player))
      return;

   %damage = %player.getDamagePercent();
   %weaponry = AIEngageWeaponRating(%client);
if ($debugInv) echo(getTaggedString(%client.name) @ " AIUseInventoryTask %t.descr" SPC %task.description SPC  "%t.eq" SPC  %task.equipment SPC  "%t.desEq" SPC  %task.desiredEquipment SPC  "%weaponry" SPC  %weaponry);

   //if there's an inv station, and we haven't used an inv station since we
   //spawned, the bot should use an inv once regardless
   if (%client.spawnUseInv)
   {
      //see if we're already heading there
      if (%client.buyInvTime != %task.buyInvTime)
      {
	   //see if there's an inventory we can use
         %result = AIFindClosestInventory(%client, false);
         %closestInv = getWord(%result, 0);
	   if (isObject(%closestInv))
         {
            %task.setWeight($AIWeightNeedItem);
            return;
         }
         else
            %client.spawnUseInv = false;
      }
      else
      {
         %task.setWeight($AIWeightNeedItem);
         return;
      }
   }

   //first, see if we need equipment or health
   if (%damage < 0.3 && %weaponry >= 40)
   {
      %task.setWeight(0);
      return;
   }

   //don't use inv stations if we're not that badly damaged, and we're in the middle of a fight
   if (%damage < 0.6 && %client.getEngageTarget() > 0 && !AIEngageOutOfAmmo(%client))
   {
      %task.setWeight(0);
      return;
   }

   //if we're already buying, continue
   if (%task.buyInvTime == %client.buyInvTime)
   {
      //set the weight - if our damage is above 0.8 or our we're out of ammo
      if (%damage > 0.8 || AIEngageOutOfAmmo(%client))
         %task.setWeight($AIWeightNeedItemBadly);
      else
         %task.setWeight($AIWeightNeedItem);

      return;
   }

   //we need to search for an inv station near us...
   %result = AIFindClosestInventory(%client, false);
   %closestInv = getWord(%result, 0);
   %closestDist = getWord(%result, 1);

   //only use inv stations if we're right near them...  patrolTask will get us nearer if required
   if (%closestDist > 35)
   {
      %task.setWeight(0);
      return;
   }

   //set the weight...
   %task.closestInv = %closestInv;
   if (%damage > 0.8 || AIEngageOutOfAmmo(%client))
      %task.setWeight($AIWeightNeedItemBadly);
   else if (%closestDist < 20 && (AIEngageWeaponRating(%client) <= 30 || %damage > 0.4))
      %task.setWeight($AIWeightNeedItem);
   else if (%hasLOS)
      %task.setWeight($AIWeightFoundItem);
   else
      %task.setWeight(0);
}

function AIUseInventoryTask::monitor(%task, %client)
{
   //make sure we still need equipment
   %player = %client.player;
   if (!isObject(%player))
      return;

   %damage = %player.getDamagePercent();
   %weaponry = AIEngageWeaponRating(%client);
   if (%damage < 0.3 && %weaponry >= 40 && !%client.spawnUseInv)
   {
      %task.buyInvTime = getSimTime();
      return;
   }

   //pick a random set based on armor...
//    %randNum = getRandom();
//    if (%randNum < 0.4)
//       %buySet = "LightEnergyDefault MediumEnergySet HeavyEnergySet";
//    else if (%randNum < 0.6)
//       %buySet = "LightShieldSet MediumShieldSet HeavyShieldSet";
//    else if (%randNum < 0.8)
//       %buySet = "LightEnergyELF MediumRepairSet HeavyAmmoSet";
//    else
      %buySet = "LightEnergySniper LightAttacker ReconShotgun MediumEnergySet FieldTechAmmoSet HeavyEnergySet TitanNuke";

   //process the inv buying state machine
   %result = AIBuyInventory(%client, "", %buySet, %task.buyInvTime);

   //if we succeeded, reset the spawn flag
   if (%result $= "Finished")
      %client.spawnUseInv = false;

   //if we succeeded or failed, reset the state machine...
   if (%result !$= "InProgress")
      %task.buyInvTime = getSimTime();


   //this call works in conjunction with AIEngageTask
   %client.setEngageTarget(%client.shouldEngage);
}

//-----------------------------------------------------------------------------
//AITauntCorpseTask is should happen only after an enemy is freshly killed
//modified slightly - Lagg... - 4-4-2003

function AITauntCorpseTask::init(%task, %client)
{
}

function AITauntCorpseTask::assume(%task, %client)
{
   %task.setWeightFreq(11);
   %task.setMonitorFreq(11);
}

function AITauntCorpseTask::retire(%task, %client)
{
}

function AITauntCorpseTask::weight(%task, %client)
{
   %task.corpse = %client.getVictimCorpse();
   if (%task.corpse > 0 && getSimTime() - %client.getVictimTime() < 7500)
   {
      //see if we're already taunting, and if it's time to stop
      if ((%task.tauntTime > %client.getVictimTime()) && (getSimTime() - %task.tauntTime > 1000))
         %task.corpse = -1;
      else
      {
         //if the corpse is within 25m, taunt
         %distToCorpse = %client.getPathDistance(%task.corpse.getWorldBoxCenter());
         if (%dist < 0 || %distToCorpse > 25)
            %task.corpse = -1;
      }
   }
   else
      %task.corpse = -1;

   //set the weight
   if (%task.corpse > 0)
   {
      //don't taunt someone if there's an enemy right near by...
      %result = AIFindClosestEnemy(%client, 40, 15000);
      %closestEnemy = getWord(%result, 0);
      %closestdist = getWord(%result, 1);
      if (%closestEnemy > 0)
         %task.setWeight(0);
      else
         %task.setWeight($AIWeightTauntVictim);
   }
   else
      %task.setWeight(0);

   AISensorJammerActivation(%client);
}

function AITauntCorpseTask::monitor(%task, %client)
{
   //make sure we still have a corpse, and are not fighting anyone
   if (%client.getEngageTarget() <= 0 && %task.corpse > 0 && isObject(%task.corpse))
   {
      %clientPos = %client.player.getWorldBoxCenter();
      %corpsePos = %task.corpse.getWorldBoxCenter();
      %distToCorpse = VectorDist(%clientPos, %corpsePos);
      if (%distToCorpse < 2.0)
      {
         //start the taunt!
         if (%task.tauntTime < %client.getVictimTime())
         {
            %task.tauntTime = getSimTime();
            %client.stop();

            if (getRandom() > 0.2)
            {
               //pick the sound and taunt cels
               %sound = $AITauntChat[mFloor(getRandom() * 3.99)];
               %minCel = 2;
               %maxCel = 8;
               schedule(250, %client, "AIPlayAnimSound", %client, %corpsePos, %sound, %minCel, %maxCel, 0);
            }
            //say 'bye'  :)
            else
               schedule(250, %client, "AIPlayAnimSound", %client, %corpsePos, "gbl.bye", 2, 2, 0);
         }
      }
      else
         %client.stepMove(%task.corpse.getWorldBoxCenter(), 1.75);
   }
}

//-----------------------------------------------------------------------------
//AIPatrolTask used to wander around the map (DM and Hunters mainly) looking for something to do...

function AIPatrolTask::init(%task, %client)
{
}

function AIPatrolTask::assume(%task, %client)
{
   %task.setWeightFreq(13);
   %task.setMonitorFreq(13);
   %task.findLocation = true;
   %task.patrolLocation = "0 0 0";
   %task.idleing = false;
   %task.idleEndTime = 0;
}

function AIPatrolTask::retire(%task, %client)
{

}

function AIPatrolTask::weight(%task, %client)
{
   %task.setWeight($AIWeightPatrolling);

   AISensorJammerActivation(%client);
}

function AIPatrolTask::monitor(%task, %client)
{
   //this call works in conjunction with AIEngageTask
   %client.setEngageTarget(%client.shouldEngage);

   //see if we're close enough to our patrol point
   if (%task.idleing)
   {
      if (getSimTime() > %task.idleEndTime)
      {
         %task.findLocation = true;
         %task.idleing = false;
      }
   }
   //see if we need to find a place to go...
   else if (%task.findLocation)
   {
      //first, see if we're in need of either health, or ammo
      //note: normally, I'd be tempted to put this kind of "looking for health" code
      //into the AIPickupItemTask, however, that task will be used in CTF, where you
      //don't want people on AIDefendLocation to leave their post to hunt for health, etc...
      //AIPickupItemTask only deals with items within a 30m radius around the bot.
      //AIPatrolTask will move the bot to the vicinity of an item, then AIPickUpItemTask
      //will finish the job...
      %foundItemLocation = false;
      %damage = %client.player.getDamagePercent();
      if (%damage > 0.7)
      {
         //search for a health kit
         %closestHealth = AIFindSafeItem(%client, "Health");
         if (%closestHealth > 0)
         {
            %task.patrolLocation = %closestHealth.getWorldBoxCenter();
            %foundItemLocation = true;
         }
      }
      else if (AIEngageOutOfAmmo(%client))
      {
         //search for a Ammo or a weapon...
         %closestItem = AIFindSafeItem(%client, "Ammo");
         if (%closestItem > 0)
         {
            %task.patrolLocation = %closestItem.getWorldBoxCenter();
            %foundItemLocation = true;
         }
      }

      //now see if we don't really have good equipment...
      if (!%foundItemLocation && AIEngageWeaponRating(%client) < 20)
      {
         //search for any useful item
         %closestItem = AIFindSafeItem(%client, "Any");
         if (%closestItem > 0)
         {
            %task.patrolLocation = %closestItem.getWorldBoxCenter();
            %foundItemLocation = true;
         }
      }
      //choose a randomish location only if we're not in need of health or ammo
      if (!%foundItemLocation)
      {
         //find a random item/inventory in the map, and pick a spawn point near it...
         %pickGraphNode = false;
         %chooseSet = 0;
         if ($AIInvStationSet.getCount() > 0)
            %chooseSet = $AIInvStationSet;
         else if ($AIWeaponSet.getCount() > 0)
            %chooseSet = $AIWeaponSet;
         else if ($AIItemSet.getCount() > 0)
            %chooseSet = $AIItemSet;

         if (!%chooseSet)
            %pickGraphNode = true;

         //here we pick whether we choose a random map point, or a point based on an item...
         if (getRandom() < 0.3)
            %pickGraphNode = true;

         //here we decide whether we should choose a player location...  a bit of a cheat but
         //it's scaled by the bot skill level
         %pickPlayerLocation = false;
         %skill = %client.getSkillLevel();
         if (%skill < 1.0)
            %skill = %skill / 2.0;

         if (getRandom() < (%skill * %skill) && ClientGroup.getCount() > 1)
         {
            //find a random client
            %count = ClientGroup.getCount();
            %index = (getRandom() * (%count - 0.1));
            %cl = ClientGroup.getObject(%index);
            if (%cl != %client && AIClientIsAlive(%cl))
            {
               %task.patrolLocation = %cl.player.getWorldBoxCenter();
               %pickGraphNode = false;
               %pickPlayerLocation = true;
            }
         }

         if (!%pickGraphNode && !%pickPlayerLocation)
         {
            %itemCount = %chooseSet.getCount();
            %item = %chooseSet.getObject(getRandom() * (%itemCount - 0.1));
            %nodeIndex = navGraph.randNode(%item.getWorldBoxCenter(), 10, true, true);
            if (%nodeIndex <= 0)
               %pickGraphNode = true;
            else
               %task.patrolLocation = navGraph.randNodeLoc(%nodeIndex);
         }

         //see if we failed above or have to pick just a random spot on the graph - use the spawn points...
         if (%pickGraphNode)
         {
            %task.patrolLocation = Game.pickPlayerSpawn(%client, true);
            if (%task.patrolLocation == -1)
            {
               %client.stepIdle(%client.player.getWorldBoxCenter());
               return;
            }
         }
      }
      //now that we have a new location - move towards it
      %task.findLocation = false;
      %client.stepMove(%task.patrolLocation, 8.0);
   }
   //else we're on patrol - see if we're close to our destination
   else
   {
      %client.stepMove(%task.patrolLocation, 8.0);
      %distToDest = %client.getPathDistance(%task.patrolLocation);
      if (%distToDest > 0 && %distToDest < 10)
      {
         %task.idleing = true;
         %task.idleEndTime = 4000 + getSimTime() + (getRandom() * 6000);
         %client.stepIdle(%client.player.getWorldBoxCenter());
      }
   }
}

//-----------------------------------------------------------------------------
//AIEngageTurretTask is responsible for returning turret fire...

function AIEngageTurretTask::init(%task, %client)
{
//error(getTaggedString(%client.name) @ " AIEngageTurretTask::init: ");
}

function AIEngageTurretTask::assume(%task, %client)
{
   %task.setWeightFreq(4);
   %task.setMonitorFreq(4);
}

function AIEngageTurretTask::retire(%task, %client)
{
//error(getTaggedString(%client.name) @ " AIEngageTurretTask::retire: ");
   %client.engageTurret = -1;
   %client.setEngagetarget(-1);
}

function AIEngageTurretTask::weight(%task, %client)
{
   //see if we're still fighting a turret
   %elapsedTime = getSimTime() - %task.startAttackTime;
   if (isObject(%task.engageTurret) && %task.engageTurret.getDataBlock().getClassName() $= "TurretData")
   {
      if (%task.engageTurret == %client.lastdamageTurret)
      {
         if (%task.engageTurret.isEnabled() && getSimTime() - %client.lastDamageTurretTime < 5000)
            %task.setWeight($AIWeightReturnTurretFire);
         else
            %task.setWeight($AIWeightDestroyTurret);
      }
      else if (AIClientIsAlive(%client, %elapsedTime))
      {
         //if another turret is shooting us, disable this one first...
         if (isObject(%client.lastDamageTurret) && %client.lastDamageTurret.getDataBlock().getClassName() $= "TurretData")
         {
            if (%task.engageTurret.isEnabled())
               %task.setWeight($AIWeightReturnTurretFire);
            else
            {
               //see if we need to switch to the new turret
               if (%client.lastDamageTurret.isEnabled() && %client.lastDamageTurretTime < 5000)
               {
                  %task.engageTurret = %client.lastDamageTurret;
                  %task.attackInitted = false;
                  %task.setWeight($AIWeightDestroyTurret);
               }
               else
                  %task.setWeight($AIWeightReturnTurretFire);
            }
         }
         else
         {
            if (%task.engageTurret.isEnabled() && getSimTime() - %client.lastDamageTurretTime < 5000)
               %task.setWeight($AIWeightReturnTurretFire);
            else
               %task.setWeight($AIWeightDestroyTurret);
         }
      }
      //else we died since - clear out the vars
      else
      {
         %task.engageTurret = -1;
         %task.setWeight(0);
      }
   }
   //else see if we have a new target - modified so only enemy turrets will be destroyed - Lagg... 11-4-2003
   else if (isObject(%client.lastDamageTurret) && %client.lastDamageTurret.getDataBlock().getClassName() $= "TurretData" && %client.lastDamageTurret.team != %client.team)
   {
      %task.engageTurret = %client.lastDamageTurret;
      %task.attackInitted = false;

      // throw Tactical Nuke when attacked by turret    - pinkpanther
      %distToTurret = VectorDist(%client.player.getWorldBoxCenter(), %client.lastDamageTurret.getWorldBoxCenter());
      if (%client.getSkillLevel() > 0.7 && %client.player.getInventory("TacticalNuke") > 0 && %client.player.getDamagePercent() > 0.7 && %distToTurret > 0 && %distToTurret < TacticalNukeThrown.damageRadius)
      {
         %client.player.throwStrength = 1.0;
         %client.player.use("TacticalNuke");
      }


      if (%client.lastDamageTurret.isEnabled() && %client.lastDamageTurretTime < 5000)
         %task.setWeight($AIWeightReturnTurretFire);
      else
         %task.setWeight($AIWeightDestroyTurret);
   }
   //else no turret to attack...  (later, do a query to find turrets before they attack)
   else
   {
      %task.engageTurret = -1;
      %task.setWeight(0);
   }
}

function AIEngageTurretTask::monitor(%task, %client)
{
//echo (getTaggedString(%client.name) SPC %task.engageTurret.getDataBlock().getClassName());
   if (isObject(%task.engageTurret) && %task.engageTurret.getDataBlock().getClassName() $= "TurretData")
   {
      //set the AI to fire at the turret
      %client.setEngageTarget(-1);

      //check for LOS - Lagg..
      %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType |
              $TypeMasks::StaticTSObjectType | $TypeMasks::ForceFieldObjectType;
      %turLOS = !containerRayCast(%client.player.getMuzzlePoint($WeaponSlot), %task.engageTurret.getWorldBoxCenter(), %mask, 0);
      if (%turLOS)
         %client.setTargetObject(%task.engageTurret);
      else
         %client.setTargetObject(-1);
      %clientPos = %client.player.getWorldBoxCenter();
      %turretPos = %task.engageTurret.getWorldBoxCenter();

      //control the movement - first, hide, then wait, then attack
      if (!%task.attackInitted)
      {
         %task.attackInitted = true;
         %task.startAttackTime = getSimTime();
         %task.hideLocation = %client.getHideLocation(%turretPos, 40.0, %clientPos, 4.0);
         %client.stepMove(%task.hideLocation, 2.0);
      }
      else if (getSimTime() - %task.startAttackTime > 5000)
      {
         %client.stepMove(%task.engageTurret.getWorldBoxCenter(), 8.0);
         // Added - Lagg..
         if (vectorDist(%client.player.getWorldBoxCenter(), %task.engageTurret.getWorldBoxCenter()) < 5)
            %client.setDangerLocation(%task.engageTurret.getWorldBoxCenter(), 20);
      }
   }
}

//-----------------------------------------------------------------------------
//AIAvoidMineTask is responsible for detecting/destroying enemy mines...
//modified so bots only detect mines they can see, if detected also acknowledge
//the danger. - Lagg... 11-12-2003

function AIDetectMineTask::init(%task, %client)
{
}

function AIDetectMineTask::assume(%task, %client)
{
   %task.setWeightFreq(7);
   %task.setMonitorFreq(7);
}

function AIDetectMineTask::retire(%task, %client)
{
   %task.engageMine = -1;
   %task.attackInitted = false;
   %client.setTargetObject(-1);
}

function AIDetectMineTask::weight(%task, %client)
{
   //crappy hack, but they need the proper weapon before they can destroy a mine...
   %player = %client.player;
   if (!isObject(%player))
      return;

   %hasPlasma = (%player.getInventory("Plasma") > 0) && (%player.getInventory("PlasmaAmmo") > 0);
   %hasDisc = (%player.getInventory("Disc") > 0) && (%player.getInventory("DiscAmmo") > 0);
   // Triumph weapons   - pinkpanther
   %hasBeamSword     = (%player.getInventory("BeamSword") > 0 && (%distToTarg < 18) && %canUseEnergy && %hasLOS);
   %hasDualBlaster   = (%player.getInventory("DualBlaster") > 0 && %canUseEnergy && %hasLOS);
   %hasGatlingGun    = (%player.getInventory("GatlingGun") > 0 && %player.getInventory("GatlingGunAmmo") > 0);
   %hasHeavyLauncher = (%player.getInventory("HeavyLauncher") > 0 && %player.getInventory("HeavyLauncherAmmo") > 0);
   %hasHornet        = (%player.getInventory("Hornet") > 0 && %player.getInventory("HornetAmmo") > 0) && (%playerVelocity < 1.0) && %hasLOS;
   %hasNapalm        = (%player.getInventory("Napalm") > 0 && %canUseEnergy) && !%inWater;
   %hasRailNade      = (%player.getInventory("RailNade") > 0 && %player.getInventory("RailNadeAmmo") > 0);
   %hasShotgun       = (%player.getInventory("Shotgun") > 0 && %player.getInventory("ShotgunAmmo") > 0);
   %hasSlugRifle     = (%player.getInventory("SlugRifle") > 0 && %player.getInventory("SlugRifleAmmo") > 0) && %hasLOS;
   %hasTitanChaingun = (%player.getInventory("TitanChaingun") > 0 && %player.getInventory("TitanChaingunAmmo") > 0);

   if (!%hasPlasma && !%hasDisc && !%hasHornet && !%hasNapalm && !%hasRailNade && !%hasTitanChaingun && !%hasSlugRifle
      && !%hasShotgun && !%hasHeavyLauncher && !%hasGatlingGun && !%hasDualBlaster && !%hasBeamSword)
   {
      %task.setWeight(0);
      return;
   }

   //if we're already attacking a mine,
   if (%task.engageMine > 0 && isObject(%task.engageMine))
   {
      %task.setWeight($AIWeightDetectMine);
      return;
   }
   // only attack enemy mines in own half of the area -  pinkpanther
   %clientPos = %client.player.getWorldBoxCenter();
   %enemyTeam = (%client.team == 1) ? 2 : 1;
   %inEnemyLand = VectorDist(%clientPos, $AITeamFlag[%enemyTeam].originalPosition) < VectorDist(%clientPos, $AITeamFlag[%client.team].originalPosition);

   //see if we're within the viscinity of a new (enemy) mine
   %task.engageMine = -1;
   %closestMine = -1;
   %closestDist = 15; //initialize so only mines within 15 m will be detected...
   %mineCount = $AIDeployedMineSet.getCount();
   for (%i = 0; %i < %mineCount; %i++)
   {
      %mine = $AIDeployedMineSet.getObject(%i);
      %mineTeam = %mine.sourceObject.team;
      //if ((%mineTeam == %client.team) && %inEnemyLand) // don't attack our own team's mines in enemy land... -  pinkpanther
      if (%mineTeam == %client.team) // don't attack our own team's mines
         continue;

      %minePos = %mine.getWorldBoxCenter();
      %clPos = %client.player.getWorldBoxCenter();

      //see if the mine is the closest...
      %mineDist = VectorDist(%minePos, %clPos);
      if (%mineDist < %closestDist)
      {
         //now see if we're more or less heading towards the mine...
         %clVelocity = %client.player.getVelocity();
         %clVelocity = getWord(%clVelocity, 0) SPC getWord(%clVelocity, 1) SPC "0";
         %mineVector = VectorSub(%minePos, %clPos);
         %mineVector = getWord(%mineVector, 0) SPC getWord(%mineVector, 1) SPC "0";
         if (VectorLen(%clVelocity) > 2.0 && VectorLen(%mineVector > 2.0))
         {
            %clNormal = VectorNormalize(%clVelocity);
            %mineNormal = VectorNormalize(%mineVector);
            if (VectorDot(%clNormal, %mineNormal) > 0.3)
            {
               %closestMine = %mine;
               %closestDist = %mineDist;
            }
         }
      }
   }

   //check for LOS
   if (isObject(%closestMine))
   {
      %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
	%mineLOS = !containerRayCast(%player.getMuzzlePoint($WeaponSlot), %closestMine.getWorldBoxCenter(), %mask, 0);
   }

   //see if we found a mine to attack
   if (%closestMine > 0 && %mineLOS)
   {
      %task.engageMine = %closestMine;
      %task.attackInitted = false;
      %task.setWeight($AIWeightDetectMine);
   }
   else
      %task.setWeight(0);

   AISensorJammerActivation(%client);
}

function AIDetectMineTask::monitor(%task, %client)
{
   if (%task.engageMine > 0 && isObject(%task.engageMine))
   {
      if (!%task.attackInitted)
      {
         %task.attackInitted = true;
         %client.stepRangeObject(%task.engageMine, "DefaultRepairBeam", 6, 12);
         %client.setEngageTarget(-1);
         %client.setTargetObject(-1);
      }
      else if (%client.getStepStatus() $= "Finished")
      {
         %client.setTargetObject(%task.engageMine);
         %client.setDangerLocation(%task.engageMine.position, 15);
      }
   }
}

//------------------------------------------------------------------------------------
//New AIDefault Task                       ------                    Lagg... 3-20-2003
//AIDetectRemeqTask is responsible for finding and killing enemy remote inventories...
//------------------------------------------------------------------------------------

function AIDetectRemeqTask::init(%task, %client)
{
}

function AIDetectRemeqTask::assume(%task, %client)
{
   %task.setWeightFreq(7);
   %task.setMonitorFreq(7);
}

function AIDetectRemeqTask::retire(%task, %client)
{
   %task.engageRemeq = -1;
   %task.attackInitted = false;
   %client.setTargetObject(-1);
   %client.engageRemeq = -1;
}

function AIDetectRemeqTask::weight(%task, %client)
{
   %player = %client.player;
   if (!isObject(%player)) {
//error (getTaggedString(%client.name) SPC "!isObject(%player)");
      return;
    }

   //do we have the ammo
   %outOfStuff = (AIAttackOutofAmmo(%client));
   if (%outOfStuff)
   {
      %task.engageRemeq = -1;
      %client.engageRemeq = -1;
      %task.setWeight(0);
//error (getTaggedString(%client.name) SPC "OutOfStuff!!!!!!!");
      return;
   }

   //see if we're already attacking a remote inventory
   if (%task.engageRemeq > 0 && isObject(%task.engageRemeq) && %client.engageRemeq > 0)
   {
      %objName = %task.engageRemeq.getDataBlock().getName();
      if (strstr(%objName,"TurretDeployed")>=0) {
         %AIWeightDetect = $AIWeightDetectDeplTurret;
      }
      else if (%objName $= "DeployedForceField" || %objName $= "Deployedwall")
         %AIWeightDetect = $AIWeightDetectFF;
      else
         %AIWeightDetect = $AIWeightDetectRemeq;
      if (!%task.engageRemeq.owner.isAIControlled())        // if (isObject(%task.engageRemeq.owner)) Not much sense in killing stuff deployed by bots
         %AIWeightDetect += $AIWeightDetectDeplByHuman;
      // enemy stuff in our half of the area gets higher weight -  pinkpanther
      %objPos = %task.engageRemeq.getWorldBoxCenter();
      %enemyTeam = (%client.team == 1) ? 2 : 1;
      %inHomeLand = 1 - VectorDist(%objPos, $AITeamFlag[%client.Team].originalPosition) / VectorDist($AITeamFlag[%enemyTeam].originalPosition, $AITeamFlag[%client.team].originalPosition);
      %AIWeightDetect += mFloor($AIWeightDetectEnemyStuffNearBase * %inHomeLand);

      %task.setWeight(%AIWeightDetect);
//echo (getTaggedString(%client.name) SPC "setWeight" SPC %obj SPC %objName SPC %AIWeightDetect);
   }
   //see if we are still alive
   if (!AIClientIsAlive(%client)) {
      %task.engageRemeq = -1;
      %client.engageRemeq = -1;
	//%client.setEngagetarget(-1);
   } else {
      //lets find some remote equipment (inventories)...
      %task.engageRemeq = -1;
      %client.engageRemeq = -1;
	   %closestRemeq = -1;
	   %closestDist = 180;	//initialize so only deployed enemy stuff within 180 m will be detected
      %depCount = 0;
      %depGroup = nameToID("MissionCleanup/Deployables");
      if (isObject(%depGroup))
      {
         %depCount = %depGroup.getCount();
   	   for (%i = 0; %i < %depCount; %i++)
   	   {
   	      %obj = %depGroup.getObject(%i);
   	      %objName = %obj.getDataBlock().getName();
   	      if ((((%objName $= "DeployedStationInventory" || %objName $= "DeployedMotionSensor" || %objName $= "DeployedPulseSensor" || (strstr(%objName,"TurretDeployed")>=0) || (strstr(%objName,"Beacon")>=0)  // || (strstr(%objName,"BeaconDeployed")>=0)
               || %objName $= "TelePadDeployedBase" || %objName $= "TurretBaseLarge") && %obj.isEnabled()) || %objName $= "DeployedForceField" || %objName $= "Deployedwall")
               && %obj.team != %client.team) //  || %objName $= "DeployedBunker"
            {
               %remeqPos = %obj.getWorldBoxCenter();
               %remeqDist = VectorDist(%player.getWorldBoxCenter(), %remeqPos);
   	         if (%remeqDist < %closestDist)
               {
                  //check for LOS
   	            if (%objName $= "DeployedForceField")
   	               %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticTSObjectType;
   	            else
                     %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticTSObjectType | $TypeMasks::ForceFieldObjectType;
   	            %remeqLOS = !containerRayCast(%player.getMuzzlePoint($WeaponSlot), %obj.getWorldBoxCenter(), %mask, 0);
                  if (%remeqLOS)
                  {
//echo (getTaggedString(%client.name) SPC "FOR-Loop:" SPC %obj SPC %objName SPC "Dist" SPC %remeqDist SPC "cTeam" SPC %client.team SPC "oTeam" SPC %obj.team);
                     %closestDist = %remeqDist;
                     %task.engageRemeq = %obj;
                     %client.engageRemeq = %obj;
                  }
               }
            }
         }
      }
      if (%task.engageRemeq == -1) {
//if (%objName $= "DeployedForceField" || %objName $= "Deployedwall") echo (getTaggedString(%client.name) SPC "setWeight(0)" SPC %obj SPC %objName SPC "cTeam" SPC %client.team SPC "oTeam" SPC %obj.team);
         %task.setWeight(0);
      }
   }

   AISensorJammerActivation(%client);
}

function AIDetectRemeqTask::monitor(%task, %client)
{
   %obj = %task.engageRemeq;
   if (isObject(%obj)) {
      %task.objName = %obj.getDataBlock().getName();
      %task.objTeam = %obj.team;
      if (AIClientIsAlive(%client) && %client.engageRemeq > 0 && (%task.engageRemeq.isEnabled() || %task.objName $= "DeployedForceField" || %task.objName $= "Deployedwall" || (strstr(%task.objName,"Beacon")>=0)))
      {
         %inwater = %client.enviroment;
         %hasMortar = (%client.player.getInventory("Mortar") > 0) && (%client.player.getInventory("MortarAmmo") > 0 && !%inWater);
         %hasMortarStorm = (%client.player.getInventory("MortarStorm") > 0) && (%client.player.getInventory("MortarStormAmmo") > 0 && !%inWater);
         %hasHornet = (%client.player.getInventory("Hornet") > 0 && %client.player.getInventory("HornetAmmo") > 0);
         %hasSolarCharge = (%client.player.getInventory("SolarCharge") > 0);
         %hasRailNade  = (%client.player.getInventory("RailNade") > 0 && %client.player.getInventory("RailNadeAmmo") > 0);
         %hasRailCannon = %client.player.getInventory("RailCannon") > 0 && %client.player.getInventory("RailCannonAmmo") > 0;
         %hasMissile = %client.player.getInventory("MissileLauncher") > 0 && %client.player.getInventory("MissileLauncherAmmo") > 0;
         %hasDisc = %client.player.getInventory("Disc") > 0 && %client.player.getInventory("DiscAmmo") > 0;
         %hasWidowMaker = (%client.player.getInventory("WidowMaker") > 0) && (%client.player.getInventory("WidowMakerAmmo") > 0) && !%inWater;
         %hasLongRangeWeapon = %hasMortar || %hasMortarStorm || %hasMissile || %hasHornet || %hasSolarCharge || %hasRailNade || %hasRailCannon || %hasDisc || %hasWidowMaker;

         %remeqPos = %obj.getWorldBoxCenter();
         %remeqDist = VectorDist(%client.player.getWorldBoxCenter(), %remeqPos);

         %currentTime = getSimTime();

         if (%currentTime > %task.needToRangeTime)
         {
            //force a rangeObject every 2 seconds...
            %task.needToRangeTime = %currentTime + 2000;
            //check for LOS
            if (%task.objName $= "DeployedForceField")
               %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticTSObjectType;
            else
               %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticTSObjectType | $TypeMasks::ForceFieldObjectType;
   	      %remeqLOS = !containerRayCast(%client.player.getMuzzlePoint($WeaponSlot), %task.engageRemeq.getWorldBoxCenter(), %mask, 0);
            if (%task.objName $= "TurretBaseLarge" && %remeqLOS) {
               if (%hasLongRangeWeapon)
                  %client.stepRangeObject(%task.engageRemeq, "BasicTargeter", 40, 180);
               else
                  %client.stepMove(%task.engageRemeq.getWorldBoxCenter(), 5.0);
            } else {
               if (%hasLongRangeWeapon && %remeqLOS) {
                  %client.stepRangeObject(%task.engageRemeq, "ChaingunBullet", 12, 70);
//echo (getTaggedString(%client.name) SPC "Monitor:stepRangeObject70" SPC %obj SPC %task.objName SPC "cTeam" SPC %client.team SPC "oTeam" SPC %obj.team SPC "remeqLOS" SPC %remeqLOS);
               }
               else {
                  %client.stepRangeObject(%task.engageRemeq, "ChaingunBullet", 8, 15);
               }
            }
         }
         else if (!%task.attackInitted)
         {
            %task.attackInitted = true;
            %client.setEngageTarget(-1);
            if ($verbose >= 1) logEcho (%client.nameBase SPC "Attacking Team" SPC %task.engageRemeq.team SPC %task.objName);
         }
         if (%hasLongRangeWeapon && %remeqDist > 40)
            %client.setTargetObject(%task.engageRemeq, 300, "Mortar");
         else
            %client.setTargetObject(%task.engageRemeq, 300, "Destroy");
         return;
      }
   }
   //else we are done
//error (getTaggedString(%client.name) SPC "Monitor:done" SPC %obj SPC %task.objName SPC "cTeam" SPC %client.team SPC "oTeam" SPC %obj.team);
   //if ($verbose >= 1)
      logEcho ("Team " @ %task.objTeam SPC %task.objName @ " destroyed by " @ %client.nameBase);
   %task.engageRemeq = -1;
   %client.engageRemeq = -1;
   %task.attackInitted = false;
   %client.setTargetObject(-1);
   %task.setWeight(0);
}

//------------------------------------------------------------------------------------
//AICouldUseInventoryTask will cause them to use an inv station if they're low
//on ammo or health.  This is a new task in beta.                 - Lagg... 11-12-2003
//------------------------------------------------------------------------------------

function AICouldUseInventoryTask::init(%task, %client)
{
}

function AICouldUseInventoryTask::assume(%task, %client)
{
   %task.setWeightFreq(35);
   %task.setMonitorFreq(5);

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();
}

function AICouldUseInventoryTask::retire(%task, %client)
{
   //reset the state machine time stamp...
   %task.buyInvTime = getSimTime();
}

function AICouldUseInventoryTask::weight(%task, %client)
{
   //first, see if we are all there
   %player = %client.player;
   if (!isObject(%player))
      return;

   %damage = %player.getDamagePercent();
   %weaponry = AIEngageWeaponRating(%client);

if ($debugAICouldUseInventoryTask) echo(getTaggedString(%client.name) @ " AICouldUseInventoryTask %t.descr" SPC %task.description SPC  "%t.eq" SPC  %task.equipment SPC  "%t.desEq" SPC  %task.desiredEquipment SPC  "%weaponry" SPC  %weaponry);
   //first, see if we need equipment or health
   if (%damage < 0.3 && %weaponry >= 40)
   {
      %task.setWeight(0);
      return;
   }

   //don't use inv stations if we're not that badly damaged, and we're in the middle of a fight
   if (%damage < 0.6 && %client.getEngageTarget() > 0 && !AIEngageOutOfAmmo(%client))
   {
      %task.setWeight(0);
      return;
   }

   //if we're already buying, continue
   if (%task.buyInvTime == %client.buyInvTime)
   {
      //set the weight - if our damage is above 0.8 or our we're out of ammo
      if (%damage > 0.8 || AIEngageOutOfAmmo(%client))
         %task.setWeight($AIWeightNeedItemBadly);
      else
         %task.setWeight($AIWeightNeedItem);

      return;
   }

   //we need to search for an inv station near us...
   %result = AIFindClosestInventory(%client, false);
   %closestInv = getWord(%result, 0);
   %closestDist = getWord(%result, 1);

   //only use inv stations if we're right near them...
   if (%closestDist > 150)
   {
      %task.setWeight(0);
      return;
   }

   //check for LOS - Lagg...
   %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType;
   %closestInvLOS = !containerRayCast(%player.getWorldBoxCenter(), %closestInv.getWorldBoxCenter(), %mask, 0);

   //set the weight...
   %task.closestInv = %closestInv;
   if (%damage > 0.8 || AIEngageOutOfAmmo(%client))
      %task.setWeight($AIWeightNeedItemBadly);
   else if (%closestDist < 100 && (AIEngageWeaponRating(%client) <= 30 || %damage > 0.4))
      %task.setWeight($AIWeightNeedItem);
   else if (%closestInvLOS)
      %task.setWeight($AIWeightFoundItem);
   else
      %task.setWeight(0);
}

function AICouldUseInventoryTask::monitor(%task, %client)
{
   //make sure we still need equipment
   %player = %client.player;
   if (!isObject(%player))
      return;

   //if we are picking up an item or health forget it - Lagg...
   //if (%client.pickUpItem > 0)
   //return;

   %damage = %player.getDamagePercent();
   %weaponry = AIEngageWeaponRating(%client);
   if (%damage < 0.3 && %weaponry >= 40 && !%client.spawnUseInv)
   {
      %task.buyInvTime = getSimTime();
      return;
   }

   if (%client.objectiveTask.buyEquipmentSet !$= "")
      %buySet = %client.objectiveTask.buyEquipmentSet;
   //if we got an inventory station make sure we keep it
//   else if (%player.getInventory("InventoryDeployable"))
//      %buySet = "MediumInventorySet HeavyInventorySet";
   else
   {
      //pick a random set based on armor...
      %randNum = getRandom();
	if (%randNum < 0.4)
         %buySet = "MediumEnergySet LightEnergySniper LightSensorJammer ReconShotgun FieldTechAmmoSet HeavyEnergySet TitanNuke";
	else if (%randNum < 0.6)
         %buySet = "LightEnergyDefault LightEnergySniper MediumEnergySet FieldTechAmmoSet HeavyEnergySet TitanNuke";
	else if (%randNum < 0.8)
         %buySet = "FieldTechAmmoSet ReconDeepCover LightEnergySniper LightCloakSet MediumMissileSet HeavyAmmoSet TitanNuke";
        else
         %buySet = "ReconShotgun LightShieldSet MediumShieldSet FieldTechAmmoSet HeavyShieldOff TitanNuke";
   }
if ($debugAICouldUseInventoryTask) error("AICouldUseInventoryTask::monitor - " @ getTaggedString(%client.name) @ " buy.Set:"@%buySet);

   //process the inv buying state machine
   %result = AIBuyInventory(%client, "", %buySet, %task.buyInvTime);
   if (%result $= "InProgress")
   {
      //force a nervous reaction every 10 sec for this task - Lagg...
      if (getSimTime() - %task.buyInvTime > 10000)
      {
         %client.setDangerLocation(%client.player.getWorldBoxCenter(), 15);
         %task.buyInvTime = getSimTime();
      }
      return;
   }

   //if we succeeded or failed, reset the state machine...
   if (%result !$= "InProgress")
      %task.buyInvTime = getSimTime();


   //this call works in conjunction with AIEngageTask
   %client.setEngageTarget(%client.shouldEngage);
}

//------------------------------------------------------------------------------------
//New AIDefault Task                       ------                    Lagg... 11-6-2003
//AIDetectVehicleTask is for detecting / destroying enemy vehicles manned or unmanned
//------------------------------------------------------------------------------------

function AIDetectVehicleTask::init(%task, %client)
{
}

function AIDetectVehicleTask::assume(%task, %client)
{
   %task.setWeightFreq(30);
   %task.setMonitorFreq(30);
}

function AIDetectVehicleTask::retire(%task, %client)
{
   %task.engageVehicule = -1;
   %client.setTargetObject(-1);
}

function AIDetectVehicleTask::weight(%task, %client)
{
   %player = %client.player;
   if (!isObject(%player))
      return;

   //don't do this at lower skill levels
   %skill = %client.getSkillLevel();
   if (%skill <= 0.7)
   {
      %task.setWeight(0);
      return;
   }

   %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticTSShapeObjectType;

   //if we're already attacking a Vehicle set weight, let monitor have it
   if (isObject(%task.engageVehicle) && %task.engageVehicle > 0)
   {
      %hasLOS = !containerRayCast(%player.getMuzzlePoint($WeaponSlot), %task.engageVehicle.getWorldBoxCenter(), %mask, 0);
      if (%hasLOS || getSimTime() - %task.engageVehicleTime < 40000)
      {
         //check for ammo
         %hasWidowMaker    = %player.getInventory("WidowMaker") > 0 && %player.getInventory("WidowMakerAmmo") > 0;
         %hasAntiAirGun    = %player.getInventory("AntiAirGun") > 0 && %player.getInventory("AntiAirGunAmmo") > 0;
         %hasMissile       = %player.getInventory("MissileLauncher") > 0 && %player.getInventory("MissileLauncherAmmo") > 0;
         %hasHornet        = %player.getInventory("Hornet") > 0 && %player.getInventory("HornetAmmo") > 0;
         %hasRailCannon    = %player.getInventory("RailCannon") > 0 && %player.getInventory("RailCannonAmmo");
         %hasTitanChaingun = %player.getInventory("TitanChaingun") > 0 && %player.getInventory("TitanChaingunAmmo") > 0;
         %hasSolarCharge   = %player.getInventory("SolarCharge") > 0;
         %hasSensorJammerPack = %player.getInventory("SensorJammerPack") > 0 || %player.getInventory("AntiEnergyPack") > 0;
         if (!(%hasWidowMaker || %hasAntiAirGun || %hasMissile || %hasRailCannon || %hasTitanChaingun || %hasSolarCharge))
         {
            //error("AIDetectVehicleTask::weight - out of ammo - " @ %client.objectiveTask.getName() SPC getTaggedString(%client.name));
            %task.engageVehicle = -1;
            %client.setTargetObject(-1);
            %task.setWeight(0);
            return;
         }

         //some vehicles are a greater threat than others
         %type = %task.engageVehicle.getDataBlock().getName();
         if (%type $= "MobileBaseVehicle" && %task.engageVehicle.fullyDeployed)// could use ".deployed" here ***
         {
            if (%hasWidowMaker)
               %weight = $AIWeightDetectVehicle;
            else if (%hasAntiAirGun)
               %weight = $AIWeightDetectVehicle -50;
            else if (%hasMissile || %hasRailCannon || %hasSolarCharge)
               %weight = $AIWeightDetectVehicle -125;
            else
               %weight = $AIWeightFoundEnemy;
            if (%hasSensorJammerPack) %weight += 100;
         }
         else if (VectorLen(%task.engageVehicle.getVelocity()) > 18)  // speedy
         {
            if (%hasMissile)
               %weight = $AIWeightDetectVehicle -75;
            else if (%hasAntiAirGun || %hasRailCannon)
               %weight = $AIWeightDetectVehicle -125;
            else
               %weight = $AIWeightFoundEnemy;
         }
         else
         {
            if (%hasAntiAirGun)
               %weight = $AIWeightDetectVehicle -75;
            else if (%hasSolarCharge || %hasRailCannon || %hasTitanChaingun || %hasWidowMaker || %hasMissile || %hasHornet)
               %weight = $AIWeightDetectVehicle -125;
            else
               %weight = $AIWeightFoundEnemy;
         }
         if (%hasLOS) {
            %weight += 100;
            %task.engageVehicleTime = getSimTime();
         }
   //      error(getTaggedString(%client.name) SPC "AIDetectVehicleTask:Weight hasLOS " @ %weight);
         %task.setWeight(%weight);
         return;
      }
   }

   //see if we're within the viscinity of a new (enemy) vehicle
   %task.engageVehicle = -1;

   %search = aiFindClosestEnemyVehicle(%client);//--new function in aiOR_Vehicle.cs
   %closestVehicle  = getWord(%search, 0);
   %closestDist = getWord(%search, 1);

   //if we found a vehicle to attack
   if (%closestVehicle > 0 && %closestDist < 320)
   {
      //check for LOS
      %vehicleLOS = !containerRayCast(%player.getMuzzlePoint($WeaponSlot), %closestVehicle.getWorldBoxCenter(), %mask, 0);

      if (%vehicleLOS) {
         %task.engageVehicle = %closestVehicle;
         %task.engageVehicleTime = getSimTime();
      }
      else
         %task.setWeight(0);
   }
   else
      %task.setWeight(0);

   AISensorJammerActivation(%client);
}

function AIDetectVehicleTask::monitor(%task, %client)
{
   %player = %client.player;
   if (!isObject(%player) || !isObject(%task.engageVehicle))
      return;

   //check the distance
   if (isObject(%task.engageVehicle) && %task.engageVehicle > 0)
      %vehicleDist = vectorDist(%client.player.getWorldBoxCenter(), %task.engageVehicle.getWorldBoxCenter());
   else
      %vehicleDist = 99999;
   if (%vehicleDist < 350)// && %vehicleLOS)
   {
      if (%vehicleDist < 15.0)
         %client.setDangerLocation(%task.engageVehicle.getWorldBoxCenter(), 30);

      //check for ammo
      %hasWidowMaker    = %player.getInventory("WidowMaker") > 0 && %player.getInventory("WidowMakerAmmo") > 0;
      %hasMissile       = %player.getInventory("MissileLauncher") > 0 && %player.getInventory("MissileLauncherAmmo") > 0;

      if (%task.engageVehicle.getDataBlock().getName() $= "MobileBaseVehicle" && %task.engageVehicle.fullyDeployed && %hasWidowMaker)
      {
         %client.stepRangeObject(%task.engageVehicle, "BasicTargeter", 40, 300);
         //if (%client.getStepStatus() $= "Finished")
         %client.setTargetObject(%task.engageVehicle, 200, "Mortar");
      if ($watch $= "vehicle") logEcho(getTaggedString(%client.name) SPC "AIDetectVehicleTask:Monitor MobileBaseVehicle && %hasWidowMaker");
      }
      else if (%vehicleDist > 40 && %hasMissile && VectorLen(%task.engageVehicle.getVelocity()) > 18)
      {
         %client.stepRangeObject(%task.engageVehicle, "BasicTargeter", 40, 300);
         //if (%client.getStepStatus() $= "Finished")
         %client.setTargetObject(%task.engageVehicle, 300, "Missile");
      if ($watch $= "vehicle") logEcho(getTaggedString(%client.name) SPC "AIDetectVehicleTask:Monitor Missile");
      }
      else
      {
         %client.stepRangeObject(%task.engageVehicle, "BasicTargeter", 15, 120);
//         if (%client.getStepStatus() $= "Finished") {
            %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticTSShapeObjectType;
            %hasLOS = !containerRayCast(%player.getMuzzlePoint($WeaponSlot), %task.engageVehicle.getWorldBoxCenter(), %mask, 0);
            if (%hasLOS)
               %client.setTargetObject(%task.engageVehicle, 300, "Destroy");
            else
               %client.stepMove(%task.engageVehicle.getWorldBoxCenter(), 25);
      if ($watch $= "vehicle") logEcho(getTaggedString(%client.name) SPC "AIDetectVehicleTask:Monitor Destroy");
//         }
      }
   }
   else
   {
      %task.engageVehicle = -1;
      %client.setTargetObject(-1);
      %task.setWeight(0);
      //this call works in conjunction with AIEngageTask
      //%client.setEngageTarget(%client.shouldEngage);
   }
}

//-----------------------------------------------------------------------------------------------------------------------
//                                                                                                     AISpamLocationTask
//I created this to task to mortar spam people who like to mortar spam my bots :)  - Lagg... 11-15-2003
//-----------------------------------------------------------------------------------------------------------------------
function AISpamLocationTask::init(%task, %client)
{
}

function AISpamLocationTask::assume(%task, %client)
{
   %task.setWeightFreq(6);
   %task.setMonitorFreq(6);
}

function AISpamLocationTask::retire(%task, %client)
{
}

function AISpamLocationTask::weight(%task, %client)
{
   %player = %client.player;
	if (!isObject(%player))
		return;

   if(Game.class $= "SiegeGame")
   {
      if (%client.team != Game.offenseTeam)
      {
         %task.setWeight(0);
         return;
      }
   }

   //don't do this at lower skill levels
   %skill = %client.getSkillLevel();
   if (%skill <= 0.7)
   {
      %task.setWeight(0);
      return;
   }

   %clientPos = %player.getWorldBoxCenter();
   %task.spamlocation = nameToId(SpamLocation).position;
   %task.locationToSpam = nameToId(LocationToSpam).position;

   //only do this if we are in the vicinity to spam
   if (vectorDist(%clientPos, %task.spamLocation) > 16)
   {
      %task.setWeight(0);
      return;
   }

   //do we have the spam?
   %hasMortar = (%player.getInventory("Mortar") > 0) && (%player.getInventory("MortarAmmo") > 0);
   if (!%hasMortar)
   {
      %task.setWeight(0);
      //error("AISpamLocationTask::weight - Mortar Stuff" @ getTaggedString(%client.name));
      return;
   }

   //if we are using the repairpack forget it
   if (isObject(%player.getMountedImage($WeaponSlot)) && %player.getMountedImage($WeaponSlot).getName() $= "RepairGunImage" && %player.getImageState($BackpackSlot) $= "activate" && %player.getDamagePercent() > 0)
   {
      %task.setWeight(0);
      //error("AISpamLocationTask::weight - Repair Pack in usel" @ getTaggedString(%client.name));
      return;
   }

   if (nameToId("SwitchFFGenerator").getDamageState() $= "Destroyed")
   {
      %task.setWeight(0);
      //error("AISpamLocationTask::weight - Destroyed");
      return;
   }

   ////only do this if we got an enemy close to location (cheat ignore LOS) - not used in Fumarole Mission
   //%result = AIFindClosestEnemyToLoc(%client, %task.locationToSpam, 17, $AIClientLOSTimeout, true, 70);
   //%closestEnemy = getWord(%result, 0);

   //if (%closestEnemy <= 0)
   //{
      //%task.setWeight(0);
      //return;
   //}

   //if we are being harrassed by deployed turrets, lets take care of them first
   if (isObject(%client.lastDamageTurret) && %client.lastDamageTurret.getDataBlock().getClassName() $= "TurretData" && %client.lastDamageTurret.team != %client.team)
   {
      %task.setWeight(0);
      return;
   }
   %task.setWeight($AIWeightTauntVictim);

   AISensorJammerActivation(%client);
}

function AISpamLocationTask::monitor(%task, %client)
{
   //make sure we are all there
   %player = %client.player;
   if (!isObject(%player))
      return;

   //check for LOS
   %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType |
     $TypeMasks::ForceFieldObjectType | $TypeMasks::VehicleObjectType;
   %hasLOS = !containerRayCast(%client.player.getMuzzlePoint($WeaponSlot), %task.locationToSpam, %mask, 0);

   if (%hasLOS)
   {
      //%client.stop();
      %client.stepMove(%task.spamLocation, 0.25);
      %client.aimAt(%task.locationToSpam, 3000);
      %player.use("Mortar");
      %client.pressFire(1);

      //error(" AISpamLocationTask::monitor - FIRE! " @ getTaggedString(%client.name));
   }
   else
      %task.setWeight(0);
}
