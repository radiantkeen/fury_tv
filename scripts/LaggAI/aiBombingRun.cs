//-------------------------------------------------------------------------------------------------- AIO Bombing Run ---
//Place AIO Objective Marker in the spot you want pilot to park and wait for passegers
//Set up to 10 simgroups called "T" @ %team @ "BomberPath" @ %num
//(example T2BomberPath1 to T2BomberPath10 / T1BomberPath1 to T1BomberPath10)
//NOTE: must start with number 1 and count in order up to 10
//inside these simgroups place markers in the order to follow for each
//path. Thats 1 simgroup per path (up to 10) for each team - Lagg... 4-8-2004

function AIOBombingRun::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   //do a quick check to disqualify this objective if it can't meet the minimum weight
   if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
   {
      if (%this.targetObjectId > 0 && %this.issuedByClientId == %client.controlByHuman)
      {
         if ($AIWeightHumanIssuedCommand < %minWeight)
	    return 0;
      }
      else
         return 0;
   }

   //check for Vehicle station near buy of forget it if we are noy mounted yet - Lagg...
   //would like to add a check for unpiloted vehicles with passengers that need a ride (someday)
   if (!%client.player.isMounted() && !%client.justBought)
   {
      %clVs = AIFindClosestVStation(%client);
      if (%clVs > 0)
      {
         %closestVs = getWord(%clVs, 0);
         %closestVsDist = getWord(%clVs, 1);
//         if (%closestVsDist > 300)//----------------------* close to VPad or return 0 *---
//            return 0;
      }
      else
         return 0;

      //check if any of vehicle type are availible
//      %blockName = "BomberFlyer";
      %blockName = getRandomVehicleType($VehicleList::Gunships); //"ScoutFlyer";
      %client.aiVehicleConsidering = %blockName;
      if (!vehicleCheck(%blockName, %client.team))
         return 0;
   }

   %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);

   //never bump a pilot off his ride
   if (%this.clientLevel1 > 0  && %level == 1 && %this.clientLevel1 != %client)
      return 0;
   else if (%this.clientLevel1 == %client)
      %weight = 10000;

   return %weight;
}

function AIOBombingRun::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIBombingRun);
   %client.objectiveTask.initFromObjective(%this, %client);

   //create the escort (passenger) objectives
   //make sure everybody has flares :)


   %client.escort = new AIObjective(AIOEscortPlayer)
                     {
                        dataBlock = "AIObjectiveMarker";
                        weightLevel1 = $AIWeightVehicleMountedEscort;
                        weightLevel2 = 0;
                        description = "Escort " @ getTaggedString(%client.name);
                        targetClientId = %client;
                        offense = true;
                        chat = "ChatNeedHold";
                        equipment = "FlareGrenade HeavyLauncher";
                        desiredEquipment = "AmmoPack";
                        buyEquipmentSet = "MediumTailgunner";
                     };

   %client.escort1 = new AIObjective(AIOEscortPlayer)
                     {
                        dataBlock = "AIObjectiveMarker";
                        weightLevel1 = $AIWeightVehicleMountedEscort;
                        weightLevel2 = 0;
                        description = "Escort " @ getTaggedString(%client.name);
                        targetClientId = %client;
                        offense = true;
                        chat = "ChatNeedRide";
                        equipment = "FlareGrenade HeavyLauncher";
                        desiredEquipment = "AmmoPack";
                        buyEquipmentSet = "MediumTailgunner";
                     };

   MissionCleanup.add(%client.escort);
   MissionCleanup.add(%client.escort1);

   //if is siege game we have to do this right - Lagg... 11-3-2003
   if (Game.class $= "SiegeGame")
   {
      //the objectives in $ObjectiveQ[1] are all offense objectives, $ObjectiveQ[2] defense
      if (%client.team == game.offenseTeam)
      {
         $ObjectiveQ[1].add(%client.escort);
         $ObjectiveQ[1].add(%client.escort1);
      }
      else
      {
         $ObjectiveQ[2].add(%client.escort);
         $ObjectiveQ[2].add(%client.escort1);
      }
   }
   //else if not siege game use the default team's Q
   else
   {
         $ObjectiveQ[%client.team].add(%client.escort);
         $ObjectiveQ[%client.team].add(%client.escort1);
   }
}

function AIOBombingRun::unassignClient(%this, %client)
{
   //kill the escort (passenger) objectives
   if (%client.escort)
   {
      clearObjectiveFromTable(%client.escort);//new function below to clear objective from $objTable[] - Lagg... 1-27-2004
      AIClearObjective(%client.escort);
      %client.escort.delete();
      %client.escort = "";
   }
   if (%client.escort1)
   {
      clearObjectiveFromTable(%client.escort1);//new function below to clear objective from $objTable[] - Lagg... 1-27-2004
      AIClearObjective(%client.escort1);
      %client.escort1.delete();
      %client.escort1 = "";
   }
   if(%client.pilotVehicle)
      AIDisembarkVehicle(%client);

   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//----------------------------------------------------------------------------------------------- AI Need A Ride ---

function AIBombingRun::initFromObjective(%task, %objective, %client)
{
   //initialize the task vars from the objective
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;

   //task location gets updated as pilot flys to markers in monitor
   %task.location = %objective.location;

   %task.equipment = %objective.equipment;
   %task.buyEquipmentSet = %objective.buyEquipmentSet;
   %task.desiredEquipment = %objective.desiredEquipment;
   %task.issuedByClient = %objective.issuedByClientId;
   %task.chat = %objective.chat;

   //initialize other task vars
   %task.sendMsg = true;
   %task.sendMsgTime = 0;
   %task.path = "";
   %task.hit = false;//new

   %client.player.mVehicle = "";// needed if bot just hopped out a vehicle and it was not destroyed/removed yet
}

function AIBombingRun::assume(%task, %client)
{
   %task.setWeightFreq(20);
   %task.setMonitorFreq(20);

   %client.needEquipment = AINeedEquipment(%task.equipment, %client);

   //even if we don't *need* equipemnt, see if we should buy some...
   if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
   {
      //see if we could benefit from inventory
      %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      %closestDist = getWord(%result, 1);
      if (AINeedEquipment(%task.desiredEquipment, %client) && %closestInv > 0)
      {
         %result = AIFindClosestEnemy(%client, 100, $AIClientLOSTimeout);
	 %closestEnemy = getWord(%result, 0);
	 %closestEnemydist = getWord(%result, 1);

	 if (%closestEnemy <= 0 || (%closestEnemyDist > %closestDist * 1.5))
	    %client.needEquipment = true;
      }
   }

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();

   //reset the vehicle station wait time
   %client.vsWaitTime = "";

   //set the destination paths for each team and game type

   //first see how many paths we have
   //check if flight path exists
   if (Game.class $= "SiegeGame")
      %team = Game.offenseTeam == %client.team ? 1 : 2;
   else
      %team = %client.team;

   for(%i = 1; %i < 10; %i++)
   {
      if(isObject(nameToId("T" @ %team @ "BomberPath" @ %i)))
         %mx = %i;
   }

   %random = mFloor(getRandom(1, %mx));

   // - set the path simgroup to follow
   %task.group = nameToId("T" @ %team @ "BomberPath" @ %random);

   //%task.group = nameToId("T1BomberPath2");//use this for testing paths - Lagg...

   %task.count = %task.group.getCount();
   %task.locationIndex = 0;
   %client.needVehicle = true;

   %task.shouldWait = true;
}


function AIBombingRun::retire(%task, %client)
{
   if (aiClientIsAlive(%client))
   {
      %client.needVehicle = false;
      %client.clearStep();
      if(%client.player.isMounted())
         AIDisembarkVehicle(%client);
   }
}

function AIBombingRun::weight(%task, %client)
{
   if (%client.player.isMounted() && isObject(%client.player.mVehicle))
      %vehicle = %Client.vehicleMounted;

   ////see if we were hit
   //%lastAttacker = %vehicle.lastDamagedBy;//---------------------------------------- what damaged us ???
   //if (isObject(%lastAttacker))
   //{
      //if (%lastAttacker.isMounted())
         //%lastAttacker = %lastAttacker.getObjectMount();

      //%task.hit = true;//new

      ////%lastClass = %lastAttacker.getDataBlock().getClassName();// - we need the target class to make decisions down below
      //%vehicle.lastDamagedBy = -1;// ----------------------------------------------- CLEAR THE VEHICLE DAMAGED FLAG
   //}

   //update the task weight...
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;

   //let the monitor decide when to quit :)//--- NEEDED SO IMPORTANT !
   %task.setWeight(%task.baseWeight);

}

function AIBombingRun::monitor(%task, %client)
{
   //first, buy equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;
      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
         return;
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(20);
	 %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
         %task.setMonitorFreq(20);
	 %client.needEquipment = false;
      }
   }
   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   if (%client.needVehicle)
   {
      %clVs = AIFindClosestVStation(%client);
      if (%clVs > 0)
      {
         %closestVs = getWord(%clVs, 0);
         %closestVsDist = getWord(%clVs, 1);
      }
      //if (%closestVs > 0 && %closestVsDist < 300 && !isObject(%client.player.mVehicle))
      if (%closestVs > 0 && !isObject(%client.player.mVehicle)) // && %closestVsDist < 300
      {
         //If we're in light or medium armor, buy the vehicle - Lagg...
         if (%client.player.getArmorSize() !$= "Heavy" && %client.player.getArmorSize() !$= "Titan")
         {
            %task.setMonitorFreq(9);
            %buyResult = aiBuyVehicle(%client.aiVehicleConsidering, %client);
         }
         else
         {
            //if ai in heavy armor buy equipment
            if (%task == %client.objectiveTask)
	    {
               %task.baseWeight = %client.objectiveWeight;
               %task.equipment = "Medium || FieldTech HeavyLauncher";
	       %task.buyEquipmentSet = "MediumTailgunner";
               %client.needEquipment = true;
               return;
	    }
         }

         if (%buyResult $= "InProgress")
         {
            //reset pilot's wait time for passengers
            %task.shouldWaitTime = getSimTime();

	    return;
         }

         else if (%buyResult $= "Finished")
         {
            //if we are finished buying vehicle, then we are done
            //this is handled in function Armor::AIonMount - Lagg... 7-9-2004
         }
         else if (%result $= "Failed")
         {
            //if this task is the objective task, choose a new objective
	    if (%task == %client.objectiveTask)
	    {
	       AIUnassignClient(%client);
	       Game.AIChooseGameObjective(%client);
               return;
	    }
         }
      }
      else if ((%closestVs <= 0) && !isObject(%client.player.mVehicle)) // || %closestVsDist >= 300
      {
         if (%task == %client.objectiveTask)
         {
            AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
            return;
         }
      }
   }

   //chat - send the message
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
         {
            if (%task.chat !$= "")
            {
               %chatMsg = getWord(%task.chat, 0);
               %chatTemplate = getWord(%task.chat, 1);
               if (%chatTemplate !$= "")
                  AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
               else
                  AIMessageThread(%task.chat, %client, -1);
            }
         }
      }
   }

   //if we managed to get in vehicle then go
   if (%client.player.isMounted() && isObject(%client.player.mVehicle))
   {
      //update monitor frequency
      %task.setMonitorFreq(20);

      //set a time limit on this task in case he gets stuck somewhere
      %time = getSimTime() - %client.getTaskTime();
//      if (%time > $aiNeedARideTaskTime)
//      {
//         //set the index to the last
//         %task.locationIndex = %task.count - 1;
//
//         //eject, eject, eject
//         AIDisembarkVehicle(%client);
//      }

      //get some vehicle data that we will require
      %vehicle = %Client.vehicleMounted;
      %mySpd = VectorLen(%vehicle.getVelocity());
      %vehData = %vehicle.getDataBlock();
      %skill = %client.getSkillLevel();// --------------------- %skill *

      //find out our altitude
      %myPos = %vehicle.getWorldBoxCenter();//---------------------------------------- %myPos *
      %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType |
        $TypeMasks::ForceFieldObjectType;
      %downVec = getWord(%myPos, 0) @ " " @ getWord(%myPos, 1) @ "0";
      %altSur = containerRayCast(%myPos, %downVec, %mask, 0);
      %altPos = posfromRayCast(%altSur);
      %terHt = getTerrainHeight(%altPos);// ------------------------------------------ %terHt *
      %myAlt = getWord(%myPos, 2) - getWord(%altPos, 2);// --------------------------- %myAlt *

      %myZLev = getWord(%myPos, 2);

      //---------------------------------------------------------------------------------- Avoidance ---
      InitContainerRadiusSearch(%myPos, 5.0, $TypeMasks::InteriorObjectType
        | $TypeMasks::TSStaticShapeObjectType);
      %avoid = containerSearchNext();

      if ((%mySpd <= 0.1 && %avoid) || %mySpd == 0)//this needs some attention either %mySpd is 0 or it is not !
      {

         //%v = %vehicle.getVelocity();
         %vx = getWord(%myPos, 0);
         %vy = getWord(%myPos, 1);
         %vz = getWord(%myPos, 2);
         //%vz += 2;
         //%vz = %terHt + 5;
         %vR = getWords(%vehicle.getTransform(), 3, 6);

         %client.setPilotPitchRange(-0.025, 0.025, 0.025);
         %client.setPilotDestination(%vx SPC %vy SPC %vz + 5, 1.0);// - max speed
         %vehicle.setTransform(%vx SPC %vy SPC %vz + 2 SPC %vR);// - set the Transform Here up a little
         %client.pressJet();// - hit the gas

         //error("AIBombingRun::monitor - We Are Stuck - " @ %client.nameBase);
         //echo("AIBombingRun::monitor - Altitude = " @ %myAlt);
         //echo("AIBombingRun::monitor - SPEED = " @ %mySpd);
         //echo(" ");
         //return;

      }
      //-------------------------------------------------------------------------------- End Avoidance ---

      // - if we have an empty seat, wait for ($aiVehicleWaitTime) milliseconds

      //tower this is bluebird requesting clearance to take off
      if (%task.shouldWait)
      {
         // - get the passenger seat: 0 = empty 1 = full
//         for(%i = 0; %i < %vehicle.getDataBlock().numMountPoints; %i++)
//         {
//            %empty = false;
//            if (!%vehicle.getMountNodeObject(%i))
//            {
//	       %empty = true;
//	       break;
//            }
//         }
//         if (!%empty || getSimTime() - $aiVehicleWaitTime > %task.shouldWaitTime)
         if (%vehicle.getMountNodeObject(1) && (%vehicle.getMountNodeObject(2) || getSimTime() - $aiVehicleWaitTime > %task.shouldWaitTime))  // Bombardier is mandatory, while for TG we use the timeout $aiVehicleWaitTime
            %task.shouldWait = false;

         %pos = %client.vehicleMounted.position;
         %dest = %task.location;

         %client.setPilotPitchRange(-0.025, 0.025, 0.025);//real smooth setting here

         // - if can't see location marker gain height
         %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
         %hasLOS = !containerRayCast(%myPos, %task.location, %mask, 0);//------- %hasLOS to %task.location ?

         //if we get blocked fly straight up a little
         if (!%hasLOS)
         {
            %x = firstWord(%myPos);
            %y = getWord(%myPos, 1);
            %z = getWord(%myPos, 2);
            %client.setPilotPitchRange(-0.025, 0.025, 0.025);//real smooth setting here
            %client.setPilotDestination(%x SPC %y SPC %z + 5, 1.0);// -- if we can't see %task.location move straight up ^
         }
         //else fly straight to task location marker
         else
         {
            if (VectorDist(%dest, %pos) < 10)
               %client.setPilotDestination(%task.location, 0); //stop and wait
            else if (VectorDist(%dest, %pos) < 25)
               %client.setPilotDestination(%task.location, 0.05); //move real,real slow
            else
	       %client.setPilotDestination(%task.location, 0.3); //move to wait spot, slow
         }
      }
      //wait time is up or we are fully loaded
      else
      {
         //clear for take off, please fasten your seat belts :)
         %client.setPilotPitchRange(-0.005, 0.005, 0.0025);

         //set the flight path
         %task.location = %task.group.getObject(%task.locationIndex).position;

         %pos = %client.vehicleMounted.position;
	 %dest = %task.location;

         //are we close to location index marker?
	 if (VectorDist(%dest, %pos) < 75)//75 meters from marker
	 {
	    //if we have another location index
            if ((%task.count - 1) > %task.locationIndex)//has to be -1 on the groups count
               %task.locationIndex++;
            //we are at end of trail
            else if (%vehicle.getMountNodeObject(1) > 0)  // we still have a bombardier -> do not eject yet, but go one step back
               %task.locationIndex--;
            else
            {
              if (%task == %client.objectiveTask)
	       {
                 if (%client.vehicleMounted )
                 {
                     AIDisembarkVehicle(%client); //Hop off...
                     %client.stepMove(%task.location, 0.25);
                     return;
                 }
	       }
            }
         }
         // ------------------ FLIGHT CONTROL -----------------
         else
         {
            // - lets keep the pilot at about same height as location marker
            // - Good Luck if you want to mess around with these settings.

            // ---------------------------------------- lets get some data
            %transform = %vehicle.getTransform();
            %x = firstWord(%transform);
            %y = getWord(%transform, 1);
            %z = getWord(%transform, 2);
            //%xR = getWord(%transform, 4);
            //%yR = getWord(%transform, 5);
            //%zR = getWord(%transform, 6);
            //%wR = getWord(%transform, 7);

            //echo("My Speed = " @ %mySpd);

            //use the Y-axis of the vehicle rotation as the desired direction to check
            //and calculate a point 125m in front of vehicle - Lagg...
            %point = MatrixMulVector("0 0 0 " @ getWords(%vehicle.getTransform(), 3, 6), "0 1 0");
            %point = VectorNormalize(%point);
            %factor = (100 + %mySpd);//125m in front of vehicle is the right spot to start
            %checkPoint = VectorAdd(%myPos,VectorScale(%point, %factor));



            //check if pilot can see the next flight path marker
            %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
            %hasLOS = !containerRayCast(%myPos, %task.location, %mask, 0);

            //gain height if we can't see the next path marker
            if (!%hasLOS)
            {
               %client.setPilotPitchRange(-0.006, 0.003, 0.0025);
               %client.setPilotDestination(%x SPC %y SPC %z + 10, 1.0);
            }
            // ************************************************ leave this in here 1-27-2005 lagg ************************
            //if we are way to low fly straight up
            else if ((%myZLev + 30) < getWord(%task.location, 2))//old
            //else if ((%myZLev + 40) < getWord(%task.location, 2))//new
            {
               %client.setPilotPitchRange(-0.006, 0.003, 0.0025);//smooth setting here

               //aim at the next flight path marker
               %client.setPilotAim(%task.location);

               //******************** maybe try just press jet here ?
               %client.setPilotDestination(%x SPC %y SPC (getWord(%task.location, 2) + 10), 1.0);
            }
            else
            {
               //we are clear to fly to next marker, adjust pitch so we stay kinda level
               %dif = (getWord(%checkPoint, 2) - getWord(%task.location, 2));

               //-------------------------------------------------------------------- we are in a crash drive PULL IT UP! - DIVE 1
               if (%dif < -25)
               {
                  //%client.setPilotPitchRange(-0.01, 0.001, 0.9);
                  //%flingee = %obj.getMountNodeObject(%i);
                  applyKick(%vehicle, "down", 0.5, true);

                  //error("Crash drive PULL IT UP!    DIVE 1");

                  //error("Crash drive APPLY KICK to pull UP!    DIVE 1");
                  //error("dest Z = " @ %z @ ", CeckPoint Z = " @ getWord(%checkPoint, 2) @ ", Difference = " @ %dif);
               }
               //------------------------------------------------------------------------------ we are in a drive PULL UP - DIVE 2
               else if (%dif < -15)
               {
                  %client.setPilotPitchRange(-0.01, 0.001, 0.05);

                  //echo("Dive PULL UP              DIVE 2");
                  //echo("dest Z = " @ %z @ ", CeckPoint Z = " @ getWord(%checkPoint, 2) @ ", Difference = " @ %dif);
               }
               //-------------------------------------------------------------------------- we are nose down a little bit - DIVE 3
               else if (%dif < -5)
               {
                  %client.setPilotPitchRange(-0.007, 0.002, 0.005);

                  //echo("Pitch Up a Little             DIVE 3");
                  //echo("dest Z = " @ %z @ ", CeckPoint Z = " @ getWord(%checkPoint, 2) @ ", Difference = " @ %dif);
               }
               //----------------------------------------------------------------- we are Going Balistic Maveric         - NOSE UP 1
               else if (%dif > 25)
               {
                  //%client.setPilotPitchRange(-0.001, 0.01, 0.07);

                  applyKick(%vehicle, "up", 0.5, true);

                  //error("were Going Balistic Maveric         Nose UP 1");
                  //error("Going Balistic Maveric APPLY KICK to go DOWN!    Nose UP 1");
                  //error("dest Z = " @ %z @ ", CeckPoint Z = " @ getWord(%checkPoint, 2) @ ", Difference = " @ %dif);
               }
               //-------------------------------------------------------------------------- we are nose up a lot         - NOSE UP 2
               else if (%dif > 15)
               {
                  %client.setPilotPitchRange(-0.0, 0.001, 0.025);

                  //echo("Nose up a lot                  Nose UP 2");
                  //echo("dest Z = " @ %z @ ", CeckPoint Z = " @ getWord(%checkPoint, 2) @ ", Difference = " @ %dif);
               }
               //-------------------------------------------------------------------------- we are nose up a little bit - NOSE UP 3
               else if (%dif > 5)
               {
                  %client.setPilotPitchRange(-0.001, 0.005, 0.015);

                  //echo("Nose up a little bit           Nose UP 3");
                  //echo("dest Z = " @ %z @ ", CeckPoint Z = " @ getWord(%checkPoint, 2) @ ", Difference = " @ %dif);
               }


               else// --------------------------------------------------------------------------------------------- else fly normally
               {
                  //echo("fly normally");
                  //echo("xR = " @ %xR @ ", yR = " @ %yR);

                  %client.setPilotPitchRange(-0.005, 0.0025, 0.0025);//real smooth setting here
                  //applyKick(%vehicle, "foward", 1, false);
               }

               //fly at max speed
               %client.setPilotDestination(%task.location, 1.0);//max speed

               //echo(" ");
            }
         }
      }
   }
   //if we are not mounted in bomber
   else if (!%client.player.isMounted())
   {
      //if we at end of path and we hopped out
      if ((%task.count - 1) == %task.locationIndex)
      {
         if (%task == %client.objectiveTask)
	 {
            AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	 }
      }

      //did we fall off our bike? if so get back on!
      if (isObject(%client.player.mVehicle) && %client.player.mVehicle.getDamageState() !$= "Destroyed" && %client.pilotVehicle)
      {
         //if client is in heavy armor buy equipment
         if (%task == %client.objectiveTask)
	 {
            %task.baseWeight = %client.objectiveWeight;
            %task.equipment = "Medium || FieldTech HeavyLauncher";
	    %task.buyEquipmentSet = "MediumTailgunner";
            %client.needEquipment = true;
            return;
	 }

         //check if someone stole our bike
         if (%client.player.mVehicle.getMountNodeObject(0) > 0)
         {
            if (%task == %client.objectiveTask)
	    {
               AIUnassignClient(%client);
	       Game.AIChooseGameObjective(%client);
               return;
            }
         }

         //throw away any packs that won't fit
	 if (%client.player.getInventory(InventoryDeployable) > 0)
	    %client.player.throwPack();
	 else if (%client.player.getInventory(TurretIndoorDeployable) > 0)
	    %client.player.throwPack();
	 else if (%client.player.getInventory(TurretOutdoorDeployable) > 0)
	    %client.player.throwPack();

         %client.pilotVehicle = true;//needed to let ai mount pilot seat
         %client.stepMove(%client.player.mVehicle.position, 0.25, $AIModeMountVehicle);
      }

      //did someone shoot our legs out? if so we are done!
      else if (isObject(%client.player.mVehicle) && %client.player.mVehicle.getDamageState() $= "Destroyed")
      {
         if (%task == %client.objectiveTask)
	 {
            AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	 }
      }
   }
}
