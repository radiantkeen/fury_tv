//------------------------------
//TASKS AND OBJECTIVES

//AIO AttackLocation
//AIO AttackObject - teleport true
//AIO AttackPlayer
//AIO DefendLocation - teleport true
//AIO DeployEquipment - teleport true
//AIO EscortPlayer
//AIO LazeObject
//AIO MortarObject - teleport true
//AIO RepairObject
//AIO TouchObject - teleport true

// DEBUG flags - pinkpanther
// trace(0);
 $debugInv = 0;
 $debugAIChooseObjective = 0;
// $debugAIMortarObject = 0;
// $debugAIAttackObject = 0;
// $debugAILazeObject = 0;
// $debugAIDefendLocation = 0;
// $debugAIRepairObject = 0;
// $debugAIAttackLocation = 0;
// $debugAIAttackPlayer = 0;
// $debugAITouchObject = 0;
// $debugAIEscortPlayer = 0;
// $debugAICouldUseInventoryTask = 0;
// $debugAIPickupItemTask = 0;

//--------------------------------------------------------------
//AI Objective Q functions...

$ObjectiveClientsSet = 0;

// Define Standard LOS mask:
$LOSMask =  $TypeMasks::TerrainObjectType |
            $TypeMasks::MoveableObjectType |
            $TypeMasks::TSStaticShapeObjectType |
            $TypeMasks::ForceFieldObjectType |
            $TypeMasks::InteriorObjectType |
            $TypeMasks::VehicleObjectType;

// Not allowed:
//            $TypeMasks::StaticShapeObjectType |
//            $TypeMasks::ItemObjectType |
//            $TypeMasks::StaticObjectType |
//            $TypeMasks::StaticTSObjectType |

//--------------------------------
// Helper functions
//--------------------------------

function AISpeedFactor(%player) {   // provides armor speed to distance relation for a player:
   %speedFactor = (%player.getInventory(EnergyPack) > 0) ? 0.75 : 1;
   switch$(%player.getArmorSize()) {
      case "Heavy"    : %speedFactor *= MediumMaleHumanArmor.maxForwardSpeed / HeavyMaleHumanArmor.maxForwardSpeed;
      case "Titan"    : %speedFactor *= MediumMaleHumanArmor.maxForwardSpeed / TitanMaleHumanArmor.maxForwardSpeed;
      case "Light"    : %speedFactor *= MediumMaleHumanArmor.maxForwardSpeed / LightMaleHumanArmor.maxForwardSpeed;
      case "Sniper"   : %speedFactor *= MediumMaleHumanArmor.maxForwardSpeed / SniperMaleHumanArmor.maxForwardSpeed;
      case "Recon"    : %speedFactor *= MediumMaleHumanArmor.maxForwardSpeed / ReconMaleHumanArmor.maxForwardSpeed;
      case "FieldTech": %speedFactor *= MediumMaleHumanArmor.maxForwardSpeed / FieldTechMaleHumanArmor.maxForwardSpeed;
      default         : %speedFactor = 1;     // Medium
   }
   return %speedFactor;
}

function AIUseTeleport(%client, %task) {  // bots use Triumph teleporter
   %result = AICheckUseTeleport(%client, %task);   //new function in aiVehicle.cs
   %useTel = getWord(%result, 0);
   if (%useTel > 0) {
      %useTelLoc = %useTel.getWorldBoxCenter();
      %closestTelDist = VectorDist(%client.player.getWorldBoxCenter(), %useTelLoc);
      // Be polite and let teleport humans first:
      %humanCl = AIObjectiveGetClosestHumanClient(%useTelLoc, %client.team);
//      if ((%closestTelDist < 8) && isObject(%humanCl.player) && (VectorDist(%humanCl.player.getWorldBoxCenter(), %useTelLoc) < 3)) {
      if (%useTel.teleCharging) { // don't step towards a charging tele
         %client.setDangerLocation(%useTelLoc, 3);
         if ($watch $="tele") echo(%client.nameBase SPC "Tele: Humans first!!" SPC %useTel SPC %useTelLoc);
      }
      else if (%closestTelDist < 2)
         %client.pressJump();
      else
         %client.stepMove(%useTelLoc, 0.25, (%closestTelDist < 35 ? $AIModeWalk : $AIModeExpress));
      return 1;
   }
   else {
      return 0;
   }
}

// New: Judging whether it pays off to use a highly destructive weapon like the Nukes or MortarStorm- pinkpanther
function AIManyEnemiesInRange(%client, %explosPos, %timeUntilExplos, %radius, %overrideLOS, %bias) {
   %myTeamHealthNear = 0;
   %myTeamHealthFar = 0.01;
   %enemiesInRangeNear = %bias + 0.0001;
   %enemiesInRangeFar = 0.03;
   %count = ClientGroup.getCount();
   for(%i = 0; %i < %count; %i++)   {
      %cl = ClientGroup.getObject(%i);
      if (%cl.team != 0 && AIClientIsAlive(%cl)) {         // not an observer & they're alive
         %clPlayerPos = %cl.player.getWorldBoxCenter();
         %distance = VectorDist(%explosPos, %clPlayerPos);
         //%clIsVisible = isTargetVisible(%cl.target, %client.getSensorGroup());        // this excludes all clients not visible to the sensor network
         %clIsCloaked = %cl.player.getInventory("DeepCoverPack") > 0 || (%cl.player.getInventory("CloakingPack") > 0 && %cl.player.getImageState($BackpackSlot) $= "activate");    // // this excludes all clients which are cloaked
         if (!%clIsCloaked)   {      //see if they're not cloaked
            if (%distance > %radius)   {      //see if they're outside range: These are possible victims
               if (%cl.team == %client.team)   {     // on the same team ?
                  if ($teamDamage || %cl == %client) %myTeamHealthFar += (1-%cl.player.getDamagePercent()) * 360 / (%distance * %distance);
               } else {
                  %enemiesInRangeFar += ((%cl.player.holdingFlag > 0) ? 4 : 1) * 360 / (%distance * %distance);
               }
            } else {
               if (%overrideLOS)
                  %hasLOS = true;
               else
                  %hasLOS = !containerRayCast(%clPlayerPos, %explosPos, $LOSMask, 0);
               if (%hasLOS)  {      //see if they have LOS and are within range: These are almost sure victims
                  if (%cl.team == %client.team)   {     // on the same team ?
                     if ($teamDamage || %cl == %client) %myTeamHealthNear += (1-%cl.player.getDamagePercent());
                  } else {
                     %enemiesInRangeNear += (%cl.player.holdingFlag > 0) ? 4 : 1;
                  }
               }
            }
         }
      }
   }
//    error (getTaggedString(%client.name) @ " myTeamHealth: "@%myTeamHealthNear@" Far:"@%myTeamHealthFar @ " enemies: "@%enemiesInRangeNear@" Far:"@%enemiesInRangeFar @ " radius "@%radius SPC "quotient" SPC (%enemiesInRangeNear + %enemiesInRangeFar) / (%myTeamHealthNear + %myTeamHealthFar));
   return (%enemiesInRangeNear + %enemiesInRangeFar * %timeUntilExplos) / (%myTeamHealthNear + %myTeamHealthFar * %timeUntilExplos);
}

function AIObjectiveFindClients(%objective)
{
   //create and clear the set
   if (! $ObjectiveClientsSet)
   {
      $ObjectiveClientsSet = new SimSet();
      MissionCleanup.add($ObjectiveClientSet);
   }
   $ObjectiveClientsSet.clear();

   %clientCount = 0;
   %count = ClientGroup.getCount();
   for(%i = 0; %i < %count; %i++)
   {
      %cl = ClientGroup.getObject(%i);
      if (%cl.objective == %objective)
         $ObjectiveClientsSet.add(%cl);
   }
   return $ObjectiveClientsSet.getCount();
}

function AIObjectiveGetClosestHumanClient(%location, %team)   // human client only
{
   if (%location $= "")
      return -1;

   if (%team $= "")
      %team = 0;

   %closestClient = -1;
   %closestDistance = 32767;
   %count = ClientGroup.getCount();
   for(%i = 0; %i < %count; %i++)
   {
      %cl = ClientGroup.getObject(%i);
      if (!%cl.isAIControlled() && (%cl.team == %team || %team == 0) && AIClientIsAlive(%cl))
      {
         %testPos = %cl.player.getWorldBoxCenter();
         %distance = VectorDist(%location, %testPos);
         if (%distance < %closestDistance)
         {
            %closestDistance = %distance;
            %closestClient = %cl;
         }
      }
   }
   return %closestClient;
}

function AIObjectiveGetClosestClient(%location, %team)   // means AI client only
{
   if (%location $= "")
      return -1;

   if (%team $= "")
      %team = 0;

   %closestClient = -1;
   %closestDistance = 32767;
   %count = ClientGroup.getCount();
   for(%i = 0; %i < %count; %i++)
   {
      %cl = ClientGroup.getObject(%i);
      if (%cl.isAIControlled() && (%cl.team == %team || %team == 0) && AIClientIsAlive(%cl))
      {
         %testPos = %cl.player.getWorldBoxCenter();
         %distance = VectorDist(%location, %testPos);
         if (%distance < %closestDistance)
         {
            %closestDistance = %distance;
            %closestClient = %cl;
         }
      }
   }
   return %closestClient;
}

function AISensorJammerActivation(%client)  {     // activate sensor jammer pack if motion sensor in range:      - pinkpanther
   %backpackActive = %client.player.getImageState($BackpackSlot) $= "activate";
   if (%client.player.getInventory("SensorJammerPack") > 0) {
     %myEnergy = %client.player.getEnergyPercent();
     %inMotionSensorRange = getWord(AIFindClosestEnemyDeployable(%client, "DeployedMotionSensor", DeployMotionSensorObj.detectRadius, false), 1) > 0;   // Ignore clients with SensorJammerPacks
     if (!%backpackActive && %myEnergy > 0.4 && %inMotionSensorRange) {
        %client.player.use("Backpack");
     } else if (%backpackActive && (%myEnergy < 0.25 || !%inMotionSensorRange)) {
        %client.player.use("Backpack");
     }
   }
   return %backpackActive;
}

function AIFindClosestEnemyDeployable (%client, %findObjName, %range, %checkLOS) {
   %closestDepl = -1;
   %closestDist = (%range > 0) ? %range : 120;	//initialize so only deployed enemy stuff within this dist. will be detected
   %depGroup = nameToID("MissionCleanup/Deployables");
   if (isObject(%depGroup)) {
      %depCount = %depGroup.getCount();
      for (%i = 0; %i < %depCount; %i++) {
         %obj = %depGroup.getObject(%i);
         %objName = %obj.getDataBlock().getName();
         if (%objName $= %findObjName && %obj.team != %client.team) {
            %deplPos = %obj.getWorldBoxCenter();
            %deplDist = VectorDist(%client.player.getWorldBoxCenter(), %deplPos);
            if (%deplDist < %closestDist) {
               //check for LOS
               if (%checkLOS) {
                  %deplLOS = !containerRayCast(%player.getMuzzlePoint($WeaponSlot), %obj.getWorldBoxCenter(), $LOSMask, 0);
               } else {
                  %deplLOS = true;
               }
               if (%deplLOS) {
                  %closestDist = %deplDist;
                  %closestDepl = %obj;
               }
//   echo (getTaggedString(%client.name) SPC "found depl" SPC %obj SPC %objName SPC "Dist" SPC %deplDist SPC "cTeam" SPC %client.team SPC "oTeam" SPC %obj.team);
            }
         }
      }
   }
   if (%closestDepl < 0) %closestDist = -1;
   return %closestDepl SPC %closestDist;
}

//--------------------------------
// Objective Assignment to clients
// Also see function AIInitObjectives(%team, %game) in ai.cs !!
//--------------------------------

function AICountObjectives(%team)
{
   %objCount = 0;
   %count = $ObjectiveQ[%team].getCount();
   for (%i = 0; %i < %count; %i++)
   {
      %grp = $ObjectiveQ[%team].getObject(%i);
      if (%grp.getClassName() !$= "AIObjective")
         %objCount += %grp.getCount();
      else
         %objCount++;
   }
   error("DEBUG" SPC %team SPC "has" SPC %objCount SPC "objectives.");
}

function AIAddTableObjective(%objective, %weight, %level, %bump, %position)
{
   $objTable[%position, objective] = %objective;
   $objTable[%position, weight] = %weight;
   $objTable[%position, level] = %level;
   $objTable[%position, bump] = %bump;
   $objTableCount = %position + 1;
}

//new function to clean up the mess they made with the aiChooseObjective code above
//this gets called when ever AIObjectives need to be deleted from the $Objective[Q] - Lagg... 1-27-2004
function clearObjectiveFromTable(%objective)
{
   for(%i = 0; %i < $objTableCount; %i++)
   {
      if ($objTable[%i, objective] = %objective)
      {
         $objTable[%i, objective] = 0;
	   $objTable[%i, weight] = 0;
	   $objTable[%i, level] = 0;
	   $objTable[%i, bump] = 0;

         //error(%objective SPC "has been removed from the $objectTable - Lagg... :)");
         //echo("TAbleCount = " @ $objTableCount);

         break;
      }
   }
}

function AIChooseObjective(%client, %useThisObjectiveQ)
{
   //pick which objectiveQ to use, or use the default
   if (%useThisObjectiveQ <= 0 && %client.team < 0)
      return;
   if (%useThisObjectiveQ <= 0)
      %useThisObjectiveQ = $ObjectiveQ[%client.team];
   if (!isObject(%useThisObjectiveQ) || %useThisObjectiveQ.getCount() <= 0)
      return;

   //since most objectives check for inventory, find the closest inventory stations first
   %inventoryStr = AIFindClosestInventories(%client);

   //find the most appropriate objective
   if (!%client.objective)
   {
      //note, the table is never empty, during the course of this function
      AIAddTableObjective(0, 0, 0, 0, 0);
   }
   else
   {
      //should re-evaluate the current objective weight - but never decrease the weight!!!
      //a little change here or bots can't use vehicles :( - Lagg... *TEST FURTHER*
      //AI spends more time bumping each other than they do acually playing tribes 2.

//      %testWeight = %client.objectiveTask.baseWeight;
      %testWeight = %client.objective.weight(%client, %client.objectiveLevel, 0, %inventoryStr);

      if (%testWeight <= 0 || %testWeight > %client.objectiveWeight)
         %client.objectiveWeight = %testWeight;

      if (%client.objectiveWeight > 0) {
         AIAddTableObjective(%client.objective, %client.objectiveWeight, %client.objectiveLevel, 0, 0);
//if ($watch $= "bump") echo(%client.nameBase @ " Obj: " @ %client.objective SPC %client.objective.description @ " new Weight " @ %client.objectiveWeight);
      }
      else
         AIAddTableObjective(0, 0, 0, 0, 0);
   }

   %objCount = %useThisObjectiveQ.getCount();
   for (%i = 0; %i < %objCount; %i++) {  // loop through all objectives and see if our client finds an objective which
      // has a higher weight than our current objective AND than the weight of the client, that currently holds that
      // objective (bump client), and repeat this for all 4 weight levels. This also means, that weights from higher
      // level numbers come after lower ones, even if their weight is higher, resulting in bumping of clients !!
      %objective = %useThisObjectiveQ.getObject(%i);

      //don't re-evaluate the client's own
      if (%objective == %client.objective)
         continue;

      //try this objective at each of the 4 weight levels to see if it is weighted higher
      for (%level = 1; %level <= 4; %level++)
      {
         %minWeight = 0;
         %bumpWeight = 0;
         %bumpClient = "";

         //we can bump clients off the objective for the first three levels
         if (%level <= 3)
         {
            //if the objective is part of a group, check the whole group
            if (%objective.group > 0)
            {
               %bumpClient = %objective.group.clientLevel[%level];
               %bumpWeight = %bumpClient.objectiveWeight;
            }
            else
            {
               %bumpClient = %objective.clientLevel[%level];
               %bumpWeight = %bumpClient.objectiveWeight;
            }
         }

         //find the minimum weight the objective must have to be considered
         %minWeight = (%bumpWeight > $objTable[0, weight] ? %bumpWeight : $objTable[0, weight]);

         //evaluate the weight
         %weight = %objective.weight(%client, %level, %minWeight, %inventoryStr);
//if ($watch $= "bump") if (%bumpClient > 0) echo(%client.nameBase @ " Obj: " @ %objective SPC %objective.description @ " TryWeight: " @ %weight SPC %bumpClient.nameBase SPC %bumpWeight);

         //make sure we got a valid weight
         if (%weight <= 0)
            break;

         //if it's the highest so far, it now replaces anything else in the table
         if (%weight > $objTable[0, weight])
         {
            //never bump someone unless you significantly outweight them
            if (%weight > %bumpWeight + 40)
            {
if ($watch $= "bump") echo(%client.nameBase @ " Obj: " @ %objective.description @ " bumped " @ %weight SPC %bumpClient.nameBase SPC %bumpWeight);
               AIAddTableObjective(%objective, %weight, %level, %bumpClient, 0);

               //no need to keep checking the other levels
               break;
            }
         }
         //else if it's equal to the highest objective we've seen so far, and higher from our current objective
         else if (%weight == $objTable[0, weight] && %weight > %client.objectiveWeight)
         {
            //never bump someone unless you significantly outweigh them
            if (%weight > %bumpWeight + 40)
            {
if ($watch $= "bump") echo(%client.nameBase @ " Obj: " @ %objective.description @ " bumped2 " @ %weight SPC %bumpClient.nameBase SPC %bumpWeight);
               //if this wouldn't require us to bump someone, or the table is empty
               if (%bumpWeight <= 0 || $objTable[0, weight] <= 0)
               {
                  //if the table currently contains objectives which would require bumping someone, clear it
                  if ($objTable[0, bump] > 0)
                     %position = 0;
                  else
                     %position = $objTableCount;

                  //add it to the table
                  AIAddTableObjective(%objective, %weight, %level, %bumpClient, %position);

                  //no need to keep checking the other levels
                  break;
               }
               //otherwise, the table is not empty, and this would require us to bump someone
               //only add it if everything else in the table would also require us to bump someone
               else if ($objTable[0, bump] > 0)
               {
                  AIAddTableObjective(%objective, %weight, %level, %bumpClient, $objTableCount);

                  //no need to keep checking the other levels
                  break;
               }
            }
         }
         //else it must have been less than our highest objective so far- again, no need to keep checking other levels...
         else
            break;
      }
   }

   //if we have a table of possible objectives which are higher than our current- choose one at random
   if ($objTableCount > 0 && $objTable[0, objective] != %client.objective)
   {
      //choose the new one
      %index = mFloor(getRandom() * ($objTableCount - 0.01));

      //clear the old objective
      if (%client.objective)
      {
         if (%client.objectiveLevel <= 3)
         {
            if (%client.objective.group > 0)
            {
               if (%client.objective.group.clientLevel[%client.objectiveLevel] == %client)
                  %client.objective.group.clientLevel[%client.objectiveLevel] = "";
            }
            else
            {
               if (%client.objective.clientLevel[%client.objectiveLevel] == %client)
                  %client.objective.clientLevel[%client.objectiveLevel] = "";
            }
         }
//if ($debugAIChooseObjective) error(%client.nameBase @ " is being unassigned from " @ %client.objective SPC %client.objective.description SPC " Index:" SPC %index);
//       for(%i = 0; %i < $objTableCount; %i++) {
//         echo($objTable[%i, objective] SPC $objTable[%i, weight] SPC $objTable[%i, level] SPC $objTable[%i, bump].nameBase);
//       }
         %client.objective.unassignClient(%client);
      }

      //assign the new
      //if($objTable[%index, objective] > 0) // ZOD 2-3-03: Console spam fix
      //{
      %chooseObjective = $objTable[%index, objective];
      %client.objective = %chooseObjective;
      %client.objectiveWeight = $objTable[%index, weight];
      %client.objectiveLevel = $objTable[%index, level];
      if (%client.objectiveLevel <= 3)
      {
         if (%chooseObjective.group > 0)
            %chooseObjective.group.clientLevel[%client.objectiveLevel] = %client;
         else
            %chooseObjective.clientLevel[%client.objectiveLevel] = %client;
      }
//if ($debugAIChooseObjective) echo(%client.nameBase @ " is being assigned to " @ %client.objective.description);

      //make sure we still have a valid objective - Lagg... 1-27-2004
      if (isObject(%chooseObjective))
        %chooseObjective.assignClient(%client);
      else
      {
         //echo(%client.nameBase @ " failed choose objective " @ %client.objective.description);
         AIChooseObjective(%client, %useThisObjectiveQ); //if failed choose again - Lagg... 1-27-2004
      }

      //see if this objective needs to be acknowledged
      if (%chooseObjective.shouldAcknowledge && %chooseObjective.issuedByHuman && isObject(%chooseObjective.issuedByClientId))
      {
         //cancel any pending acknowledgements - a bot probably just got bumped off this objective
         cancel(%chooseObjective.ackSchedule);
         %chooseObjective.ackSchedule = schedule(5500, %chooseObjective, "AIAcknowledgeObjective", %client, %chooseObjective);
      }

      //if we had to bump someone off this objective,
      %bumpClient = $objTable[%index, bump];
      if (%bumpClient > 0)
      {
//if ($debugAIChooseObjective) echo(%bumpClient.nameBase @ " Obj: " @ %bumpClient.objective SPC %bumpClient.objective.description @ " bumped2 " @ %bumpClient.objectiveWeight);
         //unassign the bumped client and choose a new objective
         AIUnassignClient(%bumpClient);
         Game.AIChooseGameObjective(%bumpClient);
      }
      //}
   }

   //debuging - refresh aidebugq() if required
   //if ($AIDebugTeam >= 0)
   //   aiDebugQ($AIDebugTeam);
}

function AIAcknowledgeObjective(%client, %objective)
{
   %objective.shouldAcknowledge = false;
   //make sure the client is still assigned to this objective
   if (%client.objective == %objective)
      serverCmdAcceptTask(%client, %objective.issuedByClientId, -1, %objective.ackDescription);
}

function AIForceObjective(%client, %newObjective, %useWeight)
{
   //if we found a new objective, release the old, and assign the new
   if (%newObjective && %newObjective != %client.objective)
   {
      //see if someone was already assigned to this objective
      if (%newObjective.group > 0)
         %prevClient = newObjective.group.clientLevel[1];
      else
         %prevClient = newObjective.clientLevel[1];

      if (%prevClient > 0) {
//if ($debugAIChooseObjective) error(%prevClient.nameBase @ " Obj: " @ %prevClient.objective.description @ " forced " @ %prevClient.objectiveWeight);
         AIUnassignClient(%prevClient);
      }

      //see if we should override the weight
      if (%useWeight < %newObjective.weightLevel1)
         %useWeight = %newObjective.weightLevel1;

      //release the client, and force the assignment
      AIUnassignClient(%client);
      %client.objective = %newObjective;
      %client.objectiveWeight = %useWeight;
      %client.objectiveLevel = 1;
      if (%newObjective.group > 0)
         %newObjective.group.clientLevel[1] = %client;
      else
         %newObjective.clientLevel[1] = %client;

      %newObjective.forceClientId = %client;
      %newObjective.assignClient(%client);

      //don't acknowledge anything that's been forced...
      %newObjective.shouldAcknowledge = false;

      //now reassign the prev client
      if (%prevClient)
         Game.AIChooseGameObjective(%prevClient);
   }

   //debuging - refresh aidebugq() if required
   //if ($AIDebugTeam >= 0)
   // aiDebugQ($AIDebugTeam);
}

function AIUnassignClient(%client)
{
   //first, dissolve any link with a human
   aiReleaseHumanControl(%client.controlByHuman, %client);

   if (%client.objective)
   {
      if (%client.objectiveLevel <= 3)
      {
         if (%client.objective.group > 0)
         {
            //make sure the clientLevel was actually this client
            if (%client.objective.group.clientLevel[%client.objectiveLevel] == %client)
               %client.objective.group.clientLevel[%client.objectiveLevel] = "";
         }
         else
         {
            if (%client.objective.clientLevel[%client.objectiveLevel] == %client)
               %client.objective.clientLevel[%client.objectiveLevel] = "";
         }
      }
      if(isObject(%client.objective))
         %client.objective.unassignClient(%client);

      %client.objective = "";
      %client.objectiveWeight = 0;
      %client.objReleaseTime = getSimTime();	// used for recording the last unassign time in function AICheckDeadClients()
   }

   //debuging - refresh aidebugq() if required
   //if ($AIDebugTeam >= 0)
   //aiDebugQ($AIDebugTeam);
}

function AIClearObjective(%objective)
{
   %count = ClientGroup.getCount();
   for(%i = 0; %i < %count; %i++)
   {
      %cl = ClientGroup.getObject(%i);
      if (%cl.objective == %objective)
         AIUnassignClient(%cl);
   }

   //debuging - refresh aidebugq() if required
   //if ($AIDebugTeam >= 0)
   //aiDebugQ($AIDebugTeam);
}

//------------------------------
//TASKS AND OBJECTIVES

//----------------------------------------------------------------------------------------------- AI Defend Location ---

function AIDefendLocation::initFromObjective(%task, %objective, %client)
{
   //initialize the task vars from the objective
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;
   if (%objective.Location !$= "")
      %task.location = %objective.location;
   else
      %task.location = %objective.targetObjectId.getWorldBoxCenter();

   %task.issuedByClient = %objective.issuedByClientId;
   %task.chat = %objective.chat;
   %task.mode = %objective.mode;

   %task.desiredEquipment = "StickyBomb RepairKit";
//   %task.equipment = "Heavy || Titan || EMPLauncher";
    if ((%objective.deathCountByTurret > 0) || (%objective.deathCountByLava > 0)) {
       if (%objective.deathCountByTurret >= %objective.deathCountByLava) {
         %task.equipment = "SensorJammerPack";
         %task.desiredEquipment = "SensorJammerPack RepairKit";
         %task.buyEquipmentSet = "FieldTechSensorJammer MediumSensorJammerTriumph HeavySensorJammer";
       } else {
         %task.equipment = "ElementShieldPack";
         %task.desiredEquipment = "ElementShieldPack RepairKit";
         %task.buyEquipmentSet = "FieldTechElementShield MediumElementShield HeavyElementShield TitanElementShield";
       }
    } else if (strstr(%objective.targetObject, "FlipFlop") > -1 || %cl.objective.stuckArmor $= "Titan" || %cl.objective.stuckArmor $= "Heavy") {   // FlipFlop defenders carry SensorJammer packs
      %task.buyEquipmentSet = "FieldTechSensorJammer LightAttacker";
      %task.equipment = "FieldTech SensorJammerPack || Shotgun ShotgunAmmo";
    } else if (strstr(%objective.description, "flag") > -1) {     // Flag defenders carry shield packs
      %task.buyEquipmentSet = "TitanDefenderFlag HeavyAmmoSet FieldTechAmmoSet";
      %task.equipment = "Heavy || Titan || EMPLauncher AmmoPack";
    } else {   // Gen defenders carry repair packs
      %task.buyEquipmentSet = "TitanDefenderGens HeavyRepairSet";
      %task.equipment = "Heavy || Titan";
    }

   //added for base camping if on offensive - Lagg..
   %task.defense = %objective.defense;
   %task.offense = %objective.offense;

   //initialize other task vars
   %task.sendMsg = true;
   %task.sendMsgTime = 0;

   %task.engageTarget = -1;
}

function AIDefendLocation::assume(%task, %client)
{
   %task.setWeightFreq(15);
   %task.setMonitorFreq(15);
   %client.inPerimeter = false;
   %client.needEquipment = AINeedEquipment(%task.equipment, %client);

   //even if we don't *need* equipemnt, see if we should buy some...
   if (!%client.needEquipment && %task.buyEquipmentSet !$= "")
   {
      //see if we could benefit from inventory
      %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      if (%closestInv > 0) {   // No functioning inv station around -> don't bother using inv anymore
//          %closestInv.location = %closestInv.trigger.getWorldBoxCenter();
//          %result = AICheckUseTeleport(%client, %closestInv);//new function in aiVehicle.cs
         %closestDist = getWord(%result, 1);
         if (AINeedEquipment(%task.desiredEquipment, %client) && %closestDist < 200)
            %client.needEquipment = true;
      }
   }

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();

   //set a flag to determine if the objective should be re-aquired when the object is destroyed
   %task.reassignOnDestroyed = false;
   //--------------------------------------------------- assume telporter - start -
   //if the MPB Teleporter is online - Lagg... 9-30-2003
   // ZOD: Less code this way...
//   if(MPBTeleporterCheck(%client, %task))
      %task.useTeleport = true;
//    else
//       %task.useTeleport = false;
   //--------------------------------------------------- assume telporter - end -

   //added if defending with sniper gear - Lagg... 3-25-2004
   %task.snipeLocation = "";
   %task.hideLocation = "";
   %task.moveToPosition = true;
   %task.moveToSnipe = false;
   %task.nextSnipeTime = 0;
}

function AIDefendLocation::retire(%task, %client)
{
   %task.engageVehicle = -1;
   %client.setTargetObject(-1);
}

function AIDefendLocation::weight(%task, %client)
{
   //update the task weight
   if (%task == %client.objectiveTask) {
      // Defending generator, SolarPanel or FlipFlop: set weight to default (from ai.cs) if weight from map is higher - pinkpanther
      %targetObject = %task.targetObject;
      if (isObject(%targetObject)) {
         %targetObjectName = %targetObject.getDataBlock().getName();   //%objective.description
         if ((%client.objectiveWeight > $AIWeightDefendGenerator[1])
               && ((strstr(%targetObjectName, "Generator") != -1) || (strstr(%targetObjectName, "SolarPanel") != -1) || (strstr(%targetObjectName, "FlipFlop") != -1 && Game.class $= "CTFGame")))
            %client.objectiveWeight =  $AIWeightDefendGenerator[1];
      }
      %task.baseWeight = %client.objectiveWeight;
   }

   %player = %client.player;
   if (!isObject(%player))
      return;

   %hasMissile = (%player.getInventory("MissileLauncher") > 0) && (%player.getInventory("MissileLauncherAmmo") > 0);
   %hasAntiAirGun = (%player.getInventory("AntiAirGun") > 0) && (%player.getInventory("AntiAirGunAmmo") > 0);  // added AntiAirGun -  pinkpanther

   //if we're defending with a missile launcher, our first priority is to take out vehicles...
   //see if we're already attacking a vehicle...
   if (%task.engageVehicle > 0 && isObject(%task.engageVehicle) && (%hasMissile || %hasAntiAirGun))  // added AntiAirGun - pinkpanther
   {
      //set the weight
      %task.setWeight(%task.baseWeight);
      return;
   }

   //search for a new vehicle to attack
   %task.engageVehicle = -1;
   %losTimeout = $AIClientMinLOSTime + ($AIClientLOSTimeout * %client.getSkillLevel());
   %result = AIFindClosestEnemyPilot(%client, 300, %losTimeout);
   %pilot = getWord(%result, 0);
   %pilotDist = getWord(%result, 1);

   //if we've got missiles, and a vehicle to attack...
   if ((%hasMissile || %hasAntiAirGun) && AIClientIsAlive(%pilot))
   {
      %task.engageVehicle = %pilot.vehicleMounted;
      %client.needEquipment = false;
   }
   //otherwise look for a regular enemy to fight...
   else
   {
      //if we are sniper cheat and ignore los :)
      if (%client.player.getInventory(SniperRifle) > 0 && %client.player.getInventory(EnergyPack) > 0)
         %result = AIFindClosestEnemyToLoc(%client, %task.location, 400, $AIClientLOSTimeout, true);
      //if we have mortar than extend the range
      else if (%client.player.getInventory(Mortar) > 0 && %client.player.getInventory(MortarAmmo) > 0)
         %result = AIFindClosestEnemyToLoc(%client, %task.location, 400, $AIClientLOSTimeout, false);
      //otherwise, do the search normally.
      else
	       %result = AIFindClosestEnemyToLoc(%client, %task.location, 150, %losTimeout, false);

      %closestEnemy = getWord(%result, 0);
      %closestdist = getWord(%result, 1);

      //see if we found someone
      if (%closestEnemy > 0)
      {
         if (%task.engageTarget != %closestEnemy)
         {
            //reset sniper stuff - Lagg... 3-25-2004
            %task.snipeLocation = "";
            %task.hideLocation = "";
            %task.moveToPosition = true;
            %task.moveToSnipe = false;
            %task.nextSnipeTime = 0;
         }
         %task.engageTarget = %closestEnemy;
      }
      else
      {
         %task.engageTarget = -1;

         //see if someone is near me...
         %result = AIFindClosestEnemy(%client, 100, %losTimeout);
         %closestEnemy = getWord(%result, 0);
         %closestdist = getWord(%result, 1);
         if (%closestEnemy <= 0 || %closestDist > 150)
            %client.setEngageTarget(-1);

         //  Make the defender move away when the flag capper arrives:    - pinkpanther
         %enemyTeam = (%client.team == 1) ? 2 : 1;
         %enemyFlag = $AITeamFlag[%enemyTeam];
         if (!%enemyFlag.isHome) {
            %myEnemyFlagDist = vectorDist(%client.player.position, %enemyFlag.carrier.position);
            if (%myEnemyFlagDist < 12)
               %client.setDangerLocation(%client.player.getWorldBoxCenter(), 6);
         }
      }
   }

   //set the weight
   %task.setWeight(%task.baseWeight);
}

function AIDefendLocation::monitor(%task, %client)
{
   //if the defend location task has an object, set the "reset" flag
   if (%task == %client.objectiveTask && isObject(%task.targetObject))
   {
      if(%task.targetObject.name !$= "zone") // ZOD: Console Spam fix for KOTH game
      {
         //if (%task.targetObject.getDamageState() !$= "Destroyed")
         if (%task.targetObject.isDisabled() && %task.defense > 0)
            %task.reassignOnDestroyed = true;
         else if (%task.targetObject.isEnabled() && %task.offense > 0)
	      %task.reassignOnDestroyed = true;

         if (%task.reassignOnDestroyed)
         {
            AIUnassignClient(%client);
            Game.AIChooseGameObjective(%client);
            return;
         }
      }
   }

   //buy the equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
	      %equipmentList = %task.desiredEquipment;

      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime, %task);
      if (%result $= "InProgress")
      {
         //force a nervous reaction every 15 sec - Lagg...
         if (getSimTime() - %task.buyInvTime > 15000)
         {
            %client.setDangerLocation(%client.player.getWorldBoxCenter(), 20);
            %task.buyInvTime = getSimTime();
            %client.buyInvTime = %task.buyInvTime;
         }
         return;
      }
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(15);
         %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
         //if this task is the objective task, choose a new objective
         if (%task == %client.objectiveTask)
         {
          AIUnassignClient(%client);
          Game.AIChooseGameObjective(%client);
         }
         return;
      }
   }
   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   //chat
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
         {
            if (%task.chat !$= "")
            {
               %chatMsg = getWord(%task.chat, 0);
               %chatTemplate = getWord(%task.chat, 1);
               if (%chatTemplate !$= "")
                  AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
               else
                  AIMessageThread(%task.chat, %client, -1);
            }
            else if (%task.targetObject > 0)
            {
               %type = %task.targetObject.getDataBlock().getName();
               if (%type $= "Flag")
                  AIMessageThreadTemplate("DefendBase", "ChatSelfDefendFlag", %client, -1);
               else if (%type $= "GeneratorLarge")
                  AIMessageThreadTemplate("DefendBase", "ChatSelfDefendGenerator", %client, -1);
               else if (%type $= "StationVehicle")
                  AIMessageThreadTemplate("DefendBase", "ChatSelfDefendVehicle", %client, -1);
               else if (%type $= "SensorLargePulse")
                  AIMessageThreadTemplate("DefendBase", "ChatSelfDefendSensors", %client, -1);
               else if (%type $= "SensorMediumPulse")
                  AIMessageThreadTemplate("DefendBase", "ChatSelfDefendSensors", %client, -1);
               else if (%type $= "TurretBaseLarge")
                  AIMessageThreadTemplate("DefendBase", "ChatSelfDefendTurrets", %client, -1);
            }
         }
      }
   }
   //first, check for a vehicle to engage
   if (%task.engageVehicle > 0 && isObject(%task.engageVehicle))
   {
      %client.stop();
      %client.clearStep();
      %client.setEngageTarget(-1);
      %client.setTargetObject(%task.engageVehicle, 300, "Missile");
   }
   else
   {
      //--------------------------------------------------- monitor telporter - start -
      // use triumph teleporter if appropriate:
      if (%task.useTeleport) if (AIUseTeleport(%client, %task)) return;
      //--------------------------------------------------- monitor telporter - end -

      //clear the target vehicle...
      %client.setTargetObject(-1);

      //see if we're engaging a player
      if (%client.getEngageTarget() > 0)
      {
         //too far, or killed the enemy - return home
         if (%client.getStepStatus() !$= "InProgress" || %distance > 75)//error here (%distance ?) - Lagg...
         {
            %client.setEngageTarget(-1);
            %client.stepMove(%task.location, 8.0);
         }
      }
      //else see if we have a target to begin attacking
      else if (%task.engageTarget > 0 && AIClientIsAlive(%task.engageTarget))
      {
         if (%client.player.getInventory(SniperRifle) > 0 && %client.player.getInventory(EnergyPack) > 0)
         {
            //first, find an LOS location
            if (%task.snipeLocation $= "")
            {
               %task.snipeLocation = %client.getLOSLocation(%task.engageTarget.player.getWorldBoxCenter(), 150, 400);
               %task.hideLocation = %client.getHideLocation(%task.engageTarget.player.getWorldBoxCenter(), VectorDist(%task.engageTarget.player.getWorldBoxCenter(), %task.snipeLocation), %task.snipeLocation, 1);
               %client.stepMove(%task.hideLocation, 4.0);
               %task.moveToPosition = true;
            }
            else
            {
               //see if we can acquire a target
               %energy = %client.player.getEnergyPercent();
               %distToSnipe = VectorDist(%task.snipelocation, %client.player.getWorldBoxCenter());
               %distToHide = VectorDist(%task.hidelocation, %client.player.getWorldBoxCenter());

               //until we're in position, we can move using the AIModeExpress, after that, we only want to walk...
               if (%task.moveToPosition)
               {
                  if (%distToHide < 4.0)
                  {
                     //dissolve the human control link
                     if (%task == %client.objectiveTask)
                     {
                        if (aiHumanHasControl(%task.issuedByClient, %client))
                        {
                           aiReleaseHumanControl(%client.controlByHuman, %client);

                           //should re-evaluate the current objective weight
                           %inventoryStr = AIFindClosestInventories(%client);
                           %client.objectiveWeight = %client.objective.weight(%client, %client.objectiveLevel, 0, %inventoryStr);
                        }
                     }
                     %task.moveToPosition = false;
                  }
               }
               else if (%task.moveToSnipe)
               {
                  if (%energy > 0.75 && %client.getStepStatus() $= "Finished")
                  {
                     %client.stepMove(%task.snipeLocation, 4.0, $AIModeWalk);
                     %client.setEngageTarget(%task.engageTarget);
                  }
                  else if (%energy < 0.4)
                  {
                     %client.setEngageTarget(-1);
                     %client.stepMove(%task.hideLocation, 4.0);
                     %task.nextSnipeTime = getSimTime() + 1000 + (getRandom() * 1000);
                     %task.moveToSnipe = false;
                  }
               }
               else if (%energy > 0.5 && %task.engageTarget > 0 && getSimTime() > %task.nextSnipeTime)
               {
                  %client.stepRangeObject(%task.engageTarget.player.getWorldBoxCenter(), "BasicSniperShot", 150, 400, %task.snipelocation);
                  %client.aimAt(%task.engageTarget.player.getWorldBoxCenter(), 8000);
                  %task.moveToSnipe = true;
               }
            }
         }
         else
            %client.stepEngage(%task.engageTarget);
      }
	//else move to a random location around where we are defending
      else if (%client.getStepName() !$= "AIStepIdlePatrol")
      {
         %dist = VectorDist(%client.player.getWorldBoxCenter(), %task.location);
         if (%dist < 10)
         {
            //dissolve the human control link and re-evaluate the weight
            if (%task == %client.objectiveTask)
            {
               if (aiHumanHasControl(%task.issuedByClient, %client))
               {
                  aiReleaseHumanControl(%client.controlByHuman, %client);

                  //should re-evaluate the current objective weight
                  %inventoryStr = AIFindClosestInventories(%client);
                  %client.objectiveWeight = %client.objective.weight(%client, %client.objectiveLevel, 0, %inventoryStr);
               }
            }
            %client.stepIdle(%task.location);
         }
         else
            %client.stepMove(%task.location, 8.0);
      }
   }

   //see if we're supposed to be engaging anyone...
   if (!AIClientIsAlive(%client.getEngageTarget()) && AIClientIsAlive(%client.shouldEngage))
      %client.setEngageTarget(%client.shouldEngage);
}

//----------------------------------------------------------------------------------------------- AI Attack Location ---

function AIAttackLocation::initFromObjective(%task, %objective, %client)
{
   //initialize the task vars from the objective
   %task.baseWeight = %client.objectiveWeight;
   %task.location = %objective.location;
   %task.issuedByClient = %objective.issuedByClientId;
   %task.chat = %objective.chat;
   %task.buyEquipmentSet = %objective.buyEquipmentSet;
   // replace Equipment request "SniperRifle" with "SlugRifle" - pinkpanther
//   %task.equipment = strReplace(%objective.equipment, "SniperRifle", "SlugRifle");
//   %task.desiredEquipment = strReplace(%objective.desiredEquipment, "SniperRifle", "SlugRifle");
//   %task.equipment = strReplace(%task.equipment, "Plasma", "Shotgun || Hornet");
//   %task.desiredEquipment = strReplace(%task.desiredEquipment, "Plasma", "Hornet");
   %task.equipment = "";
   %task.desiredEquipment = "FlareGrenade || SensorJammerPack || WidowMaker";

   //initialize other task vars
   %task.sendMsg = true;
   %task.sendMsgTime = 0;
   %task.engageTarget = -1;
}

function AIAttackLocation::assume(%task, %client)
{
   %task.setWeightFreq(30);
   %task.setMonitorFreq(30);
   %client.needEquipment = AINeedEquipment(%task.equipment, %client);

   //even if we don't *need* equipemnt, see if we should buy some...
   if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
   {
      //see if we could benefit from inventory
      %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      if (%closestInv > 0) {   // No functioning inv station around -> don't bother using inv anymore
//          %closestInv.location = %closestInv.trigger.getWorldBoxCenter();
//          %result = AICheckUseTeleport(%client, %closestInv);//new function in aiVehicle.cs
         %closestDist = getWord(%result, 1);
         if (AINeedEquipment(%task.desiredEquipment, %client) && %closestDist < 200)
            %client.needEquipment = true;
      }
   }

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();

   %task.snipeLocation = "";
   %task.hideLocation = "";
   %task.moveToPosition = true;
   %task.moveToSnipe = false;
   %task.nextSnipeTime = 0;
}

function AIAttackLocation::retire(%task, %client)
{
}

function AIAttackLocation::weight(%task, %client)
{
   //update the task weight
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;

   //if we're a sniper, we're going to cheat, and see if there are clients near the attack location
   %hasSlugRifle   = %client.player.getInventory("SlugRifle") > 0 && %client.player.getInventory("SlugRifleAmmo") > 0;
   %hasSniperRifle = %client.player.getInventory("SniperRifle") > 0;
   %hasWidowMaker  = (%client.player.getInventory("WidowMaker") > 0) && (%client.player.getInventory("WidowMakerAmmo") > 0);
   %losTimeout = $AIClientMinLOSTime + ($AIClientLOSTimeout * %client.getSkillLevel());
   %distToLoc = VectorDist(%client.player.getWorldBoxCenter(), %task.location);
   if ((%hasSniperRifle || %hasSlugRifle || %hasWidowMaker) && %distToLoc > 60)
      %result = AIFindClosestEnemyToLoc(%client, %task.location, 50, $AIClientLOSTimeout, true);
   //otherwise, do the search normally.  (cheat ignores LOS)...
   else
      %result = AIFindClosestEnemyToLoc(%client, %task.location, 50, %losTimeout, false);

   %closestEnemy = getWord(%result, 0);
   %closestdist = getWord(%result, 1);

   //see if we found someone
   if (%closestEnemy > 0)
      %task.engageTarget = %closestEnemy;
   else
      %task.engageTarget = -1;

   %task.setWeight(%task.baseWeight);
}

function AIAttackLocation::monitor(%task, %client)
{
   //see if we're supposed to be engaging anyone...
   if (!AIClientIsAlive(%client.getEngageTarget()) && AIClientIsAlive(%client.shouldEngage))
      %client.setEngageTarget(%client.shouldEngage);

   %hasSlugRifle = %client.player.getInventory("SlugRifle") > 0 && %client.player.getInventory("SlugRifleAmmo") > 0;

   %client.needEquipment = AINeedEquipment(%task.equipment, %client);
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;

      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime, %task);
      if (%result $= "InProgress")
      {
         //force a nervous reaction every 15 sec - Lagg...
         if (getSimTime() - %task.buyInvTime > 15000)
         {
            %client.setDangerLocation(%client.player.getWorldBoxCenter(), 20);
            %task.buyInvTime = getSimTime();
            %client.buyInvTime = %task.buyInvTime;
         }
         return;
      }
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(30);
	 %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
	 //if this task is the objective task, choose a new objective
	 if (%task == %client.objectiveTask)
	 {
	    AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	 }
	 return;
      }
   }
   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   //chat
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
         {
            if (%task.chat !$= "")
            {
               %chatMsg = getWord(%task.chat, 0);
               %chatTemplate = getWord(%task.chat, 1);
               if (%chatTemplate !$= "")
                  AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
               else
                  AIMessageThread(%task.chat, %client, -1);
            }
            else
               AIMessageThreadTemplate("AttackBase", "ChatSelfAttack", %client, -1);
         }
      }
   }

   //--------------------------------------------------- monitor telporter - start -
   // use triumph teleporter if appropriate:
   if (%task.useTeleport) if (AIUseTeleport(%client, %task)) return;
   //--------------------------------------------------- monitor telporter - end -

   //how far are we from the location we're defending
   %myPos = %client.player.getWorldBoxCenter();
   %distance = %client.getPathDistance(%task.location);
   if (%distance < 0)
      %distance = 32767;

//   if (%client.player.getInventory(SniperRifle) > 0 && %client.player.getInventory(EnergyPack) > 0)
   if (%hasSlugRifle)
   {
      //first, find an LOS location
      if (%task.snipeLocation $= "")
      {
         %task.snipeLocation = %client.getLOSLocation(%task.location, 150, 350);
         %task.hideLocation = %client.getHideLocation(%task.location, VectorDist(%task.location, %task.snipeLocation), %task.snipeLocation, 1);
         %client.stepMove(%task.hideLocation, 4.0);
         %task.moveToPosition = true;
      }
      else
      {
         //see if we can acquire a target
         %distToSnipe = VectorDist(%task.snipelocation, %client.player.getWorldBoxCenter());
         %distToHide = VectorDist(%task.hidelocation, %client.player.getWorldBoxCenter());

         //until we're in position, we can move using the AIModeExpress, after that, we only want to walk...
         if (%task.moveToPosition)
         {
            if (%distToHide < 4.0)
            {
               //dissolve the human control link
               if (%task == %client.objectiveTask)
               {
                  if (aiHumanHasControl(%task.issuedByClient, %client))
                  {
                     aiReleaseHumanControl(%client.controlByHuman, %client);

                     //should re-evaluate the current objective weight
                     %inventoryStr = AIFindClosestInventories(%client);
                     %client.objectiveWeight = %client.objective.weight(%client, %client.objectiveLevel, 0, %inventoryStr);
                  }
               }
               %task.moveToPosition = false;
            }
         }
         else if (%task.moveToSnipe)
         {
            if (%client.getStepStatus() $= "Finished")
            {
               %client.stepMove(%task.snipeLocation, 4.0, $AIModeWalk);
               %client.setEngageTarget(%task.engageTarget);
            }
            else
            {
               %client.setEngageTarget(-1);
               %client.stepMove(%task.hideLocation, 4.0);
               %task.nextSnipeTime = getSimTime() + 1000 + (getRandom() * 1000);
               %task.moveToSnipe = false;
            }
         }
         else if (%task.engageTarget > 0 && getSimTime() > %task.engageTarget)
         {
//            %client.stepRangeObject(%task.engageTarget.player.getWorldBoxCenter(), "BasicSniperShot", 150, 250, %task.snipelocation);
            %client.stepRangeObject(%task.engageTarget.player.getWorldBoxCenter(), "SlugRifleBullet", 20, 350, %task.snipelocation);
            %client.aimAt(%task.engageTarget.player.getWorldBoxCenter(), 8000);
            %task.moveToSnipe = true;
         }
      }
   }
   else      //else see if we have a target to begin attacking
   {
      if (%client.getEngageTarget() <= 0 && %task.engageTarget > 0) {
         %client.stepEngage(%task.engageTarget);
      }//else move to the location we're defending
      else if (%client.getEngageTarget() <= 0)
      {
         %client.stepMove(%task.location, 8.0);
         %distance = VectorDist(%client.player.position, %task.location);
         if (%distance < 10)
         {
            //dissolve the human control link
            if (%task == %client.objectiveTask)
            {
               if (aiHumanHasControl(%task.issuedByClient, %client))
               {
                  aiReleaseHumanControl(%client.controlByHuman, %client);

                  //should re-evaluate the current objective weight
                  %inventoryStr = AIFindClosestInventories(%client);
                  %client.objectiveWeight = %client.objective.weight(%client, %client.objectiveLevel, 0, %inventoryStr);
               }
            }
         }
      }
   }
}

//------------------------------------------------------------------------------------------------- AI Attack Player ---

function AIAttackPlayer::initFromObjective(%task, %objective, %client)
{
   %task.baseWeight = %client.objectiveWeight;
   %task.targetClient = %objective.targetClientId;
   %task.issuedByClient = %objective.issuedByClientId;
   %task.chat = %objective.chat;

   if (%task.targetClient.player.isMounted()) {               // Use RailCannon against Vehicles   - pinkpanther
      %task.buyEquipmentSet = "TurretSniper";
      %task.desiredEquipment = "SlugRifle || RailCannon";           // || HeavyLauncher
   }
   else {
      %task.buyEquipmentSet = %objective.buyEquipmentSet;
      %task.desiredEquipment = "SlugRifle || Shotgun";           // || HeavyLauncher
   }

   %task.equipment = "FieldTech RepairKit || Medium RepairKit || Light RepairKit || Sniper RepairKit || Recon RepairKit";

   //initialize other task vars
   %task.sendMsg = true;
   %task.sendMsgTime = 0;
}

function AIAttackPlayer::assume(%task, %client)
{
   %task.setWeightFreq(15);
   %task.setMonitorFreq(15);
   %client.needEquipment = AINeedEquipment(%task.equipment, %client);
   if (! %client.needEquipment)
      %client.stepEngage(%task.targetClient);

   //even if we don't *need* equipemnt, see if we should buy some...
   if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
   {
      //see if we could benefit from inventory
      %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      if (%closestInv > 0) {   // No functioning inv station around -> don't bother using inv anymore
//          %closestInv.location = %closestInv.trigger.getWorldBoxCenter();
//          %result = AICheckUseTeleport(%client, %closestInv);//new function in aiVehicle.cs
         %closestDist = getWord(%result, 1);
         if (AINeedEquipment(%task.desiredEquipment, %client) && %closestDist < 200)
            %client.needEquipment = true;
      }
   }

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();
}

function AIAttackPlayer::retire(%task, %client)
{
   //dissolve the human control link
   if (%task == %client.objectiveTask)
      aiReleaseHumanControl(%client.controlByHuman, %client);
}

function AIAttackPlayer::weight(%task, %client)
{
   //make sure we still have someone to attack
   if (!AiClientIsAlive(%task.targetClient))
   {
		%task.setWeight(0);
      return;
   }

   // update the task weight
   // Lower the weight if not attacking the enemy flag carrier:   - pinkpanther
//    if (%task == %client.objectiveTask) {
//       %myFlag = $AITeamFlag[%client.team];
//       if (%myFlag.carrier != %task.targetClient.player)
//          %client.objectiveWeight = 2800;
//       %task.baseWeight = %client.objectiveWeight;
//    }

   %task.setWeight(%task.baseWeight);
}

function AIAttackPlayer::monitor(%task, %client)
{
   //make sure we still have someone to attack
   if (!AiClientIsAlive(%task.targetClient))
   {
      if (%task == %client.objectiveTask)
      {
         AIUnassignClient(%client);
         Game.AIChooseGameObjective(%client);
      }
      return;
   }
   //chat
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
         {
            if (%task.chat !$= "")
            {
               %chatMsg = getWord(%task.chat, 0);
               %chatTemplate = getWord(%task.chat, 1);
               if (%chatTemplate !$= "")
                  AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
               else
                  AIMessageThread(%task.chat, %client, true);
            }
            else
               AIMessageThread("ChatSelfAttack", %client, -1);
         }
      }
   }

   //second, buy the equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;

      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime, %task);
      if (%result $= "InProgress")
      {
         //force a nervous reaction every 10 sec - Lagg...
         if (getSimTime() - %task.buyInvTime > 10000)
         {
            %client.setDangerLocation(%client.player.getWorldBoxCenter(), 20);
            %task.buyInvTime = getSimTime();
         }
         return;
      }
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(15);
         %client.needEquipment = false;
         %client.stepEngage(%task.targetClient);
      }
      else if (%result $= "Failed")
      {
         //if this task is the objective task, choose a new objective
         if (%task == %client.objectiveTask)
         {
            AIUnassignClient(%client);
            Game.AIChooseGameObjective(%client);
         }
         return;
      }
   }
//   %distance = vectorDist(%clientPos, %targetPos);
//   if (%distance < 0) %distance = 32767;
//
//   %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticTSObjectType | $TypeMasks::ForceFieldObjectType;
//   %hasLOS = !containerRayCast(%client.player.getMuzzlePoint($WeaponSlot), %task.targetClient.player.getWorldBoxCenter(), %mask, 0);

   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   //--------------------------------------------------- monitor telporter - start -
   // use triumph teleporter if appropriate:
   if (%task.useTeleport) if (AIUseTeleport(%client, %task)) return;
   //--------------------------------------------------- monitor telporter - end -

   //cheap hack for now...   make the bot always know where you are...
   %client.clientDetected(%task.targetClient);

   //make sure we're still attacking...
//   if (%distance < 200 && %hasLOS)       {
//      %client.stepIdle(%client.player.getWorldBoxCenter());
//   } else if (%client.getStepName() !$= "AIStepEngage")
   if (%client.getStepName() !$= "AIStepEngage")
      %client.stepEngage(%task.targetClient);

   //make sure we're still attacking the right target
   %client.setEngageTarget(%task.targetClient);

   if (%client.getStepStatus() !$= "InProgress" && %task == %client.objectiveTask)
   {
      AIUnassignClient(%client);
      Game.AIChooseGameObjective(%client);
   }
}

//----------------------------------------------------------------------------------------------- AI Touch Object ---
//random set to go for flag/flipflop -                                      - Lagg... - 4-14-2003

function AITouchObject::initFromObjective(%task, %objective, %client)
{
   %task.targetObject = %objective.targetObjectId;
   %task.objective = %objective;
   %task.mode = %objective.mode;
   %task.issuedByClient = %objective.issuedByClientId;
   %task.chat = %objective.chat;
   %task.useTeleport = true;
   %task.location = %objective.location;
   %task.baseWeight = %client.objectiveWeight;

   %task.sendMsgTime = 0;
   if (%task.mode $= "FlagGrab" || %task.mode $= "FlagCapture")
      %task.sendMsg = true;
   else
      %task.sendMsg = false;

    if (((%objective.deathCountByTurret > 0) || (%objective.deathCountByLava > 0)) && ((%task.mode $= "FlagGrab" || %task.mode $= "TouchFlipFlop"))) {
       if (%objective.deathCountByTurret >= %objective.deathCountByLava) {
         %task.equipment = "SensorJammerPack || AntiEnergyPack";
         %task.desiredEquipment = "SensorJammerPack RepairKit || AntiEnergyPack RepairKit";
         if (getRandom() < 1/(%objective.deathCountByTurret + 0.01))
            %task.buyEquipmentSet = "LightSensorJammer ReconShotgun MediumSensorJammerTriumph HeavySensorJammer";
         else
            %task.buyEquipmentSet = "ReconShotgun LightSensorJammer MediumSensorJammerTriumph HeavySensorJammer";
       } else {
         %task.equipment = "ElementShieldPack";
         %task.desiredEquipment = "ElementShieldPack RepairKit";
         %task.buyEquipmentSet = "LightElementShield SniperElementShield MediumElementShield FieldTechElementShield HeavyElementShield";
       }
    } else {
      %task.desiredEquipment = "RepairGun2 || Medium RepairKit || Light RepairKit || Sniper RepairKit || Recon RepairKit";
      %task.buyEquipmentSet = "FieldTechEnergySet MediumEnergySet LightEnergyDefault TurretSniper";
      if (%task.mode $= "FlagGrab" || %task.mode $= "TouchFlipFlop")
         %task.equipment = "FieldTech || Medium || Light || Sniper || Recon";
      else
         %task.equipment = "";
    }
//if (%objective.deathCountByTurret > 0) echo (getTaggedString(%client.name) SPC "deathCountByTurret" SPC %objective.deathCountByTurret SPC %task.buyEquipmentSet);
//if (%objective.deathCountByLava > 0) echo (getTaggedString(%client.name) SPC "deathCountByLava" SPC %objective.deathCountByLava);

}

function AITouchObject::assume(%task, %client)
{
   %task.setWeightFreq(15);
   %task.setMonitorFreq(15);
   %task.engageTarget = 0;
   if (%task.mode $= "FlagGrab") {
      %client.objective.weightLevel2 = $AIWeightGrabFlag[2];
      %client.objective.weightLevel3 = $AIWeightGrabFlag[3];
      %client.objective.weightLevel4 = $AIWeightGrabFlag[4];
   }
//    if (%task.equipment !$= "")
       %equipmentList = %task.equipment;
//    else
//       %equipmentList = %task.desiredEquipment;

   //--------------------------------------------------- assume telporter - start -
   //if the MPB Teleporter is online - Lagg... 9-30-2003
   // ZOD: Less code this way...
//    if(MPBTeleporterCheck(%client, %task))
      %task.useTeleport = true;
//    else
//       %task.useTeleport = false;
   //--------------------------------------------------- assume telporter - end -

   %client.needEquipment = AINeedEquipment(%equipmentList, %client);
   //even if we don't *need* equipment, see if we should buy some...
   if ((%task.mode $= "FlagGrab" || %task.mode $= "TouchFlipFlop")) if (!%client.needEquipment && %task.buyEquipmentSet !$= "")
   {
      //see if we could benefit from inventory
      %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      if (%closestInv > 0) {   // No functioning inv station around -> don't bother using inv anymore
//          if (%task.useTeleport && %task.mode !$= "FlagCapture") {
//             %closestInv.location = %closestInv.trigger.getWorldBoxCenter();
//             %result = AICheckUseTeleport(%client, %closestInv);//new function in aiVehicle.cs
//          }
         %closestDist = getWord(%result, 1);
         if (AINeedEquipment(%task.desiredEquipment, %client) && %closestDist < 200)
            %client.needEquipment = true;
      }
   }
//error (getTaggedString(%client.name) SPC "client.needEquipment" SPC %client.needEquipment);

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();
}

function AITouchObject::retire(%task, %client)
{
}

function AITouchObject::weight(%task, %client)
{
   //update the task weight
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;
      %weight = %task.baseWeight;

   //see if we can find someone to shoot at...
   if (%client.getEngageTarget() <= 0)
   {
      %losTimeout = $AIClientMinLOSTime + ($AIClientLOSTimeout * %client.getSkillLevel());
      %myLocation = %client.player.getWorldBoxCenter();
      %result = AIFindClosestEnemy(%client, 40, %losTimeout);
      %task.engageTarget = getWord(%result, 0);
   }
   if (%task.mode $= "FlagGrab") {
      %task.objective.targetObjectID.stopAICapping = false;
      for(%i = 0; %i < ClientGroup.getCount(); %i++) {
         %cl = ClientGroup.getObject(%i);
         if (%cl.isAIControlled()) continue;
         if (isObject(%cl.player)) if (%cl.player.teamStopAICapping == %client.team) {
            %task.objective.targetObjectID.stopAICapping = true;
            break;
         }
      }
      if (%task.objective.targetObjectID.stopAICapping) {   // Capping by bots can be disabled by VFG and re-enabled by VAF (code in serverTasks.cs) - pinkpanther
//         error ("StopAICapping Weight: " @ %task.objective.targetObjectID);
         %weight = 0;
      }
      else {
         // Healthy players should be preferred for flag grabbing:
         %weight += mFloor(200*(0.5-%client.player.getDamagePercent()));
      }
if ($watch == %client.team) echo(%client.nameBase SPC "AITouchObject::weight:" SPC %weight);
   }

   %task.setWeight(%weight);
}

function AITouchObject::monitor(%task, %client)
{
//if (%task.mode $= "FlagDropped") error(%client.nameBase SPC "AITouchObject::monitor1:" SPC %task.description);
   //see if we're supposed to be engaging anyone...
   if (!AIClientIsAlive(%client.getEngageTarget()) && AIClientIsAlive(%client.shouldEngage))
      %client.setEngageTarget(%client.shouldEngage);

   // Force Field Avoidance - Lagg..
//    InitContainerRadiusSearch(%client.player.getWorldBoxCenter(), 4, $TypeMasks::ForceFieldObjectType);
//    %objSearch = containerSearchNext();
//    if (%objSearch > 0)
//    {
//       %ffDis = %client.getPathDistance(%objSearch.getWorldBoxCenter());
//       %myDis = %client.getPathDistance(%task.location);
//       if (%ffDis < %myDis )
//       {
//          if (%objSearch.isPowered() && %objSearch.team != %client.team)
//          {
//             //if this task is the objective task, choose a new objective
//             if (%task == %client.objectiveTask)
//             {
//                %client.setDangerLocation(%objSearch.getWorldBoxCenter(), 20);
//                AIUnassignClient(%client);
//                Game.AIChooseGameObjective(%client);
//             }
//             return;
//          }
//       }
//    }

   //first, buy the equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
	      %equipmentList = %task.desiredEquipment;

      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime, %task);
//echo (getTaggedString(%client.name) SPC "client.needEquipment" SPC %client.needEquipment SPC %result);
      if (%result $= "InProgress")
      {
         //force a nervous reaction every 10 sec - Lagg...
         if (getSimTime() - %task.buyInvTime > 10000)
         {
            %client.setDangerLocation(%client.player.getWorldBoxCenter(), 20);
            %task.buyInvTime = getSimTime();
            %client.buyInvTime = %task.buyInvTime;
         }
         return;
      }
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(15);
	      %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
         //if this task is the objective task, choose a new objective
         if (%task == %client.objectiveTask)
         {
          AIUnassignClient(%client);
          Game.AIChooseGameObjective(%client);
         }
	      return;
      }
//if (%client.team == 1) echo (getTaggedString(%client.name) SPC %client.needEquipment SPC %equipmentList);
   }

   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   //chat
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
	 if (%client.isAIControlled())
	 {
	    if (%task.chat !$= "")
	    {
	       %chatMsg = getWord(%task.chat, 0);
	       %chatTemplate = getWord(%task.chat, 1);
	       if (%chatTemplate !$= "")
	          AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
	       else
		  AIMessageThread(%task.chat, %client, -1);
            }
         }
      }
   }

   //keep updating the position, in case the flag is flying through the air...
   if (%task.location !$= "" && %task.mode !$= "FlagDropped")
      %touchPos = %task.location;
   else
      %touchPos = %task.targetObject.getWorldBoxCenter();

   //--------------------------------------------------- monitor telporter - start -
   // use triumph teleporter if appropriate:
   if (%task.useTeleport && %task.mode !$= "FlagCapture") if (AIUseTeleport(%client, %task)) return;
   //--------------------------------------------------- monitor telporter - end -

// Avoid engagements when carrying a flag   - pinkpanther
   %enemyTeam = (%client.team == 1) ? 2 : 1;
   %enemyFlag = $AITeamFlag[%enemyTeam];
   //see if we need to engage a new target
   %engageTarget = %client.getEngageTarget();
   if (!AIClientIsAlive(%engageTarget) && %task.engageTarget > 0 && %client.player != %enemyFlag.carrier) {
      %client.setEngageTarget(%task.engageTarget);
   }
   //else see if we should abandon the engagement
   else if (AIClientIsAlive(%engageTarget))
   {
      %myPos = %client.player.getWorldBoxCenter();
      %testPos = %engageTarget.player.getWorldBoxCenter();
      %distance = %client.getPathDistance(%testPos);
      if (%distance < 0 || %distance > 50)
         %client.setEngageTarget(-1);
   }

   //see if we have completed our objective
   if (%task == %client.objectiveTask)
   {
      %completed = false;
      switch$ (%task.mode)
      {
    case "TouchFlipFlop":
	    if (%task.targetObject.team == %client.team)
	       %completed = true;
	 case "FlagGrab":
	    if (!%task.targetObject.isHome || %task.objective.targetObjectID.stopAICapping)
          %completed = true;
	 case "FlagDropped":
	    if ((%task.targetObject.isHome) || (%task.targetObject.carrier !$= ""))
	       %completed = true;
	 case "FlagCapture":
	    if (%task.targetObject.carrier != %client.player)
	       %completed = true;
      }
      if (%completed)
      {
         AIUnassignClient(%client);
	      Game.AIChooseGameObjective(%client);
	      return;
      }
   }

   if (%task.mode $= "FlagCapture")
   {
      %homeFlag = $AITeamFlag[%client.team];

      //if we're within range of the flag's home position, and the flag isn't home, start idling...
      if (VectorDist(%client.player.position, %touchPos) < 40 && !%homeFlag.isHome)
      {
         if (%client.getStepName() !$= "AIStepIdlePatrol")
         // Better jump around a little bit:   - pinkpanther
	      %client.setDangerLocation(%touchPos, 15);
//	      %client.stepIdle(%touchPos);
      }
      else
         %client.stepMove(%touchPos, 0.25, $AIModeExpress);
   }

   //if siege game we want a special way of capturing the flip flop - Lagg... 9-11-2003
   else if ((%task.mode $= "TouchFlipFlop") && ($CurrentMissionType $= "Siege"))
   {
      //if there is a force field blocking the way - Lagg... 9-11-2003
      InitContainerRadiusSearch(%touchPos, 12, $TypeMasks::ForceFieldObjectType);
      %objSearch = containerSearchNext();
      if (%objSearch > 0 && %objSearch.isPowered())//should we check for all pass force fields? - Lagg ... (nah)
      {
         if (%task == %client.objectiveTask)
         {
            AIUnassignClient(%client);
	         Game.AIChooseGameObjective(%client);
	         return;
         }
      }
      else
         %client.stepMove(%touchPos, 0.25, $AIModeExpress);
   } else if (%task.mode $= "FlagGrab") {
      // allow for nuking  - pinkpanther
      %hasWidowMaker = %client.player.getInventory("WidowMaker") > 0 && %client.player.getInventory("WidowMakerAmmo") > 0;// && %enemyFlagDist < WidowMakerShot.damageRadius *1.33;
      %distance = vectorDist(%client.player.getWorldBoxCenter(), %touchPos);

      if (%hasWidowMaker && %distance < $AIWidowMakerLaunchDistance) {
         %client.setDangerLocation(%client.player.getWorldBoxCenter(), 16);
//error (getTaggedString(%client.name) SPC "hasWidowMaker" SPC "vecDist" SPC %distance);
      } else
         %client.stepMove(%touchPos, 0.25, $AIModeExpress);

      if (%distance < 30)
         %targetMode = "Destroy";
      else
         %targetMode = "Mortar";
      if (%hasWidowMaker && %distance < $AIWidowMakerLaunchDistance) {
         %client.setTargetObject(%enemyFlag, %distance +5, %targetMode);
//         error (%client.nameBase SPC "Flag Nuke:" SPC %enemyFlag);
      }
      else
         %client.setTargetObject(-1);
   } else {
      %client.stepMove(%touchPos, 0.25, $AIModeExpress);
   }

   if (VectorDist(%client.player.position, %touchPos) < 10)
   {
      //dissolve the human control link
      if (%task == %client.objectiveTask)
      {
         if (aiHumanHasControl(%task.issuedByClient, %client))
	 {
	    aiReleaseHumanControl(%client.controlByHuman, %client);

	    //should re-evaluate the current objective weight
	    %inventoryStr = AIFindClosestInventories(%client);
	    %client.objectiveWeight = %client.objective.weight(%client, %client.objectiveLevel, 0, %inventoryStr);
	 }
      }
   }

}

//----------------------------------------------------------------------------------------------- AI Escort Player ---

function AIEscortPlayer::initFromObjective(%task, %objective, %client)
{
   %task.baseWeight = %client.objectiveWeight;
   %task.targetClient = %objective.targetClientId;
   %task.mode = %objective.mode;
   %task.issuedByClient = %objective.issuedByClientId;
   %task.forceClient = %objective.forceClientId;
   %task.chat = %objective.chat;

//    %task.buyEquipmentSet = "FieldTechEnergySet MediumEnergyELF LightEnergyDefault";
//    %task.equipment = "HeavyLauncher || GrenadeLauncher";
//    %task.desiredEquipment = "HeavyLauncher";

   %task.buyEquipmentSet = %objective.buyEquipmentSet;
   %task.equipment = strReplace(%task.equipment, "Plasma", "Hornet");
   %task.desiredEquipment = strReplace(%task.desiredEquipment, "Plasma", "Hornet");
    if (%objective.deathCountByLava > 0) {
         %task.equipment = "ElementShieldPack";
         %task.desiredEquipment = "ElementShieldPack RepairKit";
         %task.buyEquipmentSet = "LightElementShield SniperElementShield MediumElementShield FieldTechElementShield";
    }

   //initialize other task vars
   %task.sendMsg = true;
   %task.sendMsgTime = 0;
}

function AIEscortPlayer::assume(%task, %client)
{
   %task.setWeightFreq(20);//was 15
   %task.setMonitorFreq(20);//was 15
   %task.rangedTarget = false;
   if (%task == %client.objectiveTask && %client == %task.forceClient && %task.issuedByClient == %task.targetClient && !%task.targetClient.player.isMounted())
   {
      %client.needEquipment = false;
      %client.mountVehicle = false;
   }
   else
   {
      %client.needEquipment = AINeedEquipment(%task.equipment, %client);
      if (! %client.needEquipment)
         %client.stepEscort(%task.targetClient);

      //even if we don't *need* equipemnt, see if we should buy some...
      if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
      {
         //see if we could benefit from inventory
         %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
         %result = AIFindClosestInventory(%client, %needArmor);
         %closestInv = getWord(%result, 0);
         if (%closestInv > 0) {   // No functioning inv station around -> don't bother using inv anymore
//          %closestInv.location = %closestInv.trigger.getWorldBoxCenter();
//          %result = AICheckUseTeleport(%client, %closestInv);//new function in aiVehicle.cs
         %closestDist = getWord(%result, 1);
         if (AINeedEquipment(%task.desiredEquipment, %client) && %closestDist < 200)
            %client.needEquipment = true;
         }
      }
   }
   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();
}

function AIEscortPlayer::retire(%task, %client)
{
   if (aiClientIsAlive(%client))
   {
      %client.clearStep();
      if(%client.player.isMounted())
         AIDisembarkVehicle(%client);

      //clear the target object
      %client.setTargetObject(-1);

      //tailgunner stop repairing
      if (%client.player.getImageState($BackpackSlot) $= "Activate")
      {
         %client.player.use("RepairPack");
         %client.player.cycleWeapon();
      }

      //make sure we are standing up right
      %client.player.setActionThread("root");
   }
}

function AIEscortPlayer::weight(%task, %client)
{
	//update the task weight
	if (%task == %client.objectiveTask)
		%task.baseWeight = %client.objectiveWeight;

   //make sure we still have someone to escort
   if (!AiClientIsAlive(%task.targetClient))
   {
		%task.setWeight(0);
      return;
   }

   //always shoot at the closest person to the client being escorted
   %targetPos = %task.targetClient.player.getWorldBoxCenter();
   %losTimeout = $AIClientMinLOSTime + ($AIClientLOSTimeout * %client.getSkillLevel());
	%result = AIFindClosestEnemyToLoc(%client, %targetPos, 200, %losTimeout);
   %task.engageTarget = getWord(%result, 0);
   if (!AIClientIsAlive(%task.engageTarget))
   {
      if (AIClientIsAlive(%task.targetClient.lastDamageClient, %losTimeout) && getSimTime() - %task.targetClient.lastDamageTime < %losTimeout)
         %task.engageTarget = %task.targetClient.lastDamageClient;
   }
   if (!AIClientIsAlive(%task.engageTarget))
   {
      %myPos = %client.player.getWorldBoxCenter();
		%result = AIFindClosestEnemy(%client, 50, %losTimeout);
      %task.engageTarget = getWord(%result, 0);
   }

   //if both us and the person we're escorting are in a vehicle, set the weight high!
   if (%task.targetClient.player.isMounted() && %client.player.isMounted())
   {
      %vehicle = %client.vehicleMounted;
      if (%vehicle > 0 && isObject(%vehicle) && %vehicle.getDamagePercent() < 0.8)
      {
         %task.setWeight($AIWeightVehicleMountedEscort);
         AIProcessVehicle(%client); //call the vehicle process routine - Lagg...
      }
      //else
         //%task.setWeight(%task.baseWeight);
   }
   //else
      %task.setWeight(%task.baseWeight);

   //find out if our escortee is lazing a target...
   %task.missileTarget = -1;
   %targetCount = ServerTargetSet.getCount();
   for (%i = 0; %i < %targetCount; %i++)
   {
      %targ = ServerTargetSet.getObject(%i);
      if (%targ.sourceObject == %task.targetClient.player)
      {
         //find out which item is being targetted...
	      %targPoint = %targ.getWorldBoxCenter();
         InitContainerRadiusSearch(%targPoint, 10, $TypeMasks::TurretObjectType | $TypeMasks::StaticShapeObjectType |
         $TypeMasks::VehicleObjectType);
	      %task.missileTarget = containerSearchNext();
	      break;
      }
   }

   AISensorJammerActivation(%client);
}

function AIEscortPlayer::monitor(%task, %client)
{
   //make sure we still have someone to escort
   if (!AiClientIsAlive(%task.targetClient))
   {
      if (%task == %client.objectiveTask)
      {
         AIUnassignClient(%client);
         Game.AIChooseGameObjective(%client);
      }
      return;
   }
   //lets check the range on this one
   else if (vectorDist(%client.Player.getWorldBoxCenter(), %task.targetClient.player.getWorldBoxCenter()) > 300)
   {
      if (%task == %client.objectiveTask)
      {
         AIUnassignClient(%client);
         Game.AIChooseGameObjective(%client);
      }
      return;
   }

   //first, buy the equipment
   if (%client.needEquipment && !%client.player.isMounted())
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;

      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime, %task);
      if (%result $= "InProgress")
      {
         //force a nervous reaction every 8 sec - Lagg...
         if (getSimTime() - %task.buyInvTime > 8000)
         {
            %client.setDangerLocation(%client.player.getWorldBoxCenter(), 20);
            %task.buyInvTime = getSimTime();
            %client.buyInvTime = %task.buyInvTime;
         }
         return;
      }
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(20);//was 15
         %client.needEquipment = false;
	 %client.stepEscort(%task.targetClient);
	 %task.buyInvTime = getSimTime();
      }
      else if (%result $= "Failed")
      {
         //if this task is the objective task, choose a new objective
	 if (%task == %client.objectiveTask)
	 {
	    AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	 }
	 return;
      }
   }

    //chat second
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
         {
	    if (%task.chat !$= "")
	    {
	       %chatMsg = getWord(%task.chat, 0);
	       %chatTemplate = getWord(%task.chat, 1);
	       if (%chatTemplate !$= "")
	          AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
	       else
		  AIMessageThread(%task.chat, %client, -1, true);
	    }
	    else
               AIMessageThreadTemplate("ChatTaskCover", "ChatTaskOnIt", %client, -1);
         }
      }
   }

  //see if our target is mounted in a vehicle...
   if (%task.targetClient.player.isMounted())
   {
      %task.setWeightFreq(10);//was 15 - Lagg...
      %task.setMonitorFreq(10);//was 15 - Lagg...

      //find the passenger seat the bot will take
      %vehicle = %task.targetClient.vehicleMounted;
      if (!%client.isMounted)//added - *
         %node = findAIEmptySeat(%vehicle, %client.player);

      //make sure there is an empty seat
      if ((%node < 0 && !%client.vehicleMounted) || (%node < 0 && %client.vehicleMounted != %task.targetClient.vehicleMounted))
      {
         if (%task == %client.objectiveTask)
	 {
	    AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	 }
         return;
      }

      //find the passenger seat location...
      %slotPosition = %vehicle.getSlotTransform(%node);

      //make sure we're in the correct armor - assault tanks cannot have a heavy...
      if (%task.targetClient.vehicleMounted.getDataBlock().getName() $= "AssaultVehicle")
      {
         //if the bot is in a heavy, go get equipment - Lagg...
	 if (%client.player.getArmorSize() $= "Heavy" || %client.player.getArmorSize() $= "Titan")
	 {
            if (%task == %client.objectiveTask)
            {
               if (! %task.sendMsg && isObject(%vehicle))
               {
                  %task.sendMsg = true;
                  %task.sendMsgTime = getSimTime();
                  %task.chat = "ChatNeedHold";
               }
               %task.equipment = "FlareGrenade RepairGun2";
               %task.buyEquipmentSet = "MediumTailgunner";
               %client.needEquipment = true;
               return;
	    }
	 }

         //throw away any packs that won't fit
	 if (%client.player.getInventory(InventoryDeployable) > 0)
	    %client.player.throwPack();
	 else if (%client.player.getInventory(TurretIndoorDeployable) > 0)
	    %client.player.throwPack();
	 else if (%client.player.getInventory(TurretOutdoorDeployable) > 0)
	    %client.player.throwPack();
      }

      //make sure we're in the correct armor - bomber turrets cannot have a heavy...
      if (%task.targetClient.vehicleMounted.getDataBlock().getName() $= "BomberFlyer")
      {
         //bd seat
         %nodeBd = %vehicle.getMountNodeObject(1);

         //tg seat
         %nodeTg = %vehicle.getMountNodeObject(2);

         //if we are a tailgunner with a repair pack lets use it :) - Lagg...
         if (%nodeTg == %client.player)
         {
            if (%client.getStepName() !$= "AIStepEscort")
               %client.stepEscort(%task.targetClient);

            //make sure we're still shooting...
            %client.setEngageTarget(%task.engageTarget);

            //see if we're supposed to be engaging anyone...
            if (!AIClientIsAlive(%client.getEngageTarget()) && AIClientIsAlive(%client.shouldEngage))
               %client.setEngageTarget(%client.shouldEngage);

            if (%vehicle.getDamagePercent() > 0 && %client.player.getInventory(RepairPack) > 0 && %client.getEngageTarget() <= 0)
	        {
               if (isObject(%vehicle) && %client.player.getInventory("RepairGun2") > 0 &&(%client.player.getImageState($BackpackSlot) !$= "Activate"))
                  %client.player.use("RepairGun2");
               else if (isObject(%vehicle) && %client.player.getInventory("RepairPack") > 0 &&(%client.player.getImageState($BackpackSlot) !$= "Activate"))
                  %client.player.use("RepairPack");
               else if (isObject(%vehicle) && isObject(%client.player.getMountedImage($WeaponSlot))
                    && %client.player.getMountedImage($WeaponSlot).getName() $= "RepairGunImage")
               {
                  %client.aimAt(%vehicle.getWorldBoxCenter());
                  %client.repairObject = %vehicle;
                  %client.setTargetObject(%vehicle, 12, "Repair");
               }
               else
               {
                  %client.repairObject = -1;
                  %client.setTargetObject(-1);
                  %client.pressFire(-1);
               }
            }
            else
            {
               %client.repairObject = -1;
               %client.setTargetObject(-1);
            }
	 }

         //do we need a tailgunner - Lagg...
         if (! %client.escort && %nodeTg <= 0 && %task.mode !$= "Tailgunner")
         {

            //create the escort objective (require flares in this case...)
            if (%task == %client.objectiveTask)
               {
                  if (! %task.sendMsg && isObject(%vehicle))
                  {
                     %task.sendMsg = true;
                     %task.sendMsgTime = getSimTime();
                     %task.chat = "ChatNeedTailgunner";
                  }
               }

            %client.escort = new AIObjective(AIOEscortPlayer)
            {
               dataBlock = "AIObjectiveMarker";
                        weightLevel1 = $AIWeightVehicleMountedEscort;
                        weightLevel2 = 0;
                        description = "Escort " @ getTaggedString(%client.name);
                        targetClientId = %client;
                        issuedByHuman        = true;
                        mode = "Tailgunner";
                        offense = true;
                        chat = "ChatNeedRide";
                        equipment = "FlareGrenade";
                        desiredEquipment = "AmmoPack";
	                buyEquipmentSet = "MediumTailgunner";
            };
            MissionCleanup.add(%client.escort);
            //if is siege game we have to do this right - Lagg... 11-3-2003
            if (Game.class $= "SiegeGame")
            {
               //the objectives in $ObjectiveQ[1] are all offense objectives, $ObjectiveQ[2] has the defensive ones..
               if (%client.team == game.offenseTeam)
                  $ObjectiveQ[1].add(%client.escort);
               else
                  $ObjectiveQ[2].add(%client.escort);
            }
            else
               $ObjectiveQ[%client.team].add(%client.escort);
         }

         //throw away any packs that won't fit
	 if (%client.player.getInventory(InventoryDeployable) > 0 && %node == 1 && %task.mode !$= "Tailgunner")
	    %client.player.throwPack();
	 else if (%client.player.getInventory(TurretIndoorDeployable) > 0 && %node == 1 && %task.mode !$= "Tailgunner")
	    %client.player.throwPack();
	 else if (%client.player.getInventory(TurretOutdoorDeployable) > 0 && %node == 1 && %task.mode !$= "Tailgunner")
	    %client.player.throwPack();
      }
      //we mounted in a havoc
      else
      {
         if (%client.getStepName() !$= "AIStepEscort")
            %client.stepEscort(%task.targetClient);

         //make sure we're still shooting...
         %client.setEngageTarget(%task.engageTarget);

         //see if we're supposed to be engaging anyone...
         if (!AIClientIsAlive(%client.getEngageTarget()) && AIClientIsAlive(%client.shouldEngage))
            %client.setEngageTarget(%client.shouldEngage);
      }

      if (%client.player.isMounted())
      {
         %vehicle = %task.targetClient.vehicleMounted;

         //make sure it's the same vehicle :)
         if (%client.vehicleMounted != %vehicle)
            AIDisembarkVehicle(%client);
      }
      else if (!%client.player.isMounted())
      {
         //check for empty seat
         if (!%client.isMounted)//added - *
            %node = findAIEmptySeat(%vehicle, %client.player);

         //make sure there is still an empty seat and/or we are in the same vehicle as escortee
         if ((%node < 0 && !%client.vehicleMounted) || (%node < 0 && %client.vehicleMounted != %task.targetClient.vehicleMounted))
         {
            if (%task == %client.objectiveTask)
            {
               AIUnassignClient(%client);
               Game.AIChooseGameObjective(%client);
            }
            return;
         }

         //find the passenger seat location...
         %slotPosition = %vehicle.getSlotTransform(%node);

         if (%task.targetClient.vehicleMounted.getDataBlock().getName() $= "BomberFlyer")
         {
            if (%nodeBd <= 0 && %task.mode !$= "Tailgunner" && (%client.player.getArmorSize() $= "Heavy" || %client.player.getArmorSize() $= "Titan"))
            {
               if (%task == %client.objectiveTask)
               {
                  if (! %task.sendMsg && isObject(%vehicle))
                  {
                     %task.sendMsg = true;
                     %task.sendMsgTime = getSimTime();
                     %task.chat = "ChatNeedHold";
                  }
                  %task.equipment = "FlareGrenade";
                  %task.desiredEquipment = "AmmoPack";
                  %task.buyEquipmentSet = "MediumTailgunner";
                  %client.needEquipment = true;
                  return;
               }
            }
            if (%nodeBd > 0 && %nodeTg <= 0 && %client.player.getInventory(FlareGrenade) <= 0)
            {
               if (%task == %client.objectiveTask)
               {
                  if (! %task.sendMsg && isObject(%vehicle))
                  {
                     %task.sendMsg = true;
                     %task.sendMsgTime = getSimTime();
                     %task.chat = "ChatNeedHold";
                  }
                  %task.mode = "Tailgunner";
                  %task.equipment = "FlareGrenade";
                  %task.desiredEquipment = "AmmoPack";
	          %task.buyEquipmentSet = "MediumTailgunner";
                  %client.needEquipment = true;
                  return;
               }
            }
         }
         //mount the vehicle - heavily modified - Lagg... 10-16-2003
         //%client.stepMove(%slotPosition, 0.25, $AIModeMountVehicle);

         //check for empty seat
         if (!%client.isMounted)//added - *
            %node = findAIEmptySeat(%vehicle, %client.player);

         //first check if it is a bomber
         if (%vehicle.getdataBlock().getName() $= "BomberFlyer")
         {
            if (vectorDist(%client.Player.getWorldBoxCenter(), %task.targetClient.player.getWorldBoxCenter()) < 20)
            {
               //%client.player.setActionThread(%vehicle.getDatablock().mountPose[0]);
               %client.stepMove(%slotPosition, 0.25, $AIModeMountVehicle);

               //if (vectorDist(%client.Player.position, %slotPosition) < 6)
                  //%client.player.setActionThread(%vehicle.getDatablock().mountPose[0]);
            }
            else
            {
               //%client.player.setActionThread("root");
               %client.stepMove(%task.targetClient.vehicleMounted.getWorldBoxCenter(), 16, $AIModeExpress);
            }
         }
         //vehicle is a Tank
         else if (%vehicle.getdataBlock().getName() $= "AssaultVehicle")
         {
            //if (vectorDist(%client.Player.position, %slotPosition) < 10)
                  //%client.player.setActionThread(sitting, true, true);
            if (vectorDist(%client.Player.getWorldBoxCenter(), %task.targetClient.player.getWorldBoxCenter()) < 18)
            {
               //%client.player.setActionThread(sitting, true, true);
               %client.stepMove(%slotPosition, 0.25, $AIModeMountVehicle);

               //if (vectorDist(%client.Player.position, %slotPosition) < 10)
                  //%client.player.setActionThread(sitting, true, true);
            }
            else
            {
               //%client.player.setActionThread("root");
               %client.stepMove(%task.targetClient.vehicleMounted.getWorldBoxCenter(), 14, $AIModeWalk);
            }
         }
         //else must be a havoc so mount regularly (not so fast) move to a good spot and wait
         else
         {
            if (vectorDist(%client.Player.getWorldBoxCenter(), %task.targetClient.player.position) > 40)
            {
               if (vectorDist(%client.player.getWorldBoxcenter(), %task.targetClient.objectiveTask.location) <= 20)
                  %client.stepIdle(%task.targetClient.objectiveTask.location);
               else
                  %client.stepMove(%task.targetClient.objectiveTask.location, 0.25, $AIModeExpress);
            }
            else
               %client.stepMove(%task.targetClient.vehicleMounted.getWorldBoxCenter(), 0.25, $AIModeMountVehicle);
               //%client.stepMove(%slotPosition, 0.25, $AIModeMountVehicle);
         }
      }
   }
   else//------------------------------------------------------------------//target client is not mounted
   {
      //lets end the vehicle conversation stuff - Lagg...
      if (%task == %client.objectiveTask)
      {
         %task.sendMsg = false;
         %task.chat = "";
      }

      //disembark if we're mounted, but our target isn't (anymore)
      if (%client.player.isMounted())
         AIDisembarkVehicle(%client);

      //kill the escort (tailgunner) objective
      if (%client.escort)
      {
         AIClearObjective(%client.escort);
         %client.escort.delete();
         %client.escort = "";
      }

      //don't block the vehicle pad if we are escorting a pilot
      if (%task.targetClient.needVehicle)
      {
         %clVs = AIFindClosestVStation(%client);
         if (%clVs > 0)
         {
            %closestVs = getWord(%clVs, 0);
            %closestVsDist = getWord(%clVs, 1);
            if (%closestVsDist < 7)
               %client.setDangerLocation(%task.targetClient.player.location, 10);
            else if (%closestVsDist < 10)
               %client.stop();
         }
      }

      //END VEHICLE STUFF----------------------------------------------------- Lagg...

      //see if we're supposed to be mortaring/missiling something...
      //%client.player.setActionThread("root", true);
      %hasMortar = (%client.player.getInventory("Mortar") > 0) && (%client.player.getInventory("MortarAmmo") > 0);
      %hasMissile = (%client.player.getInventory("MissileLauncher") > 0) && (%client.player.getInventory("MissileLauncherAmmo") > 0);
      if (!isObject(%task.engageTarget) && isobject(%task.missileTarget) && %task.missileTarget.getDamageState() !$= "Destroyed" && (%hasMortar || %hasMissile))
      {
         if (%task.rangedTarget)
         {
            %client.stop();
	    %client.clearStep();
	    %client.setEngageTarget(-1);

            %type = %task.missileTarget.getDataBlock().getName();
	    if (%type $= "SensorLargePulse" && %hasMissile)
               %client.setTargetObject(%task.missileTarget, 300, "MissileNoLock");
	    else if (%type $= "SensorMediumPulse" && %hasMissile)
               %client.setTargetObject(%task.missileTarget, 300, "MissileNoLock");
	    else if (%type $= "TurretBaseLarge" && %hasMissile)
               %client.setTargetObject(%task.missileTarget, 300, "MissileNoLock");
            else if (%type $= "MobileBaseVehicle" && %hasMissile)
               %client.setTargetObject(%task.missileTarget, 300, "MissileNoLock");
	    else
               %client.setTargetObject(%task.missileTarget, 300, "Mortar");
         }
         else if (%client.getStepName() !$= "AIStepRangeObject")
         {
            if (%hasMortar || %hasMissile)
	       %client.stepRangeObject(%task.missileTarget, "BasicTargeter", 100, 300);
	    else
	       %client.stepRangeObject(%task.missileTarget, "DefaultRepairBeam", 20, 30);
         }
         else if (%client.getStepStatus() $= "Finished")
         %task.rangedTarget = true;
      }
      //move to waiting spot for a ride if we are escorting a pilot
      else if (%task.targetClient.needVehicle || %task.targetClient.vehicleMounted)
      {
         //if (vectorDist(%client.Player.getWorldBoxCenter(), %task.targetClient.player.position) > 30)
         //{
            if (vectorDist(%client.player.getWorldBoxcenter(), %task.targetClient.objectiveTask.location) < 20)
               %client.stepIdle(%task.targetClient.objectiveTask.location);
            else
               %client.stepMove(%task.targetClient.objectiveTask.location, 0.25, $AIModeExpress);
         //}
      }
      else
      {
        if ((%client.repairObject != %task.targetClient.vehicleMounted) || !%client.player.isMounted())
        {
           %task.rangedTarget = false;
           %client.setTargetObject(-1);
           //echo("set targetobject -1");
        }
        if (%client.getStepName() !$= "AIStepEscort")
           %client.stepEscort(%task.targetClient);

        //make sure we're still shooting...
        %client.setEngageTarget(%task.engageTarget);

        //see if we're supposed to be engaging anyone...
        if (!AIClientIsAlive(%client.getEngageTarget()) && AIClientIsAlive(%client.shouldEngage))
           %client.setEngageTarget(%client.shouldEngage);
      }
   }
}

//---------------------------------------------------------------------------------------------- AI Attack Object ---

function AIAttackObject::initFromObjective(%task, %objective, %client)
{
    %task.targetObject = %objective.targetObjectId;
    %task.issuedByClient = %objective.issuedByClientId;
    %task.location = %objective.location;
    %task.mode = %objective.mode;
    %targetPos = %task.targetObject.getWorldBoxCenter();
    %targetObjectName = %task.targetObject.getDataBlock().getName();
    %task.baseWeight = %client.objectiveWeight;

    //initialize other task vars
    %task.sendMsg = true;
    %task.sendMsgTime = 0;
    %task.useTeleport = true;

    %turretOrLavaDeath = (%objective.deathCountByTurret > 0) || (%objective.deathCountByLava > 0);

    // try to find a LOS location:
    %task.locationDist = VectorDist(%targetPos, %task.location);
    if (%task.locationDist < 8) {
       %proposedLocationDist = 293;
       %hasNoLOS = true;
       while (%proposedLocationDist > 8 && %hasNoLOS) {
          %task.losLocation = %client.getLOSLocation(%targetPos, 0, %proposedLocationDist);
          %losLocationDist = VectorDist(%targetPos, %task.losLocation);
          if (%losLocationDist > %proposedLocationDist + 10) {
             %losLocationDist = 0;
             %task.losLocation = %targetPos;
             %hasNoLOS = true;
//             break;
          } else {
             %hasNoLOS = containerRayCast(%task.losLocation, %targetPos, $LOSMask - $TypeMasks::ForceFieldObjectType, 0);     // Ignore forcefields for calculating attack position
          }
          if (%losLocationDist > 190 && %losLocationDist < 250 && (getWord(%task.losLocation, 2) - getWord(%targetPos, 2) > 20) && !%turretOrLavaDeath) {
             if (!%hasNoLOS) {
                %objective.mode = "Nuke";
                %objective.location = %task.losLocation;
                if ($verbose >= 2) echo(%client.nameBase SPC %losLocationDist SPC "Nuke Attack: LOS Saved" SPC %objective.targetObject SPC %task.losLocation);
             } else {
               if ($verbose >= 2) logEcho(%client.nameBase SPC %losLocationDist SPC "Nuke Attack: NoLOS" SPC %objective.targetObject);
             }
             %task.mode = "Nuke";
             %hasNoLOS = false;
          } else if (%hasNoLOS) {
             %proposedLocationDist -= 90; // retry using a closer range
          }
       }
       if (%losLocationDist > 8) {
          %task.location = %task.losLocation;
          %task.locationDist = %losLocationDist;
          if ($verbose >= 3) logEcho(%client.nameBase SPC %losLocationDist SPC "Do LOS Attack" SPC %objective.targetObject);
       } else {
          if ($verbose >= 3) logEcho(%client.nameBase SPC "No LOS Attack" SPC %objective.targetObject);
       }
    }

//    %objPos = %task.targetObject.getWorldBoxCenter();
    if (%task.mode $= "Nuke") {
         %task.equipment = "WidowMaker WidowMakerAmmo || MortarStorm MortarStormAmmo";
         %task.desiredEquipment = "WidowMaker WidowMakerAmmo";
         if (%turretOrLavaDeath) {
            if (%objective.deathCountByTurret >= %objective.deathCountByLava) {
               %task.buyEquipmentSet = "TitanSensorJammer HeavySensorJammer MediumSensorJammerTriumph FieldTechSensorJammer";
            } else {
               %task.buyEquipmentSet = "TitanElementShield HeavyElementShield MediumElementShield FieldTechElementShield SniperElementShield";
            }
         } else {
            %task.buyEquipmentSet = "TitanNuke2 HeavyEnergySet MediumEnergySet TurretSniper FieldTechAmmoSet";
         }
    } else if (%turretOrLavaDeath) {
      if (%objective.deathCountByTurret >= %objective.deathCountByLava) {
        %task.equipment = "SensorJammerPack || AntiEnergyPack";
        %task.desiredEquipment = "SensorJammerPack RepairKit || AntiEnergyPack RepairKit";
        if (getRandom() > 1/(%objective.deathCountByTurret + 0.01) && %task.locationDist < LightBeam.maxRifleRange)
          %task.buyEquipmentSet = "ReconBeamSword HeavySensorJammer LightSensorJammer FieldTechSensorJammer MediumAttacker";
        else
          %task.buyEquipmentSet = "MediumAttacker FieldTechSensorJammer LightSensorJammer ReconShotgun HeavySensorJammer";
      } else {
        %task.equipment = "ElementShieldPack";
        %task.desiredEquipment = "ElementShieldPack RepairKit";
        %task.buyEquipmentSet = "MediumElementShield FieldTechElementShield LightElementShield SniperElementShield HeavyElementShield";
      }
    } else if (strstr(%task.targetObject.getDataBlock().getName(), "Turret") >= 0) {
        %task.equipment = "SensorJammerPack || AntiEnergyPack";
        %task.desiredEquipment = "SensorJammerPack RepairKit || AntiEnergyPack RepairKit";
        if (getRandom() > 0.6 && %task.locationDist < LightBeam.maxRifleRange)
          %task.buyEquipmentSet = "ReconBeamSword HeavySensorJammer LightSensorJammer FieldTechSensorJammer MediumAttacker";
        else
          %task.buyEquipmentSet = "MediumAttacker FieldTechSensorJammer LightSensorJammer ReconShotgun HeavySensorJammer";
    } else {
      %task.equipment = "FieldTech MissileLauncher MissileLauncherAmmo || Plasma PlasmaAmmo || Napalm || BeamSword || Shotgun ShotgunAmmo || TimeBomb";
      %task.desiredEquipment = "";
      // Choose a different set at random but also depending on distance to target - pinkpanther
      %clientPos = %client.player.getWorldBoxCenter();
      //   %distToObject = %client.getPathDistance(%objPos);
      %distToObject = getWord(AICheckUseTeleport(%client, %task), 1);    //new function in aiVehicle.cs
      %longDistanceSet = getRandom() < %distToObject/2000;   // distToObject = 1000 results in the same odds for spawning heavier or lighter armor - pinkpanther
      if (%longDistanceSet){
//        if (strstr(%task.targetObject.getDataBlock().getName(), "Generator") >= 0 && %task.locationDist < LightBeam.maxRifleRange)// Attacking generator behind forcefield requires Light Saber - pinkpanther
        %task.buyEquipmentSet = "LightAttacker ReconDualBlaster MediumAttacker FieldTechAmmoSet HeavySensorJammer";
      } else {
        %task.buyEquipmentSet = "MediumAttacker FieldTechSensorJammer LightAttacker ReconDualBlaster HeavySensorJammer";
      }
    }

}

function AIAttackObject::assume(%task, %client)
{
   %task.setWeightFreq(15);
   %task.setMonitorFreq(15);
   if ((%client.objective.weightLevel1 < $AIWeightAttackGenerator[1]) && strstr(%targetObjectName, "Generator") >= 0 && strstr(%targetObjectName, "Team0") < 0) {
      %client.objective.weightLevel1 = $AIWeightAttackGenerator[1];
//      %client.objectiveWeight =  $AIWeightAttackGenerator[1]; // crashes !!! - pinkpanther
   }
   %client.needEquipment = AINeedEquipment(%task.equipment, %client);
   if ($verbose >= 4) logEcho(%client.nameBase SPC "AIAttackObject::Assume" SPC %client.objective.targetObject);

   //set up some task vars
   %task.celebrate = false;
   %task.waitTimerMS = 0;

   //even if we don't *need* equipemnt, see if we should buy some...
   if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
   {
      //see if we could benefit from inventory
      %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      if (%closestInv > 0) {   // No functioning inv station around -> don't bother using inv anymore
//          %closestInv.location = %closestInv.trigger.getWorldBoxCenter();
//          %result = AICheckUseTeleport(%client, %closestInv);//new function in aiVehicle.cs
         %closestDist = getWord(%result, 1);
         if (AINeedEquipment(%task.desiredEquipment, %client) && %closestDist < 200)
            %client.needEquipment = true;
      }
   }

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();

   //--------------------------------------------------- assume telporter - start -
   //if the MPB Teleporter is online - Lagg... 9-30-2003
   // ZOD: Less code this way...
//    if(MPBTeleporterCheck(%client, %task))
      %task.useTeleport = true;
//    else
//       %task.useTeleport = false;
   //--------------------------------------------------- assume telporter - end -

}

function AIAttackObject::retire(%task, %client)
{
   %client.setTargetObject(-1);
}

function AIAttackObject::weight(%task, %client)
{
   //update the task weight
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;
   %task.setWeight(%task.baseWeight);
}

function AIAttackObject::monitor(%task, %client)
{

//    InitContainerRadiusSearch(%client.player.getWorldBoxCenter(), 4, $TypeMasks::ForceFieldObjectType);
//    %objSearch = containerSearchNext();
//    if (%objSearch > 0)
//    {
//       %ffDis = %client.getPathDistance(%objSearch.getWorldBoxCenter());
//       %myDis = %client.getPathDistance(%task.position);
//       if (%ffDis < %myDis )
//       {
//          if (%objSearch.isPowered() && %objSearch.team != %client.team)
//          {
//             //if this task is the objective task, choose a new objective
//             if (%task == %client.objectiveTask)
//             {
//                %client.setDangerLocation(%objSearch.getWorldBoxCenter(), 20);
//                AIUnassignClient(%client);
//                Game.AIChooseGameObjective(%client);
//             }
//             return;
//          }
//       }
//    }

   %client.needEquipment = AINeedEquipment(%task.equipment, %client);

   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;

      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime, %task);
// echo (getTaggedString(%client.name) SPC "Attack:result" SPC %result SPC "equipmentList" SPC %equipmentList SPC "buyEquipmentSet" SPC %task.buyEquipmentSet);
      if (%result $= "InProgress")
      {
         //force a nervous reaction every 15 sec - Lagg...
         if (getSimTime() - %task.buyInvTime > 15000)
         {
            %client.setDangerLocation(%client.player.getWorldBoxCenter(), 20);
            %task.buyInvTime = getSimTime();
            %client.buyInvTime = %task.buyInvTime;
         }
         return;
      }
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(15);
         %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
         //if this task is the objective task, choose a new objective
         if (%task == %client.objectiveTask)
         {
            AIUnassignClient(%client);
            Game.AIChooseGameObjective(%client);
         }
         return;
      }
   }

   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   //chat
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
         {
            if (%task.chat !$= "")
            {
               %chatMsg = getWord(%task.chat, 0);
               %chatTemplate = getWord(%task.chat, 1);
               if (%chatTemplate !$= "")
                  AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
               else
                  AIMessageThread(%task.chat, %client, -1);
            }
            else if (%task.targetObject > 0)
            {
               %type = %task.targetObject.getDataBlock().getName();
               if (%type $= "GeneratorLarge")
                  AIMessageThreadTemplate("AttackBase", "ChatSelfAttackGenerator", %client, -1);
               else if (%type $= "SensorLargePulse")
                  AIMessageThreadTemplate("AttackBase", "ChatSelfAttackSensors", %client, -1);
               else if (%type $= "SensorMediumPulse")
                  AIMessageThreadTemplate("AttackBase", "ChatSelfAttackSensors", %client, -1);
               else if (%type $= "TurretBaseLarge")
                  AIMessageThreadTemplate("AttackBase", "ChatSelfAttackTurrets", %client, -1);
               else if (%type $= "StationVehicle")
                  AIMessageThreadTemplate("AttackBase", "ChatSelfAttackVehicle", %client, -1);
            }
         }
      }
   }

   //--------------------------------------------------- monitor telporter - start -
   // use triumph teleporter if appropriate:
   if (%task.useTeleport) if (AIUseTeleport(%client, %task)) return;
   //--------------------------------------------------- monitor telporter - end -

   //set the target object if targetObject not destroyed
   %outOfStuff = (AIAttackOutofAmmo(%client));
   if (isObject(%task.targetObject) && !%outOfStuff && %task.targetObject.getDamageState() !$= "Destroyed")
   {
      // approach target  - pinkpanther
      %hasBeamSword        = %client.player.getInventory("BeamSword") > 0 && %client.player.getEnergyPercent() > BeamSwordImage.minEnergy;
//      %hasNapalm           = %client.player.getInventory("Napalm") > 0 && %client.player.getEnergyPercent() > 0.3;
      %hasWidowMaker       = %client.player.getInventory("WidowMaker") > 0 && %client.player.getInventory("WidowMakerAmmo") > 0;// && %enemyFlagDist < WidowMakerShot.damageRadius *1.33;
      %hasSolarCharge      = %client.player.getInventory("SolarCharge") > 0 && %client.player.getEnergyPercent() > 0.33;
      %hasHornet           = %client.player.getInventory("Hornet") > 0 && %client.player.getInventory("HornetAmmo") > 0;
      %hasPlasma           = %client.player.getInventory("Plasma") > 0 && %client.player.getInventory("PlasmaAmmo") > 0;
      %hasRailCannon       = %client.player.getInventory("RailCannon") > 0 && %client.player.getInventory("RailCannonAmmo") > 0;
      %hasMortar           = %client.player.getInventory("Mortar") > 0 && %client.player.getInventory("MortarAmmo") > 0;
//      %hasMortarStorm      = %client.player.getInventory("MortarStorm") > 0 && %client.player.getInventory("MortarStormAmmo") > 0;
      %hasMissileLauncher  = %client.player.getInventory("MissileLauncher") > 0 && %client.player.getInventory("MissileLauncherAmmo") > 0;
      %hasDisc             = %client.player.getInventory("Disc") > 0 && %client.player.getInventory("DiscAmmo") > 0;
      %hasLongRangeWeapon  = %hasSolarCharge || %hasRailCannon || %hasHornet || %hasMissileLauncher || %hasDisc; // || %hasMortarStorm
//      %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::ForceFieldObjectType;
      %targetLOS = !containerRayCast(%client.player.getMuzzlePoint($WeaponSlot), %task.targetObject.getWorldBoxCenter(), $LOSMask - (%hasBeamSword ? $TypeMasks::ForceFieldObjectType : 0), 0);
      %clientPos = %client.player.getWorldBoxCenter();
      %targetPos    = %task.targetObject.getWorldBoxCenter();
      %objDistance  = vectorDist(%clientPos, %targetPos);
      %locDistance  = vectorDist(%clientPos, %task.location);

      if (%task.locationDist > 8) {
         %client.stepMove(%task.location, 0.25, (%locDistance < 60 ? $AIModeWalk : $AIModeExpress));
      } else if (%hasWidowMaker && %objDistance < $AIWidowMakerLaunchDistance) {
         %client.setDangerLocation(%client.player.getWorldBoxCenter(), 16);
      } else if ((%hasLongRangeWeapon || %hasPlasma || %hasMortar) && %objDistance < (%hasPlasma || %hasMortar ? 200 : 300) && %targetLOS) {
         %client.stepIdle(%client.player.getWorldBoxCenter());
//error (getTaggedString(%client.name) SPC "targetObject" SPC %task.targetObject SPC "targetLOS" SPC %targetLOS);
      } else {
         %client.stepMove(%task.location,(%targetLOS ? 11 : 3), (%locDistance < 35 ? $AIModeWalk : $AIModeExpress));
      }

      if (%objDistance < 30)
         %targetMode = "Destroy";
      else
         %targetMode = "Mortar";
      if (%targetLOS || %hasBeamSword || (%hasWidowMaker && %objDistance < $AIWidowMakerLaunchDistance)){
         %client.setTargetObject(%task.targetObject, %distance +5, %targetMode);
         %task.celebrate = true;
         %task.waitTimerMS = 0;
      }
      else
         %client.setTargetObject(-1);
   }
   else
   {
      if (%task.waitTimerMS == 0) if (%task.targetObject.getDamageState() $= "Destroyed") {
         %clientPos = %client.player.getWorldBoxCenter();
         %targetPos    = %task.targetObject.getWorldBoxCenter();
			if (%task.mode $= "Nuke") %missStr = $MissionName @ " ";
			else %missStr = "";
			if (%task.locationDist) {
         if ($verbose >= 1) logEcho (%missStr @ %client.objective.targetObject SPC "LOS Dist.:" SPC %task.locationDist SPC %task.mode SPC "destroyed by" SPC %client.nameBase SPC %task.location);
			} else {
   			if ($verbose >= 1) logEcho (%missStr @ %client.objective.targetObject SPC "Dist.:" SPC vectorDist(%clientPos, %targetPos) SPC %task.mode SPC "destroyed by" SPC %client.nameBase SPC %clientPos);
			}
		}
      %client.setTargetObject(-1);
      //%client.stop();

      //aim at the floor if we need to repair ourself
      if (%client.player.getDamagePercent() > 0 && %client.player.getInventory(RepairPack) > 0)
      {
         %pos = %client.player.getWorldBoxCenter();
         %posX = getWord(%pos, 0);
         %posY = getWord(%pos, 1);
         %posZ = getWord(%pos, 2) - 1;
         %pos = %posX SPC %posY SPC %posZ;
         %client.aimAt(%pos, 500);
      }

      if (%task.celebrate)
      {
         if (%task.waitTimerMS == 0)
         {
		//add in a "woohoo"!  :)
		//choose the animation range
		%minCel = 3;
		%maxCel = 8;

            //pick a random sound
            %sound = $AITauntChat[mFloor(getRandom() * 3.99)];

            %randTime = mFloor(getRandom() * 500) + 1;
            schedule(%randTime, %client, "AIPlayAnimSound", %client, %task.targetObject.getWorldBoxCenter(), %sound, %minCel, %maxCel, 0);

            //set the timer
            %task.waitTimerMS = getSimTime();
         }
         //else see if the celebration period is over
         else if (getSimTime() - %task.waitTimerMS > 3000)
            %task.celebrate = false;
      }
      else
      {
         //if this task is the objective task, choose a new objective
         if (%task == %client.objectiveTask)
         {
            AIUnassignClient(%client);
            Game.AIChooseGameObjective(%client);
         }
      }
   }
}

//----------------------------------------------------------------------------------------------- AI Repair Object ---

function AIRepairObject::initFromObjective(%task, %objective, %client)
{
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;
   //need to force this objective to only require a repair pack
   %task.equipment = "Light RepairPack || Sniper RepairPack || Recon RepairPack || Medium RepairPack || FieldTech RepairGun2";
   %task.desiredEquipment = "RepairGun2";
   %task.buyEquipmentSet = "FieldTechRepairSet MediumRepairSet";  //%objective.buyEquipmentSet;
   %task.issuedByClient = %objective.issuedByClientId;
   %task.location = %objective.location;

    if (%objective.deathCountByLava > 0) {
         %task.equipment = "ElementShieldPack";
         %task.desiredEquipment = "ElementShieldPack RepairKit";
         %task.buyEquipmentSet = "FieldTechElementShield";
    }

   %task.deployed = %objective.deployed;
   if (%task.deployed)
   {
      if (!isObject(%task.targetObject) && isObject(%objective.repairObjective))
      {
         //if this task is the objective task, choose a new objective
         if (%task == %client.objectiveTask)
         {
            AIUnassignClient(%client);
            Game.AIChooseGameObjective(%client);
         }
         return;
      }
      %task.location = %objective.position;
      %task.deployDirection = MatrixMulVector("0 0 0 " @ getWords(%objective.getTransform(), 3, 6), "0 1 0");
      %task.deployDirection = VectorNormalize(%task.deployDirection);
   }
}

function AIRepairObject::assume(%task, %client)
{
   %task.setWeightFreq(15);
   %task.setMonitorFreq(15);
   %client.needEquipment = AINeedEquipment(%task.equipment, %client);

   //clear the target object, and range it
   %client.setTargetObject(-1);
   if (! %client.needEquipment)
   {
      if (%task.deployed)
      {
         if (!isObject(%task.targetObject) && isObject(%objective.repairObjective))
         {
            //if this task is the objective task, choose a new objective
            if (%task == %client.objectiveTask)
            {
               AIUnassignClient(%client);
               Game.AIChooseGameObjective(%client);
            }
		return;
         }
         %task.repairLocation = VectorAdd(%task.location,VectorScale(%task.deployDirection, -4.0));//was -4
         %client.stepMove(%task.repairLocation, 0.25);
      }
      //if the objective marker was moved than we have a problemed target
      //and we move to the objective marker to fix bot getting stuck trying
      //to repair object - Lagg... 10-10-2003
      else if (%task.targetObject.getWorldBoxCenter() == %task.location)
         %client.stepRangeObject(%task.targetObject, "DefaultRepairBeam", 2, 4);
      else
	      %client.stepMove(%task.location);
   }

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();
   %task.needToRangeTime = 0;
   %task.pickupRepairPack = -1;
   %task.usingInv = false;

   //set a tag to help the repairPack.cs script fudge acquiring a target
   %client.repairObject = %task.targetObject;

   //--------------------------------------------------- assume telporter - start -
   //if the MPB Teleporter is online - Lagg... 9-30-2003
   // ZOD: Less code this way...
//    if(MPBTeleporterCheck(%client, %task))
      %task.useTeleport = true;
//    else
//       %task.useTeleport = false;
   //--------------------------------------------------- assume telporter - end -
}

function AIRepairObject::retire(%task, %client)
{
   %client.setTargetObject(-1);
   %client.repairObject = -1;
}

function AIRepairObject::weight(%task, %client)
{
   //update the task weight
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;

   //let the monitor decide when to stop repairing
   %task.setWeight(%task.baseWeight);
}

function AIRepairObject::monitor(%task, %client)
{

   //echo("AIRepairObject --- monitor");
   //first, buy the equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);

      //first, see if we still need a repair pack
      if ((%client.player.getInventory(RepairPack) > 0) || (%client.player.getInventory(RepairGun2) > 0))
      {
         %client.needEquipment = false;
         %task.setMonitorFreq(15);
//echo (getTaggedString(%client.name) SPC "needEq:" SPC %client.needEquipment);

         //if this is to repair a deployed object, walk to the deploy point...
         if (%task.deployed)
         {
            if (!isObject(%task.targetObject) && isObject(%objective.repairObjective))
            {
               %client.setTargetObject(-1);
               //if this task is the objective task, choose a new objective
               //if (%task == %client.objectiveTask)
               //{
                  //AIUnassignClient(%client);
                  //Game.AIChooseGameObjective(%client);
               //}
               AIClearObjective(%objective.repairObjective);
               %objective.repairObjective.delete();
               %objective.repairObjective = "";
               clearObjectiveFromTable(%objective.repairObjective);//new function below to stop objective spam - Lagg... 1-27-2004
               echo("AIRepairObject - DOES THIS GET CALLED AT ALL it is an error you know init - is not an object so delete repairobjective");
               return;
            }
            %task.repairLocation = VectorAdd(%task.location,VectorScale(%task.deployDirection, -4.0));//was -4
            %client.stepMove(%task.repairLocation, 0.25);
         }
         //if the objective marker was moved than we have a problemed target
         //and we move to the objective marker to fix bot getting stuck trying
         //to repair object - Lagg... 10-10-2003
         if (%task.targetObject.getDataBlock().getName() $= "MobileBaseVehicle")
            %client.stepRangeObject(%task.targetObject, "DefaultRepairBeam", 9, 10);
         else if (%task.targetObject.getWorldBoxCenter() == %task.location) {
            if (%client.player.getInventory(RepairGun2) > 0)
               %client.stepRangeObject(%task.targetObject, "DefaultRepairBeam", 2, 30);
            else
               %client.stepRangeObject(%task.targetObject, "DefaultRepairBeam", 2, 4);
         }
         else
            %client.stepMove(%task.location);
      }
      else
      {
         // check to see if there's a repair pack nearby
         %closestRepairPack = -1;
         %closestRepairDist = 32767;

         //search the AIItemSet for a repair pack (someone might have dropped one...)
         %itemCount = $AIItemSet.getCount();
         for (%i = 0; %i < %itemCount; %i++)
         {
            %item = $AIItemSet.getObject(%i);
            if(isObject(%item)) // ZOD: Console spam fix, if it was picked up at this time it no exist!
            {
               if (%item.getDataBlock().getName() $= "RepairPack" && !%item.isHidden())
               {
                  %dist = %client.getPathDistance(%item.getWorldBoxCenter());
                  if (%dist > 0 && %dist < %closestRepairDist)
                  {
                     %closestRepairPack = %item;
                     %closestRepairDist = %dist;
                  }
               }
            }
         }
//echo (getTaggedString(%client.name) SPC "needEq:" SPC %client.needEquipment SPC "closestRepairDist:" SPC %closestRepairDist);

         //choose whether we're picking up the closest pack, or buying from an inv station...
         //with all these errors by Dynamix it is a wonder the bots do anything - Lagg... 3-25-2003
         if ((isObject(%closestRepairPack) && %closestRepairPack != %task.pickupRepairPack) || (%task.buyInvTime != %client.buyInvTime))
         {
            %task.pickupRepairPack = %closestRepairPack;

            //initialize the inv buying
            %task.buyInvTime = getSimTime();
            AIBuyInventory(%client, "RepairPack || RepairGun2", %task.buyEquipmentSet, %task.buyInvTime, %task);

            //now decide which is closer
            if (isObject(%closestRepairPack))
            {
               //added a little here to check for power - Lagg... - 3-25-2003
               if (isObject(%client.invToUse) && (%client.invToUse.isEnabled()) && (%client.invToUse.isPowered()))
               {
                  %dist = %client.getPathDistance(%client.invToUse.getWorldBoxCenter());
                  if (%dist < %closestRepairDist)
                     %task.usingInv = true;
                  else
                     %task.usingInv = false;
               }
               else
                  %task.usingInv = false;
            }
            else
               %task.usingInv = true;
         }
//echo (getTaggedString(%client.name) SPC "usingInv:" SPC %task.usingInv SPC "buyEquipmentSet:" SPC %task.buyEquipmentSet);

         //now see if we found a closer repair pack
         if (!%task.usingInv)
         {
            %client.stepMove(%task.pickupRepairPack.position, 0.25);
            %distToPack = %client.getPathDistance(%task.pickupRepairPack.position);
            if (%distToPack < 10 && %client.player.getMountedImage($BackpackSlot) > 0)
               %client.player.throwPack();

            //and we're finished until we actually have a repair pack... should now! - Lagg...
            return;
         }
         else
         {
            %result = AIBuyInventory(%client, "RepairPack || RepairGun2", %task.buyEquipmentSet, %task.buyInvTime, %task);
            if (%result $= "InProgress")
            {
               //force a nervous reaction every 15 sec - Lagg...
               if (getSimTime() - %task.buyInvTime > 15000)
               {
                  %client.setDangerLocation(%client.player.getWorldBoxCenter(), 20);
                  %task.buyInvTime = getSimTime();
               }
               return;
            }
            else if (%result $= "Finished")
            {
               %client.needEquipment = false;
               %task.setMonitorFreq(15);

               //if this is to repair a deployed object, walk to the deploy point...
               if (%task.deployed)
               {
                  %task.repairLocation = VectorAdd(%task.location,VectorScale(%task.deployDirection, -6.0));//was -4
                  %client.stepMove(%task.repairLocation, 0.25);
               }
               //if the objective marker was moved than we have a problemed target
               //and we move to the objective marker to fix bot getting stuck trying
               //to repair object - Lagg... 10-10-2003
               else if (%task.targetObject.getDataBlock().getName() $= "MobileBaseVehicle")
                  %client.stepRangeObject(%task.targetObject, "DefaultRepairBeam", 9, 10);
               else if (%task.targetObject.getWorldBoxCenter() == %task.location)
                  %client.stepRangeObject(%task.targetObject, "DefaultRepairBeam", 2, 4);
               else
	               %client.stepMove(%task.location);
            }
            else if (%result $= "Failed")
            {
               //if this task is the objective task, choose a new objective
               if (%task == %client.objectiveTask)
               {
                  AIUnassignClient(%client);
                  Game.AIChooseGameObjective(%client);
               }
               return;
            }
         }
      }
   }
   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   //chat
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
         {
            if (%task.chat !$= "")
            {
               %chatMsg = getWord(%task.chat, 0);
               %chatTemplate = getWord(%task.chat, 1);
               if (%chatTemplate !$= "")
                  AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
               else
                  AIMessageThread(%task.chat, %client, -1);
            }
            else if (%task.targetObject > 0)
            {
               %type = %task.targetObject.getDataBlock().getName();
               if (%type $= "GeneratorLarge")
                  AIMessageThreadTemplate("RepairBase", "ChatSelfRepairGenerator", %client, -1);
               else if (%type $= "StationVehicle")
                  AIMessageThreadTemplate("RepairBase", "ChatSelfRepairVehicle", %client, -1);
               else if (%type $= "SensorLargePulse")
                  AIMessageThreadTemplate("RepairBase", "ChatSelfRepairSensors", %client, -1);
               else if (%type $= "SensorMediumPulse")
                  AIMessageThreadTemplate("RepairBase", "ChatSelfRepairSensors", %client, -1);
               else if (%type $= "TurretBaseLarge")
                  AIMessageThreadTemplate("RepairBase", "ChatSelfRepairTurrets", %client, -1);
            }
         }
      }
   }

   //--------------------------------------------------- monitor telporter - start -
   // use triumph teleporter if appropriate:
   if (%task.useTeleport) if (AIUseTeleport(%client, %task)) return;
   //--------------------------------------------------- monitor telporter - end -

   //if deployable got killed before we get to it - Lagg...
   if (!isObject(%task.targetObject))
   {
      %client.setTargetObject(-1);

      //if this task is the objective task, choose a new objective
      if (%task == %client.objectiveTask)
      {
            AIUnassignClient(%client);
	         Game.AIChooseGameObjective(%client);
            return;
      }
   }
   //if(isObject(%task.targetObject)) // ZOD: Console spam fix
   //{
   if (%task.targetObject.getDamagePercent() > 0)
   {
      //make sure we still have equipment
      %client.needEquipment = AINeedEquipment(%task.equipment, %client);
      if (%client.needEquipment)
      {
         //if this task is the objective task, choose a new objective
         if (%task == %client.objectiveTask)
         {
            AIUnassignClient(%client);
            Game.AIChooseGameObjective(%client);
            return;
         }
      }

      if (%task.deployed)
      {
         //see if we're within range of the deploy location
         %clLoc = %client.player.position;
         %distance = VectorDist(%clLoc, %task.repairLocation);
         %dist2D = VectorDist(%client.player.position, getWords(%task.repairLocation, 0, 1) SPC getWord(%client.player.position, 2));

         //set the aim when we get near the target...  this will be overwritten when we're actually trying to repair deployed
         if (%distance < 10 && %dist2D < 10)
            %client.aimAt(%task.location, 1000);

         //see if we're at the deploy location
         if ((%client.pathDistRemaining(20) > %distance + 0.25) || %dist2D > 0.3)
         {
            %client.setTargetObject(-1);
            %client.stepMove(%task.repairLocation, 0.25);
         }
         else
         {
            %client.stop();
            %client.setTargetObject(%task.targetObject, 12, "Repair");//increased range - Lagg...
         }
      }
      else
      {
         //modified here in case bot gets stuck trying to get to repair spot - Lagg... - 4-21-2003
         %currentTime = getSimTime();
         if (%currentTime > %task.needToRangeTime)
         {
            //force a rangeObject every 45 seconds...
            %task.needToRangeTime = %currentTime + 45000;
            %client.setTargetObject(-1);
            %client.setDangerLocation(%task.targetObject.position, 15);
            //%client.player.cycleWeapon();
            //if ( !%client.player.getImageTrigger( $BackpackSlot ) )
            //%client.player.setImageTrigger( $BackpackSlot, true );

            //if the objective marker was moved than we have a problemed target
            //and we move to the objective marker to fix bot getting stuck trying
            //to repair object - Lagg... 10-10-2003
            if (%task.targetObject.getDataBlock().getName() $= "MobileBaseVehicle")
               %client.stepRangeObject(%task.targetObject, "DefaultRepairBeam", 9, 10);
            else if (%task.targetObject.getWorldBoxCenter() == %task.location)
               %client.stepRangeObject(%task.targetObject, "DefaultRepairBeam", 2, 4);
            else
               %client.stepMove(%task.location);
//echo (getTaggedString(%client.name) SPC "Moving" SPC "vecDist" SPC VectorDist(%client.player.getWorldBoxCenter(), %task.location));
         }
         //if we've ranged the object, start repairing, else unset the object
         else if (%client.getStepStatus() $= "Finished")
         {
            //dissolve the human control link
            if (%task == %client.objectiveTask)
               aiReleaseHumanControl(%client.controlByHuman, %client);

            %client.setTargetObject(%task.targetObject, 13, "Repair");
//error (getTaggedString(%client.name) SPC "Finished" SPC "vecDist" SPC VectorDist(%client.player.getWorldBoxCenter(), %task.location));
            //if (%task.targetObject.getDataBlock().getName() $= "MobileBaseVehicle")
            //echo("REPAIRING A MPB = " @ getTaggedString(%client.name));
         }
         else
            %client.setTargetObject(-1);
      }
   }
   else
   {
      %client.setTargetObject(-1);

      //if this task is the objective task, choose a new objective
      if (%task == %client.objectiveTask)
      {
         AIUnassignClient(%client);
         Game.AIChooseGameObjective(%client);
      }
   }
   //}
   //else // ZOD: Part of above spam fix
   //{
   //   %client.setTargetObject(-1);

      //if this task is the objective task, choose a new objective
   //   if (%task == %client.objectiveTask)
   //   {
   //      AIUnassignClient(%client);
   //      Game.AIChooseGameObjective(%client);
   //   }
   //}
}

//----------------------------------------------------------------------------------------------- AI Laze Object ---

function AILazeObject::initFromObjective(%task, %objective, %client)
{
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;
   %task.issuedByClient = %objective.issuedByClientId;
   %task.msgAck = true;
   %task.msgFire = true;

   %task.buyEquipmentSet = %objective.buyEquipmentSet;
   %task.equipment = strReplace(%objective.equipment, "Plasma", "Hornet");
   %task.desiredEquipment = strReplace(%objective.desiredEquipment, "Plasma", "Hornet");

    if ((%objective.deathCountByTurret > 0) || (%objective.deathCountByLava > 0)) {
       if (%objective.deathCountByTurret >= %objective.deathCountByLava) {
         %task.equipment = "SensorJammerPack || AntiEnergyPack";
         %task.desiredEquipment = "SensorJammerPack RepairKit || AntiEnergyPack RepairKit";
         %task.buyEquipmentSet = "MediumSensorJammerTriumph FieldTechSensorJammer HeavySensorJammer LightSensorJammer ReconShotgun";
       } else {
         %task.equipment = "ElementShieldPack";
         %task.desiredEquipment = "ElementShieldPack RepairKit";
         %task.buyEquipmentSet = "SniperElementShield MediumElementShield FieldTechElementShield HeavyElementShield TitanElementShield LightElementShield";
       }
    }

}

function AILazeObject::assume(%task, %client)
{
   %task.setWeightFreq(30);
   %task.setMonitorFreq(30);
   %client.needEquipment = AINeedEquipment(%task.equipment, %client);

   //clear the target object, and range it
   %client.setTargetObject(-1);
   if (!%client.needEquipment)
      %client.stepRangeObject(%task.targetObject, "BasicTargeter", 80, 300, %task.issuedByClient.player.getWorldBoxCenter());

   //set up some task vars
   %task.celebrate = false;
   %task.waitTimerMS = 0;

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();
}

function AILazeObject::retire(%task, %client)
{
   %client.setTargetObject(-1);
}

function AILazeObject::weight(%task, %client)
{
   //update the task weight
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;

   //let the monitor decide when to stop lazing
   %task.setWeight(%task.baseWeight);


   AISensorJammerActivation(%client);
}

function AILazeObject::monitor(%task, %client)
{

   //first, buy the equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;

      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime, %task);
      if (%result $= "InProgress")
      {
         //force a nervous reaction every 16 sec - Lagg...
         if (getSimTime() - %task.buyInvTime > 16000)
         {
            %client.setDangerLocation(%client.player.getWorldBoxCenter(), 20);
            %task.buyInvTime = getSimTime();
         }
         return;
      }
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(30);
         %client.needEquipment = false;
         %client.stepRangeObject(%task.targetObject, "BasicTargeter", 80, 300);
      }
      else if (%result $= "Failed")
      {
         //if this task is the objective task, choose a new objective
         if (%task == %client.objectiveTask)
         {
            AIUnassignClient(%client);
            Game.AIChooseGameObjective(%client);
         }
         return;
      }
   }
   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   //set the target object
   if (isObject(%task.targetObject) && %task.targetObject.getDamageState() !$= "Destroyed")
   {
      //make sure we still have equipment
      %client.needEquipment = AINeedEquipment(%task.equipment, %client);
      if (%client.needEquipment)
      {
         //if this task is the objective task, choose a new objective
         if (%task == %client.objectiveTask)
         {
            AIUnassignClient(%client);
            Game.AIChooseGameObjective(%client);
            return;
         }
      }

      //look to see if anyone else is also targetting...
      %foundTarget = false;
      %numTargets = ServerTargetSet.getCount();
      for (%i = 0; %i < %numTargets; %i++)
      {
         %targ = ServerTargetSet.getObject(%i);
         if (%targ.sourceObject != %client.player)
         {
            %targDist = VectorDist(%targ.getWorldBoxCenter(), %task.targetObject.getWorldBoxCenter());
            if (%targDist < 60)//was 10 - Lagg...
            {
               %foundTarget = true;
               break;
            }
         }
      }

      if (%foundTarget)
      {
         //if this task is the objective task, choose a new objective
         if (%task == %client.objectiveTask)
            AIUnassignClient(%client);
      }
      else if (%client.getStepStatus() $= "Finished")
      {
         //dissolve the human control link
         if (%task == %client.objectiveTask)
            aiReleaseHumanControl(%client.controlByHuman, %client);

         %client.setTargetObject(%task.targetObject, 300, "Laze");
         %task.celebrate = true;
         %task.waitTimerMS = 0;

         //make sure we only say "fire..." once
         if (%task.msgFire)
         {
            AIMessageThread("ChatTargetFire", %client, -1);
            %task.msgFire = false;
         }
      }
      else
      {
         %client.stepRangeObject(%task.targetObject, "BasicTargeter", 80, 300, %task.issuedByClient.player.getWorldBoxCenter());//added here - Lagg...
         %client.aimAt(%task.targetObject.getWorldBoxCenter(), 1000);
         %client.setTargetObject(-1);
      }
   }
   else
   {
      %client.setTargetObject(-1);

      if (%task.celebrate)
      {
         if (%task.waitTimerMS == 0)
         {
		//add in a "woohoo"!  :)
		//choose the animation range
		%minCel = 3;
		%maxCel = 8;

            //pick a random sound
            %sound = $AITauntChat[mFloor(getRandom() * 3.99)];

            %randTime = mFloor(getRandom() * 500) + 1;
            schedule(%randTime, %client, "AIPlayAnimSound", %client, %task.targetObject.getWorldBoxCenter(), %sound, %minCel, %maxCel, 0);

            //set the timer
            %task.waitTimerMS = getSimTime();
         }
         //else see if the celebration period is over
         else if (getSimTime() - %task.waitTimerMS > 3000)
            %task.celebrate = false;
      }
      else
      {
         //if this task is the objective task, choose a new objective
         if (%task == %client.objectiveTask)
         {
            AIUnassignClient(%client);
            Game.AIChooseGameObjective(%client);
         }
      }
   }
}

//----------------------------------------------------------------------------------------------- AI Mortar Object ---
function AIMortarObject::initFromObjective(%task, %objective, %client)
{
//error(getTaggedString(%client.name) SPC "Init:Start" SPC %objective.targetObject);
   %task.location = %objective.location;
   %task.targetObject = %objective.targetObjectId;
   %task.mode = %objective.mode;
//   %task.buyEquipmentSet = %objective.buyEquipmentSet;
   %targetPos = %task.targetObject.getWorldBoxCenter();

   %turretOrLavaDeath = (%objective.deathCountByTurret > 0) || (%objective.deathCountByLava > 0);

    // try to find a LOS location:
    %task.locationDist = VectorDist(%targetPos, %task.location);
    if (%task.locationDist < 8) {
       %proposedLocationDist = ((%turretOrLavaDeath || getWord(AIFindClosestInventory(%client, false), 0) <= 0) ? 293 : 527);
       %hasNoLOS = true;
       while (%proposedLocationDist > 8 && %hasNoLOS) {
          %task.losLocation = %client.getLOSLocation(%targetPos, 0, %proposedLocationDist);
          %losLocationDist = VectorDist(%targetPos, %task.losLocation);
          if (%losLocationDist > %proposedLocationDist + 10) {
             %losLocationDist = 0;
             %task.losLocation = %targetPos;
             %hasNoLOS = true;
//             break;
          } else {
             %hasNoLOS = containerRayCast(%task.losLocation, %targetPos, $LOSMask, 0);     // Also take forcefields into account for calculating mortar position
          }
          if (%losLocationDist > 190 && %losLocationDist < 250 && !%turretOrLavaDeath) {
             if (!%hasNoLOS) {
                %objective.mode = "Nuke";
                %objective.location = %task.losLocation;
                %objective.weightLevel1 =  $AIWeightMortarTurret[1] + 200;
                if ($verbose >= 2) echo(%client.nameBase SPC %losLocationDist SPC "Nuke Mortar: LOS Saved" SPC %objective.targetObject SPC "zDiff:" SPC (getWord(%task.losLocation, 2) - getWord(%targetPos, 2)) SPC "Loc:" SPC %task.losLocation);
             } else {
                if ($verbose >= 2) logEcho(%client.nameBase SPC %losLocationDist SPC "Nuke Mortar: No LOS" SPC %objective.targetObject);
             }
             %task.mode = "Nuke";
             %hasNoLOS = false;
          } else if (%hasNoLOS) {
             %proposedLocationDist -= 100; // retry using a closer range
          }
       }
       if (%losLocationDist > 8) {
          %task.location = %task.losLocation;
          %task.locationDist = %losLocationDist;
          if ($verbose >= 3) logEcho(%client.nameBase SPC %losLocationDist SPC "Do LOS Mortar" SPC %objective.targetObject);
       } else {
          if ($verbose >= 3) logEcho(%client.nameBase SPC "No LOS Mortar" SPC %objective.targetObject);
       }
    }
   if ($verbose >= 4) echo(%client.nameBase SPC "AIMortarObject::initFromObjective" SPC %client.objective.targetObject);

   // Add weight if turret is ELF, subtract if it is Missile
   if (strstr(%task.targetObject.getDataBlock().getName(), "Turret") >= 0 && %task.mode $= "") {
      %targImage = %task.targetObject.getMountedImage(0).getName();
      if (strstr(%targImage, "ELFBarrel") > -1) {
         %newWeight = $AIWeightMortarTurret[1] + 180;
//          %client.objectiveWeight = %client.objective.weightLevel1; // crashes !!! - pinkpanther
      } else if (strstr(%targImage, "Missile") > -1) {
         %newWeight = $AIWeightMortarTurret[1] - 120;
//          %client.objectiveWeight = %client.objective.weightLevel1; // crashes !!! - pinkpanther
      } else %newWeight = $AIWeightMortarTurret[1];
      if (%newWeight > %client.objective.weightLevel1) %client.objective.weightLevel1 =  %newWeight;
   }
   %task.baseWeight = %client.objectiveWeight;

    if (%task.mode $= "Nuke") {
         %task.equipment = "WidowMaker WidowMakerAmmo || MortarStorm MortarStormAmmo";
         %task.desiredEquipment = "WidowMaker WidowMakerAmmo";
         if (%turretOrLavaDeath) {
            if (%objective.deathCountByTurret >= %objective.deathCountByLava) {
               %task.buyEquipmentSet = "TitanSensorJammer HeavySensorJammer MediumSensorJammerTriumph FieldTechSensorJammer";
            } else {
               %task.buyEquipmentSet = "TitanElementShield HeavyElementShield MediumElementShield FieldTechElementShield SniperElementShield";
            }
         } else {
            %task.buyEquipmentSet = "TitanNuke HeavyEnergySet MediumEnergySet TurretSniper FieldTechAmmoSet";
         }
    } else if (%turretOrLavaDeath) {
       if (%objective.deathCountByTurret >= %objective.deathCountByLava) {
         %task.equipment = "SensorJammerPack MissileLauncher MissileLauncherAmmo || SensorJammerPack Hornet HornetAmmo || SensorJammerPack SolarCharge";
         %task.desiredEquipment = "SensorJammerPack RepairKit";
         %task.buyEquipmentSet = "FieldTechSensorJammer MediumSensorJammerTriumph HeavySensorJammer";
       } else {
         %task.equipment = "ElementShieldPack";
         %task.desiredEquipment = "ElementShieldPack RepairKit";
         %task.buyEquipmentSet = "FieldTechElementShield MediumElementShield SniperElementShield HeavyElementShield";
       }
    } else {
//      %task.desiredEquipment = "RailCannon RailCannonAmmo || Plasma || Medium || Heavy EnergyPack || Titan EnergyPack";
      %task.desiredEquipment = "";

//    %distToObject = %client.getPathDistance(%objPos) +2;
      %distToObject = getWord(AICheckUseTeleport(%client, %task), 1);    //new function in aiVehicle.cs
      if (%task.locationDist < 8) {
         %task.equipment = "SensorJammerPack Plasma PlasmaAmmo || SensorJammerPack MissileLauncher MissileLauncherAmmo || SensorJammerPack Hornet HornetAmmo || SensorJammerPack SolarCharge";
         %task.buyEquipmentSet = "HeavySensorJammer MediumSensorJammerTriumph FieldTechSensorJammer LightAttacker";
      } else if (%task.locationDist > 310) {
         %task.buyEquipmentSet = "TurretSniper HeavyEnergyDefault TitanNuke";   // Has RailCannon
         %task.equipment = "RailCannon RailCannonAmmo || SolarCharge";
      } else if (%task.locationDist < 180) {
//         %task.equipment = "SensorJammerPack Plasma PlasmaAmmo || SensorJammerPack MissileLauncher MissileLauncherAmmo || SensorJammerPack Hornet HornetAmmo || SensorJammerPack SolarCharge";
         %task.equipment = "Plasma PlasmaAmmo || MissileLauncher MissileLauncherAmmo || Hornet HornetAmmo || SolarCharge";
         %task.buyEquipmentSet = "FieldTechSensorJammer MediumSensorJammerTriumph HeavySensorJammer";
      } else {
         %task.equipment = "MissileLauncher MissileLauncherAmmo || Hornet HornetAmmo || RailCannon RailCannonAmmo || SolarCharge";
         %task.buyEquipmentSet = "HeavyEnergyDefault TitanNuke MediumMissileSet TurretSniper FieldTechAmmoSet";   // Has MissileLauncher
     }
   //    %objPos = %task.targetObject.getWorldBoxCenter();
   //    %enemyTeam           = (%client.team == 1) ? 2 : 1;
   //    %enemyFlag           = $AITeamFlag[%enemyTeam];
   //    %enemyFlagDist       = VectorDist(%objPos, %enemyFlag.originalPosition);

//       // Determine whether target is a type of turret that keeps on firing while the bot is mortaring. Avoid such targets or at least keep some distance   - pinkpanther
//       %losLocationDist = 100;
//       %targObjIsDangerous = false;
//       if (strstr(%task.targetObject.getDataBlock().getName(), "Turret") >= 0) {
//          %targImage = %task.targetObject.getMountedImage(0).getName();
//          if (strstr(%targImage, "Missile") < 0 && strstr(%targImage, "AABarrel") < 0 && strstr(%targImage, "ELFBarrel") < 0) {
//             %targObjIsDangerous = true;
//             %losLocation = %client.getLOSLocation(%targetPos, 1, 500);
//             %losLocationDist = vectorDist(%targetPos, %losLocation);
//          }
//       }

   }

//error(getTaggedString(%client.name) SPC "Mortar:Init" SPC %objective.targetObject SPC %objective.deathCountByTurret SPC %task.buyEquipmentSet);

   %task.issuedByClient = %objective.issuedByClientId;

   %task.sendMsgTime = 0;
   %task.sendMsg = true;
   %task.useTeleport = true;
if (($watch == %client) && ($debugInv || $debugAIMortarObject)) error (getTaggedString(%client.name) @ " initFromObjective:obj.buyEq.Set:"@%task.buyEquipmentSet@" obj.eq:"@%task.equipment@"obj.desiredEq:"@%task.desiredEquipment SPC "%task.mode" SPC %task.mode);
}

function AIMortarObject::assume(%task, %client)
{
//error(getTaggedString(%client.name) SPC "Assume:Start" SPC %objective.targetObject);
   %task.setWeightFreq(30);
   %task.setMonitorFreq(30);
   %task.state = moveToRange;
   %task.waitForTargetter = false;
   %task.celebrate = false;
   %task.waitTimerMS = 0;
   %task.targetAcquired = false;
   %task.sayAcquired = true;
   %task.useLosLocation = true;
   %client.needEquipment = AINeedEquipment(%task.equipment, %client);
//    if ((%client.objective.weightLevel1 > $AIWeightMortarTurret[1]) && (%task.mode !$= "Nuke")) {
//       %client.objective.weightLevel1 = $AIWeightMortarTurret[1];
//       %client.objectiveWeight =  $AIWeightMortarTurret[1];
//       }
   if ($verbose >= 4) echo(%client.nameBase SPC "AIMortarObject::Assume" SPC %client.objective.targetObject);

   //even if we don't *need* equipment, see if we should buy some...
   if (!%client.needEquipment && %task.buyEquipmentSet !$= "")
   {
      //see if we could benefit from inventory
      %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      if (%closestInv > 0) {   // No functioning inv station around -> don't bother using inv anymore
//          %closestInv.location = %closestInv.trigger.getWorldBoxCenter();
//          %result = AICheckUseTeleport(%client, %closestInv);//new function in aiVehicle.cs
         %closestDist = getWord(%result, 1);
         if (AINeedEquipment(%task.desiredEquipment, %client) && %closestDist < 200)
            %client.needEquipment = true;
      }
   }

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();

   //--------------------------------------------------- assume telporter - start -
   //if the MPB Teleporter is online - Lagg... 9-30-2003
   // ZOD: Less code this way...
//    if(MPBTeleporterCheck(%client, %task))
      %task.useTeleport = true;
//    else
//       %task.useTeleport = false;
   //--------------------------------------------------- assume telporter - end -


   %task.losLocation = %client.getLOSLocation(%task.location, 5, 500);
//error(getTaggedString(%client.name) SPC "Assume:End" SPC %objective.targetObject);
}

function AIMortarObject::retire(%task, %client)
{
   %client.setTargetObject(-1);
   //remove the associated lazeObjective
   if (%task.targetterObjective)
   {
      clearObjectiveFromTable(%task.targetterObjective);
      AIClearObjective(%task.targetterObjective); //new function to stop objective spam - Lagg... 1-27-2004
      %task.targetterObjective.delete();
      %task.targetterObjective = "";
   }
//error(getTaggedString(%client.name) SPC "Retire:End" SPC %objective.targetObject);
}

function AIMortarObject::weight(%task, %client)
{
   //update the task weight
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;
   //let the monitor decide when to stop mortaring
   %task.setWeight(%task.baseWeight);


   AISensorJammerActivation(%client);
//error(getTaggedString(%client.name) SPC "Weight:End" SPC %objective.targetObject);
}

function AIMortarObject::monitor(%task, %client)
{
//error(getTaggedString(%client.name) SPC "Monitor:Start" SPC %objective.targetObject);

   //first, buy the equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;

      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime, %task);
      if (%result $= "InProgress")
      {
         //force a nervous reaction every 15 sec - Lagg...
         if (getSimTime() - %task.buyInvTime > 15000)
         {
            %client.setDangerLocation(%client.player.getWorldBoxCenter(), 20);
            %task.buyInvTime = getSimTime();
         }
         return;
      }
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(30);
	 %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
         //if this task is the objective task, choose a new objective
	 if (%task == %client.objectiveTask)
	 {
	    AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	 }
	 return;
      }
   }
//error(getTaggedString(%client.name) SPC "Monitor:Start2" SPC %objective.targetObject);

if ($watch == %client) echo(getTaggedString(%client.name) @ " Mortar::Monitor past inventory buying");
   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();
   //chat
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
	 if (%client.isAIControlled())
	 {
	    if (%task.chat !$= "")
	    {
	       %chatMsg = getWord(%task.chat, 0);
	       %chatTemplate = getWord(%task.chat, 1);
	       if (%chatTemplate !$= "")
	          AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
	       else
		  AIMessageThread(%task.chat, %client, -1);
	    }
	    else if (%task.targetObject > 0)
	    {
	       %type = %task.targetObject.getDataBlock().getName();
	       if (%type $= "GeneratorLarge")
	          AIMessageThreadTemplate("AttackBase", "ChatSelfAttackGenerator", %client, -1);
	       else if (%type $= "SensorLargePulse")
	          AIMessageThreadTemplate("AttackBase", "ChatSelfAttackSensors", %client, -1);
	       else if (%type $= "SensorMediumPulse")
	          AIMessageThreadTemplate("AttackBase", "ChatSelfAttackSensors", %client, -1);
	       else if (%type $= "TurretBaseLarge")
		  AIMessageThreadTemplate("AttackBase", "ChatSelfAttackTurrets", %client, -1);
	       else if (%type $= "StationVehicle")
		  AIMessageThreadTemplate("AttackBase", "ChatSelfAttackVehicle", %client, -1);
	    }
         }
      }
   }
//error(getTaggedString(%client.name) SPC "Monitor:Start3" SPC %objective.targetObject);

   //--------------------------------------------------- monitor telporter - start -
   // use triumph teleporter if appropriate:
   if (%task.useTeleport) if (AIUseTeleport(%client, %task)) return;
   //--------------------------------------------------- monitor telporter - end -
//error(getTaggedString(%client.name) SPC "Monitor:Start4" SPC %objective.targetObject);

   %enemyTeam           = (%client.team == 1) ? 2 : 1;
   %enemyFlag           = $AITeamFlag[%enemyTeam];
   %enemyFlagPos        = %enemyFlag.originalPosition;
   %targetObjectVect    = getWord(%targetPos, 0) SPC getWord(%targetPos, 1) SPC "0";
   %enemyFlagVect       = getWord(%enemyFlagPos, 0) SPC getWord(%enemyFlagPos, 1) SPC "0";
   %enemyFlagDist       = VectorDist(%targetObjectVect, %enemyFlagVect);
   //make sure we still have something to destroy
   if (%task.targetObject.getDamageState() !$= "Destroyed" && isObject(%task.targetObject)
      && %task.targetObject.getDamagePercent() < 0.98)
   {
if ($watch == %client) echo(getTaggedString(%client.name) @ " Mortar::Monitor we still have something to destroy");
      %clientPos = %client.player.getWorldBoxCenter();
      %pathDistance = %client.getPathDistance(%task.location);
      %targetPos    = %task.targetObject.getWorldBoxCenter();
      %objDistance  = vectorDist(%clientPos, %targetPos);
      %locDistance  = vectorDist(%clientPos, %task.location);
      if (%locDistance < 0)
         %locDistance = 32767;

      // Determine whether a turret would keep on firing while the bot is mortaring. Avoid such targets or at least keep some distance   - pinkpanther
      %targObjIsDangerous = false;
      %hasSensorJammerPack = %client.player.getInventory("SensorJammerPack") > 0 || %client.player.getInventory("AntiEnergyPack") > 0;
      if (!%hasSensorJammerPack && strstr(%task.targetObject.getDataBlock().getName(), "Turret") >= 0) {
         %targImage = %task.targetObject.getMountedImage(0).getName();
         if (strstr(%targImage, "Missile") < 0 && strstr(%targImage, "AABarrel") < 0 && strstr(%targImage, "ELFBarrel") < 0)
            %targObjIsDangerous = true;
      }
//error(getTaggedString(%client.name) SPC "Monitor:Start5" SPC %objective.targetObject);

      %missileLauncherAmmo = %client.player.getInventory("MissileLauncherAmmo");
      %hornetAmmo          = %client.player.getInventory("HornetAmmo");
      %hasMissileLauncher  = %client.player.getInventory("MissileLauncher") > 0 && %missileLauncherAmmo > 0;
      %hasHornet           = %client.player.getInventory("Hornet") > 0 && %hornetAmmo > 0;
      //  Make him walk closer to the target if he has more missiles than hornets   - pinkpanther
      if (%hasHornet && %hasMissileLauncher) if (%missileLauncherAmmo > %hornetAmmo) %hasHornet = false;
      %hasPlasma           = %client.player.getInventory("Plasma") > 0 && %client.player.getInventory("PlasmaAmmo") > 0;
      %hasSolarCharge      = %client.player.getInventory("SolarCharge") > 0 && %client.player.getEnergyPercent() > 0.33;
      %hasBeamSword        = %client.player.getInventory("BeamSword") > 0;
      %hasDualBlaster      = %client.player.getInventory("DualBlaster") > 0;
      %hasBlaster          = %client.player.getInventory("Blaster") > 0;
      %hasNapalm           = %client.player.getInventory("Napalm") > 0;
      %hasRailCannon       = %client.player.getInventory("RailCannon") > 0 && %client.player.getInventory("RailCannonAmmo") > 0;
      %hasWidowMaker       = %client.player.getInventory("WidowMaker") > 0 && %client.player.getInventory("WidowMakerAmmo") > 0;// && %enemyFlagDist < WidowMakerShot.damageRadius *1.33;
      %hasMortarStorm      = %client.player.getInventory("MortarStorm") > 0 && %client.player.getInventory("MortarStormAmmo") > 0;
      %hasShotgun          = %client.player.getInventory("Shotgun") > 0 && %client.player.getInventory("ShotgunAmmo") > 0;
//      %hasMortar          = %client.player.getInventory("Mortar") > 0 && %client.player.getInventory("MortarAmmo") > 0;
      %hasShortRangeWeapon = %hasShotgun || %hasBlaster;
      %hasLongRangeWeapon  = %hasSolarCharge || %hasRailCannon || %hasHornet;
      %targetWithWeapon    = %hasShortRangeWeapon || %hasLongRangeWeapon || %hasMissileLauncher;
      if (%hasWidowMaker) {// && !%targObjIsDangerous
         %stepDist = $AIWidowMakerLaunchDistance;//(%targObjIsDangerous ? 360 : 240);
//error (getTaggedString(%client.name) SPC "hasWidowMaker" SPC %pathDistance SPC "vecDist" SPC %locDistance);
      }
      else if (%hasDualBlaster && !%targObjIsDangerous) %stepDist = 45;
      else if (%hasLongRangeWeapon) %stepDist = 520;
      else if (%hasMissileLauncher) %stepDist = 400;
      else if (%hasPlasma || %hasDualBlaster) %stepDist = 190;
      else if (%hasShortRangeWeapon && !%targObjIsDangerous) %stepDist = LightBeam.maxRifleRange;
      else %stepDist = 300;


//error(getTaggedString(%client.name) SPC "Monitor:Mid1" SPC %objective.targetObject);
      if (%task.locationDist < 8) {
         %mask = $LOSMask - (%hasBeamSword ? $TypeMasks::ForceFieldObjectType : 0);
         %hasLOS = !containerRayCast(%client.player.getMuzzlePoint($WeaponSlot), %task.targetObject.getWorldBoxCenter(), %mask, 0);

         %invalidLosCalc = containerRayCast(%task.losLocation, %task.targetObject.getWorldBoxCenter(), %mask, 0);
         %losLocationDist = vectorDist(%targetPos, %task.losLocation);
         if (%losLocationDist > %stepDist || %invalidLosCalc) {
   			%currentTime = getSimTime();
   			if (%currentTime > %task.needToRangeTime) {
   				//do a getLOSLocation every 20 seconds...
   				%task.needToRangeTime = %currentTime + 20000;
               %task.losLocation = %client.getLOSLocation(%targetPos, 5, %stepDist);
               %losLocationDist = vectorDist(%targetPos, %task.losLocation);
               %invalidLosCalc = containerRayCast(%task.losLocation, %task.targetObject.getWorldBoxCenter(), %mask, 0);
            }
         }
         if (%losLocationDist > %stepDist || %invalidLosCalc || (%locDistance < %stepDist && %hasLOS))
            %task.useLosLocation = false;
         if (%task.useLosLocation)
            %xyDistance = vectorDist(getWord(%clientPos, 0) SPC getWord(%clientPos, 1) SPC "0", getWord(%task.losLocation, 0) SPC getWord(%task.losLocation, 1) SPC "0");
      } else {
         %task.useLosLocation = false;
         %hasLOS = true;
         %stepDist = 2;
      }
//error(getTaggedString(%client.name) SPC "Monitor:Mid2" SPC %objective.targetObject);

      //make sure we still have equipment
      %client.needEquipment = AINeedEquipment(%task.equipment, %client);
      if (%client.needEquipment)
      {
         //if this task is the objective task, choose a new objective
         if (%task == %client.objectiveTask)
	      {
            AIUnassignClient(%client);
	         Game.AIChooseGameObjective(%client);
	         return;
	      }
      }

      //next move to losLocation
      else if (%task.useLosLocation && %xyDistance > 1 + 0.05 * %losLocationDist)
      {
if ($watch == %client) echo (getTaggedString(%client.name) SPC "LOS:Move2Loc" SPC %client.getStepName() SPC %client.getStepStatus() SPC %stepDist SPC %xyDistance);
         %client.setTargetObject(-1);
         %client.stepMove(%task.losLocation);
//         %client.stepMove(%task.losLocation, 4.0, $AIModeWalk);

      }
      else if (%task.useLosLocation && !%hasLOS)
      {
         %client.setTargetObject(-1);
			%currentTime = getSimTime();
			if (%currentTime > %task.needToRangeTime) {
				//do a getLOSLocation every 10 seconds...
				%task.needToRangeTime = %currentTime + 10000;
            %newLosLocation = %client.getLOSLocation(%task.location, 5, 0.9 * %losLocationDist);
if ($watch == %client) echo (getTaggedString(%client.name) SPC "LOS:hasNoLOS:newLos" SPC %client.getStepName() SPC %client.getStepStatus() SPC %stepDist SPC %xyDistance);
            if (%newLosLocation $= %task.losLocation) {
               %task.losLocation = %enemyFlagPos;
               %task.location = %enemyFlagPos;
               %client.stepMove(%task.losLocation);
            } else {
               %task.losLocation = %newLosLocation;
               %client.stepMove(%task.losLocation, 0.5);
            }
         }
      }
      // move to within %stepDist (VectorDist)
      else if (!%task.useLosLocation && (%locDistance > %stepDist || !(%hasLOS || %hasBeamSword)))// || (%hasWidowMaker && %objDistance < $AIWidowMakerLaunchDistance)
      {
         %client.setTargetObject(-1);
         if (%objDistance < 10)
            %client.setDangerLocation(%client.player.getWorldBoxCenter(), 12);
         else if (%objDistance < 35 && %task.locationDist < 8) {
            %client.setDangerLocation(%task.targetObject.position, 15);
            if (%hasLOS)
               %client.stepRangeObject(%task.targetObject, "NapalmBolt", 6, 8);
            else
               %client.stepRangeObject(%task.targetObject, "LightBeam", 2, 4);
         } else {
            %client.stepMove(%task.location, 0.25, (%locDistance < 100 ? $AIModeWalk : $AIModeExpress));
//if ($watch == %client) error (getTaggedString(%client.name) SPC "NoLOS:Moving:locDistance" SPC %locDistance SPC %client.getStepStatus() SPC %objDistance SPC %stepDist);
         }

      }
      else if (%task.waitForTargetter)      //now start ask for someone to laze the target, and start a 20 sec timer
      {
if ($watch == %client) echo (getTaggedString(%client.name) SPC "waitForTargetter");
         //see if we've started the timer
         if (%task.waitTimerMS == 0)
         {
	    //range the object
//	    %client.stepRangeObject(%task.targetObject, "BasicTargeter", 200, 300);
         %client.stepRangeObject(%task.targetObject, "BasicTargeter", %pathDistance -50, %pathDistance);

            //now ask for a targeter...
	    %targetType = %task.targetObject.getDataBlock().getName();
	    if (%targetType $= "TurretBaseLarge")
	       AIMessageThread("ChatCmdTargetTurret", %client, -1);
	    else if (%targetType $= "SensorLargePulse")
	       AIMessageThread("ChatCmdTargetSensors", %client, -1);
	    else if (%targetType $= "SensorMediumPulse")
	       AIMessageThread("ChatCmdTargetSensors", %client, -1);
	    else
	       AIMessageThread("ChatTargetNeed", %client, -1);

            %task.waitTimerMS = getSimTime();

            //create the objective
	    if (! %task.targetterObjective)
          {
	       %task.targetterObjective = new AIObjective(AIOLazeObject)
	               {
		               dataBlock = "AIObjectiveMarker";
	                       weightLevel1 = $AIWeightLazeObject[1];
	                       weightLevel2 = $AIWeightLazeObject[2];
	                       description = "Laze the " @ %task.targetObject.getName();
	                       targetObjectId = %task.targetObject;
			                 issuedByClientId = %client;
	                       offense = true;
	                       equipment = %targetWithWeapon ? "Hornet":"TargetingLaser";
	               };

               MissionCleanup.add(%task.targetterObjective);

               //if is siege game we have to do this right - Lagg... 11-3-2003
               if (Game.class $= "SiegeGame")
               {
                  //the objectives in $ObjectiveQ[1] are all offense objectives, $ObjectiveQ[2] defense
                  if (%client.team == game.offenseTeam)
                     $ObjectiveQ[1].add(%task.targetterObjective);
                  else
                     $ObjectiveQ[2].add(%task.targetterObjective);
               }
               else
                  $ObjectiveQ[%client.team].add(%task.targetterObjective);
            }
	    %task.targetterObjective.lastLazedTime = 0;

	    //remove the escort (want a targetter instead)
//	    if (%client.escort)
//	    {
//          clearObjectiveFromTable(%client.escort); //new function to stop objective spam - Lagg... 1-27-2004
//	       AIClearObjective(%client.escort);
//	       %client.escort.delete();
//	       %client.escort = "";
//	    }
         }
         else
         {
            %elapsedTime = getSimTime() - %task.waitTimerMS;
	    if (%task.targetterObjective.group > 0)
	       %targetter = %task.targetterObjective.group.clientLevel[1];
	    else
	       %targetter = %task.targetterObjective.clientLevel[1];

	    //see if we can find a target near our objective
	    %task.targetAcquired = false;
	    %numTargets = ServerTargetSet.getCount();
	    for (%i = 0; %i < %numTargets; %i++)
	    {
	       %targ = ServerTargetSet.getObject(%i);
	       %targDist = VectorDist(%targ.getWorldBoxCenter(), %task.targetObject.getWorldBoxCenter());
	       if (%targDist < 300)
	       {
	          %task.targetAcquired = true;
		  break;
	       }
	    }

	    if (%task.targetAcquired)
            {
               %task.waitForTargetter = false;
	            %task.waitTimerMS = 0;
	            %task.celebrate = true;
	            %task.sayAcquired = false;
	            AIMessageThread("ChatTargetAcquired", %client, -1);
            }

	    //else see if we've run out of time
	    else if (! %targetter || ! %targetter.isAIControlled() || %elapsedTime > (%targetWithWeapon ? 3000 : 20000))
	    {
          %task.waitForTargetter = false;
	       %task.waitTimerMS = 0;
	       %task.celebrate = true;
	    }
         }
      }

      //now we should finally be attacking with or without a targetter
      //eventually, the target will be destroyed, or we'll run out of ammo...
      else
      {
if ($watch == %client) echo (getTaggedString(%client.name) SPC "attacking with or without a targeter");
         //dissolve the human control link
   	 if (%task == %client.objectiveTask)
   	    aiReleaseHumanControl(%client.controlByHuman, %client);

   	 //see if we didn't acquired a spotter along the way
   	 if (%task.targetterObjective.group > 0)
   	    %targetter = %task.targetterObjective.group.clientLevel[1];
   	 else
   	    %targetter = %task.targetterObjective.clientLevel[1];

   	 if (! %task.targetAcquired && AIClientIsAlive(%targetter) && %targetter.isAIControlled())
   	 {
   	    %client.setTargetObject(-1);
          %task.waitForTargetter = true;
   	 }
   	 else
   	 {
   	    //see if we can find a target near our objective
   	    if (! %task.targetAcquired)
   	    {
   	       %numTargets = ServerTargetSet.getCount();
   	       for (%i = 0; %i < %numTargets; %i++)
   	       {
   		  %targ = ServerTargetSet.getObject(%i);
   		  %targDist = VectorDist(%targ.getWorldBoxcenter(), %task.targetObject.getWorldBoxCenter());
   		  if (%targDist < 20)
   		  {
   		     %task.targetAcquired = true;
   		     break;
   		  }
   	       }
   	       //see if we found a target (must be by a human)
   	       if (%task.targetAcquired && %task.sayAcquired)
   	       {
   	          %task.sayAcquired = false;
   		  AIMessageThread("ChatTargetAcquired", %client, -1);
   	       }
   	    }

   	    if (%objDistance < 30)
   	       %targetMode = "Destroy";
   	    else
   	       %targetMode = "Mortar";
          //set the target object, and keep attacking it
   	    if (%objDistance < 8)
             %client.setDangerLocation(%client.player.getWorldBoxCenter(), 12);
          else {//if (%client.getStepStatus() $= "Finished" || %hasLOS)       {
if ($watch == %client) echo (getTaggedString(%client.name) SPC "Idle:locDistance" SPC %locDistance SPC %client.getStepStatus() SPC %stepDist SPC %objDistance SPC %targetMode);
             %client.stepIdle(%client.player.getWorldBoxCenter());
   	       %client.setTargetObject(%task.targetObject, %objDistance +5, %targetMode);
//    	       %client.aimAt(%task.targetObject, 15000);
           }// else
//    	       %client.setTargetObject(-1);
       }
      }
   }
   else   //the target must have been destroyed  :)
   {
if ($watch == %client) echo (getTaggedString(%client.name) SPC "target must have been destroyed");
      if (%task.targetObject.getDamageState() $= "Destroyed") {
         %clientPos = %client.player.getWorldBoxCenter();
         %targetPos    = %task.targetObject.getWorldBoxCenter();
			if (%task.mode $= "Nuke") %missStr = $MissionName @ " ";
			else %missStr = "";
			if (%task.locationDist) {
            if ($verbose >= 1) logEcho (%missStr @ %client.objective.targetObject SPC "LOS Dist.:" SPC %task.locationDist SPC %task.mode SPC "mortared by" SPC %client.nameBase SPC %task.location);
			} else {
   			if ($verbose >= 1) logEcho (%missStr @ %client.objective.targetObject SPC "Dist.:" SPC vectorDist(%clientPos, %targetPos) SPC %task.mode SPC "mortared by" SPC %client.nameBase SPC %clientPos);
			}
		}
      //dissolve the human control link
      if (%task == %client.objectiveTask)
         aiReleaseHumanControl(%client.controlByHuman, %client);

     if (%hasWidowMaker || %hasMortarStorm) {
       %task.location = %enemyFlagPos;
       %task.targetObject = %enemyFlag;
     } else {
      %client.setTargetObject(-1);
      %client.clearStep();
      %client.stop();

      if (%task.celebrate)
      {
         if (%task.waitTimerMS == 0)
	 {
	    //client animation "woohoo"!  :)
	    //choose the animation range
	    %minCel = 3;
	    %maxCel = 8;

            //pick a random sound
            %sound = $AITauntChat[mFloor(getRandom() * 3.99)];
	    %randTime = mFloor(getRandom() * 500) + 1;
	    schedule(%randTime, %client, "AIPlayAnimSound", %client, %task.targetObject.getWorldBoxCenter(), %sound, %minCel, %maxCel, 0);

	    //team message
	    AIMessageThread("ChatEnemyTurretsDestroyed", %client, -1);

	    //set the timer
            %task.waitTimerMS = getSimTime();
	 }

	 //else see if the celebration period is over
	 else if (getSimTime() - %task.waitTimerMS > 3000)
	    %task.celebrate = false;
      }
      else
      {
         //if this task is the objective task, choose a new objective
	 if (%task == %client.objectiveTask)
	 {
	    AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
         }
      }
    }
   }
//error(getTaggedString(%client.name) SPC "Monitor:End" SPC %objective.targetObject);
}

//---------------------------------------------------------------------------------------------- AI Deploy Equipment ---

function AIDeployEquipment::initFromObjective(%task, %objective, %client)
{
   //initialize the task vars from the objective
   %task.baseWeight = %client.objectiveWeight;
   %task.location = %objective.location;
   %task.issuedByClient = %objective.issuedByClientId;
   %task.chat = %objective.chat;

   %myFlag = $AITeamFlag[%client.team];
   %myFlagDist = vectorDist(%myFlag.originalPosition, %task.location);
   %myFlagYDiff = getWord(%task.location, 2) - getWord(%myFlag.originalPosition, 2);

   if (getRandom() > 1/(1 + (%myFlagDist + 20)/120)) {
      %task.equipment = strReplace(%objective.equipment, "Outdoor", "Laser");
      %task.buyEquipmentSet = strReplace(%objective.buyEquipmentSet, "Outdoor", "Laser");
      %task.desiredEquipment = strReplace(%objective.desiredEquipment, "Outdoor", "Laser");
   } else if (%myFlagYDiff/%myFlagDist - 0.2 > getRandom()){
      %task.equipment = strReplace(%objective.equipment, "Outdoor", "EMP");
      %task.buyEquipmentSet = strReplace(%objective.buyEquipmentSet, "Outdoor", "EMP");
      %task.desiredEquipment = strReplace(%objective.desiredEquipment, "Outdoor", "EMP");
   } else {
      %task.equipment = %objective.equipment;
      %task.buyEquipmentSet = %objective.buyEquipmentSet;
      %task.desiredEquipment = %objective.desiredEquipment;
   }

   //initialize other task vars
   %task.sendMsg = true;
   %task.sendMsgTime = 0;
   %task.useTeleport = true;

   //use the Y-axis of the rotation as the desired direction of deployement,
   //and calculate a walk to point 3 m behind the deploy point.
   %task.deployDirection = MatrixMulVector("0 0 0 " @ getWords(%objective.getTransform(), 3, 6), "0 1 0");
   %task.deployDirection = VectorNormalize(%task.deployDirection);
}

function AIDeployEquipment::assume(%task, %client)
{
   %task.setWeightFreq(15);
   %task.setMonitorFreq(15);

   %client.needEquipment = AINeedEquipment(%task.equipment, %client);

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();

   %task.passes = 0;
   %task.deployAttempts = 0;
   %task.checkObstructed = false;
   %task.waitMove = 0;

   //add some for picking up deployables from ground - Laggg... 2-26-2004
   %task.pickupPack = -1;
   %task.usingInv = false;

   //--------------------------------------------------- assume telporter - start -
   //if the MPB Teleporter is online - Lagg... 9-30-2003
   // ZOD: Less code this way...
//    if(MPBTeleporterCheck(%client, %task))
      %task.useTeleport = true;
//    else
//       %task.useTeleport = false;
   //--------------------------------------------------- assume telporter - end -
}

function AIDeployEquipment::retire(%task, %client)
{
}

function AIDeployEquipment::weight(%task, %client)
{
   //update the task weight
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;

   %task.setWeight(%task.baseWeight);
}

function findTurretDeployPoint(%client, %location, %attempt)
{
   %player = %client.player;
   if (!isObject(%player))
      return "0 0 0";

   %feetPos = posFromTransform(%player.getTransform());
   %temp = VectorSub(%location, %feetPos);
   %temp2 = getWord(%temp, 0) @ " " @ getWord(%temp, 1) @ " 0";
   %facingVector = VectorNormalize(%temp2);
   %aimPoint = VectorAdd(%feetPos, %facingVector);
   //assume that there will be 10 attempts
   %height = getWord(%location, 2) + 1.0 - (0.2 * %attempt);
   %aimAt = getWord(%aimPoint, 0) @ " " @ getWord(%aimPoint, 1) @ " " @ %height;
   return %aimAt;
}

//modified range to deploy to make easier for outdoor inventory deployments - Lagg... - 4-12-2003
function AIDeployEquipment::monitor(%task, %client)
{
   //first, make sure we haven't deployed too many - Lagg
//   if (%task.equipment $= "TurretOutdoorDeployable" || %task.equipment $= "TurretIndoorDeployable")
   if (strstr(%task.equipment, "Turret") > -1 && strstr(%task.equipment, "Deployable") > -1)
      %maxAllowed = countTurretsAllowed(%task.equipment);
   else
      %maxAllowed = $TeamDeployableMax[%task.equipment];

   if($TeamDeployedCount[%client.team, %task.equipment] >= %maxAllowed)
   {
      //if this task is the objective task, choose a new objective
      if (%task == %client.objectiveTask)
      {
         AIUnassignClient(%client);
         Game.AIChooseGameObjective(%client);
         return;
      }
   }

   //make sure this was not deployed already - Lagg..
   InitContainerRadiusSearch(%task.location, $MinDeployableDistance, $TypeMasks::StaticShapeObjectType);
   %found = containerSearchNext();
   if (isObject(%found))
   {
      if (%found.team == %client.team)
      {
         if (%task == %client.objectiveTask)
         {
            AIUnassignClient(%client);
            Game.AIChooseGameObjective(%client);
            return;
         }
      }
   }

   //see if we're supposed to be engaging anyone...
   if (AIClientIsAlive(%client.shouldEngage))
   {
      %hasLOS = %client.hasLOSToClient(%client.shouldEngage);
      %losTime = %client.getClientLOSTime(%client.shouldEngage);
      if (%hasLOS || %losTime < 1000)
         %client.setEngageTarget(%client.shouldEngage);
      else
         %client.setEngageTarget(-1);
   }
   else
      %client.setEngageTarget(-1);

   //see if we are hung up on a force field - Lagg..
   InitContainerRadiusSearch(%client.player.getWorldBoxCenter(), 4, $TypeMasks::ForceFieldObjectType);
   %objSearch = containerSearchNext();
   if (%objSearch > 0)
   {
      %ffDis = %client.getPathDistance(%objSearch.getWorldBoxCenter());
      %myDis = %client.getPathDistance(%task.position);
      if (%ffDis < %myDis )
      {
         if (%objSearch.isPowered() && %objSearch.team != %client.team)
         {
            //if this task is the objective task, choose a new objective
            if (%task == %client.objectiveTask)
            {
               %client.setDangerLocation(%objSearch.getWorldBoxCenter(), 20);
               AIUnassignClient(%client);
               Game.AIChooseGameObjective(%client);
            }
            return;
         }
      }
   }

   //first, see if we still need the deployable pack - Lagg... 2-25-2004
   %itemType = getWord(%task.equipment, 0);
   if (%client.player.getInventory(%itemType) > 0)
   {
      %client.needEquipment = false;
      %task.setMonitorFreq(15);
   }
   //then, buy the equipment or find it if you can :) - Lagg... 2-25-2004
   else if (%client.needEquipment)
   {
      // check to see if there's a deployable pack nearby
      %closestItem = -1;
      %closestItemDist = 32767;
      %itemType = getWord(%task.equipment, 0);

      //search the AIItemSet for a deployable pack (someone might have dropped one...)
      %armor = %client.player.getArmorSize();
      %itemCount = $AIItemSet.getCount();
      for (%i = 0; %i < %itemCount; %i++)
      {
         %item = $AIItemSet.getObject(%i);
         if (%item.getDataBlock().getName() $= %itemType && !%item.isHidden())
         {
            %cant = false;
            if ((%itemType $= "InventoryDeployable" || %itemType $= "TurretOutdoorDeployable" || %itemType $= "TurretIndoorDeployable") && %armor $= "Light")
               %cant = true;

            %dist = %client.getPathDistance(%item.getWorldBoxCenter());
            if (%dist > 0 && %dist < %closestItemDist && !%cant)
            {
               %closestItem = %item;
               %closestItemDist = %dist;
            }
         }
      }
      //choose whether we're picking up the closest pack, or buying from an inv station...
      if ((isObject(%closestItem) && %closestItem != %task.pickupPack) || (%task.buyInvTime != %client.buyInvTime))
      {
         %task.pickupPack = %closestItem;
         //initialize the inv buying
         %task.buyInvTime = getSimTime();
         AIBuyInventory(%client, %task.equipment, %task.buyEquipmentSet, %task.buyInvTime, %task);

         //now decide which is closer
         if (isObject(%closestItem))
         {
            //added a little here to check for power - Lagg... - 3-25-2003
            if (isObject(%client.invToUse) && (%client.invToUse.isEnabled()) && (%client.invToUse.isPowered()))
            {
               %dist = %client.getPathDistance(%client.invToUse.getWorldBoxCenter());
               if (%dist < %closestItemDist)
                  %task.usingInv = true;
               else
                  %task.usingInv = false;
            }
            else
               %task.usingInv = false;
         }
         else
            %task.usingInv = true;
      }
      //now see if we found a closer deployable pack
      if (!%task.usingInv && isObject(%task.pickupPack))
      {
         %client.stepMove(%task.pickupPack.position, 0.25);
         %distToPack = %client.getPathDistance(%task.pickupPack.position);
         if (%distToPack < 10 && %client.player.getMountedImage($BackpackSlot) > 0)
            %client.player.throwPack();

         //and we're finished until we actually have the new pack... we should - Lagg...
         return;
      }
      else
      {
         %task.setMonitorFreq(5);
         if (%task.equipment !$= "")
            %equipmentList = %task.equipment;
         else
            %equipmentList = %task.desiredEquipment;

         %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime, %task);
         if (%result $= "InProgress")
         {
            //force a nervous reaction every 12 sec - Lagg...
            if (getSimTime() - %task.buyInvTime > 12000)
            {
               %client.setDangerLocation(%client.player.getWorldBoxCenter(), 20);
               %task.buyInvTime = getSimTime();
               %client.buyInvTime = %task.buyInvTime;
            }
            return;
         }
         else if (%result $= "Finished")
         {
            %task.setMonitorFreq(30);
            %client.needEquipment = false;
            //if we made it past the inventory buying, reset the inv time
            %task.buyInvTime = getSimTime();
         }
         else if (%result $= "Failed")
         {
            //if this task is the objective task, choose a new objective
            if (%task == %client.objectiveTask)
            {
               AIUnassignClient(%client);
               Game.AIChooseGameObjective(%client);
            }
            return;
         }
      }
   }

   //make sure we still have equipment
   %client.needEquipment = AINeedEquipment(%task.equipment, %client);
   if (%client.needEquipment)
   {
      //if this task is the objective task, choose a new objective
      if (%task == %client.objectiveTask)
      {
         AIUnassignClient(%client);
         Game.AIChooseGameObjective(%client);
         return;
      }
   }

   //--------------------------------------------------- monitor telporter - start -
   // use triumph teleporter if appropriate:
   if (%task.useTeleport) if (AIUseTeleport(%client, %task)) return;
   //--------------------------------------------------- monitor telporter - end -

   //make sure this was not deployed already
   InitContainerRadiusSearch(%task.location, $MinDeployableDistance, $TypeMasks::StaticShapeObjectType);
   %found = containerSearchNext();
   if (isObject(%found))
   {
      if (%found.team == %client.team)
      {
         if (%task == %client.objectiveTask)
         {
            error("MinDeployableDistance: Could not deploy" SPC %client.player.getMountedImage($BackpackSlot).getName() SPC "at" SPC %task.location);
            AIUnassignClient(%client);
            Game.AIChooseGameObjective(%client);
            return;
         }
      }
   }

   //chat
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
         {
            if (%task.chat !$= "")
            {
               %chatMsg = getWord(%task.chat, 0);
               %chatTemplate = getWord(%task.chat, 1);
               if (%chatTemplate !$= "")
                  AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
               else
                  AIMessageThread(%task.chat, %client, -1);
            }
         }
      }
   }


   //calculate the deployFromLocation
   %factor = -1 * (4 - (%task.passes * 0.5));//changed to 4m behind to improve proformance --- Lagg... 3-15-2003
   %task.deployFromLocation = VectorAdd(%task.location,VectorScale(%task.deployDirection, %factor));

   //see if we're within range of the deploy location
   %clLoc = %client.player.position;
   %distance = VectorDist(%clLoc, %task.deployFromLocation);
   %dist2D = VectorDist(%clLoc, getWords(%task.deployFromLocation, 0, 1) SPC getWord(%clLoc, 2));

   //set the aim when we get near the target...  this will be overwritten when we're actually trying to deploy
   if (%distance < 10 && %dist2D < 10)
      %client.aimAt(%task.location, 1000);

   if ((%client.pathDistRemaining(20) > %distance + 0.25) || %dist2D > 0.5)
   {
      %task.deployAttempts = 0;
      %task.checkObstructed = false;
      %task.waitMove = 0;
      %client.stepMove(%task.deployFromLocation, 0.25, $AIModeExpress);
      %task.setMonitorFreq(15);
      return;
   }

   if (%task.deployAttempts < 10 && %task.passes < 5 && !AIClientIsAlive(%client.getEngageTarget()))
   {
      //dissolve the human control link
      if (%task == %client.objectiveTask)
         aiReleaseHumanControl(%client.controlByHuman, %client);

      %task.setMonitorFreq(3);
      %client.stop();
      if (%task.deployAttempts == 0)
         %deployPoint = %task.location;
      else
         %deployPoint = findTurretDeployPoint(%client, %task.location, %task.deployAttempts);

      if(%deployPoint !$= "")
      {
         // we have possible point
         %task.deployAttempts++;
         %client.aimAt(%deployPoint, 2000);

         //try to deploy the backpack
         %client.deployPack = true;
         %client.lastDeployedObject = -1;
         %client.player.use(Backpack);

         // check if pack deployed
         if (isObject(%client.lastDeployedObject))
         {
            //see if there's a "repairObject" objective for the newly deployed thingy...
            if (%task == %client.objectiveTask)
            {
               %deployedObject = %client.lastDeployedObject;

               //search the current objective group and search for a "repair Object" task...
               %objective = %client.objective;

               //delete any previously associated "AIORepairObject" objective
               if (isObject(%objective.repairObjective))
               {
                  clearObjectiveFromTable(%objective.repairObjective); // New function - Lagg...
                  AIClearObjective(%objective.repairObjective);
                  %objective.repairObjective.delete();
                  %objective.repairObjective = "";
               }

               //add the repair objective
               %objective.repairObjective = new AIObjective(AIORepairObject)
               {
                  dataBlock = "AIObjectiveMarker";
                  weightLevel1 = %objective.weightLevel1 - 150;
                  weightLevel2 = 0;
                  description = "Repair the " @ %deployedObject.getDataBlock().getName();
                  targetObjectId = %deployedObject;
                  issuedByClientId = %client;
                  offense = false;
                  defense = true;
                  equipment = "RepairPack || RepairGun2";
               };
               %objective.repairObjective.deployed = true;
               %objective.repairObjective.setTransform(%objective.getTransform());
               %objective.repairObjective.group = %objective.group;
               MissionCleanup.add(%objective.repairObjective);

               //if is siege game we have to do this right - Lagg... 11-3-2003
               if (Game.class $= "SiegeGame")
               {
                  //the objectives in $ObjectiveQ[1] are all offense objectives, $ObjectiveQ[2] has the defensive ones..
                  if (%client.team == game.offenseTeam)
                     $ObjectiveQ[1].add(%objective.repairObjective);
                  else
                     $ObjectiveQ[2].add(%objective.repairObjective);
               }
               else
                  $ObjectiveQ[%client.team].add(%objective.repairObjective);

               //changed to fix Siege gameType wrong Q error - Lagg... 11-5-2003
               //MissionCleanup.add(%objective.repairObjective);
               //$ObjectiveQ[%client.team].add(%objective.repairObjective);

               //finally, unassign the client so he'll go do something else...
               AIUnassignClient(%client);
               Game.AIChooseGameObjective(%client);
            }
            //finished
            return;
         }
      }
   }
   else if (!%task.checkObstructed)
   {
      %task.checkObstructed = true;

      //see if anything is in our way
      InitContainerRadiusSearch(%task.location, 4, $TypeMasks::MoveableObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::PlayerObjectType);
      %objSrch = containerSearchNext();
      if (%objSrch == %client.player)
         %objSrch = containerSearchNext();
      if (%objSrch)
         AIMessageThread("ChatMove", %client, -1);
   }
   else if (%task.waitMove < 5 && %task.passes < 5)
   {
      %task.waitMove++;

      //try another pass at deploying
      if (%task.waitMove == 5)
      {
         %task.waitMove = 0;
         %task.passes++;
         %task.deployAttempts = 0;

         //see if we're *right* underneath the deploy point
         %deployDist2D = VectorDist(getWords(%client.player.position, 0, 1) @ "0", getWords(%task.location, 0, 1) @ "0");
         if (%deployDist2D < 0.25)
         {
            %client.pressjump();
            %client.pressJet(); // Added - Lagg..
            %client.deployPack = true;
            %client.player.use(Backpack);

            // check if pack deployed
            if(%client.player.getMountedImage($BackpackSlot) == 0)
            {
               //don't add a "repairObject" objective for ceiling turrets
               if (%task == %client.objectiveTask)
               {
                  AIUnassignClient(%client);
                  Game.AIChooseGameObjective(%client);
               }
            }
         }
      }
   }
   else
   {
      //find a new assignment - and remove this one from the Queue
      if (%task == %client.objectiveTask)
      {
//         error(%client.nameBase SPC "from team" SPC %client.team SPC "is invalidating objective:" SPC %client.objective.description SPC "UNABLE TO DEPLOY EQUIPMENT");
         //error("client = " @ getTaggedString(%client.name));
         %client.objective.isInvalid = true;
         AIUnassignClient(%client);
         Game.AIChooseGameObjective(%client);
      }
   }
}

//--------------------------------------------------------------------------------------------------------------------------
//AI Objective functions

function ClientHasAffinity(%objective, %client)
{
   if (%objective.offense && %client.offense)
      return true;
   else if (%objective.defense && !%client.offense)
      return true;
   else
      return false;
}

function ClientHasRequiredEquipment(%objective, %client)
{
   return true;
}

function AIODefault::weight(%objective, %client, %level, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   //check equipment requirement
   if (isObject(%objective.clientLevel1)) {  // there's already some bot on this task
     %needEquipment = AINeedEquipment(%objective.clientLevel1.getTaskID().equipment, %client);
     %desireEquipment = AINeedEquipment(%objective.clientLevel1.getTaskID().desiredEquipment, %client);
   }
   else {
     %needEquipment = AINeedEquipment(%objective.equipment, %client);
     %desireEquipment = AINeedEquipment(%objective.desiredEquipment, %client);
   }
   //set the base weight
   switch (%level)
   {
      case 1:
         %weight = %objective.weightLevel1;
      case 2:
         %weight = %objective.weightLevel2;
         if (isObject(%objective.clientLevel2)) {  // there's already some bot on this task
           %needEquipment = AINeedEquipment(%objective.clientLevel2.getTaskID().equipment, %client);
           %desireEquipment = AINeedEquipment(%objective.clientLevel2.getTaskID().desiredEquipment, %client);
         }
      case 3:
         %weight = %objective.weightLevel3;
         if (isObject(%objective.clientLevel3)) {  // there's already some bot on this task
           %needEquipment = AINeedEquipment(%objective.clientLevel3.getTaskID().equipment, %client);
           %desireEquipment = AINeedEquipment(%objective.clientLevel3.getTaskID().desiredEquipment, %client);
         }
      default:
         %weight = %objective.weightLevel4;
         if (isObject(%objective.clientLevel4)) {  // there's already some bot on this task
           %needEquipment = AINeedEquipment(%objective.clientLevel4.getTaskID().equipment, %client);
           %desireEquipment = AINeedEquipment(%objective.clientLevel4.getTaskID().desiredEquipment, %client);
         }
   }

   //check Affinity
   if (ClientHasAffinity(%objective, %client))
      %weight += 40;

//**********************************************************************************

//    //if the objective doesn't require any equipment, it automatically get's the +100...
//    if (%objective.equipment $= "" && %objective.desiredEquipment $= "")
//       %weight += 100;
//    else
//    {
if ($watch == %objective) echo(%client.nameBase SPC "Need Eq:" SPC %needEquipment SPC "Equipm:" SPC %objective.clientLevel1.getTaskID().equipment SPC "Init. Weight:" SPC %weight);

   //figure out the percentage of desired equipment the bot has
   if (%desireEquipment) %weight -= 50;

// This block does not fit into the new equipment algorithm anymore:   - pinkpanther
//       else if (%objective.desiredEquipment !$= "")
//       {
//          %count = getWordCount(%objective.desiredEquipment);
//          %itemCount = 0;
//          for (%i = 0; %i < %count; %i++)
//          {
//             %item = getWord(%objective.desiredEquipment, %i);
//             if (!AINeedEquipment(%item, %client))
//                %itemCount++;
//          }
//          //add to the weight
//          %weight += mFloor((%itemCount / %count) * 75);
//       }
//   }

   //find the distance to target
   if (%objective.targetClientId !$= "" || %objective.targetObjectId !$= "")
   {
      if (AIClientIsAlive(%objective.targetClientId))
      {
         %targetPos = %objective.targetClientId.player.getWorldBoxCenter();
      }
      else if (VectorDist(%objective.location, "0 0 0") > 1)
         %targetPos = %objective.location;
      else
      {
         if(%objective.targetObjectId > 0)
            %targetPos = %objective.targetObjectId.getWorldBoxCenter();
      }
   }

//error(getTaggedString(%client.name) SPC "AIODefault::weight"SPC %weight);

   //make sure the destination is accessible
   %distance = %client.getPathDistance(%targetPos);
   if (%distance < 0)
      return 0;

   %closestInvIsRemote = (getWordCount(%inventoryStr) == 4);
   %closestInv = getWord(%inventoryStr, 0);
   %closestDist = getWord(%inventoryStr, 1);
   %closestRemoteInv = getWord(%inventoryStr, 2);
   %closestRemoteDist = getWord(%inventoryStr, 3);

   //if we need equipment, the distance is from the client, to an inv, then to the target
   if (%needEquipment)
   {
      %needArmor = AIMustUseRegularInvStation(%objective.equipment, %client);// - added here to fix below - Lagg... 3-10-2004

      // Scale distance-to-inv weight according to armor speed:   - pinkpanther
      %armorFactor = AISpeedFactor(%client.player);

     //if we need a regular inventory station, and one doesn't exist, exit
      if (!isObject(%closestInv) && %needArmor) // - we got an error here - %needArmor = "" (lets just review/redo the whole function) * fixed for now - Lagg... ***
         return 0;

      //find the closest inv based on whether we require armor (from a regular inv station)
      if (!%closestInvIsRemote)
      {
         %needArmor = false;
         %weightDist = %closestDist;
         %weightInv = %closestInv;
      }
      else
      {
         //%needArmor = AIMustUseRegularInvStation(%objective.equipment, %client); // - removed here moved up ^ - Lagg... 3-10-2004
         if (%needArmor)
         {
            %weightDist = %closestDist;
            %weightInv = %closestInv;
         }
         else
         {
            %weightDist = %closestRemoteDist;
            %weightInv = %closestRemoteInv;
         }
      }

      //tested this next check to be true, now need to ad a search for items in each aiObjective :) - Lagg... 1-20-2004
      //if we don't need armor, and there's no inventory station, see if the equipment we need

      // ------------------------------------------------------------- have to modify to see what is closer item or inventory *

      //is something we can pick up off the ground (likely this would be a repair pack...)
      //updated AIODeployEquipment to consider picking up droped packs :) - Lagg... 3-10-2004

      //echo(%client.nameBase SPC "AIODefault::weight - %weightDist = " @ %weightDist);
      if (%weightDist >= 32767)
      {
         %itemType = getWord(%objective.equipment, 0);
         %found = false;
         %itemCount = $AIItemSet.getCount();
         for (%i = 0; %i < %itemCount; %i++)
         {
            %item = $AIItemSet.getObject(%i);
            //if(isObject(%item)) // ZOD: Console spam fix
            //{
               if (%item.getDataBlock().getName() $= %itemType && !%item.isHidden())
               {
                  %weightDist = %client.getPathDistance(%item.getWorldBoxCenter());
                  if (%weightDist > 0)
                  {
                     %weightInv = %item;  //set the var so the distance function will work...
                     %found = true;
                     break;
                  }
               }
            //}
         }
         if (! %found)
            return 0;
      }

      //now find the distance used for weighting the objective
      %inv2posDist = AIGetPathDistance(%targetPos, %weightInv.getWorldBoxCenter());
      if (%inv2posDist < 0)
         %inv2posDist = 32767;
      if (%closestInvIsRemote)
         %inv2posDist *= %armorFactor;

      //Subtract penalty for having to get inv first (missing "Required equipment"):
      %weight -= 50 + mFloor((%armorFactor * %weightDist + %inv2posDist - %distance)/8);
      if ($watch == %objective) echo(%client.nameBase SPC "Recalc. Weight:" SPC %weight SPC "Penalty:" SPC mFloor((%armorFactor * %weightDist + %inv2posDist - %distance)/8) SPC %needEquipment);

      %distance = %weightDist * %armorFactor + %inv2posDist;
   }

   // -------------------------------------------------- lets give him some personallity on this one :) - Lagg... 2-13-2004
   if (%objective.targetObjectId > 0 && !%needEquipment)
   {
      if (VectorDist(%client.player.getWorldBoxCenter(), %targetPos) < 150)
      {

         //check for LOS
         %hasLOS = !containerRayCast(%client.player.getMuzzlePoint($WeaponSlot), %objective.targetObjectId.getWorldBoxCenter(), $LOSMask, 0);

         if (%hasLOS)
         {
            //echo("AIODefault::weight - We Got LOS - " @ getTaggedString(%client.name));
            %weight += 120;
         }
         else {
            // extreme close range should get a weight increase up to 100:
            %weight += mFloor(1000000/(100 + %distance)/(100 + %distance));
         }
      }
   }

//    //see if we're within 200 m
//    if (%distance < 200)
//       %weight += 10;
//
//    //see if we're within 90 m
//    if (%distance < 90)
//       %weight += 30;
//
//    //see if we're within 45 m
// //    if (%distance < 45)
// //       %weight += 30;
//
//    //see if we're within 20 m
//    if (%distance < 20)
//       %weight += 30;
//
//    //if we get bonus for being close we should get penalty if far - Lagg... 2-15-2004
//    //see if we're passed 450 m
//    if (%distance > 450)
//       %weight -= 50;
//
//    //see if we're pass 900 m
//    if (%distance > 900)
//       %weight -= 70;
//
//    //see if we're pass 1350 m
//    if (%distance > 1350)
//       %weight -= 40;
//
//    //see if we're pass 1600 m
//    if (%distance > 1600)
//       %weight -= 40;

   // New formula - pinkpanther: Penalty for distance and damage
   %weight += 200 - mFloor(%distance/4 - %client.player.getDamagePercent() *50);

if ($watch == %objective) echo(%client.nameBase @ " Objective: " @ %objective SPC %objective.description SPC " Distance:" SPC %distance SPC "Final Weight:" SPC %weight SPC %needEquipment);

   //final return, since we've made it through all the rest
   return %weight;
}

function AIODefault::QuickWeight(%objective, %client, %level, %minWeight)
{
   //can't do a quick weight when re-evaluating a client's current objective
   if (%client.objective == %objective)
      return true;

   //do a quick check to disqualify this objective if it can't meet the minimum weight
   switch (%level)
   {
      case 1:
         %testWeight = %objective.weightLevel1;
      case 2:
         %testWeight = %objective.weightLevel2;
      case 3:
         %testWeight = %objective.weightLevel3;
      default:
         %testWeight = %objective.weightLevel4;
   }
   if (%testWeight + 260 < %minWeight)
      return false;
   else
      return true;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ AIO Defend Location +++
//modified so if set to offensive the bot will defend a destroyed enemy object (can you say Camping in Base ?) - Lagg... 1/7/2004
//must be placed in the enemy teams objective simgroup and DO NOT set defense = 1
//for defense: it must be set to defense = 1, for Raping enemy: must not have defense > 0 in the .mis file  - * IMPORTANT * -
//also added if we are a defending sniper than defend like a sniper :) - Lagg... 3-25-2004

function AIODefendLocation::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   // if were playing CnH, check who owns this
   if (%this.targetObjectId > 0 && %this.defense > 0)
   {
      //error("THIS: " @ %this.getName() @ " ID: " @ %this.targetObjectId.name);
      if(%this.targetObjectId.name !$= "zone") // ZOD: Console Spam fix for KOTH game
         if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden() || %this.targetObjectId.team != %client.team)
	      return 0;
   }
   else if (%this.targetObjectId > 0  && %this.offense > 0)
   {
      if(%this.targetObjectId.name !$= "zone") // ZOD: Console Spam fix for KOTH game
         if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden() || %this.targetObjectId.team == %client.team)
	      return 0;

      //don't camp unless base is down
      if (%this.targetObjectId.getGroup().powerCount > 0)
         return 0;
   }

   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   //do a quick check to disqualify this objective if it can't meet the minimum weight
   if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
   {
      if (%this.targetObjectId > 0 && %this.issuedByClientId == %client.controlByHuman)
      {
         if ($AIWeightHumanIssuedCommand < %minWeight)
            return 0;
      }
      else
         return 0;
   }

   %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);

   //if defensive the object has been destroyed, reduce the weight / if offensive increase the weight if destroyed (Camping !)
   if (%this.targetObjectId > 0 && %this.defense > 0)
   {
      //see if we were forced on the objective
      if (%this.issuedByClientId == %client.controlByHuman && %weight < $AIWeightHumanIssuedCommand)
         %weight = $AIWeightHumanIssuedCommand;
	else if(%this.targetObjectId.name !$= "zone") // ZOD: Console Spam fix for KOTH game
      {
         //else see if the object has been destroyed - slight modification here - Lagg... 11-7-2003
	   //else if (!isObject(%this.targetObjectId) || %this.targetObjectId.getDamageState() $= "Destroyed")
         if (!isObject(%this.targetObjectId) || %this.targetObjectId.isDisabled())
            %weight -= 320;
      }
   }
   else if (%this.targetObjectId > 0 && %this.offense > 0)
   {
      if (%this.issuedByClientId == %client.controlByHuman && %weight < $AIWeightHumanIssuedCommand)
         %weight = $AIWeightHumanIssuedCommand;
	else if(%this.targetObjectId.name !$= "zone") // ZOD: Console Spam fix for KOTH game
      {
         // - slight modification here - Lagg... 11-7-2003
         if (!isObject(%this.targetObjectId) || %this.targetObjectId.getDamageState() !$= "Destroyed")
            return 0;

         if (%client.player.getInventory("InventoryDeployable") > 0)
            return 0;
      }
   }
   return %weight;
}

function AIODefendLocation::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIDefendLocation);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIODefendLocation::unassignClient(%this, %client)
{
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ AIO Attack Location +++

function AIOAttackLocation::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   //now, if this bot is linked to a human who has issued this command, up the weight
   if (%this.issuedByClientId == %client.controlByHuman)
   {
      //make sure we have the potential to reach the minWeight
      if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
      {
         if ($AIWeightHumanIssuedCommand < %minWeight)
            return 0;
         else
            %weight = $AIWeightHumanIssuedCommand;
      }
      else
      {
         // calculate the default...
         %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
         if (%weight < $AIWeightHumanIssuedCommand)
            %weight = $AIWeightHumanIssuedCommand;
      }
   }
   else
   {
      //make sure we have the potential to reach the minWeight
      if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
         return 0;

      // calculate the default...
      %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
   }
   return %weight;
}

function AIOAttackLocation::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIAttackLocation);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIOAttackLocation::unassignClient(%this, %client)
{
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ AIO Touch Object +++

function AIOTouchObject::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   // Force Field Avoidance - Lagg..
//    InitContainerRadiusSearch(%client.player.getWorldBoxCenter(), 4, $TypeMasks::ForceFieldObjectType);
//    %objSearch = containerSearchNext();
//    if (%objSearch > 0)
//    {
//       %ffDis = %client.getPathDistance(%objSearch.getWorldBoxCenter());
//       %myDis = %client.getPathDistance(%this.position);
//       if (%ffDis < %myDis)
//       {
//          if (%objSearch.isPowered() && %objSearch.team != %client.team)
//             return 0;
//       }
//    }

   if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
      return 0;

   switch$ (%this.mode)
   {
      case "TouchFlipFlop":
         //added a check for siege games to enhance the ai, fix navGraph bug with forcefields - Lagg...
         if ($CurrentMissionType $= "Siege")
         {
            InitContainerRadiusSearch(%this.location, 12, $TypeMasks::ForceFieldObjectType);
            %objSearch = containerSearchNext();
            if (%objSearch > 0 && %objSearch.isPowered() && %objSearch.team != %client.team)
               return 0;
            else
	         return AIODefault::weight(%this, %client, %level, %inventoryStr);
         }
         // ZOD: Added isObject check. Console spam fix.
         if(!isObject(%this.targetObjectId) || %this.targetObjectId.team == %client.team || %this.targetObjectId.isHidden())
            return 0;
         else
            return AIODefault::weight(%this, %client, %level, %inventoryStr);

      case "FlagGrab":
         if (! %this.targetObjectId.isHome)
            return 0;
         else {
            %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
            // Certain armors have better odds at capping the flag:
            switch$(%client.player.getArmorSize()) {
               case "Heavy"    : %weight += 20;
               case "Medium"   : %weight += 80;
               case "FieldTech": %weight += 100;
               case "Light"    : %weight += 70;
               case "Sniper"   : %weight += 60;
               case "Recon"    : %weight += 50;
//               default         : ;     // Titan gets no uplift
            }
         return %weight;
         }

      case "FlagDropped":
         if ((%this.targetObjectId.isHome) || (%this.targetObjectId.carrier !$= "")) {
            return 0;
         }
         else {
            switch (%level)
            {
               case 1:
                  %weight = %this.weightLevel1;
               case 2:
                  %weight = %this.weightLevel2;
               case 3:
                  %weight = %this.weightLevel3;
               default:
                  %weight = %this.weightLevel4;
            }
            // in choosing a client for reaching a dropped flag, distance counts most! - pinkpanther
            %distance = %client.getPathDistance(%this.targetObjectId.getWorldBoxCenter()) * AISpeedFactor(%client.player);    // distance correction by armor speed
            %weight += mFloor(1960000/(70+%distance)/(70+%distance) - 40);
            if ($watch $= "flag") echo(%client.team SPC %client.nameBase SPC "AIOTouchObject: FlagDropped Weight" SPC %weight);
            return %weight;
//            return AIODefault::weight(%this, %client, %level, %inventoryStr);
         }

      case "FlagCapture":
         if (%this.targetObjectId.carrier != %client.player)
            return 0;
         else
         {
            //find our home flag location
            %homeTeam = %client.team;
            %homeFlag = $AITeamFlag[%homeTeam];
            %this.location = %homeFlag.originalPosition;
            return AIODefault::weight(%this, %client, %level, %inventoryStr);
         }
   }
   return 0;
}

function AIOTouchObject::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AITouchObject);
   %client.objectiveTask.initFromObjective(%this, %client);

   //create an AIOEscortPlayer objective to help out, if required
   if (%this.mode $= "FlagGrab")
   {
      %client.escort = new AIObjective(AIOEscortPlayer)
      {
         dataBlock = "AIObjectiveMarker";
         weightLevel1 = $AIWeightEscortOffense[1];
         weightLevel2 = $AIWeightEscortOffense[2];
         description = "Escort flag grabber " @ getTaggedString(%client.name);
         targetClientId = %client;
         offense = true;
         equipment = "SensorJammerPack Shotgun || SensorJammerPack FieldTech || AntiEnergyPack Shotgun";
         desiredEquipment = "SensorJammerPack RepairKit || AntiEnergyPack RepairKit";
         buyEquipmentSet = "LightSensorJammer ReconShotgun FieldTechSensorJammer";
      };
      MissionCleanup.add(%client.escort);
      $ObjectiveQ[%client.team].add(%client.escort);

//       %client.escort1 = new AIObjective(AIOAttackLocation) {
//          position = %this.targetObjectId.position;
//          dataBlock = "AIObjectiveMarker";
//          description = "EscortOffense AIOAttackLocation";
//          lockCount = "0";
//          homingCount = "0";
//          targetClientId = "-1";
//          targetObjectId = "-1";
//          location = %this.targetObjectId.position;
//          weightLevel1 = $AIWeightEscortOffense[2];
//          weightLevel2 = "0";
//          weightLevel3 = "0";
//          weightLevel4 = "0";
//          offense = "1";
//          defense = "0";
//          equipment = "Shotgun ShotgunAmmo";
//          buyEquipmentSet = "ReconShotgun LightSensorJammer MediumEnergySet";
//          chat = "ChatCmdAttackFlag";
//          issuedByHuman = "0";
//          issuedByClientId = "-1";
//          forceClientId = "-1";
//       };
//       MissionCleanup.add(%client.escort1);
//       $ObjectiveQ[%client.team].add(%client.escort1);
   }
   else if (%this.mode $= "FlagCapture")
   {
      %client.escort = new AIObjective(AIOEscortPlayer)
      {
         dataBlock = "AIObjectiveMarker";
         weightLevel1 = $AIWeightEscortCapper[1];
         weightLevel2 = $AIWeightEscortCapper[2];
         description = "Escort the flag carrier " @ getTaggedString(%client.name);
         targetClientId = %client;
         offense = true;
         equipment = "RepairGun2 || Medium || Light || Sniper";
         desiredEquipment = "FlareGrenade";
         buyEquipmentSet = "FieldTechEnergySet MediumEnergyELF LightEnergyDefault";
         chat = "ChatCmdDefendCarrier";
      };
      MissionCleanup.add(%client.escort);
      $ObjectiveQ[%client.team].add(%client.escort);
   }
}

function AIOTouchObject::unassignClient(%this, %client)
{
   //kill the escort objective
   if (%client.escort)
   {
      clearObjectiveFromTable(%client.escort); // New function - Lagg...
      AIClearObjective(%client.escort);
      %client.escort.delete();
      %client.escort = "";
   }
   if (%client.escort1)
   {
      clearObjectiveFromTable(%client.escort1); // New function - Lagg...
      AIClearObjective(%client.escort1);
      %client.escort1.delete();
      %client.escort1 = "";
   }

   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ AIO Attack Player +++

function AIOAttackPlayer::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (!AIClientIsAlive(%client))
      return 0;

   //if we're attacking the flag carrier, make sure a flag carrier exists
   if (%this.mode $= "FlagCarrier")
   {
      if (%this.targetObjectId.carrier $= "")
         return 0;
      else
         %this.targetClientId = %this.targetObjectId.carrier.client;
   }

   //now, if this bot is linked to a human who has issued this command, up the weight
   if (%this.issuedByClientId == %client.controlByHuman)
   {
      //make sure we have the potential to reach the minWeight
      if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
      {
         if ($AIWeightHumanIssuedCommand < %minWeight)
            return 0;
         else
            %weight = $AIWeightHumanIssuedCommand;
      }
      else
      {
         // calculate the default...
         %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
         if (%weight < $AIWeightHumanIssuedCommand)
            %weight = $AIWeightHumanIssuedCommand;
      }
   }
   else
   {
      //make sure we have the potential to reach the minWeight
      if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
         return 0;

      // calculate the default...
      %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
   }
   return %weight;
}

function AIOAttackPlayer::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIAttackPlayer);
   %client.objectiveTask.initFromObjective(%this, %client);

   //create an escort objective (require an offensive player...)
   %client.escort = new AIObjective(AIOEscortPlayer)
   {
      dataBlock = "AIObjectiveMarker";
      weightLevel1 = $AIWeightKillFlagCarrier[2];
      weightLevel2 = $AIWeightEscortOffense[2];
      description = "Escort " @ getTaggedString(%client.name) SPC "to kill the enemy flag carrier";
      targetClientId = %client;
      offense = true;
      desiredEquipment = "SlugRifle";
      equipment = "FieldTech || Medium || Light || Sniper";
      buyEquipmentSet = "LightEnergySniper LightSensorJammer ReconShotgun MediumSensorJammerTriumph";
   };
   MissionCleanup.add(%client.escort);

   //if is siege game we have to do this right - Lagg... 11-3-2003
   if (Game.class $= "SiegeGame")
   {
      //the objectives in $ObjectiveQ[1] are all offense objectives, $ObjectiveQ[2] has the defensive ones..
      if (%client.team == game.offenseTeam)
         $ObjectiveQ[1].add(%client.escort);
      else
         $ObjectiveQ[2].add(%client.escort);
   }
   else
      $ObjectiveQ[%client.team].add(%client.escort);
}

function AIOAttackPlayer::unassignClient(%this, %client)
{
   //kill the escort objective
   if (%client.escort)
   {
      clearObjectiveFromTable(%client.escort);
      AIClearObjective(%client.escort);
      %client.escort.delete();
      %client.escort = "";
   }

   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ AIO Escort Player +++
//modified escorter armor type allowed, rewritten all aivehicle escort stuff, included vehicleobjecttypes
//as potential targets if lazed by escortie.                                              - Lagg... - 4-12-2003

function AIOEscortPlayer::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client) || ! AIClientIsAlive(%this.targetClientId))
      return 0;

   //can't escort yourself
   if (%client == %this.targetClientId)
      return 0;

   //if vehicle mounted escort check for empty seats - Lagg... - 5-17-2003
   %targMounted = %this.targetClientId.player.ismounted();
   if (%targMounted > 0)
   {
      //%this.forceClientId = %client;//this should stop em getting bumped
      %vehicle = %this.targetClientId.vehicleMounted;
      %node = findAIEmptySeat(%vehicle, %client.player);

      if (%node < 0 && %client.vehicleMounted != %this.targetClientId.vehicleMounted)
         return 0;
   }

   //make sure the class is appropriate unless we are in a vehicle :) - Lagg...
   if (%this.forceClientId <= 0 && %this.issuedByClientId != %client.controlByHuman)
   {
      %targArmor = %this.targetClientId.player.getArmorSize();
      %myArmor = %client.player.getArmorSize();
      %targMounted = %this.targetClientId.player.ismounted();

      //if ((%targArmor $= "Light" && %myArmor !$= "Light" && !%targMounted)
        //|| (%targArmor $= "Medium" && %myArmor $= "Heavy" && !%targMounted))
	      //return 0;

      //changed here to allow bigger escorts - Lagg... 1-31-2003
      if (%targArmor $= "Light" && %myArmor $= "Heavy" && !%targMounted)
	      return 0;
   }

   //can't bump a forced client from level 1
   if (%this.forceClientId > 0 && %this.forceClientId != %client && %level == 1)
      return 0;

   //lets check the range on this one
   if (vectorDist(%client.Player.getWorldBoxCenter(), %this.targetClientId.player.getWorldBoxCenter()) > 300)
      return 0;

   //if this bot is linked to a human who has issued this command, up the weight
   if (%this.issuedByClientId == %client.controlByHuman)
   {
      //make sure we have the potential to reach the minWeight
      if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
      {
         if ($AIWeightHumanIssuedEscort < %minWeight)
            return 0;
         else
            %weight = $AIWeightHumanIssuedEscort;
      }
      else
      {
         // calculate the default...
         %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
         if (%weight < $AIWeightHumanIssuedEscort)
            %weight = $AIWeightHumanIssuedEscort;
      }
   }
   else
   {
      //make sure we have the potential to reach the minWeight
      if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
         return 0;

      // calculate the default...
      %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
   }
   return %weight;
}

function AIOEscortPlayer::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIEscortPlayer);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIOEscortPlayer::unassignClient(%this, %client)
{
   //kill the escort objective
   if (%client.escort)
   {
      AIClearObjective(%client.escort);
      %client.escort.delete();
      %client.escort = "";
   }
   %client.repairObject = -1;
   %client.setTargetObject(-1);
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ AIO Attack Object +++
//totally rewritten so but will RAPE the object. - Lagg... 11-1-2003
// ZOD: Changed it back but is still heavily modified
function AIOAttackObject::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   // if were playing CnH, check who owns this
   if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden() || %this.targetObjectId.team == %client.team)
      return 0;

   //don't attack team 0 stuffs - Lagg..
   if (%this.targetObjectId.team <= 0)
      return 0;

   //make sure the player is still alive!!!!!
   if (!AIClientIsAlive(%client))
      return 0;

   // Force Field Avoidance - Lagg..
//    InitContainerRadiusSearch(%client.player.getWorldBoxCenter(), 8, $TypeMasks::ForceFieldObjectType);
//    %objSearch = containerSearchNext();
//    if (%objSearch > 0)
//    {
//       %ffDis = %client.getPathDistance(%objSearch.getWorldBoxCenter());
//       %myDis = %client.getPathDistance(%this.position);
//       if (%ffDis < %myDis)
//       {
//          if (%objSearch.isPowered() && %objSearch.team != %client.team)
//             return 0;
//       }
//    }

   //no need to attack if the object is already destroyed
   if (!isObject(%this.targetObjectId) || %this.targetObjectId.getDamageState() $= "Destroyed")
      return 0;
   else
   {
      //if this bot is linked to a human who has issued this command, up the weight
      if (%this.issuedByClientId == %client.controlByHuman)
      {
         //make sure we have the potential to reach the minWeight
         if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
         {
            if ($AIWeightHumanIssuedCommand < %minWeight)
               return 0;
            else
               %weight = $AIWeightHumanIssuedCommand;
         }
         else
         {
            // calculate the default...
            %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
            if (%weight < $AIWeightHumanIssuedCommand)
               %weight = $AIWeightHumanIssuedCommand;
         }
      }
      else
      {
         //make sure we have the potential to reach the minWeight
         if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
            return 0;

         // calculate the default...
         %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
      }
      // Fix for too low weight on this objective in most maps:    pinkpanther
      if (%this.description $= "Attack the SentryTurret") {
         %weight += 120;
      }

      //lets give him some personallity on this one :) - Lagg...
      if ((findTargetInView(%client, 150)) == %this.targetObjectId)
         %weight += 200;

      return %weight;
   }
}

function AIOAttackObject::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIAttackObject);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIOAttackObject::unassignClient(%this, %client)
{
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ AIO Repair Object +++
//if you have a problem with bot getting stuck navigating to repair any object,
//place objective marker in spot you want bot to stand for repair at about bots waist level.
//if there is no problem with bot getting stuck repairing this object set the AIORepairObject
//markers transform to the same exact transform as the Object (x, y, z) updated - 10-18-2003

//fixed spam error when trying to repair a deployed object in case it gets destroyed while
//in loop. also fixed bots finding repair pack if base is down :) ----------- Lagg... 3-26-2003

function AIORepairObject::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   // if were playing CnH, check who owns this - fixed because it spammed like crazy - ZOD
   if (!isObject(%this.targetObjectId) || %this.targetObjectId.team != %client.team)
      return 0;

   // Fix for above spam by moving check
   if(Game.class $= "CnHGame" || Game.class $= "ConquestGame" || Game.class $= "DomGame" && %this.targetObjectId.isHidden())
      return 0;

   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   if (!isObject(%this.targetObjectId) || %this.targetObjectId.getDamagePercent() <= 0)
      return 0;
   else
   {
      //if this bot is linked to a human who has issued this command, up the weight
      if (%this.issuedByClientId == %client.controlByHuman)
      {
         //make sure we have the potential to reach the minWeight
         if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
         {
            if ($AIWeightHumanIssuedCommand < %minWeight)
               return 0;
            else
               %weight = $AIWeightHumanIssuedCommand;
         }
         else
         {
            // calculate the default...
            %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
            if (%weight < $AIWeightHumanIssuedCommand)
               %weight = $AIWeightHumanIssuedCommand;
         }
      }
      else
      {
         //make sure we have the potential to reach the minWeight
         if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
            return 0;

         if (%level == 1) if (strstr(%this.targetObject,"StationInventory")>=0) if (getWord(AIFindClosestInventory(%client, true), 0) < 0) {
            %this.weightLevel1 = $AIWeightRepairInventory[1] + 700;
//      error(%client.nameBase SPC "AIODeploy: StationInventory" SPC %this.weightLevel1);
         }

         // calculate the default...
         %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
//      echo(%client.nameBase SPC "AIODeploy: StationInventory" SPC %weight);
      }
      //lets give him some personallity on this one :) - Lagg...
      //if ((findTargetInView(%client, 100)) == %this.targetObjectId)
      //   %weight += 200;

      return %weight;
   }
}

function AIORepairObject::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIRepairObject);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIORepairObject::unassignClient(%this, %client)
{
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ AIO Laze Object +++
//fixed here so bots will acually use the targetting lazer for the right team :) - Lagg... - 4-12-2003
//modified last laze time and distance to client we laze for.

function AIOLazeObject::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   //if we are in water forget it
   if (%client.inWater)
      return 0;

   //not if we are in the base
   if (%client.inBase)
      return 0;

   //lets check the range on this one
   if (vectorDist(%client.Player.getWorldBoxCenter(), %this.getWorldBoxCenter()) < 100)
      return 0;

   //see if it's already being lazed
   %numTargets = ServerTargetSet.getCount();
   for (%i = 0; %i < %numTargets; %i++)
   {
      %targ = ServerTargetSet.getObject(%i);
      if (%targ.sourceObject != %client.player)
      {
         %targDist = VectorDist(%targ.getWorldBoxCenter(), %this.targetObjectId.getWorldBoxCenter());
         if (%targDist < 100)
         {
            %this.lastLazedTime = getSimTime();
            %this.lastLazedClient = %targ.sourceObject.client;
            break;
         }
      }
   }
   if (%this.issuedByClientId > 0 && %this.issuedByClinetId == %client)
   {
       echo("Hey " @ %client.nameBase @ "! cant laze target for your self IDIOT !");
       return 0;
   }

   //no need to laze if the object is already destroyed
   if (!isObject(%this.targetObjectId) || %this.targetObjectId.getDamageState() $= "Destroyed")
      return 0;
   else if (%this.targetObjectId.isHidden() || %this.targetObjectId.team == %client.team)
      return 0;
   else if (getSimTime() - %this.lastLazedTime <= 10000 && %this.lastLazedClient == %client)
      return 0;
   else
   {
      //set the base weight
      switch (%level)
      {
         case 1:
            %weight = %this.weightLevel1;
         case 2:
            %weight = %this.weightLevel2;
         case 3:
            %weight = %this.weightLevel3;
         default:
            %weight = %this.weightLevel4;
      }

      //check Affinity
      if (ClientHasAffinity(%this, %client))
         %weight += 100;

      //for now, do not deviate from the current assignment to laze a target, if you don't
      //already have a targeting laser.
      %needEquipment = AINeedEquipment(%this.equipment, %client);
      if (!%needEquipment)
         %weight += 100;
      else if (!aiHumanHasControl(%client.controlByHuman, %client))
         return 0;

      //see if this client is close to the issuing client
      if (%this.issuedByClientId > 0)
      {
         if (! AIClientIsAlive(%this.issuedByClientId))
            return 0;

         %distance = %client.getPathDistance(%this.issuedByClientId.player.getWorldBoxCenter());
         if (%distance < 0)
            %distance = 32767;

         //see if we're over 300 m
         if (%distance > 300)
            return 0;

         //see if we're within 200 m
         if (%distance < 200)
            %weight += 30;

         //see if we're within 90 m
         if (%distance < 90)
            %weight += 30;

         //see if we're within 45 m
         if (%distance < 45)
            %weight += 30;
      }
      //now, if this bot is linked to a human who has issued this command, up the weight
      if (%this.issuedByClientId == %client.controlByHuman && %weight < $AIWeightHumanIssuedCommand)
         %weight = $AIWeightHumanIssuedCommand;

      return %weight;
   }
}

function AIOLazeObject::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AILazeObject);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIOLazeObject::unassignClient(%this, %client)
{
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ AIO Mortar Object +++
//Modified so bot gets LOS to target before firing. Fixed a lot of stuff here.          - Lagg...

function AIOMortarObject::weight(%this, %client, %level, %minWeight, %inventoryStr)
{

   // if were playing CnH, check who owns this
   if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden() || %this.targetObjectId.team == %client.team)
      return 0;

   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   //no need to attack if the object is already destroyed
   if (%this.targetObjectId.getDamageState() $= "Destroyed")
      return 0;
   else
   {
      //if this bot is linked to a human who has issued this command, up the weight
      if (%this.issuedByClientId == %client.controlByHuman)
      {
         //make sure we have the potential to reach the minWeight
         if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
         {
            if ($AIWeightHumanIssuedCommand < %minWeight)
               return 0;
            else
               %weight = $AIWeightHumanIssuedCommand;
         }
         else
         {
            // calculate the default...
            %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
            if (%weight < $AIWeightHumanIssuedCommand)
               %weight = $AIWeightHumanIssuedCommand;
         }
      }
      else
      {
         //make sure we have the potential to reach the minWeight
         if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
	    return 0;

	 // calculate the default...
         %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
      }
      return %weight;
   }
}

//modified client escorter - Lagg... - 4-12-2003
function AIOMortarObject::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIMortarObject);
   %client.objectiveTask.initFromObjective(%this, %client);

   //create the escort objective (require a targeting laser in this case...)
//   %client.escort = new AIObjective(AIOEscortPlayer)
//   {
//      dataBlock = "AIObjectiveMarker";
//      weightLevel1 = $AIWeightEscortOffense[1];
//      weightLevel2 = $AIWeightEscortOffense[2];
//      description = "Escort " @ getTaggedString(%client.name);
//      targetClientId = %client;
//      offense = true;
//      equipment = "TargetingLaser";
//      buyEquipmentSet = "MediumSensorJammerTriumph";
//   };
//   MissionCleanup.add(%client.escort);

   //if is siege game we have to do this right - Lagg... 11-3-2003
//   if (Game.class $= "SiegeGame")
//   {
      //the objectives in $ObjectiveQ[1] are all offense objectives, $ObjectiveQ[2] has the defensive ones..
//      if (%client.team == game.offenseTeam)
//         $ObjectiveQ[1].add(%client.escort);
//      else
//         $ObjectiveQ[2].add(%client.escort);
//   }
//   else
//      $ObjectiveQ[%client.team].add(%client.escort);
}

function AIOMortarObject::unassignClient(%this, %client)
{
   //kill the escort objective
//   if (%client.escort)
//   {
//      clearObjectiveFromTable(%client.escort); // New function - Lagg...
//      AIClearObjective(%client.escort);
//      %client.escort.delete();
//      %client.escort = "";
//   }

   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ AIO Deploy Equipment +++
//modified here so bot will continue deploying if has equip and not get bumped
//can be set at a realistic weight now, like 3400               - Lagg... 4-12-2003
//If the function ShapeBaseImageData::testInvalidDeployConditions() changes at all, those changes need to be reflected here

function AIODeployEquipment::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   //make sure the deploy objective is valid
   if (%this.isInvalid)
      return 0;

   if (%this.mode $= "MPB")
   {
      %result = AIFindDeployedMPB(%client);
      %clM = getWord(%result, 0);
      if (!isObject(%clM) || VectorDist(%clM.getWorldBoxCenter(), %this.position) > 150)
         return 0;
   }

   // Force Field Avoidance - Lagg..
//    InitContainerRadiusSearch(%client.player.getWorldBoxCenter(), 8, $TypeMasks::ForceFieldObjectType);
//    %objSearch = containerSearchNext();
//    if (%objSearch > 0)
//    {
//       %ffDis = %client.getPathDistance(%objSearch.getWorldBoxCenter());
//       %myDis = %client.getPathDistance(%this.position);
//       if (%ffDis < %myDis )
//       {
//          if (%objSearch.isPowered() && %objSearch.team != %client.team)
//             return 0;
//       }
//    }

   //first, make sure we haven't deployed too many...
   if (%this.equipment $= "TurretOutdoorDeployable" || %this.equipment $= "TurretIndoorDeployable")
      %maxAllowed = countTurretsAllowed(%this.equipment);
   else
      %maxAllowed = $TeamDeployableMax[%this.equipment];

   if ($TeamDeployedCount[%client.team, %this.equipment] >= %maxAllowed)
      return 0;

   //now make sure there are no other items in the way...
   InitContainerRadiusSearch(%this.location, $MinDeployableDistance, $TypeMasks::VehicleObjectType |
  	                                             $TypeMasks::MoveableObjectType |
  	                                             $TypeMasks::StaticShapeObjectType |
  	                                             $TypeMasks::TSStaticShapeObjectType |
  	                                             $TypeMasks::ForceFieldObjectType |
  	                                             $TypeMasks::ItemObjectType |
  	                                             $TypeMasks::TurretObjectType);
   %objSearch = containerSearchNext();

   // deploy despite of deployables of the other team in range
   if (%objSearch.team > 0 && %objSearch.team != %client.team)
      %objSearch = containerSearchNext();

   //did we find an object which would block deploying the equipment?
   if (isObject(%objSearch))
      return 0;

   //now run individual checks based on the equipment type...
   if (%this.equipment $= "TurretIndoorDeployable")
   {
      //check if there's another turret close to the deploy location
      InitContainerRadiusSearch(%this.location, $TurretIndoorSpaceRadius, $TypeMasks::StaticShapeObjectType);
      %found = containerSearchNext();
      if (isObject(%found))
      {
         %foundName = %found.getDataBlock().getName();
 	    if ((%foundName $= TurretDeployedFloorIndoor) || (%foundName $= "TurretDeployedWallIndoor") ||
             (%foundName $= "TurretDeployedCeilingIndoor") || (%foundName $= "TurretDeployedOutdoor"))
 	       return 0;
      }

      //now see if there are too many turrets in the area...
      %highestDensity = 0;
      InitContainerRadiusSearch(%this.location, $TurretIndoorSphereRadius, $TypeMasks::StaticShapeObjectType);
      %found = containerSearchNext();
      while (isObject(%found))
      {
         %foundName = %found.getDataBlock().getName();
 	   if ((%foundName $= "TurretDeployedFloorIndoor") || (%foundName $= "TurretDeployedWallIndoor") ||
            (%foundName $= "TurretDeployedCeilingIndoor") || (%foundName $= "TurretDeployedOutdoor"))
 	   {
 	    //found one
 	      %numTurretsNearby++; //+++++++++++++++++++++++++++++++++++++++++ ahh review this - Lagg... +++
            %nearbyDensity = testNearbyDensity(%found, $TurretIndoorSphereRadius);
 	      if (%nearbyDensity > %highestDensity)
 	         %highestDensity = %nearbyDensity;
         }
 	   %found = containerSearchNext();
      }

      if (%numTurretsNearby > %highestDensity)
         %highestDensity = %numTurretsNearby;

      //now see if the area is already saturated
      if (%highestDensity > $TurretIndoorMaxPerSphere)
         return 0;
   }
   //--------------------left off here - Lagg...+++++++++++++++++++++++++
   else if (%this.equipment $= "TurretOutdoorDeployable")
   {
      //check if there's another turret close to the deploy location
      InitContainerRadiusSearch(%this.location, $TurretOutdoorSpaceRadius, $TypeMasks::StaticShapeObjectType);
      %found = containerSearchNext();
      if (isObject(%found))
      {
         %foundName = %found.getDataBlock().getName();
         if ((%foundName $= "TurretDeployedFloorIndoor") || (%foundName $= "TurretDeployedWallIndoor") || (%foundName $= "TurretDeployedCeilingIndoor") || (%foundName $= "TurretDeployedOutdoor"))
            return 0;
      }

      //now see if there are too many turrets in the area...
      %highestDensity = 0;
      InitContainerRadiusSearch(%this.location, $TurretOutdoorSphereRadius, $TypeMasks::StaticShapeObjectType);
      %found = containerSearchNext();
      while (isObject(%found))
      {
         %foundName = %found.getDataBlock().getName();
         if ((%foundName $= "TurretDeployedFloorIndoor") || (%foundName $= "TurretDeployedWallIndoor") || (%foundName $= "TurretDeployedCeilingIndoor") || (%foundName $= "TurretDeployedOutdoor"))
         {
            //found one
            %numTurretsNearby++;
            %nearbyDensity = testNearbyDensity(%found, $TurretOutdoorSphereRadius);
            if (%nearbyDensity > %highestDensity)
               %highestDensity = %nearbyDensity;
         }
         %found = containerSearchNext();
      }
      if (%numTurretsNearby > %highestDensity)
         %highestDensity = %numTurretsNearby;

      //now see if the area is already saturated
      if (%highestDensity > $TurretOutdoorMaxPerSphere)
         return 0;
   }

   //check equipment requirement
   %needEquipment = AINeedEquipment(%this.equipment, %client);

   //if don't need equipment, see if we've past the "point of no return", and should continue regardless
   if (!%needEquipment)
   {
      %needArmor = AIMustUseRegularInvStation(%this.equipment, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      if (%closestInv > 0) {   // No functioning inv station around -> don't bother using inv anymore
         %closestInv.location = %closestInv.trigger.getWorldBoxCenter();
         %result = AICheckUseTeleport(%client, %closestInv);//new function in aiVehicle.cs
         %closestDist = getWord(%result, 1);

         //if we're too far from the inv to go back, or we're too close to the deploy location, force continue
         //if (%closestDist > 50 && VectorDist(%client.player.getWorldBoxCenter(), %task.location) < 50)
         if (%closestDist > 25 || %closestInv < 0)
         {
            %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
            if (%weight < $AIWeightContinueDeploying)
               %weight = $AIWeightContinueDeploying;

            return %weight;
         }
      }
    }

   //if this bot is linked to a human who has issued this command, up the weight
   if (%this.issuedByClientId == %client.controlByHuman)
   {
      //make sure we have the potential to reach the minWeight
      if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
      {
         if ($AIWeightHumanIssuedCommand < %minWeight)
            return 0;
         else
            %weight = $AIWeightHumanIssuedCommand;
      }
      else
      {
         // calculate the default...
         %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);

         if (%weight < $AIWeightHumanIssuedCommand)
            %weight = $AIWeightHumanIssuedCommand;
      }
   }
   else
   {
      //make sure we have the potential to reach the minWeight
      if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
         return 0;

      if (%level == 1) if (%this.equipment !$= "TelePadPack") {
         // Items to be deployed near our flag are in jeopardy of getting nuked away constantly, thus blocking a lot of bots by deploying in vain. Therefore give a weight penalty to the base defense:
         %distance = VectorDist(%this.location, $AITeamFlag[%client.team].originalPosition);
         if (%distance < 180) %this.weightLevel1 = $AIWeightDeployBase;
      }

      // calculate the default...
      %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
//      error(%client.nameBase SPC "AIODeploy: Weight2" SPC %weight);
   }
   return %weight;
}

function AIODeployEquipment::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIDeployEquipment);
   %task = %client.objectiveTask;
   %task.initFromObjective(%this, %client);
}

function AIODeployEquipment::unassignClient(%this, %client)
{
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//------------------------------------------------------------------------
