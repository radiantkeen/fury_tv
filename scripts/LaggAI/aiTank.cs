//----------------------------------------------------------------------------------------------- AIO Tank Patrol ---
//Set up to 10 simgroups called "T" @ %team @ "TankPath" @ %num
//(example T2TankPath1 to T2TankPath10 / T1TankPath1 to T1TankPath10)
//NOTE: must start with number 1 and count in order up to 10
//inside these simgroups place markers in the order to follow for each
//path. Thats 1 simgroup per path for each team - Lagg... 4-8-2004


function AIOTankPatrol::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   //check if flight path exists
   if (Game.class $= "SiegeGame")
      %team = Game.offenseTeam == %client.team ? 1 : 2;
   else
      %team = %client.team;

   if(! isObject(nameToId("T" @ %team @ "TankPath1")))
      return 0;

   //do a quick check to disqualify this objective if it can't meet the minimum weight
   if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
   {
      if (%this.targetObjectId > 0 && %this.issuedByClientId == %client.controlByHuman)
      {
         if ($AIWeightHumanIssuedCommand < %minWeight)
	    return 0;
      }
      else
         return 0;
   }

   //check for Vehicle station near buy of forget it if we are not mounted yet or didn't just buy - Lagg...
   if (!%client.player.isMounted() && !%client.justBought)
   {
      %clVs = AIFindClosestVStation(%client);
      if (%clVs > 0)
      {
         %closestVs = getWord(%clVs, 0);
         %closestVsDist = getWord(%clVs, 1);
//         if (%closestVsDist > 300)//----------------------* close to VPad or return 0 *---
//            return 0;
      }
      else
         return 0;

      //check if any of vehicle type are availible
//      %blockName = "AssaultVehicle";
      %blockName = getRandomVehicleType($VehicleList::Tanks); //"ScoutFlyer";
      %client.aiVehicleConsidering = %blockName;
      if (!vehicleCheck(%blockName, %client.team))
         return 0;
   }

   %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);

   //never bump a pilot from his ride
   if (%this.clientLevel1 > 0  && %level == 1 && %this.clientLevel1 != %client)
      return 0;
   else if (%this.clientLevel1 == %client)
      %weight = 20000;

   return %weight;
}

function AIOTankPatrol::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AITankPatrol);
   %client.objectiveTask.initFromObjective(%this, %client);

   //create the escort objective (require a gunner in this case...)
   %client.escort = new AIObjective(AIOEscortPlayer)
                     {
                        dataBlock = "AIObjectiveMarker";
                        weightLevel1 = $AIWeightVehicleMountedEscort;
                        weightLevel2 = 0;
                        description = "Escort " @ getTaggedString(%client.name);
                        targetClientId = %client;
                        offense = true;
                        chat = "ChatNeedHold";
                        equipment = "FieldTech HeavyLauncher || Medium";
                        buyEquipmentSet = "MediumTailgunner";
                     };
   MissionCleanup.add(%client.escort);

   //if is siege game we have to do this right - Lagg... 11-3-2003
   if (Game.class $= "SiegeGame")
   {
      //the objectives in $ObjectiveQ[1] are all offense objectives, $ObjectiveQ[2] defense
      if (%client.team == game.offenseTeam)
         $ObjectiveQ[1].add(%client.escort);
      else
         $ObjectiveQ[2].add(%client.escort);
   }
   else
      $ObjectiveQ[%client.team].add(%client.escort);
}

function AIOTankPatrol::unassignClient(%this, %client)
{
   //kill the escort objective
   if (%client.escort)
   {
      clearObjectiveFromTable(%client.escort);//new function below to clear objective from $objTable[] - Lagg... 1-27-2004
      AIClearObjective(%client.escort);
      %client.escort.delete();
      %client.escort = "";
   }
   if(%client.pilotVehicle)
      {
         AIDisembarkVehicle(%client);
      }
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//----------------------------------------------------------------------------------------------- AI Tank Patrol ---

function AITankPatrol::initFromObjective(%task, %objective, %client)
{
   //initialize the task vars from the objective
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;
   if (%objective.Location !$= "")
      %task.location = %objective.location;
   else
      %task.location = %objective.targetObjectId.getWorldBoxCenter();

   %task.equipment = %objective.equipment;
   %task.buyEquipmentSet = %objective.buyEquipmentSet;
   %task.desiredEquipment = %objective.desiredEquipment;
   %task.issuedByClient = %objective.issuedByClientId;
   %task.chat = %objective.chat;

   //initialize other task vars
   %task.sendMsg = true;
   %task.sendMsgTime = 0;
   %task.path = "";

   %client.player.mVehicle = "";// needed if bot just hopped out a vehicle and it was not destroyed/removed yet
}

function AITankPatrol::assume(%task, %client)
{
   //set to call real slow (bots are safe in a tank)
   %task.setWeightFreq(50);
   %task.setMonitorFreq(50);

   %client.needEquipment = AINeedEquipment(%task.equipment, %client);

   //even if we don't *need* equipemnt, see if we should buy some...
   if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
   {
      //see if we could benefit from inventory
      %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      %closestDist = getWord(%result, 1);
      if (AINeedEquipment(%task.desiredEquipment, %client) && %closestInv > 0)
      {
         %result = AIFindClosestEnemy(%client, 100, $AIClientLOSTimeout);
	 %closestEnemy = getWord(%result, 0);
	 %closestEnemydist = getWord(%result, 1);

	 if (%closestEnemy <= 0 || (%closestEnemyDist > %closestDist * 1.5))
	    %client.needEquipment = true;
      }
   }

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();

   //reset the vehicle station wait time
   %client.vsWaitTime = "";

   //set the destination paths for each team and game type

   //first see how many paths we have
   //check if flight path exists
   if (Game.class $= "SiegeGame")
      %team = Game.offenseTeam == %client.team ? 1 : 2;
   else
      %team = %client.team;

   for(%i = 1; %i < 10; %i++)
   {
      if(isObject(nameToId("T" @ %team @ "TankPath" @ %i)))
         %mx = %i;
   }

   %random = mFloor(getRandom(1, %mx));

   // - set the path simgroup to follow
   %task.group = nameToId("T" @ %team @ "TankPath" @ %random);

   %task.count = %task.group.getCount();
   %task.locationIndex = 0;
   %client.needVehicle = true;
}


function AITankPatrol::retire(%task, %client)
{
   if (aiClientIsAlive(%client))
   {
      %client.needVehicle = false;
      %client.clearStep();
      if(%client.player.isMounted())
         AIDisembarkVehicle(%client);
   }
}

function AITankPatrol::weight(%task, %client)
{
   //update the task weight...
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;

   //let the monitor decide when to quit :)//--- NEEDED SO IMPORTANT !
   %task.setWeight(%task.baseWeight);

}

function AITankPatrol::monitor(%task, %client)
{
   //first, buy equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;
      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
         return;
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(50);
	 %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
         //if this task is the objective task, choose a new objective
	 if (%task == %client.objectiveTask)
	 {
	    AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	 }
	 return;
      }
   }
   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   if (%client.needVehicle)
   {
      %clVs = AIFindClosestVStation(%client);
      if (%clVs > 0)
      {
         %closestVs = getWord(%clVs, 0);
         %closestVsDist = getWord(%clVs, 1);
      }
      if (%closestVs > 0 && !isObject(%client.player.mVehicle)) // && %closestVsDist < 300
      {
         //If we're in light or medium armor, buy the vehicle - Lagg...
         if (%client.player.getArmorSize() !$= "Heavy" && %client.player.getArmorSize() !$= "Titan")
         {
            %task.setMonitorFreq(9);
            %buyResult = aiBuyVehicle(%client.aiVehicleConsidering, %client);
         }
         else
         {
            //if ai in heavy armor buy equipment
            if (%task == %client.objectiveTask)
	    {
               %task.baseWeight = %client.objectiveWeight;
               %task.equipment = "Medium || FieldTech HeavyLauncher";
	       %task.buyEquipmentSet = "MediumTailgunner";
               %client.needEquipment = true;
               return;
	    }
         }

         if (%buyResult $= "InProgress")
         {
            //clear offensive tags
            %client.lastDamageClient = -1;
            %client.lastDamageTurret = -1;
            %client.shouldEngage = -1;
            %client.setEngageTarget(-1);
            %client.setTargetObject(-1);
            %client.engageRemeq = -1;
            %client.pickUpItem = -1;
	    return;
         }

         else if (%buyResult $= "Finished")
         {
            //if we are finished buying the vehicle, then we are done
            //this is handled in function Armor::AIonMount - Lagg... 7-9-2004
         }
         else if (%result $= "Failed")
         {
            //if this task is the objective task, choose a new objective
	    if (%task == %client.objectiveTask)
	    {
	       AIUnassignClient(%client);
	       Game.AIChooseGameObjective(%client);
               return;
	    }
         }
      }
      else if ((%closestVs <= 0) && !isObject(%client.player.mVehicle)) // || %closestVsDist >= 300
      {
         if (%task == %client.objectiveTask)
         {
            AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
            return;
         }
      }
   }

   //chat - send the message
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
         {
            if (%task.chat !$= "")
            {
               %chatMsg = getWord(%task.chat, 0);
               %chatTemplate = getWord(%task.chat, 1);
               if (%chatTemplate !$= "")
                  AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
               else
                  AIMessageThread(%task.chat, %client, -1);
            }
         }
      }
   }

   //let set some variables

   //if we managed to get in vehicle then go
   if (%client.player.isMounted() && isObject(%client.player.mVehicle))
   {
      //set a low frequency on this task
      %task.setMonitorFreq(50);

      //get the vehicle
      %vehicle = %Client.vehicleMounted;
      %mySpd = VectorLen(%vehicle.getVelocity());

      //---------------------------------------------------------------------------------------------------------------------- Avoidance ---
      if (%mySpd < 0.1)
      {
         InitContainerRadiusSearch(%vehicle.getWorldBoxCenter(), 5.0, $TypeMasks::InteriorObjectType | $TypeMasks::VehicleObjectType
           | $TypeMasks::TSStaticShapeObjectType);
         %avoid = containerSearchNext();

         if (%avoid == %vehicle)
            %avoid = containerSearchNext();

         //if we are moving slow and close to objects we are probably stuck
         if (%avoid)
         {
            %vx = getWord(%vehicle.getWorldBoxCenter(), 0);
            %vy = getWord(%vehicle.getWorldBoxCenter(), 1);
            %vz = getWord(%vehicle.getWorldBoxCenter(), 2);
            %vz += 2.0;
            %vR = getWords(%vehicle.getTransform(), 3, 6);

            %client.setPilotDestination(%task.location, 1.0);//max speed
            %vehicle.setTransform(%vx SPC %vy SPC %vz SPC %vR);//------------------------ set the Transform Here up a little
         }
      }

      //get the gunner seat: 0 = empty 1 = full
      %nodeGun = %vehicle.getMountNodeObject(1);

      //should we wait for a gunner?
      if (%nodeGun <= 0)
      {
         %pos = %client.vehicleMounted.position;
	 %pos2D = getWord(%pos, 0) SPC getWord(%pos, 1) SPC "0";
         %dest = %task.location;
	 %dest2D = getWord(%dest, 0) SPC getWord(%dest, 1) SPC "0";
         if (VectorDist(%dest2D, %pos2D) < 20)
            %client.stop();//doen't seem to work if ai is pilot
         else
         {
            %client.setPilotPitchRange(-0.2, 0.05, 0.05);
	    %client.setPilotDestination(%task.location, 10); //move to wait spot
         }
      }
      else
      {
         //little check to see if anybody around

         //first check turret potential targets
         %vehicle = %Client.vehicleMounted;
         %turret = %client.player.vehicleTurret; //%vehicle.getMountNodeObject(10);
         %target = %turret.getTargetObject();

         %ourFlag = $AITeamFlag[%client.team];
         %closestdist = 9999;
         if (Game.class $= "CTFGame" && %ourFlag.carrier > 0)  // first check for someone stealing our flag - pinkpanther
         {
            %closestEnemy = %ourFlag.carrier;
            %closestdist = VectorDist(%ourFlag.carrier, %vehicle.getWorldBoxCenter());
         }
         if (%closestdist > 180) {
            //driver see if anybody close to run down (cheat ignor LOS) :) - Lagg...
            %losTimeout = $AIClientMinLOSTime + ($AIClientLOSTimeout * %client.getSkillLevel());
            %result = AIFindClosestEnemyToLoc(%client, %client.player.getWorldBoxCenter(), 70, %losTimeout, true, false);
            %closestEnemy = getWord(%result, 0);
            %closestdist = getWord(%result, 1);
         }

         //check for obstacles
         %vLoc = %vehicle.getWorldBoxCenter();
         InitContainerRadiusSearch(%vLoc, 30, $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType |
           $TypeMasks::VehicleObjectType);
         %obs = containerSearchNext();
         if (%obs == %vehicle)
            %obs = containerSearchNext();

         if (%target > 0)
            %dist = vectorDist(%vehicle.getWorldBoxCenter(), %target.getWorldBoxCenter());

         //if driver spotted a target
         if (%closestEnemy > 0 && %closestdist < 70 && !%obs)//run em dowm even if gunner has target
         {
            %client.setPilotDestination(%closestEnemy.position, 1.0);//run em down!
            %client.aimAt(%closestEnemy.position, 1000);
         }
         else if (%closestEnemy > 0 && %target <= 0 && %closestdist < 200 && !%obs)//only move if gunner has no target
            %client.setPilotDestination(%closestEnemy.position, 0.5);//shoot em

         //if gunner has target - move to target
         else if (%target > 0 && %dist > 200 && !%obs)
            %client.setPilotDestination(%target.position, 0.5);//if gunner has target move slow

         //slow down so gunner can hit something
         else if (%target > 0 && %dist <= 200)//don't check for obstacles, just stop
            %client.setPilotDestination(%client.vehicleMounted.position, 0.0); //this stops him

         //stop so gunner can chaingun target
         else if (%target > 0 && %dist < 35)//since we are stopped no check for %obs
            %client.setPilotDestination(%client.vehicleMounted.position, 0);  //this stops him

         //if there is no enemy, continue on path
         else
         {
            //set the locations
            %location = %task.group.getObject(%task.locationIndex);

            %pos = %client.vehicleMounted.position;
	    %pos2D = getWord(%pos, 0) SPC getWord(%pos, 1) SPC "0";
	    %dest = %location.position;
	    %dest2D = getWord(%dest, 0) SPC getWord(%dest, 1) SPC "0";

            //are we close to location index marker?
	    if (VectorDist(%dest2D, %pos2D) < 25)//25 meters from marker
	    {
	       //if we have another location index
               if ((%task.count - 1) > %task.locationIndex)//has to be -1 on the groups count
                  %task.locationIndex++;
               //we are at end of trail
               else if (%vehicle.getMountNodeObject(1) > 0)  // we still have a gunner -> do not eject yet, but go one step back
                  %task.locationIndex--;
               else
               {
                 if (%task == %client.objectiveTask)
	          {
                    if (%client.vehicleMounted )
                    {
                        AIDisembarkVehicle(%client); //Hop off...
                        %client.stepMove(%location.position, 0.25);
                        return;
                    }
	          }
               }
            }
            %client.setPilotDestination(%location.position, 20);
         }
      }
   }
   else if (!%client.player.isMounted())
   {
      //if we at end of path and we hopped out
      if ((%task.count - 1) == %task.locationIndex)//has to be -1 on the groups count
      {
         if (%task == %client.objectiveTask)
	 {
            AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	 }
      }

      //did we fall off our bike? if so get back on!
      if (isObject(%client.player.mVehicle) && %client.player.mVehicle.getDamageState() !$= "Destroyed" && %client.pilotVehicle)
      {
         //if ai in heavy armor buy equipment
         if (%task == %client.objectiveTask)
	 {
            %task.baseWeight = %client.objectiveWeight;
            %task.equipment = "Medium || FieldTech HeavyLauncher";
	    %task.buyEquipmentSet = "MediumTailgunner";
            %client.needEquipment = true;
            return;
	 }

         //check if someone stole our bike
         if (%client.player.mVehicle.getMountNodeObject(0) > 0)
         {
            if (%task == %client.objectiveTask)
	    {
               AIUnassignClient(%client);
	       Game.AIChooseGameObjective(%client);
               return;
            }
         }

         //throw away any packs that won't fit
	 if (%client.player.getInventory(InventoryDeployable) > 0)
	    %client.player.throwPack();
	 else if (%client.player.getInventory(TurretIndoorDeployable) > 0)
	    %client.player.throwPack();
	 else if (%client.player.getInventory(TurretOutdoorDeployable) > 0)
	    %client.player.throwPack();

         %client.pilotVehicle = true;//needed to let ai mount pilot seat
         %client.stepMove(%client.player.mVehicle.position, 0.25, $AIModeMountVehicle);
      }

      //did someone shoot our legs out? if so we are done!
      else if (isObject(%client.player.mVehicle) && %client.player.mVehicle.getDamageState() $= "Destroyed")
      {
         if (%task == %client.objectiveTask)
	 {
            AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	 }
      }
   }
}
