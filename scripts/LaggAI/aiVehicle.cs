//In this file you will find many but not all of the ai vehicle functions.
//The default vehicle functions are in ai.cs.

//------------------------------------------------------------------------------
//ai find closest enemy vehicle - Lagg... 10-8-2003
//------------------------------------------------------------------------------
function aiFindClosestEnemyVehicle(%client)
{
   //get some vars
   %player = %client.player;
   if (!isObject(%player))
      return -1 @ " " @ 32767;

   if (! AIClientIsAlive(%client))
      return -1 @ " " @ 32767;

   %closestVehicle = -1;
   %closestDist = 32767;
   %vehicleCount = $AIVehicleSet.getCount();
   for (%i = 0; %i < %vehicleCount; %i++)
   {
      if (%vehicleCount > 0)
      {
         %vehicle = $AIVehicleSet.getObject(%i);
         if(%vehicle.getDamageState() !$= "Destroyed")
         {
            %enOnBoard = false;
            for (%m = 0; %m < %vehicle.getDataBlock().numMountPoints; %m++)
            {
               %mount = %vehicle.getMountNodeObject(%m);
               if (isObject(%mount) && %mount.team != %client.team)
               {
                  //echo("aiFindClosestEnemyVehicle - we got an enemy on board");
                  %enOnBoard = true;
               }
               //else
                  //%enOnBoard = false;
            }

            if (%vehicle.team != %client.team || %enOnBoard)
            {
               %vehiclePos = %vehicle.getWorldBoxCenter();
               %clPos = %player.getWorldBoxCenter();

               //check for LOS (kinda)
               %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
               %vehicleLOS = !containerRayCast(%clPos, %vehiclePos, %mask, 0);

               //see if the vehicle is the closest...
               %vehicleDist = VectorDist(%vehiclePos, %clPos);
               if (%vehicleDist < %closestDist && %vehicleLOS)
               {
                  %closestVehicle = %vehicle;
                  %closestDist = %vehicleDist;
               }
            }
         }
      }
   }
   //give em what you got
   return %closestVehicle @ " " @ %closestDist;
}

//------------------------------------------------------------------------------
// ai Buy Vehicle - Lagg... 8-19-2003
//------------------------------------------------------------------------------

function aiBuyVehicle(%vehicleType, %client)
{
   //get some vars
   %player = %client.player;
   if (!isObject(%player))
      return "Failed";

   if (! AIClientIsAlive(%client))
      return "Failed";

   if (%player.getArmorSize() $= "Heavy" || %player.getArmorSize() $= "Titan")
      return "Failed";

   %team = %client.team;

   //get the closest enabled vehicle station
   %result = AIFindClosestVStation(%client);

   %closestVs = getWord(%result, 0);
   %closestDist = getWord(%result, 1);

   //if no vpad then get out
   if (%closestVs <= 0)
      return "Failed";

   //check how many vehicles of this type are in the field if we didn't buy already
   //if (!vehicleCheck(%vehicleType, %client.team))// && !%client.pilotVehicle)
   if (!vehicleCheck(%vehicleType, %client.team) && !%client.player.lastVehicle)
   {
      //error("aiBuyVehicle - no vehicles left :)");
      return "Failed";
   }

   //at this point we have located the vehicle station
   if (%closestVs.isPowered())//is powered - thanks ZOD
   {
      if (!%closestVs.isDisabled())//not blown up - thanks ZOD
      {
         //use the Y-axis of the rotation as the desired direction of approach,
         //and calculate a walk to point 1m in front of the trigger point - Lagg...

         %aprchDirection = MatrixMulVector("0 0 0 " @ getWords(%closestVs.getTransform(), 3, 6), "0 1 0");
         %aprchDirection = VectorNormalize(%aprchDirection);

         //make sure the vehicle station is not blocked
         %vsLoc = %closestVs.getWorldBoxCenter();
         InitContainerRadiusSearch(%vsLoc, 1.5, $TypeMasks::PlayerObjectType);
         %objSrch = containerSearchNext();
         if (%objSrch == %client.player)
            %objSrch = containerSearchNext();

         //is the pad blocked? don't look for players here. just blow them up :)
         %position = %closestVs.pad.position;
         InitContainerRadiusSearch(%position, 20, $TypeMasks::VehicleObjectType);
         %posSrch = containerSearchNext();

         //did we find some something?
         if (%posSrch > 0)
         {
            //did someone forget their vehicle?
            %vehicle = %posSrch.getDataBlock();
            %pilot = %posSrch.getMountNodeObject(0);
            if (%posSrch.lastPilot != %client.player && !%pilot && !%posSrch.inStation)
               %posSrch = -1;//just continue and buy a vehicle

            //we must be mounted so go
            else if (%player.isMounted())
            {
               %client.needVehicle = false;
               return "Finished";
            }
         }

         //if the closest vehicle station is busy...
         //if ((%objSrch > 0) || (%posSrch > 0))
         if (%objSrch > 0)
         {
            //have the AI range the vehicle station
	      //if (%closestVs > 0 && %posSrch.lastPilot != %client.player)
            if (vectorDist(%client.player.getWorldBoxCenter(), %closestVs.getWorldBoxCenter()) > 8)
               %client.stepRangeObject(%closestVs, "DefaultRepairBeam", 3, 6);
	    //vehicle station is still busy - see if we're within range
	      else if (vectorDist(%client.player.getWorldBoxCenter(), %closestVs.getWorldBoxCenter()) < 12)
	      {

	       //initialize the wait time
	       if (%client.vsWaitTime $= "")
                  %client.vsWaitTime = getSimTime() + 6000 + (getRandom() * 1000);

	       //else see if we've waited long enough and set an itchy reaction
               else if (getSimTime() > %client.vsWaitTime && %objSrch > 0)
	       {
                  schedule(250, %client, "AIPlayAnimSound", %client, %objSrch.getWorldBoxCenter(), "vqk.move", -1, -1, 0);
	          %client.vsWaitTime = getSimTime() + 6000 + (getRandom() * 1000);
                  %client.setDangerLocation(%client.player.getWorldBoxCenter(), 10);
	       }
	     }
         }

         //else if we've triggered the vs then buy vehicle, and wait till we are mounted
         else if (%client.needVehicle && isObject(%closestVs.trigger) &&
           VectorDist(%closestVs.trigger.getWorldBoxCenter(), %player.getWorldBoxCenter()) < 1.5)
         {
            //first stop...
	    %client.stop();

            //look in awe
            %client.aimAt(%closestVs.pad.getWorldBoxCenter(), 2500);

            //initialize the wait time
	    if (%client.vsWaitTime $= "")
               %client.vsWaitTime = getSimTime() + 5000;

	    //wait a few sec before trying to buy
            else if (getSimTime() > %client.vsWaitTime)
	    {
               //buy vehicle
               %client.pilotVehicle = true;//needed to allow ai to get in pilot seat
               schedule(1500, 0, "serverCmdBuyVehicle", %client, %vehicleType);
               %client.justBought = true;//set the vehicle station tag, reset in function Armor::AIonMount - Lagg...

               //jump on VPad every fifteen sec to reset the vehicle hud
	       %client.vsWaitTime = getSimTime() + 15000;
               %client.pressJump();
	    }

            //throw away any packs that won't fit
	    if (%client.player.getInventory(InventoryDeployable) > 0)
	       %client.player.throwPack();
	    else if (%client.player.getInventory(TurretIndoorDeployable) > 0)
	       %client.player.throwPack();
            else if (%client.player.getInventory(TurretOutdoorDeployable) > 0)
               %client.player.throwPack();
            else if (%client.player.getInventory(BunkerDeployable) > 0)
               %client.player.throwPack();
            else if (%client.player.getInventory(TelePadPack) > 0)
               %client.player.throwPack();

            return "InProgress";
         }

         //else, keep moving towards the inv station
         else
         {
            if (isObject(%closestVs) && isObject(%closestVs.trigger))
	    {
               %factor = 1; //1m in front of station vehicle is the right spot to stand
               %aprchFromLocation = VectorAdd(%vsLoc,VectorScale(%aprchDirection, %factor));
               %client.stepMove(%aprchFromLocation);
            }
	    return "InProgress";
         }
      }
      else
         return "Failed";
   }
   else
      return "Failed";
}

//------------------------------------------------------------------------------
// Find closest Vehicle station - Lagg... 8-19-2003
//------------------------------------------------------------------------------

//this function will return the closest vehicle station and the distance - Lagg... 8-25-2003

function AIFindClosestVStation(%client)
{
   %closestVStation = -1;
   %closestDist = 32767;

   // lets find the closest Vpad that is powered and enabled and not enemy owned
   %vsCount = $AIVehiclePadSet.getCount();
   for (%i = 0; %i < %vsCount; %i++)
   {
      if (%vsCount > 0)
      {
         %VStation = $AIVehiclePadSet.getObject(%i);
         if (%VStation.team <= 0 || %VStation.team == %client.team)
         {
            //make sure the station is not destroyed - ZOD
            if (!%VStation.isDisabled())
            {
               //make sure the station is getting power - ZOD
               if (%VStation.isPowered())
               {
                  %dist = %client.getPathDistance(%VStation.getTransform());
                  if (%dist > 0 && %dist < %closestDist)
                  {
                     %closestVStation = %VStation;
                     %closestDist = %dist;
                  }
               }
            }
         }
      }
      else
         error("VStation count = 0");
   }

   //give em what you got
   return %closestVStation @ " " @ %closestDist;
}

//------------------------------------------------------------------------------
// AI Find deployed MPB - Lagg... 9-30-2003
//------------------------------------------------------------------------------

//this function will return the closest team deployed MPB and the distance - Lagg... 8-30-2003
//called from aiObjectives.cs

function AIFindDeployedMPB(%client)
{
   //first look for vehicles
   %closestVeh = -1;
   %closestDist = 32767;
   %vehCount = $AIVehicleSet.getCount();
   for (%i = 0; %i < %vehCount; %i++)
   {
      if (%vehCount > 0)
      {
         %Vehicle = $AIVehicleSet.getObject(%i);
         if (!%vehicle.respawn)
         {
            if (%Vehicle.team == %client.team)
            {
               if (%vehicle.getDataBlock().getName() $= "mobileBaseVehicle")
               {
                  if (%vehicle.fullyDeployed)
                  {
                     if (%vehicle.getDamageState() !$= "Destroyed")
                     {
                        %dist = %client.getPathDistance(%vehicle.getTransform());
                        if (%dist > 0 && %dist < %closestDist)
                        {
                           %closestVeh = %vehicle;
                           %closestDist = %dist;
                        }
                     }
                  }
               }
            }
         }
      }
   }

   //give em what you got
   return %closestVeh @ " " @ %closestDist;
}

// lets find Triumph deployable teleporters...   -   pinkpanther
function AICheckUseTeleport(%client, %task) {
   %tel1 = -1;
   %tel2 = -1;
   %closestTel = -1;
   %closestDist = 32767;
   %depCount = 0;
   %depGroup = nameToID("MissionCleanup/Deployables");
   %clPos = %client.player.getWorldBoxCenter();
   %clObjDist = VectorDist(%task.location, %clPos);
   if (isObject(%depGroup) && !(%client.player.holdingFlag > 0))
   {
      %depCount = %depGroup.getCount();
      for (%i = 0; %i < %depCount; %i++)
      {
         %obj = %depGroup.getObject(%i);
         %objName = %obj.getDataBlock().getName();
         if (%objName $= "TelePadDeployedBase" && %obj.isEnabled() && %obj.team == %client.team) {
            if (%tel1 == -1) {
               %tel1 = %obj;
               %telPos1 = %obj.getWorldBoxCenter();
            } else {
               %tel2 = %obj;
               %telPos2 = %obj.getWorldBoxCenter();
            }
// error (getTaggedString(%client.name) SPC %telPos1 SPC %telPos2);
         }
      }
  	   if (!(%tel2 == -1)) {
         %clTelDist1 = VectorDist(%telPos1, %clPos) + VectorDist(%telPos2, %task.location);
// error (getTaggedString(%client.name) SPC %clTelDist1 SPC %objTelDist2 SPC %clObjDist);
         if (%clTelDist1 < %clObjDist) {
            return %tel1 SPC %clTelDist1;
         } else {
            %clTelDist2 = VectorDist(%telPos2, %clPos) + VectorDist(%telPos1, %task.location);
            if (%clTelDist2 < %clObjDist)
               return %tel2 SPC %clTelDist2;
         }
      }
   }
// echo (getTaggedString(%client.name) SPC %clTelDist1 SPC %objTelDist2 SPC %clObjDist);
   return -1 SPC %client.getPathDistance(%task.location);
}
