//------------------------------
//AI Inventory functions

function AINeedEquipment(%equipmentList, %client)
{
   %result = false;
   %index = 0;
   %item = getWord(%equipmentList, %index);

   if (%equipmentList $= "RepairPack") %equipmentList = "RepairPack || RepairGun2";
   if (%equipmentList $= "Mortar MortarAmmo") %equipmentList = "Plasma PlasmaAmmo || MissileLauncher MissileLauncherAmmo || Hornet HornetAmmo || RailCannon RailCannonAmmo || SolarCharge";
//   %equipmentList = strReplace(%equipmentList, "RepairPack", "RepairPack || RepairGun2");
//   %equipmentList = strReplace(%equipmentList, "Mortar MortarAmmo", "Plasma PlasmaAmmo || MissileLauncher MissileLauncherAmmo || Hornet HornetAmmo || RailCannon RailCannonAmmo || SolarCharge");

   while (%item !$= "")
   {
//if (strstr(%equipmentList,"WidowMaker")>=0){ echo (getTaggedString(%client.name) @ " AINeedEquipment:eq.List" SPC %equipmentList SPC "item:"@%item@" #inv:"@%client.player.getInventory(%item));}

      // Make OR conjunction of equipmentLists possible using "||" - pinkpanther
      if (%item $= "||") {
         if (%result)
            %result = false;
         else
            return false;
      }
      // testing the armor class...
      else if (%item $= "Heavy" || %item $= "Medium" || %item $= "Light" || %item $= "FieldTech" || %item $= "Sniper" || %item $= "Recon" || %item $= "Titan") {
         if (%client.player.getArmorSize() !$= %item)
            %result = true;
      }
      else if (%client.player.getInventory(%item) == 0)
         %result = true;

      // get the next item
      %index++;
      %item = getWord(%equipmentList, %index);
   }

   //made it through the list, now return the result
   return %result;
}

function AIBuyInventory(%client, %requiredEquipment, %equipmentSets, %buyInvTime, %task)
{
   //make sure we have a live player
   %player = %client.player;
   if (!isObject(%player))
      return "Failed";

   if (! AIClientIsAlive(%client))
      return "Failed";

   //see if we've already initialized our state machine
   if (%client.buyInvTime == %buyInvTime)
      return AIProcessBuyInventory(%client);

   //if the closest inv station is not a remote, buy the first available set...
   %result = AIFindClosestInventory(%client, false);
   %closestInv = getWord(%result, 0);
   %closestDist = getWord(%result, 1);
   if (%closestInv <= 0) {        // reset equipment to default loadout for maps with no inv station (or if gen is down) - pinkpanther
      %task.equipment = "RepairGun2";
      %task.desiredEquipment = "";
      %client.needEquipment = AINeedEquipment(%task.equipment, %client);
      if (%client.needEquipment)
         return "Failed";
      else
         return "Finished";
   }

   //see if the closest inv station was a remote
   %buyingSet = false;
   %usingRemote = false;
   %eqListHasORConj = strstr(%requiredEquipment, "||") >= 0;
   if (%closestInv.getDataBlock().getName() $= "DeployedStationInventory")
   {
      // reworked   - pinkpanther
      %inventorySet = AIFindSameArmorEquipSet(%equipmentSets, %client);
      if (%inventorySet !$= "")
         %canUseRemote = true;
      else if (%requiredEquipment !$= "") {
         //see if we can buy at least the required equipment from the set
         %canUseRemote = ! AIMustUseRegularInvStation(%requiredEquipment, %client);
         // If theres an OR conjunction in the equipment list we cannot buy inv by equipment, but by set   - pinkpanther
         if (%canUseRemote && %eqListHasORConj) {
            %inventorySet = AIFindSameArmorDefaultSet(%client);
         }
      }
      else
         %canUseRemote = false;

      //if we can't use a remote, we need to look for a regular inv station
      if (! %canUseRemote)
      {
         %result = AIFindClosestInventory(%client, true);
         %closestInv = getWord(%result, 0);
         %closestDist = getWord(%result, 1);
         if (%closestInv <= 0)
            return "Failed";
      }
      else
         %usingRemote = true;
   }

   //at this point we've found the closest inv, see which set/list we need to buy
   if (!%usingRemote) {
      //choose the equipment first equipment set
      if (%equipmentSets !$= "") {
         %inventorySet = getWord(%equipmentSets, 0);
         %buyingSet = true;
      } else if (%eqListHasORConj) {
         %inventorySet = AIFindSameArmorDefaultSet(%client);
         %buyingSet = true;
      } else {
         %inventorySet = %requiredEquipment;
         %buyingSet = false;
      }
   } else {
//      %inventorySet = AIFindSameArmorEquipSet(%equipmentSets, %client);
      if (%inventorySet $= "") {
         %inventorySet = %requiredEquipment;
         %buyingSet = false;
      } else
         %buyingSet = true;
   }

   //init some vars for the state machine...
   %client.buyInvTime = %buyInvTime; //used to mark the begining of the inv buy session
   %client.invToUse = %closestInv;	//used if we need to go to an alternate inv station
   %client.invWaitTime = "";	//used to track how long we've been waiting
   %client.invBuyList = %inventorySet; //the list/set of items we're going to buy...
   %client.buyingSet = %buyingSet; //whether it's a list or a set...
   %client.isSeekingInv = false;
   %client.seekingInv = "";

   //now process the state machine
   return AIProcessBuyInventory(%client);
}

//--------------------------------------------------------------------------------------------------
//long needed fix for AI in Tribes 2 ---------------------------------------------- Lagg... 3-14-2003
//bots will never get stuck on a remote inventory again :)

function AIProcessBuyInventory(%client)
{
   //get some vars
   %player = %client.player;
   if (!isObject(%player))
      return "Failed";

   %closestInv = %client.invToUse;
   %inventorySet = %client.invBuyList;
   %buyingSet = %client.buyingSet;
   //make sure it's still valid, enabled, and on our team
   if (! (%closestInv > 0 && isObject(%closestInv) && (%closestInv.team <= 0 || %closestInv.team == %client.team) && %closestInv.isEnabled()))
   {
      //reset the state machine
      %client.buyInvTime = 0;
      //return "InProgress";
      //echo("RETURN FAILED ! - LAGG...");
      return "Failed";
   }

   //use the Y-axis of the rotation as the desired direction of approach,
   //and calculate a walk to point 1.1 - 1.3 m behind the trigger point - Lagg...
   %aprchDirection = MatrixMulVector("0 0 0 " @ getWords(%closestInv.getTransform(), 3, 6), "0 1 0");
   %aprchDirection = VectorNormalize(%aprchDirection);

   //make sure the inventory station is not blocked
   %invLocation = %closestInv.getWorldBoxCenter();
   InitContainerRadiusSearch(%invLocation, 2, $TypeMasks::PlayerObjectType);
   %objSrch = containerSearchNext();
   if (%objSrch == %client.player)
      %objSrch = containerSearchNext();

   //the closestInv is busy...
   if (%objSrch > 0)
   {
      //have the AI range the inv
      if (%client.seekingInv $= "" || %client.seekingInv != %closestInv)
      {
         %client.invWaitTime = "";
         %client.seekingInv = %closestInv;
         %client.stepRangeObject(%closestInv, "DefaultRepairBeam", 5, 10);
      }
      //inv is still busy - see if we're within range
      else if (%client.getStepStatus() $= "Finished")
      {
         //initialize the wait time
         if (%client.invWaitTime $= "")
            %client.invWaitTime = getSimTime() + 5000 + (getRandom() * 10000);
         //else see if we've waited long enough
         else if (getSimTime() > %client.invWaitTime)
         {
            schedule(250, %client, "AIPlayAnimSound", %client, %objSrch.getWorldBoxCenter(), "vqk.move", -1, -1, 0);
            %client.invWaitTime = getSimTime() + 5000 + (getRandom() * 10000);
         }
      }
      else
      {
         //in case we got bumped, and are ranging the target again...
         %client.invWaitTime = "";
      }
   }
   //else if we've triggered the inv, automatically give us the equipment...
   else if (isObject(%closestInv) && isObject(%closestInv.trigger) && VectorDist(%closestInv.trigger.getWorldBoxCenter(), %player.getWorldBoxCenter()) < 1.5)
   {
      //first stop...
      %client.stop();
      %index = 0;
      if (%buyingSet)
      {
         //first, clear the players inventory
         %player.clearInventory();
         %item = $AIEquipmentSet[%inventorySet, %index];
      }
      else
         %item = getWord(%inventorySet, %index);


      //armor must always be bought first
   	if (%item $= "Light" || %item $= "Medium" || %item $= "Heavy" || %item $= "FieldTech" || %item $= "Sniper" || %item $= "Recon" || %item $= "Titan")       // Triumph armors added 7/13/04 pinkpanther@swissonline.ch
   	{
   	   %player.setArmor(%item);
   	   %index++;
   	}

      //set the data block after the armor had been upgraded
      %playerDataBlock = %player.getDataBlock();

      //next, loop through the inventory set, and buy each item
      if (%buyingSet)
         %item = $AIEquipmentSet[%inventorySet, %index];
      else
         %item = getWord(%inventorySet, %index);

      while (%item !$= "")
      {
         //set the inventory amount to the maximum quantity available
         if (%player.getInventory(AmmoPack) > 0)
            %ammoPackQuantity = AmmoPack.max[%item];
         else
            %ammoPackQuantity = 0;

         %quantity = %player.getDataBlock().max[%item] + %ammoPackQuantity;
         if ($InvBanList[$CurrentMissionType, %item])
            %quantity = 0;

         // ZOD 2-2-03: Added Force = 1, make sure they get their stuff, I hope.
         %player.setInventory(%item, %quantity, 1);

         //get the next item
         %index++;
         if (%buyingSet)
            %item = $AIEquipmentSet[%inventorySet, %index];
         else
            %item = getWord(%inventorySet, %index);
      }

      //put a weapon in the bot's hand...
      %player.cycleWeapon();

      //return a success - but wait if we are not healed all the way :) - Lagg...
      //if (%player.getDamagePercent() <= 0)
      //{
         //get away from inventory if we are done -  pinkpanther
         %client.setDangerLocation(%closestInv.aprchFromLocation, 15);
         return "Finished";
      //}
   }
   //else, keep moving towards the inv station
   else
   {
      if (isObject(%closestInv) && isObject(%closestInv.trigger))
      {
         //quite possibly we may need to deal with what happens if a bot doesn't have a path to the inv...
         //the current premise is that no inventory stations are "unpathable"...
         //this is true so I have fixed it some what - Lagg... :)

         //if (%client.isSeekingInv)
         //{
         //   %dist = %client.getPathDistance(%closestInv.trigger.getWorldBoxCenter());
         // if (%dist < 0)
         //	error("DEBUG Tinman - still need to handle bot stuck trying to get to an inv!");
         //}
         //calculate the approachFromLocation

         %clientPos = %client.player.getWorldBoxCenter();
         %invPos = %closestInv.trigger.getWorldBoxCenter();

         //--------------------------------------------------- Triumph teleporter - start -
         // checking teleporter
         %closestInv.location = %invPos;
         // use triumph teleporter if appropriate:
         if (AIUseTeleport(%client, %closestInv)) return;
         //--------------------------------------------------- Triumph teleporter - end -

         // Approach inv from the front because of deployable bunkers - pinkpanther
         %mask = $TypeMasks::StaticShapeObjectType;           // Bunker walls
         %hasBunkerWallsInLOS = containerRayCast(%clientPos, %invPos, %mask, 0);
         %zDiff = getWord(%invPos, 2) - getWord(%clientPos, 2);
         if (%hasBunkerWallsInLOS > 0 && %zDiff < 5) {
            %factor = -14;
            %client.leaveInvLocation = %client.player.getWorldBoxCenter();
         } else if (%closestInv.getDataBlock().getName() $= "MobileInvStation")
            %factor = -2;
         else {
            %clientArmor = %client.player.getArmorSize();
            if (%clientArmor $= "Heavy" || %clientArmor $= "Titan")       // Triumph armors added 7/13/04 pinkpanther@swissonline.ch
	            %factor = -1.3;//1.3m behind inv trigger for heavy bots
            else if (%clientArmor $= "Medium" || %clientArmor $= "FieldTech")       // Triumph armors added 7/13/04 pinkpanther@swissonline.ch
	            %factor = -1.175;//1.175m behind inv trigger for medium bots
            else
               %factor = -1.1;//1.1m behind inv trigger for light bots
         }
         %closestInv.aprchFromLocation = VectorAdd(%invLocation,VectorScale(%aprchDirection, %factor));
         %client.stepMove(%closestInv.aprchFromLocation);
         %client.isSeekingInv = true;
//echo (getTaggedString(%client.name) SPC %hasBunkerWallsInLOS SPC "aprchFromLocation" SPC %closestInv.aprchFromLocation);
      }
      //added here so bots shoot - Lagg... 9-18-2003
      //this call works in conjunction with AIEngageTask
	   %client.setEngageTarget(%client.shouldEngage);

      return "InProgress";
   }
}

function AIFindSameArmorEquipSet(%equipmentSets, %client)
{
   %clientArmor = %client.player.getArmorSize();
   %index = 0;
   %set = getWord(%equipmentSets, %index);
   while (%set !$= "")
   {
      if ($AIEquipmentSet[%set, 0] $= %clientArmor)
         return %set;

      //get the next equipment set in the list of sets
      %index++;
      %set = getWord(%equipmentSets, %index);
   }
   return "";
}

function AIFindSameArmorDefaultSet(%client)    // new   - pinkpanther
{
   %clientArmor = %client.player.getArmorSize();
   switch$ (%clientArmor) {
   case "Light": return "LightSensorJammer";
   case "Medium": return "MediumSensorJammerTriumph";
   case "Heavy": return "HeavySensorJammer";
   case "FieldTech": return "FieldTechAmmoSet";
   case "Sniper": return "TurretSniper";
   case "Recon": return "ReconShotgun";
   case "Titan": return "TitanDefenderFlag";
   default: return "ReconShotgun";     // Recon
   }
}
//---------------------------------------------------------------------------------------------------

//killed a couple roaches here :) - Lagg... - 4-12-2003
// rewrite -- pinkpanther
function AIMustUseRegularInvStation(%equipmentList, %client)
{
   %result = false;
   %clientArmor = %client.player.getArmorSize();
   %clientDb = getArmorDatablock(%client, %clientArmor); // ZOD: Get our datablock

   //first, see if the set contains an item not available
   %index = 0;
   %item = getWord(%equipmentList, 0);
   while (%item !$= "")
   {
      // Make OR conjunction of equipmentLists possible using "||" - pinkpanther
      if (%item $= "||") {
         if (%result)
            %result = false;
         else
            return false;
      // ZOD: Does the set include armor and if so do we already have it?
      } else if (%item $= "Light" || %item $= "Medium" || %item $= "Heavy" || %item $= "FieldTech" || %item $= "Sniper" || %item $= "Recon" || %item $= "Titan") {
         if (%clientArmor !$= %item) %result = true;
         //else %suitableArmor = %item;
      } // ZOD: Does the set include items we can't carry in our current armor?
      else if ((%clientDb.max[%item] <= 0) || (%item $= "InventoryDeployable")) {
         %result = true;
      }
      %index++;
      %item = getWord(%equipmentList, %index);
   }

//   //otherwise, see if the set begins with an armor class
//   %needArmor = %equipmentList[0];
//   if (%needArmor !$= "Light" && %needArmor !$= "Medium" && %needArmor !$= "Heavy")
//      return false;
//
//   //also including looking for an inventory set
//   //this one does not seem to work light armor still bought at remotes(fixed above) - Lagg...
//   if (%needArmor != %clientArmor)
//      return true;

   //we must be fine...
   return %result;
}

//---------------------------------------------------------------------------------------------------------------
//is called from - 'AIPickupItemTask' in "aiDefaultTasks.cs"
//makes calls to - 'function AICanPickupWeapon'

//modified for better performance and bugs removed - Lagg... 4-3-2003 - (updated 11-5-2003)
function AICouldUseItem(%client, %item)
{
   if(!AIClientIsAlive(%client))
      return false;

   %player = %client.player;
   if (!isObject(%player))
      return false;

   %playerDataBlock = %client.player.getDataBlock();
   %armor = %player.getArmorSize();
   %type = %item.getDataBlock().getName();

   //check packs first
   if(%item.getDataBlock().className $= Pack)
   {
      if(%playerDataBlock.max[%type] > 0)
      {
         if(%player.getMountedImage($BackpackSlot) <= 0)
            return true;
         else
            return false;
      }
   }

   //if the item is acutally, a corpse, check the corpse inventory and compare it to ours - Lagg...
   // ZOD: Ammo only apparently
   if (%item.isCorpse)
   {
      for(%j = 0; $ammoType[%j] !$= ""; %j++)
      {
         if (%item.getInventory($ammoType[%j]) > 0)
         {
            if(%player.getInventory($ammoType[%j]) < %playerDataBlock.max[$ammoType[%j]])
               return true;
         }
      }
   }
   else
   {
      if(%item.getDataBlock().getName() $= "RepairPatch") {
//if (%player.getDamagePercent() > 0) echo (getTaggedString(%client.name) SPC "0:RepairPatch" SPC %item.getDataBlock().className SPC %player.getDamagePercent());
         if (%player.getDamagePercent() > 0)
            return true;
      }
      //check ammo
      else if(%item.getDataBlock().className $= Ammo)
      {
         %quantity = mFloor(%playerDataBlock.max[%type]);
//echo (getTaggedString(%client.name) SPC "1:Ammo" SPC %player.getInventory(%type) SPC %quantity SPC %item.getDataBlock().getName());
         if (%player.getInventory(%type) < %quantity)
            return true;
//echo (getTaggedString(%client.name) SPC "11:Ammo" SPC %item.getDataBlock().getName());
      }
      else if(%item.getDataBlock().className $= HandInventory)
      {
//echo (getTaggedString(%client.name) SPC "2:HandInventory" SPC %item.getDataBlock().getName());
         // ZOD: Loop through grenades and see what type of grenade we already carry..
         for(%i = 0; $InvGrenade[%i] !$= ""; %i++)
         {
            %gren = $NameToInv[$InvGrenade[%x]];
            if (%player.getInventory(%gren) > 0 && %gren $= %type) // We have this grenade
            {
               %quantity = mFloor(%playerDataBlock.max[%type]); // How many can we carry?
               if (%player.getInventory(%type) < %quantity) // Ok we have less then the total we can carry
                  return true;
            }
         }
//echo (getTaggedString(%client.name) SPC "22:HandInventory" SPC %item.getDataBlock().getName());
      }

//echo (getTaggedString(%client.name) SPC "3:AICanPickupWeapon" SPC %item.getDataBlock().getName());
      //see if we can carry another weapon...
      if (AICanPickupWeapon(%client, %item))
         return true;
   }
//echo (getTaggedString(%client.name) SPC "4:nothing useful" SPC %item.getDataBlock().getName());
   //guess we didn't find anything useful...  (should still check for mines and grenades) did that - Lagg...
   return false;
}

//-----------------------------------------------------------------------------------------------------------

//added check for all weapons - Lagg... - 4-12-2003
function AIEngageOutofAmmo(%client)
{
   //this function only cares about weapons used in engagement...
   //no mortars, or missiles - yeah right...Lagg
   %player = %client.player;
   if (!isObject(%player))
      return false;

   %ammoWeapons = 0;
   %energyWeapons = 0;

   //get our inventory
   %hasBlaster = (%player.getInventory("Blaster") > 0);
   %hasPlasma  = (%player.getInventory("Plasma") > 0);
   %hasChain   = (%player.getInventory("Chaingun") > 0);
   %hasDisc    = (%player.getInventory("Disc") > 0);
   %hasGrenade = (%player.getInventory("GrenadeLauncher") > 0);
   %hasSniper  = (%player.getInventory("SniperRifle") > 0);
   %hasHeavyLaser  = (%player.getInventory("HeavyLaser") > 0) && %player.getInventory("EnergyPack") > 0;
   %hasELF     = (%player.getInventory("ELFGun") > 0);
   %hasMortar  = (%player.getInventory("Mortar") > 0);
   %hasMissile = (%player.getInventory("MissileLauncher") > 0);
   %hasLance   = (%player.getInventory("ShockLance") > 0);
   // ----- Triumph specific weapons here: pinkpanther 07/21/04  -----
   %hasAntiAirGun    = (%player.getInventory("AntiAirGun") > 0);
   %hasBeamSword     = (%player.getInventory("BeamSword") > 0);
   %hasDualBlaster   = (%player.getInventory("DualBlaster") > 0);
   %hasElfCannon     = (%player.getInventory("ElfCannon") > 0);
   %hasEMPLauncher   = (%player.getInventory("EMPLauncher") > 0);
   %hasGatlingGun    = (%player.getInventory("GatlingGun") > 0);
   %hasHeadHunter    = (%player.getInventory("HeadHunter") > 0) && %player.getInventory("EnergyPack") > 0;
   %hasHeavyLauncher = (%player.getInventory("HeavyLauncher") > 0);
   %hasHornet        = (%player.getInventory("Hornet") > 0);
   %hasNapalm        = (%player.getInventory("Napalm") > 0);
   %hasRailCannon    = (%player.getInventory("RailCannon") > 0);
   %hasRailNade      = (%player.getInventory("RailNade") > 0);
   %hasShotgun       = (%player.getInventory("Shotgun") > 0);
   %hasSlugRifle     = (%player.getInventory("SlugRifle") > 0);
   %hasSolarCharge   = (%player.getInventory("SolarCharge") > 0);
   %hasTitanChaingun = (%player.getInventory("TitanChaingun") > 0);
   %hasMortarStorm   = (%player.getInventory("MortarStorm") > 0);

   if (%hasBlaster || %hasSniper || %hasHeavyLaser || %hasElf || %hasLance || %hasHeadHunter || %hasDualBlaster || %hasSolarCharge || %hasElfCannon || %hasNapalm || %hasBeamSword)	// pinkpanther 07/21/04
      return false;
   else
   {
      // we only have ammo type weapons
      if(%hasDisc && (%player.getInventory("DiscAmmo") > 0))
         return false;
      else if(%hasChain && (%player.getInventory("ChainGunAmmo") > 0))
         return false;
      else if(%hasGrenade && (%player.getInventory("GrenadeLauncherAmmo") > 0))
         return false;
      else if(%hasPlasma && (%player.getInventory("PlasmaAmmo") > 0))
         return false;
      else if(%hasMissile && (%player.getInventory("MissileLauncherAmmo") > 0))
         return false;
      else if(%hasMortar && (%player.getInventory("MortarAmmo") > 0))
         return false;
   // ----- Triumph specific weapons here: pinkpanther 07/21/04  -----
      else if(%hasAntiAirGun && (%player.getInventory("AntiAirGunAmmo") > 0))
         return false;
      else if(%hasEMPLauncher && (%player.getInventory("EMPLauncherAmmo") > 0))
         return false;
      else if(%hasGatlingGun && (%player.getInventory("GatlingGunAmmo") > 0))
         return false;
      else if(%hasHeavyLauncher && (%player.getInventory("HeavyLauncherAmmo") > 0))
         return false;
      else if(%hasHornet && (%player.getInventory("HornetAmmo") > 0))
         return false;
      else if(%hasRailCannon && (%player.getInventory("RailCannonAmmo") > 0))
         return false;
      else if(%hasRailNade && (%player.getInventory("RailNadeAmmo") > 0))
         return false;
      else if(%hasShotgun && (%player.getInventory("ShotgunAmmo") > 0))
         return false;
      else if(%hasSlugRifle && (%player.getInventory("SlugRifleAmmo") > 0))
         return false;
      else if(%hasTitanChaingun && (%player.getInventory("TitanChaingunAmmo") > 0))
         return false;
      else if(%hasMortarStorm && (%player.getInventory("MortarStormAmmo") > 0))
         return false;
   }
   return true; // were out!
}

//------------------------------------------------------------------------------------------------
//is called from - 'function AICouldUseItem'
//makes calls to - ''


//almost a complete rewrite here :) - Lagg... 11-5-2003
function AICanPickupWeapon(%client, %weapon)
{
   //first, make sure it's not a weapon we already have...
   %player = %client.player;
   if (!isObject(%player))
      return false;

   %armor = %player.getArmorSize();
   if (%player.getInventory(%weapon) > 0)
      return false;

   %type = %weapon.getDataBlock().getName();
   %aBlock = %player.getDatablock(); // ZOD: Get our datablock

   // ZOD: Loop through the weapon arrays, less lines this way.
   %weaponCount = 0;
   for(%a = 0; $InvWeapon[%a] !$= ""; %a++)
   {
      if(%player.getInventory($NameToInv[$InvWeapon[%a]]) > 0)
         %weaponCount++;
   }

   // ZOD: Now check to see if we can carry the item
   if(%weaponCount < %aBlock.maxWeapons)
   {
      // ZOD: We have a special case for the sniper rifle only..
      if(%type $= "HeadHunter" && %player.getInventory("EnergyPack") >= 0)
         return true;
      else if(%aBlock.max[%type] > 0)
         return true;
      else
         return false;
   }
         return false;
}

//-----------------------------------------------------------------------------------------------------------

//added for use of all weapons - Lagg... 4-12-2003
function AIEngageWeaponRating(%client)
{
   %player = %client.player;
   if (!isObject(%player))
      return;

   %playerDataBlock = %client.player.getDataBlock();

   //get our inventory
   %hasBlaster = (%player.getInventory("Blaster") > 0);
   %hasPlasma  = (%player.getInventory("Plasma") > 0 && %player.getInventory("PlasmaAmmo") >= 1);
   %hasMortar  = (%player.getInventory("Mortar") > 0 && %player.getInventory("MortarAmmo") >= 1);
   %hasChain   = (%player.getInventory("Chaingun") > 0 && %player.getInventory("ChaingunAmmo") >= 1);
   %hasDisc    = (%player.getInventory("Disc") > 0 && %player.getInventory("DiscAmmo") >= 1);
   %hasGrenade = (%player.getInventory("GrenadeLauncher") > 0 && %player.getInventory("GrenadeLauncherAmmo") >= 1);
   %hasMissile = (%player.getInventory("MissileLauncher") > 0 && %player.getInventory("MissileLauncherAmmo") >= 1);
   %hasSniper  = (%player.getInventory("SniperRifle") > 0);
   %hasHeavyLaser  = (%player.getInventory("HeavyLaser") > 0) && %player.getInventory("EnergyPack") > 0;
   %hasELF     = (%player.getInventory("ELFGun") > 0);
   %hasLance   = (%player.getInventory("ShockLance") > 0);
   // ----- Triumph specific weapons here: pinkpanther 07/21/04  -----
   %hasAntiAirGun    = (%player.getInventory("AntiAirGun") > 0) && %player.getInventory("AntiAirGunAmmo") > 0;
   %hasBeamSword     = (%player.getInventory("BeamSword") > 0);
   %hasDualBlaster   = (%player.getInventory("DualBlaster") > 0);
   %hasElfCannon     = (%player.getInventory("ElfCannon") > 0);
   %hasEMPLauncher   = (%player.getInventory("EMPLauncher") > 0 && %player.getInventory("EMPLauncherAmmo") > 0);
   %hasGatlingGun    = (%player.getInventory("GatlingGun") > 0 && %player.getInventory("GatlingGunAmmo") > 0);
   %hasHeadHunter    = (%player.getInventory("HeadHunter") > 0) && (%player.getInventory("EnergyPack") > 0);
   %hasHeavyLauncher = (%player.getInventory("HeavyLauncher") > 0 && %player.getInventory("HeavyLauncherAmmo") > 0);
   %hasHornet        = (%player.getInventory("Hornet") > 0 && %player.getInventory("HornetAmmo") > 0);
   %hasNapalm        = (%player.getInventory("Napalm") > 0);
   %hasRailCannon    = (%player.getInventory("RailCannon") > 0 && %player.getInventory("RailCannonAmmo") > 0);
   %hasRailNade      = (%player.getInventory("RailNade") > 0 && %player.getInventory("RailNadeAmmo") > 0);
   %hasShotgun       = (%player.getInventory("Shotgun") > 0 && %player.getInventory("ShotgunAmmo") > 0);
   %hasSlugRifle     = (%player.getInventory("SlugRifle") > 0 && %player.getInventory("SlugRifleAmmo") > 0);
   %hasSolarCharge   = (%player.getInventory("SolarCharge") > 0);
   %hasTitanChaingun = (%player.getInventory("TitanChaingun") > 0 && %player.getInventory("TitanChaingunAmmo") > 0);
   %hasMortarStorm   = (%player.getInventory("MortarStorm") > 0) && (%player.getInventory("MortarStormAmmo") > 0);
   %hasWidowMaker    = (%player.getInventory("WidowMaker") > 0) && (%player.getInventory("WidowMakerAmmo") > 0);

   //check ammo
   %quantity = mFloor(%playerDataBlock.max[%type] * 0.7);

   %rating = 0;
   if (%hasBeamSword)
      %rating += 15;
   if (%hasBlaster)
      %rating += 9;
   if (%hasDualBlaster)
      %rating += 90;
   if (%hasSniper)
      %rating += 9;
   if (%hasHeavyLaser)
      %rating += 9;
   if (%hasHeadHunter)
      %rating += 9;
   if (%hasElf)
      %rating += 9;
   if (%hasElfCannon)
      %rating += 9;
   if (%hasEMPLauncher)
      %rating += 9;
   if (%hasLance)
      %rating += 9;
   if (%hasNapalm)
      %rating += 20;

   if (%hasDisc)
   {
      %quantity = %player.getInventory("DiscAmmo") / %playerDataBlock.max["DiscAmmo"];
      %rating += 20 + (15 * %quantity);
   }
   if (%hasPlasma)
   {
      %quantity = %player.getInventory("PlasmaAmmo") / %playerDataBlock.max["PlasmaAmmo"];
      %rating += 15 + (15 * %quantity);
   }
   if (%hasShotgun)
   {
      %quantity = %player.getInventory("ShotgunAmmo") / %playerDataBlock.max["ShotgunAmmo"];
      %rating += 30 + (15 * %quantity);
   }
   if (%hasSlugRifle)
   {
      %quantity = %player.getInventory("SlugRifleAmmo") / %playerDataBlock.max["SlugRifleAmmo"];
      %rating += 15 + (15 * %quantity);
   }
   if (%hasRailCannon)
   {
      %quantity = %player.getInventory("RailCannonAmmo") / %playerDataBlock.max["RailCannonAmmo"];
      %rating += 5 + (2 * %quantity);
   }
   if (%hasRailNade)
   {
      %quantity = %player.getInventory("RailNadeAmmo") / %playerDataBlock.max["RailNadeAmmo"];
      %rating += 15 + (15 * %quantity);
   }
   if (%hasChain)
   {
      %quantity = %player.getInventory("ChainGunAmmo") / %playerDataBlock.max["ChainGunAmmo"];
      %rating += 15 + (15 * %quantity);
   }
   if (%hasGatlingGun)
   {
      %quantity = %player.getInventory("GatlingGunAmmo") / %playerDataBlock.max["GatlingGunAmmo"];
      %rating += 15 + (15 * %quantity);
   }
   if (%hasTitanChaingun)
   {
      %quantity = %player.getInventory("TitanChaingunAmmo") / %playerDataBlock.max["TitanChaingunAmmo"];
      %rating += 15 + (15 * %quantity);
   }
   if (%hasMortarStorm)
   {
      %quantity = %player.getInventory("MortarStormAmmo") / %playerDataBlock.max["MortarStormAmmo"];
      %rating += 15 + (15 * %quantity);
   }
//    if (%hasWidowMaker)
//    {
//       %quantity = %player.getInventory("WidowMakerAmmo") / %playerDataBlock.max["WidowMakerAmmo"];
//       %rating += 15 + (15 * %quantity);
//    }
   if (%hasHornet)
   {
      %quantity = %player.getInventory("HornetAmmo") / %playerDataBlock.max["HornetAmmo"];
      %rating += 20 + (15 * %quantity);
   }

   //not really an effective weapon for hand to hand...
   if (%hasGrenade)
   {
      %quantity =  %player.getInventory("GrenadeLauncherAmmo") / %playerDataBlock.max["GrenadeLauncherAmmo"];
      %rating += 10 + (15 * %quantity);
   }
   if (%hasHeavyLauncher)
   {
      %quantity =  %player.getInventory("HeavyLauncherAmmo") / %playerDataBlock.max["HeavyLauncherAmmo"];
      %rating += 20 + (15 * %quantity);
   }
   //not really an effective weapon for hand to hand...
   // oh yes, and without weighing it, the AI gets in trouble spawning certain heavies - pinkpanther
   if (%hasMortar)
   {
      %quantity =  %player.getInventory("MortarAmmo") / %playerDataBlock.max["MortarAmmo"];
      %rating += 10 + (15 * %quantity);
   }
   //not really an effective weapon for hand to hand...
   if (%hasMissile)
   {
      %quantity =  %player.getInventory("MissileLauncherAmmo") / %playerDataBlock.max["MissileLauncherAmmo"];
      %rating += 10 + (15 * %quantity);
   }

   if (%player.getArmorSize() $= "Recon")
      %rating *= 4;   // recon has ONLY one weapon and therefore needs a higher rating by default, otherwise he gets unassigned by the AI!

   //note a rating of 20+ means at least two energy weapons, or an ammo weapon with at least 1/3 ammo...
   return %rating;
}

function AIFindSafeItem(%client, %needType)
{
   %player = %client.player;
   if (!isObject(%player))
      return -1;

   %closestItem = -1;
   %closestDist = 32767;

   %itemCount = $AIItemSet.getCount();
   for (%i = 0; %i < %itemCount; %i++)
   {
      %item = $AIItemSet.getObject(%i);
      if (%item.isHidden())
         continue;

      %type = %item.getDataBlock().getName();
      if ((%needType $= "Health" && (%type $= "RepairKit" || %type $= "RepairPatch") && %player.getDamagePercent() > 0) ||
         (%needType $= "Ammo" && (%type $= "ChainGunAmmo" || %type $= "PlasmaAmmo" || %type $= "DiscAmmo" ||
          %type $= "GrenadeLauncherAmmo" || %type $= "MortarAmmo") && AICouldUseItem(%client, %item)) ||
         (%needType $= "Ammo" && AICanPickupWeapon(%type)) ||
         ((%needType $= "" || %needType $= "Any") && AICouldUseItem(%client, %item)))
      {
         //first, see if it's close to us...
         %distance = %client.getPathDistance(%item.getTransform());
         if (%distance > 0 && %distance < %closestDist)
         {
            //now see if it's got bad enemies near it...
            %clientCount = ClientGroup.getCount();
            for (%j = 0; %j < %clientCount; %j++)
            {
               %cl = ClientGroup.getObject(%j);
               if (%cl == %client || %cl.team == %client.team || !AIClientIsAlive(%cl))
                  continue;

               //if the enemy is stronger, see if they're close to the item
               if (AIEngageWhoWillWin(%client, %cl) == %cl)
               {
                  %tempDist = %client.getPathDistance(%item.getWorldBoxCenter());
                  if (%tempDist > 0 && %tempDist < %distance + 50)
                     continue;
               }
               //either no enemy, or a weaker one...
               %closestItem = %item;
               %closestDist = %distance;
            }
         }
      }
   }
   return %closestItem;
}

//-------------------------------------------

//big modifications here - Lagg... - 4-12-2003
function AIChooseObjectWeapon(%client, %targetObject, %distToTarg, %mode, %canUseEnergyStr, %environmentStr)
{
   //get our inventory
   %player = %client.player;
   if (!isObject(%player))
      return;

   if (!isObject(%targetObject))
      return;

   %inWater = (%environmentStr $= "water");
   %outdoors = (%environmentStr $= "outdoors");//added - Lagg... 1-20-2003
   %myEnergy = %player.getEnergyPercent();//added - Lagg... 1-28-2003
   %canUseEnergy = (%canUseEnergyStr $= "true");
   %targetObjectPos  = %targetObject.getWorldBoxCenter();
   %targetName = %targetObject.getDataBlock().getName();                  // pinkpanther
   %targetIsVehicle = %targetObject.getDataBlock().catagory $= "Vehicles";
   %hasBeamSword     = (%player.getInventory("BeamSword") > 0 && %distToTarg < LightBeam.maxRifleRange && %targetName !$= "SentryTurret" && %targetName !$= "DeployedForceField" && %targetName !$= "Deployedwall"
                     && %targetName !$= "Deployedwall" && %targetName !$= "MineDeployed" && %targetName !$= "TelePadDeployedBase" && %canUseEnergy && %myEnergy > BeamSwordImage.minEnergy);
   //check for LOS - Lagg..
   %mask = $LOSMask - ((%targetName $= "DeployedForceField") ? $TypeMasks::ForceFieldObjectType : 0);
   %hasLOS = !containerRayCast(%player.getMuzzlePoint($WeaponSlot), %targetObjectPos, %mask, 0);
   %playerVelocity = VectorLen(%player.getVelocity());
   %playerDamage = %player.getDamagePercent();
   if (%player.getMountedImage($WeaponSlot) > 0)
      %lastWeaponImage = %player.getMountedImage($WeaponSlot).getName(); // pinkpanther
   %enemyTeam        = (%client.team == 1) ? 2 : 1;
   %enemyFlag        = $AITeamFlag[%enemyTeam];
   %enemyFlagPos     = %enemyFlag.originalPosition;
   %targetObjectVect = getWord(%targetObjectPos, 0) SPC getWord(%targetObjectPos, 1) SPC "0";
   %enemyFlagVect    = getWord(%enemyFlagPos, 0) SPC getWord(%enemyFlagPos, 1) SPC "0";
   %enemyFlagDist    = VectorDist(%targetObjectVect, %enemyFlagVect);

   %hasBlaster = (%player.getInventory("Blaster") > 0) && %canUseEnergy && %hasLOS && %myEnergy > (%lastWeaponImage $= "BlasterImage" ? 0.1 : 0.5); //pinkpanther - 07/22/04
   %hasPlasma = (%player.getInventory("Plasma") > 0) && (%player.getInventory("PlasmaAmmo") > 0) && !%inWater;
   %hasChain = (%player.getInventory("Chaingun") > 0) && (%player.getInventory("ChaingunAmmo") > 0);
   %hasDisc = (%player.getInventory("Disc") > 0) && (%player.getInventory("DiscAmmo") > 0) && %hasLOS;
   %hasGrenade = (%player.getInventory("GrenadeLauncher") > 0) && (%player.getInventory("GrenadeLauncherAmmo") > 0) && !%inWater && %hasLOS;
   %hasSniper = (%player.getInventory("SniperRifle") > 0) && (%player.getInventory("EnergyPack") > 0) && %canUseEnergy && !%inWater && %hasLOS;
   %hasMortar = (%player.getInventory("Mortar") > 0) && (%player.getInventory("MortarAmmo") > 0) && !%inWater && %hasLOS;
   %hasRepairPack = (%player.getInventory("RepairPack") > 0) && %canUseEnergy;
   %hasRepairGun2 = (%player.getInventory("RepairGun2") > 0) && %canUseEnergy;
   %hasTargetingLaser = (%player.getInventory("TargetingLaser") > 0) && %canUseEnergy && !%inWater;
   //added missile shocklance and elf ------------------------------------------------------------- Lagg... 1-15-2003
   %hasShockLance = (%player.getInventory("ShockLance") > 0) && %canUseEnergy && !%inWater && %distToTarg < %hasLOSBasicShocker.extension;
   %hasELF = (%player.getInventory("ELFGun") > 0) && %canUseEnergy && !%inWater;
   // ----- Triumph specific weapons : pinkpanther 07/21/04  -----
   %hasSolarCharge      = %player.getInventory("SolarCharge") > 0 && %canUseEnergy && %distToTarg > 45 && %hasLOS; // you better have LOS with this ;-)
   %missileLauncherAmmo = %player.getInventory("MissileLauncherAmmo");
   %hornetAmmo          = %player.getInventory("HornetAmmo");
   %hasMissile          = %player.getInventory("MissileLauncher") > 0 && %missileLauncherAmmo > 0 && %distToTarg < 410;
   %hasHornet           = %player.getInventory("Hornet") > 0 && %hornetAmmo > 0 && %playerVelocity < 1.0 && %hasLOS && !%hasSolarCharge;
   //  Let's switch back and forth between missiles and hornets if we have both   - pinkpanther
   if (%hasHornet && %hasMissile) {
      if (%hornetAmmo > %missileLauncherAmmo) %hasMissile = false;
      else %hasHornet = false;
   }
   %hasAntiAirGun    = (%player.getInventory("AntiAirGun") > 0 && %player.getInventory("AntiAirGunAmmo") > 0) && (strstr(%targetName, "Deployed") < 0) && !%inWater;
   %hasDualBlaster   = (%player.getInventory("DualBlaster") > 0 && %canUseEnergy && %myEnergy > 0.1 && %hasLOS);
   %hasElfCannon     = (%player.getInventory("ElfCannon") > 0 && %canUseEnergy) && !%inWater;
   %hasGatlingGun    = (%player.getInventory("GatlingGun") > 0 && %player.getInventory("GatlingGunAmmo") > 0);
   %hasHeadHunter    = (%player.getInventory("HeadHunter") > 0) && (%player.getInventory("EnergyPack") > 0 && %canUseEnergy) && !%inWater;
   %hasHeavyLauncher = (%player.getInventory("HeavyLauncher") > 0 && %player.getInventory("HeavyLauncherAmmo") > 0 && %hasLOS);
   %hasNapalm        = (%player.getInventory("Napalm") > 0 && %myEnergy > (%lastWeaponImage $= "NapalmImage" ? 0.1 : 0.5) && (strstr(%targetName, "Sensor") < 0) && (strstr(%targetName, "TurretBase") < 0));    // No Napalm against sensors and base turrets!
   %hasRailCannon    = (%player.getInventory("RailCannon") > 0 && %player.getInventory("RailCannonAmmo") > 0 && !%inWater && %playerVelocity < 4);
   %hasRailNade      = (%player.getInventory("RailNade") > 0 && %player.getInventory("RailNadeAmmo") > 0 && %hasLOS);
   %hasGreenLantern  = false; //(%player.getInventory("GreenLantern") > 0 && %player.getInventory("GreenLanternAmmo") > 0 && %hasLOS && %distToTarg < GreenLanternBolt.damageRadius);
   %hasShotgun       = (%player.getInventory("Shotgun") > 0 && %player.getInventory("ShotgunAmmo") > 0);
   %hasSlugRifle     = (%player.getInventory("SlugRifle") > 0 && %player.getInventory("SlugRifleAmmo") > 0 && %hasLOS);
   %canUseSolarCharge= (%myEnergy > SolarChargeImage.fireEnergy/100) && (%playerVelocity < 1.0);
   %hasTitanChaingun = (%player.getInventory("TitanChaingun") > 0 && %player.getInventory("TitanChaingunAmmo") > 0);
   %hasMortarStorm   = (%player.getInventory("MortarStorm") > 0) && (%player.getInventory("MortarStormAmmo") > 0) && !%inWater && %hasLOS;
   %hasWidowMaker    = (%player.getInventory("WidowMaker") > 0) && (%player.getInventory("WidowMakerAmmo") > 0) && !%inWater && %distToTarg < $AIWidowMakerLaunchDistance +5;// && %enemyFlagDist < WidowMakerShot.damageRadius *1.33;
   %useWidowMaker    = !containerRayCast(%player.getMuzzlePoint($WeaponSlot), %targetObjectPos, $TypeMasks::TerrainObjectType, 0)
                     && AIManyEnemiesInRange(%client, %targetObjectPos, 20, WidowMakerShot.damageRadius, true, 1.2) > 2.5;//strstr(%targetName,"Turret") >= 0 && %distToTarg > 20;
   %hasTacNuke       = %player.getInventory("TacticalNuke") > 0;
   %dropTacNuke      = %hasTacNuke && ((%player.getInventory("WidowMaker") > 0 && %player.getInventory("WidowMakerAmmo") == 0) || %playerDamage > 0.7);

   //choose the weapon
   %client.objectWeapon = "NoAmmo";
//if (%hasBeamSword)   error (getTaggedString(%client.name) @ " %targetName - " @ %targetName @ " %mode - " @ %mode @ " %distToTarg - " @ %distToTarg SPC "%targetObject.getDataBlock().getClassName()"SPC %targetObject.getDataBlock().getClassName());
   //see if we're destroying the object -- added for remote inventories destruction -------- Lagg...1-12-2003
   if (%mode $= "Destroy" && !%targetIsVehicle)
   {
      //heavy modifications here could not list em all ------------------------------ Lagg... 1-21-2003
      // same goes for my modifications ;-)    ------------------ pinkpanther - 07/22/04
      if (%hasTacNuke && strstr(%targetName,"Deployed") == -1 && %targetName !$= "StationInventory" && %distToTarg < TacticalNukeThrown.damageRadius && %hasLOS) {
         %player.throwStrength = 1.0;
         %player.use("TacticalNuke");
//         error (getTaggedString(%client.name) SPC "TacticalNuke:Destroy" SPC %targetName SPC "Dist:" SPC %distToTarg);
      }

      // ZOD: Compresed this a little bit
      if (%targetObject.getDataBlock().getClassName() $= "TurretData" ||
           %targetName $= "DeployedStationInventory" || %targetName $= "DeployedMine")
      {
         if (%distToTarg < 8)
         {
            if (%hasBeamSword)
               %client.objectWeapon = "BeamSword";
            else if (%hasDualBlaster)
               %client.objectWeapon = "DualBlaster";
            else if (%hasPlasma)
               %client.objectWeapon = "Plasma";
            else if (%hasNapalm && %distToTarg > 4)
               %client.objectWeapon = "Napalm";
            else if (%hasRailCannon)
               %client.objectWeapon = "RailCannon";
            else if (%hasGreenLantern)
               %client.objectWeapon = "GreenLantern";
            else if (%hasAntiAirGun)
               %client.objectWeapon = "AntiAirGun";
            else if (%hasBlaster)
               %client.objectWeapon = "Blaster";
            else if (%hasShockLance)
               %client.objectWeapon = "ShockLance";
            else if (%hasTitanChaingun)
               %client.objectWeapon = "TitanChaingun";
            else if (%hasChain)
               %client.objectWeapon = "Chaingun";
            else if (%hasShotgun)
               %client.objectWeapon = "Shotgun";
//            else if (%hasGatlingGun) // GatlingGun is not accurate enough for destroying objects - pinkpanther
//               %client.objectWeapon = "GatlingGun";
            else if (%hasNapalm)
               %client.objectWeapon = "Napalm";
            else if (%hasDisc)
               %client.objectWeapon = "Disc";
            else if (%hasGrenade)
               %client.objectWeapon = "GrenadeLauncher";
         }
         else // ZOD: Is greater then 8 meters
         {
            if (%hasBeamSword)
               %client.objectWeapon = "BeamSword";
            else if (%hasSolarCharge && %canUseSolarCharge)
               %client.objectWeapon = "SolarCharge";
            else if (%hasDualBlaster)
               %client.objectWeapon = "DualBlaster";
            else if (%hasBlaster)
               %client.objectWeapon = "Blaster";
            else if (%hasRailCannon)
               %client.objectWeapon = "RailCannon";
            else if (%hasGreenLantern)
               %client.objectWeapon = "GreenLantern";
            else if (%hasPlasma && %distToTarg < 200)
               %client.objectWeapon = "Plasma";
            else if (%hasNapalm  && %distToTarg < 30)
               %client.objectWeapon = "Napalm";
            else if (%hasMortarStorm && %distToTarg > 140)
               %client.objectWeapon = "MortarStorm";
            else if (%hasAntiAirGun  && %distToTarg < 30)
               %client.objectWeapon = "AntiAirGun";
            else if (%hasDisc && %distToTarg > (%hasShotgun ?14:8))
               %client.objectWeapon = "Disc";
            else if (%hasMissile)
               %client.objectWeapon = "MissileLauncher";
            else if (%hasHornet)
               %client.objectWeapon = "Hornet";
            else if (%hasShockLance)
               %client.objectWeapon = "ShockLance";
            else if (%hasTitanChaingun)
               %client.objectWeapon = "TitanChaingun";
            else if (%hasChaingun)
               %client.objectWeapon = "Chaingun";
            else if (%hasShotgun && %distToTarg < 30)
               %client.objectWeapon = "Shotgun";
            else if (%hasMortar && !%hasHornet && %distToTarg > 20 && %distToTarg < 100)
               %client.objectWeapon = "Mortar";
            else if (%hasMortarStorm && %distToTarg > 80)
               %client.objectWeapon = "MortarStorm";
            else if (%hasRailNade)
               %client.objectWeapon = "RailNade";
            else if (%hasDisc)
               %client.objectWeapon = "Disc";
            else if (%hasGrenade)
               %client.objectWeapon = "GrenadeLauncher";
//            else if (%hasGatlingGun) // GatlingGun is not accurate enough for destroying objects - pinkpanther
//               %client.objectWeapon = "GatlingGun";
         }
      }
      else if (%distToTarg < 30)
      {
         if (%hasBeamSword)
            %client.objectWeapon = "BeamSword";
         else if (%hasDualBlaster)
            %client.objectWeapon = "DualBlaster";
         else if (%hasBlaster)
            %client.objectWeapon = "Blaster";
         else if (%hasGreenLantern)
            %client.objectWeapon = "GreenLantern";
         else if (%hasPlasma && %distToTarg > 3)
            %client.objectWeapon = "Plasma";
         else if (%hasNapalm && %distToTarg > 4)
            %client.objectWeapon = "Napalm";
         else if (%hasAntiAirGun)
            %client.objectWeapon = "AntiAirGun";
         else if (%hasHornet && %distToTarg > 9)
            %client.objectWeapon = "Hornet";
         else if (%hasRailCannon)
            %client.objectWeapon = "RailCannon";
         else if (%hasShockLance)
            %client.objectWeapon = "ShockLance";
         else if (%hasDisc && %distToTarg > (%hasShotgun ?14:8))
            %client.objectWeapon = "Disc";
         else if (%hasChain)
            %client.objectWeapon = "Chaingun";
         else if (%hasShotgun)
            %client.objectWeapon = "Shotgun";
         else if (%hasTitanChaingun)
            %client.objectWeapon = "TitanChaingun";
         else if (%hasRailCannon)
            %client.objectWeapon = "RailCannon";
         else if (%hasMissile)
            %client.objectWeapon = "MissileLauncher";
         else if (%hasSniper && %myEnergy > 0.8)
            %client.objectWeapon = "SniperRifle";
         else if (%hasGatlingGun) // GatlingGun is not accurate enough for destroying objects - pinkpanther
            %client.objectWeapon = "GatlingGun";
         else if (%hasNapalm)
            %client.objectWeapon = "Napalm";
         else if (%hasDisc)
            %client.objectWeapon = "Disc";
         else if (%hasGrenade)
            %client.objectWeapon = "GrenadeLauncher";
      } else {
         if (%hasDualBlaster && %distToTarg < 250)
            %client.objectWeapon = "DualBlaster";
         else if (%hasSolarCharge && %canUseSolarCharge)
            %client.objectWeapon = "SolarCharge";
         else if (%hasRailCannon)
            %client.objectWeapon = "RailCannon";
         else if (%hasPlasma && %distToTarg < 210)
            %client.objectWeapon = "Plasma";
         else if (%hasMortarStorm && %distToTarg > 140)
            %client.objectWeapon = "MortarStorm";
         else if (%hasMissile && %distToTarg < 600)
            %client.objectWeapon = "MissileLauncher";
         else if (%hasHornet && %distToTarg < 600)
            %client.objectWeapon = "Hornet";
         else if (%hasMortar && !%hasHornet && %distToTarg < 100)
            %client.objectWeapon = "Mortar";
         else if (%hasMortarStorm && %distToTarg > 80)
            %client.objectWeapon = "MortarStorm";
         else if (%hasDisc)
            %client.objectWeapon = "Disc";
         else if (%hasGrenade)
            %client.objectWeapon = "GrenadeLauncher";
         else if (%hasChain)
            %client.objectWeapon = "Chaingun";
      }
   }
   //else See if we're repairing the object
   else if (%mode $= "Repair")
   {
      if (%hasRepairGun2)
         %client.objectWeapon = "RepairGun2";
      else if (%hasRepairPack)
         %client.objectWeapon = "RepairPack";
   }
   //else see if we're lazing the object
   else if (%mode $= "Laze")
   {
      // Instead of just lazing allow the bots to fire directly at the target as long as they got an appropriate weapon
      // pinkpanther 07/22/04
      if (%hasBeamSword)
         %client.objectWeapon = "BeamSword";
      else if (%hasDualBlaster && %distToTarg < 250)
         %client.objectWeapon = "DualBlaster";
      else if (%hasSolarCharge && %canUseSolarCharge)
         %client.objectWeapon = "SolarCharge";
      else if (%hasRailCannon)
         %client.objectWeapon = "RailCannon";
      else if (%hasHornet && %distToTarg < 600)
         %client.objectWeapon = "Hornet";
      else if (%hasAntiAirGun && %distToTarg < 30)
         %client.objectWeapon = "AntiAirGun";
//      else if (%hasMissile)
//         %client.objectWeapon = "MissileLauncher";
      else if (%hasTargetingLaser && !(%hasSolarCharge || %hasHornet))
         %client.objectWeapon = "TargetingLaser";
   }
   //else see if we're mortaring the object -- Made some modifications here -- Lagg... 1-12-2003
      // Same here as above: Before mortaring, we prefer the better stuff. - pinkpanther 07/22/04
   else if (%mode $= "Mortar")
   {
      if (%dropTacNuke) if (AIManyEnemiesInRange(%client, %client.player.getWorldBoxCenter(), 10, TacticalNukeThrown.damageRadius) > 2.5 || (%distToTarg < TacticalNukeThrown.damageRadius && %hasLOS)){
         %player.throwStrength = 1.0;
         %player.use("TacticalNuke");
//         error (getTaggedString(%client.name) SPC "TacticalNuke:Mortar" SPC %targetName SPC "Dist:" SPC %distToTarg);
      }
      if (%hasMortarStorm) {
         %radius = 0.2 * %distToTarg + MortarStormShot.damageRadius;
         %hasMortarStorm = AIManyEnemiesInRange(%client, %targetObject.getWorldBoxCenter(), 2 + %distToTarg/MortarStormShot.muzzleVelocity, %radius) > 2.5 || %playerDamage > 0.5;
      }
      if (%hasWidowMaker && %useWidowMaker) {           // dropping a nuke, a suicide job...
         %client.objectWeapon = "WidowMaker";
//         error (getTaggedString(%client.name) SPC "WidowMaker:Mortar" SPC %targetName SPC "Dist:" SPC %distToTarg);
      }
      else if (%distToTarg > 6 && %distToTarg < 25 && !%inWater)
      {
          if (%hasBeamSword)
            %client.objectWeapon = "BeamSword";
          else if (%hasPlasma)
            %client.objectWeapon = "Plasma";
          else if (%hasTitanChaingun)
            %client.objectWeapon = "TitanChaingun";
          else if (%hasAntiAirGun)
            %client.objectWeapon = "AntiAirGun";
          else if (%hasRailCannon)
            %client.objectWeapon = "RailCannon";
          else if (%hasNapalm)
            %client.objectWeapon = "Napalm";
          else if (%hasShotgun)
            %client.objectWeapon = "Shotgun";
          else if (%hasDualBlaster)
            %client.objectWeapon = "DualBlaster";
          else if (%hasBlaster)
            %client.objectWeapon = "Blaster";
          else if (%hasGreenLantern)
            %client.objectWeapon = "GreenLantern";
          else if (%hasHornet)
            %client.objectWeapon = "Hornet";
          else if (%hasMissile)
            %client.objectWeapon = "MissileLauncher";
          else if (%hasChain)
            %client.objectWeapon = "Chaingun";
          else if (%hasDisc)
            %client.objectWeapon = "Disc";
          else if (%hasGatlingGun) // GatlingGun is not accurate enough for destroying objects - pinkpanther
            %client.objectWeapon = "GatlingGun";
      }
      else if (%distToTarg < 140 && !%inWater)
      {
          if (%hasSolarCharge && %canUseSolarCharge)
            %client.objectWeapon = "SolarCharge";
          else if (%hasRailCannon)
            %client.objectWeapon = "RailCannon";
          else if (%hasMortarStorm && %distToTarg > 80)
            %client.objectWeapon = "MortarStorm";
          else if (%hasPlasma)
            %client.objectWeapon = "Plasma";
          else if (%hasMissile)
            %client.objectWeapon = "MissileLauncher";
          else if (%hasGreenLantern)
            %client.objectWeapon = "GreenLantern";
          else if (%hasHornet)
            %client.objectWeapon = "Hornet";
          else if (%hasMortar)
            %client.objectWeapon = "Mortar";
          else if (%hasDualBlaster)
            %client.objectWeapon = "DualBlaster";
          else if (%hasTitanChaingun)
            %client.objectWeapon = "TitanChaingun";
          else if (%hasDisc)
            %client.objectWeapon = "Disc";
          else if (%hasRailNade)
            %client.objectWeapon = "RailNade";
          else if (%hasBlaster)
            %client.objectWeapon = "Blaster";
          else if (%hasNapalm)
            %client.objectWeapon = "Napalm";
      }
      else if (%distToTarg < 120 && %inWater)
      {
          if (%hasDualBlaster)
            %client.objectWeapon = "DualBlaster";
          else if (%hasSolarCharge && %canUseSolarCharge)
            %client.objectWeapon = "SolarCharge";
          else if (%hasHornet)
            %client.objectWeapon = "Hornet";
          else if (%hasMissile)
            %client.objectWeapon = "MissileLauncher";
          else if (%hasDisc)
            %client.objectWeapon = "Disc";
          else if (%hasChain)
            %client.objectWeapon = "Chaingun";
          else if (%hasBlaster)
            %client.objectWeapon = "Blaster";
          else if (%hasTitanChaingun)
            %client.objectWeapon = "TitanChaingun";
          else if (%hasGatlingGun) // GatlingGun is not accurate enough for destroying objects - pinkpanther
            %client.objectWeapon = "GatlingGun";
      }
      else if (%distToTarg >= 120 && %inWater)
      {
          if (%hasDualBlaster)
            %client.objectWeapon = "DualBlaster";
          else if (%hasSolarCharge && %canUseSolarCharge)
            %client.objectWeapon = "SolarCharge";
          else if (%hasHornet)
            %client.objectWeapon = "Hornet";
          else if (%hasMissile)
            %client.objectWeapon = "MissileLauncher";
          else if (%hasDisc)
            %client.objectWeapon = "Disc";
          else if (%hasBlaster)
            %client.objectWeapon = "Blaster";
      }
      else if (%distToTarg < 300 && !%inWater)
      {
          if (%hasSolarCharge && %canUseSolarCharge)
            %client.objectWeapon = "SolarCharge";
          else if (%hasPlasma && %distToTarg < 210)
            %client.objectWeapon = "Plasma";
          else if (%hasMortarStorm)
            %client.objectWeapon = "MortarStorm";
          else if (%hasHornet)
            %client.objectWeapon = "Hornet";
          else if (%hasDualBlaster)
            %client.objectWeapon = "DualBlaster";
          else if (%hasRailCannon)
            %client.objectWeapon = "RailCannon";
          else if (%hasMortar)
            %client.objectWeapon = "Mortar";
          else if (%hasBlaster)
            %client.objectWeapon = "Blaster";
          else if (%hasMissile)
            %client.objectWeapon = "MissileLauncher";
          else if (%hasDisc)
            %client.objectWeapon = "Disc";
      }
      else if (%distToTarg >= 300 && !%inWater)
      {
          if (%hasSolarCharge && %canUseSolarCharge)
            %client.objectWeapon = "SolarCharge";
          else if (%hasHornet)
            %client.objectWeapon = "Hornet";
          else if (%hasMissile)
            %client.objectWeapon = "MissileLauncher";
          else if (%hasRailCannon)
            %client.objectWeapon = "RailCannon";
          else if (%hasDisc)
            %client.objectWeapon = "Disc";
      }
   }
   //  Destroying a vehicle -- used for vehicles which are standing still
   else if (%mode $= "Destroy" && %targetIsVehicle)
   {
      if (%hasAntiAirGun)
         %client.objectWeapon = "AntiAirGun";
      else if (%hasSolarCharge && %canUseSolarCharge)
         %client.objectWeapon = "SolarCharge";
      else if (%hasRailCannon)
         %client.objectWeapon = "RailCannon";
      else if (%hasTitanChaingun)
         %client.objectWeapon = "TitanChaingun";
      else if (%hasGatlingGun)
         %client.objectWeapon = "GatlingGun";
      else if (%hasHornet && %distToTarg > 9)
         %client.objectWeapon = "Hornet";
      else if (%hasMissile)
         %client.objectWeapon = "MissileLauncherDumbfire";
      else if (%hasChain)
         %client.objectWeapon = "Chaingun";
      else if (%hasPlasma && %distToTarg > 3)
         %client.objectWeapon = "Plasma";
      else if (%hasDualBlaster)
         %client.objectWeapon = "DualBlaster";
      else if (%hasGreenLantern)
         %client.objectWeapon = "GreenLantern";
   }
   else if (%mode $= "Missile" && %targetIsVehicle)  // otherwise we missile the vehicle in case of higher speed
   {
      if (%hasMissile)
         %client.objectWeapon = "MissileLauncher";
      else if (%hasAntiAirGun)
         %client.objectWeapon = "AntiAirGun";
      else if (%hasRailCannon)
         %client.objectWeapon = "RailCannon";
      else if (%hasTitanChaingun)
         %client.objectWeapon = "TitanChaingun";
      else if (%hasGatlingGun)
         %client.objectWeapon = "GatlingGun";
      else if (%hasChain)
         %client.objectWeapon = "Chaingun";
      else if (%hasDualBlaster)
         %client.objectWeapon = "DualBlaster";
      else if (%hasHornet && %distToTarg > 9)
         %client.objectWeapon = "Hornet";
   }
   //else see if we're missiling the object
   else if ((%mode $= "Missile" || %mode $= "MissileNoLock") && !%targetIsVehicle)
   {
      if (%distToTarg > 40)
      {
          if (%hasMissile)
            %client.objectWeapon = "MissileLauncher";
          else if (%hasSolarCharge)
            %client.objectWeapon = "SolarCharge";
      }
      else if (%hasHornet)
         %client.objectWeapon = "Hornet";
   }

   if (strstr(%client.objectWeapon, "Repair") < 0) if (%targetObject.getDamagePercent() > 0.98) if (strstr(%targetName,"Deployed") < 0) %client.objectWeapon = "NoAmmo";
//   if (%hasBeamSword) echo (getTaggedString(%client.name) SPC %client.objectWeapon);
   //now select the weapon
//if (%player.getInventory("WidowMaker") > 0) if (%player.getInventory("WidowMakerAmmo") > 0) { %client.objectWeapon = "WidowMaker"; }
//if (%targetName $= "DeployedForceField" || %targetName $= "Deployedwall") error(getTaggedString(%client.name) SPC %targetObject SPC %mode SPC %client.objectWeapon SPC "objWpn:dist" SPC %distToTarg SPC "targName" SPC %targetName SPC "LOS" SPC %hasLOS SPC "Ch Di Gr" SPC %hasChain SPC %hasDisc SPC %hasGrenade);
   switch$ (%client.objectWeapon)
   {
      case "Blaster":
         %client.player.use("Blaster");
         %client.setWeaponInfo("EnergyBolt", 0, 150, 1, 0.1);

      case "Plasma":
         %client.player.use("Plasma");
         %client.setWeaponInfo("PlasmaBolt", 1, 50);

      case "Chaingun":
         %client.player.use("Chaingun");
         %client.setWeaponInfo("ChaingunBullet", 0, 75, 150);

      case "Disc":
         %client.player.use("Disc");
         %client.setWeaponInfo("DiscProjectile", 0, 350);

      case "GrenadeLauncher":
         %client.player.use("GrenadeLauncher");
         %client.setWeaponInfo("BasicGrenade", 0, 75);

      case "Mortar":
         %client.player.use("Mortar");
         %client.setWeaponInfo("MortarShot", 4, 300);

      case "SniperRifle":
         %client.player.use("SniperRifle");
         %client.setWeaponInfo("BasicSniperShot", 40, 2000, 1, 0.75, 0.5);

      case "RepairPack"://added a little fix here - Lagg... 4-9-2003
	   if (%player.getImageState($BackpackSlot) $= "Idle")
	      %client.player.use("RepairPack");
         if (%player.getImageState($BackpackSlot) $= "Activate")
         {
            if(%client.player.getMountedImage($WeaponSlot).getName() !$= "RepairGunImage")
               %client.player.use("RepairPack");

            %client.setWeaponInfo("DefaultRepairBeam", 0, 75, 300, 0.1);
         }

         //if ( !%client.player.getImageTrigger( $BackpackSlot ) )
            //%client.player.setImageTrigger( $BackpackSlot, true );
         //if ( %client.player.getImageTrigger( $BackpackSlot ) )
            //%client.setWeaponInfo("DefaultRepairBeam", 40, 75, 300, 0.1);

      case "RepairGun2":
          %client.player.use("RepairGun2");
          %client.setWeaponInfo("RepairGun2RepairBeam", 40, 75, 300, 0.1);

      case "TargetingLaser":
         %client.player.use("TargetingLaser");
         %client.setWeaponInfo("BasicTargeter", 20, 500, 300, 0.1);

      case "MissileLauncher":
         %client.player.use("MissileLauncher");
          if (%hasSolarCharge || %hasHornet)   //  || getRandom() > 0.6
             %client.setWeaponInfo("ShoulderMissile", 0, 600);  // Locked missile mode
          else
             %client.setWeaponInfo("DumbFire", 0, 600);  // DumbFire mode

      case "MissileLauncherDumbfire":
         %client.player.use("MissileLauncher");
         %client.setWeaponInfo("DumbFire", 7, 350);

      case "ELFGun":
         %client.player.use("ELFGun");
         %client.setWeaponInfo("BasicELF", 25, 45, 90, 0.1);

      case "ShockLance":
         %client.player.use("ShockLance");
         %client.setWeaponInfo("BasicShocker", 0.5, BasicShocker.extension, 1, 0.1);

   // ----- Triumph specific weapons here: pinkpanther 07/21/04  -----
   // I'm not sure at all about the %client.setWeaponInfo() parameters here!!!

      case "AntiAirGun":
         %client.player.use("AntiAirGun");
         %client.setWeaponInfo("AntiAirGunBolt", 0, 300);

      case "BeamSword":
         %client.player.use("BeamSword");
         %client.setWeaponInfo("LightBeam", 0.5, 28, 100);
//         %client.setWeaponInfo("BeamSwordFakeProj", 1, 18);

      case "DualBlaster":
         %client.player.use("DualBlaster");
         %client.setWeaponInfo("DualEnergyBolt", 1, 250, 1, 0.1);

      case "ElfCannon":
         %client.player.use("ElfCannon");
         %client.setWeaponInfo("ELFTurretBolt", 5, 75, 175, 0.1);

      case "GatlingGun":
         %client.player.use("GatlingGun");
         %client.setWeaponInfo("GatlingGunBullet", 0, 100, 250);

//       case "GreenLantern":
//          %client.player.use("GreenLantern");
//          %client.setWeaponInfo("GreenLanternBolt", 3, 40, 1);
//          echo (getTaggedString(%client.name) SPC "GreenLantern");
//          %client.setDangerLocation(%targetObject.getWorldBoxCenter(), 1.5*GreenLanternBolt.damageRadius);  // pinkpanther

      case "HeadHunter":
         %client.player.use("HeadHunter");
         %client.setWeaponInfo("HeadHunterSniperShot", 5, 2000, 1, 0.75);

      case "HeavyLauncher":
         %client.player.use("HeavyLauncher");
         %client.setWeaponInfo("HeavyGrenade", 20, 175);

      case "Hornet":
//         %client.aimAt(%targetClient, 4000);
         %client.player.use("Hornet");
         %client.setWeaponInfo("HornetProjectile", 10, 600);

      case "Napalm":
         %client.player.use("Napalm");
         %client.setWeaponInfo("NapalmBolt", 0, 60);

      case "RailCannon":
         %client.player.use("RailCannon");
         %client.setWeaponInfo("RailCannonBolt", 0, 1000);

      case "RailNade":
         %client.player.use("RailNade");
         %client.setWeaponInfo("RailNadeProjectile", 0, 175);

      case "SlugRifle":
         %client.player.use("SlugRifle");
         %client.setWeaponInfo("SlugRifleBullet", 10, 3000);

      case "Shotgun":
         %client.player.use("Shotgun");
         %client.setWeaponInfo("ShotgunBullet", 0, 40);

      case "SolarCharge":
         %client.player.use("SolarCharge");
         %client.setWeaponInfo("SolarChargeBolt", 50, 800);

      case "TitanChaingun":
         %client.player.use("TitanChaingun");
         %client.setWeaponInfo("TitanChaingunBullet", 0, 30, 100);

      case "MortarStorm":
         %client.player.use("MortarStorm");
         %client.setWeaponInfo("MortarStormShot", 40, 600);
         %client.setDangerLocation(%targetObjectPos, %distToTarg +MortarStormShot.damageRadius);

      case "WidowMaker":
         %client.player.use("WidowMaker");
         %client.setWeaponInfo("WidowMakerShot", 2, 600);
         %client.aimAt(%targetObject, 15000);
         %client.setDangerLocation(%targetObjectPos, %distToTarg +WidowMakerShot.damageRadius);

      case "NoAmmo":
         %client.setWeaponInfo("NoAmmo", 30, 75);
   }
}

//------------------------------------------- pinkpanther
// How are the odds of a linear projectile to hit its target depending on the several factors in the parameter list:
function AIWeighLinearProjectile(%factor, %distToTarg, %targVelocity, %projVelocity, %targVelocityTolerance) {
   return %factor/(1 + %targVelocity * %distToTarg * (%distToTarg + %targVelocity)/(%projVelocity * %targVelocityTolerance));
}
// Certain weapons spread their linear projectiles. This results in a damage reduction that is quadratic over distance.
function AIWeighSpread(%factor, %distToTarg, %targVelocity, %spread, %projVelocity, %closeRangeMultiplier) {
   %closeRange = 200/%spread;   // Approx.
   return AIWeighLinearProjectile(%factor, %distToTarg, %targVelocity, %projVelocity, 1500)
      * ((%distToTarg < %closeRange) ? %closeRangeMultiplier : (%closeRange * %closeRange / (%distToTarg * (%distToTarg + %targVelocity))));
}
// Probability of hitting a target in the splash damage zone on the ground (thus applying splash damage) instead of in the air.
function AIWeighSplash(%enemyAlt, %distToTarg, %targetZDiff, %targetZVel, %projVelocity, %damageRadius, %ceilingAltitude, %ceilingValue) {
   if (mAbs(%targetZVel) < 0.3) return 1 - %targetZDiff / %distToTarg;
   %enemyImpactAlt = %enemyAlt -3 + %distToTarg * %targetZVel / %projVelocity;
   if (%enemyImpactAlt < 0) %enemyImpactAlt = -0.05 * %enemyImpactAlt;
   if (%enemyImpactAlt > %ceilingAltitude)
      return %ceilingValue /(1 + %ceilingAltitude / %damageRadius);
   else {
      %tanPhi = %targetZDiff / %distToTarg;
      if (%tanPhi < 0) %tanPhi = 0;
      return (1 - %tanPhi) /(1 + %enemyImpactAlt / %damageRadius);
   }
}

//most of it rewritten   - pinkpanther
function AIChooseEngageWeapon(%client, %targetClient, %distToTarg, %canUseEnergyStr, %environmentStr)
{
   //get some status
   %player = %client.player;
   if (!isObject(%player))
      return;

   %enemy = %targetClient.player;
   if (!isObject(%enemy))
      return;

   %clientPos = %player.getWorldBoxCenter();
   %enemyPos = %targetClient.player.getWorldBoxCenter();

   //added a check to call function for bots in vehicle - Lagg...
   if (%client.player.isMounted())
   {
      AIProcessVehicle(%client);//added this call to help bomber/gunner bot - Lagg...
      //check for LOS
      %vehicleLOS = "false";
      %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType | $TypeMasks::VehicleObjectType;
      %vehicleLOS = !containerRayCast(%client.player.getWorldBoxCenter(), %enemyPos, %mask, 0);
      if (! %vehicleLos)
      {
         //clear the pressFire
	   %client.pressFire(-1);
         %client.setEngageTarget(-1);
         return;
      }
   }

   %canUseEnergy = (%canUseEnergyStr $= "true");
   %inWater = (%environmentStr $= "water");
   %outdoors = (%environmentStr $= "outdoors");

   %distanceVector = VectorSub(%enemy.getTransform(), %player.getTransform());
   %distance = VectorLen(%distanceVector);
   %velocityDiffVector = VectorSub(%enemy.getVelocity(), %player.getVelocity());
   %newDistanceVector = VectorAdd(%distanceVector, %velocityDiffVector);
   %newDistance = VectorLen(%newDistanceVector);
   %targRadialVelocity = %newDistance - %distance;
   %targTangentialVelocityVector = VectorSub(%newDistanceVector, VectorScale(%distanceVector, %newDistance/%distance));
   %targTangentialVelocity = VectorLen(%targTangentialVelocityVector);

   %targVelocityVector = %enemy.getVelocity();
   %targVelocity = VectorLen(VectorSub(%targVelocityVector, %player.getVelocity()));
   %targEnergy = %targetClient.player.getEnergyPercent();
   %targDamage = %targetClient.player.getDamagePercent();
//   %playerVelocity = VectorLen(%player.getVelocity());
   %myEnergy = %player.getEnergyPercent();
   %myDamage = %player.getDamagePercent();
   %damageIncrease = %myDamage - %player.lastDamage;
   %player.lastDamage = %myDamage;
//   %hasLOS = %client.hasLOSToClient(%targetClient);//added - Lagg...
   %hasBeamSword     = %player.getInventory("BeamSword") > 0 && %distToTarg < LightBeam.maxRifleRange && %canUseEnergy && %myEnergy > BeamSwordImage.minEnergy;
   %mask = $LOSMask - (%hasBeamSword ? $TypeMasks::ForceFieldObjectType : 0);
   %hasLOS = !containerRayCast(%player.getMuzzlePoint($WeaponSlot), %enemyPos, $mask, 0);
   %myArmor = %player.getArmorSize();//added - Lagg...
   %targIsPilot = %enemy.isMounted() > 0;
   if (%player.getMountedImage($WeaponSlot) > 0)
      %lastWeaponImage = %player.getMountedImage($WeaponSlot).getName(); // pinkpanther
   %enemyArmor   = %targetClient.player.dataBlock;
   %enemyIsRecon = (strstr(%enemyArmor, "Recon") > -1);
   %enemyIsSniper= (strstr(%enemyArmor, "Sniper") > -1);
   %enemyIsLight = (strstr(%enemyArmor, "Light") > -1);
   %enemyIsHeavy = (strstr(%enemyArmor, "Heavy") > -1);
   %enemyIsTitan = (strstr(%enemyArmor, "Titan") > -1);
   %enemyInLightArmor = %enemyIsLight || %enemyIsSniper || %enemyIsRecon;
   %enemyInHeavyArmor = %enemyIsHeavy || %enemyIsTitan;
   %inEnemyLand = VectorDist(%clientPos, $AITeamFlag[%enemyTeam].originalPosition) < VectorDist(%clientPos, $AITeamFlag[%client.team].originalPosition);
   //added this here to fix for elf gun - Lagg...
   %speed = VectorLen(%targVelocityVector);
   %myZ = getWord(%player.getTransform(), 2);
   %targetZ = getWord(%enemy.getTransform(), 2);
   %targetZVel = getWord(%targVelocityVector, 2);

   //find out the enemy's altitude - lagg...
   %downVect = getWord(%enemyPos, 0) SPC getWord(%enemyPos, 1) SPC "0";
   %altSur = containerRayCast(%enemyPos, %downVec, $LOSMask, 0);
   %altPos = posfromRayCast(%altSur);
   %enemyAlt = getWord(%enemyPos, 2) - getWord(%altPos, 2);
   //lets get the enemy's heat
   %targetHeat = %targetClient.player.getHeat();

   %client.enviroment = %inwater; // Capto Lamia for this one
   //%client.enviromental = %outdoors;// not used standing on a roof counts as indoors :(

   // find out best weapon   -   complete rewrite here based on formulas, lightning fast code   -   pinkpanther
   %client.engageWeapon = "NoAmmo";
   %client.engageWeaponWeight = 0;
// Weapons with no projectile degradation over distance
   if (%player.getInventory("ELFGun") > 0) if (%canUseEnergy && !%inWater && %distToTarg < 60 && %myEnergy > 0.1 && %myDamage <= %targDamage && %targetZ - %myZ > 10) {
      %weaponWeight =  6 * %targEnergy ;
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "ELFGun"; }
   }
   if (%player.getInventory("ElfCannon") > 0) if (%canUseEnergy && !%inWater && %distToTarg < 120 && %myEnergy > 0.2) {
      %weaponWeight =  (%targEnergy > 0.1 ? (%enemy.holdingFlag ? 8 : 0.5) : 0.1) * (1 - AIWeighSplash(%enemyAlt, %distToTarg, %targetZ - %myZ, %targetZVel, 42, 15, 18, 0));
      %weaponWeight *=  (%enemyInLightArmor ? 2.5 : (%enemyInHeavyArmor ? 0.4 : 1));
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "ElfCannon"; }
   }
   if (%player.getInventory("BeamSword") > 0) if (%canUseEnergy && %myEnergy > BeamSwordImage.minEnergy && %distToTarg < LightBeam.maxRifleRange) {
      %weaponWeight =  7 *  ((%distToTarg < 10) ? 1 : (0.02 * %distToTarg * (LightBeam.maxRifleRange - %distToTarg)))/(1 + %targVelocity/12 + 5 * %myDamage);
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "BeamSword"; }
   }
   if (%player.getInventory("ShockLance") > 0) if (%canUseEnergy && !%inWater && %distToTarg < BasicShocker.extension) {
      %weaponWeight =  6 * 1/(1 + %targVelocity/15 + %distToTarg * %distToTarg / 400) ;
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "ShockLance"; }
   }
// Linear projectile weapons with no significant splash damage
   if (%player.getInventory("Plasma") > 0) if (%player.getInventory("PlasmaAmmo") > 0 && %hasLOS && !%inWater && %distToTarg < 150) {  // && %distToTarg > PlasmaBolt.damageRadius
      %weaponWeight = AIWeighLinearProjectile(1.0, %distToTarg, %targTangentialVelocity, 75, 300) ;
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "Plasma"; }
   }
   if (%player.getInventory("Blaster") > 0) if (%hasLOS && %canUseEnergy && %myEnergy > (%lastWeaponImage $= "BlasterImage" ? 0.1 : 0.5) && %distToTarg < 200) {
      %weaponWeight = AIWeighLinearProjectile(1.0, %distToTarg, %targTangentialVelocity, (%inWater?EnergyBolt.wetVelocity:EnergyBolt.dryVelocity), 200);
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "Blaster"; }
   }
   if (%player.getInventory("DualBlaster") > 0) if (%canUseEnergy && %hasLOS && %distToTarg < 250) {
      %weaponWeight =  AIWeighLinearProjectile(1.2, %distToTarg, %targTangentialVelocity, (%inWater?DualEnergyBolt.wetVelocity:DualEnergyBolt.dryVelocity), 200) ;
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "DualBlaster"; }
   }
   if (%player.getInventory("RailCannon") > 0) if (%player.getInventory("RailCannonAmmo") > 0 && %hasLOS && !%inWater && %distToTarg < 1000) {
      %weaponWeight =  AIWeighLinearProjectile((%targIsPilot ? 15 : 0.3), %distToTarg, %targTangentialVelocity, 1500, 800) ;
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "RailCannon"; }
   }
   if (%player.getInventory("SlugRifle") > 0) if (%player.getInventory("SlugRifleAmmo") > 0 && %hasLOS && !%targIsPilot && %distToTarg < 1000) {
      %weaponWeight =  AIWeighLinearProjectile((%enemy.holdingFlag ? 8 : 1.4), %distToTarg, %targTangentialVelocity, 8000, 10) ;
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "SlugRifle"; }
   }
   if (%player.getInventory("SniperRifle") > 0) if (%canUseEnergy && !%inWater && !%targIsPilot && %hasLOS && %myEnergy > 0.75 && %distToTarg < 450) {
      %weaponWeight =  AIWeighLinearProjectile((%enemy.holdingFlag ? 4 : 1.2), %distToTarg, %targTangentialVelocity, 8000, 20) ;
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "SniperRifle"; }
   }
   if (%player.getInventory("HeavyLaser") > 0) if (%canUseEnergy && !%inWater && !%targIsPilot && %myEnergy > 0.75 && %distToTarg < 1000) {
      %weaponWeight =  (!%hasLOS || (%lastWeaponImage $= "HeavyLaserImage" && %player.getImageState($WeaponSlot) $= "Delay") ? 50 : AIWeighLinearProjectile(%distToTarg/40 + 1, %distToTarg, %targTangentialVelocity, 8000, 20)) ;
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "HeavyLaser"; }
//error(getTaggedString(%client.name) SPC "HeavyLaser" SPC %distToTarg SPC %lastWeaponImage SPC %player.getImageState($WeaponSlot) SPC "weaponWeight" SPC %weaponWeight);
   }
   if (%player.getInventory("HeadHunter") > 0) if (%player.getInventory("EnergyPack") > 0 && %canUseEnergy && %hasLOS && !%inWater && !%targIsPilot && %distToTarg < 1000) {
      %weaponWeight =  AIWeighLinearProjectile(1.2, %distToTarg, %targTangentialVelocity, 8000, 15) ;
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "HeadHunter"; }
   }
//   Weapons w/projectileSpread:
   if (%player.getInventory("Napalm") > 0) if (%canUseEnergy && %myEnergy > (%lastWeaponImage $= "NapalmImage" ? 0.1 : 0.3) && %hasLOS && !%inWater && %distToTarg > NapalmBolt.damageRadius && %distToTarg < (%inWater?50:60)) {
      %weaponWeight = AIWeighSpread((%enemyInLightArmor ? 12: 8), %distToTarg, %targTangentialVelocity, 14, (%inWater?45:50), 1);
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "Napalm"; }
   }
   if (%player.getInventory("Shotgun") > 0) if (%player.getInventory("ShotgunAmmo") > 0 && %hasLOS && %distToTarg < (%inWater?100:450)) {
      %weaponWeight = AIWeighSpread((%enemyInLightArmor ? 2.5: 4.5), %distToTarg, %targVelocity, 18, (%inWater?100:425), 1.8);
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "Shotgun"; }
   }
   if (%player.getInventory("Chaingun") > 0) if (%player.getInventory("ChaingunAmmo") > 0 && %distToTarg < (%inWater?425:600)) {
      %weaponWeight = AIWeighSpread((%targIsPilot ? 6 : 2.5), %distToTarg, %targTangentialVelocity, 7, (%inWater?410:600), 1);
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "Chaingun"; }
   }
   if (%player.getInventory("GatlingGun") > 0) if (%player.getInventory("GatlingGunAmmo") > 0 && %hasLOS && %distToTarg < (%inWater?725:825)) {
      %weaponWeight = AIWeighSpread((%targIsPilot ? 4 : 1.4), %distToTarg, %targTangentialVelocity, 3, (%inWater?725:825), 1);
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "GatlingGun"; }
   }
   if (%player.getInventory("TitanChaingun") > 0) if (%player.getInventory("TitanChaingunAmmo") > 0 && (%hasLOS || %distToTarg < 30) && %distToTarg < (%inWater?400:500)) {
      %weaponWeight = AIWeighSpread((%enemyInLightArmor ? 12: 20), %distToTarg, %targTangentialVelocity, 18, (%inWater?400:500), 1);
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "TitanChaingun"; }
   }
   if (%player.getInventory("AntiAirGun") > 0 ) if (%player.getInventory("AntiAirGunAmmo") > 0 && !%inWater && %hasLOS && %distToTarg < 1000) {
      %weaponWeight = AIWeighSpread((%targIsPilot ? 40 : 0.3), %distToTarg, %targTangentialVelocity, 7, AntiAirGunBolt.dryVelocity, 1);
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "AntiAirGun"; }
   }
//   Missile Launcher
   if (%player.getInventory("MissileLauncher") > 0) if (%player.getInventory("MissileLauncherAmmo") > 0 && !%inWater && %distToTarg > ShoulderMissile.damageRadius) {
      if ((%targetHeat > 0.7 || %targIsPilot) && %distToTarg < 400) {                // Heat lock
         %client.aimAt(%targetClient, 3000);
         %weaponWeight = (%targIsPilot ? 5 : 4);
         if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "MissileLauncher"; }
//error(getTaggedString(%client.name) SPC "Missile" SPC %distToTarg SPC "targetHeat" SPC %targetHeat SPC "weaponWeight" SPC %weaponWeight);
      } else if (%distToTarg < 0.001 * DumbFire.lifetimeMS * (%inWater?DumbFire.wetVelocity:DumbFire.dryVelocity) && %hasLOS) {                     // Dumb fire
         %splash = AIWeighSplash(%enemyAlt, %distToTarg, %targetZ - %myZ, %targetZVel, (%inWater?75:85), 7, 8, 1);
         %weaponWeight = AIWeighLinearProjectile(8/(1+%distToTarg*%distToTarg/400), %distToTarg, %targTangentialVelocity, (%inWater?75:85), 300 * %splash *%splash *%splash);
         if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "MissileLauncherDumbfire"; }
//echo(getTaggedString(%client.name) SPC "Dumbfire" SPC %distToTarg SPC "splash" SPC %splash SPC "weaponWeight" SPC %weaponWeight SPC %client.engageWeapon);
      }
   }
// Linear projectile weapons with splash damage
   if (%player.getInventory("Disc") > 0) if (%player.getInventory("DiscAmmo") > 0 && %distToTarg > DiscProjectile.damageRadius && %distToTarg < 600) {
      %splash = AIWeighSplash(%enemyAlt, %distToTarg, %targetZ - %myZ, %targetZVel, (%inWater?DiscProjectile.wetVelocity:DiscProjectile.dryVelocity), 8, 9, 1);
      %weaponWeight = AIWeighLinearProjectile(1.4, %distToTarg, %targTangentialVelocity, (%inWater?DiscProjectile.wetVelocity:DiscProjectile.dryVelocity), 6000 * %splash *%splash *%splash) ;
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "Disc"; }
   }
   if (%player.getInventory("RailNade") > 0) if (%player.getInventory("RailNadeAmmo") > 0 && %hasLOS && %distToTarg > RailNadeProjectile.damageRadius && %distToTarg < 300) {
      %splash = AIWeighSplash(%enemyAlt, %distToTarg, %targetZ - %myZ, %targetZVel, (%inWater?65:85), 10, 12, 1);
      %weaponWeight = AIWeighLinearProjectile((%enemyInLightArmor ? 2.8 : 2.1), %distToTarg, %targTangentialVelocity, (%inWater?65:85), 7000 * %splash *%splash *%splash) ;
      if (%distToTarg < 20) %weaponWeight = %weaponWeight * %distToTarg / 20;
//echo(getTaggedString(%client.name) SPC "RailNade:distToTarg" SPC %distToTarg SPC "targTangentialVelocity" SPC %targTangentialVelocity SPC "hitAlt" SPC %enemyAlt + %distToTarg * %targetZVel / 75 SPC "splash" SPC %splash SPC "weaponWeight" SPC %weaponWeight);
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "RailNade"; }
   }
   if (%player.getInventory("SolarCharge") > 0) if (%canUseEnergy && %myEnergy > SolarChargeImage.fireEnergy/100 && %hasLOS && %outdoors && %distToTarg > 1.2*SolarChargeBolt.damageRadius && %distToTarg < 800) {
      %splash = AIWeighSplash(%enemyAlt, %distToTarg, %targetZ - %myZ, %targetZVel, SolarChargeBolt.dryVelocity, SolarChargeBolt.damageRadius, 1.2*SolarChargeBolt.damageRadius, 1);
      %weaponWeight = AIWeighLinearProjectile(5, %distToTarg, %targTangentialVelocity, SolarChargeBolt.dryVelocity, 10000 * %splash *%splash *%splash);
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "SolarCharge"; }
   }
   if (%player.getInventory("Hornet") > 0) if (%player.getInventory("HornetAmmo") > 0 && %hasLOS && %distToTarg > HornetProjectile.damageRadius && %distToTarg < 600) {
      %splash = AIWeighSplash(%enemyAlt, %distToTarg, %targetZ - %myZ, %targetZVel, (%inWater?100:110), 8, 9, 1);
      %weaponWeight = AIWeighLinearProjectile((%targIsPilot ? 3 : 2.3), %distToTarg, %targTangentialVelocity, (%inWater?100:110), 2400 * %splash *%splash *%splash);
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "Hornet"; }
//echo(getTaggedString(%client.name) SPC "Hornet" SPC %distToTarg SPC "splash" SPC %splash SPC "weaponWeight" SPC %weaponWeight SPC %client.engageWeapon);
   }
//   Grenades/Mortars:
   if (%player.getInventory("GrenadeLauncher") > 0) if (%player.getInventory("GrenadeLauncherAmmo") > 0 && !%inWater && %distToTarg > 15 && %distToTarg < 200) {
      %weaponWeight =  3 * AIWeighLinearProjectile(AIWeighSplash(%enemyAlt, %distToTarg, %targetZ - %myZ, %targetZVel, 75, 12, 15, 0), %distToTarg, %targTangentialVelocity, 75, 5000) ;
      if (%distToTarg < 40 && %enemy.holdingFlag == 0) %weaponWeight = %weaponWeight * %distToTarg / 40;
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "GrenadeLauncher"; }
   }
   if (%player.getInventory("HeavyLauncher") > 0) if (%player.getInventory("HeavyLauncherAmmo") > 0 && %hasLOS && %distToTarg > 20 && %distToTarg < 200) {
      %weaponWeight =  4 * AIWeighLinearProjectile(AIWeighSplash(%enemyAlt, %distToTarg, %targetZ - %myZ, %targetZVel, 70, 17, 20, 0), %distToTarg, %targTangentialVelocity, 70, 5000) ;
      if (%distToTarg < 40 && %enemy.holdingFlag == 0) %weaponWeight = %weaponWeight * %distToTarg / 40;
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "HeavyLauncher"; }
   }
   if (%player.getInventory("EMPLauncher") > 0) if (%player.getInventory("EMPLauncherAmmo") > 0 && %hasLOS && !%inEnemyLand && %distToTarg < 120 && %targEnergy > 0.1 && !%enemyIsRecon) {
      %weaponWeight = (%enemy.holdingFlag ? 200 : 100)/(%targVelocity + 10) * AIWeighLinearProjectile(AIWeighSplash(%enemyAlt, %distToTarg, %targetZ - %myZ, %targetZVel, 42, 15, 18, 0), %distToTarg, %targTangentialVelocity, 42, 7000) ;
      if (%distToTarg < 20 && %enemy.holdingFlag == 0) %weaponWeight = %weaponWeight * %distToTarg / 20;
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "EMPLauncher"; }
   }
   if (%player.getInventory("Mortar") > 0) if (%player.getInventory("MortarAmmo") > 0 && !%inWater && %hasLOS && %distToTarg > 20 && %distToTarg < 400) {
      %weaponWeight =  (%enemyInHeavyArmor ? 7.5 : 5.8) * AIWeighLinearProjectile(AIWeighSplash(%enemyAlt, %distToTarg, %targetZ - %myZ, %targetZVel, MortarShot.muzzleVelocity, MortarShot.damageRadius, 1.2*MortarShot.damageRadius, 0), %distToTarg, %targTangentialVelocity, MortarShot.muzzleVelocity, 1000) ;
      if (%distToTarg < 100 && %enemy.holdingFlag == 0) %weaponWeight = %weaponWeight * %distToTarg / 100;
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "Mortar"; }
   }
   if (%player.getInventory("WidowMaker") > 0) if (%player.getInventory("WidowMakerAmmo") > 0 && %distToTarg < 250 && !%enemy.holdingFlag && %inEnemyLand) {// && !%hasLOS
      %weaponWeight = ((AIManyEnemiesInRange(%client, %player.getWorldBoxCenter(), 20 + %distToTarg/WidowMakerShot.muzzleVelocity, WidowMakerShot.damageRadius, true) > 2.5) ? 25 : 0);
      if (%distToTarg < 200) %weaponWeight = %weaponWeight * %distToTarg * %distToTarg / 40000;
//      echo(getTaggedString(%client.name) SPC %enemy.nameBase SPC "WidowMaker:dist" SPC %distToTarg SPC "myDamage" SPC %myDamage SPC "bestWW" SPC %client.engageWeaponWeight SPC "wWeight" SPC %weaponWeight SPC "%damageIncrease" SPC %damageIncrease);
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "WidowMaker"; }
   }
   if (%player.getInventory("MortarStorm") > 0) if (%player.getInventory("MortarStormAmmo") > 0 && !%inWater && %hasLOS && %distToTarg > 40 && %distToTarg < 600) {
      %baseWeight = AIWeighSplash(%enemyAlt, %distToTarg, %targetZ - %myZ, %targetZVel, MortarStormShot.muzzleVelocity, 0.2 * %distToTarg + MortarStormShot.damageRadius, 0.2 * %distToTarg + 1.2*MortarStormShot.damageRadius, 0);
      if ($teamDamage) {
         %radius = %distToTarg;// + MortarStormShot.damageRadius;
         %enemiesInRange = AIManyEnemiesInRange(%client, %enemy.getWorldBoxCenter(), 1 + %distToTarg/MortarStormShot.muzzleVelocity, %radius) > 2.5;
         %weaponWeight =  (%enemiesInRange ? 16 * (%myDamage + 0.5) : 0.4 *%myDamage) * %baseWeight;
      } else
         %weaponWeight =  ((%newDistance > 4*MortarStormShot.damageRadius || %myDamage > 0.75) ? 9 : %myDamage) * %baseWeight;
      if (%distToTarg < 100 && !%enemy.holdingFlag) %weaponWeight = %weaponWeight * %distToTarg * %distToTarg / 10000;
      if (%weaponWeight > %client.engageWeaponWeight) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "MortarStorm"; }
//      echo(%client.nameBase SPC "MortarStorm" SPC %newDistance SPC %client.engageWeapon SPC "%bestWpWght" SPC %client.engageWeaponWeight SPC "%wpWght" SPC %weaponWeight SPC "%baseWght" SPC %baseWeight SPC %enemiesInRange);
//      if (%targetClient.nameBase $= "P!nkP?nther") echo(getTaggedString(%client.name) SPC "MortarStorm:distToTarg" SPC %distToTarg SPC "targTangentialVelocity" SPC %targTangentialVelocity SPC "hitAlt" SPC %enemyAlt + %distToTarg * %targetZVel / MortarStormShot.muzzleVelocity SPC "weaponWeight" SPC %weaponWeight);
   }

   if (%client.engageWeaponWeight > 0.15) {
// if (%targetClient.nameBase $= "P!nkP?nther") error(getTaggedString(%client.name) SPC %client.engageWeapon SPC
//     "dist" SPC %distToTarg SPC "Alt" SPC %enemyAlt SPC "Armor" SPC %enemyArmor SPC "targVel." SPC %targVelocity
//      SPC "targTang.Vel." SPC %targTangentialVelocity SPC "splash" SPC %splash SPC "bestWpnWeight" SPC %client.engageWeaponWeight);

   //now select the weapon
   switch$ (%client.engageWeapon)
   {
      case "Blaster":
         %client.player.use("Blaster");
         %client.setWeaponInfo("EnergyBolt", 8, 150, 1, 0.1);

      case "Plasma":
         %client.player.use("Plasma");
         %client.setWeaponInfo("PlasmaBolt", 10, 50);

      case "Chaingun":
         %client.player.use("Chaingun");
         %client.setWeaponInfo("ChaingunBullet", 5, 75, 150);

      case "Disc":
         %client.player.use("Disc");
         %client.setWeaponInfo("DiscProjectile", 10, 150);

      case "GrenadeLauncher":
         %client.player.use("GrenadeLauncher");
         %client.setWeaponInfo("BasicGrenade", 20, 75);

      case "SniperRifle":
         %client.player.use("SniperRifle");
         %client.setWeaponInfo("BasicSniperShot", 40, 500, 1, 0.75, 0.5);

      case "ELFGun":
         %client.player.use("ELFGun");
         %client.setWeaponInfo("BasicELF", 2, 45, 90, 0.1);

      case "ShockLance":
         %client.player.use("ShockLance");
         %client.setWeaponInfo("BasicShocker", 0.5, BasicShocker.extension, 1, 0.1);

      case "MissileLauncher":
         %client.aimAt(%targetClient, 4000);
         %client.player.use("MissileLauncher");
         %client.setWeaponInfo("ShoulderMissile", 20, 450);

      case "MissileLauncherDumbfire":
         %client.player.use("MissileLauncher");
         %client.setWeaponInfo("DumbFire", 7, 350);

      case "Mortar":
         %client.player.use("Mortar");
         %client.setWeaponInfo("MortarShot", 40, 400);

   // ----- Triumph specific weapons here: pinkpanther 07/21/04  -----

      case "AntiAirGun":
         %client.player.use("AntiAirGun");
         %client.setWeaponInfo("AntiAirGunBolt", 5, 300);

      case "BeamSword":
         %client.player.use("BeamSword");
         %client.setWeaponInfo("LightBeam", 1, 28, 100);

      case "DualBlaster":
         %client.player.use("DualBlaster");
         %client.setWeaponInfo("DualEnergyBolt", 5, 250, 1, 0.1);

      case "ElfCannon":
         %client.player.use("ElfCannon");
         %client.setWeaponInfo("ELFTurretBolt", 2, 75, 175, 0.1);

      case "EMPLauncher":
         %client.player.use("EMPLauncher");
         %client.setWeaponInfo("EMPlaunGrenade", 1, 175);

      case "GatlingGun":
         %client.player.use("GatlingGun");
         %client.setWeaponInfo("GatlingGunBullet", 15, 100, 250);

      case "HeadHunter":
         %client.player.use("HeadHunter");
         %client.setWeaponInfo("HeadHunterSniperShot", 10, 1000, 1, 0.75);

      case "HeavyLauncher":
         %client.player.use("HeavyLauncher");
         %client.setWeaponInfo("HeavyGrenade", 30, 200);

      case "Hornet":
         %client.player.use("Hornet");
         %client.setWeaponInfo("HornetProjectile", 8, 550);

      case "Napalm":
         %client.player.use("Napalm");
         %client.setWeaponInfo("NapalmBolt", 4, 60);

      case "RailCannon":
         %client.player.use("RailCannon");
         %client.setWeaponInfo("RailCannonBolt", 10, 1000);

      case "RailNade":
         %client.player.use("RailNade");
         %client.setWeaponInfo("RailNadeProjectile", 10, 250);

       case "HeavyLaser":
         %client.player.use("HeavyLaser");
         %client.setWeaponInfo("HeavyLaserSniperShot", 1, 1000, 1, 0.75);
//         %client.setWeaponInfo("HeavyLaserSniperShot", 1, 1000, 1, 0.2, 0.5);

      case "SlugRifle":
         %client.player.use("SlugRifle");
         %client.setWeaponInfo("SlugRifleBullet", 5, 1200);

      case "Shotgun":
         %client.player.use("Shotgun");
         %client.setWeaponInfo("ShotgunBullet", 1, 40);

      case "SolarCharge":
         %client.player.use("SolarCharge");
         %client.setWeaponInfo("SolarChargeBolt", 50, 300);

      case "MortarStorm":
         %client.player.use("MortarStorm");
         %client.setWeaponInfo("MortarStormShot", 40, 600);
         %client.setDangerLocation(%enemyPos, %distToTarg +MortarStormShot.damageRadius);

      case "WidowMaker":
         %client.player.use("WidowMaker");
         %client.setWeaponInfo("WidowMakerShot", 4, 600);
         %client.aimAt(%targetClient, 15000);
         %client.setDangerLocation(%enemyPos, %distToTarg +WidowMakerShot.damageRadius);

      case "TitanChaingun":
         %client.player.use("TitanChaingun");
         %client.setWeaponInfo("TitanChaingunBullet", 1, 90, 100);

      case "NoAmmo":
         %client.setWeaponInfo("NoAmmo", 30, 75);
   }
   }
}

//function is called once per frame, to handle packs, healthkits, grenades, etc...
//heh heh heh this one was fun to do - Lagg... - 12-2003
// Heavy modifications here   - pinkpanther
function AIProcessEngagement(%client, %target, %type, %projectile)
{
   %player = %client.player;
   if (!isObject(%player))
      return;

   //make sure we're still alive
   if (!AIClientIsAlive(%client))
      return;

   //clear the pressFire
   %client.pressFire(-1);

   //LEAVE THIS OUT OR FLARES NO WORK !!!
   //added a check to call function for bots in vehicle - Lagg...
   //if (%client.player.isMounted())
   //{
   //AIProcessVehicle(%client);//added this call to help bomber bot - Lagg...
   //check for LOS
   //%vehicleLOS = "false";
   //%mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType |
   //$TypeMasks::TSStaticShapeObjectType | $TypeMasks::VehicleObjectType;
   //%vehicleLOS = !containerRayCast(%client.player.getWorldBoxCenter(), %enemyPos, %mask, 0);
   //if (! %vehicleLos)
   //{
   //clear the pressFire
   //%client.pressFire(-1);
   //%client.setEngageTarget(-1);
   //return;
   //}
   //}

   %inwater = %client.enviroment; // Capto Lamia for this one --
   %outdoors = %client.enviromental;

   //see if we have to use a repairkit
   %mounted = %player.isMounted();
   %myEnergy = %player.getEnergyPercent();//added for cloaking and sensorjammer packs - Lagg... - 4-9-2003
   //%tur = -1;//added for cloaking and sensorjammer packs - Lagg... - 4-9-2003

   if (%client.getSkillLevel() > 0.1 && %player.getDamagePercent() > 0.3 && %player.getInventory("RepairKit") > 0)
   {
      //add in a "skill" value to delay the using of the repair kit for up to 10 seconds...
      %elapsedTime = getSimTime() - %client.lastDamageTime;
      %skillValue = (1.0 - %client.getSkillLevel()) * (1.0 - %client.getSkillLevel());
      if (%elapsedTime > (%skillValue * 20000))
         %player.use("RepairKit");
   }

   //see if we've been blinded
   if (%player.getWhiteOut() > 0.6)
      %client.setBlinded(2000);

   //else see if there's a grenade in the vicinity...
   else
   {
      %count = $AIGrenadeSet.getCount();
      for (%i = 0; %i < %count; %i++)
      {
         %grenade = $AIGrenadeSet.getObject(%i);

         //make sure the grenade isn't ours
         //if (%grenade.sourceObject.client != %client) //----- taken out Lagg... 11/24/2002
         //{  //----------------------------------------------- taken out Lagg... 11/24/2002
         // A grenade is a danger no matter who threw it. Has great effect when mortaring
         //see if it's within 15 m
         if (VectorDist(%grenade.position, %client.player.position) < 15)
         {
            %client.setDangerLocation(%grenade.position, 30);
            break;
         }
         //}  //----------------------------------------------- taken out Lagg... 11/24/2002
      }
   }

   //if we're being hunted by a seeker projectile, throw a flare grenade
   //modified from Capto Lamias tailgunner script ------------------------------ Lagg... 1-13-2003
   //the bots will flare to protect any team owned object if in range to do so
   if (%player.getInventory("FlareGrenade") > 0)
   {
      %missileCount = MissileSet.getCount();
      for (%i = 0; %i < %missileCount; %i++)
      {
         %missile = MissileSet.getObject(%i);
         %missileTarg = (%missile.getTargetObject());
         if (%missileTarg.Team == %client.Team)
         {
            //see if the missile is within range
            if (VectorDist(%missile.getTransform(), %player.getTransform()) < 150)
            {
               if (VectorDist(%missileTarg.getTransform(), %player.getTransform()) < 75)
               {
                  %player.throwStrength = 2;
                  %player.use("FlareGrenade");
                  break;
               }
            }
         }
      }
   }

   //see what we're fighting
   switch$ (%type)
   {
      case "player":
         //make sure the target is alive
         if (AIClientIsAlive(%target))
         {
            //if the target is in range, and within 10-40 m, & heading in this direction,use grenade
            //but not if we are mounted in a vehicle - Lagg...
            if (!$AIDisableGrenades && %client.getSkillLevel() >= 0.3 && !%inwater && !%isMounted)
            {
               %grenadeType = "";
               if (%player.getInventory("Grenade") > 0)
                  %grenadeType = "Grenade";
               else if (%player.getInventory("FlashGrenade") > 0)
                  %grenadeType = "FlashGrenade";
               else if (%player.getInventory("ConcussionGrenade") > 0)
                  %grenadeType = "ConcussionGrenade";
               // Triumph stuff:      - pinkpanther
               else if (%player.getInventory("StickyBomb") > 0)
                  %grenadeType = "StickyBomb";
               else if (%player.getInventory("BoostGrenade") > 0)// && %player.holdingFlag && VectorLen(%player.getVelocity()) > LightMaleHumanArmor.maxBackwardSpeed)
                  %grenadeType = "BoostGrenade";
               else if (%player.getInventory("TimeBomb") > 0) {
                  if (AIManyEnemiesInRange(%client, %client.player.getWorldBoxCenter(), 10, 2*TimeBombThrown.damageRadius) > 2.5)
                     %grenadeType = "TimeBomb";
                  }
               else if (%player.getInventory("TacticalNuke") > 0 && %player.getDamagePercent() > 0.7)     {        // throw Tactical Nuke if low on health and more enemies than teammates in range
                  if ($teamDamage && %client.getSkillLevel() > 0.5) {
                     if (AIManyEnemiesInRange(%client, %client.player.getWorldBoxCenter(), 10, TacticalNukeThrown.damageRadius) > 2.5) {
                        %grenadeType = "TacticalNuke";
                     }
                  } else if (%client.getSkillLevel() > 0.3) {
                     if (%player.getDamagePercent() - %target.player.getDamagePercent() > 0.05) {
                        %grenadeType = "TacticalNuke";
                     }
                  }
               }
               //--------------------------------Capto Lamia for this one as well
               else if (%player.getInventory("Mine") > 0) {
                 // only use mines in enemy half of the area -  pinkpanther
                 %clientPos = %client.player.getWorldBoxCenter();
                 %enemyTeam = (%client.team == 1) ? 2 : 1;
                 %inEnemyLand = getRandom() > VectorDist(%clientPos, $AITeamFlag[%enemyTeam].originalPosition) / (VectorDist(%clientPos, $AITeamFlag[%client.team].originalPosition) + 1);
                 if (%inEnemyLand) %grenadeType = "Mine";
               }

               if (%grenadeType !$= "" && %client.targetInSight())
               {
                  //see if the predicted location of the target is within 10m
                  %targPos = %target.player.getWorldBoxCenter();
                  %clientPos = %player.getWorldBoxCenter();

                  %radius = (%grenadeType $= "TacticalNuke") ? TacticalNukeThrown.damageRadius : 25;
                  //make sure we're not *way* above the target
                  if (getWord(%clientPos, 2) - getWord(%targPos, 2) < 3 && VectorDist(%targPos, %player.lastGrenadePos) > %radius)     // added minimal distance between thrown grenades   - pinkpanther
                  {
                     %dist = VectorDist(%targPos, %clientPos);
                     %direction = VectorDot(VectorSub(%clientPos, %targPos), %target.player.getVelocity());
                     %facing = VectorDot(VectorSub(%client.getAimLocation(), %clientPos), VectorSub(%targPos, %clientPos));
                     if (%dist > 20 && %dist < 45 && (%direction > 0.9 || %direction < -0.9) && (%facing > 0.9))
                     {
                        %player.throwStrength = 1.0;
                        %player.use(%grenadeType);
                        %player.lastGrenadePos = %targPos;     //pinkpanther
                        %client.setDangerLocation(%targPos, 1.2*%radius);  // pinkpanther
                     }
                  }
               }
            }
            //see if we have a shield pack that we need to use
            if (%player.getInventory("ShieldPack") > 0)
            {
               if (%projectile > 0 && %player.getImageState($BackpackSlot) $= "Idle" && %myEnergy > 0.2)
                  %player.use("Backpack");
               else if (%projectile <= 0 && %player.getImageState($BackpackSlot) $= "activate" && %myEnergy < 0.05)         // keep shield pack activated until energy gets low   - pinkpanther
                  %player.use("Backpack");
            }
            //see if we have a cloaking pack that we need to use -----Capto Lamia - Lagg modified... 4-13-2003
            if (%player.getInventory("CloakingPack") > 0)
            {
               if (%player.getImageState($BackpackSlot) $= "Idle" && %myEnergy > 0.4)
                  %player.use("Backpack");
               else if (%player.getImageState($BackpackSlot) $= "activate" && %myEnergy < 0.4)
                  %player.use("Backpack");
            }
            //see if we have a sensor jammer pack that we need to use ---------Capto Lamia - Lagg modified... 4-13-2003
            //thinking about a radius search for turrets, sensors, ect... - Lagg 1-13-2003
//       ********** in Triumph 2.1 we don't need to activate the sensor jammer when approaching the target since it works passive:  ********  - pinkpanther
//           if (%player.getInventory("SensorJammerPack") > 0)
//           {
//              %myEnergy = %client.player.getEnergyPercent();
//              %closestTeammate = AIFindClosestTeammate(%client, 75, $AIClientLOSTimeout);
//error(getTaggedString(%client.name) @ " %closestTeammate: "@%closestTeammate);
//              if (%player.getImageState($BackpackSlot) $= "Idle" && %myEnergy > 0.5 && getWord(%closestTeammate, 1) < 75)
//                 %player.use("Backpack");
//              else if (%player.getImageState($BackpackSlot) $= "activate" && (%myEnergy < 0.4 || getWord(%closestTeammate, 0) == -1))
//                 %player.use("Backpack");
//           }

//    if (%player.getInventory("RepairGun2") > 0) if (%canUseEnergy && %myDamage > 0) {
//       %weaponWeight = (%hasLOS ? (%enemy.holdingFlag ? 0 : %distToTarg*%myDamage*%myDamage*0.1) : 100);
//       if (1) { %client.engageWeaponWeight = %weaponWeight; %client.engageWeapon = "RepairGun2"; }
//    }
// ***************************** Trying to get the bot to repair itself while engaging and not being in LOS - pinkpanther
//          if (%player.getDamagePercent() > 0 && %player.getInventory(RepairGun2) > 0)
//          {
//            %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticTSObjectType
//                  | $TypeMasks::StaticObjectType | $TypeMasks::ForceFieldObjectType | $TypeMasks::ItemObjectType;
//             %hasNoLOS = containerRayCast(%player.getMuzzlePoint($WeaponSlot), %target.player.getWorldBoxCenter(), %mask, 0);
//             if (%hasNoLOS) {
// 		         %client.setTargetObject(-1);
// 		         %client.setEngageTarget(-1);
//                %client.player.use("RepairGun2");
//                %client.pressFire(30);
//  if (%target.nameBase $= "P!nkP?nther") error(getTaggedString(%client.name) SPC %player.getDamagePercent());
//                }
//          }
        }

      case "object":
//    Use grenades when engaging but please not against objects! (because bots most of the time kill themselves when using grens against a gen)  - pinkpanther
//    this time we're using TimeBombs instead, works pretty well against gens and turrets
         %hasGrenade = %player.getInventory("TimeBomb") > 0;
         if (%hasGrenade && %client.targetInRange() && !%inwater && !%mounted)
         {
            %targPos = %target.getWorldBoxCenter();
            %myPos = %player.getWorldBoxCenter();
            %dist = VectorDist(%targPos, %myPos);
            if (%dist > 5 && %dist < 20 && VectorDist(%targPos, %player.lastGrenadePos) > 20)
            {
               %player.throwStrength = 1.0;
               %player.use("TimeBomb");
               %player.lastGrenadePos = %targPos;     //pinkpanther
               %client.setDangerLocation(%targPos, 1.5*TimeBombThrown.damageRadius);  // pinkpanther
            }
         }

         //Capto Lamia, add it here as well---------------- Lagg... 1-12-2003
         %hasmine = %player.getInventory("Mine") > 0;
         if (%hasMine && %client.targetInRange() && !%inwater)
         {
            //only throw a mine when killing gens or invos - Lagg... 4-12-2003
            %targ = %target.getDataBlock().getName();
            //   throw mine not before target is almost destroyed:    - pinkpanther
            if ((%targ $= "StationInventory" || %targ $= "GeneratorLarge" || %targ $= "StationVehicle") && %target.getDamagePercent() > 0.99)
            {
               %targPos = %target.getWorldBoxCenter();
               %myPos = %player.getWorldBoxCenter();
               %dist = VectorDist(%targPos, %myPos);
               if (%dist < 20)
               {
                  %player.throwStrength = 1.0;
                  %player.use("Mine");
               }
            }
         }
         //see if we have a cloaking pack that we need to use -----Capto Lamia - Lagg modified... 4-13-2003
         if (%player.getInventory("CloakingPack") > 0)
         {
            if (%player.getImageState($BackpackSlot) $= "Idle" && %myEnergy > 0.4 && %client.targetInRange())
               %player.use("Backpack");
            else if (%player.getImageState($BackpackSlot) $= "activate" && (%myEnergy < 0.4 || !%client.targetInRange()))
               %player.use("Backpack");
         }
         //see if we have a sensor jammer pack that we need to use ---------Capto Lamia
         //thinking about a radius search for turrets, sensors, ect... - Lagg 1-13-2003
//       ********** in Triumph 2.1 we don't need to activate the sensor jammer when approaching the target since it works passive:  ********  - pinkpanther
//         if (%player.getInventory("SensorJammerPack") > 0 && %client.targetInRange())
//         {
//            if (%player.getImageState($BackpackSlot) $= "Idle" && %myEnergy > 0.4 && %client.targetInRange())
//               %player.use("Backpack");
//            else if (%player.getImageState($BackpackSlot) $= "activate" && (%myEnergy < 0.4 || !%client.targetInRange()))
//               %player.use("Backpack");
//         }

      case "none":
         //use the repair pack if we have one and not right next to a target - Lagg... 3-29-2003
         if (%player.getDamagePercent() > 0 && (%player.getInventory(RepairPack) > 0 || %player.getInventory(RepairGun2) > 0) && !%client.targetInRange())
         {
            //if (%player.getImageState($BackpackSlot) $= "Idle")
		//%client.player.use("RepairPack");
            //else if (%player.getImageState($BackpackSlot) $= "activate")
            //sustain the fire for 30 frames - this callback is timesliced...
            //%client.pressFire(30);
            //if (%player.getImageState($BackpackSlot) !$= "activate")
            //clear the fire if not using repairpack - Lagg... - 4-9-2003
            //%client.pressFire(-1);

            if (%player.getInventory(RepairGun2) > 0) {
		         %client.player.use("RepairGun2");
               %client.pressFire(30);
               }
            else if (%player.getImageState($BackpackSlot) !$= "activate")
		         %client.player.use("RepairPack");
            else if (isObject(%client.player.getMountedImage($WeaponSlot)) && %client.player.getMountedImage($WeaponSlot).getName() $= "RepairGunImage")
               //sustain the fire for 30 frames - this callback is timesliced...
               %client.pressFire(30);
            else if (!isObject(%client.player.getMountedImage($WeaponSlot)) || %client.player.getMountedImage($WeaponSlot).getName() !$= "RepairGunImage")
               //clear the fire if not using repairpack - Lagg... - 4-9-2003
               %client.pressFire(-1);
         }
   }
}

//------------------------------------------------------------------------------

function AIFindClosestInventory(%client, %armorChange)
{
   %closestInv = -1;
   %closestDist = 32767;

   %depGroup = nameToID("MissionCleanup/Deployables");
   if (%depGroup > 0)
      %depCount = %depGroup.getCount();
   else
      %depCount = 0;

   // there exists a deployed station, lets find it
   if(!%armorChange && %depCount > 0)
   {
      for(%i = 0; %i < %depCount; %i++)
      {
         %obj = %depGroup.getObject(%i);
         if (!isObject(%obj)) continue;

         if(strstr(%obj.getDataBlock().getName(), "StationInventory") >= 0 && %obj.team == %client.team && %obj.isEnabled())
         {
            //make sure the inventory station is not blocked - lets add a check for parked vehicles :) - Lagg... 2-25-2004
	         %invLocation = %obj.getWorldBoxCenter();
            InitContainerRadiusSearch(%invLocation, 12, $TypeMasks::VehicleObjectType);
            %objSrch = containerSearchNext();

            %distance = %client.getPathDistance(%obj.getTransform()) * 1.4 + 200;            // Added "* 1.4 + 200" to penalize deployed inv a bit - pinkpanther
            if (%distance > 0 && %distance < %closestDist && !%objSrch)
            {
               %closestInv = %obj;
               %closestDist = %distance;
            }
         }
      }
   }

   // Check for MPB Inv - pinkpanther
   %depGroup = nameToID("MissionCleanup");
   if (%depGroup > 0)
      %depCount = %depGroup.getCount();
   else
      %depCount = 0;

   if(%depCount > 0)
   {
      for(%i = 0; %i < %depCount; %i++)
      {
         %obj = %depGroup.getObject(%i);
         if (!isObject(%obj)) continue;

         if (%obj.getClassName() $= "StaticShape")
            if(%obj.getDataBlock().getName() $= "MobileInvStation" && %obj.team == %client.team)
            {
               %distance = %client.getPathDistance(%obj.getTransform()) + getRandom(0,100);     // Added random distance to randomly pick invs within a certain range - pinkpanther
               if (%distance > 0 && %distance < %closestDist && !%objSrch)
               {
                  %closestInv = %obj;
                  %closestDist = %distance;
               }
            }
      }
   }

   // still check if there is one that is closer
   %invCount = $AIInvStationSet.getCount();
   for (%i = 0; %i < %invCount; %i++)
   {
      %invStation = $AIInvStationSet.getObject(%i);
      if (%invStation.team <= 0 || %invStation.team == %client.team)
      {
         //error("DEBUG: found an inventory station: " @ %invStation @ "  status: " @ %invStation.isPowered());
         //make sure the station is powered
         if (!%invStation.isDisabled() && %invStation.isPowered())
         {
            %dist = %client.getPathDistance(%invStation.getTransform()) + getRandom(0,100);     // Added random distance to randomly pick invs within a certain range - pinkpanther
            if (%dist > 0 && %dist < %closestDist)
            {
               %closestInv = %invStation;
               %closestDist = %dist;
            }
         }
      }
   }
   return %closestInv @ " " @ %closestDist;
}

//------------------------------
//find the closest inventories for the objective weight functions
function AIFindClosestInventories(%client)
{
   %closestInv = -1;
   %closestDist = 32767;
   %closestRemoteInv = -1;
   %closestRemoteDist = 32767;

   %depCount = 0;
   %depGroup = nameToID("MissionCleanup/Deployables");

   //first, search for the nearest deployable inventory station
   if (isObject(%depGroup))
   {
      %depCount = %depGroup.getCount();
      for (%i = 0; %i < %depCount; %i++)
      {
         %obj = %depGroup.getObject(%i);
         if (!isObject(%obj)) continue;
         if (%obj.getDataBlock().getName() $= "DeployedStationInventory" && %obj.team == %client.team && %obj.isEnabled())
         {
            //make sure the inventory station is not blocked - lets add a check for parked vehicles :) - Lagg... 2-25-2004
	         %invLocation = %obj.getWorldBoxCenter();
            InitContainerRadiusSearch(%invLocation, 12, $TypeMasks::VehicleObjectType);
            %objSrch = containerSearchNext();

            %distance = %client.getPathDistance(%obj.getTransform());
            if (%distance > 0 && %distance < %closestRemoteDist && !%objSrch)
            {
               %closestRemoteInv = %obj;
               %closestRemoteDist = %distance;
            }
         }
         else if (%obj.getDataBlock().getName() $= "StationInventory" && %obj.team == %client.team && !%obj.isDisabled())
         {    // Check for inv inside deployable bunker - pinkpanther
            %distance = %client.getPathDistance(%obj.getTransform());
            if (%distance > 0 && %distance < %closestDist && !%objSrch)
            {
               %closestInv = %obj;
               %closestDist = %distance;
            }
         }
      }
   }

   // Check for MPB Inv - pinkpanther
   %depGroup = nameToID("MissionCleanup");
   if (%depGroup > 0)
      %depCount = %depGroup.getCount();
   else
      %depCount = 0;

   if(%depCount > 0)
   {
      for(%i = 0; %i < %depCount; %i++)
      {
         %obj = %depGroup.getObject(%i);
         if (!isObject(%obj)) continue;

         if (%obj.getClassName() $= "StaticShape")
            if(%obj.getDataBlock().getName() $= "MobileInvStation" && %obj.team == %client.team)
            {
               %distance = %client.getPathDistance(%obj.getTransform());
               if (%distance > 0 && %distance < %closestDist && !%objSrch)
               {
                  %closestInv = %obj;
                  %closestDist = %distance;
               }
            }
      }
   }

   // now find the closest regular inventory station
   %invCount = $AIInvStationSet.getCount();
   for (%i = 0; %i < %invCount; %i++)
   {
      %invStation = $AIInvStationSet.getObject(%i);
      if (%invStation.team <= 0 || %invStation.team == %client.team)
      {
         //make sure the station is powered
         if (!%invStation.isDisabled() && %invStation.isPowered())
         {
            %dist = %client.getPathDistance(%invStation.getTransform());
            if (%dist > 0 && %dist < %closestDist)
            {
               %closestInv = %invStation;
               %closestDist = %dist;
            }
         }
      }
   }

   //if the regular inv station is closer than the deployed, don't bother with the remote
   if (%closestDist < %closestRemoteDist)
      %returnStr = %closestInv SPC %closestDist;
   else
      %returnStr = %closestInv SPC %closestDist SPC %closestRemoteInv SPC %closestRemoteDist;

   return %returnStr;
}

//new function called from AIAttackObject and AIdestroyObject and aiDetectVehiculeTask - Lagg... - 4-12-2003
function AIAttackOutofAmmo(%client)
{
   //this function only cares about weapons used in engagement...
   //no mortars, or missiles - yeah right...Lagg
   %player = %client.player;
   if (!isObject(%player))
      return false;

   %ammoWeapons = 0;
   %energyWeapons = 0;

   //get our inventory
   %hasBlaster = (%player.getInventory("Blaster") > 0);
   %hasPlasma  = (%player.getInventory("Plasma") > 0);
   %hasChain   = (%player.getInventory("Chaingun") > 0);
   %hasDisc    = (%player.getInventory("Disc") > 0);
   %hasGrenade = (%player.getInventory("GrenadeLauncher") > 0);
   %hasSniper  = (%player.getInventory("SniperRifle") > 0);
   %hasHeavyLaser  = (%player.getInventory("HeavyLaser") > 0) && %player.getInventory("EnergyPack") > 0;
   %hasELF     = (%player.getInventory("ELFGun") > 0);
   %hasMortar  = (%player.getInventory("Mortar") > 0);
   %hasMissile = (%player.getInventory("MissileLauncher") > 0);
   %hasLance   = (%player.getInventory("ShockLance") > 0);
   // ----- Triumph specific weapons here: pinkpanther 07/21/04  -----
   %hasAntiAirGun    = (%player.getInventory("AntiAirGun") > 0);
   %hasDualBlaster   = (%player.getInventory("DualBlaster") > 0);
   %hasElfCannon     = (%player.getInventory("ElfCannon") > 0);
   %hasGatlingGun    = (%player.getInventory("GatlingGun") > 0);
   %hasHeadHunter    = (%player.getInventory("HeadHunter") > 0) && (%player.getInventory("EnergyPack") > 0);
   %hasHeavyLauncher = (%player.getInventory("HeavyLauncher") > 0);
   %hasHornet        = (%player.getInventory("Hornet") > 0);
   %hasNapalm        = (%player.getInventory("Napalm") > 0);
   %hasRailCannon    = (%player.getInventory("RailCannon") > 0);
   %hasRailNade      = (%player.getInventory("RailNade") > 0);
   %hasShotgun       = (%player.getInventory("Shotgun") > 0);
   %hasSlugRifle     = (%player.getInventory("SlugRifle") > 0);
   %hasSolarCharge   = (%player.getInventory("SolarCharge") > 0);
   %hasBeamSword     = (%player.getInventory("BeamSword") > 0);

   if (%hasBlaster || %hasSniper || %hasHeavyLaser || %hasElf || %hasLance || %hasHeadHunter || %hasDualBlaster || %hasSolarCharge || %hasElfCannon || %hasNapalm || %hasBeamSword)	// pinkpanther 07/21/04
      return false;
   else
   {
      // we only have ammo type weapons
      if(%hasDisc && (%player.getInventory("DiscAmmo") > 0))
         return false;
      else if(%hasChain && (%player.getInventory("ChainGunAmmo") > 0))
         return false;
      else if(%hasGrenade && (%player.getInventory("GrenadeLauncherAmmo") > 0))
         return false;
      else if(%hasPlasma && (%player.getInventory("PlasmaAmmo") > 0))
         return false;
      else if(%hasMissile && (%player.getInventory("MissileLauncherAmmo") > 0))
         return false;
      else if(%hasMortar && (%player.getInventory("MortarAmmo") > 0))
         return false;
   // ----- Triumph specific weapons here: pinkpanther 07/21/04  -----
      else if(%hasGatlingGun && (%player.getInventory("GatlingGunAmmo") > 0))
         return false;
      else if(%hasHeavyLauncher && (%player.getInventory("HeavyLauncherAmmo") > 0))
         return false;
      else if(%hasHornet && (%player.getInventory("HornetAmmo") > 0))
         return false;
      else if(%hasRailCannon && (%player.getInventory("RailCannonAmmo") > 0))
         return false;
      else if(%hasRailNade && (%player.getInventory("RailNadeAmmo") > 0))
         return false;
      else if(%hasShotgun && (%player.getInventory("ShotgunAmmo") > 0))
         return false;
      else if(%hasSlugRifle && (%player.getInventory("SlugRifleAmmo") > 0))
         return false;
      else if(%hasAntiAirGun && (%player.getInventory("AntiAirGunAmmo") > 0))
         return false;
   }
   return true; // were out!
}

//------------------------------------------------------------------------------------------------

// Too many modifications to mention -  pinkpanther@swissonline.ch

//AI Equipment Configs
$EquipConfigIndex = -1;
$AIEquipmentSet[HeavyEnergyDefault, $EquipConfigIndex++] = "Heavy";
$AIEquipmentSet[HeavyEnergyDefault, $EquipConfigIndex++] = "EnergyPack";
$AIEquipmentSet[HeavyEnergyDefault, $EquipConfigIndex++] = "Napalm";
$AIEquipmentSet[HeavyEnergyDefault, $EquipConfigIndex++] = "SolarCharge";
$AIEquipmentSet[HeavyEnergyDefault, $EquipConfigIndex++] = "Hornet";
$AIEquipmentSet[HeavyEnergyDefault, $EquipConfigIndex++] = "HornetAmmo";
//$AIEquipmentSet[HeavyEnergyDefault, $EquipConfigIndex++] = "AntiAirGun";
//$AIEquipmentSet[HeavyEnergyDefault, $EquipConfigIndex++] = "AntiAirGunAmmo";
$AIEquipmentSet[HeavyEnergyDefault, $EquipConfigIndex++] = "Mortar";
$AIEquipmentSet[HeavyEnergyDefault, $EquipConfigIndex++] = "MortarAmmo";
$AIEquipmentSet[HeavyEnergyDefault, $EquipConfigIndex++] = "Plasma";
$AIEquipmentSet[HeavyEnergyDefault, $EquipConfigIndex++] = "PlasmaAmmo";
$AIEquipmentSet[HeavyEnergyDefault, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[HeavyEnergyDefault, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[HeavyEnergyDefault, $EquipConfigIndex++] = "TacticalNuke";
$AIEquipmentSet[HeavyEnergyDefault, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "Heavy";
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "EnergyPack";
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "RailNade";
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "RailNadeAmmo";
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "SolarCharge";
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "Plasma";
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "PlasmaAmmo";
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "Hornet";
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "HornetAmmo";
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "Mortar";
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "MortarAmmo";
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "TacticalNuke";
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "Heavy";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "AmmoPack";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "Plasma";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "PlasmaAmmo";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "RailNade";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "RailNadeAmmo";
//$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "AntiAirGun";
//$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "AntiAirGunAmmo";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "Hornet";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "HornetAmmo";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "Mortar";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "MortarAmmo";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "TacticalNuke";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[HeavyElementShield, $EquipConfigIndex++] = "Heavy";
$AIEquipmentSet[HeavyElementShield, $EquipConfigIndex++] = "ElementShieldPack";
$AIEquipmentSet[HeavyElementShield, $EquipConfigIndex++] = "SolarCharge";
$AIEquipmentSet[HeavyElementShield, $EquipConfigIndex++] = "Plasma";
$AIEquipmentSet[HeavyElementShield, $EquipConfigIndex++] = "PlasmaAmmo";
$AIEquipmentSet[HeavyElementShield, $EquipConfigIndex++] = "RailNade";
$AIEquipmentSet[HeavyElementShield, $EquipConfigIndex++] = "RailNadeAmmo";
$AIEquipmentSet[HeavyElementShield, $EquipConfigIndex++] = "Hornet";
$AIEquipmentSet[HeavyElementShield, $EquipConfigIndex++] = "HornetAmmo";
$AIEquipmentSet[HeavyElementShield, $EquipConfigIndex++] = "Mortar";
$AIEquipmentSet[HeavyElementShield, $EquipConfigIndex++] = "MortarAmmo";
$AIEquipmentSet[HeavyElementShield, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[HeavyElementShield, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[HeavyElementShield, $EquipConfigIndex++] = "TacticalNuke";
$AIEquipmentSet[HeavyElementShield, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "Heavy";
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "ShieldPack";
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "RailNade";
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "RailNadeAmmo";
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "Napalm";
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "Hornet";
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "HornetAmmo";
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "Mortar";
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "MortarAmmo";
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[HeavyShieldOff, $EquipConfigIndex++] = "Heavy";
$AIEquipmentSet[HeavyShieldOff, $EquipConfigIndex++] = "ShieldPack";
$AIEquipmentSet[HeavyShieldOff, $EquipConfigIndex++] = "Napalm";
$AIEquipmentSet[HeavyShieldOff, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[HeavyShieldOff, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[HeavyShieldOff, $EquipConfigIndex++] = "SolarCharge";
$AIEquipmentSet[HeavyShieldOff, $EquipConfigIndex++] = "Hornet";
$AIEquipmentSet[HeavyShieldOff, $EquipConfigIndex++] = "HornetAmmo";
$AIEquipmentSet[HeavyShieldOff, $EquipConfigIndex++] = "Mortar";
$AIEquipmentSet[HeavyShieldOff, $EquipConfigIndex++] = "MortarAmmo";
$AIEquipmentSet[HeavyShieldOff, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[HeavyShieldOff, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[HeavyShieldOff, $EquipConfigIndex++] = "TacticalNuke";
$AIEquipmentSet[HeavyShieldOff, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "Heavy";
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "RepairPack";
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "Napalm";
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "Hornet";
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "HornetAmmo";
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "Disc";
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "Mortar";
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "MortarAmmo";
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "AntiAirGun";
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "AntiAirGunAmmo";
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
// Turret monkey must be FieldTech - pinkpanther
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "FieldTech";
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "TurretIndoorDeployable";
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "EMPLauncher";
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "EMPLauncherAmmo";
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "ElfCannon";
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "Plasma";
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "PlasmaAmmo";
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "ConcussionGrenade";
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
// Turret monkey must be FieldTech - pinkpanther
$AIEquipmentSet[HeavyOutdoorTurretSet, $EquipConfigIndex++] = "FieldTech";
$AIEquipmentSet[HeavyOutdoorTurretSet, $EquipConfigIndex++] = "TurretOutdoorDeployable";
$AIEquipmentSet[HeavyOutdoorTurretSet, $EquipConfigIndex++] = "ElfCannon";
$AIEquipmentSet[HeavyOutdoorTurretSet, $EquipConfigIndex++] = "EMPLauncher";
$AIEquipmentSet[HeavyOutdoorTurretSet, $EquipConfigIndex++] = "EMPLauncherAmmo";
$AIEquipmentSet[HeavyOutdoorTurretSet, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[HeavyOutdoorTurretSet, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[HeavyOutdoorTurretSet, $EquipConfigIndex++] = "AntiAirGun";
$AIEquipmentSet[HeavyOutdoorTurretSet, $EquipConfigIndex++] = "AntiAirGunAmmo";
$AIEquipmentSet[HeavyOutdoorTurretSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[HeavyOutdoorTurretSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[HeavyOutdoorTurretSet, $EquipConfigIndex++] = "ConcussionGrenade";
$AIEquipmentSet[HeavyOutdoorTurretSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
// Turret monkey must be FieldTech - pinkpanther
$AIEquipmentSet[HeavyLaserTurretSet, $EquipConfigIndex++] = "FieldTech";
$AIEquipmentSet[HeavyLaserTurretSet, $EquipConfigIndex++] = "TurretLaserDeployable";
$AIEquipmentSet[HeavyLaserTurretSet, $EquipConfigIndex++] = "ElfCannon";
$AIEquipmentSet[HeavyLaserTurretSet, $EquipConfigIndex++] = "EMPLauncher";
$AIEquipmentSet[HeavyLaserTurretSet, $EquipConfigIndex++] = "EMPLauncherAmmo";
$AIEquipmentSet[HeavyLaserTurretSet, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[HeavyLaserTurretSet, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[HeavyLaserTurretSet, $EquipConfigIndex++] = "AntiAirGun";
$AIEquipmentSet[HeavyLaserTurretSet, $EquipConfigIndex++] = "AntiAirGunAmmo";
$AIEquipmentSet[HeavyLaserTurretSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[HeavyLaserTurretSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[HeavyLaserTurretSet, $EquipConfigIndex++] = "ConcussionGrenade";
$AIEquipmentSet[HeavyLaserTurretSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
// Turret monkey must be FieldTech - pinkpanther
$AIEquipmentSet[HeavyEMPTurretSet, $EquipConfigIndex++] = "FieldTech";
$AIEquipmentSet[HeavyEMPTurretSet, $EquipConfigIndex++] = "TurretEMPDeployable";
$AIEquipmentSet[HeavyEMPTurretSet, $EquipConfigIndex++] = "ElfCannon";
$AIEquipmentSet[HeavyEMPTurretSet, $EquipConfigIndex++] = "EMPLauncher";
$AIEquipmentSet[HeavyEMPTurretSet, $EquipConfigIndex++] = "EMPLauncherAmmo";
$AIEquipmentSet[HeavyEMPTurretSet, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[HeavyEMPTurretSet, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[HeavyEMPTurretSet, $EquipConfigIndex++] = "AntiAirGun";
$AIEquipmentSet[HeavyEMPTurretSet, $EquipConfigIndex++] = "AntiAirGunAmmo";
$AIEquipmentSet[HeavyEMPTurretSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[HeavyEMPTurretSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[HeavyEMPTurretSet, $EquipConfigIndex++] = "ConcussionGrenade";
$AIEquipmentSet[HeavyEMPTurretSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "Heavy";
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "InventoryDeployable";
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "Plasma";
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "PlasmaAmmo";
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "Disc";
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "Hornet";
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "HornetAmmo";
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "Napalm";
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "TacticalNuke";
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[HeavyTailgunner, $EquipConfigIndex++] = "Heavy";
$AIEquipmentSet[HeavyTailgunner, $EquipConfigIndex++] = "AmmoPack";
$AIEquipmentSet[HeavyTailgunner, $EquipConfigIndex++] = "Hornet";
$AIEquipmentSet[HeavyTailgunner, $EquipConfigIndex++] = "HornetAmmo";
$AIEquipmentSet[HeavyTailgunner, $EquipConfigIndex++] = "Chaingun";
$AIEquipmentSet[HeavyTailgunner, $EquipConfigIndex++] = "ChaingunAmmo";
$AIEquipmentSet[HeavyTailgunner, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[HeavyTailgunner, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[HeavyTailgunner, $EquipConfigIndex++] = "AntiAirGun";
$AIEquipmentSet[HeavyTailgunner, $EquipConfigIndex++] = "AntiAirGunAmmo";
$AIEquipmentSet[HeavyTailgunner, $EquipConfigIndex++] = "MissileLauncher";
$AIEquipmentSet[HeavyTailgunner, $EquipConfigIndex++] = "MissileLauncherAmmo";
$AIEquipmentSet[HeavyTailgunner, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[HeavyTailgunner, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[HeavyTailgunner, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[HeavyTailgunner, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[HeavySensorJammer, $EquipConfigIndex++] = "Heavy";
$AIEquipmentSet[HeavySensorJammer, $EquipConfigIndex++] = "SensorJammerPack";
$AIEquipmentSet[HeavySensorJammer, $EquipConfigIndex++] = "Plasma";
$AIEquipmentSet[HeavySensorJammer, $EquipConfigIndex++] = "PlasmaAmmo";
// $AIEquipmentSet[HeavySensorJammer, $EquipConfigIndex++] = "AntiAirGun";
// $AIEquipmentSet[HeavySensorJammer, $EquipConfigIndex++] = "AntiAirGunAmmo";
$AIEquipmentSet[HeavySensorJammer, $EquipConfigIndex++] = "RailNade";
$AIEquipmentSet[HeavySensorJammer, $EquipConfigIndex++] = "RailNadeAmmo";
$AIEquipmentSet[HeavySensorJammer, $EquipConfigIndex++] = "SolarCharge";
$AIEquipmentSet[HeavySensorJammer, $EquipConfigIndex++] = "Mortar";
$AIEquipmentSet[HeavySensorJammer, $EquipConfigIndex++] = "MortarAmmo";
$AIEquipmentSet[HeavySensorJammer, $EquipConfigIndex++] = "Hornet";
$AIEquipmentSet[HeavySensorJammer, $EquipConfigIndex++] = "HornetAmmo";
$AIEquipmentSet[HeavySensorJammer, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[HeavySensorJammer, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[HeavySensorJammer, $EquipConfigIndex++] = "TacticalNuke";
$AIEquipmentSet[HeavySensorJammer, $EquipConfigIndex++] = "Mine";

//------------------------------

$EquipConfigIndex = -1;
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "Medium";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "RepairPack";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "Hornet";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "HornetAmmo";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "HeavyLauncher";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "HeavyLauncherAmmo";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "MissileLauncher";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "MissileLauncherAmmo";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "Medium";
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "ShieldPack";
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "Napalm";
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "Chaingun";
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "ChaingunAmmo";
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "Hornet";
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "HornetAmmo";
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "MissileLauncher";
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "MissileLauncherAmmo";
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "Medium";
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "EnergyPack";
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "HeavyLauncher";
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "HeavyLauncherAmmo";
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "Napalm";
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "Hornet";
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "HornetAmmo";
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "MissileLauncher";
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "MissileLauncherAmmo";
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[MediumEnergyELF, $EquipConfigIndex++] = "Medium";
$AIEquipmentSet[MediumEnergyELF, $EquipConfigIndex++] = "EnergyPack";
$AIEquipmentSet[MediumEnergyELF, $EquipConfigIndex++] = "ELFGun";
$AIEquipmentSet[MediumEnergyELF, $EquipConfigIndex++] = "HeavyLauncher";
$AIEquipmentSet[MediumEnergyELF, $EquipConfigIndex++] = "HeavyLauncherAmmo";
$AIEquipmentSet[MediumEnergyELF, $EquipConfigIndex++] = "Hornet";
$AIEquipmentSet[MediumEnergyELF, $EquipConfigIndex++] = "HornetAmmo";
$AIEquipmentSet[MediumEnergyELF, $EquipConfigIndex++] = "MissileLauncher";
$AIEquipmentSet[MediumEnergyELF, $EquipConfigIndex++] = "MissileLauncherAmmo";
$AIEquipmentSet[MediumEnergyELF, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[MediumEnergyELF, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[MediumEnergyELF, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[MediumEnergyELF, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "Medium";
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "AmmoPack";
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "HeavyLauncher";
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "HeavyLauncherAmmo";
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "Hornet";
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "HornetAmmo";
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "MissileLauncher";
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "MissileLauncherAmmo";
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[MediumElementShield, $EquipConfigIndex++] = "Medium";
$AIEquipmentSet[MediumElementShield, $EquipConfigIndex++] = "ElementShieldPack";
$AIEquipmentSet[MediumElementShield, $EquipConfigIndex++] = "Plasma";
$AIEquipmentSet[MediumElementShield, $EquipConfigIndex++] = "PlasmaAmmo";
$AIEquipmentSet[MediumElementShield, $EquipConfigIndex++] = "Napalm";
$AIEquipmentSet[MediumElementShield, $EquipConfigIndex++] = "Hornet";
$AIEquipmentSet[MediumElementShield, $EquipConfigIndex++] = "HornetAmmo";
$AIEquipmentSet[MediumElementShield, $EquipConfigIndex++] = "MissileLauncher";
$AIEquipmentSet[MediumElementShield, $EquipConfigIndex++] = "MissileLauncherAmmo";
$AIEquipmentSet[MediumElementShield, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[MediumElementShield, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[MediumElementShield, $EquipConfigIndex++] = "TimeBomb";
$AIEquipmentSet[MediumElementShield, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "FieldTech";     // Turret monkey must be FieldTech - pinkpanther
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "TurretIndoorDeployable";
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "EMPLauncher";
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "EMPLauncherAmmo";
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "MissileLauncher";
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "MissileLauncherAmmo";
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "Plasma";
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "PlasmaAmmo";
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "ConcussionGrenade";
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "FieldTech";      // Turret monkey must be FieldTech - pinkpanther
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "TurretOutdoorDeployable";
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "EMPLauncher";
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "EMPLauncherAmmo";
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "AntiAirGun";
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "AntiAirGunAmmo";
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "MissileLauncher";
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "MissileLauncherAmmo";
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
// Turret monkey must be FieldTech - pinkpanther
$AIEquipmentSet[MediumLaserTurretSet, $EquipConfigIndex++] = "FieldTech";
$AIEquipmentSet[MediumLaserTurretSet, $EquipConfigIndex++] = "TurretLaserDeployable";
$AIEquipmentSet[MediumLaserTurretSet, $EquipConfigIndex++] = "EMPLauncher";
$AIEquipmentSet[MediumLaserTurretSet, $EquipConfigIndex++] = "EMPLauncherAmmo";
$AIEquipmentSet[MediumLaserTurretSet, $EquipConfigIndex++] = "AntiAirGun";
$AIEquipmentSet[MediumLaserTurretSet, $EquipConfigIndex++] = "AntiAirGunAmmo";
$AIEquipmentSet[MediumLaserTurretSet, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[MediumLaserTurretSet, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[MediumLaserTurretSet, $EquipConfigIndex++] = "MissileLauncher";
$AIEquipmentSet[MediumLaserTurretSet, $EquipConfigIndex++] = "MissileLauncherAmmo";
$AIEquipmentSet[MediumLaserTurretSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[MediumLaserTurretSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[MediumLaserTurretSet, $EquipConfigIndex++] = "StickyBomb";
$AIEquipmentSet[MediumLaserTurretSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
// Turret monkey must be FieldTech - pinkpanther
$AIEquipmentSet[MediumEMPTurretSet, $EquipConfigIndex++] = "FieldTech";
$AIEquipmentSet[MediumEMPTurretSet, $EquipConfigIndex++] = "TurretEMPDeployable";
$AIEquipmentSet[MediumEMPTurretSet, $EquipConfigIndex++] = "EMPLauncher";
$AIEquipmentSet[MediumEMPTurretSet, $EquipConfigIndex++] = "EMPLauncherAmmo";
$AIEquipmentSet[MediumEMPTurretSet, $EquipConfigIndex++] = "AntiAirGun";
$AIEquipmentSet[MediumEMPTurretSet, $EquipConfigIndex++] = "AntiAirGunAmmo";
$AIEquipmentSet[MediumEMPTurretSet, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[MediumEMPTurretSet, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[MediumEMPTurretSet, $EquipConfigIndex++] = "MissileLauncher";
$AIEquipmentSet[MediumEMPTurretSet, $EquipConfigIndex++] = "MissileLauncherAmmo";
$AIEquipmentSet[MediumEMPTurretSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[MediumEMPTurretSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[MediumEMPTurretSet, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[MediumEMPTurretSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "FieldTech";
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "InventoryDeployable";
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "RepairGun2";
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "Plasma";
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "PlasmaAmmo";
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "MissileLauncher";
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "MissileLauncherAmmo";
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[MediumPulseSet, $EquipConfigIndex++] = "FieldTech";
$AIEquipmentSet[MediumPulseSet, $EquipConfigIndex++] = "PulseSensorDeployable";
$AIEquipmentSet[MediumPulseSet, $EquipConfigIndex++] = "EMPLauncher";
$AIEquipmentSet[MediumPulseSet, $EquipConfigIndex++] = "EMPLauncherAmmo";
$AIEquipmentSet[MediumPulseSet, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[MediumPulseSet, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[MediumPulseSet, $EquipConfigIndex++] = "RepairGun2";
$AIEquipmentSet[MediumPulseSet, $EquipConfigIndex++] = "MissileLauncher";
$AIEquipmentSet[MediumPulseSet, $EquipConfigIndex++] = "MissileLauncherAmmo";
$AIEquipmentSet[MediumPulseSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[MediumPulseSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[MediumPulseSet, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[MediumPulseSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[MediumMotionSet, $EquipConfigIndex++] = "FieldTech";
$AIEquipmentSet[MediumMotionSet, $EquipConfigIndex++] = "MotionSensorDeployable";
$AIEquipmentSet[MediumMotionSet, $EquipConfigIndex++] = "EMPLauncher";
$AIEquipmentSet[MediumMotionSet, $EquipConfigIndex++] = "EMPLauncherAmmo";
$AIEquipmentSet[MediumMotionSet, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[MediumMotionSet, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[MediumMotionSet, $EquipConfigIndex++] = "RepairGun2";
$AIEquipmentSet[MediumMotionSet, $EquipConfigIndex++] = "MissileLauncher";
$AIEquipmentSet[MediumMotionSet, $EquipConfigIndex++] = "MissileLauncherAmmo";
$AIEquipmentSet[MediumMotionSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[MediumMotionSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[MediumMotionSet, $EquipConfigIndex++] = "CameraGrenade";
$AIEquipmentSet[MediumMotionSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[MediumTailgunner, $EquipConfigIndex++] = "FieldTech";
$AIEquipmentSet[MediumTailgunner, $EquipConfigIndex++] = "AmmoPack";
$AIEquipmentSet[MediumTailgunner, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[MediumTailgunner, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[MediumTailgunner, $EquipConfigIndex++] = "RepairGun2";
$AIEquipmentSet[MediumTailgunner, $EquipConfigIndex++] = "AntiAirGun";
$AIEquipmentSet[MediumTailgunner, $EquipConfigIndex++] = "AntiAirGunAmmo";
$AIEquipmentSet[MediumTailgunner, $EquipConfigIndex++] = "MissileLauncher";
$AIEquipmentSet[MediumTailgunner, $EquipConfigIndex++] = "MissileLauncherAmmo";
$AIEquipmentSet[MediumTailgunner, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[MediumTailgunner, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[MediumTailgunner, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[MediumTailgunner, $EquipConfigIndex++] = "Mine";

//------------------------------

$EquipConfigIndex = -1;
$AIEquipmentSet[LightEnergyDefault, $EquipConfigIndex++] = "Light";
$AIEquipmentSet[LightEnergyDefault, $EquipConfigIndex++] = "EnergyPack";
$AIEquipmentSet[LightEnergyDefault, $EquipConfigIndex++] = "Shotgun";
$AIEquipmentSet[LightEnergyDefault, $EquipConfigIndex++] = "ShotgunAmmo";
$AIEquipmentSet[LightEnergyDefault, $EquipConfigIndex++] = "Disc";
$AIEquipmentSet[LightEnergyDefault, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[LightEnergyDefault, $EquipConfigIndex++] = "GrenadeLauncher";
$AIEquipmentSet[LightEnergyDefault, $EquipConfigIndex++] = "GrenadeLauncherAmmo";
$AIEquipmentSet[LightEnergyDefault, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[LightEnergyDefault, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[LightEnergyDefault, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[LightEnergyDefault, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[LightElementShield, $EquipConfigIndex++] = "Light";
$AIEquipmentSet[LightElementShield, $EquipConfigIndex++] = "ElementShieldPack";
$AIEquipmentSet[LightElementShield, $EquipConfigIndex++] = "BeamSword";
$AIEquipmentSet[LightElementShield, $EquipConfigIndex++] = "Shotgun";
$AIEquipmentSet[LightElementShield, $EquipConfigIndex++] = "ShotgunAmmo";
$AIEquipmentSet[LightElementShield, $EquipConfigIndex++] = "Disc";
$AIEquipmentSet[LightElementShield, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[LightElementShield, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[LightElementShield, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[LightElementShield, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[LightElementShield, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "Sniper";
$AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "EnergyPack";
//$AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "SniperRifle";
$AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "SlugRifle";
$AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "SlugRifleAmmo";
$AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "Disc";
$AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "DiscAmmo";
// $AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "RailCannon";
// $AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "RailCannonAmmo";
$AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "Shocklance";
$AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "Light";
$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "EnergyPack";
$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "ELFGun";
$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "Shotgun";
$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "ShotgunAmmo";
$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "Disc";
$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "Light";
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "ShieldPack";
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "Shotgun";
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "ShotgunAmmo";
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "Disc";
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "Chaingun";
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "ChaingunAmmo";
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "ConcussionGrenade";
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[LightCloakSet, $EquipConfigIndex++] = "Light";
$AIEquipmentSet[LightCloakSet, $EquipConfigIndex++] = "CloakingPack";
$AIEquipmentSet[LightCloakSet, $EquipConfigIndex++] = "Shotgun";
$AIEquipmentSet[LightCloakSet, $EquipConfigIndex++] = "ShotgunAmmo";
$AIEquipmentSet[LightCloakSet, $EquipConfigIndex++] = "Disc";
$AIEquipmentSet[LightCloakSet, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[LightCloakSet, $EquipConfigIndex++] = "ShockLance";
//$AIEquipmentSet[LightCloakSet, $EquipConfigIndex++] = "Napalm";
//$AIEquipmentSet[LightCloakSet, $EquipConfigIndex++] = "BeamSword";
$AIEquipmentSet[LightCloakSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[LightCloakSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[LightCloakSet, $EquipConfigIndex++] = "FlashGrenade";
$AIEquipmentSet[LightCloakSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[LightRepairSet, $EquipConfigIndex++] = "Light";
$AIEquipmentSet[LightRepairSet, $EquipConfigIndex++] = "RepairPack";
$AIEquipmentSet[LightRepairSet, $EquipConfigIndex++] = "Shotgun";
$AIEquipmentSet[LightRepairSet, $EquipConfigIndex++] = "ShotgunAmmo";
$AIEquipmentSet[LightRepairSet, $EquipConfigIndex++] = "Chaingun";
$AIEquipmentSet[LightRepairSet, $EquipConfigIndex++] = "ChaingunAmmo";
$AIEquipmentSet[LightRepairSet, $EquipConfigIndex++] = "Disc";
$AIEquipmentSet[LightRepairSet, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[LightRepairSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[LightRepairSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[LightRepairSet, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[LightRepairSet, $EquipConfigIndex++] = "Mine";

//new set - Lagg...
$EquipConfigIndex = -1;
$AIEquipmentSet[LightCameraSet, $EquipConfigIndex++] = "Light";
$AIEquipmentSet[LightCameraSet, $EquipConfigIndex++] = "MotionSensorDeployable";
$AIEquipmentSet[LightCameraSet, $EquipConfigIndex++] = "Shotgun";
$AIEquipmentSet[LightCameraSet, $EquipConfigIndex++] = "ShotgunAmmo";
$AIEquipmentSet[LightCameraSet, $EquipConfigIndex++] = "BeamSword";
$AIEquipmentSet[LightCameraSet, $EquipConfigIndex++] = "Disc";
$AIEquipmentSet[LightCameraSet, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[LightCameraSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[LightCameraSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[LightCameraSet, $EquipConfigIndex++] = "CameraGrenade";
$AIEquipmentSet[LightCameraSet, $EquipConfigIndex++] = "Mine";

//new set - Lagg...
$EquipConfigIndex = -1;
$AIEquipmentSet[LightSensorJammer, $EquipConfigIndex++] = "Light";
$AIEquipmentSet[LightSensorJammer, $EquipConfigIndex++] = "SensorJammerPack";
$AIEquipmentSet[LightSensorJammer, $EquipConfigIndex++] = "BeamSword";
$AIEquipmentSet[LightSensorJammer, $EquipConfigIndex++] = "Shotgun";
$AIEquipmentSet[LightSensorJammer, $EquipConfigIndex++] = "ShotgunAmmo";
$AIEquipmentSet[LightSensorJammer, $EquipConfigIndex++] = "Disc";
$AIEquipmentSet[LightSensorJammer, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[LightSensorJammer, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[LightSensorJammer, $EquipConfigIndex++] = "RepairKit";
//$AIEquipmentSet[LightSensorJammer, $EquipConfigIndex++] = "BoostGrenade";
$AIEquipmentSet[LightSensorJammer, $EquipConfigIndex++] = "TimeBomb";
$AIEquipmentSet[LightSensorJammer, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[LightAttacker, $EquipConfigIndex++] = "Light";
$AIEquipmentSet[LightAttacker, $EquipConfigIndex++] = "SensorJammerPack";
$AIEquipmentSet[LightAttacker, $EquipConfigIndex++] = "BeamSword";
$AIEquipmentSet[LightAttacker, $EquipConfigIndex++] = "Plasma";
$AIEquipmentSet[LightAttacker, $EquipConfigIndex++] = "PlasmaAmmo";
$AIEquipmentSet[LightAttacker, $EquipConfigIndex++] = "Shotgun";
$AIEquipmentSet[LightAttacker, $EquipConfigIndex++] = "ShotgunAmmo";
//$AIEquipmentSet[LightAttacker, $EquipConfigIndex++] = "Disc";
//$AIEquipmentSet[LightAttacker, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[LightAttacker, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[LightAttacker, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[LightAttacker, $EquipConfigIndex++] = "TimeBomb";
//$AIEquipmentSet[LightAttacker, $EquipConfigIndex++] = "BoostGrenade";
$AIEquipmentSet[LightAttacker, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[TitanDefenderFlag, $EquipConfigIndex++] = "Titan";
$AIEquipmentSet[TitanDefenderFlag, $EquipConfigIndex++] = "AmmoPack";
//$AIEquipmentSet[TitanDefenderFlag, $EquipConfigIndex++] = "ShieldPack";
//$AIEquipmentSet[TitanDefenderFlag, $EquipConfigIndex++] = "HeavyLaser";
//$AIEquipmentSet[TitanDefenderFlag, $EquipConfigIndex++] = "Shocklance";
$AIEquipmentSet[TitanDefenderFlag, $EquipConfigIndex++] = "RailNade";
$AIEquipmentSet[TitanDefenderFlag, $EquipConfigIndex++] = "RailNadeAmmo";
$AIEquipmentSet[TitanDefenderFlag, $EquipConfigIndex++] = "TitanChaingun";
$AIEquipmentSet[TitanDefenderFlag, $EquipConfigIndex++] = "TitanChaingunAmmo";
$AIEquipmentSet[TitanDefenderFlag, $EquipConfigIndex++] = "MortarStorm";
$AIEquipmentSet[TitanDefenderFlag, $EquipConfigIndex++] = "MortarStormAmmo";
$AIEquipmentSet[TitanDefenderFlag, $EquipConfigIndex++] = "SolarCharge";
$AIEquipmentSet[TitanDefenderFlag, $EquipConfigIndex++] = "AntiAirGun";
$AIEquipmentSet[TitanDefenderFlag, $EquipConfigIndex++] = "AntiAirGunAmmo";
$AIEquipmentSet[TitanDefenderFlag, $EquipConfigIndex++] = "Hornet";
$AIEquipmentSet[TitanDefenderFlag, $EquipConfigIndex++] = "HornetAmmo";
$AIEquipmentSet[TitanDefenderFlag, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[TitanDefenderFlag, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[TitanDefenderFlag, $EquipConfigIndex++] = "StickyBomb";
//$AIEquipmentSet[TitanDefenderFlag, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[TitanDefenderGens, $EquipConfigIndex++] = "Titan";
$AIEquipmentSet[TitanDefenderGens, $EquipConfigIndex++] = "RepairPack";
$AIEquipmentSet[TitanDefenderGens, $EquipConfigIndex++] = "Shocklance";
$AIEquipmentSet[TitanDefenderGens, $EquipConfigIndex++] = "TitanChaingun";
$AIEquipmentSet[TitanDefenderGens, $EquipConfigIndex++] = "TitanChaingunAmmo";
$AIEquipmentSet[TitanDefenderGens, $EquipConfigIndex++] = "RailNade";
$AIEquipmentSet[TitanDefenderGens, $EquipConfigIndex++] = "RailNadeAmmo";
$AIEquipmentSet[TitanDefenderGens, $EquipConfigIndex++] = "Hornet";
$AIEquipmentSet[TitanDefenderGens, $EquipConfigIndex++] = "HornetAmmo";
$AIEquipmentSet[TitanDefenderGens, $EquipConfigIndex++] = "Plasma";
$AIEquipmentSet[TitanDefenderGens, $EquipConfigIndex++] = "PlasmaAmmo";
$AIEquipmentSet[TitanDefenderGens, $EquipConfigIndex++] = "MissileLauncher";
$AIEquipmentSet[TitanDefenderGens, $EquipConfigIndex++] = "MissileLauncherAmmo";
$AIEquipmentSet[TitanDefenderGens, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[TitanDefenderGens, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[TitanDefenderGens, $EquipConfigIndex++] = "StickyBomb";
//$AIEquipmentSet[TitanDefenderGens, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[TitanNuke, $EquipConfigIndex++] = "Titan";
$AIEquipmentSet[TitanNuke, $EquipConfigIndex++] = "EnergyPack";
$AIEquipmentSet[TitanNuke, $EquipConfigIndex++] = "WidowMaker";
$AIEquipmentSet[TitanNuke, $EquipConfigIndex++] = "WidowMakerAmmo";
$AIEquipmentSet[TitanNuke, $EquipConfigIndex++] = "HeavyLaser";
$AIEquipmentSet[TitanNuke, $EquipConfigIndex++] = "TitanChaingun";
$AIEquipmentSet[TitanNuke, $EquipConfigIndex++] = "TitanChaingunAmmo";
$AIEquipmentSet[TitanNuke, $EquipConfigIndex++] = "SolarCharge";
$AIEquipmentSet[TitanNuke, $EquipConfigIndex++] = "MortarStorm";
$AIEquipmentSet[TitanNuke, $EquipConfigIndex++] = "MortarStormAmmo";
$AIEquipmentSet[TitanNuke, $EquipConfigIndex++] = "Hornet";
$AIEquipmentSet[TitanNuke, $EquipConfigIndex++] = "HornetAmmo";
$AIEquipmentSet[TitanNuke, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[TitanNuke, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[TitanNuke, $EquipConfigIndex++] = "TacticalNuke";
$AIEquipmentSet[TitanNuke, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[TitanNuke2, $EquipConfigIndex++] = "Titan";
$AIEquipmentSet[TitanNuke2, $EquipConfigIndex++] = "EnergyPack";
$AIEquipmentSet[TitanNuke2, $EquipConfigIndex++] = "TitanChaingun";
$AIEquipmentSet[TitanNuke2, $EquipConfigIndex++] = "TitanChaingunAmmo";
$AIEquipmentSet[TitanNuke2, $EquipConfigIndex++] = "WidowMaker";
$AIEquipmentSet[TitanNuke2, $EquipConfigIndex++] = "WidowMakerAmmo";
$AIEquipmentSet[TitanNuke2, $EquipConfigIndex++] = "RailNade";
$AIEquipmentSet[TitanNuke2, $EquipConfigIndex++] = "RailNadeAmmo";
$AIEquipmentSet[TitanNuke2, $EquipConfigIndex++] = "Hornet";
$AIEquipmentSet[TitanNuke2, $EquipConfigIndex++] = "HornetAmmo";
$AIEquipmentSet[TitanNuke2, $EquipConfigIndex++] = "SolarCharge";
$AIEquipmentSet[TitanNuke2, $EquipConfigIndex++] = "MortarStorm";
$AIEquipmentSet[TitanNuke2, $EquipConfigIndex++] = "MortarStormAmmo";
$AIEquipmentSet[TitanNuke2, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[TitanNuke2, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[TitanNuke2, $EquipConfigIndex++] = "TacticalNuke";
$AIEquipmentSet[TitanNuke2, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[TitanElementShield, $EquipConfigIndex++] = "Titan";
$AIEquipmentSet[TitanElementShield, $EquipConfigIndex++] = "ElementShieldPack";
$AIEquipmentSet[TitanElementShield, $EquipConfigIndex++] = "WidowMaker";
$AIEquipmentSet[TitanElementShield, $EquipConfigIndex++] = "WidowMakerAmmo";
$AIEquipmentSet[TitanElementShield, $EquipConfigIndex++] = "TitanChaingun";
$AIEquipmentSet[TitanElementShield, $EquipConfigIndex++] = "TitanChaingunAmmo";
$AIEquipmentSet[TitanElementShield, $EquipConfigIndex++] = "RailNade";
$AIEquipmentSet[TitanElementShield, $EquipConfigIndex++] = "RailNadeAmmo";
$AIEquipmentSet[TitanElementShield, $EquipConfigIndex++] = "SolarCharge";
$AIEquipmentSet[TitanElementShield, $EquipConfigIndex++] = "MortarStorm";
$AIEquipmentSet[TitanElementShield, $EquipConfigIndex++] = "MortarStormAmmo";
$AIEquipmentSet[TitanElementShield, $EquipConfigIndex++] = "Hornet";
$AIEquipmentSet[TitanElementShield, $EquipConfigIndex++] = "HornetAmmo";
$AIEquipmentSet[TitanElementShield, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[TitanElementShield, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[TitanElementShield, $EquipConfigIndex++] = "TacticalNuke";
$AIEquipmentSet[TitanElementShield, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[TitanSensorJammer, $EquipConfigIndex++] = "Titan";
$AIEquipmentSet[TitanSensorJammer, $EquipConfigIndex++] = "SensorJammerPack";
$AIEquipmentSet[TitanSensorJammer, $EquipConfigIndex++] = "WidowMaker";
$AIEquipmentSet[TitanSensorJammer, $EquipConfigIndex++] = "WidowMakerAmmo";
$AIEquipmentSet[TitanSensorJammer, $EquipConfigIndex++] = "TitanChaingun";
$AIEquipmentSet[TitanSensorJammer, $EquipConfigIndex++] = "TitanChaingunAmmo";
$AIEquipmentSet[TitanSensorJammer, $EquipConfigIndex++] = "RailNade";
$AIEquipmentSet[TitanSensorJammer, $EquipConfigIndex++] = "RailNadeAmmo";
$AIEquipmentSet[TitanSensorJammer, $EquipConfigIndex++] = "SolarCharge";
$AIEquipmentSet[TitanSensorJammer, $EquipConfigIndex++] = "MortarStorm";
$AIEquipmentSet[TitanSensorJammer, $EquipConfigIndex++] = "MortarStormAmmo";
$AIEquipmentSet[TitanSensorJammer, $EquipConfigIndex++] = "Hornet";
$AIEquipmentSet[TitanSensorJammer, $EquipConfigIndex++] = "HornetAmmo";
$AIEquipmentSet[TitanSensorJammer, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[TitanSensorJammer, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[TitanSensorJammer, $EquipConfigIndex++] = "TacticalNuke";
$AIEquipmentSet[TitanSensorJammer, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[MediumEnergyTriumph, $EquipConfigIndex++] = "Medium";
$AIEquipmentSet[MediumEnergyTriumph, $EquipConfigIndex++] = "EnergyPack";
$AIEquipmentSet[MediumEnergyTriumph, $EquipConfigIndex++] = "HeavyLauncher";
$AIEquipmentSet[MediumEnergyTriumph, $EquipConfigIndex++] = "HeavyLauncherAmmo";
$AIEquipmentSet[MediumEnergyTriumph, $EquipConfigIndex++] = "Shocklance";
$AIEquipmentSet[MediumEnergyTriumph, $EquipConfigIndex++] = "Hornet";
$AIEquipmentSet[MediumEnergyTriumph, $EquipConfigIndex++] = "HornetAmmo";
$AIEquipmentSet[MediumEnergyTriumph, $EquipConfigIndex++] = "AntiAirGun";
$AIEquipmentSet[MediumEnergyTriumph, $EquipConfigIndex++] = "AntiAirGunAmmo";
$AIEquipmentSet[MediumEnergyTriumph, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[MediumEnergyTriumph, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[MediumEnergyTriumph, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[MediumEnergyTriumph, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[MediumAttacker, $EquipConfigIndex++] = "Medium";
$AIEquipmentSet[MediumAttacker, $EquipConfigIndex++] = "SensorJammerPack";
$AIEquipmentSet[MediumAttacker, $EquipConfigIndex++] = "Plasma";
$AIEquipmentSet[MediumAttacker, $EquipConfigIndex++] = "PlasmaAmmo";
$AIEquipmentSet[MediumAttacker, $EquipConfigIndex++] = "Napalm";
$AIEquipmentSet[MediumAttacker, $EquipConfigIndex++] = "Hornet";
$AIEquipmentSet[MediumAttacker, $EquipConfigIndex++] = "HornetAmmo";
$AIEquipmentSet[MediumAttacker, $EquipConfigIndex++] = "AntiAirGun";
$AIEquipmentSet[MediumAttacker, $EquipConfigIndex++] = "AntiAirGunAmmo";
$AIEquipmentSet[MediumAttacker, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[MediumAttacker, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[MediumAttacker, $EquipConfigIndex++] = "TimeBomb";
$AIEquipmentSet[MediumAttacker, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[MediumSensorJammerTriumph, $EquipConfigIndex++] = "Medium";
$AIEquipmentSet[MediumSensorJammerTriumph, $EquipConfigIndex++] = "SensorJammerPack";
$AIEquipmentSet[MediumSensorJammerTriumph, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[MediumSensorJammerTriumph, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[MediumSensorJammerTriumph, $EquipConfigIndex++] = "AntiAirGun";
$AIEquipmentSet[MediumSensorJammerTriumph, $EquipConfigIndex++] = "AntiAirGunAmmo";
$AIEquipmentSet[MediumSensorJammerTriumph, $EquipConfigIndex++] = "Hornet";
$AIEquipmentSet[MediumSensorJammerTriumph, $EquipConfigIndex++] = "HornetAmmo";
//$AIEquipmentSet[MediumSensorJammerTriumph, $EquipConfigIndex++] = "GreenLantern";
//$AIEquipmentSet[MediumSensorJammerTriumph, $EquipConfigIndex++] = "GreenLanternAmmo";
$AIEquipmentSet[MediumSensorJammerTriumph, $EquipConfigIndex++] = "Plasma";
$AIEquipmentSet[MediumSensorJammerTriumph, $EquipConfigIndex++] = "PlasmaAmmo";
$AIEquipmentSet[MediumSensorJammerTriumph, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[MediumSensorJammerTriumph, $EquipConfigIndex++] = "RepairKit";
//$AIEquipmentSet[MediumSensorJammerTriumph, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[MediumSensorJammerTriumph, $EquipConfigIndex++] = "TimeBomb";
$AIEquipmentSet[MediumSensorJammerTriumph, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[TurretSniper, $EquipConfigIndex++] = "Sniper";
$AIEquipmentSet[TurretSniper, $EquipConfigIndex++] = "EnergyPack";
$AIEquipmentSet[TurretSniper, $EquipConfigIndex++] = "RailCannon";
$AIEquipmentSet[TurretSniper, $EquipConfigIndex++] = "RailCannonAmmo";
$AIEquipmentSet[TurretSniper, $EquipConfigIndex++] = "SlugRifle";
$AIEquipmentSet[TurretSniper, $EquipConfigIndex++] = "SlugRifleAmmo";
$AIEquipmentSet[TurretSniper, $EquipConfigIndex++] = "Disc";
$AIEquipmentSet[TurretSniper, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[TurretSniper, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[TurretSniper, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[TurretSniper, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[TurretSniper, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[SniperElementShield, $EquipConfigIndex++] = "Sniper";
$AIEquipmentSet[SniperElementShield, $EquipConfigIndex++] = "ElementShieldPack";
$AIEquipmentSet[SniperElementShield, $EquipConfigIndex++] = "RailCannon";
$AIEquipmentSet[SniperElementShield, $EquipConfigIndex++] = "RailCannonAmmo";
$AIEquipmentSet[SniperElementShield, $EquipConfigIndex++] = "SlugRifle";
$AIEquipmentSet[SniperElementShield, $EquipConfigIndex++] = "SlugRifleAmmo";
$AIEquipmentSet[SniperElementShield, $EquipConfigIndex++] = "Disc";
$AIEquipmentSet[SniperElementShield, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[SniperElementShield, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[SniperElementShield, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[SniperElementShield, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[SniperElementShield, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[FieldTechEnergySet, $EquipConfigIndex++] = "FieldTech";
$AIEquipmentSet[FieldTechEnergySet, $EquipConfigIndex++] = "EnergyPack";
$AIEquipmentSet[FieldTechEnergySet, $EquipConfigIndex++] = "RepairGun2";
$AIEquipmentSet[FieldTechEnergySet, $EquipConfigIndex++] = "HeavyLauncher";
$AIEquipmentSet[FieldTechEnergySet, $EquipConfigIndex++] = "HeavyLauncherAmmo";
$AIEquipmentSet[FieldTechEnergySet, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[FieldTechEnergySet, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[FieldTechEnergySet, $EquipConfigIndex++] = "MissileLauncher";
$AIEquipmentSet[FieldTechEnergySet, $EquipConfigIndex++] = "MissileLauncherAmmo";
$AIEquipmentSet[FieldTechEnergySet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[FieldTechEnergySet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[FieldTechEnergySet, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[FieldTechEnergySet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[FieldTechRepairSet, $EquipConfigIndex++] = "FieldTech";
//$AIEquipmentSet[FieldTechRepairSet, $EquipConfigIndex++] = "RepairPack";
$AIEquipmentSet[FieldTechRepairSet, $EquipConfigIndex++] = "AmmoPack";
//$AIEquipmentSet[FieldTechRepairSet, $EquipConfigIndex++] = "ElfCannon";
$AIEquipmentSet[FieldTechRepairSet, $EquipConfigIndex++] = "RepairGun2";
$AIEquipmentSet[FieldTechRepairSet, $EquipConfigIndex++] = "HeavyLauncher";
$AIEquipmentSet[FieldTechRepairSet, $EquipConfigIndex++] = "HeavyLauncherAmmo";
$AIEquipmentSet[FieldTechRepairSet, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[FieldTechRepairSet, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[FieldTechRepairSet, $EquipConfigIndex++] = "EMPLauncher";
$AIEquipmentSet[FieldTechRepairSet, $EquipConfigIndex++] = "EMPLauncherAmmo";
$AIEquipmentSet[FieldTechRepairSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[FieldTechRepairSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[FieldTechRepairSet, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[FieldTechRepairSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[FieldTechAmmoSet, $EquipConfigIndex++] = "FieldTech";
$AIEquipmentSet[FieldTechAmmoSet, $EquipConfigIndex++] = "AmmoPack";
$AIEquipmentSet[FieldTechAmmoSet, $EquipConfigIndex++] = "EMPLauncher";
$AIEquipmentSet[FieldTechAmmoSet, $EquipConfigIndex++] = "EMPLauncherAmmo";
$AIEquipmentSet[FieldTechAmmoSet, $EquipConfigIndex++] = "HeavyLauncher";
$AIEquipmentSet[FieldTechAmmoSet, $EquipConfigIndex++] = "HeavyLauncherAmmo";
$AIEquipmentSet[FieldTechAmmoSet, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[FieldTechAmmoSet, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[FieldTechAmmoSet, $EquipConfigIndex++] = "MissileLauncher";
$AIEquipmentSet[FieldTechAmmoSet, $EquipConfigIndex++] = "MissileLauncherAmmo";
$AIEquipmentSet[FieldTechAmmoSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[FieldTechAmmoSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[FieldTechAmmoSet, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[FieldTechAmmoSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[FieldTechSensorJammer, $EquipConfigIndex++] = "FieldTech";
$AIEquipmentSet[FieldTechSensorJammer, $EquipConfigIndex++] = "SensorJammerPack";
$AIEquipmentSet[FieldTechSensorJammer, $EquipConfigIndex++] = "RepairGun2";
$AIEquipmentSet[FieldTechSensorJammer, $EquipConfigIndex++] = "Plasma";
$AIEquipmentSet[FieldTechSensorJammer, $EquipConfigIndex++] = "PlasmaAmmo";
$AIEquipmentSet[FieldTechSensorJammer, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[FieldTechSensorJammer, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[FieldTechSensorJammer, $EquipConfigIndex++] = "MissileLauncher";
$AIEquipmentSet[FieldTechSensorJammer, $EquipConfigIndex++] = "MissileLauncherAmmo";
$AIEquipmentSet[FieldTechSensorJammer, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[FieldTechSensorJammer, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[FieldTechSensorJammer, $EquipConfigIndex++] = "StickyBomb";
$AIEquipmentSet[FieldTechSensorJammer, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[FieldTechElementShield, $EquipConfigIndex++] = "FieldTech";
$AIEquipmentSet[FieldTechElementShield, $EquipConfigIndex++] = "ElementShieldPack";
$AIEquipmentSet[FieldTechElementShield, $EquipConfigIndex++] = "RepairGun2";
$AIEquipmentSet[FieldTechElementShield, $EquipConfigIndex++] = "Plasma";
$AIEquipmentSet[FieldTechElementShield, $EquipConfigIndex++] = "PlasmaAmmo";
$AIEquipmentSet[FieldTechElementShield, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[FieldTechElementShield, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[FieldTechElementShield, $EquipConfigIndex++] = "MissileLauncher";
$AIEquipmentSet[FieldTechElementShield, $EquipConfigIndex++] = "MissileLauncherAmmo";
$AIEquipmentSet[FieldTechElementShield, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[FieldTechElementShield, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[FieldTechElementShield, $EquipConfigIndex++] = "StickyBomb";
$AIEquipmentSet[FieldTechElementShield, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[FieldTechBunkerSet, $EquipConfigIndex++] = "FieldTech";
$AIEquipmentSet[FieldTechBunkerSet, $EquipConfigIndex++] = "BunkerDeployable";
$AIEquipmentSet[FieldTechBunkerSet, $EquipConfigIndex++] = "Plasma";
$AIEquipmentSet[FieldTechBunkerSet, $EquipConfigIndex++] = "PlasmaAmmo";
$AIEquipmentSet[FieldTechBunkerSet, $EquipConfigIndex++] = "RepairGun2";
$AIEquipmentSet[FieldTechBunkerSet, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[FieldTechBunkerSet, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[FieldTechBunkerSet, $EquipConfigIndex++] = "MissileLauncher";
$AIEquipmentSet[FieldTechBunkerSet, $EquipConfigIndex++] = "MissileLauncherAmmo";
$AIEquipmentSet[FieldTechBunkerSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[FieldTechBunkerSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[FieldTechBunkerSet, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[FieldTechBunkerSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[FieldTechTelePadSet, $EquipConfigIndex++] = "FieldTech";
$AIEquipmentSet[FieldTechTelePadSet, $EquipConfigIndex++] = "TelePadPack";
$AIEquipmentSet[FieldTechTelePadSet, $EquipConfigIndex++] = "Plasma";
$AIEquipmentSet[FieldTechTelePadSet, $EquipConfigIndex++] = "PlasmaAmmo";
$AIEquipmentSet[FieldTechTelePadSet, $EquipConfigIndex++] = "RepairGun2";
$AIEquipmentSet[FieldTechTelePadSet, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[FieldTechTelePadSet, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[FieldTechTelePadSet, $EquipConfigIndex++] = "MissileLauncher";
$AIEquipmentSet[FieldTechTelePadSet, $EquipConfigIndex++] = "MissileLauncherAmmo";
$AIEquipmentSet[FieldTechTelePadSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[FieldTechTelePadSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[FieldTechTelePadSet, $EquipConfigIndex++] = "FlareGrenade";
$AIEquipmentSet[FieldTechTelePadSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[FieldTechForceFieldSet, $EquipConfigIndex++] = "FieldTech";
$AIEquipmentSet[FieldTechForceFieldSet, $EquipConfigIndex++] = "ForceFieldDeployable";
$AIEquipmentSet[FieldTechForceFieldSet, $EquipConfigIndex++] = "RepairGun2";
$AIEquipmentSet[FieldTechForceFieldSet, $EquipConfigIndex++] = "EMPLauncher";
$AIEquipmentSet[FieldTechForceFieldSet, $EquipConfigIndex++] = "EMPLauncherAmmo";
$AIEquipmentSet[FieldTechForceFieldSet, $EquipConfigIndex++] = "MissileLauncher";
$AIEquipmentSet[FieldTechForceFieldSet, $EquipConfigIndex++] = "MissileLauncherAmmo";
$AIEquipmentSet[FieldTechForceFieldSet, $EquipConfigIndex++] = "GatlingGun";
$AIEquipmentSet[FieldTechForceFieldSet, $EquipConfigIndex++] = "GatlingGunAmmo";
$AIEquipmentSet[FieldTechForceFieldSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[FieldTechForceFieldSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[FieldTechForceFieldSet, $EquipConfigIndex++] = "ConcussionGrenade";
$AIEquipmentSet[FieldTechForceFieldSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[ReconShotgun, $EquipConfigIndex++] = "Recon";
$AIEquipmentSet[ReconShotgun, $EquipConfigIndex++] = "AntiEnergyPack";
$AIEquipmentSet[ReconShotgun, $EquipConfigIndex++] = "Shotgun";
$AIEquipmentSet[ReconShotgun, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[ReconShotgun, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[ReconShotgun, $EquipConfigIndex++] = "TimeBomb";
$AIEquipmentSet[ReconShotgun, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[ReconBeamSword, $EquipConfigIndex++] = "Recon";
$AIEquipmentSet[ReconBeamSword, $EquipConfigIndex++] = "AntiEnergyPack";
$AIEquipmentSet[ReconBeamSword, $EquipConfigIndex++] = "BeamSword";
$AIEquipmentSet[ReconBeamSword, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[ReconBeamSword, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[ReconBeamSword, $EquipConfigIndex++] = "TimeBomb";
$AIEquipmentSet[ReconBeamSword, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[ReconDualBlaster, $EquipConfigIndex++] = "Recon";
$AIEquipmentSet[ReconDualBlaster, $EquipConfigIndex++] = "AntiEnergyPack";
$AIEquipmentSet[ReconDualBlaster, $EquipConfigIndex++] = "DualBlaster";
$AIEquipmentSet[ReconDualBlaster, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[ReconDualBlaster, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[ReconDualBlaster, $EquipConfigIndex++] = "TimeBomb";
$AIEquipmentSet[ReconDualBlaster, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[ReconDeepCover, $EquipConfigIndex++] = "Recon";
$AIEquipmentSet[ReconDeepCover, $EquipConfigIndex++] = "DeepCoverPack";
$AIEquipmentSet[ReconDeepCover, $EquipConfigIndex++] = "Shotgun";
$AIEquipmentSet[ReconDeepCover, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[ReconDeepCover, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[ReconDeepCover, $EquipConfigIndex++] = "TimeBomb";
$AIEquipmentSet[ReconDeepCover, $EquipConfigIndex++] = "Mine";