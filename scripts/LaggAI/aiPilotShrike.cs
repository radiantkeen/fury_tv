//I would like to extend thanks to Founder (Martin Hoover) from WWW.GarageGames.com for his mathamatics help.
//ZOD, Dev and everybody else at www.tribes2maps.com/forums that helped me with scripts and/or ideas.
//After many months in development this file is still a work in progress, if you make any improvments please
//comment them and send me the upgraded file to JPerricone@nyc.rr.com (A.K.A. Lagg-Alot)

//----------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------- AIOPilotShrike::weight ---
//Set up to 10 simgroups called "T" @ %team @ "ShrikePath" @ %num
//(example T2ShrikePath1 to T2ShrikePath10 / T1ShrikePath1 to T1ShrikePath10)
//NOTE: must start with number 1 and count in order up to 10
//inside these simgroups place markers in the order to follow for each
//path. Thats 1 simgroup per path for each team - Lagg... 4-8-2004
//* NOTE *
//Give this guy plenty of room with the markers, he flys like a wild man :)

function AIOPilotShrike::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   //check if flight path exists
   if (Game.class $= "SiegeGame")
      %team = Game.offenseTeam == %client.team ? 1 : 2;
   else
      %team = %client.team;

   if(! isObject(nameToId("T" @ %team @ "ShrikePath1")))
      return 0;

   //do a quick check to disqualify this objective if it can't meet the minimum weight
   if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
   {
      if (%this.targetObjectId > 0 && %this.issuedByClientId == %client.controlByHuman)
      {
         if ($AIWeightHumanIssuedCommand < %minWeight)
       return 0;
      }
      else
         return 0;
   }

   //check for Vehicle station near buy of forget it if we are not mounted yet or didn't just buy - Lagg...
   if (!%client.player.isMounted() && !%client.justBought)
   {
      %clVs = AIFindClosestVStation(%client);
      if (%clVs > 0)
      {
         %closestVs = getWord(%clVs, 0);
         %closestVsDist = getWord(%clVs, 1);
//         if (%closestVsDist > 300)//----------------------* close to VPad or return 0 *---
//            return 0;
      }
      else
         return 0;

      //check if any of vehicle type are availible
      %blockName = getRandomVehicleType($VehicleList::Fighters); //"ScoutFlyer";
      %client.aiVehicleConsidering = %blockName;
      if (!vehicleCheck(%blockName, %client.team))
         return 0;
   }

   %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);

   //never bump a shrike pilot from his ride
   if (%this.clientLevel1 > 0  && %level == 1 && %this.clientLevel1 != %client)
      return 0;
   else if (%this.clientLevel1 == %client)
      %weight = 20000;

   return %weight;
}

function AIOPilotShrike::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIPilotShrike);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIOPilotShrike::unassignClient(%this, %client)
{
   if(%client.pilotVehicle)
      AIDisembarkVehicle(%client);

   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//-------------------------------------------------------------------------------------

function AIPilotShrike::initFromObjective(%task, %objective, %client)
{
   //initialize the task vars from the objective
   %task.objective = %objective;
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;
   if (%objective.Location !$= "")
      %task.location = %objective.location;
   else
      %task.location = %objective.targetObjectId.getWorldBoxCenter();

   %task.equipment = %objective.equipment;
   // Any human in the other team who likes to shoot our bot pilots in turbogravs? - triumph specific, removed
//   %isKillerHuman = false;
//   %count = ClientGroup.getCount();
//   for(%i = 0; %i < %count; %i++) {
//      %cl = ClientGroup.getObject(%i);
//      if (%cl.lastVehicleDestroyed $= "turbograv" && %cl.team != %client.team)
//         %isKillerHuman = true;
//   }
//   if (%isKillerHuman || %objective.vehicleDestroyed > 0) { // if before the vehicle was destroyed by a human, spawn invisible vehicles using DeepCoverPack - pinkpanther
//     %task.buyEquipmentSet =  "ReconDeepCover";
//     %task.desiredEquipment = "DeepCoverPack";
     //echo(%client.nameBase @ " goes stealth! ");
//   }
//   else {
     %task.buyEquipmentSet = %objective.buyEquipmentSet;
     %task.desiredEquipment = %objective.desiredEquipment;
//   }
   %task.issuedByClient = %objective.issuedByClientId;
   %task.chat = %objective.chat;

   //initialize other task vars
   %task.sendMsg = true;
   %task.sendMsgTime = 0;
   %task.path = "";

   %client.player.mVehicle = "";// needed if bot just hopped out a vehicle and it was not destroyed/removed yet !
}

function AIPilotShrike::assume(%task, %client)
{
   //would like to call these at frequency 1 each for performance, set it a happier balance for the rest of the bots
   %task.setWeightFreq(5);
   %task.setMonitorFreq(5);

   %client.needEquipment = AINeedEquipment(%task.equipment, %client);

   //even if we don't *need* equipemnt, see if we should buy some... // --- need to update here to check if we can use a invo refresh * NOTE *
   if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
   {
      //see if we could benefit from inventory
      %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      %closestDist = getWord(%result, 1);
      if (AINeedEquipment(%task.desiredEquipment, %client) && %closestInv > 0)
      {
         %result = AIFindClosestEnemy(%client, 100, $AIClientLOSTimeout);
    %closestEnemy = getWord(%result, 0);
    %closestEnemydist = getWord(%result, 1);

    if (%closestEnemy <= 0 || (%closestEnemyDist > %closestDist * 1.5))
       %client.needEquipment = true;
      }
   }

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();

   //reset the vehicle station wait time
   %client.vsWaitTime = "";

   //clear the target tag
   %task.shouldAttack = -1;

   //set the destination paths for each team and game type

   //for siege game type
   if (Game.class $= "SiegeGame")
      %team = Game.offenseTeam == %client.team ? 1 : 2;
   else
      %team = %client.team;

   //first see how many paths we have
   for(%i = 1; %i < 10; %i++)
   {
      if(isObject(nameToId("T" @ %team @ "ShrikePath" @ %i)))
         %mx = %i;
   }

   //pick one path at random
   %random = mFloor(getRandom(1, %mx));

   //set the path simgroup to follow
   %task.group = nameToId("T" @ %team @ "ShrikePath" @ %random);

   %task.count = %task.group.getCount();

   %task.locationIndex = 0;
   %client.needVehicle = true;

   %client.shrikeFire = false;
   %task.firePos = -1;
   %task.searchPos = -1;
   %task.searchTime = -1;
   %task.swch = 1;//need this for firing guns at players, turrets, solar panels and sensors
}


function AIPilotShrike::retire(%task, %client)
{
   if (aiClientIsAlive(%client))
   {
      %client.needVehicle = false;
      %client.clearStep();
      if(%client.player.isMounted())
         AIDisembarkVehicle(%client);
   }
}

//------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------- lets use the weight to choose the shrike's targets ---
//------------------------------------------------------------------------------------------------------------------------
function AIPilotShrike::weight(%task, %client)
{
   //are we mounted in vehicle ?
   if (%client.player.isMounted() && isObject(%client.player.mVehicle))
   {
      //get the vehicle data stuff...
      %vehicle = %Client.vehicleMounted;//-------------------------------------------- %vehicle *

      %skill = %client.getSkillLevel();// -------------------------------------------- %skill *

      //find the terrain height directly below us
      %myPos = %vehicle.getWorldBoxCenter();//---------------------------------------- %myPos *
      %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType |
        $TypeMasks::ForceFieldObjectType;
      %downVec = getWord(%myPos, 0) @ " " @ getWord(%myPos, 1) @ "0";
      %altSur = containerRayCast(%myPos, %downVec, %mask, 0);
      %altPos = posfromRayCast(%altSur);
      %terHt = getTerrainHeight(%altPos);//-------------------------------------------- %terHt *
      //%myAlt = getWord(%myPos, 2) - getWord(%altPos, 2);// -------------------------- %myAlt * not used in weight function

      //if we have a target, is it to far, dead or destroyed?
      if (isObject(%task.shouldAttack) && %task.shouldAttack > 0)
      {
         %dist = vectorDist(%task.shouldAttack.getWorldBoxCenter(), %myPos);

         if (isObject(%task.shouldAttack) && %task.shouldAttack.isPlayer())
         {
            if (%dist > 400 || %task.shouldAttack.getState() $= "Dead")// --- dist to clear Player Targets is 400m
            {
               %task.shouldAttack = -1;
               %task.searchTime = -1;
               %task.searchPos = -1;
            }
         }
         else if (%dist > 500 || %task.shouldAttack.getDamageState() $= "Destroyed")// - dist to clear Targets is 500m
         {
            %task.shouldAttack = -1;
            %task.searchTime = -1;
            %task.searchPos = -1;
         }
      }

      %lastAttacker = %vehicle.lastDamagedBy;//---------------------------------------- what damaged us ???
      if (isObject(%lastAttacker))
      {
         if (%lastAttacker.isMounted())
            %lastAttacker = %lastAttacker.getObjectMount();

         %lastClass = %lastAttacker.getDataBlock().getClassName();// --------------- we need the target class to make decisions down below
         %vehicle.lastDamagedBy = -1;// --------------------------------------------- CLEAR THE VEHICLE DAMAGED FLAG
      }

      //----------------------------------- if an enemy has fired on our vehicle recently and we have a target already ...

      //- make sure it is new target and alive ??? * (should change so last attacker could = current target to reset time check ***???)
      if (isObject(%lastAttacker) && isObject(%task.shouldAttack) && %lastAttacker != %task.shouldAttack)
      {
         %targPos = %task.shouldAttack.getWorldBoxCenter();
         %curTargDist = vectorDist(%myPos, %targPos);

         //echo("%targPos = " @ %targPos);
         //echo("%curTargDist = " @ %curTargDist);
         //echo("----------------------------------------");


         %newTargPos = %lastAttacker.getWorldBoxCenter();
         %newTargDist = vectorDist(%myPos, %newTargPos);

         //echo("%newTargPos = " @ %newTargPos);
         //echo("%newTargDist = " @ %newTargDist);

         %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
         %hasLOS = !containerRayCast(%myPos, %targPos, %mask, 0);

         if (%lastAttacker.isVehicle() && %hasLOS)//-------------------------------------------- was it a Shrike? 1st piority
         {
            if (%newTargDist > 0 && %newTargDist < %curTargDist + 300)
            {
               %task.shouldAttack = %lastAttacker;
               if (%task.searchTime == -1)
                  %task.searchTime = getSimTime();
               %task.searchPos = %task.shouldAttack.getWorldBoxCenter();
            }
         }
         else if (%lastAttacker.isTurret() && %lastAttacker.team != %client.team && %hasLOS)//----- was it a vehicle turret? 2nd piority
         {
            %type = %lastAttacker.getDataBlock().getName();
            if (%type $= "TurretBaseLarge" && %newTargDist < %curTargDist + 250)//------------------- what's the distance of new attacker???
            {
               %task.shouldAttack = %lastAttacker;
               if (%task.searchTime == -1)
                  %task.searchTime = getSimTime();
            }
         }
//         else if (%lastClass $= "HoverVehicleData" && %hasLOS)//-------------------------- was it a vehicle (Tank) turret? 3rd piority
//         {
//            if (%newTargDist > 0 && %newTargDist < %curTargDist + 200)//--------------------- what's the distance of new attacker ???
//            {
//               %task.shouldAttack = %lastAttacker;
//               if (%task.searchTime == -1)
//                  %task.searchTime = getSimTime();
//
//               %tx = firstWord(%task.shouldAttack.getTransform());
//               %ty = getWord(%task.shouldAttack.getTransform(), 1);
//               %tz = getWord(%task.shouldAttack.getTransform(), 2);
//               %tz += mFloor(getRandom(2, 5) * 5);//add some altitude to search for targets on the ground
//               %task.searchPos = %tx SPC %ty SPC %tz;//----------------------- %task.searchPos used for moving targets, not baseturrets or sensors
//            }
//         }
         else if (%lastAttacker.isPlayer() && %lastAttacker.team != %client.team && %hasLOS)//-- was it a Player? 4th piority (check team for this only)
         {
            //see if the new targ is no more than 250 m further
            if (%newTargDist > 0 && %newTargDist < %curTargDist + 250)
            {
               %task.shouldAttack = %lastAttacker;
               if (%task.searchTime == -1)
                  %task.searchTime = getSimTime();
               %tx = firstWord(%task.shouldAttack.getTransform());
               %ty = getWord(%task.shouldAttack.getTransform(), 1);
               %tz = getWord(%task.shouldAttack.getTransform(), 2);
               %tz += mFloor(getRandom(2, 5) * 5);//-------------------------------- add some altitude to search for targets on the ground
               %task.searchPos = %tx SPC %ty SPC %tz;//---------------------------- %task.searchPos used for moving targets, not baseturrets or sensors

            }
         }
         //DON'T THINK I WANT HIM SNOOPING AROUND IF DAMAGED BY A DEPLOYED MPB (hell their deadly to shrikes !)
         //else if (%lastClass $= "WheeledVehicleData" && %hasLOS)
         //{
            //blah, blah, blah;
         //}
      }
      //------------------------------------------------------- if we don't have a target lets see if we can't find one
      else if (%task.shouldAttack <= 0)
      {
         //------------------------------------------------ search for a new vehicle to attack... ---

         %losTimeout = $AIClientMinLOSTime + ($AIClientLOSTimeout * %client.getSkillLevel());
         %result = AIFindClosestEnemyPilot(%client, 400, %losTimeout);
         %pilot = getWord(%result, 0);
         %pilotDist = getWord(%result, 1);

         //------------------------------------------------------------- getClosest enemy Vehicle ---
         //should we engage enemy vehicles?
         %result = aiFindClosestEnemyVehicle(%client);
         %closestEV = getWord(%result, 0);
         %closestEVDist = getWord(%result, 1);

         //check for LOS
         if (%closestEV > 0 && isObject(%closestEV))
         {
            %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
            %vehicleLOS = !containerRayCast(%myPos, %closestEV.getWorldBoxCenter(), %mask, 0);
         }
         else
            %vehicleLOS = -1;

         //--------------------------------------------------------------------- check for targets ---

         %result = findClosestEnemyTarget(%client, %vehicle);//--- new function above, also tied to functions in ai.cs file
         %clTarg = getWord(%result,0);
         %clTargDist = getWord(%result, 1);

         //-------------------------------------------------------------- else look for players on the ground ---

         %result = AIFindClosestEnemyToLoc(%client, %altPos, 250, $AIClientLOSTimeout, false, false);//- we don't look as far for players
         %closestEnemy = getWord(%result, 0);
         %closestEnemydist = getWord(%result, 1);

         %ourFlag = $AITeamFlag[%client.team];
         if (Game.class $= "CTFGame" && %ourFlag.carrier > 0)  // first check for someone stealing our flag - pinkpanther
         {
            %task.shouldAttack = %ourFlag.carrier;
            %task.searchPos = %task.shouldAttack.getWorldBoxCenter();//need to UP ^ the Z LEVEL HERE ***
            %task.searchTime = getSimTime();
         }
         //--------------------------------------------------------------------------------------------------- set the newly found target here ---
         else if (%closestEnemy > 0 && %vehicleLOS)// we check los the fast way here, not through "AIFindClosestEnemy" above
         {
            %task.shouldAttack = %closestEnemy.player;
            %task.searchPos = %task.shouldAttack.getWorldBoxCenter();//need to UP ^ the Z LEVEL HERE ***
            %task.searchTime = getSimTime();
         }
         else if (%closestEV > 0 && %closestEVDist <= 400 && %vehicleLOS)
         {
            %task.shouldAttack = %closestEV;
            %task.searchPos = %task.shouldAttack.getWorldBoxCenter();//need to UP ^ the Z LEVEL HERE ***
            %task.searchTime = getSimTime();
         }
         else if (%clTarg > 0 && %clTargDist < 400 && %vehicleLOS)//--- else set turrets/sensors as targets (sometimes we get caught behind a hill)
         {
            %task.shouldAttack = %clTarg;
            %task.searchPos = %task.shouldAttack.getWorldBoxCenter();//need to UP ^ the Z LEVEL HERE ***
            %task.searchTime = getSimTime();
         }
         else if (AIClientIsAlive(%pilot) && %pilotDist < 400)//- else set Pilot as targets (use default find enemy pilots here)
         {
            %task.shouldAttack = %pilot.vehicleMounted;
            %task.searchPos = %task.shouldAttack.getWorldBoxCenter();//--------------------- (need to UP ^ the Z LEVEL HERE ***)
            %task.searchTime = getSimTime();
         }
         else if (isObject(%lastAttacker) && %lastAttacker.team != %client.team)//else attack our last attacker
         {
            %newTargPos = %lastAttacker.getWorldBoxCenter();
            %newTargDist = vectorDist(%myPos, %newTargPos);
            if (%newTargDist < 400)
            {
               %task.shouldAttack = %lastAttacker;
               %task.searchPos = %task.shouldAttack.getWorldBoxCenter();//need to UP ^ the Z LEVEL HERE ***
               %task.searchTime = getSimTime();
            }
         }
         //--------------------------------------------- reset the searchPos and searchTime for safety (should not be needed though)
         else
         {
            %task.shouldAttack = -1;
            %task.searchTime = -1;
            %task.searchPos = -1;
         }
      }
   }

   //----------------------------------------------------could add here to fight regularly if we are not mounted yet - Lagg... (*** someday ***)

   //update the task weight... this is the most important part - calls the monitor below
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;

   //let the monitor decide when to quit :)//--- NEEDED SO IMPORTANT !
   %task.setWeight(%task.baseWeight);

}

//-------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------- AIPilotShrike::monitor ---
//-------------------------------------------------------------------------------------------------------------------------
function AIPilotShrike::monitor(%task, %client)
{
   //first, buy equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;
      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
         return;
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(3);
    %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
         %task.setMonitorFreq(3);
         %client.needEquipment = false;
      }
   }
   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   if (%client.needVehicle)
   {
      %clVs = AIFindClosestVStation(%client);
      if (%clVs > 0)
      {
         %closestVs = getWord(%clVs, 0);
         %closestVsDist = getWord(%clVs, 1);
      }

      if (%closestVs > 0 && !isObject(%client.player.mVehicle)) // && %closestVsDist < 300
      {
         //If we're in light or medium armor, buy the vehicle - Lagg...
         if (%client.player.getArmorSize() !$= "Heavy" && %client.player.getArmorSize() !$= "Titan")
         {
            %task.setMonitorFreq(5);
            %buyResult = aiBuyVehicle(%client.aiVehicleConsidering, %client);
         }
         else
         {
            //if ai in heavy armor buy equipment
            if (%task == %client.objectiveTask)
       {
               %task.baseWeight = %client.objectiveWeight;//-- so he can get bumped off objective --
               %task.equipment = "Medium || FieldTech HeavyLauncher";
          %task.buyEquipmentSet = "MediumTailgunner";
               %client.needEquipment = true;
               return;
       }
         }
         //- need to add if attacked, Defend yourself or Lower weight so can get bumped and go get em,
         //or set %task.shouldAttack client.lastDamagedClient! (maybe)? ***
         if (%buyResult $= "InProgress")
       return;

         else if (%buyResult $= "Finished")
         {
            //if we are finished buying the vehicle, then we are done
            //this is handled in function Armor::AIonMount - Lagg... 7-9-2004
         }
         else if (%result $= "Failed")
         {
            //error("AIPilotShrike::monitor - %buyResult failed - " @ %client.nameBase);

            //if this task is the objective task, choose a new objective
       if (%task == %client.objectiveTask)
       {
          AIUnassignClient(%client);
          Game.AIChooseGameObjective(%client);
               return;
       }
         }
      }
      else if ((%closestVs <= 0) && !isObject(%client.player.mVehicle)) // || %closestVsDist >= 300
      {
         //error("AIPilotShrike::monitor - no vpad or to far " @ getTaggedString(%client.name));

         if (%task == %client.objectiveTask)
         {
            AIUnassignClient(%client);
       Game.AIChooseGameObjective(%client);
            return;
         }
      }
   }

   //chat - send the message
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
         {
            if (%task.chat !$= "")
            {
               %chatMsg = getWord(%task.chat, 0);
               %chatTemplate = getWord(%task.chat, 1);
               if (%chatTemplate !$= "")
                  AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
               else
                  AIMessageThread(%task.chat, %client, -1);
            }
         }
      }
   }

   // ------------------------------------------------------------------------ if we managed to get in vehicle then go ---
   if (%client.player.isMounted() && isObject(%client.player.mVehicle))//--------------- are we mounted ???
   {
      //------------------------------------------------------------------------------- set monitor frequency ---
      %task.setMonitorFreq(3);

      //------------------------------------------------------------------------- set the variables that we will use ---
      %vehicle = %Client.vehicleMounted;//--------------------- %vehicle *
      %vehData = %vehicle.getDataBlock();// ------------------- %vehData *
      %myZLev = getWord(%vehicle.getTransform(), 2);//--------- %myZLev *
      %mySpd = vectorLen(%vehicle.getVelocity());//------------ %mySpd *
      %skill = %client.getSkillLevel();// --------------------- %skill *
      %collision = aiCollisionCheck(%vehicle, %mySpd);// ------ %collision *

      //-------------------------------------------------------------------- make sure we got in the correct vehicle ---
      if (!%vehicle.vehicleCheckType($VehicleList::Fighters))
      {
         //error("AIPilotShrike::monitor - opps we got in wrong vehicle");
         AIDisembarkVehicle(%client);
         %client.needVehicle = true;
         return;
      }
      //-------------------------------------------------------------------------------------- get our altitude ---
      //find out our altitude
      %myPos = %vehicle.getWorldBoxCenter();//---------------------------------------- %myPos *
      %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType |
        $TypeMasks::ForceFieldObjectType;
      %downVec = getWord(%myPos, 0) @ " " @ getWord(%myPos, 1) @ "0";
      %altSur = containerRayCast(%myPos, %downVec, %mask, 0);
      %altPos = posfromRayCast(%altSur);

      %myAlt = getWord(%myPos, 2) - getWord(%altPos, 2);// --------------------------- %myAlt *
      %terHt = getTerrainHeight(%myPos);// ------------------------------------------ %terHt *

      %vx = getWord(%myPos, 0);// ---------------------------------------------------- %vx *
      %vy = getWord(%myPos, 1);// ---------------------------------------------------- %vy *
      %vz = getWord(%myPos, 2);// ---------------------------------------------------- %vz * //have this already above in %myZLev but still used below
      %vR = getWords(%vehicle.getTransform(), 3, 6);// ------------------------------- %vR *



      //------------------------------------------------------------------------------------------- Avoidance (Stuck) ---
      if (%mySpd < 0.1)
      {
         InitContainerRadiusSearch(%myPos, 7, $TypeMasks::InteriorObjectType | $TypeMasks::VehicleObjectType
           | $TypeMasks::TSStaticShapeObjectType);
         %avoid = containerSearchNext();

         if (%avoid == %vehicle)
            %avoid = containerSearchNext();

         //if we are moving slow and close to objects we are probably stuck
         if (%avoid)
         {
            %tempx = getWord(%vehicle.getWorldBoxCenter(), 0);
            %tempy = getWord(%vehicle.getWorldBoxCenter(), 1);
            %tempz = getWord(%vehicle.getWorldBoxCenter(), 2);
            %tempz += 1.0;
            %tempR = getWords(%vehicle.getTransform(), 3, 6);

            %client.setPilotDestination(%task.location, 1.0);//max speed
            %vehicle.setTransform(%tempx SPC %tempy SPC %tempz SPC %tempR);//----------------- set the Transform Here up a little
         }
      }

      //-------------------------------------------------------------------------------------- Task Should Attack (do we have a target ?) ---
      if (isObject(%task.shouldAttack) && %task.shouldAttack > 0)
      {
         //--------------------------------------------------------------------------- get some target data here ---

         %targClass = %task.shouldAttack.getDataBlock().getClassName();//------------- %targClass *
         %targPos = %task.shouldAttack.getWorldBoxCenter();// ------------------------ %targPos *
         %targDist = vectorDist(%myPos, %task.shouldAttack.getWorldBoxCenter());//---- %targDist *
         %targZLev = getWord(%task.shouldAttack.getTransform(), 2);//----------------- %targZLev *
         %targSpd = vectorLen(%task.shouldAttack.getVelocity());//-------------------- %targSpd *
         %compZLev = %myZLev - %targZLev;//------------------------------------------- %compZLev *
         %tx = firstWord(%task.shouldAttack.getTransform());// ------------------------ %tx *
         %ty = getWord(%task.shouldAttack.getTransform(), 1);// ----------------------- %ty *
         %tz = getWord(%task.shouldAttack.getTransform(), 2);// ----------------------- %tz *

         //check for LOS - do not include TerrainObjectType - pinkpanther
         %mask = $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
//         %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
         %hasLOS = !containerRayCast(%vx SPC %vy SPC %vz - 1, %targPos, %mask, 0);//------- %hasLOS *

//         %ourFlag = $AITeamFlag[%client.team];
//         if (%hasLOS || (Game.class $= "CTFGame" && %ourFlag.carrier > 0))
         if (%hasLOS)
         {
            //---------------------------------------------------------------local %varibles only if has LOS - %firePos
            %x = getWord(%vehicle.getTransform(), 0);// ------------------------- my %x vec
            %y = getWord(%vehicle.getTransform(), 1);// ------------------------- my %y vec
            //%z = getWord(%task.shouldAttack.getTransform(), 2);// ----------- target %z vec
            %z = %tz;// ----------------------------------------------------- target %z vec
            //if (%targClass $= "TurretData" || %targClass $= "StaticShapeData" || %targClass $= "PlayerData")
            if (%targClass $= "PlayerData")
            {
               if (%z < %terHt + 5)
                  %z = %terHt + 5;
               else
                  //%z += 0.7;
                  %z += 1.0;
            }
            //------------------------------------------------------------- set firePos a little higher for land lovers
            else if (%targClass $= "TurretData" || %targClass $= "StaticShapeData" || %targClass $= "HoverVehicleData")
            {
               if (%z < %terHt)
                  %z = %terHt + 25.0;
               else
                  %z += 25.0;
            }

            //set the firing position
            %task.firePos = %vx SPC %vy SPC %z;//------------------------------------------------------- %task.firePos *
            %z = %tz;//getWord(%task.shouldAttack.getTransform(), 2);
            if (%targClass $= "WheeledVehicleData" || %targClass $= "HoverVehicleData" || %targClass $= "PlayerData")
               %z += mFloor(getRandom(2, 5) * 5);//add some altitude to search for targets on the ground
            %task.searchPos = %tx SPC %ty SPC %z;//---------------------------------------------------- %task.searchPos *
            %task.searchTime = getSimTime();
         }
         else
         {
            //------------------------------------------------------------------------------------------------- search for targets for 35 sec only ---
            if (getSimTime() - %task.searchTime > 35000)
            {
               %task.shouldAttack = -1;
               %task.searchTime = -1;
               return;
            }
         }

         // -------------------------------------------------------------------------------------------------------------
         //this part sets the vehicle transform to aim at targets, it has to aim differently for each target type. Thanks Martin Hoover(founder)

         //sub the two positions so we get a vector pointing from the origin in the direction we want our object to face
         if (%targClass $= "PlayerData" || %targClass $= "TurretData" || %targClass $= "StaticShapeData")
         {
            //use the x-axis of the players rotation as the desired aiming point,
       //and calculate a point 1.1 - 1.3 m behind the trigger point - Lagg...
       %aprchDirection = MatrixMulVector("0 0 0 " @ getWords(%task.shouldAttack.getTransform(), 3, 6), "1 0 0");
       %aprchDirection = VectorNormalize(%aprchDirection);

            //alternate fire so we can hit turrets, sensors, players and solar panels
            if (%task.swch == 1)
            {
               %task.swch = 2;
               %factor = 2.0;//2.0m to the side of target
            }
            else
            {
               %task.swch = 1;
               %factor = -2.0;//2.0m to the other side of target
            }
            %aprchFromLocation = VectorAdd(%task.shouldAttack.getWorldBoxCenter(),VectorScale(%aprchDirection, %factor));
            %ax = getWord(%aprchFromLocation, 0);
            %ay = getWord(%aprchFromLocation, 1);
            %az = getWord(%aprchFromLocation, 2);


            if (%targClass $= "PlayerData")
               %vec = VectorSub(%aprchFromLocation, %myPos);
            else
               %vec = VectorSub(%ax SPC %ay SPC %az + 2, %myPos);// ------ up 2 so he hits turrets and solar panels
         }
         else if (%targClass $= "HoverVehicleData" || %targClass $= "WheeledVehicleData")
            %vec = VectorSub(%tx SPC %ty SPC %tz + 2.5, %myPos);
         else
            //%vec = VectorSub(%task.shouldAttack.getWorldBoxCenter(), %myPos);
            %vec = VectorSub(%tx SPC %ty SPC %tz + 1, %myPos);// --- up the target point by 1 to counter offset of shrike weapons for big targets only


         // pull the values out of the vector
         %x = firstWord(%vec);
         %y = getWord(%vec, 1);
         %z = getWord(%vec, 2);

         %ourTrans = getWords(%vehicle.getTransform(), 3, 6);
         %ourxRot = firstWord(%ourTrans);
         %ourYRot = getWord(%ourTrans, 1);
         %ourzRot = getWord(%ourTrans, 2);
         %ourwRot = getWord(%ourTrans, 3);

         //this finds the distance from origin to our point
         %len = vectorLen(%vec);

         //---------XY-----------------
         //given the rise and length of our vector this will give us the angle in radians
         %rotAngleXY = mATan( %z, %len );

         //---------Z-----------------
         //get the angle for the z axis
         %rotAngleZ = mATan( %x, %y );

         //create 2 matrices, one for the z rotation, the other for the x rotation
         %matrix = MatrixCreateFromEuler("0 0" SPC %rotAngleZ * -1);
         %matrix2 = MatrixCreateFromEuler(%rotAngleXY SPC "0 0");

         //now multiply them together so we end up with the rotation we want
         %finalMat = MatrixMultiply(%matrix, %matrix2);

         //were done, send the proper numbers back
         %xRot = getWord(%finalMat, 3);
         %yRot = getWord(%finalMat, 4);
         %zRot = getWord(%finalMat, 5);
         %wRot = getWord(%finalMat, 6);

         //------------------------------------------------- see if we are facing the enemy target (thanks Martin Hoover "Founder") ---

         //Figuring out if the enemy is in front of the bot shouldn�t be too difficult,
         //just need to compare the bots current rotation to the rotation required to point at the target...

         %vehicleRot = getWords(%vehicle.getTransform(), 3, 6);
         //(since you have T2, you have a function rotFromTransForm() )

         %targetRot = %xRot SPC %yRot SPC %zRot SPC %wRot;//---results of pointToPos;

         // rotation = x y z angle

         //Since an object can have a rotaion described with negative radians you need to multiply
         //the z and angle components to ensure you get the proper compass tick the rotation is specifying.

         %vehicleZangle = getWord(%vehicleRot, 2) * getWord(%vehicleRot, 3);

         //get the angle the bot would have to rotate to to face the target...

         %TargetZAngle = %zRot * %wRot;// --- getWord(%targetRot, 2) * getWord(%targetRot, 3);

         //Now you can simply compare %botZangle and %TargetZAngle to see how similar the angles are.
         //(the angles are in radians, if you want to see them in degrees then do a %degAngle = mRadToDeg(%botZangle) )

         %compare = %vehicleZangle - %TargetZAngle;
                                            //----------------------------------------------------------------------------------------------------------
         //since we have 6 different target classes and each one has to dealt with differently depending upon certain parameters.
         //We will have to check for each different class and in each class we can check for parameters.

         //--------------------------------------------------------------------------------------------------------------
         //---------------------------------------------------------------------------- TURRET/STATICSHAPE DATA CLASS ---
         //--------------------------------------------------------------------------------------------------------------

         //--- dealing with turrets can be dangerous so we must keep a safe distance, sensors and solar panels will be somewhat easier to handle. ---

         if (%targClass $= "TurretData" || %targClass $= "StaticShapeData")
         {
            //--- get some target data ---
            if (%targClass $= "TurretData" && %task.shouldAttack.isEnabled() && %task.shouldAttack.isPowered())// --------- is turret enabled
               %safeRange = %task.shouldAttack.getMountedImage(0).attackRadius;// ----------------------------------------- set the safeRange
            else
               %safeRange = 50;// ----------------------------------------------------------------------------------------- safe range for sensors

            // --------------------------------------------------------------------------------------- (can't hit a small target at to far a range)
            if (%terHt < %targZLev && %targDist > %safeRange && %targDist < 300.0 && (%myAlt > 5 || %mySpd < 10))
            {
               %close = vectorDist(%myPos,  %task.firePos);
               if (%myZLev >= %targZLev && %compZLev < 35.0 && (%targDist / %compZLev > 1.5) && %compare < 0.65 && %compare > -0.65
                 && %hasLOS && %myZLev > %trHt + 3 && %close < 5)
               {
                  %client.setControlObject(%client.player);// ---------------------------------------------------------- take away pilot's control
                  //%vehicle.setTransform(%myPos SPC %xRot SPC %yRot SPC %zRot SPC %wRot);// ----------------------------- set the Transform Here
                  %vehicle.setTransform(%task.firePos SPC %xRot SPC %yRot SPC %zRot SPC %wRot);// ---- set the Transform Here (CHEAT HERE, works better !)
                  %vehicle.setVelocity("0 0 0");//-------------------------------------------------- stop him if we can, so we can kill (has no effect) :(
                  %client.shrikeFire = true;// ------------------------------------------------------------------------------ turn ON the Guns
               }
               else if (%hasLos && checkLOSPoint(%task.firePos, %targPos))// ---------------------------------------------- We have LOS to Target ---
               {
                  %client.shrikeFire = false;// ---------------------------------- turn off the guns
                  %client.setControlObject(%vehicle);// -------------------------- give pilot control of shrike

                  if (VectorDist(%myPos, %task.firePos) < 8)
                  {
                     if (%myZLev < %targZLev + 1)//set a little higher if we are too low
                     {
                        %vx = getWord(%myPos, 0);
                        %vy = getWord(%myPos, 1);
                        %vz = getWord(%myPos, 2);
                        %vz += 8.0;

                        %client.setPilotPitchRange(-0.025, 0.025, 0.025);// -------- a smooth setting here
                        %client.setPilotDestination(%vx SPC %vy SPC %vz, 0.5);// --- move UP a little, slowly

                        //%vehicle.setTransform(%vx SPC %vy SPC %vz SPC %vR);// -- set the Transform Here up a little (not good looking, don't do this)
                     }
                     else
                        %client.setPilotAim(%aprchFromLocation);// -------------------------------------------------------- aim at targetPoint
                  }
                  else
                  {
                     if (%myZLev > (%targZLev + 25) && %targZLev > (%terHt + 10))
                        applyKick(%vehicle, "down", (%skill * 2));
                     else
                     {
                        %client.setPilotPitchRange(-0.025, 0.025, 0.025);// -------- a smooth setting here
                        %client.setPilotDestination(%task.firePos, 1.0);// ----------------- move to the %firePos
                     }
                  }
               }
               else// ---------------------------------------------------------------------------------------------- We DO NOT have LOS to Target ---
               {
                  %task.shouldAttack = -1;// ------------------------------------ clear the target so we can aquire a new one
                  %client.shrikeFire = false;// --------------------------------- turn off the guns
                  %client.setControlObject(%vehicle);// ------------------------- give pilot control of shrike

                  //-------------------------------------------------------------- if we can see our target move along flight path
                  if (VectorDist(%myPos, %task.location) < 50)
             {
                     //----------------------------------------------------------- if we have another location index
                     if ((%task.count - 1) == %task.locationIndex)
                     {
                        AIDisembarkVehicle(%client); // --------- Hop off...
                        %client.stepMove(%task.location, 0.25);
                     }
                     else//------------------------------------------------------- or go back to last marker, but keep searching
                     {
                        %task.locationIndex++;
                        %task.location = %task.group.getObject(%task.locationIndex).position;
                     }
                  }
                  else
                  {
                     %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
                     %hasLOS = !containerRayCast(%myPos, %task.location, %mask, 0);//------- %hasLOS to %task.location ?

                     %client.setPilotPitchRange(-0.025, 0.025, 0.025);// -------- a smooth setting here

                     if (%hasLOS)
                        %client.setPilotDestination(%task.location, 1.0);//max speed
                     else
                        %client.setPilotDestination(%vx SPC %vy SPC %vz + 10, 1.0);// -- if we can't see %task.location move straight up ^
                  }
               }
            }
            else//--- we are to close to safley attack or to far to be effective so MOVE IT ! /// --- *** review this ***
            {
               %client.shrikeFire = false;// ----------------------------------- turn off the guns
               %client.setControlObject(%vehicle);// --------------------------- give pilot control of shrike

               //if we have another location index
               if (VectorDist(%myPos, %task.location) < 50)
          {
                  if ((%task.count - 1) == %task.locationIndex)
                  {
                     AIDisembarkVehicle(%client); //--------- Hop off...
                     %client.stepMove(%task.location, 0.25);
                     //return;
                  }
                  else
                  {
                     %task.locationIndex++;
                     %task.location = %task.group.getObject(%task.locationIndex).position;
                  }
               }
               else
               {
                  %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
                  %hasLOS = !containerRayCast(%myPos, %task.location, %mask, 0);//------- %hasLOS to %task.location ?

                  %client.setPilotPitchRange(-0.025, 0.025, 0.025);// -------- a smooth setting here

                  if (%hasLOS)
                     %client.setPilotDestination(%task.location, 1.0);//max speed
                  else
                     %client.setPilotDestination(%vx SPC %vy SPC %vz + 10, 1.0);// -- if we can't see %task.location move straight up ^
               }
            }
         }
         //-------------------------------------------------------------------------------------------------------------------------
         //--------------------------------------------------------------------------------------------------- PLAYER DATA CLASS ---
         //-------------------------------------------------------------------------------------------------------------------------

         //--- Players we will want to always move toward them and maybe even run them down, if we lose LOS to target we will want to search for them ---

         else if (%targClass $= "PlayerData" && isObject(%task.shouldAttack))
         {
            %client.setControlObject(%vehicle);//----------------------------------------- give pilot control of shrike and leave it there ---

            //--- get some target data ---

       %pos2D = getWord(%myPos, 0) SPC getWord(%myPos, 1) SPC "0";
       %dest2D = getWord(%task.shouldAttack.getWorldBoxCenter(), 0) SPC getWord(%task.shouldAttack.getWorldBoxCenter(), 1) SPC "0";

            //check for obsticles near target          --------------------------------------------------------------- check for obsticles near target
            %mask = $TypeMasks::InteriorObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::ForceFieldObjectType;
            %noObs = containerRayCast(%targPos, %tx SPC %ty SPC %tz + 25, %mask, 0);//- look above target for obsticles (interiors, vehicles, FF , trees?)

            %mask = $TypeMasks::StaticShapeObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::TurretObjectType |
              $TypeMasks::ForceFieldObjectType | $TypeMasks::TSStaticShapeObjectType;
            InitContainerRadiusSearch(%targPos, 7.0, %mask);
            %obs = containerSearchNext();// != 0;
            if (%obs == %vehicle)
               %obs = containerSearchNext();// != 0;

            //---------------- find out if we are above or below target... ---
       if (VectorDist(%pos2D, %dest2D) < 45 && (%compZLev > 8.0 || %compZLev < -8.0))
            {
               %client.shrikeFire = false;//-------------- turn off the gun

               //if we don't have another location index back up
               if (VectorDist(%myPos, %task.location) < 50)
          {
                  if ((%task.count - 1) == %task.locationIndex)
                     %task.locationIndex--;
                  else
                     %task.locationIndex++;
               }

               %task.location = %task.group.getObject(%task.locationIndex).position;

               %client.setPilotPitchRange(-0.05, 0.025, 0.025);//------ set pitch range for flying
               %client.setPilotDestination(%task.location, 1.0);
            }
            else if (%hasLOS)//----------------------------------------------------------------- see if we can run em down or should just shoot ---
            {
               if (%compare < 0.65 && %compare > -0.65 && %compZLev < 30.0 && %compZLev > -15.0)
               {
                  if (!%noObs && !%obs && !%collision)// ---------------------------------------------------------- if no obstacles try to run em down
                  {
                     %client.setPilotPitchRange(-0.05, 0.025, 0.025);// ---------------------------------------- set pitch range for fire
                     %client.setPilotDestination(%aprchFromLocation, 1.0);
                     %vehicle.setTransform(%myPos SPC %xRot SPC %yRot SPC %zRot SPC %wRot);// -------------------- set the Transform Here
                  }
                  else// --------------------------------------------------------------------------------------- if we have obsticles just move to aim
                  {
                     %vehicle.setTransform(%myPos SPC %xRot SPC %yRot SPC %zRot SPC %wRot);// ------------------- set the Transform Here
                     %client.setPilotAim(%aprchFromLocation);// --- aim at targetPoint
                  }

                  %client.shrikeFire = true;// -------------------------------------------------------------------- turn on the guns
               }
               else
               {
                  %client.shrikeFire = false;//-------------- turn off the gun
                  %client.setPilotPitchRange(-0.05, 0.025, 0.025);//------ set pitch range for flying

                  if (!%noObs && !%obs && !%collision)// || %targDist > 450)//------------------------------ if no obsticles try to run em down
                     %client.setPilotDestination(%targPos, 1.0);//------------------------------------------------ move to the target
                  else
                  {
                     if (VectorDist(%myPos, %task.firePos) < 7.0)
                     {
                        if (%myZLev < %targZLev)//set a little higher if we are too low
                        {
                           %vx = getWord(%myPos, 0);
                           %vy = getWord(%myPos, 1);
                           %vz = getWord(%myPos, 2);
                           %vz += 8.0;

                           %client.setPilotDestination(%vx SPC %vy SPC %tz + 1, 1.0);//------------------------------ move UP a little
                        }
                        else
                           %client.setPilotAim(%aprchFromLocation);//------------------------------------------------ aim at targetPoint
                     }
                     else
                        %client.setPilotDestination(%task.firePos, 1.0);//------------------------------------------- move to %task.firePos
                  }
               }
            }
            else//else we can't see the target so start searching for him
            {
               if (%compZLev < 1)//we know where you are, lets adjust if we are lower
               {
                  %x = getWord(%vehicle.getTransform(), 0);//------------------------- my %x vec
                  %y = getWord(%vehicle.getTransform(), 1);//------------------------- my %y vec
                  %client.setPilotPitchRange(-0.025, 0.025, 0.025);//------ set pitch range for flying (smooth)
                  %client.setPilotDestination(%x SPC %y SPC %vz + 10, 1.0);//----------------------- set speed a little slower ---
               }
               else//else we are searching
               {
                  %client.shrikeFire = false;//-------------- turn off the gun
                  %client.setPilotPitchRange(-0.025, 0.025, 0.025);//------ set pitch range for flying (smooth)
                  %client.setPilotDestination(%task.searchPos, 0.7);//----------------------- set speed a little slower ---
               }
            }
         }
                           //----------------------------------------------------------------------------------------------------------------------------------------------
         //-------------------------------------------------------------------------------------------------------------- DEPLOYED WHEEL VEHICLE CLASS ---            //----------------------------------------------------------------------------------------------------------------------------------------------
         else if (isObject(%task.shouldAttack) && %targClass $= "WheeledVehicleData" && %task.shouldAttack.deployed)
         {
            if (%hasLOS)// -------------------------------------------------------------------------------------------------- Sec 1 --- Setup Data ---
            {
               //-------------------------------------------------------------------------------------------------------------- %firePos ---
               %x = getWord(%vehicle.getTransform(), 0);
               %y = getWord(%vehicle.getTransform(), 1);
               %z = getWord(%task.shouldAttack.getTransform(), 2);
               %c = %z + 3.0;// --- do not go lower than targZLevel ---

               if (%z < %terHt)
                  %z = %terHt + (%targDist / 10);// ------- the height we need to fire for what range to target ---
               else
                  %z += (%targDist / 10);
               if (%z < %c)
                  %z = %c;

               %task.firePos = %x SPC %y SPC %z;//------------------------------------------------------------------------- %task.firePos ---

               %tx = firstWord(%task.shouldAttack.getTransform());
               %ty = getWord(%task.shouldAttack.getTransform(), 1);
               %tz = getWord(%task.shouldAttack.getTransform(), 2);
               %tz += mFloor(getRandom(2, 5) * 5);//add some altitude to search for targets on the ground

               %task.searchPos = VectorAdd(%tx SPC %ty SPC %tz, "0 0 5");// -- 5 more ^ ---
               %task.searchTime = getSimTime();
            }

            %safeRange = %task.shouldAttack.turret.getMountedImage(0).attackRadius;//---------- set the safeRange

            %pos2D = getWord(%myPos, 0) SPC getWord(%myPos, 1) SPC "0";
       %dest2D = getWord(%task.shouldAttack.getWorldBoxCenter(), 0) SPC getWord(%task.shouldAttack.getWorldBoxCenter(), 1) SPC "0";

            //---------------- find out if we are above or below target... ------------------------------------ Sec 2 --- Don't fly into danger zone ---
            if (VectorDist(%pos2D, %dest2D) < (%compZLev * 1.25))//--- we don't want to fly over a deployed MPB if we know it is there ---
            {
               %client.shrikeFire = false;//-------------- turn off the gun

               //if we don't have another location index give up
               if (VectorDist(%myPos, %task.location) < 50)
               {
                  if ((%task.count - 1) == %task.locationIndex)
                  {
                     AIDisembarkVehicle(%client); //--------- Hop off...
                     %client.stepMove(%task.location, 0.25);
                     //return;
                  }
                  else//else move to the next path marker
                     %task.locationIndex++;
               }

               %task.location = %task.group.getObject(%task.locationIndex).position;

               %client.setPilotPitchRange(-0.05, 0.025, 0.025);//------ set pitch range for flying
               %client.setPilotDestination(%task.location, 1.0);
            }
            //----------------------------------------------------------------------------------------------------------------- Sec 3 --- Safe Kill ---
            else if (%targDist > %safeRange)
            {
               %close = vectorDist(%myPos,  %task.firePos);
               if (%myZLev >= %targZLev && (%targDist / %compZLev > 1.5) && %compare < 0.65 && %compare > -0.65 && %hasLOS && %myZLev > %trHt + 3 && %close < 5)
               {
                  %client.setControlObject(%client.player);//---------------------------------------------------------------- take away pilot's control
                  //%vehicle.setTransform(%myPos SPC %xRot SPC %yRot SPC %zRot SPC %wRot);//--------------------- set the Transform Here - TEST HERE
                  %vehicle.setTransform(%task.firePos SPC %xRot SPC %yRot SPC %zRot SPC %wRot);//------ set the Transform Here (CHEAT HERE, works better)
                  %client.shrikeFire = true;// ----------------------------------------------------------------------------------- turn ON the Guns
               }
               else if (%hasLos && checkLOSPoint(%task.firePos, %targPos))
               {
                  %client.shrikeFire = false;//-------------- turn off the gun
                  %client.setControlObject(%vehicle);//------ give pilot control of shrike

                  if (VectorDist(%myPos, %task.firePos) < 8.0)
                  {
                     if (%myZLev < %targZLev)// --- set a little higher if we are too low ---
                     {
                        %vx = getWord(%myPos, 0);
                        %vy = getWord(%myPos, 1);
                        %vz = getWord(%myPos, 2);
                        %vz += 25.0;
                        %client.setPilotDestination(%vx SPC %vy SPC %vz, 0.5);//--- move UP a little
                     }
                     else//else just aim at target
                        %client.setPilotAim(%task.shouldAttack.getWorldBoxCenter());//--- aim at targetPoint
                  }
                  else//move into firing position
                     %client.setPilotDestination(%task.firePos, 0.5);
               }
               else
               {
                  %task.shouldAttack = -1;//------------ clear the target so we can aquire a new one
                  %client.shrikeFire = false;//-------------- turn off the guns
                  %client.setControlObject(%vehicle);//------ give pilot control of shrike

                  //if we have another location index
                  if (VectorDist(%myPos, %task.location) < 50)
             {
                     if ((%task.count - 1) == %task.locationIndex)
                     {
                        %task.locationIndex--;
//                        AIDisembarkVehicle(%client); //--------- Hop off...
//                        %client.stepMove(%task.location, 0.25);
                        //return;
                     }
                     else
                     {
                        %task.locationIndex++;
                        %task.location = %task.group.getObject(%task.locationIndex).position;
                     }
                  }
                  else//searching
                     %client.setPilotDestination(%task.searchPos, 0.5);
               }
            }
            else if (%targDist > 15.0 && %targDist <= %safeRange)//--------------------------- we are to close to safely attack so MOVE TO IT !
            //----------------------------------------------------------------------------------------- Sec 4 --- Move in for a close kill ---
            {
               %client.shrikeFire = false;//-------------- turn off the gun
               %client.setControlObject(%vehicle);//------ give pilot control of shrike
               %x = getWord(%vehicle.getTransform(), 0);
               %y = getWord(%vehicle.getTransform(), 1);
               %z = getWord(%task.shouldAttack.getTransform(), 2);
               if (%z < %terHt)
                  %z = %terHt + 6.0;
               else
                  %z += 2.0;
               %task.firePos = %x SPC %y SPC %z;//------------------------------------------------------- %task.firePos ---

               if (%myZLev >= %targZLev && (%targDist / %compZLev > 0.75) && %compare < 0.65 && %compare > -0.65 && %hasLOS &&
                 %myZLev > %trHt +3 && %targDist > 40.0)
               {
                  if (%collision)
                     Stop(%vehicle, %mySpd, 10);// ------------------------------------------------------------------------ new function to STOP x 10

                  %client.setPilotDestination(%task.shouldAttack.getWorldBoxCenter(), 0.5);
                  %client.setControlObject(%client.player);//------------------------------------------------------- take away pilot's control
                  %vehicle.setTransform(%myPos SPC %xRot SPC %yRot SPC %zRot SPC %wRot);//---------------------------- set the Transform Here
                  %client.shrikeFire = true;//---------------------------------------------------------------------------- turn on the guns
               }

               else if (%myZLev >= %targZLev && (%targDist / %compZLev > 0.75) && %compare < 0.65 && %compare > -0.65 && %hasLOS &&
                 %myZLev > %trHt + 3 && %targDist < 40.0)
               {
                  if (%mySpd > 5)// ---------------------------------------------------------------- STOP if we are flying in for a close kill
                     Stop(%vehicle, %mySpd, 10);//------------------------------------------------------- new function to STOP x 10

                  %client.setControlObject(%client.player);//-------------------------------- take away pilot's control
                  //%vehicle.setTransform(%myPos SPC %xRot SPC %yRot SPC %zRot SPC %wRot);//------------------------ set the Transform Here
                  %vehicle.setTransform(%task.firePos SPC %xRot SPC %yRot SPC %zRot SPC %wRot);//------------------- set the Transform Here (CHEAT HERE)
                  %client.shrikeFire = true;//----------------------------------------------------- turn on the guns
               }

               else if (%targDist < 40)
               {
                  if (%mySpd > 5)// ---------------------------------------------------------- STOP if we are flying in for a close kill
                     Stop(%vehicle, %mySpd, 10);// -------------------------------------------------------- new function to STOP x 10

                  %client.setPilotDestination(%myPos, 0.0);
                  %client.setControlObject(%vehicle);//---------------------------------------------- give pilot control of shrike
                  %client.setPilotAim(%task.shouldAttack.getWorldBoxCenter());
                  %client.shrikeFire = false;//-------------- turn off the gun
               }

               //if we have another location index
               else if (VectorDist(%myPos, %task.location) < 50)
          {
                  %client.setControlObject(%vehicle);//------ give pilot control of shrike
                  %client.shrikeFire = false;//-------------- turn off the gun

                  if ((%task.count - 1) == %task.locationIndex)
                  {
                     AIDisembarkVehicle(%client); //--------- Hop off...
                     %client.stepMove(%task.location, 0.25);
                     //return;
                  }
                  else
                  {
                     %task.locationIndex++;
                     %task.location = %task.group.getObject(%task.locationIndex).position;
                  }
               }
               else//else just move towards the target
                  %client.setPilotDestination(%task.shouldAttack.getWorldBoxCenter(), 1.0);
            }
            else//--- we are to close to safley attack so MOVE IT ! --------------------------------------------- Sec 5 --- breaking off ---
            {
               %client.shrikeFire = false;//-------------- turn off the gun
               %client.setControlObject(%vehicle);//------ give pilot control of shrike

               //if we have another location index
               if (VectorDist(%myPos, %task.location) < 50)
          {
                  if ((%task.count - 1) == %task.locationIndex)
                  {
                     AIDisembarkVehicle(%client); //--------- Hop off...
                     %client.stepMove(%task.location, 0.25);
                     //return;
                  }
                  else
                  {
                     %task.locationIndex++;
                     %task.location = %task.group.getObject(%task.locationIndex).position;
                  }
               }
               else
               {
                  %client.setPilotPitchRange(-0.05, 0.025, 0.025);//------ set pitch range for flying
                  %client.setPilotDestination(%task.location, 1.0);
               }
            }
         }
         //---------------------------------------------------------------------------------------------------------------
         //----------------------------------------------------------- NON DEPLOYED WHEELED / HOVER VEHICLE DATA CLASS ---                    //---------------------------------------------------------------------------------------------------------------
         else if ((%targClass $= "HoverVehicleData" || %targClass $= "WheeledVehicleData") && isObject(%task.shouldAttack))
         {
            %client.setControlObject(%vehicle);//-------------------- give pilot control of shrike and leave it there ---

       %pos2D = getWord(%myPos, 0) SPC getWord(%myPos, 1) SPC "0";
       %dest2D = getWord(%task.shouldAttack.getWorldBoxCenter(), 0) SPC getWord(%task.shouldAttack.getWorldBoxCenter(), 1) SPC "0";

            //---------------- find out if we are above or below target... ---
       if (VectorDist(%pos2D, %dest2D) < 25 && (%compZLev > 7.0 || %compZLev < -7.0))
            {
               %client.shrikeFire = false;//-------------- turn off the gun

               //if we don't have another location index back up
               if (VectorDist(%myPos, %task.location) < 50)
          {
                  if ((%task.count - 1) == %task.locationIndex)
                     %task.locationIndex--;
                  else
                     %task.locationIndex++;
               }

               %task.location = %task.group.getObject(%task.locationIndex).position;

               %client.setPilotPitchRange(-0.025, 0.025, 0.025);//------ set pitch range for flying
               %client.setPilotDestination(%task.location, 1.0);
            }
            else if (%hasLOS && %targDist > 35 && ((%mySpd > 15.0 && %targDist > 100.0) || %mySpd < 15.0))//--- close the distance if target has missile stuff ---
            {
               if (%compare < 0.65 && %compare > -0.65 && %compZLev < 55.0 && %compZLev > -35.0)
               {
                  if (%collision)
                     Stop(%vehicle, %mySpd, 10);// --------------------------------- new function to STOP x 10

                  %client.setControlObject(%client.player);//-------------------------------- take away pilot's control
                  %vehicle.setTransform(%myPos SPC %xRot SPC %yRot SPC %zRot SPC %wRot);//----- set the Transform Here

                  %client.shrikeFire = true;//-------------------------- turn on the guns
               }
               else
               {
                  %client.setControlObject(%vehicle);//------ give pilot control of shrike

                  %client.shrikeFire = false;//-------------- turn off the gun
                  %client.setPilotPitchRange(-0.05, 0.025, 0.025);//------ set pitch range for flying

                  if (%trHt < getWord(%task.firePos, 2))//if the terrain height is lower than firepos
                     %client.setPilotDestination(%task.shouldAttack.getWorldBoxCenter(), 1.0);//--- move to the target
                  else
                     %client.setPilotDestination(%task.searchPos, 1.0);//--- move to search

               }
            }
            else if (%hasLos)
               {
                  %client.shrikeFire = false;//-------------- turn off the gun
                  %client.setControlObject(%vehicle);//------ give pilot control of shrike

                  if (VectorDist(%myPos, %task.firePos) < 15.0)
                  {
                     if (%myZLev < %targZLev + 5)//set a little higher if we are too low
                     {
                        %vx = getWord(%myPos, 0);
                        %vy = getWord(%myPos, 1);
                        %vz = getWord(%myPos, 2);
                        %vz += 20.0;

                        %client.setPilotDestination(%vx SPC %vy SPC %vz, 1.0);//--- move UP a little
                     }
                     else
                     {
                        %client.setPilotAim(%task.shouldAttack.getWorldBoxCenter());//--- aim at targetPoint
                        %vehicle.setTransform(%myPos SPC %xRot SPC %yRot SPC %zRot SPC %wRot);//-- set the Transform Here
                     }
                  }
                  else
                     %client.setPilotDestination(%task.firePos, 1.0);
               }
            else
            {
               %client.setControlObject(%vehicle);//---- give pilot control of shrike
               %client.shrikeFire = false;//-------------- turn off the gun
               %client.setPilotPitchRange(-0.05, 0.025, 0.025);//------ set pitch range for flying
               %client.setPilotDestination(%task.searchPos, 1.0);
            }
         }
         //------------------------------------------------------------------------------------------------------------------
         //------------------------------------------------------------------------------------ FLYING VEHICLE DATA CLASS ---                         //------------------------------------------------------------------------------------------------------------------

         // -------------------------------------------------------------------------------------------------------------------------------- sec 1 ---
         // --------------------------------------------------------------------------------------------- this sec just updates the task locations ---

         else if (%targClass $= "FlyingVehicleData" && isObject(%task.shouldAttack))
         {
            %client.setControlObject(%vehicle);//----------------------------------------- give pilot control of shrike and leave it there ---
            %type = %task.shouldAttack.getDataBlock().getName();// ----------------------- we may need this later for dif types of flyers ---

       %pos2D = getWord(%myPos, 0) SPC getWord(%myPos, 1) SPC "0";
       %dest2D = getWord(%task.shouldAttack.getWorldBoxCenter(), 0) SPC getWord(%task.shouldAttack.getWorldBoxCenter(), 1) SPC "0";

            //---------------- find out if we are above or below target... ---
       if (VectorDist(%pos2D, %dest2D) < 45 && (%compZLev > 7.0 || %compZLev < -7.0))
            {
               %client.shrikeFire = false;//-------------- turn off the gun

               //if we don't have another location index back up
               if (VectorDist(%myPos, %task.location) < 50)
          {
                  if ((%task.count - 1) == %task.locationIndex)
                     %task.locationIndex--;
                  else
                     %task.locationIndex++;
               }

               %task.location = %task.group.getObject(%task.locationIndex).position;
            }
            // ---------------------------------------------------------------------------------------------------------------------------- sec 2 ---
            // ----------------------------------------------------------------------------------------------------- this sec makes LOS decisions ---

            if (%hasLOS)
            {
               // --------------------------------------------------------------------------------------------------------------- check directions
               //%direction = VectorDot(VectorSub(%clientPos, %targPos), %target.player.getVelocity());
               //%facing = VectorDot(VectorSub(%client.getAimLocation(), %clientPos), VectorSub(%targPos, %clientPos));
          //if (%dist > 20 && %dist < 45 && (%direction > 0.9 || %direction < -0.9) && (%facing > 0.9))
          //{
             //%player.throwStrength = 1.0;
        //%player.use(%grenadeType);
          //}(original here) *

               %targVel = %task.shouldAttack.getVelocity();
               %targVel = VectorNormalize(%targVel);
               %targDir = VectorDot(VectorSub(%myPos, %task.shouldAttack.getWorldBoxCenter()), %targVel);

               %myVel = %vehicle.getVelocity();
               %myVel = VectorNormalize(%myVel);
               %myDir = VectorDot(VectorSub(%task.shouldAttack.getWorldBoxCenter(), %myPos), %myVel);

               if (%targDir > -75 && %targDir < 75)
               {
                  //sendEcho(%client, "!!! TARGET IS MOVING TOWARDS US !!!");
               }
               //sendEcho(%client, "sec 2 - TARGET DIRECTION = " @ %targDir);
               //echo("-------------------------------------");

               if (%myDir > 75 || %myDir < -75)
               {
                  //sendEcho(%client, "sec 2 - WE ARE MOVING TOWARDS TARGET !!!");
               }

               //sendEcho(%client, "sec 2 MY DIRECTION = " @ %myDir);

               //echo("-------------------------------------");

               // --------------------------------------------------------------------------------------------------------------------------- sec 3 ---
               // -------------------------------------------------------------------------------------- this sec does the shooting and dogfighting ---

               //if (%compare < 0.65 && %compare > -0.65 && %compZLev < 35.0 && %compZLev > -35.0)//testing here 2-1-2005
               if (%compare < 0.65 && %compare > -0.65 && %compZLev < 35.0 && %compZLev > -35.0)
               {

                  %client.setPilotPitchRange(-0.05, 0.025, 0.025);//------ set pitch range for flying (smooth)

                  if (!%collision && %targDist > 75)//------------------------------------------------------------------ if no collision move to target
                  {
                     %vehicle.setTransform(%myPos SPC %xRot SPC %yRot SPC %zRot SPC %wRot);//------------------------------------ set the Transform Here
                     %client.shrikeFire = true;// ----------------------------------------------------------------------------------- turn on the guns

                     if (%targDist > 150 && %myDir < -75 || %myDir > 75  && %mySpd < (%vehData.maxForwardSpeed * 3))// ---- applyImpluse Foward               < * TEST THIS ALL * >
                     {
                        %client.setPilotDestination(%targPos, 1.0);// ---------------------------------------------------- keep moving to target

                        if (%targDir <= -75 || %targDir >= 75)// --------------------------------------------------- target is not coming toward us
                        {
                           //sendEcho(%client, "sec 3a - FLYING VEHICLE DATA CLASS - FAR applyImpluse Foward 10");
                           applyKick(%vehicle, "foward", (%skill * 12), true);//----------------------------------------------- give chase
                           //sendEcho(%client, "sec 3 - Apply KICK - give chase 10 true");
                        }
                     }
                     else
                        %client.setPilotDestination(%targPos, 0.5);// ------------------------------------------------- keep moving to target (slow)

                     // --------------------------------------------------------------------- if targ is approaching us we should evade (Dogfighting) ---
                     if (%targDist > 15 && %targDir > -75 && %targDir < 75)
                     {
                        %mv = getRandom(0, 2);
                        if (%mv < 0.4)
                        {
                           applyKick(%vehicle, "up", 10);
                           //sendEcho("sec 3a - Apply RANDOM ^ UP ^ KICK 10 false");
                        }
                        else if (%mv < 0.8)
                        {
                           applyKick(%vehicle, "left", 15, true);
                           //sendEcho(%client, "sec 3a - Apply RANDOM < LEFT < KICK 15 true");
                        }
                        else if (%mv < 1.2)
                        {
                           applyKick(%vehicle, "right", 15, true);
                           //sendEcho(%client, "sec 3a - Apply RANDOM > RIGHT > KICK 15 true");
                        }
                        else if (%mv < 1.6 && %myAlt > 100)//************************************************************* TESTING HERE ********
                        {
                           applyKick(%vehicle, "Down", 5, true);
                           //sendEcho(%client, "sec 3a - Apply RANDOM !!! Apply DOWN  KICK !!!!!! 5 true");
                        }
                     }
                     else if (%targDist < 15)
                        %client.setPilotDestination(%task.location, 1.0);


                  }

                  // -------------------------------------------------------------------------------------------------------------------- sec 3a ---
                  // -------------------------------------------------------------- this sec does the stopping and collision avoidance --- Shoot = true

                  else
                  {
                     %vehicle.setTransform(%myPos SPC %xRot SPC %yRot SPC %zRot SPC %wRot);//------------------------------------- set the Transform Here

                     if (%mySpd < 35)//--------------------------------------- if we are going slow just move to fire position
                        %client.setPilotDestination(%task.firePos, 1.0);

                     %client.setPilotAim(%task.shouldAttack.getWorldBoxCenter());//--- aim at targetPoint
                     if (%collision)//----------------------------------------------------------- if we are going to crash (into target) stop and evade
                     {
                        Stop(%vehicle, %mySpd, 12);// ----------------------------------------------------------------------- new function to STOP x 10

                        //sendEcho(%client, "sec 3b - COLLISION !!! STOP !!!!!!!!!!!!!!! 12");

                        %mv = getRandom();
                        if (%mv < 0.2 && %myZLev < (nameToId("MissionArea").flightCeiling - 20))
                        {
                           applyKick(%vehicle, "up", 15);
                           //sendEcho(%client, "sec 3b - COLLISION !!! Apply ^ UP ^ KICK !!!!!!  15");
                        }
                        else if (%mv < 0.4)
                        {
                           applyKick(%vehicle, "left", 15, true);
                           //sendEcho(%client, "sec 3b - COLLISION !!! Apply < LEFT < KICK !!!!!!  15 true");
                        }
                        else if (%mv < 0.6)
                        {
                           applyKick(%vehicle, "right", 15, true);
                           //sendEcho(%client, "sec 3b - COLLISION !!! Apply > RIGHT > KICK !!!!!!  15 true");
                        }
                        else if (%mv < 0.8 && %myAlt > 100)//************************************************************* TESTING HERE ********
                        {
                           applyKick(%vehicle, "Down", 5, true);
                           //sendEcho(%client, "sec 3b - COLLISION !!! Apply DOWN  KICK !!!!!! 5 true");
                        }
                     }
                  }

                  %client.shrikeFire = true;// -------------------------------------------------------------------- turn on the guns
               }

               //--------------------------------------------------------------------------------------------------------------------------- sec 4 ---
               //----------------------------------------------------------------------------- this sec helps move us into a good position to fire ---

               else
               {
                  %client.shrikeFire = false;//-------------- turn off the gun
                  %client.setPilotPitchRange(-0.05, 0.05, 0.025);//------ set pitch range for attacking
                  %client.setPilotDestination(%task.shouldAttack.getWorldBoxCenter(), 1.0);//--- move to the target

                  if (%myZLev > (%targZLev + 10) && %targZLev > (%terHt + 10))
                  {
                     applyKick(%vehicle, "down", (%skill * 5));
                     //sendEcho(%client, "sec 4 - FLYING VEHICLE DATA CLASS - We are higher than target Move Down - " @ (%skill * 5) @ " false");
                  }
                  if (%myZLev < (%targZLev - 10) && %targZLev > (%terHt + 10))
                  {
                     applyKick(%vehicle, "up", 10, true);
                     //sendEcho(%client, "sec 4 - FLYING VEHICLE DATA CLASS - We are LOWER than target Move ^UP - 10 true");
                  }
               }
            }

            //------------------------------------------------------------------------------------------------------------------------------- sec 5 ---
            //------------------------------------------------------------------------------------ this sec handles if we do not have LOS to target ---

            else
            {
               //sendEcho(%client, "sec 5 - WE DO NOT HAVE LOS To TARGET");
               %client.shrikeFire = false;//-------------- turn off the gun

               %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
               %hasLOS = !containerRayCast(%myPos, %task.searchLocation, %mask, 0);//------- %hasLOS

               %client.setPilotPitchRange(-0.025, 0.025, 0.025);// -------- a smooth setting here

               if (%hasLOS)
               {
                  //sendEcho(%client, "sec 5 - FLYING VEHICLE DATA CLASS - move to %task.location - we HAVE los to search location");
                  %client.setPilotDestination(%task.searchLocation, 1.0);//max speed
               }
               else
               {
                  //sendEcho(%client, "sec 5 - FLYING VEHICLE DATA CLASS - MOVE ^UP - we DO NOT have los to search location");
                  %client.setPilotDestination(%vx SPC %vy SPC %vz + 10, 1.0);//max speed
               }
            }
         }
        //------------------------------------------------------------------------------------------------------------ End Of Target Data Classes ---
      }
      //------------------------------------------------------------------------------------------------------------------------------------------------
      //--------------------------------------------------------------------------------------------------------------------- Following Path Markers ---
      //------------------------------------------------------------------------------------------------------------------------------------------------

      else//----------------------------------------------------------------------------------------- we have no target so follow flight path to the end
      {
         %task.shouldAttack = -1;//-------------------------- set task no target
         %client.setControlObject(%vehicle);//--------------- give pilot control
         %client.shrikeFire = false;//----------------------- turn off the guns

         //-------------------------------------------------- set the locations
         %task.location = %task.group.getObject(%task.locationIndex).position;

         //are we close to location index marker?
    if (VectorDist(%myPos, %task.location) < (%mySpd || 75))//use our speed or 75m (which ever is less) to see how close we are the location marker
    {
       //if we have another location index
            if ((%task.count - 1) > %task.locationIndex)//has to be -1 on the %task.group count
               %task.locationIndex++;//------------------------------------------------------------ set the destination to the next marker

       else//---------------------------------------------------------------------------------------- else we are at end of trail
            {
               if (%task == %client.objectiveTask)
          {
                  if (%client.vehicleMounted )
                  {
                      AIDisembarkVehicle(%client); //------------------------------------------------------- Hop off...
                      %client.stepMove(%task.location, 0.25);
                      return;
                  }
          }
            }
         }
         else//---------------------------------------------------------------------------------------------- else we follow the flight path
         {
            %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
            %hasLOS = !containerRayCast(%myPos, %task.location, %mask, 0);//------- %hasLOS

            if (%hasLOS)//------------------------------------------------------------------------------- if we can see the next destination
            {
               //help this cowboy stay more or less on the path
               if (%myZLev < (getWord(%task.location, 2) - 75) && %myZLev < (nameToId("MissionArea").flightCeiling - 20))
               {
                  %client.setPilotPitchRange(-0.005, 0.005, 0.005);// -------- a smooth setting here
                  %client.setPilotDestination(%vx SPC %vy SPC %vz + 10, 1.0);//--- fly straight up a little
               }
               else if (%myZLev < (getWord(%task.location, 2) - 10) && %myZLev < (nameToId("MissionArea").flightCeiling - 20))
               {
                  //applyKick(%vehicle, "up", (%skill * 2));
                  applyKick(%vehicle, "up", (%skill + 1));
               }
               else if (%myZLev > (getWord(%task.location, 2) + 10) && !%collision)
               {
                  //applyKick(%vehicle, "down", (%skill / 2));
                  applyKick(%vehicle, "down", (%skill + 0.5));
               }

               %client.setPilotPitchRange(-0.005, 0.005, 0.005);// ----------------------- a real smooth setting here
               %client.setPilotDestination(%task.location, 1.0);//--------------------- move to the destination at max speed
            }
            else//---------------------------------------------------------------------------- else we are blocked to the next destination
            {
               if (%myZLev < (nameToId("MissionArea").flightCeiling - 20))//---------- don't fly off the map
               {
                  %client.setPilotPitchRange(-0.005, 0.005, 0.005);// -------- a smooth setting here
                  %client.setPilotDestination(%vx SPC %vy SPC %vz + 10, 1.0);//--- fly straight up a little
                  applyKick(%vehicle, "up", (%skill + 2));//------------------------ help him by skill level
               }
               else//--------------------------------------------------------- else just bounce off things like a retard
               {
                  %client.setPilotPitchRange(-0.005, 0.004, 0.005);// -------- a smooth setting here
                  %client.setPilotDestination(%task.location, 1.0);//max speed
               }
            }
         }

      //%client.setPilotPitchRange(-0.2, 0.05, 0.05);+ Dynamix Original Here + this sucks + max seems to be -0.05, 0.05, 0.05 +++
      }
   }
   else//------------------------------------------------------------------------------------- We are not Vehicle mounted ---
   {
      //if we at end of path and we hopped out
      if ((%task.count - 1) == %task.locationIndex)//has to be -1 on the groups count
      {
         if (%task == %client.objectiveTask)
         {
            %client.player.mVehicle = "";//---------------test here
            AIUnassignClient(%client);
            Game.AIChooseGameObjective(%client);
         }
      }

      //did we fall off our bike? if so get back on!
      if (isObject(%client.player.mVehicle) && %client.player.mVehicle.getDamageState() !$= "Destroyed" && %client.pilotVehicle)//never happens anymore thank God.
      {
         //if ai in heavy armor buy equipment
         if (%task == %client.objectiveTask)
         {
            %task.baseWeight = %client.objectiveWeight;
            %task.equipment = "Medium || FieldTech HeavyLauncher";
            %task.buyEquipmentSet = "MediumTailgunner";
            %client.needEquipment = true;
            return;
         }

         //check if someone stole our bike
         if (%client.player.mVehicle.getMountNodeObject(0) > 0)
         {
            if (%task == %client.objectiveTask)
            {
               AIUnassignClient(%client);
               Game.AIChooseGameObjective(%client);
               return;
            }
         }

         //throw away any packs that won't fit
         if (%client.player.getInventory(InventoryDeployable) > 0)
            %client.player.throwPack();
         else if (%client.player.getInventory(TurretIndoorDeployable) > 0)
            %client.player.throwPack();
         else if (%client.player.getInventory(TurretOutdoorDeployable) > 0)
            %client.player.throwPack();

         %client.pilotVehicle = true;//needed to let ai mount pilot seat in any vehicle
         %client.stepMove(%client.player.mVehicle.position, 0.25, $AIModeMountVehicle);
      }

      //did someone shoot our legs out? if so we are done!
      else if (isObject(%client.player.mVehicle) && %client.player.mVehicle.getDamageState() $= "Destroyed")
      {
         %lastDamager = %client.player.mVehicle.lastDamagedBy.client;
         if (%lastDamager > 0 && %lastDamager.team != %client.team && !%lastDamager.isAIControlled()) {
            %task.objective.vehicleDestroyed++; // counts number of vehicles destroyed by human opponents. Used for spawning invisible vehicles using DeepCoverPack - pinkpanther
            //echo("Shrike shot down: Client/count/lastDamager = " @ %client.nameBase SPC %task.objective.vehicleDestroyed SPC %lastDamager.nameBase);
         }
         if (%task == %client.objectiveTask)
         {
            AIUnassignClient(%client);
            Game.AIChooseGameObjective(%client);
         }
      }
   }
}//------------------------------------------------------------------------------------------------------------ End of AIPilotShrike - Lagg...

// ------------------------------------------------------------------ function send echo
function sendEcho(%client, %mes)
{
   //just use team 1 to test scripts (faster this way)
   if (%client.team == 1)
      error("T1 - " @ %mes);
}

//used to check if firing postion has los to target
function checkLOSPoint(%pt, %pt1)
{
   //check for LOS
   %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
   %LOS = !containerRayCast(%pt, %pt1, %mask, 0);

   if (%LOS)
      return true;
   else
      return false;
}

// ------------------------------------------------------------------ function stop - thanks Dev
function Stop(%vehicle, %mySpd, %times)
{
   %Position = %vehicle.getWorldBoxCenter();
   %Velocity = %vehicle.getVelocity();
   %Factor = %mySpd * 3;
   %ForceVector = VectorScale(VectorNormalize(%Velocity), -1 * %Factor);
   for(%i = 0; %i < %times; %i ++)
   {
      %vehicle.ApplyImpulse(%Position, %ForceVector);
   }
}

//--------------------------------------------------------------------------------------------------------------------- function findClosestEnemyTarget(%client, %vehicle)
function findClosestEnemyTarget(%client, %vehicle)
{
   //lets find some enemy targets (turrets, solar panels and sensors)
   %clTarg = -1;
   %clDist = 300;   //initialize so only targets within 300 m will be detected

   %tarCount = $AIVehTargSet.getCount();
   for (%i = 0; %i < %tarCount; %i++)
   {
      %pTarg = $AIVehTargSet.getObject(%i);//new $set in ai.cs
      if (%pTarg.team > 0 && %pTarg.team != %client.team)
      {
    if (%pTarg.getDamageState() !$= "Destroyed")
    {
            %clTargPos = %pTarg.getWorldBoxCenter();
       %dist = vectorDist(%vehicle.position, %clTargPos);
       if (%dist < %clDist)
       {
               //check for LOS
               %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType |
                 $TypeMasks::StaticTSObjectType | $TypeMasks::ForceFieldObjectType;
               %vLOS = !containerRayCast(%vehicle.getWorldBoxCenter(), %clTargPos, %mask, 0);

               if (%vLOS)
               {
                  %clTarg = %pTarg;
                  %clDist = %dist;
               }
       }
    }
      }
   }

   return %clTarg @ " " @ %clDist;
}

//-------------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------- some help from Dev to set up the $TypeMasks::
$DamageableMask = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType |
                  $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType |
                  $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType |
                  $TypeMasks::MoveableObjectType | $TypeMasks::DamagableItemObjectType;

$EverythingMask = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType |
                  $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType |
                  $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType |
                  $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType |
                  $TypeMasks::ForceFieldObjectType | $TypeMasks::StaticObjectType |
                  $TypeMasks::MoveableObjectType | $TypeMasks::DamagableItemObjectType;

//-------------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------- new function by ZOD to help me out :)
function applyKick(%obj, %dir, %amt, %useVelocity)
{
   if (%useVelocity $= "")
      %useVelocity = false;

   %data = %obj.getDataBlock();
   //%position = posFromTransform(%obj.getTransform());
   %position = %obj.getWorldBoxCenter();

   %velocity = %obj.getVelocity();
   %normal = vectorDot(%velocity, vectorNormalize(%velocity));
   if(%normal > 100) // Whatever cap we wish..
   {
      //echo("applyKick - WHAT IS THIS ?");
      %kick = %amt / %normal; // Reduce impulse.
      //retrun; // Or apply no impulse at all and exit out.
   }
   else
      %kick = %amt;

   if (%UseVelocity)
      %forwardVector = VectorNormalize(%velocity);
   else
      %forwardVector = %obj.getEyeVector();

   switch$(%dir)
   {
      case "foward":
         %impulse = VectorScale(%ForwardVector, %Kick * %Data.mass);

      case "reverse":
         %impulse = VectorScale(%ForwardVector, (%Kick * %Data.mass) * -1);

      case "up":
         %impulse = VectorScale("0 0 1", %Kick * %Data.mass);

      case "down":
         %impulse = VectorScale("0 0 -1", %Kick * %Data.mass);

      case "left":
         %impulse = VectorScale(VectorCross(%ForwardVector, "0 0 -1"), %Kick * %Data.mass);

      case "right":
         %impulse = VectorScale(VectorCross(%ForwardVector, "0 0 1"), %Kick * %Data.mass);

      default :
         error("Unknown direction passed to function applyKick: " @ %dir);
   }

   %obj.applyImpulse(%position, %impulse);
}

//-------------------------------------------------------------------------------------------------------------------------

// new function for ai pilots to check for collisions
function aiCollisionCheck(%vehicle, %speed)
{
   %vehPos = %vehicle.getWorldBoxCenter();

   //use the Y-axis of the vehicle rotation as the desired direction to check for collisions
   %muzzle = MatrixMulVector("0 0 0 " @ getWords(%vehicle.getTransform(), 3, 6), "0 1 0");
   %muzzle = VectorNormalize(%muzzle);
   %point = %muzzle;
   %factor = 4; //4m in front of vehicle is the right spot to start
   %muzzlePoint = VectorAdd(%vehPos,VectorScale(%muzzle, %factor));
   %rangePoint = VectorAdd(%vehPos,VectorScale(%point, %speed * 5));

   //error("%muzzlePoint = " @ %muzzlePoint);
   //error("%rangePoint = " @ %rangePoint);

   %masks = $TypeMasks::TerrainObjectType |
              $TypeMasks::InteriorObjectType |
              $TypeMasks::ForceFieldObjectType |
              $TypeMasks::StaticObjectType |
              $TypeMasks::MoveableObjectType |
              $TypeMasks::DamagableItemObjectType |
              $TypeMasks::VehicleObjectType |
              $TypeMasks::StaticTSObjectType;



   %collision = ContainerRayCast(%muzzlePoint, %rangePoint, %masks, 0);

   if (%collision)
      return true;

   return false;
}
