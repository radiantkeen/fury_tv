//------------------------------
//TASKS AND OBJECTIVES

//AIO Attack Location
//AIO Attack Object - teleport true
//AIO Attack Player
//AIO Defend Location - teleport true
//AIO Defense Patrol - teleport true
//AIO Deploy Equipment - teleport true
//AIO Deploy Vehicle
//AIO Destroy Object - teleport true
//AIO Escort Player
//AIO Laze Object
//AIO MAttack Location
//AIO MDestroy Object - teleport true
//AIO Missile Object - teleport true
//AIO Mortar Object - teleport true, requests escort
//AIO Place Camera
//AIO Repair Object - teleport true
//AIO Touch Object - teleport true, CTF requests escort, sniper, artillery (mortar)

//--------------------------------------------------------------------------------------------------- AIO Attack Location ---

function AIOAttackLocation::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;
      
   //now, if this bot is linked to a human who has issued this command, up the weight
   if (%this.issuedByClientId == %client.controlByHuman)
   {
      //make sure we have the potential to reach the minWeight
      if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
      {
         if ($AIWeightHumanIssuedCommand < %minWeight)
	    return 0;
	 else
	    %weight = $AIWeightHumanIssuedCommand;
      }
      else
      {
         // calculate the default...
	 %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
	 if (%weight < $AIWeightHumanIssuedCommand)
	    %weight = $AIWeightHumanIssuedCommand;
      }
   }
   else
   {
      //make sure we have the potential to reach the minWeight
      if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
         return 0;

      // calculate the default...
      %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
   }

   return %weight;
}

function AIOAttackLocation::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIAttackLocation);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIOAttackLocation::unassignClient(%this, %client)
{
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//----------------------------------------------------------------------------------------------------- AI Attack Location ---
//ai.getLOSLocation(targetPoint [, minDist, maxDist, nearPoint])

function AIAttackLocation::initFromObjective(%task, %objective, %client)
{
   //initialize the task vars from the objective
   %task.baseWeight = %client.objectiveWeight;
   %task.location = %objective.location;
   %task.equipment = %objective.equipment;
   %task.buyEquipmentSet = %objective.buyEquipmentSet;
   %task.desiredEquipment = %objective.desiredEquipment;
   %task.issuedByClient = %objective.issuedByClientId;
   %task.chat = %objective.chat;

   //initialize other task vars
   %task.sendMsg = true;
   %task.sendMsgTime = 0;
   %task.engageTarget = -1;
}

function AIAttackLocation::assume(%task, %client)
{
   %task.setWeightFreq(30);
   %task.setMonitorFreq(30);

   if (%task.equipment !$= "")
      %equipmentList = %task.equipment;
   else
      %equipmentList = %task.desiredEquipment;
   %client.needEquipment = AINeedEquipment(%equipmentList, %client);

   //even if we don't *need* equipemnt, see if we should buy some... 
   if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
   {
      //see if we could benefit from inventory
      %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      %closestDist = getWord(%result, 1);
      if (AINeedEquipment(%task.desiredEquipment, %client) && %closestInv > 0)
      {
         %result = AIFindClosestEnemyToLoc(%client, %task.location, 50, $AIClientLOSTimeout);
	 %closestEnemy = getWord(%result, 0);

         if (%closestEnemy <= 0 && %closestDist < 100)
            %client.needEquipment = true;
      }
   }

   //-------------- added extra - Lagg...
   if (%client.needEquipment && %task.equipment $= "")
   {
      if (%client.player.getInventory(SniperRifle) > 0 && %client.player.getInventory(EnergyPack) > 0)
         %client.needEquipment = false;

      %hasMortar = (%client.player.getInventory("Mortar") > 0) && (%client.player.getInventory("MortarAmmo") > 0);
      %hasDisc = (%client.player.getInventory("Disc") > 0) && (%client.player.getInventory("DiscAmmo") > 0) && (%client.player.getInventory("Blaster") <= 0);
      if (%hasMortar || %hasDisc)
         %client.needEquipment = false;
   }      

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();

   //initialize the sniper varibles
   %task.snipeLocation = "";
   %task.hideLocation = "";
   %task.moveToPosition = true;
   %task.moveToSnipe = false;
   %task.nextSnipeTime = 0;
}

function AIAttackLocation::retire(%task, %client)
{
   //do nothing here
}

function AIAttackLocation::weight(%task, %client)
{
   //update the task weight
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;

   //if we're a sniper, we're going to cheat, and see if there are clients near the attack location
   %losTimeout = $AIClientMinLOSTime + ($AIClientLOSTimeout * %client.getSkillLevel());
   %distToLoc = VectorDist(%client.player.getWorldBoxCenter(), %task.location);

   //if we have sniper gear (cheat ignores LOS)...
   if (((%client.player.getInventory(SniperRifle) > 0 && %client.player.getInventory(EnergyPack) > 0) ||
     (%client.player.getInventory(Mortar) > 0 && %client.player.getInventory(MortarAmmo) > 0)) && %distToLoc > 60)
      %result = AIFindClosestEnemyToLoc(%client, %task.location, 100, $AIClientLOSTimeout, true);

   //otherwise, do the search normally.
   else
      %result = AIFindClosestEnemyToLoc(%client, %task.location, 75, %losTimeout, false);

   %closestEnemy = getWord(%result, 0);
   %closestdist = getWord(%result, 1);
   %task.setWeight(%task.baseWeight);
   
   //see if we found someone
   if (%closestEnemy > 0)
      %task.engageTarget = %closestEnemy;
   else
      %task.engageTarget = -1;
}

function AIAttackLocation::monitor(%task, %client)
{
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
	 %equipmentList = %task.desiredEquipment;
      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
         return;
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(30);
	 %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
	 //if this task is the objective task, choose a new objective
	 if (%task == %client.objectiveTask)
	 {
	    AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	 }
	 return;
      }
   }

   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   //chat
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
	 if (%client.isAIControlled())
	 {
	    if (%task.chat !$= "")
	    {
	       %chatMsg = getWord(%task.chat, 0);
	       %chatTemplate = getWord(%task.chat, 1);
	       if (%chatTemplate !$= "")
	          AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
	       else
		  AIMessageThread(%task.chat, %client, -1);
	    }
	    else
	       AIMessageThreadTemplate("AttackBase", "ChatSelfAttack", %client, -1);
	 }
      }
   }

   //how far are we from the location we're attacking
   %myPos = %client.player.getWorldBoxCenter();
   %distance = %client.getPathDistance(%task.location);
   if (%distance < 0)
      %distance = 32767;

   if (%client.player.getInventory(SniperRifle) > 0 && %client.player.getInventory(EnergyPack) > 0)
   {
      //first, find an LOS location
      if (%task.snipeLocation $= "")
      {
         %task.snipeLocation = %client.getLOSLocation(%task.location, 150, 450);
	 %task.hideLocation = %client.getHideLocation(%task.location, VectorDist(%task.location, %task.snipeLocation),
           %task.snipeLocation, 1); 
	 %client.stepMove(%task.hideLocation, 4.0);
	 %task.moveToPosition = true;
      }
      else
      {
         //see if we can acquire a target
	 %energy = %client.player.getEnergyPercent();
	 %distToSnipe = VectorDist(%task.snipelocation, %client.player.getWorldBoxCenter());
	 %distToHide = VectorDist(%task.hidelocation, %client.player.getWorldBoxCenter());

         //until we're in position, we can move using the AIModeExpress, after that, we only want to walk...
	 if (%task.moveToPosition)
	 {
	    if (%distToHide < 4.0)
	    {
	       //dissolve the human control link
	       if (%task == %client.objectiveTask)
	       {
	          if (aiHumanHasControl(%task.issuedByClient, %client))
		  {
		     aiReleaseHumanControl(%client.controlByHuman, %client);

		     //should re-evaluate the current objective weight
		     %inventoryStr = AIFindClosestInventories(%client);
		     %client.objectiveWeight = %client.objective.weight(%client, %client.objectiveLevel, 0, %inventoryStr);
		  }
	       }

	       %task.moveToPosition = false;
	    }
	 }
	 else if (%task.moveToSnipe)
	 {
	    if (%energy > 0.75 && %client.getStepStatus() $= "Finished")
	    {
	       %client.stepMove(%task.snipeLocation, 4.0, $AIModeWalk);
	       %client.setEngageTarget(%task.engageTarget);
	    }
	    else if (%energy < 0.4)
	    {
	       %client.setEngageTarget(-1);
	       %client.stepMove(%task.hideLocation, 4.0);
	       %task.nextSnipeTime = getSimTime() + 1000 + (getRandom() * 1000);
	       %task.moveToSnipe = false;
	    }
         }
         else if (%energy > 0.5 && %task.engageTarget > 0 && getSimTime() > %task.nextSnipeTime)
         { 
            %client.stepRangeObject(%task.engageTarget.player.getWorldBoxCenter(), "BasicSniperShot", 150, 250, %task.snipelocation);
	    %client.aimAt(%task.engageTarget.player.getWorldBoxCenter(), 8000);
	    %task.moveToSnipe = true;
	 }
      }
   }
   //---------------------------------------------------------------------------------------------------------------------
   //see if we can mortar spam somebody
   else if (%client.player.getInventory(Mortar) > 0 && %client.player.getInventory(MortarAmmor) > 0)
   {
      //first, find an LOS location
      if (%task.snipeLocation $= "")
      {
         %task.snipeLocation = %client.getLOSLocation(%task.location, 100, 350);
	 %task.hideLocation = %client.getHideLocation(%task.location, VectorDist(%task.location, %task.snipeLocation),
           %task.snipeLocation, 1); 
	 %client.stepMove(%task.hideLocation, 4.0);
	 %task.moveToPosition = true;
      }
      else
      {
         //see if we can acquire a target
	 %energy = %client.player.getEnergyPercent();
	 %distToSnipe = VectorDist(%task.snipelocation, %client.player.getWorldBoxCenter());
	 %distToHide = VectorDist(%task.hidelocation, %client.player.getWorldBoxCenter());

         //until we're in position, we can move using the AIModeExpress, after that, we only want to walk...
	 if (%task.moveToPosition)
	 {
	    if (%distToHide < 4.0)
	    {
	       //dissolve the human control link
	       if (%task == %client.objectiveTask)
	       {
	          if (aiHumanHasControl(%task.issuedByClient, %client))
		  {
		     aiReleaseHumanControl(%client.controlByHuman, %client);

		     //should re-evaluate the current objective weight
		     %inventoryStr = AIFindClosestInventories(%client);
		     %client.objectiveWeight = %client.objective.weight(%client, %client.objectiveLevel, 0, %inventoryStr);
		  }
	       }

	       %task.moveToPosition = false;
	    }
	 }
	 else if (%task.moveToSnipe)
	 {
	    if (%energy > 0.75 && %client.getStepStatus() $= "Finished")
	    {
	       %client.stepMove(%task.snipeLocation, 4.0, $AIModeWalk);
	       %client.setEngageTarget(%task.engageTarget);
	    }
	    else if (%energy < 0.4)
	    {
	       %client.setEngageTarget(-1);
	       %client.stepMove(%task.hideLocation, 4.0);
	       %task.nextSnipeTime = getSimTime() + 1000 + (getRandom() * 500);
	       %task.moveToSnipe = false;
	    }
         }
         else if (%energy > 0.5 && %task.engageTarget > 0 && getSimTime() > %task.nextSnipeTime)
         { 
            %client.stepRangeObject(%task.engageTarget.player.getWorldBoxCenter(), "MortarShot", 40, 400,
              %task.snipelocation);
	    %client.aimAt(%task.engageTarget.player.getWorldBoxCenter(), 8000);
	    %task.moveToSnipe = true;
	 }
      }
   }
   //----------------------------------------------------------------------------------------------------------------
   //else just attack like a brave
   else
   {
      //else see if we have a target to begin attacking
      if (%client.getEngageTarget() <= 0 && %task.engageTarget > 0)
         %client.stepEngage(%task.engageTarget);

      //else move to the location we're defending
      else if (%client.getEngageTarget() <= 0)
      {
         %client.stepMove(%task.location, 8.0);
         if (VectorDist(%client.player.position, %task.location) < 10)
         {
	    //dissolve the human control link
	    if (%task == %client.objectiveTask)
	    {
	       if (aiHumanHasControl(%task.issuedByClient, %client))
	       {
	          aiReleaseHumanControl(%client.controlByHuman, %client);

		  //should re-evaluate the current objective weight
		  %inventoryStr = AIFindClosestInventories(%client);
		  %client.objectiveWeight = %client.objective.weight(%client, %client.objectiveLevel, 0, %inventoryStr);
	       }
	    }
	 }
      }
   }

   //see if we're supposed to be engaging anyone...
   if (!AIClientIsAlive(%client.getEngageTarget()) && AIClientIsAlive(%client.shouldEngage))
      %client.setEngageTarget(%client.shouldEngage);
}

//---------------------------------------------------------------------------------------------------------------------- AIO Attack Object ---
//totally rewritten so but will RAPE the object. used only for generators in siege - Lagg... 11-1-2003
function AIOAttackObject::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   // if were playing CnH, check who owns this
   if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden() || %this.targetObjectId.team == %client.team)
      return 0;          
   
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   //lets see if we have a force field in our path
   InitContainerRadiusSearch(%client.player.getWorldBoxCenter(), 8, $TypeMasks::ForceFieldObjectType);
   %objSearch = containerSearchNext();
   if (%objSearch > 0)
   {
      %ffDis = %client.getPathDistance(%objSearch.getWorldBoxCenter());
      %myDis = %client.getPathDistance(%this.position);
      if (%ffDis < %myDis )
      {  
         if (%objSearch.isPowered() && %objSearch.team != %client.team)
            return 0;
      }
   }
      
   //no need to attack if the object is already destroyed
   if (!isObject(%this.targetObjectId) || %this.targetObjectId.getDamageState() $= "Destroyed")
      return 0;
   else
   {
      //if this bot is linked to a human who has issued this command, up the weight
      if (%this.issuedByClientId == %client.controlByHuman)
      {
         //make sure we have the potential to reach the minWeight
	 if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
	 {
	    if ($AIWeightHumanIssuedCommand < %minWeight)
	       return 0;
	    else
	       %weight = $AIWeightHumanIssuedCommand;
         }
	 else
	 {
	    // calculate the default...
	    %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
	    if (%weight < $AIWeightHumanIssuedCommand)
	       %weight = $AIWeightHumanIssuedCommand;
	 }
      }
      else
      {
         //make sure we have the potential to reach the minWeight
	 if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
	    return 0;

	 // calculate the default...

         %this.desiredEquipment = "Plasma PlasmaAmmo";
	 %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
      }

      //if we are playing DnD, check if objective was completed already, if not give some extra :) - Lagg... 3-29-2004
      if (Game.class $= "DnDGame")
      {
         if (%this.targetObjectId.objectiveCompleted)
            %weight -= 400;
         else
            %weight += 200;
      }

      //if he can see it set accordingly - Lagg...
      if (VectorDist(%client.player.getWorldBoxCenter(), %this.targetObjectId.getWorldBoxCenter()) < 150)
      {

         //check for LOS
         %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType |
           $TypeMasks::ForceFieldObjectType | $TypeMasks::VehicleObjectType;
         %hasLOS = !containerRayCast(%client.player.getMuzzlePoint($WeaponSlot), %this.targetObjectId.getWorldBoxCenter(), %mask, 0);

         if (%hasLOS)
            %weight += 100;
      }

      return %weight;
   }
}

function AIOAttackObject::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIAttackObject);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIOAttackObject::unassignClient(%this, %client)
{
   //error("AIOAttackObject::unassignClient - " @ gettaggedString(%client.name));

   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//------------------------------------------------------------------------------------------------------------------- AI Attack Object ---
function AIAttackObject::initFromObjective(%task, %objective, %client)
{
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;
   %task.equipment = %objective.equipment;
   %task.buyEquipmentSet = %objective.buyEquipmentSet;
   %task.desiredEquipment = %objective.desiredEquipment;
   %task.issuedByClient = %objective.issuedByClientId;
   %task.location = %objective.location;

   //follow path - Lagg...
   %task.paths = %objective.paths;
   %task.followPath = false;

   //initialize other task vars
   %task.sendMsg = true;
   %task.sendMsgTime = 0;

   //MPB Telepoter
   %task.useTeleport = false;

   //Sparky's Teleporters
   %task.useTele = false;
   %task.tele = -1;
}

function AIAttackObject::assume(%task, %client)
{
   %task.setWeightFreq(50);//was 15
   %task.setMonitorFreq(50);//was 15

   //added here so Bot buys a random set if specified in objective dataBlock - Lagg... 2-14-2003
      if (%task.buyEquipmentSet $= "randomSet")
      {
         //pick a random equipment set...
         %randNum = getRandom();
         if (%randNum < 0.2)
         {
            %task.desiredEquipment = "Plasma PlasmaAmmo";
            %task.buyEquipmentSet = "HeavyEnergySet";
         }
         else if (%randNum < 0.4)
         {
            %task.desiredEquipment = "Plasma PlasmaAmmo";
            %task.buyEquipmentSet = "MediumEnergySet";
         } 
         else if (%randNum < 0.6)
         {
            %task.desiredEquipment = "Plasma PlasmaAmmo";
            %task.buyEquipmentSet = "LightCloakSet";
         }
         else if (%randNum < 0.8)
         {
            %task.desiredEquipment = "Plasma PlasmaAmmo";
            %task.buyEquipmentSet = "HeavyShieldOff";
         }
         else
         {
            %task.desiredEquipment = "Plasma PlasmaAmmo";
            %task.buyEquipmentSet = "MediumMissileSet";
         }
      }

   if (%task.equipment !$= "")
      %equipmentList = %task.equipment;
   else
      %equipmentList = %task.desiredEquipment;
   %client.needEquipment = AINeedEquipment(%equipmentList, %client);

   //even if we don't *need* equipemnt, see if we should buy some... 
   if (!%client.needEquipment && %task.buyEquipmentSet !$= "")
   {
      //see if we could benefit from inventory
      %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      %closestDist = getWord(%result, 1);
      if (AINeedEquipment(%task.desiredEquipment, %client) && %closestInv > 0)
      {
         //find where we are
         %clientPos = %client.player.getWorldBoxCenter();
         %objPos = %task.targetObject.getWorldBoxCenter();
         %distToObject = %client.getPathDistance(%objPos);

	 if (%distToObject < 0 || %closestDist < 25)//-------- 25m or less to inventoryStation in this case
            %client.needEquipment = true;
      }
   }

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();


   if (!%client.inBase)
   {
      %result = aiTeleporterCheck(%client, %task.targetObject);
      %task.useTele = getWord(%result, 0);//true or false
      %task.tele = getWord(%result, 1);//this is the linked tele to use

      //--------------------------------------------------- follow paths ---
      //set the destination paths for each team and game type
   
      //first see how many paths we have
      //check if attack path exists
      if (Game.class $= "SiegeGame")
         %team = Game.offenseTeam == %client.team ? 1 : 2;
      else
         %team = %client.team;

      //first see how many paths we have
      %mx = getWordCount(%task.paths);
      //echo("AIAttackObject::assume - # paths = " @ %mx);

      if (%mx > 0)
      {
         //and which path to use
         %random = mFloor(getRandom(1, %mx));
         %random--;
         %tg = getWord(%task.paths, %random);
         //echo("AIAttackObject::assume - path to use = " @ %tg);
         %task.group = nameToId("T" @ %team @ "AttackPath" @ %tg);
         %task.count = %task.group.getCount();  
         %task.locationIndex = 0;

         //make sure there are markers in the AttackPath group
         if (%task.count > 0)
            %task.followPath = true;
      }
   }

   //--------------------------------------------------- assume telporter - start -
   //if the MPB Teleporter is online - Lagg... 9-30-2003

   //first check for deployed MPB
   %result = AIFindDeployedMPB(%client);//new function in aiVehicle.cs
   %closestMPB = getWord(%result, 0);
   %closestMPBDist = getWord(%result, 1);
      
   //next find the teleporter
   %result = SweepForTeleporters(%client);//new function in aiVehicle.cs
   %closestTel = getWord(%result, 0);
   %closestTelDist = getWord(%result, 1);
      
   //now check for what would be closer - Lagg... 10-1-2003
   if (%closestMPB > 0 && %closestTel > 0)
   {
      %disToTarg = %client.getPathDistance(%task.targetObject.getTransform());
      %mpbDisToTarg = vectorDist(%closestMPB.getTransform(), %task.targetObject.getTransform());
         
      if (%closestTelDist < %closestMPBDist && %closestMPBDist < %disToTarg && %mpbDisToTarg < %disToTarg)
         %task.useTeleport = true;//new - Lagg... 10-1-2003
      else
         %task.useTeleport = false;             
   }
   //--------------------------------------------------- assume telporter - end -

   //error("AIAttackObject::assume - client = " @ getTaggedString(%client.name));//more debugging
}

function AIAttackObject::retire(%task, %client)
{
   %client.setTargetObject(-1);
}

function AIAttackObject::weight(%task, %client)
{
   //update the task weight
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;

   //see if we can find someone to shoot at...
   if (%client.getEngageTarget() <= 0)
   {
      %losTimeout = $AIClientMinLOSTime + ($AIClientLOSTimeout * %client.getSkillLevel());
      %myLocation = %client.player.getWorldBoxCenter();
      %result = AIFindClosestEnemy(%client, 40, %losTimeout);
      %task.engageTarget = getWord(%result, 0);
   }
   
   %task.setWeight(%task.baseWeight);
}

function AIAttackObject::monitor(%task, %client)
{
   //--------------------------------------------------- monitor telporter - start -
   //are we using teleporter
   if (%task.useTeleport)
   {
      %result = AIFindDeployedMPB(%client);//new function in aiVehicle.cs
      %closestMPB = getWord(%result, 0);
      %closestMPBDist = getWord(%result, 1);
      %result = SweepForTeleporters(%client);//new function in aiVehicle.cs
      %closestTel = getWord(%result, 0);
      %closestTelDist = getWord(%result, 1);
      if (%closestMPB > 0 && %closestTel > 0)
      {
         //we are done teleporting
         if (%closestMPBDist < %closestTelDist)
         {
            //reset the clients inventory status
            if (%client.needEquipment)
            {
               %result = AIFindClosestInventory(%client, 0);
	       %closestInv = getWord(%result, 0);
               %client.invToUse = %closestInv;
            }
            %task.useTeleport = false;
            return;
         }

         if (%closestTelDist < 2)
            %client.pressJump();
         else
         {
            %client.stepMove(%closestTel.getWorldBoxCenter(), 0.25, $AIModeWalk);
            return;
         }
      }
      else
         %task.useTeleport = false;
   }
   //--------------------------------------------------- monitor telporter - end -

   //first, buy the equipment - if we really really need some :) - Lagg... - 4-6-2003
   %hasMortar = (%client.player.getInventory("Mortar") > 0) && (%client.player.getInventory("MortarAmmo") > 0);
   %hasPlasma = (%client.player.getInventory("Plasma") > 0) && (%client.player.getInventory("PlasmaAmmo") > 0);
   %hasDisc = (%client.player.getInventory("Disc") > 0) && (%client.player.getInventory("DiscAmmo") > 0) && (!%client.player.getInventory("Blaster") > 0);
   if (%hasMortar || %hasPlasma || %hasDisc)
      %client.needEquipment = false;

   //first, buy the equipment - if we really really need some :) - Lagg... - 4-6-2003
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;
      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
         return;
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(50);//was 15
	 %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
         //if this task is the objective task, choose a new objective
	 if (%task == %client.objectiveTask)
	 {
	    AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	 }
         return;
      }
   }

   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();
   
   //chat
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
         {
	    if (%task.chat !$= "")
	    {
	       %chatMsg = getWord(%task.chat, 0);
	       %chatTemplate = getWord(%task.chat, 1);
	       if (%chatTemplate !$= "")
	          AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
	       else
		  AIMessageThread(%task.chat, %client, -1);
            }
	    else if (%task.targetObject > 0)
	    {
	       %type = %task.targetObject.getDataBlock().getName();
	       if (%type $= "GeneratorLarge")
	          AIMessageThreadTemplate("AttackBase", "ChatSelfAttackGenerator", %client, -1);
	       else if (%type $= "SensorLargePulse")
		  AIMessageThreadTemplate("AttackBase", "ChatSelfAttackSensors", %client, -1);
	       else if (%type $= "SensorMediumPulse")
		  AIMessageThreadTemplate("AttackBase", "ChatSelfAttackSensors", %client, -1);
	       else if (%type $= "TurretBaseLarge")
		  AIMessageThreadTemplate("AttackBase", "ChatSelfAttackTurrets", %client, -1);
	       else if (%type $= "StationVehicle")
		  AIMessageThreadTemplate("AttackBase", "ChatSelfAttackVehicle", %client, -1);
            }
         }
      }
   }


   //set the target object if targetObject not destroyed
   %outOfStuff = (AIAttackOutofAmmo(%client));
   if (isObject(%task.targetObject) && %task.targetObject.getDamageState() !$= "Destroyed" && !%outOfStuff)
   {
      //if we are using Sparky's teleporters
      if (%task.useTele)
      {
         %pos = %client.player.position;
         %dest = %task.tele.position;

         //are we close to location index marker?
         if (VectorDist(%dest, %pos) < 1.5 || %client.inBase)//1.5 meters from linked tele
         {
            %task.useTele = false;
            return;
         }
         else
         {
            error("AIAttackObject::monitor - WE ARE USING SPARKY'S TELEPORTER - " @ getTaggedString(%client.name));
            %client.stepMove(%task.tele.position, 0.25, $AIModeExpress);
         }
      }

      else if (%task.followPath)
      {
         //error("AIAttackObject::monitor - Following ATTACK Path - " @ getTaggedString(%client.name));

         //set the path and follow it to the end
         %task.spot = %task.group.getObject(%task.locationIndex).position;

         %pos = %client.player.position;
         %dest = %task.spot;

         //are we close to location index marker?
         if (VectorDist(%dest, %pos) < 1.5)//1.5 meters from marker
         {
            //if we have another location index
            if ((%task.count - 1) > %task.locationIndex)//has to be -1 on the groups count
               %task.locationIndex++;
            //else we are at end of trail
            else
            {
               //if this task is the objective task, choose a new objective
	       if (%task == %client.objectiveTask)
	       {
	          %task.followPath = false;
	          return;
               }
            }
         }
         else
            %client.stepMove(%task.spot, 0.25, $AIModeExpress);
      }
      else
         %client.stepMove(%task.location, 0.25, $AIModeExpress);//move to target

      %targetLOS = "false";
      %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType;
      %targetLOS = !containerRayCast(%client.player.getMuzzlePoint($WeaponSlot), %task.targetObject.getWorldBoxCenter(), %mask, 0);

      %close = false;
      if (VectorDist(%task.targetObject.position, %client.player.position) < 7)
         %close = true;
      
      //fire at target object if in sight
      if (%targetLOS && !%close)
      {
         %task.setWeight($AIWeightReturnFire);

         //if we are following a path, stop.
         %task.followPath = false;

         %client.setEngageTarget(-1);
         %client.setTargetObject(%task.targetObject, 100, "Mortar");
      }
      else
      {
         %client.setTargetObject(-1);
         %task.setWeight(%task.baseWeight);

         //see if we need to engage a new target
         %engageTarget = %client.getEngageTarget();
         if (!AIClientIsAlive(%engageTarget) && %task.engageTarget > 0)
            %client.setEngageTarget(%task.engageTarget);

         //else see if we should abandon the engagement
         else if (AIClientIsAlive(%engageTarget))
         {
            %myPos = %client.player.getWorldBoxCenter();
            %testPos = %engageTarget.player.getWorldBoxCenter();
            %distance = %client.getPathDistance(%testPos);
            if (%distance < 0 || %distance > 20)
               %client.setEngageTarget(-1);

            //see if we're supposed to be engaging anyone...
            if (!AIClientIsAlive(%client.getEngageTarget()) && AIClientIsAlive(%client.shouldEngage))
               %client.setEngageTarget(%client.shouldEngage);
         }
      } 
   }
   else
   {
      %client.setTargetObject(-1);      
            
      //if this task is the objective task, choose a new objective
      if (%task == %client.objectiveTask)
      {
         //if we need to repair self, aim at the floor
         if (%client.player.getDamagePercent() > 0 && %client.player.getInventory(RepairPack) > 0)
         {
            %pos = %client.player.getWorldBoxCenter();
            %posX = getWord(%pos, 0);
            %posY = getWord(%pos, 1);
            %posZ = getWord(%pos, 2) - 1;
            %pos = %posX SPC %posY SPC %posZ;
            %client.aimAt(%pos, 500);
         }
         AIUnassignClient(%client);
         Game.AIChooseGameObjective(%client);
      }
   }
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ AIO Attack Player +++

function AIOAttackPlayer::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

	//if we're attacking the flag carrier, make sure a flag carrier exists
	if (%this.mode $= "FlagCarrier")
	{
		if (%this.targetObjectId.carrier $= "")
			return 0;
		else
			%this.targetClientId = %this.targetObjectId.carrier.client;
	}
      
	//now, if this bot is linked to a human who has issued this command, up the weight
	if (%this.issuedByClientId == %client.controlByHuman)
	{
		//make sure we have the potential to reach the minWeight
		if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
		{
			if ($AIWeightHumanIssuedCommand < %minWeight)
				return 0;
			else
				%weight = $AIWeightHumanIssuedCommand;
		}
		else
		{
			// calculate the default...
			%weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
			if (%weight < $AIWeightHumanIssuedCommand)
				%weight = $AIWeightHumanIssuedCommand;
		}
	}
	else
	{
		//make sure we have the potential to reach the minWeight
		if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
			return 0;

		// calculate the default...
		%weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
	}

	return %weight;
}

function AIOAttackPlayer::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIAttackPlayer);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIOAttackPlayer::unassignClient(%this, %client)
{
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//------------------------------------------------------------------------------------------------- AI Attack Player ---

function AIAttackPlayer::initFromObjective(%task, %objective, %client)
{
   %task.baseWeight = %client.objectiveWeight;
   %task.targetClient = %objective.targetClientId;
   %task.equipment = %objective.equipment;
	%task.buyEquipmentSet = %objective.buyEquipmentSet;
	%task.desiredEquipment = %objective.desiredEquipment;
	%task.issuedByClient = %objective.issuedByClientId;
        %task.chat = %objective.chat;

	//initialize other task vars
	%task.sendMsg = true;
	%task.sendMsgTime = 0;
}

function AIAttackPlayer::assume(%task, %client)
{
   %task.setWeightFreq(15);
   %task.setMonitorFreq(15);
   %client.needEquipment = AINeedEquipment(%task.equipment, %client);
	if (! %client.needEquipment)
	   %client.stepEngage(%task.targetClient);

	//even if we don't *need* equipemnt, see if we should buy some... 
	if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
	{
		//see if we could benefit from inventory
		%needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
		%result = AIFindClosestInventory(%client, %needArmor);
		%closestInv = getWord(%result, 0);
		%closestDist = getWord(%result, 1);
		if (AINeedEquipment(%task.desiredEquipment, %client) && %closestInv > 0)
		{
			%distToTarg = %client.getPathDistance(%task.targetClient.player.getWorldBoxCenter());
			if (%distToTarg < 0 || %distToTarg > 100)
				%client.needEquipment = true;
		}
	}

	//mark the current time for the buy inventory state machine
	%task.buyInvTime = getSimTime();
}

function AIAttackPlayer::retire(%task, %client)
{
	//dissolve the human control link
	if (%task == %client.objectiveTask)
		aiReleaseHumanControl(%client.controlByHuman, %client);
}

function AIAttackPlayer::weight(%task, %client)
{
	//update the task weight
	if (%task == %client.objectiveTask)
		%task.baseWeight = %client.objectiveWeight;

   %task.setWeight(%task.baseWeight);
}

function AIAttackPlayer::monitor(%task, %client)
{
   //chat, first
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
	 {
	    if (%task.chat !$= "")
	    {
	       %chatMsg = getWord(%task.chat, 0);
	       %chatTemplate = getWord(%task.chat, 1);
	       if (%chatTemplate !$= "")
	          AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
	       else
		  AIMessageThread(%task.chat, %client, true);
	    }
	    else
               AIMessageThread("ChatSelfAttack", %client, -1);
	 }
      }
   }

   //second, buy the equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;
      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
         return;
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(15);
	 %client.needEquipment = false;
	 %client.stepEngage(%task.targetClient);
      }
      else if (%result $= "Failed")
      {
         ////if this task is the objective task, choose a new objective
	 //if (%task == %client.objectiveTask)
	 //{
	    //AIUnassignClient(%client);
	    //Game.AIChooseGameObjective(%client);
	 //}
	 //return;

         %task.setMonitorFreq(15);
	 %client.needEquipment = false;
	 %client.stepEngage(%task.targetClient);
      }
   }

   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   //cheap hack for now...   make the bot always know where you are...
   %client.clientDetected(%task.targetClient);

   //make sure we're still attacking...
   if (%client.getStepName() !$= "AIStepEngage")
      %client.stepEngage(%task.targetClient);

   //make sure we're still attacking the right target
   %client.setEngageTarget(%task.targetClient);

   if (%client.getStepStatus() !$= "InProgress" && %task == %client.objectiveTask)
   {
      AIUnassignClient(%client);
      Game.AIChooseGameObjective(%client);
   }
}

//------------------------------------------------------------------------------------------------ AIO Defend Location ---

//modified so if set to offensive in .mis file the bot will defend a destroyed enemy object (can you say Camping in Base ?) - Lagg... 1/7/2004
//must be placed in the offense teams objective simgroup and DO NOT set defense = 1
//for defense: DO NOT set ofense = 1 in .mis file, for Camping offense must not have defense > 0 in the .mis file  - * IMPORTANT * -
//also added if we are a defending with sniper gear (SniperRifle and EnergyPack) than defend like a sniper :) - Lagg... 3-25-2004

function AIODefendLocation::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   //who owns the object we are to defend?
   if (%this.targetObjectId > 0  && %this.defense > 0)
   {
      if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden() || %this.targetObjectId.team != %client.team)
         return 0;

      //don't defend unless the entire group is powered
      if (%this.targetObjectId.getGroup().powerCount <= 0)
         return 0;
   }
   else if (%this.targetObjectId > 0  && %this.offense > 0)
   {
      if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden() || %this.targetObjectId.team == %client.team)
         return 0;

      //don't camp unless all the gens in simgroup are destroyed
      if (%this.targetObjectId.getGroup().powerCount > 0)
         return 0;
   }

   //do a quick check to disqualify this objective if it can't meet the minimum weight
   if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
   {
      if (%this.targetObjectId > 0 && %this.issuedByClientId == %client.controlByHuman)
      {
         if ($AIWeightHumanIssuedCommand < %minWeight)
            return 0;
      }
      else
         return 0;
   }

   %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
   //if defensive  the object has been destroyed, reduce the weight / if offensive increase the weight if destroyed (Camping !)
   if (%this.targetObjectId > 0 && %this.defense > 0)
   {
      //see if we were forced on the objective
      if (%this.issuedByClientId == %client.controlByHuman && %weight < $AIWeightHumanIssuedCommand)
         %weight = $AIWeightHumanIssuedCommand;

      //else see if the object has been destroyed - slight modification here - Lagg... 11-7-2003
      //else if (!isObject(%this.targetObjectId) || %this.targetObjectId.getDamageState() $= "Destroyed")
      else if (!isObject(%this.targetObjectId) || %this.targetObjectId.isDisabled())
         %weight -= 320;
   }
   else if (%this.targetObjectId > 0 && %this.offense > 0)
   {
      //see if we were forced on the objective
      if (%this.issuedByClientId == %client.controlByHuman && %weight < $AIWeightHumanIssuedCommand)
			%weight = $AIWeightHumanIssuedCommand;

      // - slight modification here - Lagg... 11-7-2003
      else if (!isObject(%this.targetObjectId) || %this.targetObjectId.getDamageState() !$= "Destroyed")
         return 0;//don't camp if not destroyed

      if (%client.player.getInventory("InventoryDeployable") > 0)
         return 0;
   }

   return %weight;
}

function AIODefendLocation::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIDefendLocation);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIODefendLocation::unassignClient(%this, %client)
{
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//-------------------------------------------------------------------------------------------------------------- AI Defend Location ---

function AIDefendLocation::initFromObjective(%task, %objective, %client)
{
	//initialize the task vars from the objective
   %task.baseWeight = %client.objectiveWeight;
	%task.targetObject = %objective.targetObjectId;
	if (%objective.Location !$= "")
	   %task.location = %objective.location;
	else
	   %task.location = %objective.targetObjectId.getWorldBoxCenter();

   %task.equipment = %objective.equipment;
	%task.buyEquipmentSet = %objective.buyEquipmentSet;
	%task.desiredEquipment = %objective.desiredEquipment;
	%task.issuedByClient = %objective.issuedByClientId;
	%task.chat = %objective.chat;
        %task.mode = %objective.mode;

        //added for base camping if on offensive
        %task.defense = %objective.defense;
        %task.offense = %objective.offense;

	//initialize other task vars
	%task.sendMsg = true;
	%task.sendMsgTime = 0;
   
        %task.engageTarget = -1;
}

function AIDefendLocation::assume(%task, %client)
{
   %task.setWeightFreq(15);
   %task.setMonitorFreq(15);
   %client.inPerimeter = false;
	//%client.needEquipment = AINeedEquipment(%task.equipment, %client);

   if (%task.equipment !$= "")
      %equipmentList = %task.equipment;
   else
      %equipmentList = %task.desiredEquipment;
   %client.needEquipment = AINeedEquipment(%equipmentList, %client);


	//even if we don't *need* equipemnt, see if we should buy some... 
	if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
	{
		//see if we could benefit from inventory
		%needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
		%result = AIFindClosestInventory(%client, %needArmor);
		%closestInv = getWord(%result, 0);
		%closestDist = getWord(%result, 1);
		if (AINeedEquipment(%task.desiredEquipment, %client) && %closestInv > 0)
		{
		   %result = AIFindClosestEnemy(%client, 200, $AIClientLOSTimeout);
	           %closestEnemy = getWord(%result, 0);
		   %closestEnemydist = getWord(%result, 1);

			if (%closestEnemy <= 0 || (%closestEnemyDist > %closestDist * 1.5))
				%client.needEquipment = true;
		}
	}

	//mark the current time for the buy inventory state machine
	%task.buyInvTime = getSimTime();

	//set a flag to determine if the objective should be re-aquired when the object is destroyed/repaired - modified Lagg...
	%task.reassignOnDestroyed = false;
   //--------------------------------------------------- assume telporter - start -
   //if the MPB Teleporter is online - Lagg... 9-30-2003

   //first check for deployed MPB
   %result = AIFindDeployedMPB(%client);//new function in aiVehicle.cs
   %closestMPB = getWord(%result, 0);
   %closestMPBDist = getWord(%result, 1);
      
   //next find the teleporter
   %result = SweepForTeleporters(%client);//new function in aiVehicle.cs
   %closestTel = getWord(%result, 0);
   %closestTelDist = getWord(%result, 1);
      
   //now check for what would be closer - Lagg... 10-1-2003
   if (%closestMPB > 0 && %closestTel > 0)
   {
      %disToTarg = %client.getPathDistance(%task.location);
      %mpbDisToTarg = vectorDist(%closestMPB.getTransform(), %task.location);
         
      if (%closestTelDist < %closestMPBDist && %closestMPBDist < %disToTarg && %mpbDisToTarg < %disToTarg)
         %task.useTeleport = true;//new - Lagg... 10-1-2003
      else
         %task.useTeleport = false;             
   }
   //--------------------------------------------------- assume telporter - end -
}

function AIDefendLocation::retire(%task, %client)
{
	%task.engageVehicle = -1;
	%client.setTargetObject(-1);
}

function AIDefendLocation::weight(%task, %client)
{
   //update the task weight
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;

   %player = %client.player;
   if (!isObject(%player))
      return;

   %hasMissile = (%player.getInventory("MissileLauncher") > 0) && (%player.getInventory("MissileLauncherAmmo") > 0);

   //if we're defending with a missile launcher, our first priority is to take out vehicles...
   //see if we're already attacking a vehicle...
   if (%task.engageVehicle > 0 && isObject(%task.engageVehicle) && %hasMissile)
   {
      //set the weight
      %task.setWeight(%task.baseWeight);
      return;
   }

   //search for a new vehicle to attack
   %task.engageVehicle = -1;
   %losTimeout = $AIClientMinLOSTime + ($AIClientLOSTimeout * %client.getSkillLevel());
   %result = AIFindClosestEnemyPilot(%client, 300, %losTimeout);
   %pilot = getWord(%result, 0);
   %pilotDist = getWord(%result, 1);

   //if we've got missiles, and a vehicle to attack...
   if (%hasMissile && AIClientIsAlive(%pilot))
   {
      %task.engageVehicle = %pilot.vehicleMounted;
      %client.needEquipment = false;
   }

   //otherwise look for a regular enemy to fight... added for sniper defender - Lagg... 4-16-2004
   else
   {
      //if we are sniper extend the range
      if (%client.player.getInventory(SniperRifle) > 0 && %client.player.getInventory(EnergyPack) > 0)
         %result = AIFindClosestEnemyToLoc(%client, %task.location, 500, $AIClientLOSTimeout);
      ////if we are offense camping cheat a little, ignore LOS :) - Lagg...
      //else if (%task.offense)
	 //%result = AIFindClosestEnemyToLoc(%client, %task.location, 150, %losTimeout, true);
      //if we have mortar than extend the range
      else if (%client.player.getInventory(Mortar) > 0 && %client.player.getInventory(MortarAmmo) > 0)
         %result = AIFindClosestEnemyToLoc(%client, %task.location, 400, $AIClientLOSTimeout, false);
      //otherwise, do the search normally.
      else
	 %result = AIFindClosestEnemyToLoc(%client, %task.location, 150, %losTimeout, false);

      %closestEnemy = getWord(%result, 0);
      %closestdist = getWord(%result, 1);
	   
      //see if we found someone
      if (%closestEnemy > 0)
         %task.engageTarget = %closestEnemy;
      else
      {
         %task.engageTarget = -1;

	 //see if someone is near me...
	 %result = AIFindClosestEnemy(%client, 150, %losTimeout);
         %closestEnemy = getWord(%result, 0);
         %closestdist = getWord(%result, 1);
	 if (%closestEnemy <= 0 || %closestDist > 150)
	    %client.setEngageTarget(-1);
      }
   }

   //set the weight
   %task.setWeight(%task.baseWeight);
}

function AIDefendLocation::monitor(%task, %client)
{
   //if the defend location task has an object, set the "reset" flag
   if (%task == %client.objectiveTask && isObject(%task.targetObject))
   {
      //if (%task.targetObject.getDamageState() !$= "Destroyed")//modified for offensive camping (heh heh) - Lagg... 11-9-2003
      if (%task.targetObject.isDisabled() && %task.defense > 0)
         %task.reassignOnDestroyed = true;
      else if (%task.targetObject.isEnabled() && %task.offense > 0)
	 %task.reassignOnDestroyed = true;

      if (%task.reassignOnDestroyed)
      {
         AIUnassignClient(%client);
	 Game.AIChooseGameObjective(%client);
         return;
      }
   }

   //buy the equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
	 %equipmentList = %task.desiredEquipment;
      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
         return;
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(15);
	 %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
         //if this task is the objective task, choose a new objective
	 if (%task == %client.objectiveTask)
	 {
	    AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	 }
	 return;
      }
   }
   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   //chat
   if (%task.sendMsg)
   {
		if (%task.sendMsgTime == 0)
			%task.sendMsgTime = getSimTime();
		else if (getSimTime() - %task.sendMsgTime > 7000)
		{
			%task.sendMsg = false;
		   if (%client.isAIControlled())
			{
				if (%task.chat !$= "")
				{
					%chatMsg = getWord(%task.chat, 0);
					%chatTemplate = getWord(%task.chat, 1);
					if (%chatTemplate !$= "")
						AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
					else
						AIMessageThread(%task.chat, %client, -1);
				}
				else if (%task.targetObject > 0)
				{
					%type = %task.targetObject.getDataBlock().getName();
					if (%type $= "Flag")
						AIMessageThreadTemplate("DefendBase", "ChatSelfDefendFlag", %client, -1);
					else if (%type $= "GeneratorLarge")
						AIMessageThreadTemplate("DefendBase", "ChatSelfDefendGenerator", %client, -1);
					else if (%type $= "StationVehicle")
						AIMessageThreadTemplate("DefendBase", "ChatSelfDefendVehicle", %client, -1);
					else if (%type $= "SensorLargePulse")
						AIMessageThreadTemplate("DefendBase", "ChatSelfDefendSensors", %client, -1);
					else if (%type $= "SensorMediumPulse")
						AIMessageThreadTemplate("DefendBase", "ChatSelfDefendSensors", %client, -1);
					else if (%type $= "TurretBaseLarge")
						AIMessageThreadTemplate("DefendBase", "ChatSelfDefendTurrets", %client, -1);
				}
			}
		}
	}

   //--------------------------------------------------- monitor telporter - start -
   //are we using teleporter
   if (%task.useTeleport)
   {
      %result = AIFindDeployedMPB(%client);//new function in aiVehicle.cs
      %closestMPB = getWord(%result, 0);
      %closestMPBDist = getWord(%result, 1);
      %result = SweepForTeleporters(%client);//new function in aiVehicle.cs
      %closestTel = getWord(%result, 0);
      %closestTelDist = getWord(%result, 1);
      if (%closestMPB > 0 && %closestTel > 0)
      {
         //we are done teleporting
         if (%closestMPBDist < %closestTelDist)
         {
            //reset the clients inventory status
            if (%client.needEquipment)
            {
               %result = AIFindClosestInventory(%client, 0);
	           %closestInv = getWord(%result, 0);
               %client.invToUse = %closestInv;
            }
            %task.useTeleport = false;
            return;
         }

         if (%closestTelDist < 2)
            %client.pressJump();
         else
         {
            %client.stepMove(%closestTel.getWorldBoxCenter(), 0.25);
            return;
         }
      }
      else
         %task.useTeleport = false;
   }
   //--------------------------------------------------- monitor telporter - end -

   //first, check for a vehicle to engage
   if (%task.engageVehicle > 0 && isObject(%task.engageVehicle))
   {
      %client.stop();
      %client.clearStep();
      %client.setEngageTarget(-1);
      %client.setTargetObject(%task.engageVehicle, 300, "Missile");
   }
   else
   {
      //clear the target vehicle...
      %client.setTargetObject(-1);

      //see if we're engaging a player
      if (%client.getEngageTarget() > 0)
      {
         //too far, or killed the enemy - return home
         %distance = %client.getPathDistance(%task.location);
         if (%client.getStepStatus() !$= "InProgress" || %distance > 75)
         {
            //%client.setEngageTarget(-1);
            %client.stepMove(%task.location, 8.0);
         } 
      }
	   
      //else see if we have a target to begin attacking
      if (%task.engageTarget > 0 && AIClientIsAlive(%task.engageTarget))
      {
         if (%client.player.getInventory(SniperRifle) > 0 && %client.player.getInventory(EnergyPack) > 0)
	 {
            %client.stepRangeObject(%task.engageTarget.player.getWorldBoxCenter(), "BasicSniperShot", 150, 500, %task.location);
            %client.aimAt(%task.engageTarget.player.getWorldBoxCenter(), 8000);
            %client.setEngageTarget(%task.engageTarget);
         }
         else
	    %client.stepEngage(%task.engageTarget);
      }
	      
      //else move to a random location around where we are defending
      else if (%client.getStepName() !$= "AIStepIdlePatrol")
      {
         %dist = VectorDist(%client.player.getWorldBoxCenter(), %task.location);
	 if (%dist < 10)
	 {
            //dissolve the human control link and re-evaluate the weight
            if (%task == %client.objectiveTask)
	    {
	       if (aiHumanHasControl(%task.issuedByClient, %client))
	       {
	          aiReleaseHumanControl(%client.controlByHuman, %client);

		  //should re-evaluate the current objective weight
		  %inventoryStr = AIFindClosestInventories(%client);
		  %client.objectiveWeight = %client.objective.weight(%client, %client.objectiveLevel, 0, %inventoryStr);
               }
            }

            %client.stepIdle(%task.location);
         }
         else
	    %client.stepMove(%task.location, 8.0);
      }
   }

   //see if we're supposed to be engaging anyone...
   if (!AIClientIsAlive(%client.getEngageTarget()) && AIClientIsAlive(%client.shouldEngage))
      %client.setEngageTarget(%client.shouldEngage);
}

//---------------------------------------------------------------------------------------------------------------------------- AIO Defense Patrol ---
//this is a new objective in beta. bot will walk from location to location doing the default defense stuff.
//*** note to lagg redo this for more than 1 path like you did for vehicle objectives *** (ahh No need Too - Lagg 9/20/2006)

//Requires a simgroup titled T<%team#>PatrolPath<#> (example: T1PatrolPath1)  including 'markers' to set as locations to defend.
//When placing this AIOObjectiveMarket in the AI Editor you must add a dynamic field named "Path" and put a value of "1" or "2" or any
//number you want. BUT ONLY ONE NUMBER. Exit the AI Editor and creat a simgroup for this path with the NUMBER that you placed as the
//value of the path. EXAMPLE: T1PatrolPath1 or T2PatrolPath4 stands for team1patrolpath1, team2patrolpath4 only one simgroup per AIO
//Objective marker inside that simgroup you place markers at the exact location the bot will walk to and stand gaurd for a rondom
//amount of time and then will choose another location from the simgroup at ramdom. Place as many location markers in thgere as you
//lik.But make sure the location is pathable.

function AIODefensePatrol::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   // if were playing CnH, check who owns this
   if (%this.targetObjectId > 0  && %this.defense > 0)
   {
      if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden() || %this.targetObjectId.team != %client.team)
         return 0;

      //don't defend unless the entire group is powered
      if (%this.targetObjectId.getGroup().powerCount <= 0)
         return 0;
   }
   else if (%this.targetObjectId > 0  && %this.offense > 0)
   {
      if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden() || %this.targetObjectId.team == %client.team)
         return 0;

      //don't camp unless base is down
      if (%this.targetObjectId.getGroup().powerCount > 0)
         return 0;
   }
   
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

	//do a quick check to disqualify this objective if it can't meet the minimum weight
	if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
	{
		if (%this.targetObjectId > 0 && %this.issuedByClientId == %client.controlByHuman)
		{
			if ($AIWeightHumanIssuedCommand < %minWeight)
				return 0;
		}
		else
			return 0;
	}

	%weight = AIODefault::weight(%this, %client, %level, %inventoryStr);

        //if defensive  the object has been destroyed, reduce the weight / if offensive increase the weight if destroyed (Camping !)
	if (%this.targetObjectId > 0 && %this.defense > 0)
	{

		//see if we were forced on the objective
		if (%this.issuedByClientId == %client.controlByHuman && %weight < $AIWeightHumanIssuedCommand)
			%weight = $AIWeightHumanIssuedCommand;

		//else see if the object has been destroyed - slight modification here - Lagg... 11-7-2003
	   //else if (!isObject(%this.targetObjectId) || %this.targetObjectId.getDamageState() $= "Destroyed")
           else if (!isObject(%this.targetObjectId) || %this.targetObjectId.isDisabled())
			%weight -= 320;
	}
        else if (%this.targetObjectId > 0 && %this.offense > 0)
	{

		//see if we were forced on the objective
		if (%this.issuedByClientId == %client.controlByHuman && %weight < $AIWeightHumanIssuedCommand)
			%weight = $AIWeightHumanIssuedCommand;

		// - slight modification here - Lagg... 11-7-2003
                else if (!isObject(%this.targetObjectId) || %this.targetObjectId.getDamageState() !$= "Destroyed")
			return 0;//don't camp if not destroyed
                if (%client.player.getInventory("InventoryDeployable") > 0)
                   return 0;
	}

	return %weight;
}

function AIODefensePatrol::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIDefensePatrol);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIODefensePatrol::unassignClient(%this, %client)
{
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//--------------------------------------------------------------------------------------------------------------------------------- AI Defense Patrol ---

function AIDefensePatrol::initFromObjective(%task, %objective, %client)
{
   //initialize the task vars from the objective
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;
   if (%objective.Location !$= "")
      %task.location = %objective.location;
   else
      %task.location = %objective.targetObjectId.getWorldBoxCenter();

   %task.equipment = %objective.equipment;
   %task.desiredEquipment = %objective.desiredEquipment;
   %task.buyEquipmentSet = %objective.buyEquipmentSet;
   %task.path = %objective.Path;

   if (%task.buyEquipmentSet $= "randomSet")
   {
      //pick a random equipment set...
	      %randNum = getRandom();
	      if (%randNum < 0.2)
              {
                    %task.desiredEquipment = "Plasma PlasmaAmmo";
                    %task.buyEquipmentSet = "HeavyEnergySet";
              }
	      else if (%randNum < 0.4)
              {
                    %task.desiredEquipment = "Plasma PlasmaAmmo";
                    %task.buyEquipmentSet = "MediumEnergySet";
              } 
	      else if (%randNum < 0.6)
              {
                    %task.desiredEquipment = "Plasma PlasmaAmmo";
                    %task.buyEquipmentSet = "LightCloakSet";
              }
              else if (%randNum < 0.8)
              {
                    %task.desiredEquipment = "Plasma PlasmaAmmo";
                    %task.buyEquipmentSet = "HeavyShieldSet";
              }
              else
              {
                    %task.desiredEquipment = "Plasma PlasmaAmmo";
                    %task.buyEquipmentSet = "HeavyRepairSet";
              }
   }

   %task.issuedByClient = %objective.issuedByClientId;
   %task.chat = %objective.chat;

   //initialize other task vars
   %task.sendMsg = true;
   %task.sendMsgTime = 0;
   
   %task.engageTarget = -1;

   %task.timeCheck = true;
}

function AIDefensePatrol::assume(%task, %client)
{
   %task.setWeightFreq(15);
   %task.setMonitorFreq(15);
   %client.inPerimeter = false;
   %client.needEquipment = AINeedEquipment(%task.desiredEquipment, %client);

   //even if we don't *need* equipemnt, see if we should buy some... 
   if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
   {
      //see if we could benefit from inventory
      %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      %closestDist = getWord(%result, 1);
      if (AINeedEquipment(%task.desiredEquipment, %client) && %closestInv > 0)
      {
         %result = AIFindClosestEnemy(%client, 200, $AIClientLOSTimeout);
	 %closestEnemy = getWord(%result, 0);
	 %closestEnemydist = getWord(%result, 1);

	 if (%closestEnemy <= 0 || (%closestEnemyDist > %closestDist * 1.5))
            %client.needEquipment = true;
      }
   }

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();

   //set a flag to determine if the objective should be re-aquired when the object is destroyed
   %task.reassignOnDestroyed = false;

   //--------------------------------------------------- assume telporter - start -
   //if the MPB Teleporter is online - Lagg... 9-30-2003

   %task.useTeleport = MPBTeleporterCheck(%client, %task.targetObject.position);
   //--------------------------------------------------- assume telporter - end -
}

function AIDefensePatrol::retire(%task, %client)
{
   %task.engageVehicle = -1;
   %client.setTargetObject(-1);
}

function AIDefensePatrol::weight(%task, %client)
{
   //update the task weight
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;//this will reaccess on destroyed if targetobjectid > 0

   %player = %client.player;
   if (!isObject(%player))
      return;

   %hasMissile = (%player.getInventory("MissileLauncher") > 0) && (%player.getInventory("MissileLauncherAmmo") > 0);

   //if we're defending with a missile launcher, our first priority is to take out vehicles...
   //see if we're already attacking a vehicle...
   if (%task.engageVehicle > 0 && isObject(%task.engageVehicle) && %hasMissile)
   {
      //set the weight
      %task.setWeight(%task.baseWeight);
		return;
   }

   //search for a new vehicle to attack
   %task.engageVehicle = -1;
   %losTimeout = $AIClientMinLOSTime + ($AIClientLOSTimeout * %client.getSkillLevel());
   %result = AIFindClosestEnemyPilot(%client, 300, %losTimeout);
   %pilot = getWord(%result, 0);
   %pilotDist = getWord(%result, 1);

   //if we've got missiles, and a vehicle to attack...
   if (%hasMissile && AIClientIsAlive(%pilot))
   {
      %task.engageVehicle = %pilot.vehicleMounted;
      %client.needEquipment = false;
   }

   //otherwise look for a regular enemy to fight...
   else
   {
      %result = AIFindClosestEnemyToLoc(%client, %task.location, 100, %losTimeout);
      %closestEnemy = getWord(%result, 0);
      %closestdist = getWord(%result, 1);
	   
      //see if we found someone
      if (%closestEnemy > 0)
         %task.engageTarget = %closestEnemy;
      else
      {
         %task.engageTarget = -1;

	 //see if someone is near me...
	 %result = AIFindClosestEnemy(%client, 100, %losTimeout);
		   %closestEnemy = getWord(%result, 0);
		   %closestdist = getWord(%result, 1);
			if (%closestEnemy <= 0 || %closestDist > 70)
				%client.setEngageTarget(-1);
      }
   }

   //set the weight
   %task.setWeight(%task.baseWeight);
}

function AIDefensePatrol::monitor(%task, %client)
{
   //first, buy the equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;
      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
         return;
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(15);
	 %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
         //if this task is the objective task, choose a new objective
	 if (%task == %client.objectiveTask)
	 {
	    AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	 }
	 return;
      }
   }
   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   //chat
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
	 {
	    if (%task.chat !$= "")
	    {
	       %chatMsg = getWord(%task.chat, 0);
	       %chatTemplate = getWord(%task.chat, 1);
	       if (%chatTemplate !$= "")
                  AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
	       else
		  AIMessageThread(%task.chat, %client, -1);
	    }
	    else if (%task.targetObject > 0)
	    {
	       %type = %task.targetObject.getDataBlock().getName();
	       if (%type $= "Flag")
	          AIMessageThreadTemplate("DefendBase", "ChatSelfDefendFlag", %client, -1);
	       else if (%type $= "GeneratorLarge")
		  AIMessageThreadTemplate("DefendBase", "ChatSelfDefendBase", %client, -1);
	       else if (%type $= "StationVehicle")
		  AIMessageThreadTemplate("DefendBase", "ChatSelfDefendVehicle", %client, -1);
	       else if (%type $= "SensorLargePulse")
		  AIMessageThreadTemplate("DefendBase", "ChatSelfDefendSensors", %client, -1);
	       else if (%type $= "SensorMediumPulse")
		  AIMessageThreadTemplate("DefendBase", "ChatSelfDefendSensors", %client, -1);
	       else if (%type $= "TurretBaseLarge")
		  AIMessageThreadTemplate("DefendBase", "ChatSelfDefendTurrets", %client, -1);
	    }
         }
      }
   }

   //--------------------------------------------------- monitor telporter - start -
   //are we using teleporter
   if (%task.useTeleport)
   {
      %result = AIFindDeployedMPB(%client);//new function in aiVehicle.cs
      %closestMPB = getWord(%result, 0);
      %closestMPBDist = getWord(%result, 1);
      %result = SweepForTeleporters(%client);//new function in aiVehicle.cs
      %closestTel = getWord(%result, 0);
      %closestTelDist = getWord(%result, 1);
      if (%closestMPB > 0 && %closestTel > 0)
      {
         //we are done teleporting
         if (%closestMPBDist < %closestTelDist)
         {
            //reset the clients inventory status
            if (%client.needEquipment)
            {
               %result = AIFindClosestInventory(%client, 0);
	           %closestInv = getWord(%result, 0);
               %client.invToUse = %closestInv;
            }
            %task.useTeleport = false;
            return;
         }

         if (%closestTelDist < 2)
            %client.pressJump();
         else
         {
            %client.stepMove(%closestTel.getWorldBoxCenter(), 0.25);
            return;
         }
      }
      else
         %task.useTeleport = false;
   }
   //--------------------------------------------------- monitor telporter - end -       

   //if the defend location task has an object, set the "reset" flag
   if (%task == %client.objectiveTask && isObject(%task.targetObject))
   {
      //if (%task.targetObject.getDamageState() !$= "Destroyed")
      if (%task.targetObject.isDisabled())
         %task.reassignOnDestroyed = true;
      else
      {
         if (%task.reassignOnDestroyed)
         {
            AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	    return;
         }
      }
   }

   //first, check for a vehicle to engage
   if (%task.engageVehicle > 0 && isObject(%task.engageVehicle))
   {
      %client.stop();
      %client.clearStep();
      %client.setEngageTarget(-1);
      %client.setTargetObject(%task.engageVehicle, 300, "Missile");
   }
   else
   {
      //clear the target vehicle...
      %client.setTargetObject(-1);

      //see if we're engaging a player
      if (%client.getEngageTarget() > 0)
      {
         //too far, or killed the enemy - return home
	 if (%client.getStepStatus() !$= "InProgress" || %distance > 100)
	 {
	    %client.setEngageTarget(-1);
	    %client.stepMove(%task.location, 4.0);
	 } 
      }
	   
      //else see if we have a target to begin attacking
      else if (%task.engageTarget > 0)
         %client.stepEngage(%task.engageTarget);
	      
      //else move to a location taken from the T#PatrolPath# simgroup
      else
      {
         %dist = VectorDist(%client.player.getWorldBoxCenter(), %task.location);
	 if (%dist < 6)
	 {
            //random time update task location - Lagg...
            if (%task.timeCheck)
            {
               %task.watTime = getSimTime();
               %task.ranTime = getRandom(0, 20) * 1000;//random time to stepidle at defense points
               %task.timeCheck = false;
            }
            if (getSimTime() > (%task.watTime + %task.ranTime))
            {
               if (Game.class !$= "SiegeGame")
                  %patrolSpots = nameToId("T" @ %client.team @ "PatrolPath" @ %task.path);
               else
               {
                  %T = %game.offenseTeam == 1 ? 1 : 2;
                  %patrolSpots = nameToId("T" @ %T @ "PatrolPath" @ %task.path);
               }
               %count = %patrolSpots.getCount();
               %ranSpot = mFloor(getRandom(0, %count - 1));
               %task.location = %patrolSpots.getObject(%ranSpot).position;
               %task.timeCheck = true;
            }
            //dissolve the human control link and re-evaluate the weight
	    if (%task == %client.objectiveTask)
	    {
	       if (aiHumanHasControl(%task.issuedByClient, %client))
	       {
	          aiReleaseHumanControl(%client.controlByHuman, %client);

		  //should re-evaluate the current objective weight
		  %inventoryStr = AIFindClosestInventories(%client);
		  %client.objectiveWeight = %client.objective.weight(%client, %client.objectiveLevel, 0, %inventoryStr);
               }
            }
	    %client.stepIdle(%task.location);
	 }
	 else
            %client.stepMove(%task.location, 4.0, $AIModeWalk);
      }
   }

   //see if we're supposed to be engaging anyone...
   if (!AIClientIsAlive(%client.getEngageTarget()) && AIClientIsAlive(%client.shouldEngage))
      %client.setEngageTarget(%client.shouldEngage);
}

//----------------------------------------------------------------------------------------------- AIO Deploy Equipment ---
//modified here so bot will continue deploying if has equip and not get bumped
//can be set at a realistic weight now, like 3400               - Lagg... 4-12-2003
//if you want this to be around a deployed MPB, just set the dynamic field "mode = MPB" and he will deploy only
//if a team deployed MPB is within 150 meters of this objectives location.

//If the function ShapeBaseImageData::testInvalidDeployConditions() changes at all, those changes need to be reflected here

function AIODeployEquipment::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   //make sure the deploy objective is valid
   if (%this.isInvalid)
      return 0;

   if (%this.mode $= "MPB")
   {      
      %result = AIFindDeployedMPB(%client);
      %clM = getWord(%result, 0);
      if (!isObject(%clM) || VectorDist(%clM.getWorldBoxCenter(), %this.position) > 150)
         return 0;
   }


   // this checks for CnH and works with the offense/defense tags associated with each AIObjective Marker
   //if offensive the deply is assumed to be camping a destroyed object - Lagg-Alot... 7/18/2009
   if (%this.targetObjectId > 0  && %this.defense > 0)
   {
      if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden() || %this.targetObjectId.team != %client.team)
         return 0;

      //don't defend unless the entire group is powered
      if (%this.targetObjectId.getGroup().powerCount <= 0)
         return 0;
   }
   else if (%this.targetObjectId > 0  && %this.offense > 0)
   {
      if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden() || %this.targetObjectId.team == %client.team)
         return 0;

      //don't camp if target object is not destroyed
      if (%this.targetObjectId.getDamageState() !$= "Destroyed")
         return 0;
   }

   //first, make sure we haven't deployed too many...
   if (%this.equipment $= "TurretOutdoorDeployable" || %this.equipment $= "TurretIndoorDeployable")
      %maxAllowed = countTurretsAllowed(%this.equipment);
   else
      %maxAllowed = $TeamDeployableMax[%this.equipment];

   if ($TeamDeployedCount[%client.team, %this.equipment] >= %maxAllowed)
      return 0;

   //if badly damaged and we have a repair pack, don't waste time
   %damage = %client.player.getDamagePercent();
   if (%client.player.getInventory("RepairPack") > 0 && %damage > 0.3)
      return 0;

   //now make sure there are no other items in the way...
   InitContainerRadiusSearch(%this.location, $MinDeployableDistance, $TypeMasks::VehicleObjectType |
  	                                             $TypeMasks::MoveableObjectType |
  	                                             $TypeMasks::StaticShapeObjectType |
  	                                             $TypeMasks::TSStaticShapeObjectType | 
  	                                             $TypeMasks::ForceFieldObjectType |
  	                                             $TypeMasks::ItemObjectType | 
  	                                             $TypeMasks::PlayerObjectType | 
  	                                             $TypeMasks::TurretObjectType);
      %objSearch = containerSearchNext();

      //make sure we're not invalidating the deploy location with the client's own player object
      if (%objSearch == %client.player)
         %objSearch = containerSearchNext();

      //did we find an object which would block deploying the equipment?
      if (isObject(%objSearch))
         return 0;

   //now run individual checks based on the equipment type...
   if (%this.equipment $= "TurretIndoorDeployable")
   {
      //check if there's another turret close to the deploy location
      InitContainerRadiusSearch(%this.location, $TurretIndoorSpaceRadius, $TypeMasks::StaticShapeObjectType);
      %found = containerSearchNext();
      if (isObject(%found))
      {
         %foundName = %found.getDataBlock().getName();
 	 if ((%foundName $= TurretDeployedFloorIndoor) || (%foundName $= "TurretDeployedWallIndoor") ||
           (%foundName $= "TurretDeployedCeilingIndoor") || (%foundName $= "TurretDeployedOutdoor"))
 	    return 0;
      }
 
      //now see if there are too many turrets in the area...
      %highestDensity = 0;
      InitContainerRadiusSearch(%this.location, $TurretIndoorSphereRadius, $TypeMasks::StaticShapeObjectType);
      %found = containerSearchNext();
      while (isObject(%found))
      {
         %foundName = %found.getDataBlock().getName();
 	 if ((%foundName $= "TurretDeployedFloorIndoor") || (%foundName $= "TurretDeployedWallIndoor") ||
           (%foundName $= "TurretDeployedCeilingIndoor") || (%foundName $= "TurretDeployedOutdoor"))
 	 {
 	    //found one
 	    %numTurretsNearby++;//+++++++++++++++++++++++++++++++++++++++++ ahh review this - Lagg... +++
            %nearbyDensity = testNearbyDensity(%found, $TurretIndoorSphereRadius);
 	    if (%nearbyDensity > %highestDensity)
 	       %highestDensity = %nearbyDensity;
         }
 	 %found = containerSearchNext();
      }
 
      if (%numTurretsNearby > %highestDensity)
         %highestDensity = %numTurretsNearby;
 
      //now see if the area is already saturated
      if (%highestDensity > $TurretIndoorMaxPerSphere)
         return 0;
   }
   //--------------------left off here - Lagg...+++++++++++++++++++++++++
 
 	else if (%this.equipment $= "TurretOutdoorDeployable")
 	{
 		//check if there's another turret close to the deploy location
 	   InitContainerRadiusSearch(%this.location, $TurretOutdoorSpaceRadius, $TypeMasks::StaticShapeObjectType);
 	   %found = containerSearchNext();
 	   if (isObject(%found))
 		{
 			%foundName = %found.getDataBlock().getName();     
 			if ((%foundName $= "TurretDeployedFloorIndoor") || (%foundName $= "TurretDeployedWallIndoor") || (%foundName $= "TurretDeployedCeilingIndoor") || (%foundName $= "TurretDeployedOutdoor"))
 				return 0;
 		}
 
 		//now see if there are too many turrets in the area...
 	   %highestDensity = 0;
 	   InitContainerRadiusSearch(%this.location, $TurretOutdoorSphereRadius, $TypeMasks::StaticShapeObjectType);
 	   %found = containerSearchNext();
 	   while (isObject(%found))
 	   {
 	      %foundName = %found.getDataBlock().getName();
 	      if ((%foundName $= "TurretDeployedFloorIndoor") || (%foundName $= "TurretDeployedWallIndoor") || (%foundName $= "TurretDeployedCeilingIndoor") || (%foundName $= "TurretDeployedOutdoor"))
 	      {
 				//found one
 				%numTurretsNearby++;
 
 				%nearbyDensity = testNearbyDensity(%found, $TurretOutdoorSphereRadius);
 				if (%nearbyDensity > %highestDensity)
 					%highestDensity = %nearbyDensity;     
 	      }
 	     %found = containerSearchNext();
 	   }
 
 		if (%numTurretsNearby > %highestDensity)
 			%highestDensity = %numTurretsNearby;
 
 		//now see if the area is already saturated
 		if (%highestDensity > $TurretOutdoorMaxPerSphere)
 			return 0;
 	}

	//check equipment requirement
	%needEquipment = AINeedEquipment(%this.equipment, %client);

        //if don't need equipment, see if we've past the "point of no return", and should continue regardless
 	if (!%needEquipment && %this.clientLevel1 == %client)
 	{
                
 		
 		//if we're too far from the inv to go back, or we're too close to the deploy location, force continue
 		////if (%closestDist > 50 && VectorDist(%client.player.getWorldBoxCenter(), %task.location) < 50) - changed - Lagg...
                if (%closestDist > 25)
 		{
 			%weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
 			if (%weight < $AIWeightContinueDeploying)
 				%weight = $AIWeightContinueDeploying;

 			return %weight;
 		}
 	}

	//if this bot is linked to a human who has issued this command, up the weight
	if (%this.issuedByClientId == %client.controlByHuman)
	{
		//make sure we have the potential to reach the minWeight
		if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
		{
			if ($AIWeightHumanIssuedCommand < %minWeight)
				return 0;
			else
				%weight = $AIWeightHumanIssuedCommand;
		}
		else
		{
			// calculate the default...
			%weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
			if (%weight < $AIWeightHumanIssuedCommand)
				%weight = $AIWeightHumanIssuedCommand;
		}
	}
	else
	{
		//make sure we have the potential to reach the minWeight
		if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
			return 0;

		// calculate the default...
		%weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
	}

        if (!%needEquipment)
           %weight += 400;

	return %weight;
}

function AIODeployEquipment::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIDeployEquipment);
   %task = %client.objectiveTask;
   %task.initFromObjective(%this, %client);
}

function AIODeployEquipment::unassignClient(%this, %client)
{
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//---------------------------------------------------------------------------------------------- AI Deploy Equipment ---

function AIDeployEquipment::initFromObjective(%task, %objective, %client)
{
	//initialize the task vars from the objective
   %task.baseWeight = %client.objectiveWeight;
   %task.location = %objective.location;
   %task.position = %objective.position;
   %task.equipment = %objective.equipment;
	%task.buyEquipmentSet = %objective.buyEquipmentSet;
	%task.desiredEquipment = %objective.desiredEquipment;
	%task.issuedByClient = %objective.issuedByClientId;
	%task.chat = %objective.chat;
        %task.mode = %objective.mode;

	//initialize other task vars
	%task.sendMsg = true;
	%task.sendMsgTime = 0;
        %task.useTeleport = false;

	//use the Y-axis of the rotation as the desired direction of deployement,
	//and calculate a walk to point 3 m behind the deploy point. 
	%task.deployDirection = MatrixMulVector("0 0 0 " @ getWords(%objective.getTransform(), 3, 6), "0 1 0");
	%task.deployDirection = VectorNormalize(%task.deployDirection);
}

function AIDeployEquipment::assume(%task, %client)
{
   %task.setWeightFreq(15);
   %task.setMonitorFreq(15);
	
   %client.needEquipment = AINeedEquipment(%task.equipment, %client);
   
   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();

   %task.passes = 0;
   %task.deployAttempts = 0;
   %task.checkObstructed = false;
   %task.waitMove = 0;

   //add some for picking up deployables from ground - Laggg... 2-26-2004
   %task.pickupPack = -1;
   %task.usingInv = false;

   //--------------------------------------------------- assume telporter - start -
   //if the MPB Teleporter is online - Lagg... 9-30-2003

   //first check for deployed MPB
   %result = AIFindDeployedMPB(%client);//new function in aiVehicle.cs
   %closestMPB = getWord(%result, 0);
   %closestMPBDist = getWord(%result, 1);
      
   //next find the teleporter
   %result = SweepForTeleporters(%client);//new function in aiVehicle.cs
   %closestTel = getWord(%result, 0);
   %closestTelDist = getWord(%result, 1);
      
   //now check for what would be closer - Lagg... 10-1-2003
   if (%closestMPB > 0 && %closestTel > 0)
   {
      %disToTarg = %client.getPathDistance(%task.location);
      %mpbDisToTarg = vectorDist(%closestMPB.getTransform(), %task.location);
         
      if (%closestTelDist < %closestMPBDist && %closestMPBDist < %disToTarg && %mpbDisToTarg < %disToTarg)
         %task.useTeleport = true;//new - Lagg... 10-1-2003
      else
         %task.useTeleport = false;             
   }
   //--------------------------------------------------- assume telporter - end -
}

function AIDeployEquipment::retire(%task, %client)
{
}

function AIDeployEquipment::weight(%task, %client)
{
	//update the task weight
	if (%task == %client.objectiveTask)
		%task.baseWeight = %client.objectiveWeight;

   %task.setWeight(%task.baseWeight);
}

function findTurretDeployPoint(%client, %location, %attempt)
{
   %player = %client.player;
	if (!isObject(%player))
		return "0 0 0";

	%feetPos = posFromTransform(%player.getTransform());
	%temp = VectorSub(%location, %feetPos);
   %temp2 = getWord(%temp, 0) @ " " @ getWord(%temp, 1) @ " 0";
	%facingVector = VectorNormalize(%temp2);
	%aimPoint = VectorAdd(%feetPos, %facingVector);
	//assume that there will be 10 attempts
	%height = getWord(%location, 2) + 1.0 - (0.2 * %attempt);
   %aimAt = getWord(%aimPoint, 0) @ " " @ getWord(%aimPoint, 1) @ " " @ %height;
   return %aimAt;
}

//modified range to deploy to make easier for outdoor inventory deployments - Lagg... - 4-12-2003
function AIDeployEquipment::monitor(%task, %client)
{
   //first, make sure we haven't deployed too many...
   if (%task.equipment $= "TurretOutdoorDeployable" || %task.equipment $= "TurretIndoorDeployable")
      %maxAllowed = countTurretsAllowed(%task.equipment);
   else
      %maxAllowed = $TeamDeployableMax[%task.equipment];

   if ($TeamDeployedCount[%client.team, %task.equipment] >= %maxAllowed)
   {
      //if this task is the objective task, choose a new objective
      if (%task == %client.objectiveTask)
      {
         AIUnassignClient(%client);
	 Game.AIChooseGameObjective(%client);
         return;
      }
   }

   //make sure this was not deployed already
   InitContainerRadiusSearch(%task.location, $MinDeployableDistance, $TypeMasks::StaticShapeObjectType);
   %found = containerSearchNext();
   if (isObject(%found))
   {
      if (%found.team == %client.team)
      {
         if (%task == %client.objectiveTask)
	 {
            //error("AIDeployEquipment::monitor - this objective was completed/deployed already :(");
            AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	    return;
	 }
      }
   }

   //first, see if we still need the deployable pack - Lagg... 2-25-2004
   %itemType = getWord(%task.equipment, 0);
   if (%client.player.getInventory(%itemType) > 0)
   {
      //error("AIDeployEquipment - we have Item Type = " @ %itemType SPC getTaggedString(%client.name));
      %client.needEquipment = false;
      %task.setMonitorFreq(15);
   }

   //then, buy the equipment or find it if you can :) - Lagg... 2-25-2004
   else if (%client.needEquipment)
   {
      // check to see if there's a deployable pack nearby
      %closestItem = -1;
      %closestItemDist = 32767;
      %itemType = getWord(%task.equipment, 0);

      //search the AIItemSet for a deployable pack (someone might have dropped one...)
      %armor = %client.player.getArmorSize();
      %itemCount = $AIItemSet.getCount();
      for (%i = 0; %i < %itemCount; %i++)
      {
         %item = $AIItemSet.getObject(%i);
         if (%item.getDataBlock().getName() $= %itemType && !%item.isHidden())
	 {
            %cant = false;
            if ((%itemType $= "InventoryDeployable" || %itemType $= "TurretOutdoorDeployable" || %itemType $= "TurretIndoorDeployable") && %armor $= "Light")
            {
               //error("AIDeployEquipment - we found on floor but LIGHT ARMOR = " @ %itemType SPC getTaggedString(%client.name));
               %cant = true;
            }
            %dist = %client.getPathDistance(%item.getWorldBoxCenter());
	    if (%dist > 0 && %dist < %closestItemDist && !%cant)
	    {
               %closestItem = %item;
	       %closestItemDist = %dist;
	    }
         }
      }

      //choose whether we're picking up the closest pack, or buying from an inv station...
      if ((isObject(%closestItem) && %closestItem != %task.pickupPack) || (%task.buyInvTime != %client.buyInvTime))
      {
         %task.pickupPack = %closestItem;

	 //initialize the inv buying
	 %task.buyInvTime = getSimTime();
	 AIBuyInventory(%client, %task.equipment, %task.buyEquipmentSet, %task.buyInvTime);

	 //now decide which is closer
	 if (isObject(%closestItem))
	 {
            //added a little here to check for power - Lagg... - 3-25-2003
	    if (isObject(%client.invToUse) && (%client.invToUse.isEnabled()) && (%client.invToUse.isPowered()))
	    {
               %dist = %client.getPathDistance(%client.invToUse.getWorldBoxCenter());
	       if (%dist < %closestItemDist)
	          %task.usingInv = true;
	       else
		  %task.usingInv = false;
            }
	    else
               %task.usingInv = false;
	 }
	 else
	    %task.usingInv = true;
      }

      //now see if we found a closer deployable pack
      if (!%task.usingInv && isObject(%task.pickupPack))
      {
         %client.stepMove(%task.pickupPack.position, 0.25);
         %distToPack = %client.getPathDistance(%task.pickupPack.position);
         if (%distToPack < 10 && %client.player.getMountedImage($BackpackSlot) > 0)
	    %client.player.throwPack();

         //error("AIDeployEquipment - Picking Up Deployable Pack = " @ %itemType SPC getTaggedString(%client.name));

	 //and we're finished until we actually have the new pack... we should - Lagg...
	 return;
      }
      else
      {
         //error("AIDeployEquipment - we are using inventory Item Type = " @ %itemType SPC getTaggedString(%client.name));
         %task.setMonitorFreq(5);
         %result = AIBuyInventory(%client, %task.equipment, %task.buyEquipmentSet, %task.buyInvTime);
	 if (%result $= "InProgress")
            return;
	 else if (%result $= "Finished")
	 {
	    %task.setMonitorFreq(30);
            %client.needEquipment = false;

            //if we made it past the inventory buying, reset the inv time
	    %task.buyInvTime = getSimTime();
         }
	 else if (%result $= "Failed")
	 {
            //if this task is the objective task, choose a new objective
	    if (%task == %client.objectiveTask)
            {
	       AIUnassignClient(%client);
	       Game.AIChooseGameObjective(%client);
	    }
	    return;
	 }
      }
   }

   //--------------------------------------------------- monitor telporter - start -
   //are we using teleporter
   if (%task.useTeleport)
   {
      %result = AIFindDeployedMPB(%client);//new function in aiVehicle.cs
      %closestMPB = getWord(%result, 0);
      %closestMPBDist = getWord(%result, 1);
      %result = SweepForTeleporters(%client);//new function in aiVehicle.cs
      %closestTel = getWord(%result, 0);
      %closestTelDist = getWord(%result, 1);
      if (%closestMPB > 0 && %closestTel > 0)
      {
         //we are done teleporting
         if (%closestMPBDist < %closestTelDist)
         {
            //reset the clients inventory status
            if (%client.needEquipment)
            {
               %result = AIFindClosestInventory(%client, 0);
	       %closestInv = getWord(%result, 0);
               %client.invToUse = %closestInv;
            }
            %task.useTeleport = false;
            return;
         }

         if (%closestTelDist < 2)
            %client.pressJump();
         else
         {
            %client.stepMove(%closestTel.getWorldBoxCenter(), 0.25);
            return;
         }
      }
      else
         %task.useTeleport = false;
   }
   //--------------------------------------------------- monitor telporter - end -

   //make sure we still have equipment
		%client.needEquipment = AINeedEquipment(%task.equipment, %client);
      if (%client.needEquipment)
      {
         //if this task is the objective task, choose a new objective
         if (%task == %client.objectiveTask)
			{
            AIUnassignClient(%client);
	         Game.AIChooseGameObjective(%client);
				return;
			}
      }

	//chat
	if (%task.sendMsg)
	{
		if (%task.sendMsgTime == 0)
			%task.sendMsgTime = getSimTime();
		else if (getSimTime() - %task.sendMsgTime > 7000)
		{
			%task.sendMsg = false;
		   if (%client.isAIControlled())
			{
				if (%task.chat !$= "")
				{
					%chatMsg = getWord(%task.chat, 0);
					%chatTemplate = getWord(%task.chat, 1);
					if (%chatTemplate !$= "")
						AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
					else
						AIMessageThread(%task.chat, %client, -1);
				}
			}
		}
	}

   //see if we're supposed to be engaging anyone...
   if (AIClientIsAlive(%client.shouldEngage))
   {
	   %hasLOS = %client.hasLOSToClient(%client.shouldEngage);
	   %losTime = %client.getClientLOSTime(%client.shouldEngage);
	   if (%hasLOS || %losTime < 1000)
         %client.setEngageTarget(%client.shouldEngage);
      else
         %client.setEngageTarget(-1);
   }
   else
      %client.setEngageTarget(-1);

	//calculate the deployFromLocation
	%factor = -1 * (4 - (%task.passes * 0.5));//changed to 4m behind to improve proformance --- Lagg... 3-15-2003
   %task.deployFromLocation = VectorAdd(%task.location,VectorScale(%task.deployDirection, %factor));

	//see if we're within range of the deploy location
   %clLoc = %client.player.position;
   %distance = VectorDist(%clLoc, %task.deployFromLocation);
	%dist2D = VectorDist(%client.player.position, getWords(%task.deployFromLocation, 0, 1) SPC getWord(%client.player.position, 2));

	//set the aim when we get near the target...  this will be overwritten when we're actually trying to deploy
	if (%distance < 10 && %dist2D < 10)
      %client.aimAt(%task.location, 1000);

   if ((%client.pathDistRemaining(20) > %distance + 0.25) || %dist2D > 0.5)
	{
		%task.deployAttempts = 0;
		%task.checkObstructed = false;
		%task.waitMove = 0;
      %client.stepMove(%task.deployFromLocation, 0.25, $AIModeExpress);
	   %task.setMonitorFreq(15);
		return;
	}
   
	if (%task.deployAttempts < 10 && %task.passes < 5 && !AIClientIsAlive(%client.getEngageTarget()))
	{
		//dissolve the human control link
		if (%task == %client.objectiveTask)
			aiReleaseHumanControl(%client.controlByHuman, %client);

	   %task.setMonitorFreq(3);
      %client.stop();
		if (%task.deployAttempts == 0)
			%deployPoint = %task.location;
		else
	      %deployPoint = findTurretDeployPoint(%client, %task.location, %task.deployAttempts);
      if(%deployPoint !$= "")
      {
         // we have possible point
         %task.deployAttempts++;
         %client.aimAt(%deployPoint, 2000);

			//try to deploy the backpack
			%client.deployPack = true;
         %client.lastDeployedObject = -1;
         %client.player.use(Backpack);
         
         // check if pack deployed
         if (isObject(%client.lastDeployedObject))
			{
				//see if there's a "repairObject" objective for the newly deployed thingy...
				if (%task == %client.objectiveTask)
				{
					%deployedObject = %client.lastDeployedObject;

					//search the current objective group and search for a "repair Object" task...
					%objective = %client.objective;

					//delete any previously associated "AIORepairObject" objective
					if (isObject(%objective.repairObjective))
					{
						AIClearObjective(%objective.repairObjective);
						%objective.repairObjective.delete();
						%objective.repairObjective = "";
                                                clearObjectiveFromTable(%objective.repairObjective);//new function below to stop objective spam - Lagg... 1-27-2004
					}

					//add the repair objective
	            %objective.repairObjective = new AIObjective(AIORepairObject)
		                              {
											      dataBlock = "AIObjectiveMarker";
		                                 weightLevel1 = %objective.weightLevel1 - 50;
		                                 weightLevel2 = 0;
		                                 description = "Repair the " @ %deployedObject.getDataBlock().getName();
                                                 mode = %task.mode;
													targetObjectId = %deployedObject;
													issuedByClientId = %client;
		                                 offense = false;
													defense = true;
		                                 equipment = "RepairPack";
		                              };
					%objective.repairObjective.deployed = true;
					%objective.repairObjective.setTransform(%objective.getTransform());
					%objective.repairObjective.group = %objective.group;
                                        MissionCleanup.add(%objective.repairObjective);

   //if is siege game we have to do this right - Lagg... 11-3-2003
   if (Game.class $= "SiegeGame")
   {
      //the objectives in $ObjectiveQ[1] are all offense objectives, $ObjectiveQ[2] has the defensive ones..
      if (%client.team == game.offenseTeam)
         $ObjectiveQ[1].add(%objective.repairObjective);
      else
         $ObjectiveQ[2].add(%objective.repairObjective);
   }
   else
      $ObjectiveQ[%client.team].add(%objective.repairObjective);


                              //changed to fix Siege gameType wrong Q error - Lagg... 11-5-2003
			      //MissionCleanup.add(%objective.repairObjective);
	            //$ObjectiveQ[%client.team].add(%objective.repairObjective);

					//finally, unassign the client so he'll go do something else...
			      AIUnassignClient(%client);
					Game.AIChooseGameObjective(%client);
				}

				//finished
				return;
			}
      }
	}
	else if (!%task.checkObstructed)
	{
		%task.checkObstructed = true;

	   //see if anything is in our way
	   InitContainerRadiusSearch(%task.location, 4, $TypeMasks::MoveableObjectType | $TypeMasks::VehicleObjectType |
																					                      $TypeMasks::PlayerObjectType);
	   %objSrch = containerSearchNext();
		if (%objSrch == %client.player)
		   %objSrch = containerSearchNext();
	   if (%objSrch)
			AIMessageThread("ChatMove", %client, -1);
	}
	else if (%task.waitMove < 5 && %task.passes < 5)
	{
		%task.waitMove++;

		//try another pass at deploying 
		if (%task.waitMove == 5)
		{
			%task.waitMove = 0;
			%task.passes++;
			%task.deployAttempts = 0;

			//see if we're *right* underneath the deploy point
			%deployDist2D = VectorDist(getWords(%client.player.position, 0, 1) @ "0", getWords(%task.location, 0, 1) @ "0");
			if (%deployDist2D < 0.25)
			{
				%client.pressjump();
                                %client.pressJet();
				%client.deployPack = true;
	         %client.player.use(Backpack);

	         // check if pack deployed
	         if(%client.player.getMountedImage($BackpackSlot) == 0)
				{
					//don't add a "repairObject" objective for ceiling turrets
					if (%task == %client.objectiveTask)
					{
						AIUnassignClient(%client);
						Game.AIChooseGameObjective(%client);
					}
				}
			}
		}
	}
	else
	{
		//find a new assignment - and remove this one from the Queue
      if (%task == %client.objectiveTask)
		{
			error(getTaggedString(%client.name) SPC ", from team" SPC %client.team SPC ", is invalidating objective:" SPC %client.objectiveTask.equipment SPC ", UNABLE TO DEPLOY EQUIPMENT");
                        //error("client = " @ getTaggedString(%client.name));
			%client.objective.isInvalid = true;
	      AIUnassignClient(%client);
			Game.AIChooseGameObjective(%client);
		}
	}
}

//----------------------------------------------------------------------------------------------- AIO Destroy Object --- (Last Update 3-30-2004)
//place objective marker in spot you want bot to stand and destroy for sentry turrets and hard to
//reach spots. basically same as attackobject.                                                - Lagg... 3-26-2003

function AIODestroyObject::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   // if were playing CnH, check who owns this
   if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden() || %this.targetObjectId.team == %client.team)
      return 0;

   //don't attack team 0 stuffs
   if (%this.targetObjectId.team <= 0)
      return 0;      
   
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;
      
   //no need to attack if the object is already destroyed
   if (!isObject(%this.targetObjectId) || %this.targetObjectId.getDamageState() $= "Destroyed")
      return 0;
   else
   {
      //if this bot is linked to a human who has issued this command, up the weight
		if (%this.issuedByClientId == %client.controlByHuman)
		{
			//make sure we have the potential to reach the minWeight
			if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
			{
				if ($AIWeightHumanIssuedCommand < %minWeight)
					return 0;
				else
					%weight = $AIWeightHumanIssuedCommand;
			}
			else
			{
				// calculate the default...
				%weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
				if (%weight < $AIWeightHumanIssuedCommand)
					%weight = $AIWeightHumanIssuedCommand;
			}
		}
		else
		{
			//make sure we have the potential to reach the minWeight
			if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
				return 0;

			// calculate the default...
                        %this.desiredEquipment = "Plasma PlasmaAmmo";
			%weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
		}

      //if we are playing DnD, check if objective was completed already, if not give some extra :) - Lagg... 3-29-2004
      if (Game.class $= "DnDGame")
      {
         if (%this.targetObjectId.objectiveCompleted)
            %weight -= 400;
         else
            %weight += 200;
      }

      return %weight;
   }
}

function AIODestroyObject::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIdestroyObject);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIODestroyObject::unassignClient(%this, %client)
{
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//------------------------------------------------------------------------------------------------ AI Destroy Object ---
function AIDestroyObject::initFromObjective(%task, %objective, %client)
{
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;
   %task.equipment = %objective.equipment;
   %task.buyEquipmentSet = %objective.buyEquipmentSet;
   %task.desiredEquipment = %objective.desiredEquipment;
   %task.issuedByClient = %objective.issuedByClientId;
   %task.location = %objective.position;

   //initialize other task vars
   %task.sendMsg = true;
   %task.sendMsgTime = 0;
   %task.useTeleport = false;
}

function AIDestroyObject::assume(%task, %client)
{
   %task.setWeightFreq(15);
   %task.setMonitorFreq(15);

   //added here so Bot buys a random set if no set was specified in objective dataBlock - Lagg... 2-14-2003
      if (%task.buyEquipmentSet $= "randomSet")
      {
         //pick a random equipment set...
         %randNum = getRandom();
         if (%randNum < 0.2)
         {
            %task.desiredEquipment = "Plasma PlasmaAmmo";
            %task.buyEquipmentSet = "HeavyEnergySet";
         }
         else if (%randNum < 0.4)
         {
            %task.desiredEquipment = "Plasma PlasmaAmmo";
            %task.buyEquipmentSet = "MediumEnergySet";
         } 
         else if (%randNum < 0.6)
         {
            %task.desiredEquipment = "Plasma PlasmaAmmo";
            %task.buyEquipmentSet = "LightCloakSet";
         }
         else if (%randNum < 0.8)
         {
            %task.desiredEquipment = "Plasma PlasmaAmmo";
            %task.buyEquipmentSet = "HeavyShieldOff";
         }
         else
         {
            %task.desiredEquipment = "Plasma PlasmaAmmo";
            %task.buyEquipmentSet = "MediumMissileSet";
         }
      }

   if (%task.equipment !$= "")
      %equipmentList = %task.equipment;
   else
      %equipmentList = %task.desiredEquipment;
   %client.needEquipment = AINeedEquipment(%equipmentList, %client);


   //even if we don't *need* equipemnt, see if we should buy some... 
   if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
   {
      //see if we could benefit from inventory
      %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      %closestDist = getWord(%result, 1);
      if (AINeedEquipment(%task.desiredEquipment, %client) && %closestInv > 0)
      {
         //find where we are
         %clientPos = %client.player.getWorldBoxCenter();
         %objPos = %task.targetObject.getWorldBoxCenter();
         %distToObject = %client.getPathDistance(%objPos);
			
	 if (%distToObject < 0 || %closestDist < 50)// 50m to inventory in this case
         {
            //error("AIDestroyObject::assume - ! %needequipment but should buy some " @ gettaggedString(%client.name));
	    %client.needEquipment = true;
         }
      }
   }

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();

   //--------------------------------------------------- assume telporter - start -
   //if the MPB Teleporter is online - Lagg... 9-30-2003

   //first check for deployed MPB
   %result = AIFindDeployedMPB(%client);//new function in aiVehicle.cs
   %closestMPB = getWord(%result, 0);
   %closestMPBDist = getWord(%result, 1);
      
   //next find the teleporter
   %result = SweepForTeleporters(%client);//new function in aiVehicle.cs
   %closestTel = getWord(%result, 0);
   %closestTelDist = getWord(%result, 1);
      
   //now check for what would be closer - Lagg... 10-1-2003
   if (%closestMPB > 0 && %closestTel > 0)
   {
      %disToTarg = %client.getPathDistance(%task.targetObject.getTransform());
      %mpbDisToTarg = vectorDist(%closestMPB.getTransform(), %task.targetObject.getTransform());
         
      if (%closestTelDist < %closestMPBDist && %closestMPBDist < %disToTarg && %mpbDisToTarg < %disToTarg)
         %task.useTeleport = true;//new - Lagg... 10-1-2003
      else
         %task.useTeleport = false;             
   }
   //--------------------------------------------------- assume telporter - end -
}

function AIDestroyObject::retire(%task, %client)
{
   %client.setTargetObject(-1);
}

function AIDestroyObject::weight(%task, %client)
{
   //update the task weight
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;

   //let the monitor decide when to stop attacking
   %task.setWeight(%task.baseWeight);
}

function AIDestroyObject::monitor(%task, %client)
{
   //--------------------------------------------------- monitor telporter - start -
   //are we using teleporter
   if (%task.useTeleport)
   {
      %result = AIFindDeployedMPB(%client);//new function in aiVehicle.cs
      %closestMPB = getWord(%result, 0);
      %closestMPBDist = getWord(%result, 1);
      %result = SweepForTeleporters(%client);//new function in aiVehicle.cs
      %closestTel = getWord(%result, 0);
      %closestTelDist = getWord(%result, 1);
      if (%closestMPB > 0 && %closestTel > 0)
      {
         //we are done teleporting
         if (%closestMPBDist < %closestTelDist)
         {
            //reset the clients inventory status
            if (%client.needEquipment)
            {
               %result = AIFindClosestInventory(%client, 0);
	       %closestInv = getWord(%result, 0);
               %client.invToUse = %closestInv;
            }
            %task.useTeleport = false;
            return;
         }

         if (%closestTelDist < 2)
            %client.pressJump();
         else
         {
            %client.stepMove(%closestTel.getWorldBoxCenter(), 0.25);
            return;
         }
      }
      else
         %task.useTeleport = false;
   }
   //--------------------------------------------------- monitor telporter - end -

   //first, buy the equipment - if we really really need some :) - Lagg... - 4-6-2003
   %hasMortar = (%client.player.getInventory("Mortar") > 0) && (%client.player.getInventory("MortarAmmo") > 0);
   %hasPlasma = (%client.player.getInventory("Plasma") > 0) && (%client.player.getInventory("PlasmaAmmo") > 0);
   %hasDisc = (%client.player.getInventory("Disc") > 0) && (%client.player.getInventory("DiscAmmo") > 0) && (!%client.player.getInventory("Blaster") > 0);
   if (%hasMortar || %hasPlasma || %hasDisc)
      %client.needEquipment = false;

   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
			%equipmentList = %task.equipment;
		else
			%equipmentList = %task.desiredEquipment;
      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
         return;
      else if (%result $= "Finished")
		{
		   %task.setMonitorFreq(15);
			%client.needEquipment = false;
		}
		else if (%result $= "Failed")
		{
	      //if this task is the objective task, choose a new objective
	      if (%task == %client.objectiveTask)
	      {
	         AIUnassignClient(%client);
	         Game.AIChooseGameObjective(%client);
	      }
			return;
		}
   }
	//if we made it past the inventory buying, reset the inv time
	%task.buyInvTime = getSimTime();
   
	//chat
	if (%task.sendMsg)
	{
		if (%task.sendMsgTime == 0)
			%task.sendMsgTime = getSimTime();
		else if (getSimTime() - %task.sendMsgTime > 7000)
		{
			%task.sendMsg = false;
		   if (%client.isAIControlled())
			{
				if (%task.chat !$= "")
				{
					%chatMsg = getWord(%task.chat, 0);
					%chatTemplate = getWord(%task.chat, 1);
					if (%chatTemplate !$= "")
						AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
					else
						AIMessageThread(%task.chat, %client, -1);
				}
				else if (%task.targetObject > 0)
				{
					%type = %task.targetObject.getDataBlock().getName();
					if (%type $= "GeneratorLarge")
						AIMessageThreadTemplate("AttackBase", "ChatSelfAttackGenerator", %client, -1);
					else if (%type $= "SensorLargePulse")
						AIMessageThreadTemplate("AttackBase", "ChatSelfAttackSensors", %client, -1);
					else if (%type $= "SensorMediumPulse")
						AIMessageThreadTemplate("AttackBase", "ChatSelfAttackSensors", %client, -1);
					else if (%type $= "TurretBaseLarge")
						AIMessageThreadTemplate("AttackBase", "ChatSelfAttackTurrets", %client, -1);
					else if (%type $= "StationVehicle")
						AIMessageThreadTemplate("AttackBase", "ChatSelfAttackVehicle", %client, -1);
				}
			}
		}
	}

   //set the target object
   %outOfStuff = (AIAttackOutofAmmo(%client));
   if (isObject(%task.targetObject) && %task.targetObject.getDamageState() !$= "Destroyed" && !%outOfStuff)
   {
      %close = false;
      if (VectorDist(%task.targetObject.position, %client.player.position) < 4)
         %close = true;

      %type = %task.targetObject.getDataBlock().getName();
      if (%type $= "StationVehicle" || %type $= "MPBTeleporter")
      {

         %targetLOS = "false";
         %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::ForceFieldObjectType;
         //%targetLOS = !containerRayCast(%client.player.getWorldBoxCenter(), %task.targetObject.getWorldBoxCenter(), %mask, 0);
         %targetLOS = !containerRayCast(%client.player.getMuzzlePoint($WeaponSlot), %task.targetObject.getWorldBoxCenter(), %mask, 0);

         //use the Y-axis of the rotation as the desired direction of approach,
         //and calculate a walk to point 1m in front of the trigger point - Lagg...
 
         %aprchDirection = MatrixMulVector("0 0 0 " @ getWords(%task.targetObject.getTransform(), 3, 6), "0 1 0");
         %aprchDirection = VectorNormalize(%aprchDirection);
         %factor = 10; //10m in front of station vehicle is the right spot to stand                        
         //%aprchFromLocation = VectorAdd(%task.targetObject.position,VectorScale(%aprchDirection, %factor));
         %task.location = VectorAdd(%task.targetObject.position,VectorScale(%aprchDirection, %factor));
         %client.stepMove(%task.location);

         if (VectorDist(%task.targetObject.position, %client.player.position) < 15 && %targetLOS)
            %client.setDangerLocation(%task.targetObject.position, 20);
      }
      //in case lazy people don't want to move aiobjective marker
      else if (VectorDist(%task.targetObject.getWorldBoxCenter(), %task.location) < 4)
      {
         if (! %client.targetInRange())
            %client.stepMove(%task.targetObject.getWorldBoxCenter(), 8.0);
      }
      else
         %client.stepMove(%task.location);

      %targetLOS = "false";
      %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::ForceFieldObjectType;
      //%targetLOS = !containerRayCast(%client.player.getWorldBoxCenter(), %task.targetObject.getWorldBoxCenter(), %mask, 0);
      %targetLOS = !containerRayCast(%client.player.getMuzzlePoint($WeaponSlot), %task.targetObject.getWorldBoxCenter(), %mask, 0);
      if (%targetLOS && !%close)
         %client.setTargetObject(%task.targetObject, 40, "Destroy");
      else
          %client.setTargetObject(-1);
   }
   else
   {
      %client.setTargetObject(-1);

      //aim at the floor if we need to repair self
      if (%client.player.getDamagePercent() > 0 && %client.player.getInventory(RepairPack) > 0)
      {
         %pos = %client.player.getWorldBoxCenter();
         %posX = getWord(%pos, 0);
         %posY = getWord(%pos, 1);
         %posZ = getWord(%pos, 2) - 1;
         %pos = %posX SPC %posY SPC %posZ;
         %client.aimAt(%pos, 500);
      }

      
      //if this task is the objective task, choose a new objective
      if (%task == %client.objectiveTask)
      {
         AIUnassignClient(%client);
         Game.AIChooseGameObjective(%client);
      }
   }
}

//----------------------------------------------------------------------------------------------- AIO Escort Player ---

//modified escorter armor type allowed, rewritten all aivehicle escort stuff, included vehicleobjecttypes
//as potential targets if lazed by escortie.                                              - Lagg... - 4-12-2003

function AIOEscortPlayer::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client) || ! AIClientIsAlive(%this.targetClientId))
      return 0;

   //can't escort yourself
   if (%client == %this.targetClientId)
      return 0;

   if (%client.player.holdingFlag)
      return 0;

   //if vehicle mounted escort check for empty seats - Lagg... - 5-17-2003
   %targMounted = %this.targetClientId.player.ismounted();
   if (%targMounted > 0)
   {
      //%this.forceClientId = %client;//this should stop em getting bumped
      %vehicle = %this.targetClientId.vehicleMounted;
      %node = findAIEmptySeat(%vehicle, %client.player);

      if (%node < 0 && %client.vehicleMounted != %this.targetClientId.vehicleMounted)
         return 0;

      //lets keep bombardiers, gunners in their seats
      else if (%client.vehicleMounted == %this.targetClientId.vehicleMounted)
         return 10000;
   }

   //make sure the class is appropriate unless we are in a vehicle :) - Lagg...
   if (%this.forceClientId <= 0 && %this.issuedByClientId != %client.controlByHuman)
   {
      %targArmor = %this.targetClientId.player.getArmorSize();
      %myArmor = %client.player.getArmorSize();
      %targMounted = %this.targetClientId.player.ismounted();                  

      //changed here to allow bigger escorts - Lagg... 1-31-2003
      if (%targArmor $= "Light" && %myArmor $= "Heavy" && !%targMounted)
	      return 0;
   }

   //can't bump a forced client from level 1
   if (%this.forceClientId > 0 && %this.forceClientId != %client && %level == 1)
      return 0;

   //lets check the range on this one if not forced from command map
   if (%this.forceClientId <= 0 && vectorDist(%client.Player.getWorldBoxCenter(), %this.targetClientId.player.getWorldBoxCenter()) > 200)
      return 0;

   //if this bot is linked to a human who has issued this command, up the weight
   if (%this.issuedByClientId == %client.controlByHuman)
   {
      //make sure we have the potential to reach the minWeight
      if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
      {
         if ($AIWeightHumanIssuedEscort < %minWeight)
            return 0;
         else
            %weight = $AIWeightHumanIssuedEscort;
      }
      else
      {
         // calculate the default...
         %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
         if (%weight < $AIWeightHumanIssuedEscort)
            %weight = $AIWeightHumanIssuedEscort;
      }
   }
   else
   {
      //if we are in base the target client must be as well unless forced by a human - Lagg... 9/14/09
      if (!%this.targetClientId.inBase && %client.inBase)
         return 0;

      //make sure we have the potential to reach the minWeight
      if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
         return 0;

      // calculate the default...
      %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
   }

   return %weight;
}

function AIOEscortPlayer::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIEscortPlayer);
   %client.objectiveTask.initFromObjective(%this, %client);

   //echo("AIOEscortPlayer::assignClient - " @ getTaggedString(%client.name));

}

function AIOEscortPlayer::unassignClient(%this, %client)
{
   //error("AIOEscortPlayer::unassignClient - " @ getTaggedString(%client.name));

   //make sure we are standing up right// still testing this - Lagg...
   if (AIClientIsAlive(%client))
      %client.player.setActionThread("root");

   //kill the escort objective
   if (%client.escort)
   {
      AIClearObjective(%client.escort);
      %client.escort.delete();
      %client.escort = "";
   }
   %client.repairObject = -1;
   %client.setTargetObject(-1);
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//----------------------------------------------------------------------------------------------- AI Escort Player ---

function AIEscortPlayer::initFromObjective(%task, %objective, %client)
{
   %task.baseWeight = %client.objectiveWeight;
   %task.targetClient = %objective.targetClientId;
   %task.equipment = %objective.equipment;
   %task.buyEquipmentSet = %objective.buyEquipmentSet;
   %task.desiredEquipment = %objective.desiredEquipment;
   %task.mode = %objective.mode;
   %task.issuedByClient = %objective.issuedByClientId;
   %task.forceClient = %objective.forceClientId;
   %task.chat = %objective.chat;

   //initialize other task vars
   %task.sendMsg = true;
   %task.sendMsgTime = 0;
}

function AIEscortPlayer::assume(%task, %client)
{
   %task.setWeightFreq(15);//was 15
   %task.setMonitorFreq(15);//was 15
   %task.rangedTarget = false;
   %task.engageVehicle = -1;

   if (%task == %client.objectiveTask && %client == %task.forceClient &&
     %task.issuedByClient == %task.targetClient && !%task.targetClient.player.isMounted())
      %client.needEquipment = false;
   else
   {
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;

      %client.needEquipment = AINeedEquipment(%equipmentList, %client);

      if (! %client.needEquipment)
         //%client.stepEscort(%task.targetClient);
         %client.stepMove(%task.targetClient.player.position, 8, $AIModeExpress);
   }

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();
}

function AIEscortPlayer::retire(%task, %client)
{
   //clear the attack object flags
   %task.engageVehicle = -1;
   %client.setTargetObject(-1);

   if (aiClientIsAlive(%client))
   {
      %client.clearStep();
      if(%client.player.isMounted())
         AIDisembarkVehicle(%client);

      //clear the target object
      %client.setTargetObject(-1);

      //tailgunner stop repairing
      if (%client.player.getImageState($BackpackSlot) $= "Activate")
      {
         %client.player.use("RepairPack");
         %client.player.cycleWeapon();
      }
   }
}

function AIEscortPlayer::weight(%task, %client)
{
   //update the task weight
   %task.baseWeight = %client.objectiveWeight;

   //make sure we still have someone to escort
   if (!AiClientIsAlive(%task.targetClient))
   {
      %task.setWeight(0);
      AIUnassignClient(%client);
      Game.AIChooseGameObjective(%client);
      return;
   }

   //always shoot at the closest person to the client being escorted
   %targetPos = %task.targetClient.player.getWorldBoxCenter();
   %losTimeout = $AIClientMinLOSTime + ($AIClientLOSTimeout * %client.getSkillLevel());

   //if mounted in a HAPC or TAILGUNNER position act like a tailgunner first
   if (%client.player.isMounted())
   {
      %task.engageVehicle = -1;
      %vehicle = %client.vehicleMounted;
      %type = %vehicle.getDataBlock().getName();
      if (%type $= "HAPCFlyer" || %type $= "BomberFlyer")
      {        
         //search for a new vehicle to attack
         //%task.engageVehicle = -1;
         %losTimeout = $AIClientMinLOSTime + ($AIClientLOSTimeout * %client.getSkillLevel());
         %result = AIFindClosestEnemyPilot(%client, 400, %losTimeout, true);// - ignores los
         //%result = AIFindClosestEnemyPilot(%client, 400, %losTimeout);//uses los
         %pilot = getWord(%result, 0);

         //if we've got a vehicle to attack
         if (AIClientIsAlive(%pilot))
            %task.engageVehicle = %pilot.vehicleMounted;

         //no pilot then look for players
         if (firstWord(%pilot) <= 0)
            %result = AIFindClosestEnemyToLoc(%client, %targetPos, 300, %losTimeout);
      }
   }
   else
      %result = AIFindClosestEnemyToLoc(%client, %targetPos, 250, %losTimeout);//was 200 - Lagg...

   %task.engageTarget = getWord(%result, 0);

   if (!AIClientIsAlive(%task.engageTarget))
   {
      if (AIClientIsAlive(%task.targetClient.lastDamageClient, %losTimeout) && getSimTime() - %task.targetClient.lastDamageTime < %losTimeout)
         %task.engageTarget = %task.targetClient.lastDamageClient;
   }
   if (!AIClientIsAlive(%task.engageTarget))
   {
      %myPos = %client.player.getWorldBoxCenter();
		%result = AIFindClosestEnemy(%client, 50, %losTimeout);
      %task.engageTarget = getWord(%result, 0);
   }

   //if both us and the person we're escorting are in a vehicle, set the weight high!
   if (%task.targetClient.player.isMounted() && %client.player.isMounted())
   {
      %vehicle = %client.vehicleMounted;

      //if (%vehicle > 0 && isObject(%vehicle) && %vehicle.getDamagePercent() < 0.8) *** TESTING HERE *** To try and stop UE when Tank Blows up
      if (%vehicle > 0 && isObject(%vehicle))
      {
         %task.setWeight($AIWeightVehicleMountedEscort);
         AIProcessVehicle(%client); //call the vehicle process routine - Lagg...
      }
   }
   
   //set the task weight (so important)
   %task.setWeight(%task.baseWeight);

   //find out if our escortee is lazing a target... 
   //%task.missileTarget = -1;//moved down a bit
   %targetCount = ServerTargetSet.getCount();
   for (%i = 0; %i < %targetCount; %i++)
   {
      %targ = ServerTargetSet.getObject(%i);
      if (%targ.sourceObject == %task.targetClient.player)
      {
         //find out which item is being targetted...
	 %targPoint = %targ.getWorldBoxCenter();
         InitContainerRadiusSearch(%targPoint, 10, $TypeMasks::TurretObjectType | $TypeMasks::StaticShapeObjectType |
           $TypeMasks::VehicleObjectType);
	 %task.missileTarget = containerSearchNext();
	 break;
      }

      //moved to here so he says on target once found
      %task.missileTarget = -1;
   }
}

function AIEscortPlayer::monitor(%task, %client)
{
   //make sure we still have someone to escort
   if (!AiClientIsAlive(%task.targetClient))
   {
      AIUnassignClient(%client);
      Game.AIChooseGameObjective(%client);
      return;
   }
   //lets check the range on this one 
   if (vectorDist(%client.Player.getWorldBoxCenter(), %task.targetClient.player.getWorldBoxCenter()) > 300)
   {
      AIUnassignClient(%client);
      Game.AIChooseGameObjective(%client);
      return;
   }

   //for this we send chat first
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
	  {
	    if (%task.chat !$= "")
	    {
	       %chatMsg = getWord(%task.chat, 0);
	       %chatTemplate = getWord(%task.chat, 1);
	       if (%chatTemplate !$= "")
	          AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
	       else
		  AIMessageThread(%task.chat, %client, -1, true);
	    }
	    else
               AIMessageThreadTemplate("ChatTaskCover", "ChatTaskOnIt", %client, -1);
	 }
      }
   }

   //then buy the equipment
   if (%client.needEquipment && !%client.player.isMounted())
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;
      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
         return;
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(15);//was 15
         %client.needEquipment = false;
	      //%client.stepEscort(%task.targetClient);
         //%client.stepMove(%task.targetClient.player.position, 8, $AIModeExpress);
      }
      else if (%result $= "Failed")
      {
         if (%client.player.getArmorSize() $= "Heavy")
         {
            //if this task is the objective task, choose a new objective
	    AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
            return;
         }
         else
         {
            %task.setMonitorFreq(15);
	    %client.needEquipment = false;
         }
      }
   }

   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   %hasMissile = (%client.player.getInventory("MissileLauncher") > 0) &&
     (%client.player.getInventory("MissileLauncherAmmo") > 0);
     
   //see if our target is mounted in a vehicle...
   if (%task.targetClient.player.isMounted())
   {
      //get the vehicle in question
      %vehicle = %task.targetClient.vehicleMounted;

      //find the passenger seat the bot will take
      if (!%client.isMounted)//added - *
         %node = findAIEmptySeat(%vehicle, %client.player);

      //make sure there is still an empty seat and/or we are in the same vehicle as escortee
      if ((%node < 0 && !%client.vehicleMounted) || (%node < 0 && %client.vehicleMounted != %task.targetClient.vehicleMounted))
      {
         //if this task is the objective task, choose a new objective
	 AIUnassignClient(%client);
	 Game.AIChooseGameObjective(%client);
         return;
      }
                 
      //find the passenger seat location...
      %slotPosition = %vehicle.getSlotTransform(%node);
      
      //make sure we're in the correct armor - assault tanks cannot have a heavy...
      if (%vehicle.getDataBlock().getName() $= "AssaultVehicle")
      {
         //if the bot is in a heavy, go get equipment - Lagg...
	 if (%client.player.getArmorSize() $= "Heavy")
	 {
            if (%task == %client.objectiveTask)
            {
               if (! %task.sendMsg && isObject(%vehicle))
               {
                  %task.sendMsg = true;
                  %task.sendMsgTime = getSimTime();
                  %task.chat = "ChatNeedHold";
               }               
               %task.equipment = "Medium FlareGrenade RepairPack";
	       %task.buyEquipmentSet = "MediumRepairSet";
               %client.needEquipment = true;
               return;
	    }
	 }

         //throw away any packs that won't fit
         if (hasLargePack(%client.player))
            %client.player.throwPack();
      }

      //make sure we're in the correct armor - bomber turrets cannot have a heavy...
      if (%vehicle.getDataBlock().getName() $= "BomberFlyer")
      {
         //bd seat
         %nodeBd = %vehicle.getMountNodeObject(1);

         //tg seat
         %nodeTg = %vehicle.getMountNodeObject(2);

         //if we are a tailgunner with a repair pack lets use it :) - Lagg...
         if (%nodeTg == %client.player)
         {
            //first, check for a vehicle to engage
            if (%task.engageVehicle > 0 && isObject(%task.engageVehicle))
            {
               %client.setEngageTarget(-1);

               //check the range by skill level
               %dis = VectorDist(%client.player.position, %task.engageVehicle.getWorldBoxCenter());
               if (%dis > 200 + (100 * %client.getSkillLevel()))
               {
                  %client.setTargetObject = -1;
                  %task.engageVehicle = -1;
               }

               //destroy the boggy at 6 o'clock
               //%client.aimAt(%task.engageVehicle.getWorldBoxCenter(), 300);//no work - Lagg...
               %client.setTargetObject(%task.engageVehicle, 300, "Destroy");
            }
            else
            {
               //clear the target vehicle...
               %client.setTargetObject(-1);

               //make sure we're still shooting...
               %client.setEngageTarget(%task.engageTarget);

               //see if we're supposed to be engaging any players...
               if (!AIClientIsAlive(%client.getEngageTarget()) && AIClientIsAlive(%client.shouldEngage))
                  %client.setEngageTarget(%client.shouldEngage);
            
               if (%vehicle.getDamagePercent() > 0 && %client.player.getInventory(RepairPack) > 0 && %client.getEngageTarget() <= 0)
	       {
                  if (isObject(%vehicle) && (%client.player.getImageState($BackpackSlot) !$= "Activate"))               
                     %client.player.use("RepairPack");
                  else if (isObject(%vehicle) && isObject(%client.player.getMountedImage($WeaponSlot))
                    && %client.player.getMountedImage($WeaponSlot).getName() $= "RepairGunImage")
                  {
                     %client.aimAt(%task.targetClient.player.getWorldBoxCenter());
                     %client.repairObject = %vehicle;
                     %client.setTargetObject(%vehicle, 12, "Repair");
                  }
                  else
                  {
                     %client.repairObject = -1;
                     %client.setTargetObject(-1);
                     %client.pressFire(-1);
                  }
               }
               else
               {               
                  %client.repairObject = -1;
                  %client.setTargetObject(-1);
               }
            }
	 } 

         //we may need a tailgunner if we are following a human - Lagg...
         if (! %client.escort && %nodeTg <= 0 && %task.mode !$= "Tailgunner" &&
           !%task.targetClient.isAiControlled())
         {
            
            //create the escort objective (require flares in this case...)
            if (! %task.sendMsg && isObject(%vehicle))
            {
               %task.sendMsg = true;
               %task.sendMsgTime = getSimTime();
               %task.chat = "ChatNeedTailgunner";
            }

            %client.escort = new AIObjective(AIOEscortPlayer)
            {
               dataBlock = "AIObjectiveMarker";
                        weightLevel1 = $AIWeightVehicleMountedEscort;
                        weightLevel2 = 0;
                        description = "Escort " @ getTaggedString(%client.name);
                        targetClientId = %client;
                        issuedByHuman        = true;
                        mode = "Tailgunner";
                        offense = true;
                        chat = "ChatNeedRide";
                        equipment = "FlareGrenade";
	                buyEquipmentSet = "HeavyRepairSet";
            };
            MissionCleanup.add(%client.escort);
            //if is siege game we have to do this right - Lagg... 11-3-2003
            if (Game.class $= "SiegeGame")
            {
               //the objectives in $ObjectiveQ[1] are all offense objectives, $ObjectiveQ[2] has the defensive ones..
               if (%client.team == game.offenseTeam)
                  $ObjectiveQ[1].add(%client.escort);
               else
                  $ObjectiveQ[2].add(%client.escort);
            }
            else
               $ObjectiveQ[%client.team].add(%client.escort);
         }

         //throw away any packs that won't fit
         if (hasLargePack(%client.player))
            %client.player.throwPack();
      }
      //client escort is in a havoc
      else
      {
         //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ** HERE! ***
         //if we have a sensor jammer pack, use it near enemy turrets - 4/19/2009
         if (%client.player.getInventory(SensorJammerPack) > 0 && %client.player.getEnergyPercent() > 0.4)
         {
            %result = findClosestEnemyTarget(%client, %vehicle);//new function above, also tied to functions in ai.cs file
            %clTarg = getWord(%result,0);
            %clTargDist = getWord(%result, 1);

            if (%clTarg > 0 && %clTargDist < 300)
            {
               //error("AIEscortPlayer::monitor - we have SJPACK and we have turret/sensor in range turn on pack!");

               //we can check for a quick LOS to the turret with function - checkLOSPoint(%pt, %pt1) it returns true or False

               //ok we have an enemy turret or sensor and it is close, if we have LOS turn on Pack

               if (%client.player.getImageState($BackpackSlot) $= "Idle")
                  %client.player.use("Backpack");
            }
         }
             //else if (%client.player.getImageState($BackpackSlot) $= "activate" || %myEnergy < 0.4)
					           //%client.player.use("Backpack");
               

         //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ** ^^^  THERE ^^^ ***

         //check for a vehicle to engage
         if (%task.engageVehicle > 0 && isObject(%task.engageVehicle))
         {
            %client.setEngageTarget(-1);

            //check the range by skill level
            %dis = VectorDist(%client.player.position, %task.engageVehicle.getWorldBoxCenter());
            if (%dis > 200 + (100 * %client.getSkillLevel()))
            {
               %client.setTargetObject = -1;
               %task.engageVehicle = -1;
            }
            else
            {

               //destroy the boggy at 6 o'clock
               %client.aimAt(%task.engageVehicle.getWorldBoxCenter(), 300);
               %client.setTargetObject(%task.engageVehicle, 300, "Destroy");
            }
         }
         else
         {
            //clear the target vehicle...
            %client.setTargetObject(-1);

            //make sure we're still shooting...
            %client.setEngageTarget(%task.engageTarget);

            //see if we're supposed to be engaging any players...
            if (!AIClientIsAlive(%client.getEngageTarget()) && AIClientIsAlive(%client.shouldEngage))
               %client.setEngageTarget(%client.shouldEngage);
            
            if (%vehicle.getDamagePercent() > 0 && %client.player.getInventory(RepairPack) > 0 && %client.getEngageTarget() <= 0)
	    {
               if (isObject(%vehicle) && (%client.player.getImageState($BackpackSlot) !$= "Activate"))               
                  %client.player.use("RepairPack");
               else if (isObject(%vehicle) && isObject(%client.player.getMountedImage($WeaponSlot))
                 && %client.player.getMountedImage($WeaponSlot).getName() $= "RepairGunImage")
               {
                  %client.aimAt(%task.targetClient.player.getWorldBoxCenter());
                  %client.repairObject = %vehicle;
                  %client.setTargetObject(%vehicle, 12, "Repair");
               }
               else
               {
                  %client.repairObject = -1;
                  %client.setTargetObject(-1);
                  %client.pressFire(-1);
               }
            }
            else
            {               
               %client.repairObject = -1;
               %client.setTargetObject(-1);
            }
         }
      }
		
      if (%client.player.isMounted())
      {
         %task.setMonitorFreq(15);//testing here

         %vehicle = %task.targetClient.vehicleMounted;

         //make sure it's the same vehicle :)
         if (%client.vehicleMounted != %vehicle)
            AIDisembarkVehicle(%client);
      }
      else if (!%client.player.isMounted())
      {
         //check for empty seat
         if (!%client.isMounted)//added - *
            %node = findAIEmptySeat(%vehicle, %client.player);

         //make sure there is still an empty seat and/or we are in the same vehicle as escortee
         if ((%node < 0 && !%client.vehicleMounted) || (%node < 0 && %client.vehicleMounted != %vehicle))
         {
            //if (%task == %client.objectiveTask)
	    AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
            return;
         }
                 
         //find the passenger seat location...
         %slotPosition = %vehicle.getSlotTransform(%node);

         if (%vehicle.getDataBlock().getName() $= "BomberFlyer")
         {
            if (%nodeBd <= 0 && %client.player.getArmorSize() $= "Heavy" && %task.mode !$= "Tailgunner")
            {
               if (! %task.sendMsg && isObject(%vehicle))
               {
                  %task.sendMsg = true;
                  %task.sendMsgTime = getSimTime();
                  %task.chat = "ChatNeedHold";                     
               }
               %task.equipment = "Medium";
	       %task.buyEquipmentSet = "MediumMissileSet";
               %client.needEquipment = true;
               return;
            }
            if (%nodeBd > 0 && %nodeTg <= 0 && %client.player.getInventory(FlareGrenade) <= 0)
            {
               if (! %task.sendMsg && isObject(%vehicle))
               {
                  %task.sendMsg = true;
                  %task.sendMsgTime = getSimTime();
                  %task.chat = "ChatNeedHold";
               }
               %task.mode = "Tailgunner";
               %task.equipment = "FlareGrenade RepairPack";
	       %task.buyEquipmentSet = "HeavyRepairSet";
               %client.needEquipment = true;
               return;
            }
         }
         //mount the vehicle - heavily modified - Lagg... 10-16-2003
         //%client.stepMove(%slotPosition, 0.25, $AIModeMountVehicle);

         //check for empty seat
         if (!%client.isMounted)//added - *
            %node = findAIEmptySeat(%vehicle, %client.player);

         //if we are in water than we must be smart
         if (%client.inWater)
         {              
            %vPos2D = getWord(%vehicle.getPosition(), 0) SPC getWord(%vehicle.getPosition(), 1) SPC "0";
            %myPos2D = getWord(%client.player.position, 0) SPC getWord(%client.player.position, 1) SPC "0";
            if (vectorDist(%myPos2D, %vPos2D) < 2.5)
            {
               %client.pressJump();
               %client.stepMove(%vehicle.getWorldBoxCenter(), 0.25, $AIModeGainHeight);
               %client.mountVehicle = %vehicle;//force ai to mount used in ai.cs
            }
            else
               %client.stepMove(%vehicle.position, 0.25, $AIModeExpress);
         }
         
         //then check if it is a bomber
         else if (%vehicle.getdataBlock().getName() $= "BomberFlyer")
         {
            if (%node < 2)
            {
               if (vectorDist(%client.Player.getWorldBoxCenter(), %vehicle.getWorldBoxCenter()) < 20)
               {
                  //%speed = VectorLen(%client.player.getVelocity());
                  %task.setMonitorFreq(1);//testing here

                     %client.player.setActionThread("sitting", true, true);
                     %client.stepMove(%slotPosition, 0.25, $AIModeMountVehicle);
                     %client.player.setActionThread("sitting", true, true);
               }
               else
               {
                  %task.setMonitorFreq(15);//testing here

                  %client.player.setActionThread("root");

                  %client.stepMove(%vehicle.getWorldBoxCenter(), 18, $AIModeExpress);
               }
            }
            else
            {
               %task.setMonitorFreq(15);//testing here
 
               %client.player.setActionThread("root");

               %client.stepMove(%slotPosition, 0.25, $AIModeMountVehicle);//*** TESTING HERE ***
            }                     
         }
         //vehicle is a Tank 
         else if (%vehicle.getdataBlock().getName() $= "AssaultVehicle")
         {               
            if (vectorDist(%client.Player.getWorldBoxCenter(), %vehicle.getWorldBoxCenter()) < 18)
            {
               //%speed = VectorLen(%client.player.getVelocity());
               %task.setMonitorFreq(1);//testing here

                  %client.player.setActionThread("sitting", true, true);

                  %client.stepMove(%slotPosition, 0.25, $AIModeMountVehicle);//*** TESTING HERE ***

                  %client.player.setActionThread("sitting", true, true);
            }
            else
            {
               %task.setMonitorFreq(15);//testing here

               %client.player.setActionThread("root");

               %client.stepMove(%vehicle.getWorldBoxCenter(), 16, $AIModeExpress);
            }
         }
         //else must be a havoc so mount regularly (not so fast) move to a good spot and wait
         else
         {
            if (vectorDist(%client.Player.getWorldBoxCenter(), %task.targetClient.player.position) > 40)
            {
               if (vectorDist(%client.player.getWorldBoxcenter(), %task.targetClient.objectiveTask.waitSpot) <= 20)
                  %client.stepIdle(%task.targetClient.objectiveTask.waitSpot);
               else
                  %client.stepMove(%task.targetClient.objectiveTask.waitSpot, 0.25, $AIModeExpress);
            }
            else
            {
               //%client.stepMove(%slotPosition, 0.25, $AIModeMountVehicle);//*** TESTING HERE ***

               //just try to mount the vehicle
               %client.stepMove(getWords(%vehicle.getTransform(), 0, 2), 0.25, $AIModeMountVehicle);//*** TESTING HERE ***
            }
         }
      }
   }
   else//------------------------------------------------------------------//target client is not mounted
   {
      //lets end the vehicle conversation stuff - Lagg...
      if (%task == %client.objectiveTask)
      {
         %task.sendMsg = false;
         %task.chat = "";
      }

      //disembark if we're mounted, but our target isn't (anymore)
      if (%client.player.isMounted())
         AIDisembarkVehicle(%client);

      //kill the escort (tailgunner) objective
      if (%client.escort)
      {
         AIClearObjective(%client.escort);
         %client.escort.delete();
         %client.escort = "";
      }

      //don't block the vehicle pad if we are escorting a pilot
      if (%task.targetClient.needVehicle)
      {
         %clVs = AIFindClosestVStation(%client);
         if (%clVs > 0)
         {
            %closestVs = getWord(%clVs, 0);
            %closestVsDist = getWord(%clVs, 1);
            if (%closestVsDist < 7)
               %client.setDangerLocation(%task.targetClient.player.location, 10);
            else if (%closestVsDist < 10)
               %client.stop();
         }
      }

      //END VEHICLE STUFF----------------------------------------------------- Lagg...
   
      //see if we're supposed to be mortaring/missiling something...
      //%client.player.setActionThread("root", true);


      if (!isObject(%task.engageTarget) && isobject(%task.missileTarget) && %task.missileTarget.getDamageState() !$= "Destroyed" && (%hasMortar || %hasMissile))
      {
         if (%task.rangedTarget)
         {
            %client.stop();
	    %client.clearStep();
	    %client.setEngageTarget(-1);

            %type = %task.missileTarget.getDataBlock().getName();
	    if (%type $= "SensorLargePulse" && %hasMissile)
               %client.setTargetObject(%task.missileTarget, 300, "MissileNoLock");
	    else if (%type $= "SensorMediumPulse" && %hasMissile)
               %client.setTargetObject(%task.missileTarget, 300, "MissileNoLock");
	    else if (%type $= "TurretBaseLarge" && %hasMissile)
               %client.setTargetObject(%task.missileTarget, 300, "MissileNoLock");
            else if (%type $= "MobileBaseVehicle" && %hasMissile)
               %client.setTargetObject(%task.missileTarget, 300, "MissileNoLock");
	    else
               %client.setTargetObject(%task.missileTarget, 300, "Mortar");
         }
         else if (%client.getStepName() !$= "AIStepRangeObject")
         {
            if (%hasMortar || %hasMissile)
	       %client.stepRangeObject(%task.missileTarget, "BasicTargeter", 100, 300);
	    else
	       %client.stepRangeObject(%task.missileTarget, "DefaultRepairBeam", 20, 30);
         }
         else if (%client.getStepStatus() $= "Finished")
         %task.rangedTarget = true;
      }

      //move to waiting spot for a ride if we are escorting a pilot
      else if (%task.targetClient.needVehicle || %task.targetClient.vehicleMounted)
      {
         if (vectorDist(%client.player.getWorldBoxcenter(), %task.targetClient.objectiveTask.waitSpot) < 20)
            %client.stepIdle(%task.targetClient.objectiveTask.waitSpot);
         else
            %client.stepMove(%task.targetClient.objectiveTask.waitSpot, 0.25, $AIModeExpress);
      }
      else
      {
        if ((%client.repairObject != %task.targetClient.vehicleMounted) || !%client.player.isMounted())
        {
           %task.rangedTarget = false;
           %client.setTargetObject(-1);
        }
        if (%client.getStepName() !$= "AIStepEscort")
           %client.stepEscort(%task.targetClient);

        //make sure we're still shooting...
        %client.setEngageTarget(%task.engageTarget);

        //see if we're supposed to be engaging anyone...
        if (!AIClientIsAlive(%client.getEngageTarget()) && AIClientIsAlive(%client.shouldEngage))
           %client.setEngageTarget(%client.shouldEngage);
      }
   }
}

//----------------------------------------------------------------------------------------------- AIO Laze Object --- (last update 3-30-2004)
//fixed here so bots will acually use the targetting lazer for the right team :) - Lagg... - 4-12-2003
//modified last laze time and distance to client we laze for.
   
function AIOLazeObject::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   //if we are in water forget it
   if (%client.inWater)
      return 0;

   //not if we are in the base
   if (%client.inBase)
      return 0;

   //lets check the range on this one
   if (vectorDist(%client.Player.getWorldBoxCenter(), %this.targetObjectId.getWorldBoxCenter()) < 100)
      return 0;
      
	//see if it's already being lazed
	%numTargets = ServerTargetSet.getCount();
	for (%i = 0; %i < %numTargets; %i++)
	{
		%targ = ServerTargetSet.getObject(%i);
		if (%targ.sourceObject != %client.player)
		{
			%targDist = VectorDist(%targ.getWorldBoxCenter(), %this.targetObjectId.getWorldBoxCenter());
			if (%targDist < 100)//was 10
			{
				%this.lastLazedTime = getSimTime();
				%this.lastLazedClient = %targ.sourceObject.client;
				break;
 			}
		}
	}
   if (%this.issuedByClientId > 0 && %this.issuedByClinetId == %client)
   {
       echo("cant laze target for your self IDIOT !");
       return 0;
   }

   //no need to laze if the object is already destroyed
   if (!isObject(%this.targetObjectId) || %this.targetObjectId.getDamageState() $= "Destroyed")
      return 0;
	else if (%this.targetObjectId.isHidden() || %this.targetObjectId.team == %client.team)
      return 0;      
	else if (getSimTime() - %this.lastLazedTime <= 10000 && %this.lastLazedClient == %client)
		return 0;
   else
	{
	   //set the base weight
	   switch (%level)
	   {
	      case 1:
	         %weight = %this.weightLevel1;
	      case 2:
	         %weight = %this.weightLevel2;
	      case 3:
	         %weight = %this.weightLevel3;
	      default:
	         %weight = %this.weightLevel4;
	   }
   
	   //check Affinity
	   if (ClientHasAffinity(%this, %client))
	      %weight += 100;
      
		//for now, do not deviate from the current assignment to laze a target, if you don't
		//already have a targeting laser.
		%needEquipment = AINeedEquipment(%this.equipment, %client);
		if (!%needEquipment)
			%weight += 100;
		else if (!aiHumanHasControl(%client.controlByHuman, %client))
			return 0;

		//see if this client is close to the issuing client
		if (%this.issuedByClientId > 0)
		{
			if (! AIClientIsAlive(%this.issuedByClientId))
				return 0;

		   %distance = %client.getPathDistance(%this.issuedByClientId.player.getWorldBoxCenter());
			if (%distance < 0)
				%distance = 32767;

		   //see if we're over 300 m
		   if (%distance > 300)
		      return 0;

                   //see if we're within 200 m
		   if (%distance < 200)
		      %weight += 30;

		   //see if we're within 90 m
		   if (%distance < 90)
		      %weight += 30;

		   //see if we're within 45 m
		   if (%distance < 45)
		      %weight += 30;
		}

		//now, if this bot is linked to a human who has issued this command, up the weight
		if (%this.issuedByClientId == %client.controlByHuman && %weight < $AIWeightHumanIssuedCommand)
			%weight = $AIWeightHumanIssuedCommand;

		return %weight;
	}
}

function AIOLazeObject::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AILazeObject);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIOLazeObject::unassignClient(%this, %client)
{
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//----------------------------------------------------------------------------------------------- AI Laze Object ---

function AILazeObject::initFromObjective(%task, %objective, %client)
{
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;
   %task.equipment = %objective.equipment;
	%task.buyEquipmentSet = %objective.buyEquipmentSet;
	%task.desiredEquipment = %objective.desiredEquipment;
	%task.issuedByClient = %objective.issuedByClientId;
	%task.msgAck = true;
	%task.msgFire = true;

   //initialize the range variable
   %task.needToRangeTime = getSimTime() + 15000;
}

function AILazeObject::assume(%task, %client)
{
   %task.setWeightFreq(30);
   %task.setMonitorFreq(30);
   %client.needEquipment = AINeedEquipment(%task.equipment, %client);
   
   //clear the target object, and range it
   %client.setTargetObject(-1);
	if (! %client.needEquipment)
	   %client.stepRangeObject(%task.targetObject, "BasicTargeter", 80, 300, %task.issuedByClient.player.getWorldBoxCenter());

	//set up some task vars
	%task.celebrate = false;
	%task.waitTimerMS = 0;

	//mark the current time for the buy inventory state machine
	%task.buyInvTime = getSimTime();
}

function AILazeObject::retire(%task, %client)
{
   %client.setTargetObject(-1);
}

function AILazeObject::weight(%task, %client)
{
   //update the task weight
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;

   //let the monitor decide when to stop lazing
   %task.setWeight(%task.baseWeight);
}

function AILazeObject::monitor(%task, %client)
{
   //error("AILazeObject::monitor - " @ getTaggedString(%client.name));

   //first, buy the equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
	 %equipmentList = %task.desiredEquipment;
      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
         return;
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(30);
	 %client.needEquipment = false;
         %client.stepRangeObject(%task.targetObject, "BasicTargeter", 80, 300);
      }
      else if (%result $= "Failed")
      {
         //if this task is the objective task, choose a new objective
	 if (%task == %client.objectiveTask)
	 {
            AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	 }
	 return;
      }
   }

   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();
   
   //set the target object
   if (isObject(%task.targetObject) && %task.targetObject.getDamageState() !$= "Destroyed")
   {
      //make sure we still have equipment
		%client.needEquipment = AINeedEquipment(%task.equipment, %client);
      if (%client.needEquipment)
      {
         //if this task is the objective task, choose a new objective
         if (%task == %client.objectiveTask)
	 {
            AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	    return;
	 }
      }
      
      //look to see if anyone else is also targetting...
      %foundTarget = false;
      %numTargets = ServerTargetSet.getCount();
      for (%i = 0; %i < %numTargets; %i++)
      {
         %targ = ServerTargetSet.getObject(%i);
	 if (%targ.sourceObject != %client.player)
	 {
	    %targDist = VectorDist(%targ.getWorldBoxCenter(), %task.targetObject.getWorldBoxCenter());
	    if (%targDist < 100)//was 10 - Lagg...
	    {
	       %foundTarget = true;
	       break;
	    }
	 }
      }

      if (%foundTarget)
      {
         //if this task is the objective task, choose a new objective
	 if (%task == %client.objectiveTask)
         {
	    AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	    return;
	 }
      }
      else if (%client.getStepStatus() $= "Finished")
      {
         //dissolve the human control link
	 if (%task == %client.objectiveTask)
	    aiReleaseHumanControl(%client.controlByHuman, %client);

         //modified here in case bot gets stuck trying to get LOS to target - Lagg... - 4-21-2003 - 3-30-2004
	 %currentTime = getSimTime();
	 if (%currentTime > %task.needToRangeTime)
	 {
	    //force him to get LOS every 10 seconds...
            %task.needToRangeTime = %currentTime + 15000;
            %client.setDangerLocation(%client.player.getWorldBoxCenter(), 30);
            return;

         }

         %client.setTargetObject(%task.targetObject, 300, "Laze");
	 %task.celebrate = true;
	 %task.waitTimerMS = 0;

	 //make sure we only say "fire..." once
	 if (%task.msgFire)
	 {
	    AIMessageThread("ChatTargetFire", %client, -1);
	    %task.msgFire = false;
	 }
      }
      else
      {
         %client.stepRangeObject(%task.targetObject, "BasicTargeter", 80, 300, %task.issuedByClient.player.getWorldBoxCenter());//added here - Lagg...
         %client.aimAt(%task.targetObject.getWorldBoxCenter(), 1000);
         %client.setTargetObject(-1);
      }
   }
   else
   {
      %client.setTargetObject(-1);

      if (%task.celebrate)
      {
         if (%task.waitTimerMS == 0)
	 {
	    //add in a "woohoo"!  :)
	    //choose the animation range
	    %minCel = 3;
	    %maxCel = 8;

	    //pick a random sound
	    if (getRandom() > 0.25)
					%sound = "gbl.awesome";
				else if (getRandom() > 0.5)
					%sound = "gbl.thanks";
				else if (getRandom() > 0.75)
					%sound = "gbl.nice";
				else
					%sound = "gbl.rock";
			  	%randTime = mFloor(getRandom() * 500) + 1;
			   schedule(%randTime, %client, "AIPlayAnimSound", %client, %task.targetObject.getWorldBoxCenter(), %sound, %minCel, %maxCel, 0);

				//set the timer
            %task.waitTimerMS = getSimTime();
			}

			//else see if the celebration period is over
			else if (getSimTime() - %task.waitTimerMS > 3000)
				%task.celebrate = false;
		}
		else
		{
	      //if this task is the objective task, choose a new objective
	      if (%task == %client.objectiveTask)
			{
	         AIUnassignClient(%client);
				Game.AIChooseGameObjective(%client);
			}
		}
   }
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ AIO MAttack Location +++
//modified version of AIAttackLocation, could be called AIMortarSpamLocation :) - Lagg...

function AIOMAttackLocation::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;
      
   //now, if this bot is linked to a human who has issued this command, up the weight
   if (%this.issuedByClientId == %client.controlByHuman)
   {
      //make sure we have the potential to reach the minWeight
      if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
      {
         if ($AIWeightHumanIssuedCommand < %minWeight)
	    return 0;
	 else
	    %weight = $AIWeightHumanIssuedCommand;
      }
      else
      {
         // calculate the default...
	 %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
	 if (%weight < $AIWeightHumanIssuedCommand)
	    %weight = $AIWeightHumanIssuedCommand;
      }
   }
   else
   {
      //make sure we have the potential to reach the minWeight
      if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
         return 0;

      // calculate the default...
      %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
   }

   //Special cases to account for force fields up or down and if we are in the base or not
   if (nameToId("EntranceFFNorth").powerCount > 0 && nameToId("EntranceFFEast").powerCount > 0 && nameToId("EntranceFFWest").powerCount > 0 && !%client.inBase)
      return 0;

   if (%this.mode $= "SwitchGenDown")
   {
      if (nameToId("SwitchFFGenerator").isEnabled())
         return 0;

      if (isObject(%client.objectiveTask) && (%client.objectiveTask.getName() $= "AIDefendLocation" || %client.objectiveTask.getName() $= "AIDeployEquipment"))
      {
         //error("AIOMAttackLocation::weight - we are in base but we are " @ getTaggedString(%client.name) SPC %client.objectiveTask.getName());
         return 0;
      }
   }
   return %weight;
}

function AIOMAttackLocation::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIMAttackLocation);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIOMAttackLocation::unassignClient(%this, %client)
{
   //error("AIOMAttackLocation::unassignClient - " @ getTaggedString(%client.name));

   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//----------------------------------------------------------------------------------------------- AI MAttack Location ---

function AIMAttackLocation::initFromObjective(%task, %objective, %client)
{
        //initialize the task vars from the objective
   %task.baseWeight = %client.objectiveWeight;
   %task.location = %objective.location;
   %task.equipment = %objective.equipment;
	%task.buyEquipmentSet = %objective.buyEquipmentSet;
	%task.desiredEquipment = %objective.desiredEquipment;
	%task.issuedByClient = %objective.issuedByClientId;
	%task.chat = %objective.chat;

	//initialize other task vars
	%task.sendMsg = false;
	%task.sendMsgTime = 0;
   %task.engageTarget = -1;
}

function AIMAttackLocation::assume(%task, %client)
{
   %task.setWeightFreq(30);
   %task.setMonitorFreq(30);
	%client.needEquipment = AINeedEquipment(%task.equipment, %client);

	//even if we don't *need* equipemnt, see if we should buy some... 
	if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
	{
		//see if we could benefit from inventory
		%needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
		%result = AIFindClosestInventory(%client, %needArmor);
		%closestInv = getWord(%result, 0);
		%closestDist = getWord(%result, 1);
		if (AINeedEquipment(%task.desiredEquipment, %client) && %closestInv > 0)
		{
			%result = AIFindClosestEnemyToLoc(%client, %task.location, 100, $AIClientLOSTimeout);
	      %closestEnemy = getWord(%result, 0);

			if (%closestEnemy <= 0 && %closestDist < 100)
				%client.needEquipment = true;
		}
	}

	//mark the current time for the buy inventory state machine
	%task.buyInvTime = getSimTime();

	%task.snipeLocation = "";
	%task.hideLocation = "";
	%task.moveToPosition = true;
	%task.moveToSnipe = false;
	%task.nextSnipeTime = 0;
}

function AIMAttackLocation::retire(%task, %client)
{
}

function AIMAttackLocation::weight(%task, %client)
{
	//update the task weight
	if (%task == %client.objectiveTask)
		%task.baseWeight = %client.objectiveWeight;

	//if we're a sniper, we're going to cheat, and see if there are clients near the attack location
        %losTimeout = $AIClientMinLOSTime + ($AIClientLOSTimeout * %client.getSkillLevel());
	%distToLoc = VectorDist(%client.player.getWorldBoxCenter(), %task.location);
	if (((%client.player.getInventory(SniperRifle) > 0 && %client.player.getInventory(EnergyPack) > 0) ||
            (%client.player.getInventory(Mortar) > 0 && %client.player.getInventory(MortarAmmo) > 0)) && %distToLoc > 100)
		%result = AIFindClosestEnemyToLoc(%client, %task.location, 100, $AIClientLOSTimeout, true);

	//otherwise, do the search normally.  (cheat ignores LOS)...
	else
		%result = AIFindClosestEnemyToLoc(%client, %task.location, 100, %losTimeout, false);

   %closestEnemy = getWord(%result, 0);
   %closestdist = getWord(%result, 1);
   %task.setWeight(%task.baseWeight);
   
   //see if we found someone
   if (%closestEnemy > 0)
      %task.engageTarget = %closestEnemy;
   else
      %task.engageTarget = -1;
}

//---------------------------------------------------------------------
//modified range for bot sniper to attack location - Lagg... 4-12-2003
function AIMAttackLocation::monitor(%task, %client)
{
   //first, buy the equipment - if we really really need some :) - Lagg... - 4-6-2003
   %hasMortar = (%client.player.getInventory("Mortar") > 0) && (%client.player.getInventory("MortarAmmo") > 0);

   if (%hasMortar)
      %client.needEquipment = false;

   //first, buy the equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
	 %equipmentList = %task.desiredEquipment;
      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
         return;
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(30);
	 %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
         //if this task is the objective task, choose a new objective
	 if (%task == %client.objectiveTask)
	 {
	    AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	 }
	 return;
      }
   }
   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   //chat
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
	 {
	    if (%task.chat !$= "")
	    {
	       %chatMsg = getWord(%task.chat, 0);
	       %chatTemplate = getWord(%task.chat, 1);
	       if (%chatTemplate !$= "")
	          AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
	       else
	          AIMessageThread(%task.chat, %client, -1);
	    }
	    else
	       AIMessageThreadTemplate("AttackBase", "ChatSelfAttack", %client, -1);
	 }
      }
   }

   //make sure we still have equipment
   %client.needEquipment = AINeedEquipment(%task.equipment, %client);
   if (%client.needEquipment)
   {
      //if this task is the objective task, choose a new objective
      if (%task == %client.objectiveTask)
      {
         AIUnassignClient(%client);
	 Game.AIChooseGameObjective(%client);
	 return;
      }
   }

   //how far are we from the location we're defending
   %myPos = %client.player.getWorldBoxCenter();
   %distance = %client.getPathDistance(%task.location);
   %hasSniper = %client.player.getInventory(SniperRifle) > 0 && %client.player.getInventory(EnergyPack) > 0;
   %hasMortar = %client.player.getInventory(Mortar) > 0 && %client.player.getInventory(MortarAmmo) > 0;
   if (%distance < 0)
      %distance = 32767;

   if (%hasSniper || %hasMortar)
   {
      //first, find an LOS location
      if (%task.snipeLocation $= "")
      {
         %task.snipeLocation = %client.getLOSLocation(%task.location, 150, 300);//was 150-200 - Lagg...
	 %task.hideLocation = %client.getHideLocation(%task.location, VectorDist(%task.location, %task.snipeLocation), %task.snipeLocation, 1); 
	 %client.stepMove(%task.hideLocation, 4.0);
         %task.moveToPosition = true;
      }
      else
      {
         //see if we can acquire a target
	 %energy = %client.player.getEnergyPercent();
         %distToSnipe = VectorDist(%task.snipelocation, %client.player.getWorldBoxCenter());
	 %distToHide = VectorDist(%task.hidelocation, %client.player.getWorldBoxCenter());

	 //until we're in position, we can move using the AIModeExpress, after that, we only want to walk...
	 if (%task.moveToPosition)
	 {
	    if (%distToHide < 4.0)
	    {
	       //dissolve the human control link
	       if (%task == %client.objectiveTask)
	       {
	          if (aiHumanHasControl(%task.issuedByClient, %client))
		  {
		     aiReleaseHumanControl(%client.controlByHuman, %client);

		     //should re-evaluate the current objective weight
		     %inventoryStr = AIFindClosestInventories(%client);
		     %client.objectiveWeight = %client.objective.weight(%client, %client.objectiveLevel, 0, %inventoryStr);
		  }
	       }
               %task.moveToPosition = false;
	    }
         }
         else if (%task.moveToSnipe)
	 {
	    if (((%hasSniper && %energy > 0.75) || %hasMortar) && %client.getStepStatus() $= "Finished")
	    {
               %client.stepMove(%task.snipeLocation, 4.0, $AIModeWalk);
	       %client.setEngageTarget(%task.engageTarget);
	    }
	    else if (%hasSniper && %energy < 0.4)
	    {
               %client.setEngageTarget(-1);
	       %client.stepMove(%task.hideLocation, 4.0);
	       //%task.nextSnipeTime = getSimTime() + 4000 + (getRandom() * 4000);
               %task.nextSnipeTime = getSimTime() + 1000 + (getRandom() * 2000);
	       %task.moveToSnipe = false;
	    }
         }
         else if (((%hasSniper && %energy > 0.5) || %hasMortar) && %task.engageTarget > 0 && getSimTime() > %task.nextSnipeTime)
	 {
            %task.sendMsg = true;
            %client.stepRangeObject(%task.engageTarget.player.getWorldBoxCenter(), "BasicSniperShot", 150, 300, %task.snipelocation);
            if (! %hasMortar)
	       %client.aimAt(%task.engageTarget.player.getWorldBoxCenter(), 8000);
	    %task.moveToSnipe = true;
	 }
      }
   }
   //else we don't have sniper or mortar stuff so attack regular like :( - Lagg...
   else
   {
      //else see if we have a target to begin attacking
      if (%client.getEngageTarget() <= 0 && %task.engageTarget > 0)
         %client.stepEngage(%task.engageTarget);

      //else move to the location we're attacking
      else if (%client.getEngageTarget() <= 0)
      {
         %client.stepMove(%task.location, 8.0);
	 if (VectorDist(%client.player.position, %task.location) < 10)
	 {
	    //dissolve the human control link
	    if (%task == %client.objectiveTask)
	    {
	       if (aiHumanHasControl(%task.issuedByClient, %client))
	       {
		  aiReleaseHumanControl(%client.controlByHuman, %client);

		  //should re-evaluate the current objective weight
		  %inventoryStr = AIFindClosestInventories(%client);
		  %client.objectiveWeight = %client.objective.weight(%client, %client.objectiveLevel, 0, %inventoryStr);
	       }
	    }
	 }
      }
   }

   //see if we're supposed to be engaging anyone...
   if (!AIClientIsAlive(%client.getEngageTarget()) && AIClientIsAlive(%client.shouldEngage))
      %client.setEngageTarget(%client.shouldEngage);
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ AIO MDestroy Object +++
//place objective marker in spot you want bot to stand and destroy for sentry turrets and hard to
//reach spots. basically same as destroy object but with mortars :)           - Lagg... 3-26-2003

function AIOMDestroyObject::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   // if were playing CnH, check who owns this
   if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden() || %this.targetObjectId.team == %client.team)
      return 0;      
   
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;
      
   //no need to attack if the object is already destroyed
   if (!isObject(%this.targetObjectId) || %this.targetObjectId.getDamageState() $= "Destroyed")
      return 0;
   else
	{
		//if this bot is linked to a human who has issued this command, up the weight
		if (%this.issuedByClientId == %client.controlByHuman)
		{
			//make sure we have the potential to reach the minWeight
			if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
			{
				if ($AIWeightHumanIssuedCommand < %minWeight)
					return 0;
				else
					%weight = $AIWeightHumanIssuedCommand;
			}
			else
			{
				// calculate the default...
				%weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
				if (%weight < $AIWeightHumanIssuedCommand)
					%weight = $AIWeightHumanIssuedCommand;
			}
		}
		else
		{
			//make sure we have the potential to reach the minWeight
			if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
				return 0;

			// calculate the default...
			%weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
		}

      //if we are playing DnD, check if objective was completed already, if not give some extra :) - Lagg... 3-29-2004
      if (Game.class $= "DnDGame")
      {
         if (%this.targetObjectId.objectiveCompleted)
            %weight -= 400;
         else
            %weight += 200;
      }

      return %weight;
   }
}

function AIOMDestroyObject::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIMDestroyObject);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIOMDestroyObject::unassignClient(%this, %client)
{
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";

   //error("AIOMDestroyObject::unassignClient - " @ getTaggedString(%client.name));
}

//----------------------------------------------------------------------------------------------- AI MDestroy Object ---

function AIMDestroyObject::initFromObjective(%task, %objective, %client)
{
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;
   %task.equipment = %objective.equipment;
	%task.buyEquipmentSet = %objective.buyEquipmentSet;
	%task.desiredEquipment = %objective.desiredEquipment;
	%task.issuedByClient = %objective.issuedByClientId;
        %task.location = %objective.location;

	//initialize other task vars
	%task.sendMsg = true;
	%task.sendMsgTime = 0;
   %task.useTeleport = false;

   if (%task.equipment $= "")
   {
      //pick a random equipment set...
      %randNum = getRandom();
      if (%randNum < 0.2)
      {
         %task.desiredEquipment = "Plasma PlasmaAmmo";
         %task.buyEquipmentSet = "HeavyEnergySet";
      }
      else if (%randNum < 0.4)
      {
         %task.desiredEquipment = "Plasma PlasmaAmmo";
         %task.buyEquipmentSet = "HeavyAmmoSet";
      } 
      else if (%randNum < 0.6)
      {
         %task.desiredEquipment = "Plasma PlasmaAmmo";
         %task.buyEquipmentSet = "HeavyEnergyDefault";
      }
      else if (%randNum < 0.8)
      {
         %task.desiredEquipment = "Plasma PlasmaAmmo";
         %task.buyEquipmentSet = "HeavyShieldOff";
      }
      else
      {
         %task.desiredEquipment = "Plasma PlasmaAmmo";
         %task.buyEquipmentSet = "HeavyRepairSet";
      }
   }
}

function AIMDestroyObject::assume(%task, %client)
{
   %task.setWeightFreq(20);
   %task.setMonitorFreq(20);

   %client.needEquipment = AINeedEquipment(%task.equipment, %client);

	//even if we don't *need* equipemnt, see if we should buy some... 
	if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
	{
		//see if we could benefit from inventory
		%needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
		%result = AIFindClosestInventory(%client, %needArmor);
		%closestInv = getWord(%result, 0);
		%closestDist = getWord(%result, 1);
		if (AINeedEquipment(%task.desiredEquipment, %client) && %closestInv > 0)
		{
			//find where we are
			%clientPos = %client.player.getWorldBoxCenter();
			%objPos = %task.targetObject.getWorldBoxCenter();
			%distToObject = %client.getPathDistance(%objPos);
			
			if (%distToObject < 0 || %closestDist < 100)//%distToObject)
				%client.needEquipment = true;
		}
	}

	//mark the current time for the buy inventory state machine
	%task.buyInvTime = getSimTime();

   //--------------------------------------------------- assume telporter - start -
   //if the MPB Teleporter is online - Lagg... 9-30-2003

   //first check for deployed MPB
   %result = AIFindDeployedMPB(%client);//new function in aiVehicle.cs
   %closestMPB = getWord(%result, 0);
   %closestMPBDist = getWord(%result, 1);
      
   //next find the teleporter
   %result = SweepForTeleporters(%client);//new function in aiVehicle.cs
   %closestTel = getWord(%result, 0);
   %closestTelDist = getWord(%result, 1);
      
   //now check for what would be closer - Lagg... 10-1-2003
   if (%closestMPB > 0 && %closestTel > 0)
   {
      %disToTarg = %client.getPathDistance(%task.location);
      %mpbDisToTarg = vectorDist(%closestMPB.getTransform(), %task.location);
         
      if (%closestTelDist < %closestMPBDist && %closestMPBDist < %disToTarg && %mpbDisToTarg < %disToTarg)
         %task.useTeleport = true;//new - Lagg... 10-1-2003
      else
         %task.useTeleport = false;             
   }
   //--------------------------------------------------- assume telporter - end -
}

function AIMDestroyObject::retire(%task, %client)
{
   %client.setTargetObject(-1);
}

function AIMDestroyObject::weight(%task, %client)
{
	//update the task weight
	if (%task == %client.objectiveTask)
		%task.baseWeight = %client.objectiveWeight;

   //let the monitor decide when to stop attacking
   %task.setWeight(%task.baseWeight);
}

function AIMDestroyObject::monitor(%task, %client)
{
   //first, buy the equipment - if we really really need some :) - Lagg... - 4-6-2003
   %hasMortar = (%client.player.getInventory("Mortar") > 0) && (%client.player.getInventory("MortarAmmo") > 0);

   if (%hasMortar)
      %client.needEquipment = false;

   if (%client.needEquipment)
   {
	   %task.setMonitorFreq(5);
		if (%task.equipment !$= "")
			%equipmentList = %task.equipment;
		else
			%equipmentList = %task.desiredEquipment;

      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
         return;
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(20);
         %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
         //if this task is the objective task, choose a new objective
	 if (%task == %client.objectiveTask)
	 {
            AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	 }
	 return;
      }
   }
	//if we made it past the inventory buying, reset the inv time
	%task.buyInvTime = getSimTime();
   
	//chat
	if (%task.sendMsg)
	{
		if (%task.sendMsgTime == 0)
			%task.sendMsgTime = getSimTime();
		else if (getSimTime() - %task.sendMsgTime > 7000)
		{
			%task.sendMsg = false;
		   if (%client.isAIControlled())
			{
				if (%task.chat !$= "")
				{
					%chatMsg = getWord(%task.chat, 0);
					%chatTemplate = getWord(%task.chat, 1);
					if (%chatTemplate !$= "")
						AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
					else
						AIMessageThread(%task.chat, %client, -1);
				}
				else if (%task.targetObject > 0)
				{
					%type = %task.targetObject.getDataBlock().getName();
					if (%type $= "GeneratorLarge")
						AIMessageThreadTemplate("AttackBase", "ChatSelfAttackGenerator", %client, -1);
					else if (%type $= "SensorLargePulse")
						AIMessageThreadTemplate("AttackBase", "ChatSelfAttackSensors", %client, -1);
					else if (%type $= "SensorMediumPulse")
						AIMessageThreadTemplate("AttackBase", "ChatSelfAttackSensors", %client, -1);
					else if (%type $= "TurretBaseLarge")
						AIMessageThreadTemplate("AttackBase", "ChatSelfAttackTurrets", %client, -1);
					else if (%type $= "StationVehicle")
						AIMessageThreadTemplate("AttackBase", "ChatSelfAttackVehicle", %client, -1);
				}
			}
		}
	}

   //--------------------------------------------------- monitor telporter - start -
   //are we using teleporter
   if (%task.useTeleport)
   {
      %result = AIFindDeployedMPB(%client);//new function in aiVehicle.cs
      %closestMPB = getWord(%result, 0);
      %closestMPBDist = getWord(%result, 1);
      %result = SweepForTeleporters(%client);//new function in aiVehicle.cs
      %closestTel = getWord(%result, 0);
      %closestTelDist = getWord(%result, 1);
      if (%closestMPB > 0 && %closestTel > 0)
      {
         //we are done teleporting
         if (%closestMPBDist < %closestTelDist)
         {
            //reset the clients inventory status
            if (%client.needEquipment)
            {
               %result = AIFindClosestInventory(%client, 0);
	       %closestInv = getWord(%result, 0);
               %client.invToUse = %closestInv;
            }
            %task.useTeleport = false;
            return;
         }

         if (%closestTelDist < 2)
            %client.pressJump();
         else
         {
            %client.stepMove(%closestTel.getWorldBoxCenter(), 0.25);
            return;
         }
      }
      else
         %task.useTeleport = false;
   }
   //--------------------------------------------------- monitor telporter - end -

   //set the target object
   %outOfStuff = (AIAttackOutofAmmo(%client));
   if (isObject(%task.targetObject) && %task.targetObject.getDamageState() !$= "Destroyed" && !%outOfStuff)
   {
      %dist = VectorDist(%task.targetObject.position, %client.player.position);
      if (%dist < 15)
      {
         if (%task.targetObject.getDataBlock().getName() $= "StationVehicle")
            %client.setDangerLocation(%task.targetObject.position, 20);
         else if (%task.targetObject.getDataBlock().getName() $= "MPBTeleporter")
            %client.setDangerLocation(%task.targetObject.position, 20);
      }
      //in case lazy people don't want to move aiobjective marker
      else if (VectorDist(%task.targetObject.getWorldBoxCenter(), %task.location) < 4)
      {
         if (! %client.targetInRange())
            %client.stepMove(%task.targetObject.getWorldBoxCenter(), 8.0);
      }
      else
         %client.stepMove(%task.location);

      %targetLOS = "false";
      %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::ForceFieldObjectType;
      %targetLOS = !containerRayCast(%client.player.getMuzzlePoint($WeaponSlot), %task.targetObject.getWorldBoxCenter(), %mask, 0);

      if (%targetLOS && %dist > 40 && %dist < 300)
      {
         %client.setTargetObject(%task.targetObject, 300, "Mortar");
         //echo("AIMDestroyObject::monitor - Should be set to MDestroy object here");
      }
      else if (%targetLOS && %dist < 40)
      {
         %client.setTargetObject(%task.targetObject, 100, "Destroy");
         //echo("AIMDestroyObject::monitor - Should be set to MDestroy object here");
      }
      else
      {
          %client.setTargetObject(-1);
          %client.stepMove(%task.location);
      } 
   }
   else
   {
      %client.setTargetObject(-1);

      //aim at the floor if we need to repair self so we don't repair enemy equipment
      if (%client.player.getDamagePercent() > 0 && %client.player.getInventory(RepairPack) > 0)
      {
         %pos = %client.player.getWorldBoxCenter();
         %posX = getWord(%pos, 0);
         %posY = getWord(%pos, 1);
         %posZ = getWord(%pos, 2) - 1;
         %pos = %posX SPC %posY SPC %posZ;
         %client.aimAt(%pos, 500);
      }

      
      //if this task is the objective task, choose a new objective
      if (%task == %client.objectiveTask)
      {
         AIUnassignClient(%client);
         Game.AIChooseGameObjective(%client);
      }
   }
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ AIO Missile Object +++
//                                                                            - Lagg... 4-12-2003

function AIOMissileObject::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   // if were playing CnH, check who owns this
   if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden() || %this.targetObjectId.team == %client.team)
      return 0;

   //don't attack team 0 stuffs
   if (%this.targetObjectId.team <= 0)
      return 0;      
   
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   //lets check the range on this one
   if (vectorDist(%client.Player.getWorldBoxCenter(), %this.getWorldBoxCenter()) < 50)
      return 0;
    
   //no need to attack if the object is already destroyed
   if (!isObject(%this.targetObjectId) || %this.targetObjectId.getDamageState() $= "Destroyed")
      return 0;
   else
	{
		//if this bot is linked to a human who has issued this command, up the weight
		if (%this.issuedByClientId == %client.controlByHuman)
		{
			//make sure we have the potential to reach the minWeight
			if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
			{
				if ($AIWeightHumanIssuedCommand < %minWeight)
					return 0;
				else
					%weight = $AIWeightHumanIssuedCommand;
			}
			else
			{
				// calculate the default...
				%weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
				if (%weight < $AIWeightHumanIssuedCommand)
					%weight = $AIWeightHumanIssuedCommand;
			}
		}
		else
		{
			//make sure we have the potential to reach the minWeight
			if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
				return 0;

			// calculate the default...
			%weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
		}

      //if we are playing DnD, check if objective was completed already, if not give some extra :) - Lagg... 3-29-2004
      if (Game.class $= "DnDGame")
      {
         if (%this.targetObjectId.objectiveCompleted)
            %weight -= 400;
         else
            %weight += 200;
      }

      return %weight;
   }
}

function AIOMissileObject::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIMissileObject);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIOMissileObject::unassignClient(%this, %client)
{
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//----------------------------------------------------------------------------------------------- AI Missile Object ---
//                                                                            - Lagg... 4-12-2003

function AIMissileObject::initFromObjective(%task, %objective, %client)
{
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;
   %task.location = %objective.location;
   %task.equipment = %objective.equipment;
	%task.buyEquipmentSet = %objective.buyEquipmentSet;
	%task.desiredEquipment = %objective.desiredEquipment;
	%task.issuedByClient = %objective.issuedByClientId;

	//initialize other task vars
	%task.sendMsg = true;
	%task.sendMsgTime = 0;
        %task.useTeleport = false;
}

function AIMissileObject::assume(%task, %client)
{
   %task.setWeightFreq(15);
   %task.setMonitorFreq(5);
   %task.needToRangeTime = getSimTime() + 20000;
   %client.needEquipment = AINeedEquipment(%task.equipment, %client);

	//even if we don't *need* equipemnt, see if we should buy some... 
	if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
	{
		//see if we could benefit from inventory
		%needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
		%result = AIFindClosestInventory(%client, %needArmor);
		%closestInv = getWord(%result, 0);
		%closestDist = getWord(%result, 1);
		if (AINeedEquipment(%task.desiredEquipment, %client) && %closestInv > 0)
		{
			//find where we are
			%clientPos = %client.player.getWorldBoxCenter();
			%objPos = %task.targetObject.getWorldBoxCenter();
			%distToObject = %client.getPathDistance(%objPos);
			
			if (%distToObject < 0 || %closestDist < 100)//%distToObject)-modified - Lagg...
				%client.needEquipment = true;
		}
	}

	//mark the current time for the buy inventory state machine
	%task.buyInvTime = getSimTime();

   //--------------------------------------------------- assume telporter - start -
   //if the MPB Teleporter is online - Lagg... 9-30-2003

   //first check for deployed MPB
   %result = AIFindDeployedMPB(%client);//new function in aiVehicle.cs
   %closestMPB = getWord(%result, 0);
   %closestMPBDist = getWord(%result, 1);
      
   //next find the teleporter
   %result = SweepForTeleporters(%client);//new function in aiVehicle.cs
   %closestTel = getWord(%result, 0);
   %closestTelDist = getWord(%result, 1);
      
   //now check for what would be closer - Lagg... 10-1-2003
   if (%closestMPB > 0 && %closestTel > 0)
   {
      %disToTarg = %client.getPathDistance(%task.location);
      %mpbDisToTarg = vectorDist(%closestMPB.getTransform(), %task.location);
         
      if (%closestTelDist < %closestMPBDist && %closestMPBDist < %disToTarg && %mpbDisToTarg < %disToTarg)
         %task.useTeleport = true;//new - Lagg... 10-1-2003
      else
         %task.useTeleport = false;            
   }
   //--------------------------------------------------- assume telporter - end -
}

function AIMissileObject::retire(%task, %client)
{
   %client.setTargetObject(-1);
}

function AIMissileObject::weight(%task, %client)
{
        if (VectorDist(%task.location, %client.Player.getWorldBoxCenter) < 41)
        {
           echo(" Too Close to missile object set baseweight (0)");
           %task.baseWeight = 0;
        }
	
	else if (%task == %client.objectiveTask)
		%task.baseWeight = %client.objectiveWeight;

   //let the monitor decide when to stop attacking
   %task.setWeight(%task.baseWeight);
}

function AIMissileObject::monitor(%task, %client)
{
   //first, buy the equipment
   if (%client.needEquipment)
   {
	   %task.setMonitorFreq(5);
		if (%task.equipment !$= "")
			%equipmentList = %task.equipment;
		else
			%equipmentList = %task.desiredEquipment;
      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
      {
         ////force a nervous reaction every 15 sec - Lagg...
         //if (getSimTime() - %task.buyInvTime > 15000)
         //{
            //%client.setDangerLocation(%client.player.getWorldBoxCenter(), 20);
           // %task.buyInvTime = getSimTime();
           // %client.buyInvTime = %task.buyInvTime;
         //}
         return;
      }
      else if (%result $= "Finished")
		{
		   %task.setMonitorFreq(15);
			%client.needEquipment = false;
		}
		else if (%result $= "Failed")
		{
	      //if this task is the objective task, choose a new objective
	      if (%task == %client.objectiveTask)
	      {
	         AIUnassignClient(%client);
	         Game.AIChooseGameObjective(%client);
	      }
			return;
		}
   }
	//if we made it past the inventory buying, reset the inv time
	%task.buyInvTime = getSimTime();
   
	//chat
	if (%task.sendMsg)
	{
		if (%task.sendMsgTime == 0)
			%task.sendMsgTime = getSimTime();
		else if (getSimTime() - %task.sendMsgTime > 7000)
		{
			%task.sendMsg = false;
		   if (%client.isAIControlled())
			{
				if (%task.chat !$= "")
				{
					%chatMsg = getWord(%task.chat, 0);
					%chatTemplate = getWord(%task.chat, 1);
					if (%chatTemplate !$= "")
						AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
					else
						AIMessageThread(%task.chat, %client, -1);
				}
				else if (%task.targetObject > 0)
				{
					%type = %task.targetObject.getDataBlock().getName();
					if (%type $= "GeneratorLarge")
						AIMessageThreadTemplate("AttackBase", "ChatSelfAttackGenerator", %client, -1);
					else if (%type $= "SensorLargePulse")
						AIMessageThreadTemplate("AttackBase", "ChatSelfAttackSensors", %client, -1);
					else if (%type $= "SensorMediumPulse")
						AIMessageThreadTemplate("AttackBase", "ChatSelfAttackSensors", %client, -1);
					else if (%type $= "TurretBaseLarge")
						AIMessageThreadTemplate("AttackBase", "ChatSelfAttackTurrets", %client, -1);
					else if (%type $= "StationVehicle")
						AIMessageThreadTemplate("AttackBase", "ChatSelfAttackVehicle", %client, -1);
				}
			}
		}
	}

   //--------------------------------------------------- monitor telporter - start -
   //are we using teleporter
   if (%task.useTeleport)
   {
      %result = AIFindDeployedMPB(%client);//new function in aiVehicle.cs
      %closestMPB = getWord(%result, 0);
      %closestMPBDist = getWord(%result, 1);
      %result = SweepForTeleporters(%client);//new function in aiVehicle.cs
      %closestTel = getWord(%result, 0);
      %closestTelDist = getWord(%result, 1);
      if (%closestMPB > 0 && %closestTel > 0)
      {
         //we are done teleporting
         if (%closestMPBDist < %closestTelDist)
         {
            //reset the clients inventory status
            if (%client.needEquipment)
            {
               %result = AIFindClosestInventory(%client, 0);
	       %closestInv = getWord(%result, 0);
               %client.invToUse = %closestInv;
            }
            %task.useTeleport = false;
            return;
         }

         if (%closestTelDist < 2)
            %client.pressJump();
         else
         {
            %client.stepMove(%closestTel.getWorldBoxCenter(), 0.25);
            return;
         }
      }
      else
         %task.useTeleport = false;
   }
   //--------------------------------------------------- monitor telporter - end -

   //set the target object
   if (isObject(%task.targetObject) && %task.targetObject.getDamageState() !$= "Destroyed")
   {
      //make sure we still have equipment
		%client.needEquipment = AINeedEquipment(%task.equipment, %client);
      if (%client.needEquipment)
      {
         //if this task is the objective task, choose a new objective
         if (%task == %client.objectiveTask)
			{
            AIUnassignClient(%client);
	         Game.AIChooseGameObjective(%client);
				return;
			}
      }

      %clientPos = %client.player.getWorldBoxCenter();
      %targetPos = %task.targetObject.getWorldBoxCenter();
      %distance = %client.getPathDistance(%targetPos);
		if (%distance < 0)
			%distance = 32767;

      //next move to within 300 
      if (%distance > 300)
      {
         %client.setTargetObject(-1);
         %client.stepMove(%task.targetObject.getWorldBoxCenter(), 15);
      }
      else
      {
         //move to LOS location to objective marker(not to target)
         //(that makes the LOS location adjustable!)
         %firePos = %client.getLOSLocation(%task.location, 50, 290);
         %client.stepMove(%firePos);
         //check for LOS
         %missileLOS = "false";
         %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType | $TypeMasks::ForceFieldObjectType;
         %missileLOS = !containerRayCast(%client.player.getWorldBoxCenter(), %task.location, %mask, 0);
         %inRange = %client.getPathDistance(%task.location);

         //modified here in case bot gets stuck trying to get LOS to target - Lagg... - 4-21-2003
	 %currentTime = getSimTime();
	 if (%currentTime > %task.needToRangeTime)
	 {
	    //force a rangeObject every 20 seconds...
            %task.needToRangeTime = %currentTime + 20000;
            %client.setDangerLocation(%firePos, 30);
            return;

         }
      
         if ((%inRange > 49) && (%inRange < 291) && %missileLOS)
         {
            %client.setTargetObject(%task.targetObject, 300, "Missile");
	    //dissolve the human control link
	    if (%task == %client.objectiveTask)
               {
	          aiReleaseHumanControl(%client.controlByHuman, %client);
                  %client.stop();
               }
            return;
         }
         else if ((%inRange > 49) && (%inRange < 291) && !%missileLOS && (%client.getPathDistance(%client.player.getWorldBoxCenter(), %firePos) < 10))
         {     
            //if this task is the objective task, choose a new objective
            if (%task == %client.objectiveTask)
            {
               %task.baseWeight = 0;
               %task.setWeight(%task.baseWeight);
               return;
            }
         }
         else if (%client.getStepStatus() !$= "Finished")
         {
            %client.stepMove(%firePos);
            return;
         }
      }     
   }
   else
   {     
      //if this task is the objective task, choose a new objective
      if (%task == %client.objectiveTask)
      {
         %client.setTargetObject(-1);
         %client.stop();
         AIUnassignClient(%client);
         Game.AIChooseGameObjective(%client);
      }
   }
}

//------------------------------------------------------------------------------------------------- AIO Mortar Object ---
//Modified so bot gets LOS to target before firing. Fixed a lot of stuff here.          - Lagg...

function AIOMortarObject::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   // if were playing CnH, check who owns this
   if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden() || %this.targetObjectId.team == %client.team)
      return 0;
   
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;
      
   //no need to attack if the object is already destroyed
   if (%this.targetObjectId.getDamageState() $= "Destroyed")
      return 0;
   else
   {
      //if this bot is linked to a human who has issued this command, up the weight
      if (%this.issuedByClientId == %client.controlByHuman)
      {
         //make sure we have the potential to reach the minWeight
	 if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
	 {
	    if ($AIWeightHumanIssuedCommand < %minWeight)
	       return 0;
	    else
	       %weight = $AIWeightHumanIssuedCommand;
	 }
	 else
	 {
	    // calculate the default...
	    %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
	    if (%weight < $AIWeightHumanIssuedCommand)
	       %weight = $AIWeightHumanIssuedCommand;
	 }
      }
      else
      {
         //error("AIOMortarObject::weight - %minWeight = " @ %minWeight);//---------------------------------------------- NOTES HERE LAGG ***
         //make sure we have the potential to reach the minWeight
         if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
	    return 0;

         //check equipment requirement / if we are too far from inventory forget it
	 %needsEquipment = AINeedEquipment(%this.equipment, %client);
         if (%needsEquipment)
         {
            %needArmor = AIMustUseRegularInvStation(%this.equipment, %client);
            %result = AIFindClosestInventory(%client, %needArmor);
            %closestInv = getWord(%result, 0);
            %closestDist = getWord(%result, 1);

            if (%closestDist > 250 || (%needArmor && %closestDist > 150))
               return 0;
         }  

	 // calculate the default...
	 %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
      }

      //if we are playing DnD, check if objective was completed already, if not give some extra :) - Lagg... 3-29-2004
      if (Game.class $= "DnDGame")
      {
         if (%this.targetObjectId.objectiveCompleted)
            %weight -= 400;
         else
            %weight += 200;
      }

      if (%this.clientLevel1 > 0  && %level == 1 && %this.clientLevel1 == %client)
      {
         if (VectorDist(%client.player.getWorldBoxCenter(), %this.targetObjectId.getWorldBoxCenter()) < 300)
         {

            //check for LOS
            %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType |
              $TypeMasks::ForceFieldObjectType | $TypeMasks::VehicleObjectType;
            %hasLOS = !containerRayCast(%client.player.getMuzzlePoint($WeaponSlot), %this.targetObjectId.getWorldBoxCenter(), %mask, 0);

            if (%hasLOS)
               %weight += 200;
         }
      }

      //if we are to close, reconsider the task
      if (VectorDist(%client.player.getWorldBoxCenter(), %this.targetObjectId.getWorldBoxCenter()) < 60)
         %weight -= 400;

      return %weight;
   }
}

function AIOMortarObject::assignClient(%this, %client)
{
   //error("AIOMortarObject::assignClient - " @ getTaggedString(%client.name));

   %client.objectiveTask = %client.addTask(AIMortarObject);
   %client.objectiveTask.initFromObjective(%this, %client);
   
   ////create the escort objective (require a targeting laser in this case...)
   //%client.escort = new AIObjective(AIOEscortPlayer)
                     //{
						      //dataBlock = "AIObjectiveMarker";
                        //weightLevel1 = $AIWeightEscortOffense[1];
                        //weightLevel2 = $AIWeightEscortOffense[2];
                        //description = "Escort " @ getTaggedString(%client.name);
                        //targetClientId = %client;
                        //offense = true;
                        //equipment = "Plasma PlasmaAmmo TargetingLaser";
								//buyEquipmentSet = "MediumMissileSet";
                     //};
   //MissionCleanup.add(%client.escort);

   ////if is siege game we have to do this right - Lagg... 11-3-2003
   //if (Game.class $= "SiegeGame")
   //{
      ////the objectives in $ObjectiveQ[1] are all offense objectives, $ObjectiveQ[2] has the defensive ones..
      //if (%client.team == game.offenseTeam)
         //$ObjectiveQ[1].add(%client.escort);
      //else
         //$ObjectiveQ[2].add(%client.escort);
   //}
   //else
      //$ObjectiveQ[%client.team].add(%client.escort);
}

function AIOMortarObject::unassignClient(%this, %client)
{
   //error("AIOMortarObject::unassignClient - " @ getTaggedString(%client.name));

   ////kill the escort objective
   //if (%client.escort)
   //{
      //clearObjectiveFromTable(%client.escort);//new function below to stop objective spam - Lagg... 1-27-2004
      //AIClearObjective(%client.escort);
      //%client.escort.delete();
      //%client.escort = "";
   //}   
 
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//------------------------------------------------------------------------

function AIMortarObject::initFromObjective(%task, %objective, %client)
{
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;
   %task.equipment = %objective.equipment;
   %task.buyEquipmentSet = %objective.buyEquipmentSet;
   %task.desiredEquipment = %objective.desiredEquipment;
   %task.issuedByClient = %objective.issuedByClientId;
   %task.mode = %task.targetObject.getDataBlock().getName();

   %task.sendMsgTime = 0;
   %task.sendMsg = true;

   %task.useTeleport = false;
}

function AIMortarObject::assume(%task, %client)
{
   %task.setWeightFreq(30);
   %task.setMonitorFreq(30);
   %task.state = moveToRange;
   %task.waitForTargetter = true;
   %task.celebrate = false;
   %task.waitTimerMS = 0;
   %task.targetAcquired = false;
   %task.sayAcquired = true;
   %client.needEquipment = AINeedEquipment(%task.equipment, %client);

   //even if we don't *need* equipemnt, see if we should buy some... 
   if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
   {
      //see if we could benefit from inventory
      %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      %closestDist = getWord(%result, 1);
      if (AINeedEquipment(%task.desiredEquipment, %client) && %closestInv > 0)
      {
         //find where we are
	 %clientPos = %client.player.getWorldBoxCenter();
	 %objPos = %task.targetObject.getWorldBoxCenter();
	 %distToObject = %client.getPathDistance(%objPos);
			
	 if (%distToObject < 0 || %closestDist < 100)//%distToObject)
	    %client.needEquipment = true;
      }
   }

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();

   //--------------------------------------------------- assume telporter - start -
   //if the MPB Teleporter is online - Lagg... 9-30-2003

   //first check for deployed MPB
   %result = AIFindDeployedMPB(%client);//new function in aiVehicle.cs
   %closestMPB = getWord(%result, 0);
   %closestMPBDist = getWord(%result, 1);
      
   //next find the teleporter
   %result = SweepForTeleporters(%client);//new function in aiVehicle.cs
   %closestTel = getWord(%result, 0);
   %closestTelDist = getWord(%result, 1);
      
   //now check for what would be closer - Lagg... 10-1-2003
   if (%closestMPB > 0 && %closestTel > 0)
   {
      %disToTarg = %client.getPathDistance(%task.location);
      %mpbDisToTarg = vectorDist(%closestMPB.getTransform(), %task.location);
         
      if (%closestTelDist < %closestMPBDist && %closestMPBDist < %disToTarg && %mpbDisToTarg < %disToTarg)
         %task.useTeleport = true;//new - Lagg... 10-1-2003
      else
         %task.useTeleport = false;             
   }
   //--------------------------------------------------- assume telporter - end -
}

function AIMortarObject::retire(%task, %client)
{
   %client.setTargetObject(-1);
   
   //remove the associated lazeObjective
   if (%task.targetterObjective)
   {
      clearObjectiveFromTable(%task.targetterObjective);//new function below to stop objective spam - Lagg... 1-27-2004
      AIClearObjective(%task.targetterObjective);
      %task.targetterObjective.delete();
      %task.targetterObjective = "";
   }
}

function AIMortarObject::weight(%task, %client)
{
   //update the task weight
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;

   //let the monitor decide when to stop mortaring
   %task.setWeight(%task.baseWeight);
}

function AIMortarObject::monitor(%task, %client)
{
   //first, buy the equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;

      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
         return;
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(30);
	 %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
         //if this task is the objective task, choose a new objective
	 if (%task == %client.objectiveTask)
	 {
	    AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	 }
	 return;
      }
   }

   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();
   
   //chat
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
	 if (%client.isAIControlled())
	 {
	    if (%task.chat !$= "")
	    {
	       %chatMsg = getWord(%task.chat, 0);
	       %chatTemplate = getWord(%task.chat, 1);
	       if (%chatTemplate !$= "")
	          AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
	       else
		  AIMessageThread(%task.chat, %client, -1);
	    }
	    else if (%task.targetObject > 0)
	    {
	       %type = %task.targetObject.getDataBlock().getName();
	       if (%type $= "GeneratorLarge")
	          AIMessageThreadTemplate("AttackBase", "ChatSelfAttackGenerator", %client, -1);
	       else if (%type $= "SensorLargePulse")
	          AIMessageThreadTemplate("AttackBase", "ChatSelfAttackSensors", %client, -1);
	       else if (%type $= "SensorMediumPulse")
	          AIMessageThreadTemplate("AttackBase", "ChatSelfAttackSensors", %client, -1);
	       else if (%type $= "TurretBaseLarge")
		  AIMessageThreadTemplate("AttackBase", "ChatSelfAttackTurrets", %client, -1);
	       else if (%type $= "StationVehicle")
		  AIMessageThreadTemplate("AttackBase", "ChatSelfAttackVehicle", %client, -1);
	    }
         }
      }
   }

   //--------------------------------------------------- monitor telporter - start -
   //are we using teleporter
   if (%task.useTeleport)
   {
      %result = AIFindDeployedMPB(%client);//new function in aiVehicle.cs
      %closestMPB = getWord(%result, 0);
      %closestMPBDist = getWord(%result, 1);
      %result = SweepForTeleporters(%client);//new function in aiVehicle.cs
      %closestTel = getWord(%result, 0);
      %closestTelDist = getWord(%result, 1);
      if (%closestMPB > 0 && %closestTel > 0)
      {
         //we are done teleporting
         if (%closestMPBDist < %closestTelDist)
         {
            //reset the clients inventory status
            if (%client.needEquipment)
            {
               %result = AIFindClosestInventory(%client, 0);
	       %closestInv = getWord(%result, 0);
               %client.invToUse = %closestInv;
            }
            %task.useTeleport = false;
            return;
         }

         if (%closestTelDist < 2)
            %client.pressJump();
         else
         {
            %client.stepMove(%closestTel.getWorldBoxCenter(), 0.25);
            return;
         }
      }
      else
         %task.useTeleport = false;
   }
   //--------------------------------------------------- monitor telporter - end -


   //make sure we still have something to destroy
   if (isObject(%task.targetObject) && %task.targetObject.getDamageState() !$= "Destroyed")
   {
      %clientPos = %client.player.getWorldBoxCenter();
      %targetPos = %task.targetObject.getWorldBoxCenter();
      %distance = %client.getPathDistance(%targetPos);
      if (%distance < 0)
         %distance = 32767;
      
      //make sure we still have equipment
      %client.needEquipment = AINeedEquipment(%task.equipment, %client);
      if (%client.needEquipment)
      {
         //if this task is the objective task, choose a new objective
         if (%task == %client.objectiveTask)
	 {
            AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	    return;
	 }
      }
      
      //next move to within 300 
      else if (%distance > 300)
      {
         %client.setTargetObject(-1);
         %client.stepMove(%task.targetObject.getWorldBoxCenter(), 15);
      }
      
      //now start ask for someone to laze the target, and start a 20 sec timer
      else if (%task.waitForTargetter)
      {
         //see if we've started the timer
         if (%task.waitTimerMS == 0)
         {
	    //range the object
	    %client.stepRangeObject(%task.targetObject, "BasicTargeter", 150, 300);

            //now ask for a targeter...
	    %targetType = %task.targetObject.getDataBlock().getName();
	    if (%targetType $= "TurretBaseLarge")
	       AIMessageThread("ChatCmdTargetTurret", %client, -1);
	    else if (%targetType $= "SensorLargePulse")
	       AIMessageThread("ChatCmdTargetSensors", %client, -1);
	    else if (%targetType $= "SensorMediumPulse")
	       AIMessageThread("ChatCmdTargetSensors", %client, -1);
	    else
	       AIMessageThread("ChatTargetNeed", %client, -1);

            %task.waitTimerMS = getSimTime();

            //create the targetter objective
	    if (! %task.targetterObjective)
	    {
	       %task.targetterObjective = new AIObjective(AIOLazeObject)
	               {
	                       dataBlock = "AIObjectiveMarker";
	                       weightLevel1 = $AIWeightLazeObject[1];
	                       weightLevel2 = $AIWeightLazeObject[2];
	                       description = "Laze the " @ %task.targetObject.getName();
	                       targetObjectId = %task.targetObject;
			       issuedByClientId = %client;
	                       offense = true;
	                       equipment = "TargetingLaser";
	               };
	       MissionCleanup.add(%task.targetterObjective);

	       //if this is siege game we have to do stuff this right - Lagg... 11-3-2003
               if (Game.class $= "SiegeGame")
               {
                  //the objectives in $ObjectiveQ[1] are all offense objectives, $ObjectiveQ[2] defense
                  if (%client.team == game.offenseTeam)
                     $ObjectiveQ[1].add(%task.targetterObjective);
                  else
                     $ObjectiveQ[2].add(%task.targetterObjective);
               }
               else
                  $ObjectiveQ[%client.team].add(%task.targetterObjective);
            }

	    %task.targetterObjective.lastLazedTime = 0;

	    ////remove the escort (want a targetter instead)
	    //if (%client.escort)
	    //{
               //clearObjectiveFromTable(%client.escort);//new function below to stop objective spam - Lagg... 1-27-2004
	       //AIClearObjective(%client.escort);
	       //%client.escort.delete();
	       //%client.escort = "";
	    //}
         }									
         else
         {
            %elapsedTime = getSimTime() - %task.waitTimerMS;
	    if (%task.targetterObjective.group > 0)
	       %targetter = %task.targetterObjective.group.clientLevel[1];
	    else
	       %targetter = %task.targetterObjective.clientLevel[1];

	    //see if we can find a target near our objective
	    %task.targetAcquired = false;
	    %numTargets = ServerTargetSet.getCount();
	    for (%i = 0; %i < %numTargets; %i++)
	    {
	       %targ = ServerTargetSet.getObject(%i);
	       %targDist = VectorDist(%targ.getWorldBoxCenter(), %task.targetObject.getWorldBoxCenter());
	       if (%targDist < 300)
	       {
	          %task.targetAcquired = true;
		  break;
	       }
	    }

	    if (%task.targetAcquired)
            {
               %task.waitForTargetter = false;
	       %task.waitTimerMS = 0;
	       %task.celebrate = true;
	       %task.sayAcquired = false;
	       AIMessageThread("ChatTargetAcquired", %client, -1);
            }

	    //else see if we've run out of time
	    else if ((! %targetter || ! %targetter.isAIControlled()) && %elapsedTime > 10000)
	    {
               %task.waitForTargetter = false;
	       %task.waitTimerMS = 0;
	       %task.celebrate = true;
	    }  
         }
      }
      
      //now we should finally be attacking with or without a targetter
      //eventually, the target will be destroyed, or we'll run out of ammo...
      else
      {
         //dissolve the human control link
	 if (%task == %client.objectiveTask)
	    aiReleaseHumanControl(%client.controlByHuman, %client);

	 //see if we didn't acquired a spotter along the way
	 if (%task.targetterObjective.group > 0)
	    %targetter = %task.targetterObjective.group.clientLevel[1];
	 else
	    %targetter = %task.targetterObjective.clientLevel[1];

	 if (! %task.targetAcquired && AIClientIsAlive(%targetter) && %targetter.isAIControlled())
	 {
	    %client.setTargetObject(-1);
            %task.waitForTargetter = true;
	 }
	 else
	 {
	    //see if we can find a target near our objective
	    if (! %task.targetAcquired)
	    {
	       %numTargets = ServerTargetSet.getCount();
	       for (%i = 0; %i < %numTargets; %i++)
	       {
	          %targ = ServerTargetSet.getObject(%i);
		  %targDist = VectorDist(%targ.getWorldBoxCenter(), %task.targetObject.getWorldBoxCenter());
		  if (%targDist < 300)
		  {
		     %task.targetAcquired = true;
		     break;
		  }
	       }
	       //see if we found a target (must be by a human)
	       if (%task.targetAcquired && %task.sayAcquired)
	       {
	          %task.sayAcquired = false;
		  AIMessageThread("ChatTargetAcquired", %client, -1);
	       }
	    }
				
	    //set the target object, and keep attacking it
	    if (%client.getStepStatus() $= "Finished")
	       %client.setTargetObject(%task.targetObject, 300, "Mortar");
	    else
	       %client.setTargetObject(-1);
         }
      }
   }

   //the target must have been destroyed  :)
   else
   {
      //dissolve the human control link
      if (%task == %client.objectiveTask)
         aiReleaseHumanControl(%client.controlByHuman, %client);

      %client.setTargetObject(-1);
      %client.clearStep();
      %client.stop();

      if (%task.celebrate)
      {
         if (%task.waitTimerMS == 0)
	 {
	    //client animation "woohoo"!  :)
	    //choose the animation range
	    %minCel = 3;
	    %maxCel = 8;

	    //pick a random sound
	    if (getRandom() > 0.25)
	       %sound = "gbl.awesome";
	    else if (getRandom() > 0.5)
	       %sound = "gbl.thanks";
	    else if (getRandom() > 0.75)
	       %sound = "gbl.nice";
	    else
	       %sound = "gbl.rock";
	    %randTime = mFloor(getRandom() * 500) + 1;
	    schedule(%randTime, %client, "AIPlayAnimSound", %client, %task.targetObject.getWorldBoxCenter(), %sound, %minCel, %maxCel, 0);

	    //team message
	    AIMessageThread("ChatEnemyTurretsDestroyed", %client, -1);

	    //set the timer
            %task.waitTimerMS = getSimTime();
	 }

	 //else see if the celebration period is over
	 else if (getSimTime() - %task.waitTimerMS > 3000)
	    %task.celebrate = false;
      }
      else
      {
         //if this task is the objective task, choose a new objective
	 if (%task == %client.objectiveTask)
	 {
	    AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	 }
      }
   }
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ AIO Place Camera +++
// have to fix this so a repair object objective is created for the newly deployed thingy??
//rotate the objective marker, bot will place on the "Y" same as all other deploy objectives
//                                                                            - Lagg... 1-14-2003

function AIOPlaceCamera::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

	//make sure the deploy objective is valid
	//if (%this.isInvalid)
		//return 0;

	//first, make sure we haven't deployed too many...
 	if (%this.equipment $= "CameraGrenade")
        %maxAllowed = $TeamDeployableMax[DeployedCamera];
        else
                return 0;
   
	if ($TeamDeployedCount[%client.team, %this.equipment] >= %maxAllowed)
		return 0;

	//now make sure there are no other items in the way...
  	  InitContainerRadiusSearch(%this.location, 1, $TypeMasks::VehicleObjectType |
  	                                               $TypeMasks::MoveableObjectType |
  	                                               $TypeMasks::StaticShapeObjectType |
  	                                               $TypeMasks::TSStaticShapeObjectType |
  	                                               $TypeMasks::ForceFieldObjectType |
  	                                               $TypeMasks::ItemObjectType |
  	                                               $TypeMasks::PlayerObjectType | $TypeMasks::TurretObjectType);                   
	%objSearch = containerSearchNext();

	//make sure we're not invalidating the deploy location with the client's own player object
	if (%objSearch == %client.player)
		%objSearch = containerSearchNext();

	//did we find an object which would block deploying the equipment?
	if (isObject(%objSearch))
		return 0;

 	

	//check equipment requirement
	%needEquipment = AINeedEquipment(%this.equipment, %client);
	
        //if don't need equipment, see if we've past the "point of no return", and should continue regardless
 	if (! %needEquipment)
 	{
 		%needArmor = AIMustUseRegularInvStation(%this.equipment, %client);
 		%result = AIFindClosestInventory(%client, %needArmor);
 		%closestInv = getWord(%result, 0);
 		%closestDist = getWord(%result, 1);
 
 		//if we're too far from the inv to go back, or we're too close to the deploy location, force continue
 		if (%closestDist > 50 && VectorDist(%client.player.getWorldBoxCenter(), %task.location) < 150)
 		{
 			%weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
 			if (%weight < $AIWeightContinueDeploying)
 				%weight = $AIWeightContinueDeploying;
 			return %weight;
 		}
 	}

	//if this bot is linked to a human who has issued this command, up the weight
	if (%this.issuedByClientId == %client.controlByHuman)
	{
		//make sure we have the potential to reach the minWeight
		if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
		{
			if ($AIWeightHumanIssuedCommand < %minWeight)
				return 0;
			else
				%weight = $AIWeightHumanIssuedCommand;
		}
		else
		{
			// calculate the default...
			%weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
			if (%weight < $AIWeightHumanIssuedCommand)
				%weight = $AIWeightHumanIssuedCommand;
		}
	}
	else
	{
		//make sure we have the potential to reach the minWeight
		if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
			return 0;

		// calculate the default...
		%weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
	}

	return %weight;
}

function AIOPlaceCamera::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIPlaceCamera);
   %task = %client.objectiveTask;
   %task.initFromObjective(%this, %client);
}

function AIOPlaceCamera::unassignClient(%this, %client)
{
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//----------------------------------------------------------------------------------------------- AI Place Camera ---

function AIPlaceCamera::initFromObjective(%task, %objective, %client)
{
	//initialize the task vars from the objective
   %task.baseWeight = %client.objectiveWeight;
   %task.location = %objective.location;
   %task.equipment = %objective.equipment;
	%task.buyEquipmentSet = %objective.buyEquipmentSet;
	%task.desiredEquipment = %objective.desiredEquipment;
	%task.issuedByClient = %objective.issuedByClientId;
	%task.chat = %objective.chat;

	//initialize other task vars
	%task.sendMsg = true;
	%task.sendMsgTime = 0;

	//use the Y-axis of the rotation as the desired direction of deployement,
	//and calculate a walk to point 3 m behind the deploy point. 
	%task.deployDirection = MatrixMulVector("0 0 0 " @ getWords(%objective.getTransform(), 3, 6), "0 1 0");
	%task.deployDirection = VectorNormalize(%task.deployDirection);
}

function AIPlaceCamera::assume(%task, %client)
{
   %task.setWeightFreq(15);
   %task.setMonitorFreq(15);
	
	%client.needEquipment = AINeedEquipment(%task.equipment, %client);
   
   //mark the current time for the buy inventory state machine
	%task.buyInvTime = getSimTime();

	%task.passes = 0;
	%task.deployAttempts = 0;
	%task.checkObstructed = false;
	%task.waitMove = 0;
}

function AIPlaceCamera::retire(%task, %client)
{
}

function AIPlaceCamera::weight(%task, %client)
{
   //update the task weight
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;

   %task.setWeight(%task.baseWeight);
}

function AIPlaceCamera::monitor(%task, %client)
{
   //first, buy the equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;
      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
      {
         ////force a nervous reaction every 15 sec - Lagg...
         //if (getSimTime() - %task.buyInvTime > 15000)
         //{
           // %client.setDangerLocation(%client.player.getWorldBoxCenter(), 20);
            //%task.buyInvTime = getSimTime();
            //%client.buyInvTime = %task.buyInvTime;
         //}
         return;
      }
      else if (%result $= "Finished")
      {	
        %task.setMonitorFreq(30);
        %client.needEquipment = false;
        //if we made it past the inventory buying, reset the inv time
	   %task.buyInvTime = getSimTime();
      }
      else if (%result $= "Failed")
      {
         //if this task is the objective task, choose a new objective
	 if (%task == %client.objectiveTask)
	 {
	    AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	 }
         return;
      }
   }

   //make sure we still have equipment
   %client.needEquipment = AINeedEquipment(%task.equipment, %client);
   if (%client.needEquipment)
   {
      //if this task is the objective task, choose a new objective
      if (%task == %client.objectiveTask)
      {
         AIUnassignClient(%client);
	 Game.AIChooseGameObjective(%client);
	 return;
      }
   }

   //chat
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
         {
            if (%task.chat !$= "")
	    {
               %chatMsg = getWord(%task.chat, 0);
               %chatTemplate = getWord(%task.chat, 1);
	       if (%chatTemplate !$= "")
	          AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
	       else
		  AIMessageThread(%task.chat, %client, -1);
            }
         }
      }
   }

   //see if we're supposed to be engaging anyone...
   if (AIClientIsAlive(%client.shouldEngage))
   {
      %hasLOS = %client.hasLOSToClient(%client.shouldEngage);
      %losTime = %client.getClientLOSTime(%client.shouldEngage);
      if (%hasLOS || %losTime < 1000)
         %client.setEngageTarget(%client.shouldEngage);
      else
         %client.setEngageTarget(-1);
   }
   else
      %client.setEngageTarget(-1);

   //calculate the deployFromLocation
   %factor = -4;// * (3 - (%task.passes * 0.5));
   %task.deployFromLocation = VectorAdd(%task.location,VectorScale(%task.deployDirection, %factor));

   //see if we're within range of the deploy location
   %clLoc = %client.player.position;
   %distance = VectorDist(%clLoc, %task.deployFromLocation);
   %dist2D = VectorDist(%client.player.position, getWords(%task.deployFromLocation, 0, 1) SPC getWord(%client.player.position, 2));

   //set the aim when we get near the target...  this will be overwritten when we're actually trying to deploy
   if (%distance < 10 && %dist2D < 10)
      %client.aimAt(%task.location, 1000);

   if ((%client.pathDistRemaining(20) > %distance + 0.25) || %dist2D > 0.5)
   {
      %task.deployAttempts = 0;
      %task.checkObstructed = false;
      %task.waitMove = 0;
      %client.stepMove(%task.deployFromLocation, 0.25);
      %task.setMonitorFreq(15);
      return;
   }
   
   if (%task.deployAttempts < 1 && %task.passes < 1 && !AIClientIsAlive(%client.getEngageTarget()))
   {
		//dissolve the human control link
		if (%task == %client.objectiveTask)
			aiReleaseHumanControl(%client.controlByHuman, %client);

	   %task.setMonitorFreq(3);
      %client.stop();
		if (%task.deployAttempts == 0)
			%deployPoint = %task.location;
		else
	      %deployPoint = findTurretDeployPoint(%client, %task.location, %task.deployAttempts);
      if(%deployPoint !$= "")
      {
         // we have possible point
         %task.deployAttempts++;
         %client.aimAt(%deployPoint, 2000);

			//try to place the camera
			//%client.deployPack = true;
         %client.lastThrownObject = -1;
         %client.player.throwStrength = 2;
         %client.player.use(CameraGrenade);
         
         // check if camera deployed
         if (isObject(%client.lastDeployedObject))
			{
				//see if there's a "repairObject" objective for the newly deployed thingy...
				if (%task == %client.objectiveTask)
				{
					%deployedObject = %client.lastDeployedObject;

					//search the current objective group and search for a "repair Object" task...
					%objective = %client.objective;

					//delete any previously associated "AIORepairObject" objective
					if (isObject(%objective.repairObjective))
					{
						AIClearObjective(%objective.repairObjective);
						%objective.repairObjective.delete();
						%objective.repairObjective = "";
					}

					//add the repair objective
	            %objective.repairObjective = new AIObjective(AIORepairObject)
		                              {
											      dataBlock = "AIObjectiveMarker";
		                                 weightLevel1 = %objective.weightLevel1 - 100;
		                                 weightLevel2 = 0;
		                                 description = "Repair the Deployed Camera";
													targetObjectId = %deployedObject;
													issuedByClientId = %client;
		                                 offense = false;
													defense = true;
		                                 equipment = "RepairPack";
                                                 buyEquipmentSet = "LightRepairSet";
		                              };
					%objective.repairObjective.deployed = true;
					%objective.repairObjective.setTransform(%objective.getTransform());
					%objective.repairObjective.group = %objective.group;
			      MissionCleanup.add(%objective.repairObjective);
	            $ObjectiveQ[%client.team].add(%objective.repairObjective);

					//finally, unassign the client so he'll go do something else...
			      AIUnassignClient(%client);
					Game.AIChooseGameObjective(%client);
				}

				//finished
				return;
			}
      }
	}
	else if (!%task.checkObstructed)
	{
		%task.checkObstructed = true;

	   //see if anything is in our way
	   InitContainerRadiusSearch(%task.location, 4, $TypeMasks::MoveableObjectType | $TypeMasks::VehicleObjectType |
             $TypeMasks::PlayerObjectType);
	   %objSrch = containerSearchNext();
		if (%objSrch == %client.player)
		   %objSrch = containerSearchNext();
	   if (%objSrch)
			AIMessageThread("ChatMove", %client, -1);
	}
	else if (%task.waitMove < 5 && %task.passes < 1)
	{
		%task.waitMove++;

		//try another pass at deploying 
		if (%task.waitMove == 5)
		{
			%task.waitMove = 0;
			%task.passes++;
			%task.deployAttempts = 0;

			//see if we're *right* underneath the deploy point
			%deployDist2D = VectorDist(getWords(%client.player.position, 0, 1) @ "0", getWords(%task.location, 0, 1) @ "0");
			if (%deployDist2D < 0.25)
			{
				%client.pressjump();
				//%client.deployPack = true;
                                %client.player.throwStrength = 2;
	                        %client.player.use(CameraGrenade);

	         // check if pack deployed
	         //if(%client.player.getMountedImage($BackpackSlot) == 0)
				   // {
					//don't add a "repairObject" objective for ceiling turrets
					if (%task == %client.objectiveTask)
					{
						AIUnassignClient(%client);
						Game.AIChooseGameObjective(%client);
					}
				//}
			}
		}
	}
	else
	{
		//find a new assignment - and remove this one from the Queue
      if (%task == %client.objectiveTask)
      {
			%client.objective.isInvalid = false;
	      AIUnassignClient(%client);
			Game.AIChooseGameObjective(%client);
       }
	}
}

//--------------------------------------------------------------------------------------- AIO Repair Object ---
//if you have a problem with bot getting stuck navigating to repair any object,
//place objective marker in spot you want bot to stand for repair at about bots waist level.
//if there is no problem with bot getting stuck repairing this object set the AIORepairObject
//markers transform to the same exact transform as the Object (x, y, z) updated - 10-18-2003

//fixed spam error when trying to repair a deployed object in case it gets destroyed while
//in loop. also fixed bots finding repair pack if base is down :) ----------- Lagg... 3-26-2003

function AIORepairObject::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   // if were playing CnH, check who owns this
   if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden() || %this.targetObjectId.team != %client.team)
      return 0;      
   
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;
      
   //no need to repair if the object isn't in need
   if (!isObject(%this.targetObjectId) || %this.targetObjectId.getDamagePercent() <= 0)
      return 0;
   else
	{
		//if this bot is linked to a human who has issued this command, up the weight
		if (%this.issuedByClientId == %client.controlByHuman)
		{
			//make sure we have the potential to reach the minWeight
			if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
			{
				if ($AIWeightHumanIssuedCommand < %minWeight)
					return 0;
				else
					%weight = $AIWeightHumanIssuedCommand;
			}
			else
			{
				// calculate the default...
				%weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
				if (%weight < $AIWeightHumanIssuedCommand)
					%weight = $AIWeightHumanIssuedCommand;
			}
		}
		else
		{
			//make sure we have the potential to reach the minWeight
			if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
				return 0;

			// calculate the default...
			%weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
		}

		return %weight;
	}
}

function AIORepairObject::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIRepairObject);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIORepairObject::unassignClient(%this, %client)
{
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//----------------------------------------------------------------------------------------------- AI Repair Object ---

function AIRepairObject::initFromObjective(%task, %objective, %client)
{
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;
	//need to force this objective to only require a repair pack
   //%task.equipment = %objective.equipment;
   %task.equipment = "RepairPack";
	%task.buyEquipmentSet = %objective.buyEquipmentSet;
	%task.desiredEquipment = %objective.desiredEquipment;
	%task.issuedByClient = %objective.issuedByClientId;
        //%task.location = %objective.location;
        %task.location = %objective.position;
        
	%task.deployed = %objective.deployed;
	if (%task.deployed)
	{
                if (!isObject(%task.targetObject) && isObject(%objective.repairObjective))
		{
                    //if this task is the objective task, choose a new objective
                    if (%task == %client.objectiveTask)
		    {
                        AIUnassignClient(%client);
	                Game.AIChooseGameObjective(%client);			
		    }
                    return;                    
	        }
		%task.location = %objective.position;
		%task.deployDirection = MatrixMulVector("0 0 0 " @ getWords(%objective.getTransform(), 3, 6), "0 1 0");
		%task.deployDirection = VectorNormalize(%task.deployDirection);
	}
}

function AIRepairObject::assume(%task, %client)
{
   %task.setWeightFreq(15);
   %task.setMonitorFreq(15);
   %client.needEquipment = AINeedEquipment(%task.equipment, %client);
      
   //clear the target object, and range it
   %client.setTargetObject(-1);
	if (! %client.needEquipment)
	{
	    if (%task.deployed)
	    {
                if (!isObject(%task.targetObject) && isObject(%objective.repairObjective))
		{
                    //if this task is the objective task, choose a new objective
                    if (%task == %client.objectiveTask)
		    {
                        AIUnassignClient(%client);
	                Game.AIChooseGameObjective(%client);			
		    }
		    return;                    
	        }
		   %task.repairLocation = VectorAdd(%task.location,VectorScale(%task.deployDirection, -4.0));//was -4
			%client.stepMove(%task.repairLocation, 0.25);
	    }

            //if the objective marker was moved than we have a problemed target
            //and we move to the objective marker to fix bot getting stuck trying
            //to repair object - Lagg... 10-10-2003            
            else if (%task.targetObject.getWorldBoxCenter() == %task.location)
                %client.stepRangeObject(%task.targetObject, "DefaultRepairBeam", 2, 4);                    
            else
	        %client.stepMove(%task.location);
	}

	//mark the current time for the buy inventory state machine
	%task.buyInvTime = getSimTime();
	%task.needToRangeTime = 0;
	%task.pickupRepairPack = -1;
	%task.usingInv = false;

	//set a tag to help the repairPack.cs script fudge acquiring a target
	%client.repairObject = %task.targetObject;

        //--------------------------------------------------- assume telporter - start -
        //if the MPB Teleporter is online - Lagg... 9-30-2003

        //first check for deployed MPB
        %result = AIFindDeployedMPB(%client);//new function in aiVehicle.cs
        %closestMPB = getWord(%result, 0);
        %closestMPBDist = getWord(%result, 1);
      
        //next find the teleporter
        %result = SweepForTeleporters(%client);//new function in aiVehicle.cs
        %closestTel = getWord(%result, 0);
        %closestTelDist = getWord(%result, 1);
      
       //now check for what would be closer - Lagg... 10-1-2003
       //and check if a deployed is still there...
       if (isObject(%task.targetObject) && %closestMPB > 0 && %closestTel > 0)
       {
          %disToTarg = %client.getPathDistance(%task.targetObject.getTransform());
          %mpbDisToTarg = vectorDist(%closestMPB.getTransform(), %task.targetObject.getTransform());
         
          if (%closestTelDist < %closestMPBDist && %closestMPBDist < %disToTarg && %mpbDisToTarg < %disToTarg)
             %task.useTeleport = true;//new - Lagg... 10-1-2003
          else
             %task.useTeleport = false;             
       }
   //--------------------------------------------------- assume telporter - end -
}

function AIRepairObject::retire(%task, %client)
{
   %client.setTargetObject(-1);
	%client.repairObject = -1;
}

function AIRepairObject::weight(%task, %client)
{
        //update the task weight
	if (%task == %client.objectiveTask)
		%task.baseWeight = %client.objectiveWeight;

   //let the monitor decide when to stop repairing
   %task.setWeight(%task.baseWeight);
}

function AIRepairObject::monitor(%task, %client)
{
   //echo("AIRepairObject --- monitor");
   //first, buy the equipment
   if (%client.needEquipment)
   {
           %task.setMonitorFreq(5);

		//first, see if we still need a repair pack
		if (%client.player.getInventory(RepairPack) > 0)
		{
			%client.needEquipment = false;
		   %task.setMonitorFreq(15);

			//if this is to repair a deployed object, walk to the deploy point...
			if (%task.deployed)
			{
                           if (!isObject(%task.targetObject) && isObject(%objective.repairObjective))
		           {
                               %client.setTargetObject(-1);
                               //if this task is the objective task, choose a new objective
                               if (%task == %client.objectiveTask)
		               {
                                  //AIUnassignClient(%client);
	                          //Game.AIChooseGameObjective(%client);			
		               }
		               AIClearObjective(%objective.repairObjective);
		               %objective.repairObjective.delete();
		               %objective.repairObjective = "";
                               clearObjectiveFromTable(%objective.repairObjective);//new function below to stop objective spam - Lagg... 1-27-2004
                               echo("AIRepairObject - DOES THIS GET CALLED AT ALL it is an error you know init - is not an object so delete repairobjective");
                               return;                    
	                   }
			   %task.repairLocation = VectorAdd(%task.location,VectorScale(%task.deployDirection, -4.0));//was -4
				%client.stepMove(%task.repairLocation, 0.25);
			}
                        //if the objective marker was moved than we have a problemed target
                        //and we move to the objective marker to fix bot getting stuck trying
                        //to repair object - Lagg... 10-10-2003            
                        
                        if (%task.targetObject.getDataBlock().getName() $= "MobileBaseVehicle")
                           %client.stepRangeObject(%task.targetObject, "DefaultRepairBeam", 10, 12);          
                        //else if (%task.targetObject.getWorldBoxCenter() == %task.location)
                        else if (VectorDist(%task.targetObject.getWorldBoxCenter(), %task.location) < 1.0)
                           %client.stepRangeObject(%task.targetObject, "DefaultRepairBeam", 2, 4);
                        else
	                   %client.stepMove(%task.location, 0.25);
		}
		else
		{
			// check to see if there's a repair pack nearby
			%closestRepairPack = -1;
			%closestRepairDist = 32767;

			//search the AIItemSet for a repair pack (someone might have dropped one...)
			%itemCount = $AIItemSet.getCount();
			for (%i = 0; %i < %itemCount; %i++)
			{
				%item = $AIItemSet.getObject(%i);
				if (%item.getDataBlock().getName() $= "RepairPack" && !%item.isHidden())
				{
                                        %dist = %client.getPathDistance(%item.getWorldBoxCenter());
					if (%dist > 0 && %dist < %closestRepairDist)
					{
                                                %closestRepairPack = %item;
						%closestRepairDist = %dist;
					}
				}
			}

			//choose whether we're picking up the closest pack, or buying from an inv station...
                        //with all these errors by Dynamix it is a wonder the bots do anything - Lagg... 3-25-2003
			if ((isObject(%closestRepairPack) && %closestRepairPack != %task.pickupRepairPack) || (%task.buyInvTime != %client.buyInvTime))
                        {
                                %task.pickupRepairPack = %closestRepairPack;

				//initialize the inv buying
				%task.buyInvTime = getSimTime();
				AIBuyInventory(%client, "RepairPack", %task.buyEquipmentSet, %task.buyInvTime);

				//now decide which is closer
				if (isObject(%closestRepairPack))
				{
                                        //added a little here to check for power - Lagg... - 3-25-2003
					if (isObject(%client.invToUse) && (%client.invToUse.isEnabled()) && (%client.invToUse.isPowered()))
					{
                                                %dist = %client.getPathDistance(%client.invToUse.getWorldBoxCenter());
						if (%dist < %closestRepairDist)
							%task.usingInv = true;
						else
							%task.usingInv = false;
					}
					else
                                           %task.usingInv = false;
				}
				else
					%task.usingInv = true;
			}

			//now see if we found a closer repair pack
			if (!%task.usingInv && isObject(%task.pickupRepairPack))
			{
				%client.stepMove(%task.pickupRepairPack.position, 0.25);
				%distToPack = %client.getPathDistance(%task.pickupRepairPack.position);
				if (%distToPack < 10 && %client.player.getMountedImage($BackpackSlot) > 0)
					%client.player.throwPack();

				//and we're finished until we actually have a repair pack... should now! - Lagg...
				return;
			}
			else
			{
		                %result = AIBuyInventory(%client, "RepairPack", %task.buyEquipmentSet, %task.buyInvTime);
				if (%result $= "InProgress")
                                   return;
				else if (%result $= "Finished")
				{
					%client.needEquipment = false;
				        %task.setMonitorFreq(15);

                                        //if we have to repair the station that we are using get away from it
                                        %client.setDangerLocation(%client.invToUse.getWorldBoxCenter(), 20);

					//if this is to repair a deployed object, walk to the deploy point...
					if (%task.deployed)
					{ 	
					   %task.repairLocation = VectorAdd(%task.location,VectorScale(%task.deployDirection, -6.0));//was -4
						%client.stepMove(%task.repairLocation, 0.25);
					}
                                        //if the objective marker was moved than we have a problemed target
                                        //and we move to the objective marker to fix bot getting stuck trying
                                        //to repair object - Lagg... 10-10-2003

                                        else if (%task.targetObject.getDataBlock().getName() $= "MobileBaseVehicle")
                                           %client.stepRangeObject(%task.targetObject, "DefaultRepairBeam", 10, 12);         
                                        //else if (%task.targetObject.getWorldBoxCenter() == %task.location)
                                        else if (VectorDist(%task.targetObject.getWorldBoxCenter(), %task.location) < 1.0)
                                           %client.stepRangeObject(%task.targetObject, "DefaultRepairBeam", 2, 4);
                                        else
	                                   %client.stepMove(%task.location, 0.25);
				}
				else if (%result $= "Failed")
				{
			           //if this task is the objective task, choose a new objective
			           if (%task == %client.objectiveTask)
			           {
			              AIUnassignClient(%client);
			              Game.AIChooseGameObjective(%client);
			           }
				   return;
				}
			     }
		          }
   }
	//if we made it past the inventory buying, reset the inv time
	%task.buyInvTime = getSimTime();
   
	//chat
	if (%task.sendMsg)
	{
		if (%task.sendMsgTime == 0)
			%task.sendMsgTime = getSimTime();
		else if (getSimTime() - %task.sendMsgTime > 7000)
		{
			%task.sendMsg = false;
		   if (%client.isAIControlled())
			{
				if (%task.chat !$= "")
				{
					%chatMsg = getWord(%task.chat, 0);
					%chatTemplate = getWord(%task.chat, 1);
					if (%chatTemplate !$= "")
						AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
					else
						AIMessageThread(%task.chat, %client, -1);
				}
				else if (%task.targetObject > 0)
				{
					%type = %task.targetObject.getDataBlock().getName();
					if (%type $= "GeneratorLarge")
						AIMessageThreadTemplate("RepairBase", "ChatSelfRepairGenerator", %client, -1);
					else if (%type $= "StationVehicle")
						AIMessageThreadTemplate("RepairBase", "ChatSelfRepairVehicle", %client, -1);
					else if (%type $= "SensorLargePulse")
						AIMessageThreadTemplate("RepairBase", "ChatSelfRepairSensors", %client, -1);
					else if (%type $= "SensorMediumPulse")
						AIMessageThreadTemplate("RepairBase", "ChatSelfRepairSensors", %client, -1);
					else if (%type $= "TurretBaseLarge")
						AIMessageThreadTemplate("RepairBase", "ChatSelfRepairTurrets", %client, -1);
				}
			}
		}
	}

   //--------------------------------------------------- monitor telporter - start -
   //are we using teleporter
   if (%task.useTeleport)
   {
      %result = AIFindDeployedMPB(%client);//new function in aiVehicle.cs
      %closestMPB = getWord(%result, 0);
      %closestMPBDist = getWord(%result, 1);
      %result = SweepForTeleporters(%client);//new function in aiVehicle.cs
      %closestTel = getWord(%result, 0);
      %closestTelDist = getWord(%result, 1);
      if (%closestMPB > 0 && %closestTel > 0)
      {
         //we are done teleporting
         if (%closestMPBDist < %closestTelDist)
         {
            //reset the clients inventory status
            if (%client.needEquipment)
            {
               %result = AIFindClosestInventory(%client, 0);
	       %closestInv = getWord(%result, 0);
               %client.invToUse = %closestInv;
            }
            %task.useTeleport = false;
            return;
         }

         if (%closestTelDist < 2)
            %client.pressJump();
         else
         {
            %client.stepMove(%closestTel.getWorldBoxCenter(), 0.25);
            return;
         }
      }
      else
         %task.useTeleport = false;
   }
   //--------------------------------------------------- monitor telporter - end -

   //if deployable got killed before we get to it - Lagg...
   if (!isObject(%task.targetObject))
   {
      %client.setTargetObject(-1);

      //if this task is the objective task, choose a new objective
      if (%task == %client.objectiveTask)
      {
         AIUnassignClient(%client);
	 Game.AIChooseGameObjective(%client);
         return;
      }
   }

   if (%task.targetObject.getDamagePercent() > 0)
   {
      //make sure we still have equipment
      %client.needEquipment = AINeedEquipment(%task.equipment, %client);
      if (%client.needEquipment)
      {
         //if this task is the objective task, choose a new objective
         if (%task == %client.objectiveTask)
	 {
            AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	    return;
	 }
      }
      
		if (%task.deployed)
		{
			//see if we're within range of the deploy location
		   %clLoc = %client.player.position;
		   %distance = VectorDist(%clLoc, %task.repairLocation);
			%dist2D = VectorDist(%client.player.position, getWords(%task.repairLocation, 0, 1) SPC getWord(%client.player.position, 2));

			//set the aim when we get near the target...  this will be overwritten when we're actually trying to repair deployed
 			if (%distance < 10 && %dist2D < 10)
 		      %client.aimAt(%task.location, 1000);

			//see if we're at the deploy location
		   if ((%client.pathDistRemaining(20) > %distance + 0.25) || %dist2D > 0.3)
			{
	         %client.setTargetObject(-1);
		      %client.stepMove(%task.repairLocation, 0.25);
			}
			else
			{
				%client.stop();
                                %client.setTargetObject(%task.targetObject, 12, "Repair");//increased range - Lagg...
			}
		}
		else
		{
                        //modified here in case bot gets stuck trying to get to repair spot - Lagg... - 4-21-2003
			%currentTime = getSimTime();
			if (%currentTime > %task.needToRangeTime)
			{
				//force a rangeObject every 45 seconds...
                                %task.needToRangeTime = %currentTime + 45000;
				%client.setTargetObject(-1);
                                %client.setDangerLocation(%task.targetObject.position, 15);
                                //%client.player.cycleWeapon();
                                //if ( !%client.player.getImageTrigger( $BackpackSlot ) )
                                   //%client.player.setImageTrigger( $BackpackSlot, true );
                                
                                //if the objective marker was moved than we have a problemed target
                                //and we move to the objective marker to fix bot getting stuck trying
                                //to repair object - Lagg... 10-10-2003            
                                
                                if (%task.targetObject.getDataBlock().getName() $= "MobileBaseVehicle")
                                   %client.stepRangeObject(%task.targetObject, "DefaultRepairBeam", 10, 12);             
                                //else if (%task.targetObject.getWorldBoxCenter() == %task.location)
                                else if (VectorDist(%task.targetObject.getWorldBoxCenter(), %task.location) < 1.0)
                                   %client.stepRangeObject(%task.targetObject, "DefaultRepairBeam", 2, 4);
                                else
	                           %client.stepMove(%task.location, 0.25);
	 }

	 //if we've ranged the object, start repairing, else unset the object
	 else if (%client.getStepStatus() $= "Finished")
         {
            //if we are standing in the trigger of the target object, move a little
            if (%task.targetObject.getDataBlock().getName() $= "MobileBaseVehicle")
            {
               if (VectorDist(%task.targetObject.station.position, %client.player.position) < 2)
                  %client.setDangerLocation(%task.targetObject.station.position, 30);
            }
            else if (%task.targetObject.getDataBlock().getName() $= "StationInventory")
            {
               if (VectorDist(%task.targetObject.position, %client.player.position) < 2)
                  %client.setDangerLocation(%task.targetObject.position, 30);
            }
                        
	    //dissolve the human control link
	    if (%task == %client.objectiveTask)
	       aiReleaseHumanControl(%client.controlByHuman, %client);

	    %client.setTargetObject(%task.targetObject, 12, "Repair");
            //if (%task.targetObject.getDataBlock().getName() $= "MobileBaseVehicle")
            //echo("REPAIRING A MPB = " @ getTaggedString(%client.name));
         }
         else
            %client.setTargetObject(-1);
      }
   }
   else
   {
      %client.setTargetObject(-1);
      
      //if this task is the objective task, choose a new objective
      if (%task == %client.objectiveTask)
      {
         AIUnassignClient(%client);
         Game.AIChooseGameObjective(%client);
      }
   }
}

//--------------------------------------------------------------------------------------------- AIO Touch Object ---
//Heavily modified Lagg... 5-17-2005

function AIOTouchObject::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
      return 0;

   switch$ (%this.mode)
   {
      case "TouchFlipFlop":
         if(%this.targetObjectId.team == %client.team || %this.targetObjectId.isHidden())
	    return 0;

         //don't even try to touch a flip flop surrounded by a forcefield - Lagg...
         if ($CurrentMissionType $= "Siege")
         {
            InitContainerRadiusSearch(%this.targetObjectId.position, 14, $TypeMasks::ForceFieldObjectType);
            %objSearch = containerSearchNext();
            if (%objSearch > 0 && %objSearch.isPowered() && %objSearch.team != %client.team)
               return 0;
         }

	 return AIODefault::weight(%this, %client, %level, %inventoryStr);

      case "FlagGrab":
			if (! %this.targetObjectId.isHome)
				return 0;
			else
	                        return AIODefault::weight(%this, %client, %level, %inventoryStr);

      case "FlagDropped":
			if ((%this.targetObjectId.isHome) || (%this.targetObjectId.carrier !$= ""))
				return 0;
			else
                        {
                           //we want to get the team trying to cap acting a little better than the Defending team - Lagg...
                           if (%client.team != %this.targetObjectId.team)
                           {
                              %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);
                              %distance = %client.getPathDistance(%this.getWorldBoxCenter());
			      if (%distance < 0)
				%distance = 32767;

                              //see if we're within 300 m
		              if (%distance < 300)
		                 %weight += 100;

		              //see if we're within 200 m
		              if (%distance < 200)
		                 %weight += 100;

                              //see if we're within 100 m
		              if (%distance < 100)
		                 %weight += 100;

		              //see if we're within 45 m
		              if (%distance < 45)
		                 %weight += 100;

                              return %weight;
                           }
                           else

                              return AIODefault::weight(%this, %client, %level, %inventoryStr);
		        }

      case "FlagCapture":
			if (%this.targetObjectId.carrier != %client.player)
                                return 0;

                        //find our home flag location
                        %homeTeam = %client.team;
		        %homeFlag = $AITeamFlag[%homeTeam];
			%this.location = %homeFlag.originalPosition;

                        //never bump the pilot with the flag - Lagg...
                        if (%client.player.isMounted())
                           return 10000;
                        else
			   return AIODefault::weight(%this, %client, %level, %inventoryStr);
   }
   return 0;
}

function AIOTouchObject::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AITouchObject);
   %client.objectiveTask.initFromObjective(%this, %client);
   
   //create an AIOEscortPlayer objective to help out, if required
   if (%this.mode $= "FlagGrab")
   {
      %client.escort = new AIObjective(AIOAttackLocation) {
					position = %this.targetObjectId.position;
					dataBlock = "AIObjectiveMarker";
					lockCount = "0";
					homingCount = "0";
                                        description = "Attack Flag Location for Capper";
					targetClientId = "-1";
					targetObjectId = "-1";
					location = %this.targetObjectId.position;
					weightLevel1 = $AIWeightEscortCapper[1];
					offense = "1";
					defense = "0";
                                        desiredEquipment = "SniperRifle EnergyPack Mortar MortarAmmo";
					buyEquipmentSet = "LightEnergySniper";
					chat = "ChatCmdAttackFlag";
					issuedByHuman = "0";
					issuedByClientId = "-1";
					forceClientId = "-1";
				};
      MissionCleanup.add(%client.escort);
      $ObjectiveQ[%client.team].add(%client.escort);
   }   
   else if (%this.mode $= "FlagCapture")
   {
      %client.escort = new AIObjective(AIOEscortPlayer)
                        {
							      dataBlock = "AIObjectiveMarker";
                           weightLevel1 = $AIWeightEscortCapper[1];
                           weightLevel2 = $AIWeightEscortCapper[2];
                           description = "Protect flag carrier " @ getTaggedString(%client.name);
                           targetClientId = %client;
                           offense = true;
									desiredEquipment = "Disc DiscAmmo";
									buyEquipmentSet = "LightEnergyDefault";
                           chat = "ChatCmdDefendCarrier";
                        };
      MissionCleanup.add(%client.escort);
      $ObjectiveQ[%client.team].add(%client.escort);
   }
}

function AIOTouchObject::unassignClient(%this, %client)
{

   //error("AIOTouchObject::unassignClient - Ahhh!");

   //kill the escort objective
   if (%client.escort)
   {
      clearObjectiveFromTable(%client.escort);
      AIClearObjective(%client.escort);
      %client.escort.delete();//seen spam error here if escort gets killed before call - Lagg... 1-7-2005
      %client.escort = "";
   }   
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}



//----------------------------------------------------------------------------------------------- AI Touch Object ---
//ramdom set to go for flag/flipflop -                                      - Lagg... - 4-14-2003

function AITouchObject::initFromObjective(%task, %objective, %client)
{
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;
   %task.mode = %objective.mode;
   %task.equipment = %objective.equipment;
   %task.buyEquipmentSet = %objective.buyEquipmentSet;
   %task.desiredEquipment = %objective.desiredEquipment;
   %task.issuedByClient = %objective.issuedByClientId;
   %task.chat = %objective.chat;
   %task.useTeleport = false;

   %task.sendMsgTime = 0;
   if (%task.mode $= "FlagGrab" || %task.mode $= "FlagCapture")
      %task.sendMsg = true;
   else
      %task.sendMsg = false;
   if (%task.mode $= "FlagCapture")
      %task.location = %objective.location;
   else if(%task.mode $= "TouchFlipFlop" || %task.mode $= "FlagGrab")
   {
      %task.location = %objective.location;
      //if  (%task.desiredEquipment $= "" && %client.player.getArmorSize() $= "Light" && %client.player.getInventory("Blaster") > 0)
      if  (%task.desiredEquipment $= "")
      {
         //pick a random set based on armor...
         %randNum = getRandom();
         if (%randNum < 0.2)
         {
            %task.desiredEquipment = "SensorJammerPack";
            %task.buyEquipmentSet = "HeavyEnergySet";
         }
         else if (%randNum < 0.4)
         {
            %task.desiredEquipment = "SensorJammerPack";
            %task.buyEquipmentSet = "LightSensorJammer";
         } 
         else if (%randNum < 0.6)
         {
            %task.desiredEquipment = "CloakingPack";
            %task.buyEquipmentSet = "LightCloakSet";
         }
         else if (%randNum < 0.8)
         {
            %task.desiredEquipment = "ShieldPack";
            %task.buyEquipmentSet = "HeavyShieldOff";
         }
         else
         {
            %task.desiredEquipment = "EnergyPack";
            %task.buyEquipmentSet = "LightEnergyDefault";
         }
      }
   }
   else
      %task.location = "";
}

function AITouchObject::assume(%task, %client)
{
   %task.setWeightFreq(15);
   %task.setMonitorFreq(15);
   %task.engageTarget = 0;

   //if we are to far from inventory are close to target forget inventory - Lagg...
   %client.needEquipment = false; 
   if (%task.buyEquipmentSet !$= "")
   {
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;

      %needArmor = AIMustUseRegularInvStation(%equipmentList, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      %closestDist = getWord(%result, 1);
      if (AINeedEquipment(%equipmentList, %client) && %closestInv > 0)
      {
         //find where we are
	 %distToObject = %client.getPathDistance(%task.location);
	 if (%closestDist < 100 && %distToObject > 300)
	    %client.needEquipment = true;
      }
   }

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();

   //--------------------------------------------------- assume telporter - start -
   //if the MPB Teleporter is online - Lagg... 9-30-2003
   if (!%client.player.isMounted())
      %task.useTeleport = MPBTeleporterCheck(%client, %task.targetObject.position);
   else
       %task.useTeleport = false;
   //--------------------------------------------------- assume telporter - end -
}

function AITouchObject::retire(%task, %client)
{
}

function AITouchObject::weight(%task, %client)
{
   //update the task weight
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;

   //see if we can find someone to shoot at...
   if (%client.getEngageTarget() <= 0)
   {
      %losTimeout = $AIClientMinLOSTime + ($AIClientLOSTimeout * %client.getSkillLevel());
      %myLocation = %client.player.getWorldBoxCenter();
      %result = AIFindClosestEnemy(%client, 40, %losTimeout);
      %task.engageTarget = getWord(%result, 0);
   }
   
   %task.setWeight(%task.baseWeight);
}

function AITouchObject::monitor(%task, %client)
{
   //echo("AITouchObject::monitor - " @ getTaggedString(%client.name) @ ", " @ %client.objectiveWeight);

   //--------------------------------------------------- monitor telporter - start -
   //are we using teleporter
   if (%task.useTeleport)
   {
      %result = AIFindDeployedMPB(%client);//new function in aiVehicle.cs
      %closestMPB = getWord(%result, 0);
      %closestMPBDist = getWord(%result, 1);
      %result = SweepForTeleporters(%client);//new function in aiVehicle.cs
      %closestTel = getWord(%result, 0);
      %closestTelDist = getWord(%result, 1);
      if (%closestMPB > 0 && %closestTel > 0)
      {
         //we are done teleporting
         if (%closestMPBDist < %closestTelDist)
         {
            //reset the clients inventory status
            if (%client.needEquipment)
            {
               %result = AIFindClosestInventory(%client, 0);
	       %closestInv = getWord(%result, 0);
               %client.invToUse = %closestInv;
            }
            %task.useTeleport = false;
            return;
         }

         if (%closestTelDist < 2)
            %client.pressJump();
         else
         {
            %client.stepMove(%closestTel.getWorldBoxCenter(), 0.25);
            return;
         }
      }
      else
         %task.useTeleport = false;
   }
   //--------------------------------------------------- monitor telporter - end -

   //first, buy the equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
	 %equipmentList = %task.desiredEquipment;
      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
         return;
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(15);
	 %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
         %task.setMonitorFreq(15);
	 %client.needEquipment = false;
      }
   }

   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   //chat
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
	 if (%client.isAIControlled())
	 {
	    if (%task.chat !$= "")
	    {
	       %chatMsg = getWord(%task.chat, 0);
	       %chatTemplate = getWord(%task.chat, 1);
	       if (%chatTemplate !$= "")
	          AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
	       else
		  AIMessageThread(%task.chat, %client, -1);
            }
         }
      }
   }

   //get clients skill
   %skill = %client.getSkillLevel();

   //echo("Skill = " @ %skill);

   //keep updating the position, in case the flag is flying through the air...
   if (%task.location !$= "")
      %touchPos = %task.location;
   else
      %touchPos = %task.targetObject.getWorldBoxCenter();
      
   //see if we need to engage a new target
   %engageTarget = %client.getEngageTarget();
   if (!AIClientIsAlive(%engageTarget) && %task.engageTarget > 0)
      %client.setEngageTarget(%task.engageTarget);
   
   //else see if we should abandon the engagement
   else if (AIClientIsAlive(%engageTarget))
   {
      %myPos = %client.player.getWorldBoxCenter();
      %testPos = %engageTarget.player.getWorldBoxCenter();
      %distance = %client.getPathDistance(%testPos);
      if (%distance < 0 || %distance > 50)
         %client.setEngageTarget(-1);
   }

   //see if we have completed our objective
   if (%task == %client.objectiveTask)
   {
      %completed = false;
      switch$ (%task.mode)
      {
         case "TouchFlipFlop":
	    if (%task.targetObject.team == %client.team)
	       %completed = true;
	 case "FlagGrab":
	    if (!%task.targetObject.isHome)
               %completed = true;
	 case "FlagDropped":
	    if ((%task.targetObject.isHome) || (%task.targetObject.carrier !$= ""))
	       %completed = true;
	 case "FlagCapture":
	    if (%task.targetObject.carrier != %client.player)
	       %completed = true;
      }
      if (%completed)
      {
         AIUnassignClient(%client);
	 Game.AIChooseGameObjective(%client);
	 return;
      }
   }

   if (%task.mode $= "FlagCapture" && %skill > 0.4)
   {
      %player = %client.player;
      %myArmor = %player.getArmorSize();

      //cut away any sand bags
      if (hasLargePack(%player))
         %player.throwPack();

      if (!%player.isMounted())
      {
         //see if we can steal a ride
         %abV = -1;
         if (%myArmor !$= "Heavy")
         {
            //max dis to search for abandon vehicles, 250
            %clDis = 250;
            for (%i = 0; %i < $aiVehicleSet.getCount(); %i ++)
            {         
               %vehicle = $aiVehicleSet.getObject(%i);
               if (isObject(%vehicle))
               {            
                  %type = %vehicle.getDataBlock().getName();
                  if (%vehicle.getMountNodeObject(0) <= 0 && %type !$= "MobileBaseVehicle")
                  {
                     if (%myArmor $= "Light" || %type !$= "ScoutVehicle")
                     {
                        %dis = VectorDist(%player.position, %vehicle.position);
                        %dis1 = VectorDist(%vehicle.position, %touchPos);
                
                        if (%dis < %clDis && %dis1 > 600)
                        {                  
                           %abV = %vehicle;
                           %clDis = %dis;
                        }
                     }
                  }
               }
            }
         }

         if (%abV > 0)
         {
            //since we have the flag, hurry up and just touch the vehicle anywhere to mount it.
            %client.pilotVehicle = true;//needed to let ai mount pilot seat

            if (%client.inWater)
            {               
               %vPos2D = getWord(%abV.getPosition(), 0) SPC getWord(%abV.getPosition(), 1) SPC "0";
               %myPos2D = getWord(%client.player.position, 0) SPC getWord(%client.player.position, 1) SPC "0";
               if (vectorDist(%myPos2D, %vPos2D) < 2.5)
               {
                  %client.pressJump();
                  %client.stepMove(%abV.getWorldBoxCenter(), 0.25, $AIModeGainHeight);
               }
               else
                  %client.stepMove(%abV.position, 0.25, $AIModeExpress);
            }
            else 
               %client.stepMove(getWords(%abv.getTransform(), 0, 2), 0.25, $AIModeMountVehicle);
         }
         else
         {  
            %homeFlag = $AITeamFlag[%client.team];

            //if we're within range of the flag's home position, and the flag isn't home, start idling...
            if (VectorDist(%player.position, %touchPos) < 40 && !%homeFlag.isHome)
            {
               if (%client.getStepName() !$= "AIStepIdlePatrol")
	          %client.stepIdle(%touchPos);
            }
            else
               %client.stepMove(%touchPos, 0.25, $AIModeExpress);
         }
      }
      else if (%player.isMounted())// -------------------------------------- ADD FLIGHT CONTROLS HERE --- Lagg...
      {
         //get the vehicle and velocity
         %vehicle = %client.vehicleMounted;
         
         %type = %vehicle.getDataBlock().getName();
         %mySpd = VectorLen(%vehicle.getVelocity());
         %alt = getWord(%player.position, 2) - getTerrainHeight(%player.position);
         %myZ = getWord(%vehicle.getWorldBoxCenter(), 2);
         %collision = aiCollisionCheck(%vehicle, (%mySpd * 2));

         //just in case he smacks into an mpb
         if (%type $= "MobileBaseVehicle")
            AIDisembarkVehicle(%client);

         echo("Speed = " @ %mySpd);

         //------------------------------------------------------- Avoidance ---
         if (%mySpd < 0.2)
         { 
            //InitContainerRadiusSearch(%vehicle.getWorldBoxCenter(), 9.0, $TypeMasks::InteriorObjectType |


             // $TypeMasks::VehicleObjectType | $TypeMasks::TSStaticShapeObjectType);
            //%avoid = containerSearchNext();

            //if (%avoid = %vehicle)
               //%avoid = containerSearchNext();

            //if we are moving slow and close to objects we are probably stuck
            //if (%avoid)
            //{
               %vx = getWord(%vehicle.getWorldBoxCenter(), 0);
               %vy = getWord(%vehicle.getWorldBoxCenter(), 1);
               %vz = getWord(%vehicle.getWorldBoxCenter(), 2);
               %vz += 2.5;
               %vR = getWords(%vehicle.getTransform(), 3, 6);

               //%client.setPilotDestination(%location, 1.0);//max speed
               %vehicle.setTransform(%vx SPC %vy SPC %vz SPC %vR);// --- set the Transform Here up a little

               error("AITouchObject Vehicle Avoidance - STUCK " @ getTaggedString(%client.name));

               //return;//i think we need this here
            //}
         }

         //----------------------------------------------------------------check to see if we are close to home...

         %pos2D = getWord(%vehicle.getWorldBoxCenter(), 0) SPC getWord(%vehicle.getWorldBoxCenter(), 1) SPC "0";
         %dest2D = getWord(%touchPos, 0) SPC getWord(%touchPos, 1) SPC "0";
         //if (VectorDist(%pos2D, %dest2D) < 100)
         if (VectorDist(%vehicle.getWorldBoxCenter(), %touchPos) < 100)
            AIDisembarkVehicle(%client);      

         //if in a land rover just drive it.
         if (%type $= "ScoutVehicle" || %type $= "AssaultVehicle")
            %client.setPilotDestination(%touchPos, 1);// --- max speed
         else
         {
            //---------------------------------------------------------------- we have to use some form of navigation here...

            %dest = getWord(%touchPos, 0) SPC getWord(%touchPos, 1) SPC getWord(%touchPos, 2) + 100;

            // - if can't see location marker gain height
            %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
            %hasLOS = !containerRayCast(%vehicle.getWorldBoxCenter, %dest, %mask, 0);//------- %hasLOS to %task.location ?
            //%hasLOS = aiCollisionCheck(%vehicle, 150);//------- %hasLOS to %task.location ?

            //if we get blocked fly straight up a little (only if we are going to hit something)
            //if (%myZ < (nameToId("MissionArea").flightCeiling - nameToId("MissionArea").flightCeilingRange) &&
            //((!%hasLOS || %collision) || %alt < 25))
            if ((%myZ < MissionArea.flightCeiling - MissionArea.flightCeilingRange) &&
              (%collision || %alt < 25))
            {
               //for shrikes and havocs
               if (%type $= "ScoutFlyer" || %type $= "HAPCFlyer")
               {
                  applyKick(%vehicle, "up", %skill * 5);
                  applyKick(%vehicle, "foward", %skill * 5, true);
                  //%client.setPilotPitchRange(-0.006, 0.0035, 0.005);
                  %client.setPilotPitchRange(-0.006, 0.0035, 0.01);
                  %client.setPilotDestination(%dest, 1);//Max Speed
               }
               //reversed for bombers
               else
               {
                  //%client.setPilotPitchRange(-0.01, 0.001, 0.05);
                  //applyKick(%vehicle, "down", 0.5, true);
                  //applyKick(%vehicle, "foward", %skill * 5, true);
                  //%client.setPilotDestination(%dest, 1);//Max Speed

                  %x = firstWord(%vehicle.getWorldBoxCenter());
                  %y = getWord(%vehicle.getWorldBoxCenter(), 1);
                  %z = getWord(%vehicle.getWorldBoxCenter(), 2);
                  %client.setPilotPitchRange(-0.005, 0.0045, 0.005);//real smooth setting here
                  %client.setPilotDestination(%x SPC %y SPC %z + 10, 1);// -- if we can't see %task.location move straight up
               } 

               echo("blocked view or collision, - pitch up");
            }
            //else fly straight to home flag
            else 
            {
               %client.setPilotPitchRange(-0.005, 0.0045, 0.005);//real smooth setting here
               %client.setPilotDestination(%dest, 1);//Max Speed

               echo("flying striaght home");
            }
         }
      }
      //-------------------------------------------------------------------------------------------- end vehicle stuff
   }

   //if siege game we want a special way of capturing the flip flop - Lagg... 9-11-2003
   else if (%task.mode $= "TouchFlipFlop" && $CurrentMissionType $= "Siege")
   {

      //if there is a force field blocking the way - Lagg... 9-11-2003
      InitContainerRadiusSearch(%touchPos, 12, $TypeMasks::ForceFieldObjectType);
      %objSearch = containerSearchNext();
      if (%objSearch > 0 && %objSearch.isPowered())//should we check for all pass force fields? - Lagg ... (nah)
      {
         if (%task == %client.objectiveTask)
         {
            AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
	    return;
         }
      }
      else
         %client.stepMove(%touchPos, 0.25, $AIModeExpress);
   }
   else
      %client.stepMove(%touchPos, 0.25, $AIModeExpress);

   if (VectorDist(%client.player.position, %touchPos) < 10)
   {
      //dissolve the human control link
      if (%task == %client.objectiveTask)
      {
         if (aiHumanHasControl(%task.issuedByClient, %client))
	 {
	    aiReleaseHumanControl(%client.controlByHuman, %client);

	    //should re-evaluate the current objective weight
	    %inventoryStr = AIFindClosestInventories(%client);
	    %client.objectiveWeight = %client.objective.weight(%client, %client.objectiveLevel, 0, %inventoryStr);
	 }
      }
   }

   //see if we're supposed to be engaging anyone...
   if (!AIClientIsAlive(%client.getEngageTarget()) && AIClientIsAlive(%client.shouldEngage))
      %client.setEngageTarget(%client.shouldEngage);

   //clear the last damaged tag
   %client.lastDamageClient = -1;
}


//--------------------------------------------------------------------------------------------------------------------------
//AI Objective functions

function ClientHasAffinity(%objective, %client)
{
   if (%objective.offense && %client.offense)
      return true;
   else if (%objective.defense && !%client.offense)
      return true;
   else
      return false;
}

//this function needs a bit of work, one would say - LaggAlot 8/22/09
function ClientHasRequiredEquipment(%objective, %client)
{
   return true;
}

function AIODefault::weight(%objective, %client, %level, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   //set the base weight
   switch (%level)
   {
      case 1:
         %weight = %objective.weightLevel1;
      case 2:
         %weight = %objective.weightLevel2;
      case 3:
         %weight = %objective.weightLevel3;
      default:
         %weight = %objective.weightLevel4;
   }
   
   //check Affinity
   if (ClientHasAffinity(%objective, %client))
      %weight += 40;

   //if the objective doesn't require any equipment, it automatically get's the +100...
   if (%objective.equipment $= "" && %objective.desiredEquipment $= "")
      %weight += 100;
   else
   {
      //check equipment requirement
      %needEquipment = AINeedEquipment(%objective.equipment, %client);
	      
      //check Required equipment
      if (%objective.equipment !$= "" && !%needEquipment)
         %weight += 100;

      //figure out the percentage of desired equipment the bot has
      else if (%objective.desiredEquipment !$= "")
      {
         %count = getWordCount(%objective.desiredEquipment);
	 %itemCount = 0;
	 for (%i = 0; %i < %count; %i++)
	 {
	    %item = getWord(%objective.desiredEquipment, %i);
	    if (!AINeedEquipment(%item, %client))
	    %itemCount++;
	 }

	 //add to the weight
	 %weight += mFloor((%itemCount / %count) * 75);
      }
   }
      
   //find the distance to target
   if (%objective.targetClientId !$= "" || %objective.targetObjectId !$= "")
   {
      if (AIClientIsAlive(%objective.targetClientId))
      {   
         %targetPos = %objective.targetClientId.player.getWorldBoxCenter();
      }
      else if (VectorDist(%objective.location, "0 0 0") > 1)
         %targetPos = %objective.location;
      else
      {   
         if(%objective.targetObjectId > 0)
            %targetPos = %objective.targetObjectId.getWorldBoxCenter();
      }
   }

   //make sure the destination is accessible
   %distance = %client.getPathDistance(%targetPos);
   if (%distance < 0)
      return 0;

   %closestInvIsRemote = (getWordCount(%inventoryStr) == 4);
   %closestInv = getWord(%inventoryStr, 0);
   %closestDist = getWord(%inventoryStr, 1);
   %closestRemoteInv = getWord(%inventoryStr, 2);
   %closestRemoteDist = getWord(%inventoryStr, 3);
	
   //if we need equipment, the distance is from the client, to an inv, then to the target
   if (%needEquipment)
   {
      //echo("AIODefault::weight - %needArmor = " @ %needArmor SPC getTaggedString(%client.name));
      //if we need a regular inventory station, and one doesn't exist, exit

      %needArmor = AIMustUseRegularInvStation(%objective.equipment, %client);// - added here to fix below - Lagg... 3-10-2004

      if (!isObject(%closestInv) && %needArmor)// - we got an error here - %needArmor = "" (lets just review/redo the whole function) * fixed for now - Lagg... ***
      {
         //error("AIODefault::weight - %needArmor = True no invos" @ getTaggedString(%client.name));
         return 0;
      }

      //find the closest inv based on whether we require armor (from a regular inv station)
      if (!%closestInvIsRemote)
      {
         %needArmor = false;
	 %weightDist = %closestDist;
	 %weightInv = %closestInv;
      }
      else
      {
         //%needArmor = AIMustUseRegularInvStation(%objective.equipment, %client);// - removed here moved up ^ - Lagg... 3-10-2004
	 if (%needArmor)
	 {
	    %weightDist = %closestDist;
	    %weightInv = %closestInv;
	 }
	 else
	 {
	    %weightDist = %closestRemoteDist;
	    %weightInv = %closestRemoteInv;
	 }
      }

      //tested this next check to be true, now need to ad a search for items in each aiObjective :) - Lagg... 1-20-2004
      //if we don't need armor, and there's no inventory station, see if the equipment we need

      // ------------------------------------------------------------- have to modify to see what is closer item or inventory *

      //is something we can pick up off the ground (likely this would be a repair pack...)
      //updated AIODeployEquipment to consider picking up droped packs :) - Lagg... 3-10-2004

      //echo("AIODefault::weight - %weightDist = " @ %weightDist SPC getTaggedString(%client.name));
      if (%weightDist >= 32767)
      {
         
         %itemType = getWord(%objective.equipment, 0);
  	 %found = false;
  	 %itemCount = $AIItemSet.getCount();
  	 for (%i = 0; %i < %itemCount; %i++)
  	 {
  	    %item = $AIItemSet.getObject(%i);
  	    if (%item.getDataBlock().getName() $= %itemType && !%item.isHidden())
  	    {
	       %weightDist = %client.getPathDistance(%item.getWorldBoxCenter());
	       if (%weightDist > 0)
	       {
	          %weightInv = %item;  //set the var so the distance function will work...
	  	  %found = true;
	  	  break;
	       }
  	    }
  	 }

  	 if (! %found)
  	    return 0;
      }
 
      //now find the distance used for weighting the objective
      %tempDist = AIGetPathDistance(%targetPos, %weightInv.getWorldBoxCenter());
      if (%tempDist < 0)
         %tempDist = 32767;

      %distance = %weightDist + %tempDist;
   }

   // -------------------------------------------------- lets give him some personallity on this one :) - Lagg... 2-13-2004
   //if (%objective.targetObjectId > 0 && !%needEquipment)
   //{
      //if (VectorDist(%client.player.getWorldBoxCenter(), %targetPos) < 250)
      //{

         ////check for LOS
         //%mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType |
           //$TypeMasks::ForceFieldObjectType | $TypeMasks::VehicleObjectType;
         //%hasLOS = !containerRayCast(%client.player.getMuzzlePoint($WeaponSlot), %objective.targetObjectId.getWorldBoxCenter(), %mask, 0);

         //if (%hasLOS)
         //{
            ////echo("AIODefault::weight - We Got LOS - " @ getTaggedString(%client.name));
            //%weight += 400;
         //}
      //}
   //}

   //are we the closest to inventory - Lagg... 4-9-2004
   %closestInv = AIFindClosestInventory(%client, false);
   %closestDist = getWord(%closestInv, 1);
   if (%closestDist < 1)
      %weight += 30;
   
   //see if we're within 200 m
   if (%distance < 200)
      %weight += 30;
      
   //see if we're within 90 m
   if (%distance < 90)
      %weight += 30;

   //see if we're within 45 m
   if (%distance < 45)
      %weight += 30;
      
   //see if we're within 20 m
   if (%distance < 20)
      %weight += 30;

   //if we get bonus for being close we should get penalty if far - Lagg... 2-15-2004
   //see if we're passed 450 m
   if (%distance >= 200)
      %weight -= 30;
      
   //see if we're pass 900 m
   if (%distance > 500)
      %weight -= 30;

   //see if we're pass 1350 m
   if (%distance > 1150)
      %weight -= 30;
      
   //see if we're pass 1600 m
   if (%distance > 1800)
      %weight -= 30;
   
   //final return, since we've made it through all the rest
   return %weight;
}

function AIODefault::QuickWeight(%objective, %client, %level, %minWeight)
{
   //can't do a quick weight when re-evaluating a client's current objective
   if (%client.objective == %objective)
      return true;

   //do a quick check to disqualify this objective if it can't meet the minimum weight
   switch (%level)
   {
      case 1:
         %testWeight = %objective.weightLevel1;
      case 2:
	 %testWeight = %objective.weightLevel2;
      case 3:
	 %testWeight = %objective.weightLevel3;
      default:
	 %testWeight = %objective.weightLevel4;
   }

   //if (%objective.getName() $= "AIOTouchObject")
   //{
      //error("AIODefault::QuickWeight - TestWeight = " @ (%testWeight + 260));
      //echo("AIODefault::QuickWeight - MinWeight = " @ %minWeight);
   //}

   if (%testWeight + 260 < %minWeight)
      return false;
   else
      return true;
}

//--------------------------------------------------------------
//AI Objective Q functions...

$ObjectiveClientsSet = 0;
function AIObjectiveFindClients(%objective)
{
   //create and clear the set
   if (! $ObjectiveClientsSet)
   {
      $ObjectiveClientsSet = new SimSet();
      MissionCleanup.add($ObjectiveClientSet);
   }
   $ObjectiveClientsSet.clear();
   
   %clientCount = 0;
   %count = ClientGroup.getCount();
   for(%i = 0; %i < %count; %i++)
   {
		%cl = ClientGroup.getObject(%i);
      if (%cl.objective == %objective)
         $ObjectiveClientsSet.add(%cl);
   }
   return $ObjectiveClientsSet.getCount();
}

function AIObjectiveGetClosestClient(%location, %team)
{
   if (%location $= "")
      return -1;
      
   if (%team $= "")
      %team = 0;
      
   %closestClient = -1;
   %closestDistance = 32767;
   %count = ClientGroup.getCount();
   for(%i = 0; %i < %count; %i++)
   {
		%cl = ClientGroup.getObject(%i);
      if (%cl.isAIControlled() && (%cl.team == %team || %team == 0) && AIClientIsAlive(%cl))
      {
         %testPos = %cl.player.getWorldBoxCenter();
         %distance = VectorDist(%location, %testPos);
         if (%distance < %closestDistance)
         {
            %closestDistance = %distance;
            %closestClient = %cl;
         }
      }
	}
   
   return %closestClient;
}

function AICountObjectives(%team)
{
	%objCount = 0;
	%count = $ObjectiveQ[%team].getCount();
	for (%i = 0; %i < %count; %i++)
	{
		%grp = $ObjectiveQ[%team].getObject(%i);
		if (%grp.getClassName() !$= "AIObjective")
			%objCount += %grp.getCount();
		else
			%objCount++;
	}
	error("DEBUG" SPC %team SPC "has" SPC %objCount SPC "objectives.");
}

function AIAddTableObjective(%objective, %weight, %level, %bump, %position)
{
	$objTable[%position, objective] = %objective;
	$objTable[%position, weight] = %weight;
	$objTable[%position, level] = %level;
	$objTable[%position, bump] = %bump;
	$objTableCount = %position + 1;

      //error("TAbleCount = " @ $objTableCount);
}

//-----------------------------------------------------------------------------------------------------------------------
//This function had me stumped for a long time. The use of the $Objective[Q] and also the $objectTable[] to choose
//objectives was the problem, when objectives are deleted from the $Objective[Q] they were still left in the $objTable[],
//the clients choose from the $objTable and so the objective that was deleted no longer exsisted while the client was
//trying to choose it.                                                                                   - Lagg... 1-27-2004
//DO NOT FOR ANY REASON cHANGE,REPLACE OR MODIFY THIS FUNCTION IN ANY WAY.
//ALL THE AIOBECTIVE SCRIPTS DEPEND ON IT!                                                               - Lagg... 5-29-2004

function AIChooseObjective(%client, %useThisObjectiveQ)
{
   //pick which objectiveQ to use, or use the default
   if (%useThisObjectiveQ <= 0 && %client.team < 0)
      return;

   if (%useThisObjectiveQ <= 0)
      %useThisObjectiveQ = $ObjectiveQ[%client.team];

   if (!isObject(%useThisObjectiveQ) || %useThisObjectiveQ.getCount() <= 0)
      return;

   //since most objectives check for inventory, find the closest inventory stations first
   %inventoryStr = AIFindClosestInventories(%client);

   //find the most appropriate objective
   if (!%client.objective)
   {
      //note, the table is never empty, during the course of this function
      AIAddTableObjective(0, 0, 0, 0, 0);
   }
   else
   {
      //should re-evaluate the current objective weight - but never decrease the weight!!!
      %testWeight = %client.objective.weight(%client, %client.objectiveLevel, 0, %inventoryStr);
      if (%testWeight <= 0 || %testWeight > %client.objectiveWeight)
         %client.objectiveWeight = %testWeight;

      if (%client.objectiveWeight > 0)
         AIAddTableObjective(%client.objective, %client.objectiveWeight, %client.objectiveLevel, 0, 0);
      else
         AIAddTableObjective(0, 0, 0, 0, 0);
   }

   %objCount = %useThisObjectiveQ.getCount();
   for (%i = 0; %i < %objCount; %i++)
   {
      %objective = %useThisObjectiveQ.getObject(%i);

      //don't re-evaluate the client's own
      if (%objective == %client.objective)
         continue;

      //try this objective at each of the 4 weight levels to see if it is weighted higher
      for (%level = 1; %level <= 4; %level++)
      {
         %minWeight = 0;
	 %bumpWeight = 0;
         %bumpClient = "";

         //we can bump clients off the objective for the first three levels
         if (%level <= 3)
         {
            //if the objective is part of a group, check the whole group
	    if (%objective.group > 0)
            {
               %bumpClient = %objective.group.clientLevel[%level];
               %bumpWeight = %bumpClient.objectiveWeight;
            }
            else
            {
               %bumpClient = %objective.clientLevel[%level];
               %bumpWeight = %bumpClient.objectiveWeight;
            }
         }

         //find the minimum weight the objective must have to be considered
         %minWeight = (%bumpWeight > $objTable[0, weight] ? %bumpWeight : $objTable[0, weight]);

         //evaluate the weight
	 %weight = %objective.weight(%client, %level, %minWeight, %inventoryStr);

	 //make sure we got a valid weight
	 if (%weight <= 0)
	    break;

         //if it's the highest so far, it now replaces anything else in the table
         if (%weight > $objTable[0, weight])
         {
            //never bump someone unless you out- weight them
            if (%weight > %bumpWeight)
            {
               AIAddTableObjective(%objective, %weight, %level, %bumpClient, 0);

               //no need to keep checking the other levels
	       break;
            }
         }

         //else if it's equal to the highest objective we've seen so far, and higher from our current objective
         else if (%weight == $objTable[0, weight] && %weight > %client.objectiveWeight)
         {
            //never bump someone unless you outweigh them
            if (%weight > %bumpWeight)
            {
	       //if this wouldn't require us to bump someone, or the table is empty
               if (%bumpWeight <= 0 || $objTable[0, weight] <= 0)
               {
						//if the table currently contains objectives which would require bumping someone, clear it
						if ($objTable[0, bump] > 0)
							%position = 0;
						else
							%position = $objTableCount;

						//add it to the table
						AIAddTableObjective(%objective, %weight, %level, %bumpClient, %position);

						//no need to keep checking the other levels
						break;
					}

					//otherwise, the table is not empty, and this would require us to bump someone
					//only add it if everything else in the table would also require us to bump someone
					else if ($objTable[0, bump] > 0)
					{
						AIAddTableObjective(%objective, %weight, %level, %bumpClient, $objTableCount);

						//no need to keep checking the other levels
						break;
					}
				}
			}

			//else it must have been less than our highest objective so far- again, no need to keep checking other levels...
			else
				break;
		}
	}

	//if we have a table of possible objectives which are higher than our current- choose one at random
	if ($objTableCount > 0 && $objTable[0, objective] != %client.objective)
	{
		//choose the new one
		%index = mFloor(getRandom() * ($objTableCount - 0.01));

		//clear the old objective
      if (%client.objective)
      {
         if (%client.objectiveLevel <= 3)
			{
				if (%client.objective.group > 0)
				{
					if (%client.objective.group.clientLevel[%client.objectiveLevel] == %client)
						%client.objective.group.clientLevel[%client.objectiveLevel] = "";
				}
				else
				{
					if (%client.objective.clientLevel[%client.objectiveLevel] == %client)
		            %client.objective.clientLevel[%client.objectiveLevel] = "";
				}
			}
         %client.objective.unassignClient(%client);
      }
		
		//assign the new
      %chooseObjective = $objTable[%index, objective];
      %client.objective = %chooseObjective;
      %client.objectiveWeight = $objTable[%index, weight];
      %client.objectiveLevel = $objTable[%index, level];
		if (%client.objectiveLevel <= 3)
		{
			if (%chooseObjective.group > 0)
		      %chooseObjective.group.clientLevel[%client.objectiveLevel] = %client;
			else
		      %chooseObjective.clientLevel[%client.objectiveLevel] = %client;
		}
      //error("%chooseObjective.assignClient = " @ %chooseObjective.getName());

      //make sure we still have a valid objective - Lagg... 1-27-2004
      if (isObject(%chooseObjective))
        %chooseObjective.assignClient(%client);
      else
      {
        //error("AIChooseObjective - Failed please choose again :) " @ getTaggedString(%client.name));
        AIChooseObjective(%client, %useThisObjectiveQ);//if failed choose again - Lagg... 1-27-2004
      }

      //see if this objective needs to be acknowledged
      if (%chooseObjective.shouldAcknowledge && %chooseObjective.issuedByHuman && isObject(%chooseObjective.issuedByClientId))
      {
         //cancel any pending acknowledgements - a bot probably just got bumped off this objective
         cancel(%chooseObjective.ackSchedule);
         %chooseObjective.ackSchedule = schedule(5500, %chooseObjective, "AIAcknowledgeObjective", %client, %chooseObjective);
      }

      //if we had to bump someone off this objective, 
		%bumpClient = $objTable[%index, bump];
      if (%bumpClient > 0)
      {
         //unassign the bumped client and choose a new objective
			AIUnassignClient(%bumpClient);
         Game.AIChooseGameObjective(%bumpClient);
      }
	}

   //debuging - refresh aidebugq() if required
   //if ($AIDebugTeam >= 0)
   //   aiDebugQ($AIDebugTeam);
}

//new function to clean up the mess they made with the aiChooseObjective code above
//this gets called when ever AIObjectives need to be deleted from the $Objective[Q] - Lagg... 1-27-2004
function clearObjectiveFromTable(%objective)
{
   for(%i = 0; %i < $objTableCount; %i ++)
   {
      if ($objTable[%i, objective] = %objective)
      {
         $objTable[%i, objective] = 0;
	 $objTable[%i, weight] = 0;
	 $objTable[%i, level] = 0;
	 $objTable[%i, bump] = 0;

         break;
      }
   }
}

function AIAcknowledgeObjective(%client, %objective)
{
   %objective.shouldAcknowledge = false;
   //make sure the client is still assigned to this objective
   if (%client.objective == %objective)
	   serverCmdAcceptTask(%client, %objective.issuedByClientId, -1, %objective.ackDescription);
}

function AIForceObjective(%client, %newObjective, %useWeight)
{
   //if we found a new objective, release the old, and assign the new
   if (%newObjective && %newObjective != %client.objective)
   {
      //see if someone was already assigned to this objective
		if (%newObjective.group > 0)
	      %prevClient = newObjective.group.clientLevel[1];
		else
	      %prevClient = newObjective.clientLevel[1];
      if (%prevClient > 0)
         AIUnassignClient(%prevClient);

		//see if we should override the weight
		if (%useWeight < %newObjective.weightLevel1)
			%useWeight = %newObjective.weightLevel1;
      
      //release the client, and force the assignment
      AIUnassignClient(%client);
      %client.objective = %newObjective;
      %client.objectiveWeight = %useWeight;
      %client.objectiveLevel = 1;
		if (%newObjective.group > 0)
	      %newObjective.group.clientLevel[1] = %client;
		else
	      %newObjective.clientLevel[1] = %client;
		%newObjective.forceClientId = %client;
      %newObjective.assignClient(%client);

      //don't acknowledge anything that's been forced...
      %newObjective.shouldAcknowledge = false;
      
      //now reassign the prev client
      if (%prevClient)
         Game.AIChooseGameObjective(%prevClient);
   }
   
   //debuging - refresh aidebugq() if required
   //if ($AIDebugTeam >= 0)
    // aiDebugQ($AIDebugTeam);
}

function AIUnassignClient(%client)
{
	//first, dissolve any link with a human
	aiReleaseHumanControl(%client.controlByHuman, %client);

   if (%client.objective)
   {
      if (%client.objectiveLevel <= 3)
		{
			if (%client.objective.group > 0)
			{
				//make sure the clientLevel was actually this client
				if (%client.objective.group.clientLevel[%client.objectiveLevel] == %client)
					%client.objective.group.clientLevel[%client.objectiveLevel] = "";
			}
			else
			{
				if (%client.objective.clientLevel[%client.objectiveLevel] == %client)
		         %client.objective.clientLevel[%client.objectiveLevel] = "";
			}
		}
      %client.objective.unassignClient(%client);
      %client.objective = "";
		%client.objectiveWeight = 0;
   }
   
   //debuging - refresh aidebugq() if required
   //if ($AIDebugTeam >= 0)
      //aiDebugQ($AIDebugTeam);
}
  
function AIClearObjective(%objective)
{
   %count = ClientGroup.getCount();
   for(%i = 0; %i < %count; %i++)
   {
		%cl = ClientGroup.getObject(%i);
      if (%cl.objective == %objective)
         AIUnassignClient(%cl);
   }
   
   //debuging - refresh aidebugq() if required
   //if ($AIDebugTeam >= 0)
      //aiDebugQ($AIDebugTeam);
}
