//----------------------------------------------------------------------------------------------- AIO Easy Rider ---
//Set up simgroups called "T" @ %team @ "BikePath" @ %num
//(example T2TankPath1 or T2BikePath7 / T1BikePath4 or T1BikePath9)
//NOTE: The AIObjective marker must be close to the vpad to use (some maps have multiple vpads per team).
//must state the path # to follow in the "paths" field of the objective marker seperated by spaces. The
//simgroups must contain markers or cameras placed in the order to follow for each path.
//Thats 1 simgroup per path - Lagg... 4-8-2004

function AIOEasyRider::weight(%this, %client, %level, %minWeight, %inventoryStr)     
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   // For CnH games, we set a target object and set def or offense (can be used for any gameType)
   if (%this.targetObjectId > 0 && %this.clientLevel1 != %client)
   {
      if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden())
         return 0;

      //if offense and we own target object quit
      if (%this.offense > 0)
      {
         if (%this.targetObjectId.team == %client.team || %this.targetObjectId.getDamageState() $= "Destroyed")
            return 0;
      }
      //if defense and we own it must be powered and not destroyed, or if enemy it must be destroyed or not powered
      else if (%this.defense > 0)
      {
         if (%this.targetObjectId.team == %client.team)
         {
            if (!%this.targetObjectId.isPowered() || %this.targetObjectId.getDamageState() $= "Destroyed")
               return 0;
         }
         else
         { 
            if (%this.targetObjectId.isPowered() && %this.targetObjectId.getDamageState() !$= "Destroyed")
              return 0;
         }
      }        
   }

   //check if flight path exists
   if (Game.class $= "SiegeGame")
      %team = Game.offenseTeam == %client.team ? 1 : 2;
   else
      %team = %client.team;

   if(! isObject(nameToId("T" @ %team @ "BikePath" @ firstWord(%this.paths))))
   {
      error("AIOEasyRider Error! - No Paths field specified in AIOEasyRider objective Marker");
      return 0;
   }

   //do a quick check to disqualify this objective if it can't meet the minimum weight
   if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
   {
      if (%this.targetObjectId > 0 && %this.issuedByClientId == %client.controlByHuman)
      {
         if ($AIWeightHumanIssuedCommand < %minWeight)
	    return 0;
      }
      else
         return 0;
   }

   //check for Vehicle station near buy of forget it if we are not mounted yet or didn't just buy - Lagg...
   if (!%client.player.isMounted() && !%client.justBought)
   {
      %clVs = AIFindClosestVStation(%client, %this.position);
      if (%clVs > 0)
      {
         %closestVs = getWord(%clVs, 0);
         %closestVsDist = getWord(%clVs, 1);
         if (%closestVsDist > 15)
         {
            //error("AIOEasyRider Error! - Marker More Than 15Meters From A Team Vehicle Station");
            return 0;
         }

         if (VectorDist (%client.player.position, %this.position) > 300)//---* close to VPad or return 0 *---
            return 0;
      }
      else
      {
         //error("AIOEasyRider Error! - No VPAD");
         return 0;
      }

      //see if this vehicle is allowed at the VPad in question
      if (%closestVs.ScoutVehicle $= "Removed")
      {
         error("AIOEasyRider Error! - ScoutVehicle not allowed at VPad :(");
         return 0;
      }

      //check if any of vehicle type are availible
      %blockName = "ScoutVehicle";
      if (!vehicleCheck(%blockName, %client.team))
         return 0;
   }

   %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);

   //never bump a shrike pilot from his ride
   if (%this.clientLevel1 > 0  && %level == 1 && %this.clientLevel1 != %client)
      return 0;
   else if (%this.clientLevel1 == %client)
      %weight = 20000;

   return %weight;
}

function AIOEasyRider::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIEasyRider);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIOEasyRider::unassignClient(%this, %client)
{
   if(%client.pilotVehicle)
      {
         AIDisembarkVehicle(%client);
      }  
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//----------------------------------------------------------------------------------------------- AI Easy Rider ---

function AIEasyRider::initFromObjective(%task, %objective, %client)
{
   //initialize the task vars from the objective
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;
   %task.location = %objective.position;

   %task.equipment = %objective.equipment;
   %task.buyEquipmentSet = %objective.buyEquipmentSet;
   %task.desiredEquipment = %objective.desiredEquipment;
   %task.issuedByClient = %objective.issuedByClientId;
   %task.chat = %objective.chat;

   //initialize other task vars
   %task.sendMsg = true;
   %task.sendMsgTime = 0;
   %task.paths = %objective.paths;
   %client.player.mVehicle = "";// needed if bot just hopped out a vehicle and it was not destroyed/removed yet
}

function AIEasyRider::assume(%task, %client)
{
   //set frequency
   %task.setWeightFreq(12);
   %task.setMonitorFreq(12);
   
   if (%task.equipment !$= "")
      %equipmentList = %task.equipment;
   else
      %equipmentList = %task.desiredEquipment;
   %client.needEquipment = AINeedEquipment(%equipmentList, %client);

   //even if we don't *need* equipemnt, see if we should buy some... 
   if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
   {
      //see if we could benefit from inventory
      %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      %closestDist = getWord(%result, 1);
      if (AINeedEquipment(%task.desiredEquipment, %client) && %closestInv > 0)
      {
         %result = AIFindClosestEnemyToLoc(%client, %task.location, 50, $AIClientLOSTimeout);
	 %closestEnemy = getWord(%result, 0);

         if (%closestEnemy <= 0 && %closestDist < 100)
            %client.needEquipment = true;
      }
   }

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();

   //reset the vehicle station wait time
   %client.vsWaitTime = "";

   //set the destination paths for each team and game type
   
   //for siege game type
   if (Game.class $= "SiegeGame")
      %team = Game.offenseTeam == %client.team ? 1 : 2;
   else
      %team = %client.team;

   //first see how many paths we have
   %mx = getWordCount(%task.paths);

   //and which path to use
   %random = mFloor(getRandom(1, %mx));
   %random--;
   %tg = getWord(%task.paths, %random);

   %task.group = nameToId("T" @ %team @ "BikePath" @ %tg);

   //echo("AIEasyRider::assume - path = " @ %task.group.getName());

   %task.count = %task.group.getCount();
   %task.locationIndex = 0;

   %client.needVehicle = true;
}


function AIEasyRider::retire(%task, %client)
{
   if (aiClientIsAlive(%client))
   {
      %client.needVehicle = false;
      %client.clearStep();
      if(%client.player.isMounted())
         AIDisembarkVehicle(%client);
   }
}

function AIEasyRider::weight(%task, %client)
{
   //update the task weight...
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;

   //let the monitor decide when to quit :)//--- NEEDED SO IMPORTANT !
   %task.setWeight(%task.baseWeight);

}

function AIEasyRider::monitor(%task, %client)
{   
   //first, buy equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;
      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
         return;
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(10);
	 %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
         if (%client.player.getArmorSize() $= "Light")
         {
            %task.setMonitorFreq(10);
	    %client.needEquipment = false;
         }
         else if (%task == %client.objectiveTask)
	 {
	    AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
            return;
	 }
      }
   }
   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   //chat - send the message
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
	 {
	    if (%task.chat !$= "")
	    {
	       %chatMsg = getWord(%task.chat, 0);
	       %chatTemplate = getWord(%task.chat, 1);
	       if (%chatTemplate !$= "")
		  AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
	       else
		  AIMessageThread(%task.chat, %client, -1);
	    }
         }
      }
   }

   if (%client.needVehicle)
   {
      %clVs = AIFindClosestVStation(%client, %task.location);
      if (%clVs > 0)
      {
         %closestVs = getWord(%clVs, 0);
         %closestVsDist = getWord(%clVs, 1);
      }
      if (%closestVs > 0 && %closestVsDist < 300 && !isObject(%client.player.mVehicle))
      {
         //If we're in light or medium armor, buy the vehicle - Lagg...
         if (%client.player.getArmorSize() $= "Light")
         {        
            %task.setMonitorFreq(9);
            %buyResult = aiBuyVehicle(ScoutVehicle, %client, %closestVs);
         }
         else
         {
            //if ai is not in light armor buy equipment
            if (%task == %client.objectiveTask)
	    {
               %task.baseWeight = %client.objectiveWeight;
               %task.equipment = "Light";
	       %task.buyEquipmentSet = "LightEnergyDefault";
               %client.needEquipment = true;
               return;
	    }
         }
           
         if (%buyResult $= "InProgress")
         {
            //clear offensive tags
            %client.lastDamageClient = -1;
            %client.lastDamageTurret = -1;
            %client.shouldEngage = -1;
            %client.setEngageTarget(-1);
            %client.setTargetObject(-1);
            %client.engageRemeq = -1;
            //%client.pickUpItem = -1;
	    return;
         }

         else if (%buyResult $= "Finished")
         {
            //if we are finished buying the vehicle, then we are done
            //this is handled in function Armor::AIonMount - Lagg... 7-9-2004
         }
         else if (%buyResult $= "Failed")
         {
            //if this task is the objective task, choose a new objective
	    if (%task == %client.objectiveTask)
	    {
	       AIUnassignClient(%client);
	       Game.AIChooseGameObjective(%client);
               return;
	    }
         }
      }
      else if ((%closestVs <= 0 || %closestVsDist >= 300) && !isObject(%client.player.mVehicle))
      {
         if (%task == %client.objectiveTask)
         {
            AIUnassignClient(%client);
            Game.AIChooseGameObjective(%client);
            return;
         }
      }
   }

   //let set some variables

   //if we managed to get in vehicle then go
   if (%client.player.isMounted() && isObject(%client.player.mVehicle))
   {
      //set low frequency
      %task.setMonitorFreq(10);
 
      //get the vehicle and velocity
      %vehicle = %Client.vehicleMounted;
      %mySpd = VectorLen(%vehicle.getVelocity());
      
      //make sure we got in the correct vehicle
      if (%vehicle.getDataBlock().getName() !$= "scoutVehicle")
      {
         error("AIEasyRider::monitor - opps we got in wrong vehicle");//believe it or not but i have seen everything - Lagg...
         AIDisembarkVehicle(%client); //Hop off...
      }

      //set the destination, speed and pitch
      %location = %task.group.getObject(%task.locationIndex).position;

      //echo("LOCATION = " @ %location);

      %speed = %task.group.getObject(%task.locationIndex).speed;
      if (%speed $= "")
         %speed = 1.0;
      %client.setPilotPitchRange(-0.2, 0.05, 0.05);

      //---------------------------------------------------------------------------------------------------------------------- Avoidance ---
      if (%mySpd < 0.1)
      { 
         InitContainerRadiusSearch(%vehicle.getWorldBoxCenter(), 5.0, $TypeMasks::InteriorObjectType | $TypeMasks::VehicleObjectType
           | $TypeMasks::TSStaticShapeObjectType);
         %avoid = containerSearchNext();

         if (%avoid = %vehicle)
            %avoid = containerSearchNext();

         //if we are moving slow and close to objects we are probably stuck
         if (%avoid)
         {
            %vx = getWord(%vehicle.getWorldBoxCenter(), 0);
            %vy = getWord(%vehicle.getWorldBoxCenter(), 1);
            %vz = getWord(%vehicle.getWorldBoxCenter(), 2);
            %vz += 2.0;
            %vR = getWords(%vehicle.getTransform(), 3, 6);

            %client.setPilotDestination(%location, 1.0);//max speed
            %vehicle.setTransform(%vx SPC %vy SPC %vz SPC %vR);//------------------------ set the Transform Here up a little

            error("AIEasyRider Avoidance we are stuck");
            //return;//i think we need this here
         }
      }            
      //or just drive to the next marker
      else 
      {
         //first check for potential targets
         %vehicle = %Client.vehicleMounted;

         //driver see if anybody close to run down (cheat ignor LOS) :) - Lagg...
         %losTimeout = $AIClientMinLOSTime + ($AIClientLOSTimeout * %client.getSkillLevel());
         %result = AIFindClosestEnemyToLoc(%client, %client.player.getWorldBoxCenter(), 70, %losTimeout, true, false);
         %closestEnemy = getWord(%result, 0);
         %closestdist = getWord(%result, 1);

         //check for obstacles
         %vLoc = %vehicle.getWorldBoxCenter();
         InitContainerRadiusSearch(%vLoc, 30, $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType |
           $TypeMasks::VehicleObjectType);
         %obs = containerSearchNext();
         if (%obs = %vehicle)
            %obs = containerSearchNext();

         //if driver spotted a target
         if (%closestEnemy > 0 && %closestdist < 70 && !%obs)//run em dowm even if gunner has target
         {
            //error("AIEasyRider::monitor - we got target Run Em Down!");

            %client.setPilotDestination(%closestEnemy.position, 1.0);//run em down!
            %client.aimAt(%closestEnemy.position, 1000); 
         }
         //if there is no enemy, follow path
         else
         {
            //echo("AIEasyRider::monitor - following path - " @ getTaggedString(%client.name));

            %pos2D = getWord(%client.vehicleMounted.position, 0) SPC getWord(%client.vehicleMounted.position, 1) SPC "0";
            %dest2D = getWord(%location, 0) SPC getWord( %location, 1) SPC "0";

            //are we close to location index marker?
            if (VectorDist(%dest2D, %pos2D) < 40)//40 meters from marker
            {
               //if we have another location index
               if ((%task.count - 1) > %task.locationIndex)//has to be -1 on the groups count
                  %task.locationIndex++;
               //else we are at end of trail
               else
               {
                  if (%task == %client.objectiveTask)
                  {
                     if (%client.vehicleMounted )
                     {
                        AIDisembarkVehicle(%client); //Hop off...
                        %client.stepMove(%location, 0.25);
                        return;
                     }
                  }
               }
            }
            
            //or just drive to marker location
            %client.setPilotDestination(%location, %speed);
         }
      }
   }
   //else task must be completed
   else// if (%task == %client.objectiveTask)
   {
      AIUnassignClient(%client);
      Game.AIChooseGameObjective(%client);
      return;
   }
}
