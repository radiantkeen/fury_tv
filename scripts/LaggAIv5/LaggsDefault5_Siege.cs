package LaggsDefault5_Siege {

// if you want to make canges to any of the functions in this file (like changing spawns or any custom stuff )
// you would make a copy of this file, rename it and use the new version ONLY for the map with cutom changes.
// Do not use this file and the custom in the same map as one of the pacakges would never be closed down.

//-------------------------------------------------------------------------------------------- begin LaggsDefault4_Siege ---***---

//------------------------------------------------------------------------------------------------ SiegeGame::gameOver ---

function SiegeGame::gameOver( %game )
{	
   deactivatePackage(LaggsDefault4_Siege);
   error("SiegeGame::gameOver - deacitavting LaggsDefault4_Siege package");
   SiegeGame::gameOver( %game );
}

//----------------------------------------------------------------------------------------- SiegeGame::missionLoadDone ---

function SiegeGame::missionLoadDone(%game)
{
   //this is where you want to set custom spawn weights, if any - Lagg...

   //call the parent
   Parent::missionLoadDone(%game);

   //call after parent
   Game.defenseTeam = Game.offenseTeam == 1 ? 2 : 1;
}

//------------------------------------------------------------------------------------------------ SiegeGame::halftime ---

function SiegeGame::halftime(%game, %reason)
{
   //this is where you set up custom spawn wieghts - Lagg...

   //call the parent
   Parent::halftime(%game, %reason);

   // z0dd - ZOD, 5/17/02. Clean up deployables triggers - I stole it - Lagg...
   cleanTriggers(nameToID("MissionCleanup/Deployables"));

   //clear bottom print messages
   clearBottomPrintAll();

   error("SiegeGame::halftime - clearing Bottom Print Messages");
}

//-------------------------------------------------------------------------------------------- SiegeGame::halftimeOver ---

function SiegeGame::halftimeOver( %game )
{
   // drop all players into mission
   %game.dropPlayers();

   //setup the AI for the second half
   %game.aiHalfTime();
   
   // start the mission again (release players)
   %game.halfTimeCountDown( 0 );

   //redo the objective waypoints
   %game.findObjectiveWaypoints();

   //set up ai vehicle functions for second half - Lagg... 10-2-2003   
   SweepForPads(NameToID(MissionGroup));
}

//-------------------------------------------------------------------------------------------- SiegeGame::resetPlayers ---
function SiegeGame::resetPlayers(%game)
{
   // go through the client group and reset anything the players were using
   // is most of this stuff really necessary?
   %count = ClientGroup.getCount();
   for(%cl = 0; %cl < %count; %cl++)
   {
      %client = ClientGroup.getObject(%cl);
      %player = %client.player;

      // clear the pack icon
      messageClient(%client, 'msgPackIconOff', "");
      // if player was firing, stop firing
      if(%player.getImageTrigger($WeaponSlot))// - this spams at siege halftime - Lagg... 3-31-2005
         %player.setImageTrigger($WeaponSlot, false);

      // if player had pack activated, deactivate it
      if(%player.getImageTrigger($BackpackSlot))// - this spams at siege halftime - Lagg... 3-31-2005
         %player.setImageTrigger($BackpackSlot, false);

      // if player was in a vehicle, get rid of vehicle hud
      commandToClient(%client, 'setHudMode', 'Standard', "", 0);

      // clear player's weapons and inventory huds
      %client.SetWeaponsHudClearAll();
      %client.SetInventoryHudClearAll();

      // if player was at a station, deactivate it
      if(%player.station)
      {
         %player.station.triggeredBy = "";
         %player.station.getDataBlock().stationTriggered(%player.station, 0);
         if(%player.armorSwitchSchedule)
            cancel(%player.armorSwitchSchedule);
      }

      // if piloting a vehicle, reset it (assuming it doesn't get deleted)
      if(%player.lastVehicle.lastPilot)
         %player.lastVehicle.lastPilot = "";
   }
}

//----------------------------------------------------------------------------------- SiegeGame::objectSwapVehiclePads ---

function SiegeGame::objectSwapVehiclePads(%game, %this)
{
   %defTeam = %game.offenseTeam == 1 ? 2 : 1;

   if(%this.getDatablock().getName() $= "StationVehicle")
   {
      if(%this.getTarget() != -1)
      {
         // swap the teams of both the vehicle pad and the vehicle station
         if(getTargetSensorGroup(%this.getTarget()) == %game.offenseTeam)
         {
            setTargetSensorGroup(%this.getTarget(), %defTeam);
            %this.team = %defTeam;
            setTargetSensorGroup(%this.pad.getTarget(), %defTeam);
            %this.pad.team = %defTeam;
            // z0dd - ZOD, 4/20/02. Switch the teleporter too.
            setTargetSensorGroup(%this.teleporter.getTarget(), %defTeam);
            %this.teleporter.team = %defTeam;
         }
         else if(getTargetSensorGroup(%this.getTarget()) == %defTeam)
         {
            setTargetSensorGroup(%this.getTarget(), %game.offenseTeam);
            %this.team = %game.offenseTeam;
            setTargetSensorGroup(%this.pad.getTarget(), %game.offenseTeam);
            %this.pad.team = %game.offenseTeam;
            // z0dd - ZOD, 4/20/02. Switch the teleporter too.
            setTargetSensorGroup(%this.teleporter.getTarget(), %game.offenseTeam);
            %this.teleporter.team = %game.offenseTeam;
         }
      }
   }
}

//---------------------------------------------------------------------------------------- SiegeGame::vehicleDestroyed ---

//siege game spam fix
function SiegeGame::vehicleDestroyed(%game, %vehicle, %destroyer)
{
  //needed to prevent Siege Game consol spam - Lagg... 11-12-2003   
}

//-------------------------------------------------------------------------------------- StaticShapeData::damageObject ---

function StaticShapeData::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType)
{
   %targDB = %targetObject.getDataBlock().getName();

   //base items are invincible on offensive team, send a message to all who try
   if (%targetObject.team == Game.offenseTeam && (%targDB $= "StationInventory" || %targDB $= "GeneratorLarge" || %targDB $= "TurretBaseLarge" ||
     %targDB $= "SentryTurret" || %targDB $= "SensorLargePulse" || (%data.noDamageInSiege && Game.class $= "SiegeGame")))
   {
      if(%sourceObject && %targetObject.isEnabled())
      {
         //if a player
         if(%sourceObject.client)
            messageClient(%sourceObject.client, 0, "\c5Offensive Base Equipment Invincible.~wfx/powered/station_denied.wav");
         //a shrike
         else if (%sourceObject.getDataBlock().catagory $= "Vehicles")
         {
            if(%sourceObject.getDatablock().numMountPoints > 0)
            {
               for(%i = 0; %i < %sourceObject.getDatablock().numMountPoints; %i++)
               {
                  if (%sourceObject.getMountNodeObject(%i) > 0)
                     messageClient( %sourceObject.getMountNodeObject(%i).client, 0, "\c5Offensive Base Equipment Invincible.~wfx/powered/station_denied.wav");
               }
            }
         }
         //or a bomber
         else if(%sourceObject.getDataBlock().getName() $= "BomberTurret")
         {
            %veh = %sourceObject.vehicleMounted;
            if(%veh.getDatablock().numMountPoints > 0)
            {
               for(%i = 0; %i < %veh.getDatablock().numMountPoints; %i++)
               {
                  if (%veh.getMountNodeObject(%i) > 0)
                     messageClient( %veh.getMountNodeObject(%i).client, 0, "\c5Offensive Base Equipment Invincible.~wfx/powered/station_denied.wav");
               }
            }
         }
         //or a tank
         else if(%sourceObject.getDataBlock().getName() $= "AssaultPlasmaTurret")
         {
            %veh = %sourceObject.getObjectMount();
            if(%veh.getDatablock().numMountPoints > 0)
            {
               for(%i = 0; %i < %veh.getDatablock().numMountPoints; %i++)
               {
                  if (%veh.getMountNodeObject(%i) > 0)
                     messageClient( %veh.getMountNodeObject(%i).client, 0, "\c5Offensive Base Equipment Invincible.~wfx/powered/station_denied.wav");
               }
            }
         }     
      }

      return;
   }

   if(%sourceObject && %targetObject.isEnabled())
   {
      if(%sourceObject.client)
      {
        %targetObject.lastDamagedBy = %sourceObject.client;
        %targetObject.lastDamagedByTeam = %sourceObject.client.team;
        %targetObject.damageTimeMS = GetSimTime();
      }
      else
      {
        %targetObject.lastDamagedBy = %sourceObject;
        %targetObject.lastDamagedByTeam = %sourceObject.team;
        %targetObject.damageTimeMS = GetSimTime();
      }
   }

   // Scale damage type & include shield calculations...
   if (%data.isShielded)
      %amount = %data.checkShields(%targetObject, %position, %amount, %damageType);

   %damageScale = %data.damageScale[%damageType];
   if(%damageScale !$= "")
      %amount *= %damageScale;

    //if team damage is off, cap the amount of damage so as not to disable the object...
    if (!$TeamDamage && !%targetObject.getDataBlock().deployedObject)
    {
       // -------------------------------------
       // z0dd - ZOD, 6/24/02. Console spam fix
       if(isObject(%sourceObject))
       {
          //see if the object is being shot by a friendly
          if(%sourceObject.getDataBlock().catagory $= "Vehicles")
             %attackerTeam = getVehicleAttackerTeam(%sourceObject);
          else
             %attackerTeam = %sourceObject.team;
      }
      if ((%targetObject.getTarget() != -1) && isTargetFriendly(%targetObject.getTarget(), %attackerTeam))
      {
         %curDamage = %targetObject.getDamageLevel();
         %availableDamage = %targetObject.getDataBlock().disabledLevel - %curDamage - 0.05;
         if (%amount > %availableDamage)
            %amount = %availableDamage;
      }
    }

   // if there's still damage to apply
   if (%amount > 0)
      %targetObject.applyDamage(%amount);
}

//-------------------------------------------------------------------------------------------- SimGroup::housekeeping ---
function SimGroup::housekeeping(%this)
{
   // delete selectively in the MissionCleanup group
   %count = %this.getCount();
   // have to do this backwards or only half the objects will be deleted
   //if (%count > 0)
   //{
   for(%i = (%count - 1); %i > -1; %i--)
   {
      %detritus = %this.getObject(%i);
      
      
      //error("HouseKeeping - %detritus.getName = " @ %detritus.getName());//******************
      //error("HouseKeeping - %detritus.getClassName = " @ %detritus.getClassName());//******************
      //error("HouseKeeping - %detritus.dataBlock.getName = " @ %detritus.getDataBlock().getName());//******************
      
      //lets delete the AiObjectiveQ now to avoid spam - Lagg... 3-31-2005
      if(%detritus.getClassName() $= "AIObjectiveQ")
      {
         %detritus.delete();
      }
      //lets delete Deployables Simgroup now to avoid spam - Lagg... 3-31-2005 // check this *********
      else if(%detritus.getName() $= "Deployables")
      {
         %detritus.delete();

         // simgroup of physical zones for force fields
         // don't delete them
      }
      else if(%detritus.getClassName() $= "SimSet")
      {
         // I don't think there are any simsets we want to delete
      }
      else if (%detritus.getClassName() $= "Camera")
      {
         // Cameras should NOT be deleted
      }
      else if(%detritus.getName() $= "PZones")
      {
         // simgroup of physical zones for force fields
         // don't delete them
      }
      //else if (%detritus.getClassName() $= "ScriptObject")
      //{
         //// this will be the game type object.
         //// DEFINITELY don't want to delete this.
      //}
      else if(%detritus.getName() $= PosMarker)
      {
         //Needed to reset non static objects...
      }
      else if((%detritus.getName() $= "TeamDrops1") || (%detritus.getName() $= "TeamDrops2"))
      {
         // this will actually be a SimSet named TeamDropsN (1 or 2)
         // don't want to delete the spawn sphere groups, so do nothing
      }
      else if (%detritus.getName() $= "PlayerListGroup")
      {
         // we don't want to delete PlayerListGroup (SimGroup)
      }
      else if (%detritus.getDatablock().getName() $= "stationTrigger")
      {
         //we don't want to delete triggers for stations
      }
      else if (%detritus.getDatablock().getName() $= "NewTeleportTrigger")
      {
         //we don't want to delete triggers for Sparky's Teleporters
      }
      else if (%detritus.getDatablock().getName() $= "BaseLogo")
      {
         //we don't want to delete team logos for Sparky's Teleporters
      }
      else if (%detritus.getDatablock().getName() $= "BaseBLogo")
      {
         //we don't want to delete team logos for Sparky's Teleporters
      }
      else if (%detritus.getDatablock().getName() $= "StationVehicle")
      {
         // vehicle stations automatically get placed in MissionCleanup in a
         // position near the vehicle pad. Don't delete it.
      }
      else
      {
         //if (%detritus.getName() !$= "")
            //echo("we are deleting by name - " @ %detritus.getName());
         //else if (%detritus.getClassName() !$= "")
            //echo("We are deleting by ClassName - " @  %detritus.getClassName());
         //else if (%detritus.getDataBlock().getName() !$= "")
            //echo("we are deleting by dataBlockName - " @ %detritus.getDataBlock().getName());
            
         
         //error("SimGroup::housekeeping - Detritus = " @ %detritus.getDataBlock().getclassName());

         // this group of stuff to be deleted should include:
         // mines, deployed objects, projectiles, explosions, corpses,
         // players, and the like.
         %detritus.delete();
      }
   //}
   }
}

//---------------------------------------------------------------------- cleanTriggers ---
// Removes triggers from Siege when players switch sides - called from SiegeGame::halftime
//                                                                  taken from classic mod
function cleanTriggers(%group)
{ 
   if (%group > 0)
      %depCount = %group.getCount();
   else
      return;

   for(%i = 0; %i < %depCount; %i++)
   {
      %deplObj = %group.getObject(%i);
      if(isObject(%deplObj))
      {
         if(%deplObj.trigger !$= "")
         {
            %deplObj.trigger.schedule(0, "delete");
            error("cleanTriggers - deleting a deployed trigger");
         }
      }
   }
}

//---------------------------------------------------------------------- SiegeGame::sendDebriefing ---
function SiegeGame::sendDebriefing( %game, %client )
{
   //if neither team captured
   %winnerName = "";
   if ( $teamScore[1] == 0 && $teamScore[2] == 0 )
      %winner = -1;

   //else see if team1 won
   else if ( $teamScore[1] > 0 && ( $teamScore[2] == 0 || $teamScore[1] < $teamScore[2] ) )
   {
      %winner = 1;
      %winnerName = $teamName[1];
   }

   //else see if team2 won
   else if ($teamScore[2] > 0 && ($teamScore[1] == 0 || $teamScore[2] < $teamScore[1]))
   {
      %winner = 2;
      %winnerName = $teamName[2];
   }

   //else see if it was a tie (right down to the millisecond - doubtful)
   else if ($teamScore[1] == $teamScore[2])
      %winner = 0;

   //send the winner message
   if (%winnerName $= 'Storm')
      messageClient( %client, 'MsgGameOver', "Match has ended.~wvoice/announcer/ann.stowins.wav" );
   else if (%winnerName $= 'Inferno')
      messageClient( %client, 'MsgGameOver', "Match has ended.~wvoice/announcer/ann.infwins.wav" );
   else if (%winnerName $= 'BloodEagle')
      messageClient( %client, 'MsgGameOver', "Match has ended.~wvoice/announcer/ann.bewin.wav" );
   else if (%winnerName $= 'Starwolf')
      messageClient( %client, 'MsgGameOver', "Match has ended.~wvoice/announcer/ann.swwin.wav" );
   else if (%winnerName $= 'Phoenix')
      messageClient( %client, 'MsgGameOver', "Match has ended.~wvoice/announcer/ann.pxwin.wav" );
   else if (%winnerName $= 'DiamondSword')
      messageClient( %client, 'MsgGameOver', "Match has ended.~wvoice/announcer/ann.dswin.wav" );
   else
      messageClient( %client, 'MsgGameOver', "Match has ended.~wvoice/announcer/ann.gameover.wav" );

   // Mission result:
   if (%winner > 0)
   {
      if (%winner == 1)
      {  
         if ($teamScore[2] == 0)
            messageClient(%client, 'MsgDebriefResult', "", '<just:center>Team %1 wins!', $TeamName[1]);
         else
         {
            %timeDiffMS = $teamScore[2] - $teamScore[1];
            messageClient(%client, 'MsgDebriefResult', "", '<just:center>Team %1 won by capturing the base %2 faster!', $TeamName[1], %game.formatTime(%timeDiffMS, true));
         }
      }
      else
      {
         if ($teamScore[1] == 0)
            messageClient(%client, 'MsgDebriefResult', "", '<just:center>Team %1 wins!', $TeamName[2]);
         else
         {
            %timeDiffMS = $teamScore[1] - $teamScore[2];
            messageClient(%client, 'MsgDebriefResult', "", '<just:center>Team %1 won by capturing the base %2 faster!', $TeamName[2], %game.formatTime(%timeDiffMS, true));
         }
      }
   }
   else
      messageClient( %client, 'MsgDebriefResult', "", '<just:center>The mission ended in a tie.' );

   // Game summary:
   messageClient( %client, 'MsgDebriefAddLine', "", '<spush><color:00dc00><font:univers condensed:18>SUMMARY:<spop>' );
   %team1 = %game.offenseTeam == 1 ? 2 : 1;
   %team2 = %game.offenseTeam;
   if ( $teamScore[%team1] > 0 )
   {  
      %timeStr = %game.formatTime($teamScore[%team1], true);
      messageClient( %client, 'MsgDebriefAddLine', "", '<bitmap:bullet_2><lmargin:24>%1 captured the %2 base for Team %3 in %4.<lmargin:0>', %game.capPlayer[%team1], $TeamName[%team2], $TeamName[%team1], %timeStr);
   }
   else
      messageClient( %client, 'MsgDebriefAddLine', "", '<bitmap:bullet_2><lmargin:24>Team %1 failed to capture the base.<lmargin:0>', $TeamName[%team1]);

   if ( $teamScore[%team2] > 0 )
   {  
      %timeStr = %game.formatTime($teamScore[%team2], true);
      messageClient( %client, 'MsgDebriefAddLine', "", '<bitmap:bullet_2><lmargin:24>%1 captured the %2 base for Team %3 in %4.<lmargin:0>', %game.capPlayer[%team2], $TeamName[%team1], $TeamName[%team2], %timeStr);
   }
   else
      messageClient( %client, 'MsgDebriefAddLine', "", '<bitmap:bullet_2><lmargin:24>Team %1 failed to capture the base.<lmargin:0>', $TeamName[%team2]);

   // List out the team rosters:
   messageClient( %client, 'MsgDebriefAddLine', "", '\n<spush><color:00dc00><font:univers condensed:18><clip%%:50>%1</clip><lmargin%%:50><clip%%:50>%2</clip><spop>', $TeamName[1], $TeamName[2] );
   %max = $TeamRank[1, count] > $TeamRank[2, count] ? $TeamRank[1, count] : $TeamRank[2, count];
   for ( %line = 0; %line < %max; %line++ )
   {
      %plyr1 = $TeamRank[1, %line] $= "" ? "" : $TeamRank[1, %line].name;
      %plyr2 = $TeamRank[2, %line] $= "" ? "" : $TeamRank[2, %line].name;
      messageClient( %client, 'MsgDebriefAddLine', "", '<lmargin:0><clip%%:50> %1</clip><lmargin%%:50><clip%%:50> %2</clip>', %plyr1, %plyr2 );
   }

   // Show observers:
   %count = ClientGroup.getCount();
   %header = false;
   for ( %i = 0; %i < %count; %i++ )
   {
      %cl = ClientGroup.getObject( %i );
      if ( %cl.team <= 0 )
      {
         if ( !%header )
         {
            messageClient( %client, 'MsgDebriefAddLine', "", '\n<lmargin:0><spush><color:00dc00><font:univers condensed:18>OBSERVERS<spop>' );
            %header = true;
         }

         messageClient( %client, 'MsgDebriefAddLine', "", ' %1', %cl.name );
      }
   }
}

//----------------------------------------------------------------------------------------- end LaggsDefualt4_Siege package ---***---
};




