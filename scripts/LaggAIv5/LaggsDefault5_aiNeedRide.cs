//-------------------------------------------------------------------------------------------------- AIO Need A Ride ---
//Place AIO Objective Marker in the spot you want pilot to park and wait for passegers
//Set up to 10 simgroups called "T" @ %team @ "HAPCPath" @ %num
//(example T2HAPCPath1 to T2HAPCPath10 / T1HAPCPath1 to T1HAPCPath10)
//NOTE: must start with number 1 and count in order up to 10
//inside these simgroups place markers in the order to follow for each
//path. Thats 1 simgroup per path (up to 10) for each team - Lagg... 4-8-2004

function AIONeedARide::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   // For CnH games, we set a target object and set def or offense (can be used for any gameType)
   if (%this.targetObjectId > 0 && %this.clientLevel1 != %client)
   {
      if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden())
         return 0;

      //if offense and we own target object quit
      if (%this.offense > 0)
      {
         if (%this.targetObjectId.team == %client.team || %this.targetObjectId.getDamageState() $= "Destroyed")
            return 0;
      }
      //if defense and we own it must be powered and not destroyed, or if enemy it must be destroyed or not powered
      else if (%this.defense > 0)
      {
         if (%this.targetObjectId.team == %client.team)
         {
            if (!%this.targetObjectId.isPowered() || %this.targetObjectId.getDamageState() $= "Destroyed")
               return 0;
         }
         else
         { 
            if (%this.targetObjectId.isPowered() && %this.targetObjectId.getDamageState() !$= "Destroyed")
              return 0;
         }
      }        
   }

   //check if flight path exists
   if (Game.class $= "SiegeGame")
      %team = Game.offenseTeam == %client.team ? 1 : 2;
   else
      %team = %client.team;

   if(! isObject(nameToId("T" @ %team @ "HAPCPath" @ firstWord(%this.paths))))
   {
      //if no hapc path than use bomber path
      if(! isObject(nameToId("T" @ %team @ "BomberPath" @ firstWord(%this.paths))))
      {
         error("AIONeedARide Error! - No Paths field specified in AIONeedARide objective Marker");
         return 0;
      }
   }

   //do a quick check to disqualify this objective if it can't meet the minimum weight
   if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
   {
      if (%this.targetObjectId > 0 && %this.issuedByClientId == %client.controlByHuman)
      {
         if ($AIWeightHumanIssuedCommand < %minWeight)
	    return 0;
      }
      else
         return 0;
   }

   //check for Vehicle station near buy of forget it if we are noy mounted yet - Lagg...
   //would like to add a check for unpiloted vehicles with passengers that need a ride (someday - DONE) - Lagg... 5-4-2005
   if (!%client.player.isMounted() && !%client.justBought)
   {
      %blockName = HAPCFlyer;

      //check to see if we can find a ride, if we found one stay with it
      if (%this.clientLevel1 == %client)
         %mode = NoBuy;
      else
         %mode = %this.mode;
      %abV = aiSearchForRide(%client, %blockName, %this.position, 250, %mode); 

      if (%abv <= 0 && (%this.mode $= "NoBuy" || %this.mode $= "PassengerOnly"))
         return 0;

      if (%abV <= 0)
      {
         %clVs = AIFindClosestVStation(%client, %this.position);
         if (%clVs > 0)
         {
            %closestVs = getWord(%clVs, 0);
            %closestVsDist = getWord(%clVs, 1);
            if (%closestVsDist > 15)
            {
               //error("AIONeedARide Error! - Marker More Than 15Meters From A Team Vehicle Station");
               return 0;
            }
            if (VectorDist (%client.player.position, %this.position) > 300)//---* close to VPad or return 0 *---
               return 0;
         }
         else
            return 0;

         //see if this vehicle is allowed at the VPad in question
         if (%closestVs.HAPCFlyer $= "Removed")
         {
            error("AIONeedARide Error! - HAPC Not Allowed At This Vehicle Pad");
            return 0;
         }

         //check if any of vehicle type are availible
         if (!vehicleCheck(%blockName, %client.team))
            return 0;
      }
   }

   %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);

   //never bump a pilot off his ride
   if (%this.clientLevel1 > 0  && %level == 1 && %this.clientLevel1 != %client)
      return 0;
   if (%this.clientLevel1 == %client)
      %weight = 10000;

   return %weight;
}

function AIONeedARide::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AINeedARide);
   %client.objectiveTask.initFromObjective(%this, %client);

   //create the escort (passenger) objectives

   //pick a random equipment set...
   %randNum = getRandom();
   if (%randNum < 0.4)
   {
      %desEquip = "InventoryDeployable";
      %set = "HeavyInventorySet";
   }
   else if (%randNum < 0.6)
   {
      %desEquip = "EnergyPack";
      %set = "HeavyEnergyDefault";
   } 
   else if (%randNum < 0.8)
   {
      %desEquip = "SensorJammerPack";
      %set = "HeavyEnergySet";
   }
   else if (%randNum < 0.9)
   {
      %desEquip = "ShieldPack";
      %set = "HeavyShieldOff";
   }
   else
   {
      %desEquip = "AmmoPack";
      %set = "HeavyTailgunner";
   }

   %client.escort = new AIObjective(AIOEscortPlayer)
                     {
						      dataBlock = "AIObjectiveMarker";
                        weightLevel1 = $AIWeightVehicleMountedEscort;//was 5000 - lagg...
                        weightLevel2 = 0;
                        description = "Escort " @ getTaggedString(%client.name);
                        targetClientId = %client;
                        offense = true;
                        chat = "ChatNeedHold";
                        desiredEquipment = "FlareGrenade";
			buyEquipmentSet = "HeavyTailgunner";
                     };

   %client.escort1 = new AIObjective(AIOEscortPlayer)
                     {
						      dataBlock = "AIObjectiveMarker";
                        weightLevel1 = $AIWeightVehicleMountedEscort;//was 5000 - lagg...
                        weightLevel2 = 0;
                        description = "Escort " @ getTaggedString(%client.name);
                        targetClientId = %client;
                        offense = true;
                        chat = "ChatNeedRide";
                        desiredEquipment = %desEquip;
                        buyEquipmentSet = %set;
                     };

   //pick a random equipment set...
   %randNum = getRandom();
   if (%randNum < 0.4)
   {
      %desEquip = "InventoryDeployable";
      %set = "MediumInventorySet";
   }
   else if (%randNum < 0.5)
   {
      %desEquip = "EnergyPack";
      %set = "HeavyEnergyDefault";
   } 
   else if (%randNum < 0.99)
   {
      %desEquip = "SensorJammerPack";
      %set = "HeavyEnergySet";
   }
   else
   {
      %desEquip = "AmmoPack";
      %set = "HeavyTailgunner";
   }

   %client.escort2 = new AIObjective(AIOEscortPlayer)
                     {
						      dataBlock = "AIObjectiveMarker";
                        weightLevel1 = $AIWeightVehicleMountedEscort;//was 5000 - lagg...
                        weightLevel2 = 0;
                        description = "Escort " @ getTaggedString(%client.name);
                        targetClientId = %client;
                        offense = true;
                        chat = "ChatNeedRide";
                        desiredEquipment = %desEquip;
			buyEquipmentSet = %set;
                     };

   MissionCleanup.add(%client.escort);
   MissionCleanup.add(%client.escort1);
   MissionCleanup.add(%client.escort2);

   //if is siege game we have to do this right - Lagg... 11-3-2003
   if (Game.class $= "SiegeGame")
   {
      //the objectives in $ObjectiveQ[1] are all offense objectives, $ObjectiveQ[2] defense
      if (%client.team == game.offenseTeam)
      {
         $ObjectiveQ[1].add(%client.escort);
         $ObjectiveQ[1].add(%client.escort1);
         $ObjectiveQ[1].add(%client.escort2);
      }
      else
      {
         $ObjectiveQ[2].add(%client.escort);
         $ObjectiveQ[2].add(%client.escort1);
         $ObjectiveQ[2].add(%client.escort2);
      }
   }
   //else if not siege game use the default team's Q
   else
   {
         $ObjectiveQ[%client.team].add(%client.escort);
         $ObjectiveQ[%client.team].add(%client.escort1);
         $ObjectiveQ[%client.team].add(%client.escort2);
   }
}

function AIONeedARide::unassignClient(%this, %client)
{
   //kill the escort (passenger) objectives
   if (%client.escort)
   {
      clearObjectiveFromTable(%client.escort);//new function below to clear objective from $objTable[] - Lagg... 1-27-2004
      AIClearObjective(%client.escort);
      %client.escort.delete();
      %client.escort = "";
   }
   if (%client.escort1)
   {
      clearObjectiveFromTable(%client.escort1);//new function below to clear objective from $objTable[] - Lagg... 1-27-2004
      AIClearObjective(%client.escort1);
      %client.escort1.delete();
      %client.escort1 = "";
   }
   if (%client.escort2)
   {
      clearObjectiveFromTable(%client.escort2);//new function below to clear objective from $objTable[] - Lagg... 1-27-2004
      AIClearObjective(%client.escort2);
      %client.escort2.delete();
      %client.escort2 = "";
   }            
   if(%client.pilotVehicle)
      AIDisembarkVehicle(%client);
  
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//----------------------------------------------------------------------------------------------- AI Need A Ride ---

function AINeedARide::initFromObjective(%task, %objective, %client)
{
   //initialize the task vars from the objective
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;

   //task location gets updated as pilot flys to markers in monitor
   %task.location = %objective.position;
   %task.mode = %objective.mode;
   %task.equipment = %objective.equipment;
   %task.buyEquipmentSet = %objective.buyEquipmentSet;
   %task.desiredEquipment = %objective.desiredEquipment;
   %task.issuedByClient = %objective.issuedByClientId;
   %task.chat = %objective.chat;

   //initialize other task vars
   %task.sendMsg = true;
   %task.sendMsgTime = 0;
   %task.paths = %objective.paths;

   %client.player.mVehicle = "";// needed if bot just hopped out a vehicle and it was not destroyed/removed yet
}

function AINeedARide::assume(%task, %client)
{
   %task.setWeightFreq(40);
   %task.setMonitorFreq(40);
   
   %client.needEquipment = AINeedEquipment(%task.equipment, %client);

   if (%task.mode $= "PassengerOnly")
   {
      if (%client.player.getArmorSize() !$= "Heavy")
         %client.needEquipment = false;
   }
   //even if we don't *need* equipemnt, see if we should buy some... 
   else if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
   {
      //see if we could benefit from inventory
      %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      %closestDist = getWord(%result, 1);
      if (AINeedEquipment(%task.desiredEquipment, %client) && %closestInv > 0)
      {
         if (%closestDist < 100)
	    %client.needEquipment = true;
      }
   }

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();

   //reset the vehicle station wait time
   %client.vsWaitTime = "";

   //set the destination paths for each team and game type

   //for siege game type
   if (Game.class $= "SiegeGame")
      %team = Game.offenseTeam == %client.team ? 1 : 2;
   else
      %team = %client.team;
   
   //first see how many paths we have
   %mx = getWordCount(%task.paths);
   
   //and which path to use
   %random = mFloor(getRandom(1, %mx));
   %random--;
   %tg = getWord(%task.paths, %random);

   if(isObject(nameToId("T" @ %team @ "HAPCPath" @ %tg)))
      %task.group = nameToId("T" @ %team @ "HAPCPath" @ %tg);
   else
      %task.group = nameToId("T" @ %team @ "BomberPath" @ %tg);
   %task.count = %task.group.getCount();
  
   %task.locationIndex = 0;
   %client.needVehicle = true;
   %task.shouldWait = true;

   //set up the waitSpot for escort objectives
   %task.waitSpot = %task.location;
   for (%i = 0; %i < %task.count; %i ++)
   {
      if (%task.group.getObject(%i).waitTime !$= "")
      {
         %task.waitSpot = %task.group.getObject(%i).position;
         break;
      }
   } 
}

function AINeedARide::retire(%task, %client)
{
   if (aiClientIsAlive(%client))
   {
      %client.needVehicle = false;
      %client.clearStep();
      if(%client.player.isMounted())
         AIDisembarkVehicle(%client);
   }
}

function AINeedARide::weight(%task, %client)
{
   //update the task weight...
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;

   //let the monitor decide when to quit :)//--- NEEDED SO IMPORTANT !
   %task.setWeight(%task.baseWeight);

}

function AINeedARide::monitor(%task, %client)
{
   //chat - send the message
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
	 {
	    if (%task.chat !$= "")
	    {
	       %chatMsg = getWord(%task.chat, 0);
	       %chatTemplate = getWord(%task.chat, 1);
	       if (%chatTemplate !$= "")
		  AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
	       else
		  AIMessageThread(%task.chat, %client, -1);
	    }
         }
      }
   }

   //buy equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;
      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
         return;
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(40);
	 %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
         if (%client.player.getArmorSize() $= "Heavy")
         {
            //if this task is the objective task, choose a new objective
	    if (%task == %client.objectiveTask)
	    {
               error("AINeedARide::monitor - Pilot is Heavy No Inventories " @ getTaggedString(%client.name));

	       AIUnassignClient(%client);
	       Game.AIChooseGameObjective(%client);
               return;
	    }
         }
         else
         {
            %task.setMonitorFreq(40);
	    %client.needEquipment = false;
         }
      }
   }
   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   if (%client.needVehicle)
   {
      //If we're in light or medium armor, buy the vehicle - Lagg...
      if (%client.player.getArmorSize() $= "Heavy")
      {
         if (%task == %client.objectiveTask)
         {
            %task.baseWeight = %client.objectiveWeight;
            %task.equipment = "Medium";
            %task.buyEquipmentSet = "MediumMissileSet";
            %client.needEquipment = true;
            return;
         }
      }
      %blockName = HAPCFlyer;

      //check to see if we can steal a ride
      %abV = aiSearchForRide(%client, %blockName, %task.location, 250, NoBuy);

      if (%abV <= 0 && %task.mode !$= "NoBuy" && %task.mode !$= "PassengerOnly")
      {
         %clVs = AIFindClosestVStation(%client, %task.location);
         if (%clVs > 0)
         {
            %closestVs = getWord(%clVs, 0);
            %closestVsDist = getWord(%clVs, 1);
         }
         if (%closestVs > 0 && %closestVsDist < 300 && !isObject(%client.player.mVehicle))
         {
            //If we're in light or medium armor, buy the vehicle - Lagg...
            if (%client.player.getArmorSize() !$= "Heavy")
            {        
               %task.setMonitorFreq(9);
               %buyResult = aiBuyVehicle(%blockName, %client, %closestVs);
            }
            else
            {
               //if ai in heavy armor buy equipment
               if (%task == %client.objectiveTask)
	       {
                  %task.baseWeight = %client.objectiveWeight;
                  %task.equipment = "Medium";
	          %task.buyEquipmentSet = "MediumMissileSet";
                  %client.needEquipment = true;
                  return;
	       }
            }
           
            if (%buyResult $= "InProgress")
            {
               //reset pilot's wait time for passengers
               %task.shouldWaitTime = getSimTime();            
	       return;
            }

            else if (%buyResult $= "Finished")
            {
               //if we are finished buying the vehicle, then we are done
               //this is handled in function Armor::AIonMount - Lagg... 7-9-2004
            }
            else if (%buyResult $= "Failed")
            {
               //if this task is the objective task, choose a new objective
	       if (%task == %client.objectiveTask)
	       {
	          AIUnassignClient(%client);
	          Game.AIChooseGameObjective(%client);
                  return;
	       }
            }
         }
         else if ((%closestVs <= 0 || %closestVsDist >= 300) && !isObject(%client.player.mVehicle))
         {
            error("AINeedARide::monitor - No V Pad or To Far unassigning");
            if (%task == %client.objectiveTask)
            {
               AIUnassignClient(%client);
	       Game.AIChooseGameObjective(%client);
               return;
            }
         }
      }
      else if (%abV > 0)
      {
         %client.pilotVehicle = true;//needed to let ai mount pilot seat

         //reset pilot's wait time for passengers
         %task.shouldWaitTime = getSimTime();

         //if we are close to vehicle throw packs that won't fit
         if (hasLargePack(%client.player))
            %client.player.throwPack();

         if (%client.inWater)
         {
            %vPos2D = getWord(%abV.getPosition(), 0) SPC getWord(%abV.getPosition(), 1) SPC "0";
            %myPos2D = getWord(%client.player.position, 0) SPC getWord(%client.player.position, 1) SPC "0";
            if (vectorDist(%myPos2D, %vPos2D) < 2.5)
            {
               %client.pressJump();
               %client.stepMove(%abV.getWorldBoxCenter(), 0.25, $AIModeGainHeight);
            }
            else
               %client.stepMove(%abV.position, 0.25, $AIModeExpress);
         }
         else 
            %client.stepMove(getWords(%abv.getTransform(), 0, 2), 0.25, $AIModeMountVehicle);

         return;
      }
      else
      {
         error("AINeedARide::monitor - BIG FAILURE unassigning " @ getTaggedString(%client.name));

         if (%task == %client.objectiveTask)
            %client.needVehicle = false;
         else
            error("WE GOT A BIG  ProBLEM HERE !!! aiNeedRideMonitor - " @ getTaggedString(%client.name));
      }      
   }

   //if we managed to get in vehicle then go
   if (%client.player.isMounted() && isObject(%client.player.mVehicle))
   {
      //update monitor frequency
      %task.setMonitorFreq(20);

      //set a time limit on this task in case he gets stuck somewhere
      %time = getSimTime() - %client.getTaskTime();
      if (%time > $aiNeedARideTaskTime)
      {
         error("AINeedARide::monitor - TASK TIME IS UP, EVERYONE GET OUT !" @ getTaggedString(%client.name));
         //set the index to the last
         %task.locationIndex = %task.count - 1;

         //eject, eject, eject
         AIDisembarkVehicle(%client);
      }

      //get some vehicle data that we will require
      %vehicle = %Client.vehicleMounted;
      %mySpd = VectorLen(%vehicle.getVelocity());
      %vehicleDb = %vehicle.getDataBlock();
      %skill = %client.getSkillLevel();// --------------------- %skill *

      //make sure we got in the correct vehicle
      if (%vehicleDb.getName() !$= "HAPCFlyer" )
      {
         error("AINeedARide::monitor - opps we got in wrong vehicle");//believe it or not i have seen everything - Lagg...
         AIDisembarkVehicle(%client); //Hop off...
      }
      
      //set the flight path
      %location = %task.group.getObject(%task.locationIndex).position;
      %speed = %task.group.getObject(%task.locationIndex).speed;
      if (%speed $= "")
         %speed = 1.0;
      
      //find out our altitude 
      %myPos = %vehicle.getWorldBoxCenter();//---------------------------------------- %myPos *
      %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType |
        $TypeMasks::ForceFieldObjectType;
      %downVec = getWord(%myPos, 0) @ " " @ getWord(%myPos, 1) @ "0";
      %altSur = containerRayCast(%myPos, %downVec, %mask, 0);
      %altPos = posfromRayCast(%altSur);
      %terHt = getTerrainHeight(%altPos);// ------------------------------------------ %terHt *
      %myAlt = getWord(%myPos, 2) - getWord(%altPos, 2);// --------------------------- %myAlt *

      %myZLev = getWord(%myPos, 2);

      //---------------------------------------------------------------------------------- Avoidance --- 
      InitContainerRadiusSearch(%myPos, 5.0, $TypeMasks::InteriorObjectType
        | $TypeMasks::TSStaticShapeObjectType);
      %avoid = containerSearchNext();

      if ((%mySpd <= 0.1 && %avoid) || %mySpd == 0)//this needs some attention either %mySpd is 0 or it is not !
      {
         %vx = getWord(%myPos, 0);
         %vy = getWord(%myPos, 1);
         %vz = getWord(%myPos, 2);
         %vR = getWords(%vehicle.getTransform(), 3, 6);

         %client.setPilotPitchRange(-0.025, 0.025, 0.025);
         %client.setPilotDestination(%vx SPC %vy SPC %vz + 5, 1.0);// - max speed
         %vehicle.setTransform(%vx SPC %vy SPC %vz + 2 SPC %vR);// - set the Transform Here up a little
         %client.pressJet();// - hit the gas

         //error("AINeedARide::monitor - We Are Stuck - " @ getTaggedString(%client.name));
         //echo("AINeedARide::monitor - Altitude = " @ %myAlt);
         //echo("AINeedARide::monitor - SPEED = " @ %mySpd);
         //echo(" ");
      }
      //-------------------------------------------------------------------------------- End Avoidance --- 

      // - if we have an empty seat, wait for ($aiVehicleWaitTime) milliseconds

      //please come up to the gate with your boarding passes ready... 
      //if (%task.shouldWait)

      //this should make him follow path before a marker with a wait time is located :)
      if (%task.shouldWait && %task.group.getObject(%task.locationIndex).waitTime !$= "")
      {
         // - get the passenger seat: 0 = empty 1 = full
         for(%i = 0; %i < %vehicle.getDataBlock().numMountPoints; %i++)
         {
            %empty = false;
            if (!%vehicle.getMountNodeObject(%i))
            {
	       %empty = true;
	       break;
            }
         }

         //check if wait timer is up or Havoc is full
         if (!%empty || getSimTime() - %task.group.getObject(%task.locationIndex).waitTime > %task.shouldWaitTime)
            %task.shouldWait = false;

         //set the pitch
         %client.setPilotPitchRange(-0.025, 0.025, 0.025);//real smooth setting here

         // - if can't see location marker gain height
         %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
         %hasLOS = !containerRayCast(%myPos, %location, %mask, 0);//------- %hasLOS to %location ?

         //if we get blocked fly straight up a little
         if (!%hasLOS)
         {
            %x = firstWord(%myPos);
            %y = getWord(%myPos, 1);
            %z = getWord(%myPos, 2);
            %client.setPilotPitchRange(-0.025, 0.025, 0.025);//real smooth setting here
            %client.setPilotDestination(%x SPC %y SPC %z + 10, 1.0);// -- if we can't see %location move straight up ^
         }
         //else fly straight to wait spot
         else if (VectorDist(%myPos, %location) < 10)
         {
            if(%task.group.getObject(%task.locationIndex).waitTime $= "")
            {
               if ((%task.count - 1) > %task.locationIndex)//has to be -1 on the groups count
                  %task.locationIndex++;
               //we are at end of trail
	       else
               {
                  if (%client.vehicleMounted )
                  {
                     AIDisembarkVehicle(%client); //Hop off...
                     %client.stepMove(%location, 0.25);
                     return;
                  }
	       } 
            }
            else
            {
               if (%mySpd > 1.5)
               {
                  //error("AINeedARide::monitor - STOP - " @ getTaggedString(%client.name));
                  stop(%vehicle, %mySpd * 2, 5);
               }

               %client.setPilotDestination(%location, 0); //stop and wait
            }
         }
         //when pulling into wait spot go real smooth and slow
         else if (VectorDist(%myPos, %location) < 15)
            %client.setPilotDestination(%location, 0.07); //move to wait spot real,real slow
         else
	    %client.setPilotDestination(%location, %speed); //move to wait spot, slow
      }
      //wait time is up or we are fully loaded
      else 
      {
         //clear for take off, please fasten your seat belts :)
         //are we close to location index marker?
         %pos2D = getWord(%client.vehicleMounted.position, 0) SPC getWord(%client.vehicleMounted.position, 1) SPC "0";
         %dest2D = getWord(%location, 0) SPC getWord( %location, 1) SPC "0";
	 //if (VectorDist(%location, %client.vehicleMounted.position) < 50)//75 meters from marker
         if (VectorDist(%pos2D, %dest2D) < 50)//75 meters from marker
	 {
	    //if we have another location index
            if ((%task.count - 1) > %task.locationIndex)//has to be -1 on the groups count
               %task.locationIndex++;
            //we are at end of trail
	    else
            {
               if (%task == %client.objectiveTask)
	       {                  
                  if (%client.vehicleMounted )
                  {
                      AIDisembarkVehicle(%client); //Hop off...
                      %client.stepMove(%location, 0.25);
                      return;
                  }
	       }
            }
         }
         else
         {
            // - lets keep the pilot at about same height as location marker
            if ((%myZLev + 15) < getWord(%location, 2))
            {
               //applyKick(%vehicle, "up", %skill + 2.5);
               //Echo("HAPC TOO LOW - Apply ^ UP ^ KICK TO HAPC");

               %x = firstWord(%myPos);
               %y = getWord(%myPos, 1);
               %z = getWord(%location, 2);
               %client.setPilotPitchRange(-0.025, 0.025, 0.025);//real smooth setting here
               %client.setPilotDestination(%x SPC %y SPC %z, 1.0);
            }
            else
            {
               // - if can't see the next location marker gain height or turn around, decision making time - Lagg...
               //checkLOSPoint(%pt, %pt1) function is in LaggsDefault#_aiPilotShrike.cs, it returns a value of True or False.
               //It checks to see if terrain, interiors or trees are blocking our view.
               %hasLOS = checkLOSPoint(%myPos, %location);

               if (!%hasLOS)
               {
                  error("AINeedARide::monitor - We Cannot SEE THE NEXT MARKER - " @ getTaggedString(%client.name));


                  //if we don't have LOS from our current position, check if we would have LOS if we were even with the path marker's height
                  //if so then pilot set height, if not turn around and make another pass, we missed our turn - Lagg...

                  %x = firstWord(%myPos);
                  %y = getWord(%myPos, 1);
                  %z = getWord(%location, 2);
                  //%qLos = checkLOSPoint(%x SPC %y SPC (nameToId("MissionArea").flightCeiling - 50), %location);

                  %qLos = checkLOSPoint(%x SPC %y SPC %z, %location);
                  if (%qLos)
                  {
                     error("AINeedARide::monitor - set height is the solution - " @ getTaggedString(%client.name));

                     %client.setPilotPitchRange(-0.025, 0.025, 0.025);//real smooth setting here
                     %client.setPilotDestination(%x SPC %y SPC %z + 10, 1.0);// -- if we can't see %location move straight up ^
                  }
                  //we are in a spot that adjusting height will not help, go back to previous flight path market & try again
                  else
                  {
                     //error("AINeedARide::monitor - adjusting height is NOT the solution, go back to previous market- " @
                         //getTaggedString(%client.name));

                     %task.locationIndex--;//just set the path marker count back one and let the pilot monitor deal with it :)
                  }

               }
               // - else fly normally
               else
               {
                  %client.setPilotPitchRange(-0.2, 0.0425, 0.05);//rough setting here               
                  %client.setPilotDestination(%location, %speed);//speed
               }               
            }
         }
      }
   }
   //else task must be completed
   else if (%task == %client.objectiveTask)
   {
      AIUnassignClient(%client);
      Game.AIChooseGameObjective(%client);
      return;
   }
}
