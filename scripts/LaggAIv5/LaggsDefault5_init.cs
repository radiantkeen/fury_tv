exec("scripts/LaggAI/LaggsDefault5_vehicle.cs");//load the modified vehicle functions
exec("scripts/LaggAI/LaggsDefault5_ai.cs");
exec("scripts/LaggAI/LaggsDefault5_aiSiege.cs");//exec 2 modified functions from aiSiege.cs
exec("scripts/LaggAI/LaggsDefault5_aiCTF.cs");//exec 2 modified functions from aiCTF.cs
exec("scripts/LaggAI/LaggsDefault5_aiDnD.cs");
exec("scripts/LaggAI/LaggsDefault5_turret.cs");//allows turrets to fire at all enemy targets
exec("scripts/LaggAI/LaggsDefault5_aiSupport.cs");//2 added functions for mortar/plasma turret fire, 2 spam fixes

//lets load all the aiVehicle Files here - Lagg...
exec("scripts/LaggAI/LaggsDefault5_aiNeedRide.cs");       // ai - pilots HAPC
exec("scripts/LaggAI/LaggsDefault5_aiPilotShrike.cs");    // ai - pilots Shrike
exec("scripts/LaggAI/LaggsDefault5_aiDeployVehicle.cs");  // ai - pilots & deploys MPB
exec("scripts/LaggAI/LaggsDefault5_aiTank.cs");  // ai - tank crew
exec("scripts/LaggAI/LaggsDefault5_aiBombingRun.cs");     // ai - bomber crew
exec("scripts/LaggAI/LaggsDefault5_aiEasyRider.cs");      // ai - rides bike

LaggsGameStartUp();

//-------------------------------------------------------------------------------------------------------- GameStartUp ---

function LaggsGameStartUp()
{
   //$ObjectiveCounter[1] = 0;
   //$ObjectiveCounter[2] = 0;
 
   if ($missionRunning == false)
   {
      schedule(500, 0, LaggsGameStartUp);
      return;
   }
   SweepForPads(NameToID(MissionGroup)); 
}

//------------------------------------------------------------------------------------------------------- SweepForPads ---

function SweepForPads(%simGroup)
{
   if(%SimGroup.getClassName() $= "SimGroup")
   {
      for (%i = 0; %i < %SimGroup.getCount(); %i++)
      {
         %obj = %SimGroup.getObject(%i);
         if (%obj.getClassName() $= "SimGroup")
         {
            SweepForPads(%obj);
         }
 
         if (%obj.getClassName() $= "StaticShape")
         {
            %type = %obj.getDataBlock().getName();
            if (%type $= "StationVehicle" || %type $= "MPBTeleporter")
            {
               //error("SweepForPads - HERE we go --------------------------------------------------");

               if (%type $= "StationVehicle")
               {
                  //echo("VPad added to $AIVehiclePadSet for bots to buy vehicles");
                  $AIVehiclePadSet.add(%obj);
               }
               else
               {
                  //echo("MPBteleporter added to $AIMPBTeleportSet for bots");
                  $AIMPBTeleportSet.add(%obj);
               }

               if (Game.class !$= "SiegeGame")
               {
                  //error("Object team = " @ %obj.team);

                  if (%obj.team > 0)
                  {
                     %homeTeam = %obj.team;

                     if(%obj.team == 1)
                       %enemyTeam = 2;
                     else
                        %enemyTeam = 1;

                     addVPadRepairObjective(%obj, %homeTeam);
                     //echo("calling addVPadRepairObjective team = " @ %homeTeam);
                     addVPadDestroyObjective(%obj, %enemyTeam);
                     //echo("calling addVPadDestroyObjective team = " @ %enemyTeam);
                  }
                  else
                  {
                     addVPadRepairObjective(%obj, 1);
                     addVPadRepairObjective(%obj, 2);
                        //echo("calling addVPadRepairObjective for both teams 1 and 2");

                     //don't add destroy for team 0 vpads
                     //addVPadDestroyObjective(%obj, 1);
                     //addVPadDestroyObjective(%obj, 2);
                         //echo("calling addVPadDestroyObjective for both teams 1 and 2");
                  }
               }
               else
               {
                  //for seige game we may need a little more work here ...
                  Game.defenseTeam = Game.offenseTeam == 1 ? 2 : 1;

                  if (%obj.team == (Game.defenseTeam || 0))
                  {
                     addVPadRepairObjective(%obj, Game.defenseTeam);
                     addVPadDestroyObjective(%obj, Game.offenseTeam);
                  }
                  else if (%obj.team == (Game.offenseTeam || 0))
                     addVPadRepairObjective(%obj, Game.offenseTeam);
               }
            }
         }
      }
   }
}

//--------------------------------------------------------------------------------------------------- addVPadObjective ---
function addVPadRepairObjective(%object, %team)
{
   if (!isObject("T" @ %team @ "VPadRepairSet"))
   {
      %set = new SimGroup("T" @ %team @ "VPadRepairSet");
      MissionCleanup.add(%set);
   }
   else
      %set = nameToId("T" @ %team @ "VPadRepairSet");

   //create the repair objective
   %repairVPad = new AIObjective(AIORepairObject)
   {
           dataBlock = "AIObjectiveMarker";
           weightLevel1 = $AIWeightMortarTurret[1];
           weightLevel2 = $AIWeightRepairGenerator[1];
           description = "Repair the " @ %object.getDataBlock().getName();
           targetObject = %object.getDataBlock().getName();
           targetObjectId = %object;
           targetClientId = -1;
           offense = false;
           equipment = "RepairPack";
	        buyEquipmentSet = "MediumRepairSet";
           location             = %object.getWorldBoxCenter();
           position	     = %object.getWorldBoxCenter();
           group = %set;
   };

   $ObjectiveQ[%team].add(%repairVPad);
   %set.add(%repairVPad);
}

function addVPadDestroyObjective(%object, %team)
{
   if (!isObject("T" @ %team @ "VPadAttackSet"))
   {
      %set = new SimGroup("T" @ %team @ "VPadAttackSet");
      MissionCleanup.add(%set);
   }
   else
      %set = nameToId("T" @ %team @ "VPadAttackSet");


   //if (%object.pad.aiCanMortar || %object.vStation.aiCanMortar)
   if (%object.pad.aiCanMortar)
   {
      //create the mortar objective
      %destroyVPad = new AIObjective(AIOMortarObject)
      {
              dataBlock = "AIObjectiveMarker";
              weightLevel1 = $AIWeightMortarTurret[1];
              //weightLevel2 = 0;
              description = "Mortar the " @ %object.getDataBlock().getName();
              targetObject = %object.getDataBlock().getName();
              targetObjectId = %object;
              targetClientId = -1;
              offense = true;
              equipment = "Mortar MortarAmmo";
	      buyEquipmentSet = "HeavyAmmoSet";
              location             = %object.getWorldBoxCenter();
              position	     = %object.getWorldBoxCenter();
              group = %set;
      };
   }
   else
   {
      //create the destroy objective
      %destroyVPad = new AIObjective(AIODestroyObject)
      {
              dataBlock = "AIObjectiveMarker";
              weightLevel1 = $AIWeightAttackGenerator[1];
              //weightLevel2 = 0;
              description = "Destroy the " @ %object.getDataBlock().getName();
              targetObject = %object.getDataBlock().getName();
              targetObjectId = %object;
              targetClientId = -1;
              offense = true;
              desiredEquipment = "Plasma PlasmaAmmo";
	      buyEquipmentSet = "HeavyAmmoSet";
              location             = %object.getWorldBoxCenter();
              position	     = %object.getWorldBoxCenter();
              group = %set;
      };
   }

   $ObjectiveQ[%team].add(%destroyVPad);
   %set.add(%destroyVPad);   
}
