function CTFGame::onAIRespawn(%game, %client)
{
   //add the default tasks
   if (! %client.defaultTasksAdded)
   {
      %client.defaultTasksAdded = true;

      %client.addTask(AIEngageTask);
      //%client.addTask(AIPickupItemTask);

      %client.addTask(AITauntCorpseTask);

      %client.addTask(AIEngageTurretTask);

      %client.addtask(AIDetectMineTask);
      %client.addtask(AIDetectRemeqTask);//- Lagg... 3-20-2003
      %client.addtask(AIDetectVehicleTask);//- Lagg... 11-6-2003
      %client.addTask(AICouldUseInventoryTask);//- Lagg... 9-6-2003
      // %client.addTask(AISpamLocationTask);//- Lagg... 11-16-2003
   }
}

//fix for spam error messages when team damage is enabled - Lagg... 3-23-2004
function CTFGame::onAIDamaged(%game, %clVictim, %clAttacker, %damageType, %implement)
{
   if (%clAttacker && %clAttacker != %clVictim && %clAttacker.team == %clVictim.team)
   {
      //schedule(250, %clVictim, "AIPlayAnimSound", %clVictim, %clAttacker.player.getWorldBoxCenter(), "wrn.watchit", -1, -1, 0);
      AIMessageThread("ChatWarnShoot", %clVictim, %clAttacker);//Lagg...
      
      //clear the "lastDamageClient" tag so we don't turn on teammates...  unless it's uberbob!
      %clVictim.lastDamageClient = -1;
   }
}

function CTFGame::onAIFriendlyFire(%game, %clVictim, %clAttacker, %damageType, %implement)
{
   if (%clAttacker && %clAttacker.team == %clVictim.team && %clAttacker != %clVictim && !%clVictim.isAIControlled())
   {
      // The Bot is only a little sorry:
      if ( getRandom() > 0.4 )
         AIMessageThread("ChatSorry", %clAttacker, %clVictim);
   }
}

//-------------------------------------------------------------------------
//lets do a little something to help Ai Play CTF better - Lagg... 3-25-2005
function CTFGame::AIplayerDroppedFlag(%game, %player, %flag)
{
   //first lets get the client that dropped flag
   %client = %player.client;

   for ( %i = 0; %i < ClientGroup.getCount(); %i++ )
   {
      %cl = ClientGroup.getObject( %i );
      if (%cl.team == %client.team && %cl.isAIControlled() && %cl.player.getState !$= "Dead")
         Game.AIChooseGameObjective(%cl);
   }
}
