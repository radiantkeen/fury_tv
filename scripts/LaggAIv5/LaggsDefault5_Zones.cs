//----------------------------------------------- Custom ForceField Stuff ------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------ DumbBOT Trigger -----------------
//trigger must have dynamic field called setPosition, when Bot triggers it set bot's transform numbers that you entered - Lagg...

datablock TriggerData(DumbBOTTrigger)
{
   tickPeriodMS = 50;
};

function DumbBOTTrigger::onEnterTrigger(%data, %obj, %colObj)
{
   if (%colObj.client.isAIControlled())
   {
      error("Dumb BOT! Dumb BOT! Dumb BOT! Dumb BOT! Dumb BOT!");

      if (AIClientIsAlive(%colObj.client))
      {
         %colObj.client.player.setTransForm(%obj.setPosition);
      }
   }   
}

function DumbBOTTrigger::onLeaveTrigger(%data, %obj, %colObj)
{
   return;
}

function DumbBOTTrigger::onTickTrigger(%data, %obj)
{
  // do nothing
}

//------------------------------------------------------------------------------------------ MPB Trigger -----------------

datablock TriggerData(mpbTrigger)
{
   tickPeriodMS = 50;
};

function mpbTrigger::onEnterTrigger(%data, %obj, %colObj)
{
   messageClient(%colObj.client, 'MsgClient', '\c5Vehicle Is Not Mountable.~wfx/powered/station_denied.wav');
}

function mpbTrigger::onLeaveTrigger(%data, %obj, %colObj)
{
   return;
}

function mpbTrigger::onTickTrigger(%data, %obj)
{
  // do nothing
}

//------------------------------------------------------------------------------------------ Message Trigger -----------------

datablock TriggerData(messageTrigger)
{
   tickPeriodMS = 50;
};

function messageTrigger::onEnterTrigger(%data, %obj, %colObj)
{
   %message = %obj.message;
   if (%obj.message $= "")
   {
      error("messageTrigger::onEnterTrigger - sorry no message !");
      return;
   }
   messageClient(%colObj.client, 'MsgClient', %message);
}

function messageTrigger::onLeaveTrigger(%data, %obj, %colObj)
{
   return;
}

function messageTrigger::onTickTrigger(%data, %obj)
{
  // do nothing
}

//-------------------------------------------------- Death Trigger ----------------------------------------------
//used only in siege game type in offensive base entrances
datablock TriggerData(deathTrigger)
{
   tickPeriodMS = 50;
};

function deathTrigger::onEnterTrigger(%data, %obj, %colObj)
{
   if (%colObj.client.team != game.offenseTeam)
   {
      %pl = %colObj.client.player;
      if(isObject(%pl))
      {
         if(%pl.isMounted())
            %pl.getDataBlock().doDismount(%pl);
         %pl.scriptKill(0);
         messageClient(%colObj.client, 'MsgClient', 'You were killed for entering the offensive base');
      }
   }
}

function deathTrigger::onLeaveTrigger(%data, %obj, %colObj)
{
   // do nothing
}

function deathTrigger::onTickTrigger(%data, %obj)
{
  // do nothing
}

//------------------------------------------------------------------------------------------ ForceFieldBareData::onAdd ---



function ForceFieldBareData::onAdd(%data, %obj)
{
   Parent::onAdd(%data, %obj);

   %velo = 0.1;
   %grav = 1.0;
   %appl = "0 0 0";

   if(%obj.custom $= "1")
   {
      %velo = %obj.velocityMod;
      %grav = %obj.gravityMod;
      %appl = %obj.appliedForce;
   }   

   %pz = new PhysicalZone() {
      position = %obj.position;
      rotation = %obj.rotation;
      scale    = %obj.scale;
      polyhedron = "0.000000 1.0000000 0.0000000 1.0000000 0.0000000 0.0000000 0.0000000 -1.0000000 0.0000000 0.0000000 0.0000000 1.0000000";
      velocityMod  = %velo;
      gravityMod   = %grav;
      appliedForce = %appl;
		ffield = %obj;
   };

   %pzGroup = nameToID("MissionCleanup/PZones");
   if(%pzGroup <= 0)
   {
      %pzGroup = new SimGroup("PZones");
      MissionCleanup.add(%pzGroup);
   }
   %pzGroup.add(%pz);
}

//------------------------------------------------------ AI ZONE STUFF ---------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

//Inside and Outside Triggers must be placed at the entrances of the defensive base (SeigeGame). When AI touches trigger tag
//is set on ai clients. They should be placed in the same simgroup as the defensive base. So the defsive team does not get tagged.
//Other scripts can then make use of the tag ".inBase" or "inRoom" for some seige setups. - Lagg...

//--------------------------------------------------- Outside Zone ---------------------------------------------

datablock TriggerData(outsideTrigger)
{
   tickPeriodMS = 50;
};

function outsideTrigger::onEnterTrigger(%data, %obj, %colObj)
{
   //set a tag on the client when he touches this trigger
   if (%colObj.client.isAIControlled())
   {
      %colObj.client.inBase = false;
      %colObj.client.inRoom = false;
   }
}

function outsideTrigger::onLeaveTrigger(%data, %obj, %colObj)
{
   // do nothing
}

function outsideTrigger::onTickTrigger(%data, %obj)
{
  // do nothing
}

//--------------------------------------------------- Inside Zone ----------------------------------------------


datablock TriggerData(insideTrigger)
{
   tickPeriodMS = 50;
};

function insideTrigger::onEnterTrigger(%data, %obj, %colObj)
{
   //set a tag on the client when he touches this trigger
   if (%colObj.client.isAIControlled())
      %colObj.client.inBase = true;
}

function insideTrigger::onLeaveTrigger(%data, %obj, %colObj)
{
   // do nothing
}

function insideTrigger::onTickTrigger(%data, %obj)
{
  // do nothing
}


datablock TriggerData(insideTower)
{
   tickPeriodMS = 50;
};

function insideTower::onEnterTrigger(%data, %obj, %colObj)
{
   //set a tag on the client when he touches this trigger
   if (%colObj.client.isAIControlled())
      %colObj.client.inTower = true;
}

function insideTower::onLeaveTrigger(%data, %obj, %colObj)
{
   // do nothing
}

function insideTower::onTickTrigger(%data, %obj)
{
  // do nothing
}

//--------------------------------------------------- InRoom Zone ----------------------------------------------

datablock TriggerData(inRoomTrigger)
{
   tickPeriodMS = 50;
};

function inRoomTrigger::onEnterTrigger(%data, %obj, %colObj)
{
   //set a tag on the client when he touches this trigger
   if (%colObj.client.isAIControlled())
      %colObj.client.inRoom = true;
}

function inRoomTrigger::onLeaveTrigger(%data, %obj, %colObj)
{
   // do nothing
}

function inRoomTrigger::onTickTrigger(%data, %obj)
{
  // do nothing
}

//----------------------------------------------- Danger over Lava Triger ------------------------------------------------------------

datablock TriggerData(dangerTrigger)
{
   tickPeriodMS = 50;
};

function dangerTrigger::onEnterTrigger(%data, %obj, %colObj)
{
   //set a tag on the client when he Enters this trigger zone
   if (%colObj.client.isAIControlled() && %colObj.client.team == Game.offenseTeam)
      %colObj.client.inDanger = true;
}

function dangerTrigger::onLeaveTrigger(%data, %obj, %colObj)
{
   // do nothing
}

function dangerTrigger::onTickTrigger(%data, %obj)
{
  // do nothing
}
//--------------------------------------------------- Misc Triggers ---------------------------------------------

//TR2 Trigger modified for practicle game use - Lagg... 4-6-2005

function cannonTrigger::onEnterTrigger(%this, %trigger, %obj)
{
   //if (nameToId("Cannon").getGroup().powerCount <= 0)
   if (%trigger.getGroup().powerCount <= 0)
   {
      error("cannonTrigger::onEnterTrigger - No Power");
      return;
   }   
   if (%obj.getState $= "Dead")
      return;
      
   %client = %obj.client;
   %obj.playAudio(0, CannonStartSound);
   %obj.inCannon = true;
   %obj.setInvincible(true);
   %client.cannonThread = %this.schedule(500, "ShootCannon", %trigger, %obj);
}

datablock TriggerData(gumbyTrigger)
{
   tickPeriodMS = 50;
};

function gumbyTrigger::onEnterTrigger(%data, %trigger, %player)
{
   //if (nameToId("Cannon").getGroup().powerCount <= 0)
   if (%trigger.getGroup().powerCount <= 0)
   {
      error("gumbyTrigger::onEnterTrigger - No Power");
      return;
   }
   
   if (getRandom() > 0.5)
   {
      %player.setVelocity("0 5 15");
   }	
   else
   {
      %player.setVelocity("0 -5 15");
   }	

   %player.playAudio($PlaySound, GridjumpSound);
}
 
function gumbyTrigger::onLeaveTrigger(%data, %obj, %colObj)
{
   return;
}

function gumbyTrigger::onTickTrigger(%data, %obj)
{
   // Stops Console Spamming
}

//--------------------------------------------------- EEPROM - Huge Fire --------------------------------------------
//EEPROM - Huge Fire
datablock ParticleData(EEPFire3Particles)
{
   dragCoefficient      = 0.0;   
   gravityCoefficient   = 0.0;   
   windCoefficient      = 1.0;

   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;

   lifetimeMS           = 850;
   lifetimeVarianceMS   = 550;  

   textureName          = "particleTest";

   colors[0]     = "1 0 0 1";
   colors[1]     = "1 0.694118 0.392157 1";
   colors[2]     = "1 0.6 0.6 1";
   
   sizes[0]      = 8;          
   sizes[1]      = 1;
   
   times[0] = 0.0;             
   times[1] = 1.0;             
   
};

datablock ParticleEmitterData(EEPFire3Emitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 9;
   ejectionVelocity = 5;    
   velocityVariance = 2;
   ejectionOffset   = 0.0;
   thetaMin         = 0.0;
   thetaMax         = 25.0;  
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "EEPFire3Particles";
};


