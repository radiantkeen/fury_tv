//In this file you will find many but not all of the ai vehicle functions.
//The default vehicle functions are in ai.cs.

//------------------------------------------------------------------------------
//ai find closest enemy vehicle - Lagg... 10-8-2003
//------------------------------------------------------------------------------
function aiFindClosestEnemyVehicle(%client)
{
   //get some vars
   %player = %client.player;
   if (!isObject(%player))
      return -1 @ " " @ 32767;

   if (! AIClientIsAlive(%client))
      return -1 @ " " @ 32767;

   %closestVehicle = -1;
   %closestDist = 32767;
   %vehicleCount = $AIVehicleSet.getCount();
   for (%i = 0; %i < %vehicleCount; %i++)
   {
      if (%vehicleCount > 0)
      {
         %vehicle = $AIVehicleSet.getObject(%i);
         if(%vehicle.getDamageState() !$= "Destroyed")
         {
            //this should be funny, bot attacks team vehicles if enemy onboard :)
            %enOnBoard = false;
            for (%m = 0; %m < %vehicle.getDataBlock().numMountPoints; %m++)
            {
               %mount = %vehicle.getMountNodeObject(%m);
               if (isObject(%mount) && %mount.team != %client.team)
                  %enOnBoard = true;
            }
            
            if ((%vehicle.team != %client.team && %vehicle.team != 0) || %enOnBoard)
            {
               %vehiclePos = %vehicle.getWorldBoxCenter();
               %clPos = %player.getWorldBoxCenter();

               //check for LOS (kinda)
               %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType |
                 $TypeMasks::TSStaticShapeObjectType;
               %vehicleLOS = !containerRayCast(%clPos, %vehiclePos, %mask, 0);

               //see if the vehicle is the closest...
               %vehicleDist = VectorDist(%vehiclePos, %clPos);
               if (%vehicleDist < %closestDist && %vehicleLOS)
               {
                  %closestVehicle = %vehicle;
                  %closestDist = %vehicleDist;
               }
            }
         }
      }
   }

   //give em what you got
   return %closestVehicle @ " " @ %closestDist;
}

//------------------------------------------------------------------------------
// ai Buy Vehicle - Lagg... 8-19-2003
//------------------------------------------------------------------------------

function aiBuyVehicle(%vehicleType, %client, %closestVs)
{
   //get some vars
   %player = %client.player;
   if (!isObject(%player))
   {
      echo("aiBuyVehicle - Client !object RETURN FAILED");
      return "Failed";
   }

   if (! AIClientIsAlive(%client))
   {
      echo("aiBuyVehicle - Client Dead RETURN FAILED");
      return "Failed";
   }

   if (%player.getArmorSize() $= "Heavy" || (%vehicleType $= "ScoutVehicle" && %player.getArmorSize() !$= "Light"))
   {
      echo("aiBuyVehicle - Wrong Armor Size RETURN FAILED");
      return "Failed";
   }

   //if no vpad then get out
   if (%closestVs <= 0)
   {
      echo("aiBuyVehicle - %closestVs <= 0 RETURN FAILED");
      return "Failed";
   }

   //leave this commented out so capturable vpads work - Lagg... 5-25-2005
   //if (%client.justBought)
   //{
      ////error("aiBuyVehicle - just bought so return inProgress - " @ getTaggedString(%client.name) @ ", " @ %vehicleType);
      //return "InProgress";
   //}

   //check how many vehicles of this type are in the field if we didn't buy already
   if (!vehicleCheck(%vehicleType, %client.team) && !%client.player.lastVehicle)
   {
      error("aiBuyVehicle - vehicleCheck - RETURN FAILED" @ %vehicleType @ ", " @ getTaggedString(%client.name));
      return "Failed";
   }

   //at this point we have located the vehicle station
   if (%closestVs.isPowered())//is powered - thanks ZOD
   {
      if (!%closestVs.isDisabled())//not blown up - thanks ZOD
      {           
         //make sure the vehicle station is not blocked
         InitContainerRadiusSearch(%closestVs.getWorldBoxCenter(), 1.5, $TypeMasks::PlayerObjectType);
         %objSrch = containerSearchNext();
         if (%objSrch == %client.player)
            %objSrch = containerSearchNext();

         //is the pad blocked? don't look for players here. just blow them up :)
         %position = %closestVs.pad.position;
         InitContainerRadiusSearch(%position, 20, $TypeMasks::VehicleObjectType);
         %posSrch = containerSearchNext();

         //did we find some something?
         if (%posSrch > 0)
         {
            //did someone forget their vehicle?
            %vehicle = %posSrch.getDataBlock();
            %pilot = %posSrch.getMountNodeObject(0);
            if (%posSrch.lastPilot != %client.player && !%pilot && !%posSrch.inStation) 
               %posSrch = -1;//just continue and buy a vehicle
            
            //we must be mounted so go
            else if (%player.isMounted())
            {
               %client.needVehicle = false;

               error("aiBuyVehicle - i don't think we see this at all ???");

               return "Finished";
            }
         }
            
         //if the closest vehicle station is busy...
         //if ((%objSrch > 0) || (%posSrch > 0))
         if (%objSrch > 0)
         {
            //have the AI range the vehicle station
            if (vectorDist(%client.player.getWorldBoxCenter(), %closestVs.getWorldBoxCenter()) > 8)
               %client.stepRangeObject(%closestVs, "DefaultRepairBeam", 3, 6);

	    //vehicle station is still busy - see if we're within range
	    else if (vectorDist(%client.player.getWorldBoxCenter(), %closestVs.getWorldBoxCenter()) < 12)
	    {
            
	       //initialize the wait time
	       if (%client.vsWaitTime $= "")
                  %client.vsWaitTime = getSimTime() + 6000 + (getRandom() * 1000);

	       //else see if we've waited long enough and set an itchy reaction
               else if (getSimTime() > %client.vsWaitTime && %objSrch > 0)
	       {
                  schedule(250, %client, "AIPlayAnimSound", %client, %objSrch.getWorldBoxCenter(), "vqk.move", -1, -1, 0);
	          %client.vsWaitTime = getSimTime() + 6000 + (getRandom() * 1000);
                  %client.setDangerLocation(%client.player.getWorldBoxCenter(), 10);
	       }
	    }
         }

         //else if we've triggered the vs then buy vehicle, and wait till we are mounted
         else if (%client.needVehicle && isObject(%closestVs.trigger) &&
           VectorDist(%closestVs.trigger.getWorldBoxCenter(), %player.getWorldBoxCenter()) < 1.5)
         {
            //first stop...
	    %client.stop();

            //look in awe
            %client.aimAt(%closestVs.pad.getWorldBoxCenter(), 2500);

            //initialize the wait time
	    if (%client.vsWaitTime $= "")
               %client.vsWaitTime = getSimTime() + 5000;

	    //wait a few sec before trying to buy
            else if (getSimTime() > %client.vsWaitTime)
	    {
               //buy vehicle
               %client.pilotVehicle = true;//needed to allow ai to get in pilot seat
               schedule(1500, 0, "serverCmdBuyVehicle", %client, %vehicleType);//buy the vehicle
               %client.justBought = true;//set the vehicle station tag, reset in function Armor::AIonMount - Lagg...

               //jump on VPad every fifteen sec to reset the vehicle hud
               %client.vsWaitTime = getSimTime() + 15000;
               %client.pressJump();
	    }

            //throw away any packs that won't fit
            if (hasLargePack(%player))
               %player.throwPack();
         }

         //else, keep moving towards the vehicle station
         else if (isObject(%closestVs) && isObject(%closestVs.trigger))
         {
            //if in water (trick em here, seeems to work)
            if (%client.inWater)               
               %client.stepRangeObject(%closestVs, "DefaultRepairBeam", 3, 6);

            //or just step move to the station 
            else
            {
               //use the Y-axis of the rotation as the desired direction of approach,
               //and calculate a walk to point 1m in front of the trigger point - Lagg...
               %aprchDirection = MatrixMulVector("0 0 0 " @ getWords(%closestVs.getTransform(), 3, 6), "0 1 0");
               %aprchDirection = VectorNormalize(%aprchDirection);
               //%factor = 1; //1m in front of station vehicle is the right spot to stand
               %aprchFromLocation = VectorAdd(%closestVs.getWorldBoxCenter(),VectorScale(%aprchDirection, 1));
               %client.stepMove(%aprchFromLocation);
            }
         }

         //just return message working on it         
         return "InProgress";
      }
   }

   error("aiBuyVehicle - vpad no power or disabled - RETURN FAILED");

   return "Failed";     
}
   
//------------------------------------------------------------------------------
// Find closest Vehicle station - Lagg... 8-19-2003
//------------------------------------------------------------------------------

//this function will return the closest vehicle station to location and distance - Lagg... 8-25-2003

function AIFindClosestVStation(%client, %loc, %closestDist)
{
   %closestVStation = -1;
   if (%closestDist $= "")
      %closestDist = 32767;
     
   // lets find the closest Vpad that is powered and enabled and not enemy owned
   %vsCount = $AIVehiclePadSet.getCount();
   for (%i = 0; %i < %vsCount; %i++)
   {
      if (%vsCount > 0)
      {
         %VStation = $AIVehiclePadSet.getObject(%i);
         if (%VStation.team == 0 || %VStation.team == %client.team)
         {
            //make sure the station is not destroyed - ZOD
            if (!%VStation.isDisabled())
            {
               //make sure the station is getting power - ZOD
               if (%VStation.isPowered())
               {
                  %dist = VectorDist(%VStation.getWorldBoxCenter(), %loc);
                  if (%dist < %closestDist)
                  {
                     %closestVStation = %VStation;
                     %closestDist = %dist;
                  }
               }
            }
         }
      }
   }
   
   //give em what you got
   return %closestVStation @ " " @ %closestDist;
}

//------------------------------------------------------------------------------
// AI Find deployed MPB - Lagg... 9-30-2003
//------------------------------------------------------------------------------

//this function will return the closest team deployed MPB and the distance - Lagg... 8-30-2003

function AIFindDeployedMPB(%client)
{

   //first look for vehicles
   %closestVeh = -1;
   %closestDist = 32767;
   %vehCount = $AIVehicleSet.getCount();
   for (%i = 0; %i < %vehCount; %i++)
   {
      if (%vehCount > 0)
      {
         %Vehicle = $AIVehicleSet.getObject(%i);
         if (!%vehicle.respawn)
         {
            if (%Vehicle.team == %client.team)
            {
               if (%vehicle.getDataBlock().getName() $= "mobileBaseVehicle")
               {
                  if (%vehicle.fullyDeployed)
                  {
                     if (%vehicle.getDamageState() !$= "Destroyed")
                     {
                        %dist = %client.getPathDistance(%vehicle.getTransform());
                        if (%dist > 0 && %dist < %closestDist)
	                {
                           %closestVeh = %vehicle;
		           %closestDist = %dist;
                        }
                     }
                  }
               }
            }
         }
      }
   }

   //give em what you got
   return %closestVeh @ " " @ %closestDist;
}

//------------------------------------------------------------------------------
// Sweep For Teleporters - Lagg... 9-30-2003
//------------------------------------------------------------------------------

//this function will find the closest team enabled mpb teleporter - Lagg... 9-30-2003
//note this will only work for mpb teleporter, a more advanced technique will
//be needed for bots to use local teleporters.

function SweepForTeleporters(%client)
{
   %closestTel = -1;
   %closestDist = 32767;
   %telCount = $AIMPBTeleportSet.getCount();
   for (%i = 0; %i < %telCount; %i++)
   {
      if (%telCount > 0)
      {
         %teleport = $AIMPBTeleportSet.getObject(%i);
         if (%teleport.team == %client.team)
         {          
            //make sure the teleporter is not destroyed - ZOD
            if (!%teleport.isDisabled())
	    {
               //make sure the teleporter is getting power - ZOD
               if (%teleport.isPowered())
               {
                  %dist = %client.getPathDistance(%teleport.getTransform());
                  if (%dist > 0 && %dist < %closestDist)
	          {
                     %closestTeh = %teleport;
		     %closestDist = %dist;
                  }
               }
            }
         }
      }
   }

   //give em what you got
   return %closestTeh @ " " @ %closestDist;
}

//--------------------------------------------------- assume telporter - start -
//This function checks if the client could and should use MPB Teleporter - Lagg...

function MPBTeleporterCheck(%client, %tarLoc)
{
   //if the MPB Teleporter is online - Lagg... 9-30-2003

   //first check for deployed MPB
   %result = AIFindDeployedMPB(%client);
   %closestMPB = getWord(%result, 0);
   %closestMPBDist = getWord(%result, 1);
      
   //next find the teleporter
   %result = SweepForTeleporters(%client);
   %closestTel = getWord(%result, 0);
   %closestTelDist = getWord(%result, 1);
      
   //now check for what would be closer - Lagg... 10-1-2003
   if (%closestMPB > 0 && %closestTel > 0)
   {
      //lets make sure we have the correct teleporter
      if (%closestTel == %closestMPB.teleporter)
      {
         %disToTarg = %client.getPathDistance(%tarLoc);
         %mpbDisToTarg = vectorDist(%closestMPB.getTransform(), %tarLoc);
         
         if (%closestTelDist * 2 < %closestMPBDist && %closestMPBDist < %disToTarg && %mpbDisToTarg < %disToTarg)
            return true;//%useTeleport = true;//new - Lagg... 10-1-2003
      }
   }
   //--------------------------------------------------- assume telporter - end -
   return false;//%useTeleport;
}

//-----------------------------------------------------------------------------------------
//This function searches for unpiloted vehicles within a certain radius - Lagg... 5-19-2005

function aiSearchForRide(%client, %blockName, %pos, %maxDis, %mode)
{
   %abV = -1;
   for (%i = 0; %i < $aiVehicleSet.getCount(); %i ++)
   {
      %vehicle = $aiVehicleSet.getObject(%i);
      if (isObject(%vehicle))
      {
         if (%vehicle.getDamageState() !$= "Destroyed")
         {
            if (%vehicle.getMountNodeObject(0) <= 0)
            {
               //sorry don't mount enemy vehicles here
               if (%vehicle.team == %client.team || %vehicle.team == 0)
               {
                  %type = %vehicle.getDataBlock().getName();
                  if (%type $= %blockName || (%blockName $= "" && %type !$= "MobileBaseVehicle"))
                  {
                     %dis = VectorDist(%client.player.position, %vehicle.position);
                     %dis1 = VectorDist(%vehicle.position, %pos);
                     if (%dis < %maxDis && %dis1 < %maxDis)
                     {
                        if (%mode $= "PassengerOnly")
                        {
                           for(%j = 1; %j < %vehicle.getDataBlock().numMountPoints; %j++)
                           {
                              if (%vehicle.getMountNodeObject(%j) > 0 &&
                                %vehicle.getMountNodeObject(%j).client.team == %client.team)
                              {
                                 %abV = %vehicle;
                                 break;
                              }
                           }
                           if (%abV> 0)
                              break;
                        }
                        else
                        {
                           %abV = %vehicle;
                           break;
                        }
                     }
                  }
               }
            }
         }
      }
   }
      
   return %abV;
}

// ---------------------------------------------------------------------------------- new function by ZOD to help me out :)
function applyKick(%obj, %dir, %amt, %useVelocity)
{
   if (%useVelocity $= "")
      %useVelocity = false;

   %data = %obj.getDataBlock();
   //%position = posFromTransform(%obj.getTransform());
   %position = %obj.getWorldBoxCenter();

   %velocity = %obj.getVelocity();
   %normal = vectorDot(%velocity, vectorNormalize(%velocity));
   if(%normal > 100) // Whatever cap we wish..
   {
      //echo("applyKick - WHAT IS THIS ?");
      %kick = %amt / %normal; // Reduce impulse.
      //return; // Or apply no impulse at all and exit out.
   }
   else
      %kick = %amt;

   if (%UseVelocity)
      %forwardVector = VectorNormalize(%velocity);
   else
      %forwardVector = %obj.getEyeVector();

   switch$(%dir)
   {
      case "foward":
         %impulse = VectorScale(%ForwardVector, %Kick * %Data.mass);

      case "reverse":
         %impulse = VectorScale(%ForwardVector, (%Kick * %Data.mass) * -1);

      case "up":
         %impulse = VectorScale("0 0 1", %Kick * %Data.mass);

      case "down":
         %impulse = VectorScale("0 0 -1", %Kick * %Data.mass);

      case "left":
         %impulse = VectorScale(VectorCross(%ForwardVector, "0 0 -1"), %Kick * %Data.mass);

      case "right":
         %impulse = VectorScale(VectorCross(%ForwardVector, "0 0 1"), %Kick * %Data.mass);

      default :
         error("Unknown direction passed to function applyKick: " @ %dir);
   }

   %obj.applyImpulse(%position, %impulse);
}

//-------------------------------------------------------------------------------------------------------------------------
