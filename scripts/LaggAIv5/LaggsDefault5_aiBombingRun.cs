//-------------------------------------------------------------------------------------------------- AIO Bombing Run ---

//Place AIO Objective Marker in the spot you want pilot to park and wait for passengers
//Set up simgroup called "T" @ %team @ "BomberPath" @ %num
//(example T2BomberPath1 or T2BomberPath8 / T1BomberPath1 or T1BomberPath5)
//Inside these simgroups place markers in the order to follow for each
//path. Thats 1 simgroup per path.
//
//In the AIOMarker for this you must add a Dynamic field called "paths" and give as value
//the number of the paths simgroups to choose seperated by spaces (example: paths = "1 3"
//for "T#BomberPath1 and T#BomberPath3"). They do not have to be in any order and
//you can have as many as you like. One path we be chosen at random - Lagg... 3-30-2005

function AIOBombingRun::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   // For CnH games, we set a target object and set def or offense (can be used for any gameType)
   if (%this.targetObjectId > 0 && %this.clientLevel1 != %client)
   {
      if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden())
         return 0;

      //if offense and we own target object quit
      if (%this.offense > 0)
      {
         if (%this.targetObjectId.team == %client.team || %this.targetObjectId.getDamageState() $= "Destroyed")
            return 0;
      }
      //if defense and we own it must be powered and not destroyed, or if enemy it must be destroyed or not powered
      else if (%this.defense > 0)
      {
         if (%this.targetObjectId.team == %client.team)
         {
            if (!%this.targetObjectId.isPowered() || %this.targetObjectId.getDamageState() $= "Destroyed")
               return 0;
         }
         else
         { 
            if (%this.targetObjectId.isPowered() && %this.targetObjectId.getDamageState() !$= "Destroyed")
              return 0;
         }
      }        
   }
      
   //check if flight path exists
   if (Game.class $= "SiegeGame")
      %team = Game.offenseTeam == %client.team ? 1 : 2;
   else
      %team = %client.team;

   if(! isObject(nameToId("T" @ %team @ "BomberPath" @ firstWord(%this.paths))))
   {
      error("AIOBombingRun Error! - No Paths field specified in AIOBomberPath objective Marker ie... paths = 1 3 5");
      return 0;
   }

   //do a quick check to disqualify this objective if it can't meet the minimum weight
   if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
   {
      if (%this.targetObjectId > 0 && %this.issuedByClientId == %client.controlByHuman)
      {
         if ($AIWeightHumanIssuedCommand < %minWeight)
	    return 0;
      }
      else
	 return 0;
   }

   //check for Vehicle station near buy of forget it if we are noy mounted yet - Lagg...
   //would like to add a check for unpiloted vehicles with passengers that need a ride (someday - DONE) - Lagg... 5-4-2005
   if (!%client.player.isMounted() && !%client.justBought)
   {
      %blockName = BomberFlyer;

      //check to see if we can steal a ride
      %abV = aiSearchForRide(%client, %blockName, %this.position, 250, %this.mode);

      //modes = "NoBuy" and "PassengerOnly" (self explanatory)
      if (%abv <= 0 && (%this.mode $= "NoBuy" || %this.mode $= "PassengerOnly"))
         return 0;

      if (%abV <= 0)
      {
         %clVs = AIFindClosestVStation(%client, %this.position);
         if (%clVs > 0)
         {
            %closestVs = getWord(%clVs, 0);
            %closestVsDist = getWord(%clVs, 1);
            if (%closestVsDist > 15)
               return 0;
            if (VectorDist (%client.player.position, %this.position) > 300)
               return 0;
         }
         else
            return 0;

         //see if this vehicle is allowed at the VPad in question
         if (%closestVs.BomberFlyer $= "Removed")
         {
            error("AIOBombingRun Error! - Bombers Not Allowed At This Vehicle Pad");
            return 0;
         }

         //check if any of vehicle type are availible
         if (!vehicleCheck(%blockName, %client.team))
            return 0;
      }
   }

   %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);

   //never bump a pilot off his ride
   if (%this.clientLevel1 > 0  && %level == 1 && %this.clientLevel1 != %client)
      return 0;
   else if (%this.clientLevel1 == %client)
      %weight = 10000;

   return %weight;
}

function AIOBombingRun::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIBombingRun);
   %client.objectiveTask.initFromObjective(%this, %client);

   //create the escort (passenger) objectives
   //make sure everybody has flares :)

   //pick a random equipment set...
   %randNum = getRandom();
   if (%randNum < 0.3)
   {
      %desEquip = "FlareGrenade";
      %set = "MediumRepairSet";
   }
   else if (%randNum < 0.6)
   {
      %desEquip = "CloakingPack";
      %set = "LightCloakSet";
   }
   else
   {
      %desEquip = "FlareGrenade";
      %set = "MediumShieldSet";
   }

   %client.escort = new AIObjective(AIOEscortPlayer)
                     {
						      dataBlock = "AIObjectiveMarker";
                        weightLevel1 = $AIWeightVehicleMountedEscort;//was 6000 Lagg...
                        weightLevel2 = 0;
                        description = "Bomber Crew 1 for" @ getTaggedString(%client.name);
                        targetClientId = %client;
                        offense = true;
                        chat = "ChatNeedHold";
                        desiredEquipment = %desEquip;
								buyEquipmentSet = %set;
                     };

   //pick a random equipment set...
   %randNum = getRandom();
   if (%randNum < 0.3)
   {
      %desEquip = "FlareGrenade";
      %set = "HeavyRepairSet";
   }
   else if (%randNum < 0.6)
   {
      %desEquip = "FlareGrenade";
      %set = "HeavyEnergySet";
   }
   else
   {
      %desEquip = "FlareGrenade";
      %set = "HeavyTailgunner";
   }

   %client.escort1 = new AIObjective(AIOEscortPlayer)
                     {
						      dataBlock = "AIObjectiveMarker";
                        weightLevel1 = $AIWeightVehicleMountedEscort;//was 6000 - Lagg...
                        weightLevel2 = 0;
                        description = "Bomber Crew 2 for" @ getTaggedString(%client.name);
                        targetClientId = %client;
                        mode = "Tailgunner";
                        offense = true;
                        chat = "ChatNeedRide";
                        desiredEquipment = "FlareGrenade";
								buyEquipmentSet = "HeavyRepairSet";
                     };

   MissionCleanup.add(%client.escort);
   MissionCleanup.add(%client.escort1);

   //if is siege game we have to do this right - Lagg... 11-3-2003
   if (Game.class $= "SiegeGame")
   {
      //the objectives in $ObjectiveQ[1] are all offense objectives, $ObjectiveQ[2] defense
      if (%client.team == game.offenseTeam)
      {
         $ObjectiveQ[1].add(%client.escort);
         $ObjectiveQ[1].add(%client.escort1);
      }
      else
      {
         $ObjectiveQ[2].add(%client.escort);
         $ObjectiveQ[2].add(%client.escort1);
      }
   }
   //else if not siege game use the default team's Q
   else
   {
         $ObjectiveQ[%client.team].add(%client.escort);
         $ObjectiveQ[%client.team].add(%client.escort1);
   }
}

function AIOBombingRun::unassignClient(%this, %client)
{
   //kill the escort (passenger) objectives
   if (%client.escort)
   {
      clearObjectiveFromTable(%client.escort);//new function below to clear objective from $objTable[] - Lagg... 1-27-2004
      AIClearObjective(%client.escort);
      %client.escort.delete();
      %client.escort = "";
   }
   if (%client.escort1)
   {
      clearObjectiveFromTable(%client.escort1);//new function below to clear objective from $objTable[] - Lagg... 1-27-2004
      AIClearObjective(%client.escort1);
      %client.escort1.delete();
      %client.escort1 = "";
   }            
   if(%client.pilotVehicle)
      AIDisembarkVehicle(%client);
  
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//----------------------------------------------------------------------------------------------- AI Need A Ride ---

function AIBombingRun::initFromObjective(%task, %objective, %client)
{
   //initialize the task vars from the objective
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;

   //task location is the position of aiomarker
   %task.location = %objective.position;
   %task.mode = %objective.mode;

   %task.equipment = %objective.equipment;
   %task.buyEquipmentSet = %objective.buyEquipmentSet;
   %task.desiredEquipment = %objective.desiredEquipment;
   %task.issuedByClient = %objective.issuedByClientId;
   %task.chat = %objective.chat;

   //initialize other task vars
   %task.sendMsg = true;
   %task.sendMsgTime = 0;
   %task.paths = %objective.paths;

   %client.player.mVehicle = "";// needed if bot just hopped out a vehicle and it was not destroyed/removed yet
}

function AIBombingRun::assume(%task, %client)
{
   //set the frequencys
   %task.setWeightFreq(20);
   %task.setMonitorFreq(20);
   
   %client.needEquipment = AINeedEquipment(%task.equipment, %client);

   if (%task.mode $= "PassengerOnly")
   {
      if (%client.player.getArmorSize() !$= "Heavy")
         %client.needEquipment = false;
   }
   //even if we don't *need* equipemnt, see if we should buy some... 
   else if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
   {
      //see if we could benefit from inventory
      %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      %closestDist = getWord(%result, 1);
      if (AINeedEquipment(%task.desiredEquipment, %client) && %closestInv > 0)
      {
         if (%closestDist < 100)
	    %client.needEquipment = true;
      }
   }

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();

   //reset the vehicle station wait time
   %client.vsWaitTime = "";

   //set the destination paths for each team and game type
   
   //first see how many paths we have
   //check if flight path exists
   if (Game.class $= "SiegeGame")
      %team = Game.offenseTeam == %client.team ? 1 : 2;
   else
      %team = %client.team;

   //first see how many paths we have
   %mx = getWordCount(%task.paths);
   echo("AIBombingRun::assume - # paths = " @ %mx);

   //and which path to use
   %random = mFloor(getRandom(1, %mx));
   %random--;
   %tg = getWord(%task.paths, %random);

   //echo("AIBombingRun::assume - path to use = " @ %tg);

   %task.group = nameToId("T" @ %team @ "BomberPath" @ %tg);
   %task.count = %task.group.getCount();  
   %task.locationIndex = 0;

   %client.needVehicle = true;
   %task.shouldWait = false;

   //set up the waitSpot for escort objectives
   %task.waitSpot = %task.location;
   for (%i = 0; %i < %task.count; %i ++)
   {
      if (%task.group.getObject(%i).waitTime !$= "")
      {
         %task.waitSpot = %task.group.getObject(%i).position;
         %task.shouldWait = true;
         break;
      }
   } 
}


function AIBombingRun::retire(%task, %client)
{
   //echo("AIBombingRun::retire - " @ getTaggedString(%client.name));

   if (aiClientIsAlive(%client))
   {
      %client.needVehicle = false;
      %client.clearStep();
      if(%client.player.isMounted())
         AIDisembarkVehicle(%client);
   }
}

function AIBombingRun::weight(%task, %client)
{
   //update the task weight...
   %task.baseWeight = %client.objectiveWeight;

   //let the monitor decide when to quit :)//--- NEEDED SO IMPORTANT !
   %task.setWeight(%task.baseWeight);

}

function AIBombingRun::monitor(%task, %client)
{
   //first, buy equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;
      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
         return;
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(20);
	 %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
         //no inventories, and heavies can't buy...
         if (%client.player.getArmorSize() $= "Heavy")
         {
            if (%task == %client.objectiveTask)
	         {
	            AIUnassignClient(%client);
	            Game.AIChooseGameObjective(%client);
               return;
	         }
         }
         //else just fly the dam thing
         else
         {
            %task.setMonitorFreq(20);
            %client.needEquipment = false; 
         }   
      }
   }
   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   //chat - send the message
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
	 {
	    if (%task.chat !$= "")
	    {
	       %chatMsg = getWord(%task.chat, 0);
	       %chatTemplate = getWord(%task.chat, 1);
	       if (%chatTemplate !$= "")
		  AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
	       else
		  AIMessageThread(%task.chat, %client, -1);
	    }
         }
      }
   }

   if (%client.needVehicle)
   {
      //reset pilot's wait time for passengers
         %task.shouldWaitTime = getSimTime();

      //If we're in light or medium armor, buy the vehicle - Lagg...
      if (%client.player.getArmorSize() $= "Heavy")
      {
         %task.baseWeight = %client.objectiveWeight;
         %task.equipment = "Medium";
         %task.buyEquipmentSet = "MediumMissileSet";
         %client.needEquipment = true;
         return;
      }

      %blockName = BomberFlyer;

      //check to see if we can steal a ride
      %abV = aiSearchForRide(%client, %blockName, %task.location, 250, %task.mode);

      if (%abV <= 0 && %task.mode !$= "NoBuy" && %task.mode !$= "PassengerOnly")
      {
         %clVs = AIFindClosestVStation(%client, %task.location);
         if (%clVs > 0)
         {
            %closestVs = getWord(%clVs, 0);
            %closestVsDist = getWord(%clVs, 1);
         }
         if (%closestVs > 0 && %closestVsDist < 300 && !isObject(%client.player.mVehicle))
         {
            //If we're in light or medium armor, buy the vehicle - Lagg...
            if (%client.player.getArmorSize() !$= "Heavy")
            {       
               %task.setMonitorFreq(9);
               %buyResult = aiBuyVehicle(%blockName, %client, %closestVs);
            }
            else
            {
               //if ai in heavy armor go buy equipment...
               %task.baseWeight = %client.objectiveWeight;
               %task.equipment = "Medium";
               %task.buyEquipmentSet = "MediumMissileSet";
               %client.needEquipment = true;
               return;
            }
           
            if (%buyResult $= "InProgress")
            {
               //reset pilot's wait time for passengers
               %task.shouldWaitTime = getSimTime();
               return;
            }

            else if (%buyResult $= "Finished")
            {
               //if we are finished buying the vehicle, then we are done
               //this is handled in function Armor::AIonMount - Lagg... 7-9-2004
            }
            else if (%buyResult $= "Failed")
            {
               echo("AIBombingRunMonitor buyResult = FAILED " @ gettaggedString(%client.name));

               if (%task == %client.objectiveTask)
	       {
	          AIUnassignClient(%client);
	          Game.AIChooseGameObjective(%client);
                  return;
	       }
            }
         }
         else if ((%closestVs <= 0 || %closestVsDist >= 300) && !isObject(%client.player.mVehicle))
         {
            //error("AIBombingRun::monitor - No V Pad or To Far unassigning");
            //no VPad so quit
            if (%task == %client.objectiveTask)
	         {
	            AIUnassignClient(%client);
	            Game.AIChooseGameObjective(%client);
                    return;
	         }
         }
      }
      else if (%abV > 0)
      {
         %client.pilotVehicle = true;//needed to let ai mount pilot seat

         //reset pilot's wait time for passengers
         %task.shouldWaitTime = getSimTime();

         //if we are close to vehicle throw packs that won't fit
         if (hasLargePack(%client.player))
            %client.player.throwPack();

         if (%client.inWater)
         {               
            %vPos2D = getWord(%abV.getPosition(), 0) SPC getWord(%abV.getPosition(), 1) SPC "0";
            %myPos2D = getWord(%client.player.position, 0) SPC getWord(%client.player.position, 1) SPC "0";
            if (vectorDist(%myPos2D, %vPos2D) < 2.5)
            {
               %client.pressJump();
               %client.stepMove(%abV.getWorldBoxCenter(), 0.25, $AIModeGainHeight);
            }
            else
               %client.stepMove(%abV.position, 0.25, $AIModeExpress);
         }
         else 
            %client.stepMove(getWords(%abv.getTransform(), 0, 2), 0.25, $AIModeMountVehicle);

         return;
      }
      else
      {
         error("AIBombingRun::monitor - BIG FAILURE unassigning " @ getTaggedString(%client.name));

         //this should unassign the bot automatically below
         %client.needVehicle = false;
      }      
   }

   //if we managed to get in vehicle then go
   if (%client.player.isMounted() && isObject(%client.player.mVehicle))
   {
      //update monitor frequency
      %task.setMonitorFreq(20);

      //set a time limit on this task in case he gets stuck somewhere
      %time = getSimTime() - %client.getTaskTime();
      if (%time > $aiNeedARideTaskTime)
      {
         //set the index to the last
         %task.locationIndex = %task.count - 1;

         //eject, eject, eject
         AIDisembarkVehicle(%client);
      }

      //get some vehicle data that we will require
      %vehicle = %Client.vehicleMounted;
      %mySpd = VectorLen(%vehicle.getVelocity());
      %vehicleDb = %vehicle.getDataBlock();
      %skill = %client.getSkillLevel();// --------------------- %skill *

      //make sure we got in the correct vehicle
      if (%vehicleDb.getName() !$= "BomberFlyer" )
      {
         error("AIBombingRun::monitor - opps we got in wrong vehicle");//believe it or not but i have seen everything - Lagg...
         AIDisembarkVehicle(%client); //Hop off...
      }
      
      //set the flight path
      %location = %task.group.getObject(%task.locationIndex).position;
      %speed = %task.group.getObject(%task.locationIndex).speed;
      if (%speed $= "")
         %speed = 1.0;
      
      //find out our altitude 
      %myPos = %vehicle.getWorldBoxCenter();//---------------------------------------- %myPos *
      %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType |
        $TypeMasks::ForceFieldObjectType;
      %downVec = getWord(%myPos, 0) @ " " @ getWord(%myPos, 1) @ "0";
      %altSur = containerRayCast(%myPos, %downVec, %mask, 0);
      %altPos = posfromRayCast(%altSur);
      %terHt = getTerrainHeight(%altPos);// ------------------------------------------ %terHt *
      %myAlt = getWord(%myPos, 2) - getWord(%altPos, 2);// --------------------------- %myAlt *

      %myZLev = getWord(%myPos, 2);

      //echo("distance to next marker = " @ VectorDist(%myPos, %location));//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      //---------------------------------------------------------------------------------- Avoidance --- 
      InitContainerRadiusSearch(%myPos, 5.0, $TypeMasks::InteriorObjectType
        | $TypeMasks::TSStaticShapeObjectType);
      %avoid = containerSearchNext();

      if ((%mySpd <= 0.1 && %avoid) || %mySpd == 0)//this needs some attention either %mySpd is 0 or it is not !
      {
         %vx = getWord(%myPos, 0);
         %vy = getWord(%myPos, 1);
         %vz = getWord(%myPos, 2);
         %vR = getWords(%vehicle.getTransform(), 3, 6);

         %client.setPilotPitchRange(-0.025, 0.025, 0.025);
         %client.setPilotDestination(%vx SPC %vy SPC %vz + 5, 1.0);// - max speed
         %vehicle.setTransform(%vx SPC %vy SPC %vz + 2 SPC %vR);// - set the Transform Here up a little

         //this was a test that did the job but did not look good at all
         //%vehicle.setTransform(%vx SPC %vy SPC (%vz + 5) SPC 1 SPC 0 SPC 0 SPC 0);// - set the Transform Here up a little
         
         %client.pressJet();// - hit the gas

         error("AIBombingRun::monitor - We Are Stuck - " @ getTaggedString(%client.name));
         echo("AIBombingRun::monitor - Altitude = " @ %myAlt);
         echo("AIBombingRun::monitor - SPEED = " @ %mySpd);
         echo(" ");
      }
      //-------------------------------------------------------------------------------- End Avoidance --- 

      // - if we have an empty seat, wait for ($aiVehicleWaitTime) milliseconds

      //tower this is bluebird requesting clearance to take off
      //if (%task.shouldWait)

      //this should make him follow path before a marker with a wait time is located :)
      if (%task.shouldWait && %task.group.getObject(%task.locationIndex).waitTime !$= "")
      {
         ////reset pilot's wait time for passengers
        //%task.shouldWaitTime = getSimTime();

         // - get the passenger seat: 0 = empty 1 = full
         for(%i = 0; %i < %vehicle.getDataBlock().numMountPoints; %i++)
         {
            %empty = false;
            if (!%vehicle.getMountNodeObject(%i))
            {
	       %empty = true;
	       break;
            }
         }

         //check if wait timer is up or bomber is full
         if (!%empty || getSimTime() - %task.group.getObject(%task.locationIndex).waitTime >
           %task.shouldWaitTime)
            %task.shouldWait = false;

         //set the pitch
         %client.setPilotPitchRange(-0.025, 0.025, 0.025);//real smooth setting here

         // - if can't see location marker gain height
         %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
         %hasLOS = !containerRayCast(%myPos, %location, %mask, 0);//------- %hasLOS to %location ?

         //if we get blocked fly straight up a little
         if (!%hasLOS)
         {
            error("if we get blocked fly straight up a little");//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

            %x = firstWord(%myPos);
            %y = getWord(%myPos, 1);
            %z = getWord(%myPos, 2);
            %client.setPilotPitchRange(-0.025, 0.025, 0.025);//real smooth setting here
            %client.setPilotDestination(%x SPC %y SPC %z + 10, 1.0);// -- if we can't see %location move straight up ^
         }
         //else fly straight to wait spot
         else if (VectorDist(%myPos, %location) < 10)
         {
            if(%task.group.getObject(%task.locationIndex).waitTime $= "")
            {
               if ((%task.count - 1) > %task.locationIndex)//has to be -1 on the groups count
                  %task.locationIndex++;
               //we are at end of trail
	       else
               {
                  if (%client.vehicleMounted )
                  {
                     AIDisembarkVehicle(%client); //Hop off...
                     %client.stepMove(%location, 0.25);
                     return;
                  }
	       } 
            }
            else
               %client.setPilotDestination(%location, 0); //stop and wait
         }
         else if (VectorDist(%myPos, %location) < 25)
            %client.setPilotDestination(%location, 0.07); //move to wait spot real,real slow
         else
	    %client.setPilotDestination(%location, 0.4); //move to wait spot, slow
      }       
      //wait time is up or we are fully loaded
      else 
      {
         //clear for take off, please fasten your seat belts :)
         %client.setPilotPitchRange(-0.005, 0.005, 0.0025);
          
         //are we close to location index marker?
	 if (VectorDist(%location, %client.vehicleMounted.position) < 50)//75 meters from marker//+++++++++++++++++++++++++++++++++++++++++++CHANGED HERE 9/9/06 LAGG...
	 {
	    //if we have another location index
            if ((%task.count - 1) > %task.locationIndex)//has to be -1 on the groups count
               %task.locationIndex++;
            //we are at end of trail
	    else
            {
               if (%task == %client.objectiveTask)
	       {                  
                  if (%client.vehicleMounted )
                  {
                      AIDisembarkVehicle(%client); //Hop off...
                      %client.stepMove(%location, 0.25);
                      return;
                  }
	       }
            }
         }
         // ------------------ FLIGHT CONTROL -----------------
         else
         {
            // - lets keep the pilot at about same height as location marker
            // - Good Luck if you want to mess around with these settings.

            // ---------------------------------------- lets get some data
            %transform = %vehicle.getTransform();
            %x = firstWord(%transform);
            %y = getWord(%transform, 1);
            %z = getWord(%transform, 2);

            //use the Y-axis of the vehicle rotation as the desired direction to check
            //and calculate a point 125m in front of vehicle - Lagg...
            %point = MatrixMulVector("0 0 0 " @ getWords(%vehicle.getTransform(), 3, 6), "0 1 0");
            %point = VectorNormalize(%point);
            %factor = (100 + %mySpd);//125m in front of vehicle is the right spot to start
            %checkPoint = VectorAdd(%myPos,VectorScale(%point, %factor));
            
            //check if pilot can see the next flight path marker
            %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
            %hasLOS = !containerRayCast(%myPos, %location, %mask, 0);

            //gain height if we can't see the next path marker
            if (!%hasLOS)
            {
               %client.setPilotPitchRange(-0.006, 0.003, 0.0025);
               %client.setPilotDestination(%x SPC %y SPC %z + 10, 1.0);
            }
            // ************************************************ leave this in here 1-27-2005 Lagg...
            //if we are way to low fly straight up
            else if ((%myZLev + 40) < getWord(%location, 2))//new
            {

               //error("BUT THIS IS PROBLEM WITH BOMBER RIGHT?");
               
               %client.setPilotPitchRange(-0.006, 0.003, 0.0025);//smooth setting here

               //aim at the next flight path marker
               %client.setPilotAim(%location);

               //******************** maybe try just press jet here ?
               %client.setPilotDestination(%x SPC %y SPC (getWord(%location, 2) + 10), 1.0);
            }
            else
            {
               //we are clear to fly to next marker, adjust pitch so we stay kinda level
               %dif = (getWord(%checkPoint, 2) - getWord(%location, 2));

               //-------------------------------------- we are in a crash drive PULL IT UP! - DIVE 1
               if (%dif < -25)
                  applyKick(%vehicle, "down", 0.5, true);
               //-------------------------------------- we are in a drive PULL UP - DIVE 2
               else if (%dif < -15)
                  %client.setPilotPitchRange(-0.01, 0.001, 0.05);
               //-------------------------------------- we are nose down a little bit - DIVE 3
               else if (%dif < -5)
                  %client.setPilotPitchRange(-0.007, 0.002, 0.005);
               //-------------------------------------- we are Going Balistic Maveric - NOSE UP 1
               else if (%dif > 25)
                  applyKick(%vehicle, "up", 0.5, true);
               //-------------------------------------- we are nose up a lot         - NOSE UP 2
               else if (%dif > 15)
                  %client.setPilotPitchRange(-0.0, 0.001, 0.025);
               //-------------------------------------- we are nose up a little bit - NOSE UP 3
               else if (%dif > 5)
                  %client.setPilotPitchRange(-0.001, 0.005, 0.015);               
               else// --------------------------------- else fly normally
                  %client.setPilotPitchRange(-0.005, 0.0025, 0.0025);//real smooth setting here

               //fly to location at speed
               %client.setPilotDestination(%location, %speed);              
            }
         }
      }
   }
   //else task must be completed, unassign the client
   else// if (%task == %client.objectiveTask)
   {
      AIUnassignClient(%client);
      Game.AIChooseGameObjective(%client);
      return;
   }
}
