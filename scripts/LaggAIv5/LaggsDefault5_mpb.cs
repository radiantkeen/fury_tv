//taken from stations.cs fixes spam when the mpb gets destroyed
function MobileInvStation::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType)
{
   //If vehicle station is hit then apply damage to the vehicle
   if (isObject(%targetObject.getObjectMount()))//check if mpb is still there - Lagg... 7-13-2004
      %targetObject.getObjectMount().damage(%sourceObject, %position, %amount, %damageType);
}

////////////////////////////////////////////////////////////////////////////////
//               Mobile Point Base from classic mod                           //
//                       z0dd - ZOD, 4/24/02                                  //
////////////////////////////////////////////////////////////////////////////////

//allows MPB to deploy closer to objects
function MobileBaseVehicle::checkDeploy(%data, %obj)
{
   %mask = $TypeMasks::VehicleObjectType | $TypeMasks::MoveableObjectType |
           $TypeMasks::StaticShapeObjectType | $TypeMasks::ForceFieldObjectType |
           $TypeMasks::ItemObjectType | $TypeMasks::PlayerObjectType | 
           $TypeMasks::TurretObjectType | //$TypeMasks::StaticTSObjectType | 
           $TypeMasks::InteriorObjectType;

   //%slot 1 = turret   %slot 2 = station
   %height[1] = 0;
   %height[2] = 0;
   %radius[1] = 2.4;
   %radius[2] = 2.4;
   %stationFailed = false;
   %turretFailed = false;
   
   for(%x = 1; %x < 3; %x++)
   {
      %posXY = getWords(%obj.getSlotTransform(%x), 0, 1);
      %posZ = (getWord(%obj.getSlotTransform(%x), 2) + %height[%x]);
      InitContainerRadiusSearch(%posXY @ " " @ %posZ, %radius[%x], %mask); 
   
      while ((%objFound = ContainerSearchNext()) != 0)
      {
         if(%objFound != %obj)
         {   
            if(%x == 1)
               %turretFailed = true;   
            else
               %stationFailed = true;   
            break;
         }
      }
   }
   
   //If turret, station or both fail the send back the error message...
   if(%turretFailed &&  %stationFailed)
      return "Both Turret and Station are blocked and unable to deploy.";
   if(%turretFailed)
      return "Turret is blocked and unable to deploy.";
   if(%stationFailed)
      return "Station is blocked and unable to deploy.";

   //Check the station for collision with the Terrain
   %mat = %obj.getTransform();
   for(%x = 1; %x < 7; %x+=2)
   {
      %startPos = MatrixMulPoint(%mat, %data.stationPoints[%x]);
      %endPos = MatrixMulPoint(%mat, %data.stationPoints[%x+1]);

      %rayCastObj = containerRayCast(%startPos, %endPos, $TypeMasks::TerrainObjectType, 0);
      if(%rayCastObj)
         return "Station is blocked by terrain and unable to deploy.";      
   }
   
   return "";   
}

//-------------------------------------------------- let the mpb deploy next to buildings - Lagg... 7-5-2004
function MobileBaseVehicle::checkTurretDistance(%data, %obj)
{
   %pos = getWords(%obj.getTransform(), 0, 2);
   InitContainerRadiusSearch(%pos, 20, $TypeMasks::TurretObjectType); // z0dd - ZOD, 6/21/02. Allow closer deploy. Was 100
   while ((%objFound = ContainerSearchNext()) != 0)
   {
      if(%objFound.getType() & $TypeMasks::TurretObjectType)
      {
         if(%objFound.getDataBlock().ClassName $= "TurretBase")
            return "Turret Base is in the area. Unable to deploy.";
      }
      else
      {
         %subStr = getSubStr(%objFound.interiorFile, 1, 4);
         if(%subStr !$= "rock" && %subStr !$= "spir" && %subStr !$= "misc")
            return "Building is in the area. Unable to deploy.";
      }        
   }
   return "";
}

//-----------------------------------------------------------------------
//add mpb inventory station to the $aiinventory set so bots can use it
//allows modular turret and teleporter as well (taken from Classic mod)
function MobileBaseVehicle::vehicleDeploy(%data, %obj, %player, %force)
{ 
   if(VectorLen(%obj.getVelocity()) <= 0.1 || %force)
   {
      %deployMessage = "";
      if( (%deployMessage = %data.checkTurretDistance(%obj)) $= "" || %force)
      {
         if(%obj.station $= "")
         {
            if( (%deployMessage = %data.checkDeploy(%obj)) $= "" || %force)
            {
               %obj.station = new StaticShape() {
                  scale = "1 1 1";
                  dataBlock = "MobileInvStation";
                  lockCount = "0";
                  homingCount = "0";
                  team = %obj.team;
                  vehicle = %obj;
               };

               //add the station to the $AIInventorySet - Lagg... 9-18-2003
               if ($Host::BotsEnabled)
                  $AIInvStationSet.add(%obj.station);
                
               %obj.station.startFade(0,0,true);
               %obj.mountObject(%obj.station, 2);
               %obj.station.getDataBlock().createTrigger(%obj.station);
               %obj.station.setSelfPowered();
               %obj.station.playThread($PowerThread,"Power");
               %obj.station.playAudio($HumSound,StationHumSound);
               %obj.station.vehicle = %obj;
               %obj.turret = new turret() {
                  scale = "1 1 1";
                  dataBlock = "MobileTurretBase";
                  lockCount = "0";
                  homingCount = "0";
                  team = %obj.team;
               };                
               %obj.turret.setDamageLevel(%obj.getDamageLevel());
               %obj.mountObject(%obj.turret, 1);
               %obj.turret.setSelfPowered();
               %obj.turret.playThread($PowerThread,"Power");
               
               //%obj.turret.mountImage(MissileBarrelLarge, 0 ,false);
               // -----------------------------------------------------
               // z0dd - ZOD, 4/25/02. modular MPB turret
               if(%obj.barrel !$= "")
               {
                  %obj.turret.mountImage(%obj.barrel, 0 ,false);
               }
               else
               {
                  %obj.turret.mountImage(MissileBarrelLarge, 0 ,false);
               }
               // -----------------------------------------------------

               %obj.beacon = new BeaconObject() {
                  dataBlock = "DeployedBeacon";
                  position = %obj.position;
                  rotation = %obj.rotation;
                  team = %obj.team;
               };
               %obj.beacon.setBeaconType(friend);
               %obj.beacon.setTarget(%obj.team);

               //we dont want to set up teleporter for respawning MPBs - Lagg... 10-1-2003
               //have to do search for spawns when teleporting, in case the area became blocked (future project)
               if (!%obj.deployed)
                  checkSpawnPos(%obj, 20);
            }
         }
         else
         {
            %obj.station.setSelfPowered();
            %obj.station.playThread($PowerThread,"Power");
            %obj.turret.setSelfPowered();
            %obj.turret.playThread($PowerThread,"Power");
         }
         if(%deployMessage $= "" || %force)
         {
            if(%obj.turret.getTarget() == -1)
            {   
               %obj.turret.setTarget(%obj.turret.target);
            }   
            %obj.turret.setThreadDir($DeployThread, true);
            %obj.turret.playThread($DeployThread,"deploy");
            %obj.turret.playAudio($DeploySound, MobileBaseTurretDeploySound);
         
            %obj.station.notDeployed = 1;
            %obj.setThreadDir($DeployThread, true);
            %obj.playThread($DeployThread,"deploy");  
            %obj.playAudio($DeploySound, MobileBaseDeploySound);
            %obj.deployed = 1;
            %obj.deploySchedule = "";
            %obj.disableMove = true;
            %obj.setFrozenState(true);
            if(isObject(%obj.shield))
               %obj.shield.delete();

            %obj.shield = new forceFieldBare()
            {
               scale = "1.22 1.8 1.1";
               dataBlock = "defaultTeamSlowFieldBare";
               team = %obj.team;
            };
            %obj.shield.open();
            setTargetSensorData(%obj.getTarget(), MPBDeployedSensor);
         }
      }
      if(%deployMessage !$= "")
         messageClient(%player.client, '', %deployMessage); 

      return true;
   }
   else
   {
      return false;
   }
}

//------------------------------------------------------------------------------------------
// Gets called from function MobileBaseVehicle::vehicleDeploy(%data, %obj, %player, %force).
// Passes this information to the MPBTeleporter::teleportOut function.
//------------------------------------------------------------------------------------------

function checkSpawnPos(%MPB, %radius)
{
   for(%y = -1; %y < 1; %y += 0.25)
   {
      %xCount=0;
      for(%x = -1; %x < 1; %x += 0.25)
      {
         $MPBSpawnPos[(%yCount * 8) + %xCount] = %x @ " " @ %y; 
         %xCount++;
      }
      %yCount++;
   }
   %count = -1;

   for(%x = 0; %x < 64; %x++)
   {
      %pPos = getWords(%MPB.getTransform(), 0, 2);
      %pPosX = getWord(%pPos, 0);
      %pPosY = getWord(%pPos, 1);
      %pPosZ = getWord(%pPos, 2);
      
      %posX = %pPosX + ( getWord($MPBSpawnPos[%x],0) * %radius);
      %posY = %pPosY + (getWord($MPBSpawnPos[%x],1) * %radius);
      
      %terrHeight = getTerrainHeight(%posX @ " " @ %posY);

      if(mAbs(%terrHeight - %pPosZ) < %radius )
      {
         %mask = $TypeMasks::VehicleObjectType     | $TypeMasks::MoveableObjectType   |
                 $TypeMasks::StaticShapeObjectType | $TypeMasks::StaticTSObjectType   | 
                 $TypeMasks::ForceFieldObjectType  | $TypeMasks::ItemObjectType       | 
                 $TypeMasks::PlayerObjectType      | $TypeMasks::TurretObjectType     |
                 $TypeMasks::InteriorObjectType;

         InitContainerRadiusSearch(%posX @ " " @ %posY @ " " @ %terrHeight, 2, %mask);
         if(ContainerSearchNext() == 0)
            %MPB.spawnPos[%count++] = %posX @ " " @ %posY @ " " @ %terrHeight;                  
      }
   }   
   %MPB.spawnPosCount = %count;
}

//--------------------------------------------------------------------------------------------- siege gamespam fix ---
function SiegeGame::vehicleDestroyed(%game, %vehicle, %destroyer)
{
  //needed to prevent Siege Game consol spam - Lagg... 11-12-2003   
}

//--------------------------------------------------------------------------------------------------------------------
//set up the mpb teleprter on team switch sides - Lagg...
function SiegeGame::objectSwapVehiclePads(%game, %this)
{
   %defTeam = %game.offenseTeam == 1 ? 2 : 1;

   if(%this.getDatablock().getName() $= "StationVehicle")
   {
      if(%this.getTarget() != -1)
      {
         // swap the teams of both the vehicle pad and the vehicle station
         if(getTargetSensorGroup(%this.getTarget()) == %game.offenseTeam)
         {
            setTargetSensorGroup(%this.getTarget(), %defTeam);
            %this.team = %defTeam;
            setTargetSensorGroup(%this.pad.getTarget(), %defTeam);
            %this.pad.team = %defTeam;
            // z0dd - ZOD, 4/20/02. Switch the teleporter too.
            setTargetSensorGroup(%this.teleporter.getTarget(), %defTeam);
            %this.teleporter.team = %defTeam;
         }
         else if(getTargetSensorGroup(%this.getTarget()) == %defTeam)
         {
            setTargetSensorGroup(%this.getTarget(), %game.offenseTeam);
            %this.team = %game.offenseTeam;
            setTargetSensorGroup(%this.pad.getTarget(), %game.offenseTeam);
            %this.pad.team = %game.offenseTeam;
            // z0dd - ZOD, 4/20/02. Switch the teleporter too.
            setTargetSensorGroup(%this.teleporter.getTarget(), %game.offenseTeam);
            %this.teleporter.team = %game.offenseTeam;
         }
      }
   }
}

//---------------------------------------------------------------------------------
//check for MPB falling through terrain
function WheeledVehicle::checkFreefall(%obj)					
{
   //echo("WheeledVehicle::checkFreefall");

   if(getword(%obj.getposition(), 2) > -2000)
   {
      cancel(%obj.freefallThread);
      %obj.freefallThread = %obj.schedule(10000, checkFreefall);
      return;
   }
   echo("BLACK HOLE: MPB " @ %obj @ " found at " @ %obj.position @ "m altitude and was destroyed");
   messageteam(%obj.team, 'msg', "\c2Your team's MPB fell into a black hole and was destroyed~wfx/misc/lightning_impact.wav");
   %obj.damage(0, %obj.position, 99999, $DamageType::Lightning);
}
