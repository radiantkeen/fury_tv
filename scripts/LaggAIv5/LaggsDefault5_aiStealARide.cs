//-------------------------------------------------------------------------------------------------- AIOStealARide ---
//Place AIO Objective Marker in the spot you want pilot to park and wait for passegers
//Set up to 10 simgroups called "T" @ %team @ "CapPath" @ %num
//(example T2CapPath1 to T2CapPath10 / T1CapPath1 to T1CapPath10)
//NOTE: must start with number 1 and count in order up to 10
//inside these simgroups place markers in the order to follow for each
//path. Thats 1 simgroup per path (up to 10) for each team - Lagg... 4-8-2004

function AIOStealARide::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   // if were playing CnH, check who owns this (if we own it forget it)
   if (%this.targetObjectId > 0 && %this.clientLevel1 != %client)
   {
      if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden() || !%this.targetObjectId.isEnabled() ||
        %this.targetObjectId.team == %client.team)
         return 0;     
   }

   if (%this.mode $= "FLAG" && (!%client.player.holdingFlag || %client.player.getArmorSize() $= "Heavy"))
      return 0;

   //never bump a pilot off his ride
   if (%this.clientLevel1 > 0  && %level == 1 && %this.clientLevel1 != %client)
      return 0;

   //check if flight path exists
   if (Game.class $= "SiegeGame")
      %team = Game.offenseTeam == %client.team ? 1 : 2;
   else
      %team = %client.team;

   if(! isObject(nameToId("T" @ %team @ "CapPath1")))
   {
      error("AIOStealARide - No Flight Path Exists ! Missing Simgroup named <T" @ %team @ "CapPath1>");
      return 0;
   }

   //do a quick check to disqualify this objective if it can't meet the minimum weight
   if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
   {
      if (%this.targetObjectId > 0 && %this.issuedByClientId == %client.controlByHuman)
      {
         if ($AIWeightHumanIssuedCommand < %minWeight)
	    return 0;
      }
      else
         return 0;
   }

   //check for Vehicle station near buy of forget it if we are not mounted yet - Lagg...
   //would like to add a check for unpiloted vehicles with passengers that need a ride (someday hehe)

   //right now he only checks for vehicles within 150 of this objective marker position. Could be made to check all along the way home
   // for abandon veh by making changes here and in monitor below (maybe)                                                          - lagg...

   if (!%client.player.isMounted() && !%client.justBought)
   {
      //check to see if we can steal a flyer
      for (%i = 0; %i < $aiVehicleSet.getCount(); %i ++)
      {
         %abv = false;
         %vehicle = $aiVehicleSet.getObject(%i);
         if (isObject(%vehicle))
         {            
            %type = %vehicle.getDataBlock().getName();
            if (%vehicle.getMountNodeObject(0) <= 0 && (%type $= "BomberFlyer" || %type $= "ScoutFlyer" || %type $= "HAPCFlyer"))
            {
               %dis = VectorDist(%client.player.position, %vehicle.position);
               %dis1 = VectorDist(%vehicle.position, %this.position);
               %dis2F = VectorDist(%client.player.position, $flagPos[%client.team]);

               //error("AIOStealARide::weight - Dis2F = " @ %dis2F);
                
               if (%dis < 300 && %dis1 < 300)
               {
                  //error("AIOStealARide::weight found one");

                  %abv = true;
                  break;
               }
            }
         }
      }

      if (!%abv)
      {
         //error("AIOStealARide::weight No veicles Found");
         return 0;
      }
   }

   %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);

   //never bump a pilot off his ride
   if (%this.clientLevel1 > 0  && %level == 1 && %this.clientLevel1 != %client)
      return 0;
   if (%this.clientLevel1 == %client)
      %weight = 10000;

   return %weight;
}

function AIOStealARide::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIStealARide);
   %client.objectiveTask.initFromObjective(%this, %client);

   //error("AIOStealARide::assignClient " @ getTaggedString(%client.name));
}

function AIOStealARide::unassignClient(%this, %client)
{
   //error("AIOStealARide::unassignClient " @ getTaggedString(%client.name));
           
   if(%client.pilotVehicle)
      AIDisembarkVehicle(%client);
  
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//----------------------------------------------------------------------------------------------- AI Need A Ride ---

function AIStealARide::initFromObjective(%task, %objective, %client)
{
   //initialize the task vars from the objective
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;

   //task location gets updated as pilot flys to markers in monitor
   %task.location = %objective.position;

   %task.equipment = %objective.equipment;
   %task.buyEquipmentSet = %objective.buyEquipmentSet;
   %task.desiredEquipment = %objective.desiredEquipment;
   %task.issuedByClient = %objective.issuedByClientId;
   %task.chat = %objective.chat;

   //initialize other task vars
   %task.sendMsg = true;
   %task.sendMsgTime = 0;
   %task.path = "";

   %client.player.mVehicle = "";// needed if bot just hopped out a vehicle and it was not destroyed/removed yet
}

function AIStealARide::assume(%task, %client)
{
   %task.setWeightFreq(40);
   %task.setMonitorFreq(40);
   
   %client.needEquipment = AINeedEquipment(%task.equipment, %client);

   //even if we don't *need* equipemnt, see if we should buy some... 
   if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
   {
      //see if we could benefit from inventory
      %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      %closestDist = getWord(%result, 1);
      if (AINeedEquipment(%task.desiredEquipment, %client) && %closestInv > 0)
      {
         %result = AIFindClosestEnemy(%client, 100, $AIClientLOSTimeout);
	 %closestEnemy = getWord(%result, 0);
	 %closestEnemydist = getWord(%result, 1);

	 if (%closestEnemy <= 0 || (%closestEnemyDist > %closestDist * 1.5))
	    %client.needEquipment = true;
      }
   }

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();

   //reset the vehicle station wait time
   %client.vsWaitTime = "";

   //set the destination paths for each team and game type
   
   //first see how many paths we have
   //check if flight path exists
   if (Game.class $= "SiegeGame")
      %team = Game.offenseTeam == %client.team ? 1 : 2;
   else
      %team = %client.team;

   for(%i = 1; %i < 10; %i++)
   {
      if(isObject(nameToId("T" @ %team @ "CapPath" @ %i)))
         %mx = %i;
   }

   %random = mFloor(getRandom(1, %mx));

   // - set the path simgroup to follow
   %task.group = nameToId("T" @ %team @ "CapPath" @ %random);      
   
   %task.count = %task.group.getCount();  
   %task.locationIndex = 0;
   %client.needVehicle = true;

   if (%client.player.holdingFlag > 0)
      %task.shouldWait = false;
   else
      %task.shouldWait = true;

   //set up the waitSpot for escort objectives
   %task.waitSpot = %task.location;
   for (%i = 0; %i < %task.count; %i ++)
   {
      if (%task.group.getObject(%i).waitTime !$= "")
      {
         %task.waitSpot = %task.group.getObject(%i).position;
         break;
      }
   }
}


function AIStealARide::retire(%task, %client)
{
   if (aiClientIsAlive(%client))
   {
      %client.needVehicle = false;
      %client.clearStep();
      if(%client.player.isMounted())
         AIDisembarkVehicle(%client);
   }
}

function AIStealARide::weight(%task, %client)
{
   //update the task weight...
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;

   //let the monitor decide when to quit :)//--- NEEDED SO IMPORTANT !
   %task.setWeight(%task.baseWeight);

}

function AIStealARide::monitor(%task, %client)
{
   //first, buy equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;
      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
         return;
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(40);
	 %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
         %task.setMonitorFreq(40);
	 %client.needEquipment = false;
      }
   }
   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   //chat - send the message
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
	 {
	    if (%task.chat !$= "")
	    {
	       %chatMsg = getWord(%task.chat, 0);
	       %chatTemplate = getWord(%task.chat, 1);
	       if (%chatTemplate !$= "")
		  AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
	       else
		  AIMessageThread(%task.chat, %client, -1);
	    }
         }
      }
   }

   if (%client.needVehicle)
   {
      //see if we can steal a ride
      //check to see if we can steal a flyer
      for (%i = 0; %i < $aiVehicleSet.getCount(); %i ++)
      {
         %abv = false;
         %vehicle = $aiVehicleSet.getObject(%i);
         if (isObject(%vehicle)) // - could add a check here if vehicle damaged forget it *** Lagg...
         {            
            %type = %vehicle.getDataBlock().getName();
            if (%vehicle.getMountNodeObject(0) <= 0 && (%type $= "BomberFlyer" || %type $= "ScoutFlyer" || %type $= "HAPCFlyer"))
            {
               %dis = VectorDist(%client.player.position, %vehicle.position);
               %dis1 = VectorDist(%vehicle.position, %task.location);
                
               if (%dis < 150 && %dis1 < 150)
               {
                  %abv = true;
                  
                  //if we are close to vehicle throw packs that won't fit
                  if (%dis < 25)
                  {
                     //throw away any packs that won't fit
                     if (hasLargePack(%client.player))
                        %client.player.throwPack();
                  }

                  break;
               }
            }
         }
      }

      if (%abv)
      {
         //error("we be steping to the vehicle now");

         %client.justBought = true;
         %client.pilotVehicle = true;//needed to let ai mount pilot seat

         //reset pilot's wait time for passengers
         %task.shouldWaitTime = getSimTime();
         
         //%client.mountingVehicle = true;
         %client.stepMove(%vehicle.getSlotTransform(0), 0.25, $AIModeMountVehicle);//*** TESTING HERE ***
         //%client.stepMove(%vehicle.getSlotTransform(0), 0.25, $AIModeExpress);
      }
      else if (!%abv)
      {
          //error("Monitor is this the problem????");

         //if this task is the objective task, choose a new objective
	 if (%task == %client.objectiveTask)
	 {
	    AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
            return;
	 }
       }
          
   }

   //if we managed to get in vehicle then go
   if (%client.player.isMounted() && isObject(%client.player.mVehicle))
   {
      // - set a time limit on this task in case he gets stuck somewhere
      %time = getSimTime() - %client.getTaskTime();
      if (%time > $aiNeedARideTaskTime)
      {
         %task.locationIndex = %task.count - 1;
         AIDisembarkVehicle(%client);
      }

      //get the vehicle data we will require
      %vehicle = %Client.vehicleMounted;
      %mySpd = VectorLen(%vehicle.getVelocity());
      

      //------------------------------------------------------------------ get our altitude ---
      //find out our altitude 
      %myPos = %vehicle.getWorldBoxCenter();//---------------------------------------- %myPos *
      //%myVehiclePosition = %vehicle.getWorldBoxCenter();
      %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType |
        $TypeMasks::ForceFieldObjectType;
      %downVec = getWord(%myPos, 0) @ " " @ getWord(%myPos, 1) @ "0";
      %altSur = containerRayCast(%myPos, %downVec, %mask, 0);
      %altPos = posfromRayCast(%altSur);
      %terHt = getTerrainHeight(%altPos);// ------------------------------------------ %terHt *
      %myAlt = getWord(%myPos, 2) - getWord(%altPos, 2);// --------------------------- %myAlt *

      %myZLev = getWord(%myPos, 2);

      //---------------------------------------------------------------------------------- Avoidance --- 
      InitContainerRadiusSearch(%myPos, 5.0, $TypeMasks::InteriorObjectType
        | $TypeMasks::TSStaticShapeObjectType);
      %avoid = containerSearchNext();

      if ((%mySpd <= 0.1 && %avoid) || %mySpd == 0 || %myZLev - 5 <= %terHt)//this needs some attention either %mySpd is 0 or it is not !
      {
         
         //%v = %vehicle.getVelocity();
         %vx = getWord(%myPos, 0);
         %vy = getWord(%myPos, 1);
         %vz = getWord(%myPos, 2);
         %vz += 2.0;
         //%vz = %terHt + 5;
         %vR = getWords(%vehicle.getTransform(), 3, 6);

         //%client.setControlObject(%vehicle);//----------------------------------------- give pilot control of shrike
         %client.pressJet();//maybe we should try APPLYIMPUSLE UP here
         %client.setPilotPitchRange(-0.025, 0.025, 0.025);//real smooth setting here
         %client.setPilotDestination(%vx SPC %vy SPC %vz + 5, 1.0);//max speed
         %vehicle.setTransform(%vx SPC %vy SPC %vz SPC %vR);//------------------------ set the Transform Here up a little

         //error("AINeedARide::monitor - We Are Stuck - " @ getTaggedString(%client.name));
         //echo("AINeedARide::monitor - Altitude = " @ %myAlt);
         //echo("AINeedARide::monitor - SPEED = " @ %mySpd);
         //echo(" ");
         //return;

      }
      //-------------------------------------------------------------------------------- End Avoidance --- 

      // - if we have an empty seat, wait for ($aiVehicleWaitTime) milliseconds

      //are we full or should we wait for a passengers?
      if (%task.shouldWait)
      {
         // - get the passenger seat: 0 = empty 1 = full
         for(%i = 0; %i < %vehicle.getDataBlock().numMountPoints; %i++)
         {
            %empty = false;
            if (!%vehicle.getMountNodeObject(%i))
            {
	       %empty = true;
	       break;
            }
         }
         if (!%empty || getSimTime() - $aiVehicleWaitTime > %task.shouldWaitTime)
            %task.shouldWait = false;

         //%pos = %client.vehicleMounted.position;
         //%dest = %task.location;

         %client.setPilotPitchRange(-0.025, 0.025, 0.025);//real smooth setting here

         // - if can't see location marker gain height
         %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
         %hasLOS = !containerRayCast(%myPos, %task.location, %mask, 0);//------- %hasLOS to %task.location ?

         //if we get blocked fly straight up a little
         if (!%hasLOS)
         {
            %x = firstWord(%myPos);
            %y = getWord(%myPos, 1);
            %z = getWord(%myPos, 2);
            %client.setPilotPitchRange(-0.025, 0.025, 0.025);//real smooth setting here
            %client.setPilotDestination(%x SPC %y SPC %z + 10, 1.0);// -- if we can't see %task.location move straight up ^
         }
         //else fly straight to wait spot
         else
         {
            if (VectorDist(%task.location, %pos) < 10)
               %client.setPilotDestination(%task.location, 0); //stop and wait
            else if (VectorDist(%task.location, %pos) < 25)
               %client.setPilotDestination(%task.location, 0.05); //move real,real slow
            else
	       %client.setPilotDestination(%task.location, 0.3); //move to wait spot, slow
         }       
      }
      else 
      {
         //clear for take off, please fasten your seat belts :)
          
         //set the flight path
         %task.location = %task.group.getObject(%task.locationIndex).position;

         //%pos = %client.vehicleMounted.position;
	 //%dest = %task.location;

         //are we close to location index marker?
	 if (VectorDist(%task.location, %client.vehicleMounted.position) < 75)//75 meters from marker
	 {
	    //if we have another location index
            if ((%task.count - 1) > %task.locationIndex)//has to be -1 on the groups count
               %task.locationIndex++;
            //we are at end of trail
	    else
            {
               if (%task == %client.objectiveTask)
	       {                  
                  if (%client.vehicleMounted )
                  {
                      AIDisembarkVehicle(%client); //Hop off...
                      %client.stepMove(%task.location, 0.25);
                      return;
                  }
	       }
            }
         }
         else
         {
            // - lets keep the pilot at about same height as location marker
            if ((%myZLev + 5) < getWord(%task.location, 2))
            {
               %x = firstWord(%myPos);
               %y = getWord(%myPos, 1);
               %z = getWord(%task.location, 2);
               %client.setPilotPitchRange(-0.025, 0.025, 0.025);//real smooth setting here
               %client.setPilotDestination(%x SPC %y SPC %z, 1.0);
            }
            else
            {
               // - if can't see location marker gain height
               %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
               %hasLOS = !containerRayCast(%myPos, %task.location, %mask, 0);//------- %hasLOS to %task.location ?

               if (!%hasLOS)
               {
                  %x = firstWord(%myPos);
                  %y = getWord(%myPos, 1);
                  %z = getWord(%myPos, 2);
                  %client.setPilotPitchRange(-0.025, 0.025, 0.025);//real smooth setting here
                  %client.setPilotDestination(%x SPC %y SPC %z + 10, 1.0);// -- if we can't see %task.location move straight up ^
               }
               // - else fly normally
               else
               {
                  %client.setPilotPitchRange(-0.2, 0.05, 0.05);//rough setting here               
                  %client.setPilotDestination(%task.location, 1.0);//max speed
               }               
            }
         }
      }
   }
   //else task must be completed
   else if (%task == %client.objectiveTask)
   {
      AIUnassignClient(%client);
      Game.AIChooseGameObjective(%client);
      return;
   }
}
