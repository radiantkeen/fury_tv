//All tasks for deathmatch, hunters, and tasks that coincide with the current objective task live here...

//Weights for tasks that override the objective task: must be between 4300 and 4700
$AIWeightVehicleMountedEscort	= 4700;
$AIWeightReturnTurretFire		= 4675;
$AIWeightNeedItemBadly    		= 4650;
$AIWeightReturnFire        	= 4600;
$AIWeightDetectMine				= 4500;
$AIWeightTauntVictim        	= 4400;
$AIWeightNeedItem    			= 4350;
$AIWeightDestroyTurret			= 4300;

//Weights that allow the objective task to continue:  must be 3000 or less
$AIWeightFoundEnemy        	= 3000;
$AIWeightFoundItem    			= 2500;
$AIWeightFoundToughEnemy     	= 1000;
$AIWeightPatrolling        	= 2000;

//Hunters weights...
$AIHuntersWeightMustCap			= 4690;
$AIHuntersWeightNeedHealth		= 4625;
$AIHuntersWeightShouldCap		= 4425;
$AIHuntersWeightMustEngage		= 4450;
$AIHuntersWeightShouldEngage	= 4325;
$AIHuntersWeightPickupFlag		= 4425;

//Rabbit weights...
$AIRabbitWeightDefault			= 4625;
$AIRabbitWeightNeedInv			= 4325;

//Bounty weights...
$AIBountyWeightShouldEngage	= 4325;

//-----------------------------------------------------------------------------

//$AIWeightVehicleMountedEscort	= 6000;//was 4700 - Lagg...
$AIWeightDetectRemeq				= 3450;//added - Lagg...
$AIWeightDetectVehicle				= 3500;//added - Lagg...


//-----------------------------------------------------------------------------
//AIEngageTask is responsible for anything to do with engaging an enemy

function AIEngageWhoWillWin(%client1, %client2)
{
	//assume both clients are alive - gather some info
	if (%client1.isAIControlled())
		%skill1 = %client1.getSkillLevel();
	else
		//%skill1 = 0.5;
                %skill1 = 0.9;//people are good at this game, set accordingly - Lagg...

	if (%client2.isAIControlled())
		%skill2 = %client2.getSkillLevel();
	else
		//%skill2 = 0.5;
                %skill2 = 0.9;//people are good at this game, set accordingly - Lagg...


	%damage1 = %client1.player.getDamagePercent();
	%damage2 = %client2.player.getDamagePercent();

	//first compare health
	%tolerance1 = 0.5 + ((%skill1 - %skill2) * 0.3);
	%tolerance2 = 0.5 + ((%skill2 - %skill1) * 0.3);
	if (%damage1 - %damage2 > %tolerance1)
		return %client2;
	else if (%damage2 - %damage1 > %tolerance2)
		return %client1;

	//health not a problem, see how the equipment compares for the two...
	%weaponry1 = AIEngageWeaponRating(%client1);
	%weaponry2 = AIEngageWeaponRating(%client2);
	%effective = 20;
	if (%weaponry1 < %effective && %weaponry2 >= %effective)
		return %client2;
	else if (%weaponry1 >= %effective && %weaponry2 < %effective)
		return %client1;

	//no other criteria for now...  return -1 to indicate a tie...
	return -1;
}

function AIEngageTask::init(%task, %client)
{
}

function AIEngageTask::assume(%task, %client)
{
	%task.setWeightFreq(9);
	%task.setMonitorFreq(9);
	%task.searching = false;
	if (isObject(%client.shouldEngage.player))
		%task.searchLocation = %client.shouldEngage.player.getWorldBoxCenter();
}

function AIEngageTask::retire(%task, %client)
{

}

function AIEngageTask::weight(%task, %client)
{
   %player = %client.player;
	if (!isObject(%player))
		return;     

   if (%player.isMounted())
              AIProcessVehicle(%client);//added this call to help vehicle mounted bots - Lagg...

   %clientPos = %player.getWorldBoxCenter();
   %currentTarget = %client.shouldEngage;
	if (!AIClientIsAlive(%currentTarget))
		%currentTarget = %client.getEngageTarget();
   %client.shouldEngage = -1;
	%mustEngage = false;
	%tougherEnemy = false;

	//first, make sure we actually can engage
	if (AIEngageOutOfAmmo(%client))
	{
		%client.shouldEngage = -1;
		%task.setWeight(0);
		return;
	}

	//see if anyone has fired on us recently...
   %losTimeout = $AIClientMinLOSTime + ($AIClientLOSTimeout * %client.getSkillLevel());
   if (AIClientIsAlive(%client.lastDamageClient, %losTimeout) && getSimTime() - %client.lastDamageTime < %losTimeout)
   {
      //see if we should turn on the new attacker
      if (AIClientIsAlive(%currentTarget))
      {
         %targPos = %currentTarget.player.getWorldBoxCenter();
         %curTargDist = %client.getPathDistance(%targPos);
      
         %newTargPos = %client.lastDamageClient.player.getWorldBoxCenter();
         %newTargDist = %client.getPathDistance(%newTargPos);
      
         //see if the new targ is no more than 30 m further
         if (%newTargDist > 0 && %newTargDist < %curTargDist + 30)
         {
            %client.shouldEngage = %client.lastDamageClient;
				%mustEngage = true;
         }
      }
      else
      {
         %client.shouldEngage = %client.lastDamageClient;
			%mustEngage = true;
      }
   }

   //no one has fired at us recently, see if we're near an enemy
   else
   {
      //if we are an assassin we cheat, always have the edge... 
      if (%player.getInventory("CloakingPack") > 0)
         %result = AIFindClosestEnemyToLoc(%client, %player.getWorldBoxCenter(), 100, %losTimeout, true, true);
      else
         %result = AIFindClosestEnemy(%client, 100, %losTimeout);

      %closestEnemy = getWord(%result, 0);
      %closestdist = getWord(%result, 1);
      if (%closestEnemy > 0)
      {
         //see if we're right on top of them
         %targPos = %closestEnemy.player.getWorldBoxCenter();
         %dist = %client.getPathDistance(%targPos);
         
         if (%dist > 0 && %dist < 25)//was 20
         {
            %client.shouldEngage = %closestEnemy;
				%mustEngage = true;
         }
         
         //else choose them only if we're not already attacking someone
         else if (%currentTarget <= 0)
         {
            %client.shouldEngage = %closestEnemy;
				%mustEngage = false;

				//Make sure the odds are not overwhelmingly in favor of the enemy...
				if (AIEngageWhoWillWin(%client, %closestEnemy) == %closestEnemy)
					%tougherEnemy = true;
         }
      }
   }
   
   //if we still haven't found a new target, keep fighting the old one
   if (%client.shouldEngage <= 0)
   {
      if (AIClientIsAlive(%currentTarget))
		{
			//see if we still have sight of the current target
			%hasLOS = %client.hasLOSToClient(%currentTarget);
			%losTime = %client.getClientLOSTime(%currentTarget);
			if (%hasLOS || %losTime < %losTimeout)
		      %client.shouldEngage = %currentTarget;
			else
				%client.shouldEngage = -1;
		}
      else
			%client.shouldEngage = -1;
		%mustEngage = false;
   }

   //finally, set the weight
	if (%client.shouldEngage > 0)
	{
		if (%mustEngage)
		   %task.setWeight($AIWeightReturnFire);
		else if (%tougherEnemy)
		   %task.setWeight($AIWeightFoundToughEnemy);
		else
		   %task.setWeight($AIWeightFoundEnemy);
	}
	else
	   %task.setWeight(0);
}

function AIEngageTask::monitor(%task, %client)
{
	if (!AIClientIsAlive(%client.shouldEngage))
	{
		%client.stop();
		%client.clearStep();
		%client.setEngageTarget(-1);
		return;
	}

	%hasLOS = %client.hasLOSToClient(%client.shouldEngage);
	%losTime = %client.getClientLOSTime(%client.shouldEngage);
	//%detectLocation = %client.getDetectLocation(%client.shouldEngage);
	%detectPeriod = %client.getDetectPeriod();

	//if we can see the target, engage...
	if (%hasLOS || %losTime < %detectPeriod)
	{
           //added here so gen attackers go for gens :) - Lagg... 12-7-2003
           if (isObject(%client.objectiveTask) && %client.objectiveTask.getName() !$= "AIAttackObject" && %client.objectiveTask.getName() !$= "AIDeployEquipment")
           {
		%client.stepEngage(%client.shouldEngage);
		%task.searching = false;
		%task.searchLocation = %client.shouldEngage.player.getWorldBoxCenter();
           }
           else
           {
              %client.setEngageTarget(%client.shouldEngage);
              %task.searching = true;
           }
	}

	//else if we haven't for approx 5 sec...  move to the last known location
	else
	{
		//clear the engage target
		%client.setEngageTarget(-1);

		if (! %task.searching)
		{
			%dist = VectorDist(%client.player.getWorldBoxCenter(), %task.searchLocation);
			if (%dist < 4)
			{
				%client.stepIdle(%task.searchLocation);
				%task.searching = true;
			}
			else
				%client.stepMove(%task.searchLocation, 4.0);
		}
	}
}

//------------------------------------------------------------------------------------------------------------
//modified pickupitem task to include repairpack as a health item and fixed some other bugs - Lagg... 4-3-2003
//added features that include swaping packs / weapons for better ones on the ground
//------------------------------------------------------------------------------------------------------------
//AIPickupItemTask is responsible for anything to do with picking up an item

function AIPickupItemTask::init(%task, %client)
{
}

function AIPickupItemTask::assume(%task, %client)
{
   %task.setWeightFreq(10);
   %task.setMonitorFreq(10);

   %task.pickupItem = -1;
}

function AIPickupItemTask::retire(%task, %client)
{
   %task.pickupItem = -1;
}

function AIPickupItemTask::weight(%task, %client)
{
   //first, see if we are all there
   %player = %client.player;
   if (!isObject(%player))
      return;


   //then, see if we need to pick up health
   
   //if we are already locked onto an PickUp Item,
   // make sure it's still valid, and leave the weight it was set at... 
   if (%task.pickupItem > 0)
   {
      if (isObject(%task.pickupItem) && !%task.pickupItem.isHidden())
      {

         //if we are less then 1.5 distance then we have picked it up, and we should be done
         %dist = %client.getPathDistance(%task.pickupItem.getWorldBoxCenter());
	 if (%dist < 1.5 || %dist > 300)
         {
            %damage = %player.getDamagePercent();

	    %task.pickupItem = -1;
            %task.setWeight(0);
         }

         //has not reached the item yet, so just return until we get closer, leave the weight alone
         else
            return;
      }
      else
      {
         //this turns off the LOCK ON and lets the rest of the weight function figure out what we need
         %task.pickupItem = -1;
         %task.setWeight(0);
      }

   }

   //otherwise, search for objects

   %damage = %player.getDamagePercent();
   %healthRad = %damage * 100;

   %closestHealth = -1;
   %closestHealthDist = %healthRad;//this is stupid why ask for %healthRadius then declare it is now %closestHealthDist???
   %closestHealthLOS = false;
   %closestItem = -1;
   %closestItemDist = 32767;
   %closestItemLOS = false;

   if (isObject(%client.objectiveTask))
      %watTask = %client.objectiveTask.getName(); 

   //loop through the item list, looking for things to pick up
   %itemCount = $AIItemSet.getCount();
   for (%i = 0; %i < %itemCount; %i++)
   {
      %item = $AIItemSet.getObject(%i);
      %itemName = %item.getDataBlock().getName();
      if (!%item.isHidden())
      {
         //%watTask = "";
         %dist = %client.getPathDistance(%item.getWorldBoxCenter());
         //if (isObject(%client.objectiveTask))
            //%watTask = %client.objectiveTask.getName(); 


         if ( (%itemName $= "RepairPack" && %player.getInventory("RepairPack") <= 0 && %damage > 0.5 &&
           %watTask !$= "AIODeployEquipment")
            || (%item.isCorpse && %item.getInventory("RepairKit") > 0 && %damage > 0.3 && %player.getInventory("RepairKit") <= 0)
            || (%itemName $= "RepairPatch" && %damage > 0.3)
            || (%itemName $= "RepairKit" && %damage > 0.3 && %player.getInventory("RepairKit") <= 0) )
         {  

            //HERE IS A PROBLEM RIGHT HERE! We say dist greater than 0 but above we ask for less than 1.5 - Lagg...
            //if (%dist > 0 && %dist < %closestHealthDist)
            if (%dist > 1.5 && %dist < %closestHealthDist)
	    {
	       %closestHealth = %item;
	       %closestHealthDist = %dist;    

	       //check for LOS
	       %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::ForceFiledObjectType;
	       %closestHealthLOS = !containerRayCast(%client.player.getWorldBoxCenter(), %item.getWorldBoxCenter(),
                 %mask, 0);
	    }
         }

         else
	 {
	    //only pick up stuff within 35m - changed 45m Lagg...
	    //if (%dist > 1 && %dist < 35 || %desired)// WAT THE HELL DOES %desired MEAN ??? - Lagg...
            if (%dist > 1.5 && %dist < 45)
	    {
               //aiCouldUseItem is located in aiInventory.cs file & it returns a true or false
	       if (AICouldUseItem(%client, %item))
	       {
	          if (%dist < %closestItemDist)
		  {
		     %closestItem = %item;
		     %closestItemDist = %dist;

		     //check for LOS
	             %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType;
		     %closestItemLOS = !containerRayCast(%client.player.getWorldBoxCenter(), %item.getWorldBoxCenter(),
                       %mask, 0);
                  }
               }
            }
         }
      }
   }

   //now set the weight if it is not hidden
   if (isObject(%closestHealth) && !%closestHealth.isHidden() && %closestHealth > 0)
   {
      //only choose an item if it's at least 25 m closer than health...
      //and we're not engageing someone or not that badly in need
      %currentTarget = %client.getEngageTarget();
      if (%closestItem > 0 && %closetItemDist < %closestHealthDist - 25 && %closestItemLOS && (%damage < 0.6 || %currentTarget <= 0))
      {

         // $AIWeightNeedItemBadly   = 4650;
         // $AIWeightNeedItem        = 4350;
         // $AIWeightFoundItem       = 2500;


         %task.pickupItem = %closestItem;
         if (AIEngageWeaponRating(%client) < 20)                
	    %task.setWeight($AIWeightNeedItemBadly);
	 else if (%closestItemDist < 25 && %closestItemLOS)//was 10 - Lagg...
	    %task.setWeight($AIWeightNeedItem);
         else if (%closestItemLOS)
            %task.setWeight($AIWeightFoundItem);
         else
	    %task.setWeight(0);
      }
      else
      {
         if (%damage > 0.8 || %watTask $= "AIOAttackObject")
	 {
	    %task.pickupItem = %closestHealth;
	    %task.setWeight($AIWeightNeedItemBadly);
	 }
	 else if (%closestHealthLOS)
	 {
	    %task.pickupItem = %closestHealth;
            %task.setWeight($AIWeightNeedItem);
	 }
	 else
	    %task.setWeight(0);
      }
   }
   else if (isObject(%closestItem) && !%closestItem.isHidden() && %closestItem > 0)
   {
      %task.pickupItem = %closestItem; 
      if (%player.getMountedImage($BackpackSlot) <= 0 && %task.pickupItem.getDataBlock().className $= "Pack"
        || AIEngageWeaponRating(%client) < 20)
      {
         //sendEcho(%client, "#1 weight picking up a PACK " @ getTaggedString(%client.name));
         %task.setWeight($AIWeightNeedItemBadly);
      }
      else if (%closestItemDist < 10 && %closestItemLOS)
      {
         //sendEcho(%client, "#2 weight just ITEM we have LOS - Pick It Up - " @ getTaggedString(%client.name));
	     %task.setWeight($AIWeightNeedItem);
      }
      else if (%closestItemLOS)
      {
         //sendEcho(%client, "#3 weight because we have LOS we should - " @ getTaggedString(%client.name));
	 %task.setWeight($AIWeightFoundItem);
      }
      else
	 %task.setWeight(0);
   }
   else
      %task.setWeight(0);
}

function AIPickupItemTask::monitor(%task, %client)
{
   %player = %client.player;
   
   //move to the pickup location
   if (isObject(%task.pickupItem) && %task.pickupItem > 0)
   {
      %dist = %client.getPathDistance(%task.pickupItem.getWorldBoxCenter());
      if (%dist < 1.5)
      {
	 %task.pickupItem = -1;
         //%task.setWeight(0);//never set weight to zero in the Monitor
         return;
      }
         
      if (%task.pickupItem.getDataBlock().className $= "Pack")
      {
         %dist = %client.getPathDistance(%task.pickupItem.position);
         if (%dist < 15 && %player.getMountedImage($BackpackSlot) > 0 &&
           %player.getInventory(%task.pickupItem.getDataBlock().getName()) <= 0)
         {
            //error("AIPickupItemTask::monitor - THROW PACK/ Pickup New Pack, " @ getTaggedString(%client.name));
            //echo("THROWING a - " @ %player.getMountedImage($BackpackSlot).item @ ", picking up a - " @
              //%task.pickupItem.getDataBlock().getName());
            //echo(" ");

	    %player.throwPack();
         }
      }

      //if the item to pick up is a weapon - Lagg... 5-29-2005
      if (%task.pickUpItem.getDataBlock().className $= "Weapon")
      {
         //error("AIPickupItemTask::monitor - weapon found - " @ %task.pickupItem.getDataBlock().getName());
         //error("by client - " @ getTaggedString(%client.name));
         //echo(" ");
 
         //check to see if we have to many weapons already
         if (aiCountWeapons(%client) == %player.getDataBlock().maxWeapons)
         {
            //echo("we are full of weapons - " @ aiCountWeapons(%client) @ ", " @ getTaggedString(%client.name));

            //we need to drop a weapon of lesser value than the weapon to pick up
            %dist = %client.getPathDistance(%task.pickupItem.position);
	    if (%dist < 15)
            {
               //loop through weapons carried and drop least valued first
	       for (%i = 0; %i < 9; %i++)
               {
                  if (%client.player.getInventory($InvWeapon[%i]) > 0 || %player.getMountedImage($WeaponSlot).getName() $=
                    "RepairGunImage")
                  {
                     //put it in hand
                     %player.use($InvWeapon[%i]);
                     if ($WeaponValue[%task.pickupItem.getDataBlock().getName()] >
                       $WeaponValue[%client.player.getMountedImage($WeaponSlot)])
                     {                 
                        //and throw it!                  
                        %player.throwWeapon();
                        break;
                     }                  
                  }
               }
            }
         }
      }

      %client.stepMove(%task.pickupItem.getWorldBoxCenter(), 0.25);

     //this call works in conjunction with AIEngageTask, keep shooting enemies!
     %client.setEngageTarget(%client.shouldEngage);
   }
   else
      %task.pickupItem = -1;
}

//-----------------------------------------------------------------------------
//AIUseInventoryTask will cause them to use an inv station if they're low
//on ammo.  This task should be used only for DM and Hunters - most objectives
//have their own logic for when to use an inv station...

function AIUseInventoryTask::init(%task, %client)
{
}

function AIUseInventoryTask::assume(%task, %client)
{
	%task.setWeightFreq(15);
	%task.setMonitorFreq(5);

	//mark the current time for the buy inventory state machine
	%task.buyInvTime = getSimTime();
}

function AIUseInventoryTask::retire(%task, %client)
{
	//reset the state machine time stamp...
	%task.buyInvTime = getSimTime();
}

function AIUseInventoryTask::weight(%task, %client)
{
   //first, see if we can pick up health
   %player = %client.player;
   if (!isObject(%player))
      return;

   %damage = %player.getDamagePercent();
   %weaponry = AIEngageWeaponRating(%client);

   //if there's an inv station, and we haven't used an inv station since we
   //spawned, the bot should use an inv once regardless
   if (%client.spawnUseInv)
   {
      //see if we're already heading there
      if (%client.buyInvTime != %task.buyInvTime)
      {
	      //see if there's an inventory we can use
         %result = AIFindClosestInventory(%client, false);
         %closestInv = getWord(%result, 0);
	      if (isObject(%closestInv))
         {
            %task.setWeight($AIWeightNeedItem);
            return;
         }
         else
            %client.spawnUseInv = false;
      }
      else
      {
         %task.setWeight($AIWeightNeedItem);
         return;
      }
   }

	//first, see if we need equipment or health
	if (%damage < 0.3 && %weaponry >= 40)
	{
		%task.setWeight(0);
		return;
	}

	//don't use inv stations if we're not that badly damaged, and we're in the middle of a fight
	if (%damage < 0.6 && %client.getEngageTarget() > 0 && !AIEngageOutOfAmmo(%client))
	{
		%task.setWeight(0);
		return;
	}

	//if we're already buying, continue
	if (%task.buyInvTime == %client.buyInvTime)
	{
		//set the weight - if our damage is above 0.8 or our we're out of ammo
		if (%damage > 0.8 || AIEngageOutOfAmmo(%client))
			%task.setWeight($AIWeightNeedItemBadly);
		else
			%task.setWeight($AIWeightNeedItem);
		return;
	}

	//we need to search for an inv station near us...
	%result = AIFindClosestInventory(%client, false);
	%closestInv = getWord(%result, 0);
	%closestDist = getWord(%result, 1);

	//only use inv stations if we're right near them...  patrolTask will get us nearer if required
	if (%closestDist > 35)
	{
		%task.setWeight(0);
		return;
	}

	//set the weight...
	%task.closestInv = %closestInv;
	if (%damage > 0.8 || AIEngageOutOfAmmo(%client))
		%task.setWeight($AIWeightNeedItemBadly);
	else if (%closestDist < 20 && (AIEngageWeaponRating(%client) <= 30 || %damage > 0.4))
		%task.setWeight($AIWeightNeedItem);
	else if (%hasLOS)
		%task.setWeight($AIWeightFoundItem);
	else
		%task.setWeight(0);
}

function AIUseInventoryTask::monitor(%task, %client)
{
	//make sure we still need equipment
	%player = %client.player;
	if (!isObject(%player))
		return;

	%damage = %player.getDamagePercent();
	%weaponry = AIEngageWeaponRating(%client);
	if (%damage < 0.3 && %weaponry >= 40 && !%client.spawnUseInv)
	{
		%task.buyInvTime = getSimTime();
		return;
	}

	//pick a random set based on armor...
	%randNum = getRandom();
	if (%randNum < 0.4)
		%buySet = "LightEnergyDefault MediumEnergySet HeavyEnergySet";
	else if (%randNum < 0.6)
		%buySet = "LightShieldSet MediumShieldSet HeavyShieldSet"; 
	else if (%randNum < 0.8)
		%buySet = "LightEnergyELF MediumRepairSet HeavyAmmoSet";
   else
      %buySet = "LightEnergySniper MediumEnergySet HeavyEnergySet";

	//process the inv buying state machine
   %result = AIBuyInventory(%client, "", %buySet, %task.buyInvTime);

   //if we succeeded, reset the spawn flag
   if (%result $= "Finished")
      %client.spawnUseInv = false;

   //if we succeeded or failed, reset the state machine...
	if (%result !$= "InProgress")
		%task.buyInvTime = getSimTime();


	//this call works in conjunction with AIEngageTask
	%client.setEngageTarget(%client.shouldEngage);
}

//-------------------------------------------------------------------------------------------------
//modified slightly -                                                            Lagg... - 4-4-2003
//AITauntCorpseTask is should happen only after an enemy is freshly killed

function AITauntCorpseTask::init(%task, %client)
{
}

function AITauntCorpseTask::assume(%task, %client)
{
	%task.setWeightFreq(11);
	%task.setMonitorFreq(11);
}

function AITauntCorpseTask::retire(%task, %client)
{
}

function AITauntCorpseTask::weight(%task, %client)
{
   %task.corpse = %client.getVictimCorpse();
   if (%task.corpse > 0 && getSimTime() - %client.getVictimTime() < 7500)
	{
		//see if we're already taunting, and if it's time to stop
		if ((%task.tauntTime > %client.getVictimTime()) && (getSimTime() - %task.tauntTime > 1000))
			%task.corpse = -1;
		else
		{
			//if the corpse is within 25m, taunt
         %distToCorpse = %client.getPathDistance(%task.corpse.getWorldBoxCenter());
			if (%dist < 0 || %distToCorpse > 25)
				%task.corpse = -1;
		}
	}
	else
		%task.corpse = -1;

	//set the weight
	if (%task.corpse > 0)
	{
		//don't taunt someone if there's an enemy right near by...
		%result = AIFindClosestEnemy(%client, 40, 15000);
      %closestEnemy = getWord(%result, 0);
      %closestdist = getWord(%result, 1);
      if (%closestEnemy > 0)
			%task.setWeight(0);
		else
			%task.setWeight($AIWeightTauntVictim);
	}
	else
		%task.setWeight(0);
}

$AITauntChat[0] = "gbl.aww";
$AITauntChat[1] = "gbl.brag";
$AITauntChat[2] = "gbl.obnoxious";
$AITauntChat[3] = "gbl.sarcasm";
$AITauntChat[4] = "gbl.when";
function AITauntCorpseTask::monitor(%task, %client)
{
	//make sure we still have a corpse, and are not fighting anyone
	if (%client.getEngageTarget() <= 0 && %task.corpse > 0 && isObject(%task.corpse))
	{
      %clientPos = %client.player.getWorldBoxCenter();
		%corpsePos = %task.corpse.getWorldBoxCenter();
		%distToCorpse = VectorDist(%clientPos, %corpsePos);
		if (%distToCorpse < 2.0)
		{
			//start the taunt!
			if (%task.tauntTime < %client.getVictimTime())
			{
				%task.tauntTime = getSimTime();
				%client.stop();

				if (getRandom() > 0.2)
				{
					//pick the sound and taunt cels
					%sound = $AITauntChat[mFloor(getRandom() * 3.99)];
					%minCel = 2;
					%maxCel = 8;
				   schedule(250, %client, "AIPlayAnimSound", %client, %corpsePos, %sound, %minCel, %maxCel, 0);
				}
				//say 'bye'  :)
				else
				   schedule(250, %client, "AIPlayAnimSound", %client, %corpsePos, "gbl.bye", 2, 2, 0);
			}
		}
		else
			%client.stepMove(%task.corpse.getWorldBoxCenter(), 1.75);
	}
}

//-----------------------------------------------------------------------------
//AIPatrolTask used to wander around the map (DM and Hunters mainly) looking for something to do...

function AIPatrolTask::init(%task, %client)
{
}

function AIPatrolTask::assume(%task, %client)
{
	%task.setWeightFreq(13);
	%task.setMonitorFreq(13);
	%task.findLocation = true;
	%task.patrolLocation = "0 0 0";
	%task.idleing = false;
	%task.idleEndTime = 0;
}

function AIPatrolTask::retire(%task, %client)
{
}

function AIPatrolTask::weight(%task, %client)
{
   ////added a check here to allow task to be used in Objective based gameTypes(CTF, Siege, etc...) - Lagg... 6-29-2005 - Maybe not a good idea :)
   //if (%client.objectiveTask > 0 && %client.objectiveTask.getName() $= "aiDefendLocation")
      //%task.setWeight($AIWeightPatrolling + 1400);
   //else

      %task.setWeight($AIWeightPatrolling);
}

function AIPatrolTask::monitor(%task, %client)
{
	//this call works in conjunction with AIEngageTask
	%client.setEngageTarget(%client.shouldEngage);

	//see if we're close enough to our patrol point
	if (%task.idleing)
	{
		if (getSimTime() > %task.idleEndTime)
		{
			%task.findLocation = true;
			%task.idleing = false;
		}
	}

	//see if we need to find a place to go...
	else if (%task.findLocation)
	{
		//first, see if we're in need of either health, or ammo
		//note: normally, I'd be tempted to put this kind of "looking for health" code
		//into the AIPickupItemTask, however, that task will be used in CTF, where you
		//don't want people on AIDefendLocation to leave their post to hunt for health, etc...
		//AIPickupItemTask only deals with items within a 30m radius around the bot.
		//AIPatrolTask will move the bot to the vicinity of an item, then AIPickUpItemTask
		//will finish the job...
		%foundItemLocation = false;
		%damage = %client.player.getDamagePercent();
		if (%damage > 0.7)
		{
			//search for a health kit
			%closestHealth = AIFindSafeItem(%client, "Health");
			if (%closestHealth > 0)
			{
				%task.patrolLocation = %closestHealth.getWorldBoxCenter();
				%foundItemLocation = true;
			}
		}
		else if (AIEngageOutOfAmmo(%client))
		{
			//search for a Ammo or a weapon...
			%closestItem = AIFindSafeItem(%client, "Ammo");
			if (%closestItem > 0)
			{
				%task.patrolLocation = %closestItem.getWorldBoxCenter();
				%foundItemLocation = true;
			}
		}

		//now see if we don't really have good equipment...
		if (!%foundItemLocation && AIEngageWeaponRating(%client) < 20)
		{
			//search for any useful item
			%closestItem = AIFindSafeItem(%client, "Any");
			if (%closestItem > 0)
			{
				%task.patrolLocation = %closestItem.getWorldBoxCenter();
				%foundItemLocation = true;
			}
		}
		//choose a randomish location only if we're not in need of health or ammo
		if (!%foundItemLocation)
		{
			//find a random item/inventory in the map, and pick a spawn point near it...
			%pickGraphNode = false;
			%chooseSet = 0;
			if ($AIInvStationSet.getCount() > 0)
				%chooseSet = $AIInvStationSet;
			else if ($AIWeaponSet.getCount() > 0)
				%chooseSet = $AIWeaponSet;
			else if ($AIItemSet.getCount() > 0)
				%chooseSet = $AIItemSet;

			if (!%chooseSet)
				%pickGraphNode = true;

			//here we pick whether we choose a random map point, or a point based on an item...
                        //if (getRandom() < 0.3) - ah - Lagg...
			else if (getRandom() < 0.3)
				%pickGraphNode = true;

			//here we decide whether we should choose a player location...  a bit of a cheat but
			//it's scaled by the bot skill level
			%pickPlayerLocation = false;
			%skill = %client.getSkillLevel();
			if (%skill < 1.0)
				%skill = %skill / 2.0;
			if (getRandom() < (%skill * %skill) && ClientGroup.getCount() > 1)
			{
				//find a random client
				%count = ClientGroup.getCount();
				%index = (getRandom() * (%count - 0.1));
				%cl = ClientGroup.getObject(%index);
				if (%cl != %client && AIClientIsAlive(%cl))
				{
					%task.patrolLocation = %cl.player.getWorldBoxCenter();
					%pickGraphNode = false;
					%pickPlayerLocation = true;
				}
			}
			
			if (!%pickGraphNode && !%pickPlayerLocation)
			{
				%itemCount = %chooseSet.getCount();
				%item = %chooseSet.getObject(getRandom() * (%itemCount - 0.1));
		      %nodeIndex = navGraph.randNode(%item.getWorldBoxCenter(), 10, true, true);
				if (%nodeIndex <= 0)
					%pickGraphNode = true;
				else
					%task.patrolLocation = navGraph.randNodeLoc(%nodeIndex);
			}

			//see if we failed above or have to pick just a random spot on the graph - use the spawn points...
			if (%pickGraphNode)
			{
			   %task.patrolLocation = Game.pickPlayerSpawn(%client, true);
				if (%task.patrolLocation == -1)
				{
					%client.stepIdle(%client.player.getWorldBoxCenter());
					return;
				}
			}
		}

		//now that we have a new location - move towards it
		%task.findLocation = false;
		%client.stepMove(%task.patrolLocation, 8.0);
	}

	//else we're on patrol - see if we're close to our destination
	else
	{
		%client.stepMove(%task.patrolLocation, 8.0);
		%distToDest = %client.getPathDistance(%task.patrolLocation);
		if (%distToDest > 0 && %distToDest < 10)
		{
			%task.idleing = true;
			%task.idleEndTime = 4000 + getSimTime() + (getRandom() * 6000);
			%client.stepIdle(%client.player.getWorldBoxCenter());
		}
	}
}

//-----------------------------------------------------------------------------
//AIEngageTurretTask is responsible for returning turret fire...

function AIEngageTurretTask::init(%task, %client)
{
}

function AIEngageTurretTask::assume(%task, %client)
{
   %task.setWeightFreq(4);
   %task.setMonitorFreq(4);
}

function AIEngageTurretTask::retire(%task, %client)
{
   %task.engageTurret = -1;
   //%client.engageTurret = -1;//this may be an error - Lagg... 6-15-2004 *
   %client.setEngagetarget(-1);
}

function AIEngageTurretTask::weight(%task, %client)
{
   //see if we're still fighting a turret
   %elapsedTime = getSimTime() - %task.startAttackTime;

   //if (isObject(%task.engageTurret) && %task.engageTurret.getDataBlock().getClassName() $= "TurretData")
   if (%client.lastDamageTurret > 0 && isObject(%task.engageTurret) && %task.engageTurret.getDataBlock().getClassName() $= "TurretData")
   {
      if (%task.engageTurret == %client.lastdamageTurret)
      {
         if (%task.engageTurret.isEnabled() && getSimTime() - %client.lastDamageTurretTime < 5000)
            %task.setWeight($AIWeightReturnTurretFire);
         else
            %task.setWeight($AIWeightDestroyTurret);
      }
      else if (AIClientIsAlive(%client, %elapsedTime))
      {
         //if another turret is shooting us, disable this one first...
         if (isObject(%client.lastDamageTurret) && %client.lastDamageTurret.getDataBlock().getClassName() $= "TurretData")
         {
            if (%task.engageTurret.isEnabled())
               %task.setWeight($AIWeightReturnTurretFire);
            else
            {
               //see if we need to switch to the new turret
               if (%client.lastDamageTurret.isEnabled() && %client.lastDamageTurretTime < 5000)
               {
                  %task.engageTurret = %client.lastDamageTurret;
		  %task.attackInitted = false;
		  %task.setWeight($AIWeightDestroyTurret);
               }
               else
		  %task.setWeight($AIWeightReturnTurretFire);
            }
         }
         else
         {
            if (%task.engageTurret.isEnabled() && getSimTime() - %client.lastDamageTurretTime < 5000)
               %task.setWeight($AIWeightReturnTurretFire);
	    else
	       %task.setWeight($AIWeightDestroyTurret);
         }
      }

      //else we died since - clear out the vars
      else
      {
         %task.engageTurret = -1;
         %task.setWeight(0);
      }
   }

   //else see if we have a new target - modified so only enemy turrets will be destroyed - Lagg... 11-4-2003
   else if (isObject(%client.lastDamageTurret) && %client.lastDamageTurret.getDataBlock().getClassName() $= "TurretData" &&
     %client.lastDamageTurret.team != %client.team)
   {
      %task.engageTurret = %client.lastDamageTurret;
      %task.attackInitted = false;

      if (%client.lastDamageTurret.isEnabled() && %client.lastDamageTurretTime < 5000)
      {
         if (%client.player.getArmorSize() $= "Heavy")
            %task.setWeight($AIWeightReturnTurretFire);
      }
      else
	 %task.setWeight($AIWeightDestroyTurret);
   }

   //else no turret to attack...  (later, do a query to find turrets before they attack - done in aiDetectRemeq task below - Lagg...)
   else
   {
      %task.engageTurret = -1;
      %task.setWeight(0);
   }

}

function AIEngageTurretTask::monitor(%task, %client)
{
	if (isObject(%task.engageTurret) && %task.engageTurret.getDataBlock().getClassName() $= "TurretData")
	{
		//set the AI to fire at the turret
		%client.setEngageTarget(-1);

                //check for LOS
		%mask = $TypeMasks::TerrainObjectType |
                              $TypeMasks::InteriorObjectType |
                              $TypeMasks::StaticTSObjectType |
                              $TypeMasks::ForceFieldObjectType;
		%turLOS = !containerRayCast(%client.player.getMuzzlePoint($WeaponSlot), %task.engageTurret.getWorldBoxCenter(), %mask, 0);
                if (%turLOS)
                   %client.setTargetObject(%task.engageTurret);
                else
                   %client.setTargetObject(-1);

		%clientPos = %client.player.getWorldBoxCenter();
		%turretPos = %task.engageTurret.getWorldBoxCenter();

		//control the movement - first, hide, then wait, then attack
		if (!%task.attackInitted)
		{
			%task.attackInitted = true;
			%task.startAttackTime = getSimTime();
			%task.hideLocation = %client.getHideLocation(%turretPos, 40.0, %clientPos, 4.0); 
			%client.stepMove(%task.hideLocation, 2.0);
		}
		else if (getSimTime() - %task.startAttackTime > 5000)
                {
			%client.stepMove(%task.engageTurret.getWorldBoxCenter(), 8.0);
                        if (vectorDist(%client.player.getWorldBoxCenter(), %task.engageTurret.getWorldBoxCenter()) < 5)
                           %client.setDangerLocation(%task.engageTurret.getWorldBoxCenter(), 20);
                }
	}
}

//-----------------------------------------------------------------------------
//AIAvoidMineTask is responsible for detecting/destroying enemy mines...
//modified so bots only detect mines they can see, if detected also acknowledge
//the danger.                                                           - Lagg... 11-12-2003

function AIDetectMineTask::init(%task, %client)
{
}

function AIDetectMineTask::assume(%task, %client)
{
	%task.setWeightFreq(7);
	%task.setMonitorFreq(7);
}

function AIDetectMineTask::retire(%task, %client)
{
   %task.engageMine = -1;
   %task.attackInitted = false;
   %client.setTargetObject(-1);
}

function AIDetectMineTask::weight(%task, %client)
{
   //crappy hack, but they need the proper weapon before they can destroy a mine...
   %player = %client.player;
   if (!isObject(%player))
      return;

   //lets cover all the reasons to disquailify client from this task...
   %hasPlasma = (%player.getInventory("Plasma") > 0) && (%player.getInventory("PlasmaAmmo") > 0);
   %hasDisc = (%player.getInventory("Disc") > 0) && (%player.getInventory("DiscAmmo") > 0);

   if ((!%hasPlasma && !%hasDisc) || !aiClientIsAlive(%client) || %player.isMounted())
   {
      %task.engageMine = -1;
      %task.setWeight(0);
      return;
   }

   //if we're already attacking a mine, make sure we can see it.
   if (%task.engageMine > 0 && isObject(%task.engageMine))
   {
      //check for LOS
      %mask = $TypeMasks::TerrainObjectType |
        $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
      %mineLOS = !containerRayCast(%player.getMuzzlePoint($WeaponSlot), %task.engageMine.getWorldBoxCenter(), %mask, 0);

      if (%mineLOS)
      {
         %task.setWeight($AIWeightDetectMine);
         return;
      }
   }

   //see if we're within the viscinity of a new (enemy) mine
   %task.engageMine = -1;
   %closestMine = -1;
   %closestDist = 15;	//initialize so only mines within 15 m will be detected...
   %mineCount = $AIDeployedMineSet.getCount();
   for (%i = 0; %i < %mineCount; %i++)
   {
      %mine = $AIDeployedMineSet.getObject(%i);
      %mineTeam = %mine.sourceObject.team;

      %minePos = %mine.getWorldBoxCenter();
      %clPos = %client.player.getWorldBoxCenter();

      //see if the mine is the closest...
      %mineDist = VectorDist(%minePos, %clPos);
      if (%mineDist < %closestDist)
      {
         //now see if we're more or less heading towards the mine...
	 %clVelocity = %client.player.getVelocity();
	 %clVelocity = getWord(%clVelocity, 0) SPC getWord(%clVelocity, 1) SPC "0";
	 %mineVector = VectorSub(%minePos, %clPos);
	 %mineVector = getWord(%mineVector, 0) SPC getWord(%mineVector, 1) SPC "0";
	 if (VectorLen(%clVelocity) > 2.0 && VectorLen(%mineVector > 2.0))
	 {
	    %clNormal = VectorNormalize(%clVelocity);
	    %mineNormal = VectorNormalize(%mineVector);
	    if (VectorDot(%clNormal, %mineNormal) > 0.3)
	    {
	       %closestMine = %mine;
	       %closestDist = %mineDist;
	    }
	 }
      }
   }

   //check for LOS
   if (isObject(%closestMine))
   {
      %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
      %mineLOS = !containerRayCast(%player.getMuzzlePoint($WeaponSlot), %closestMine.getWorldBoxCenter(), %mask, 0);
   }

   //see if we found a mine to attack
   if (%closestMine > 0 && %mineLOS)
   {
      %task.engageMine = %closestMine;
      %task.attackInitted = false;
      %task.setWeight($AIWeightDetectMine);
   }
   else
      %task.setWeight(0);
}

function AIDetectMineTask::monitor(%task, %client)
{
	if (%task.engageMine > 0 && isObject(%task.engageMine))
	{
		if (!%task.attackInitted)
		{
		   %task.attackInitted = true;
		   %client.stepRangeObject(%task.engageMine, "DefaultRepairBeam", 6, 12);
		   %client.setEngageTarget(-1);
		   %client.setTargetObject(-1);
		}

      else if (%client.getStepStatus() $= "Finished")
		{
                   %client.setTargetObject(%task.engageMine);
                   if (!%client.inDanger)
                      %client.setDangerLocation(%task.engageMine.position, 15);
		}
	}
}

//------------------------------------------------------------------------------------
//New AIDefault Task                       ------                    Lagg... 3-20-2003
//AIDetectRemeqTask is responsible for finding and killing enemy remote equipment...
//------------------------------------------------------------------------------------

function AIDetectRemeqTask::init(%task, %client)
{

}
function AIDetectRemeqTask::assume(%task, %client)
{
   %task.setWeightFreq(11);//was 7
   %task.setMonitorFreq(11);//was 7
}

function AIDetectRemeqTask::retire(%task, %client)
{
   %task.engageRemeq = -1;
   %client.setTargetObject(-1);
   %client.engageRemeq = -1;
}

function AIDetectRemeqTask::weight(%task, %client)
{
   %player = %client.player;
   if (!isObject(%player))
      return;

   if (%player.isMounted())
      return;
   //do we have the ammo
   %outOfStuff = (AIAttackOutofAmmo(%client));
   if (!aiClientIsAlive(%client) || %outOfStuff)
   {
      %task.engageRemeq = -1;
      %client.engageRemeq = -1;
      %task.setWeight(0);
      return;
   }


   //see if we're already attacking a remote inventory
   if (%task.engageRemeq > 0 && isObject(%task.engageRemeq) && %client.engageRemeq > 0)
   {
      %task.setWeight($AIWeightDetectRemeq);
      return;
   }
   //else lets find some remote equipment (deployables)...
   else
   {
      %task.engageRemeq = -1;
      %closestRemeq = -1;
      %closestDist = 100;	//initialize so only remote invetoties within 100 m will be detected
      %depCount = 0;
      %depGroup = nameToID("MissionCleanup/Deployables");
      if (isObject(%depGroup))
      {	      
         %depCount = %depGroup.getCount();
	 for (%i = 0; %i < %depCount; %i++)
	 {
	    %obj = %depGroup.getObject(%i);
	    //if (%obj.getDataBlock().getName() $= "DeployedStationInventory" && %obj.team != %client.team && %obj.isEnabled())
            if (%obj.team != %client.team && %obj.isEnabled())
            {
               %remeqPos = %obj.getWorldBoxCenter();
	       %clPos = %client.player.getWorldBoxCenter();
               %remeqDist = VectorDist(%remeqPos, %clPos);
	       if (%remeqDist < %closestDist)
               {
                  //check for LOS
		  %mask = $TypeMasks::TerrainObjectType |
                    $TypeMasks::InteriorObjectType |
                    $TypeMasks::StaticTSObjectType |
                    $TypeMasks::ForceFieldObjectType;
		  %remeqLOS = !containerRayCast(%player.getMuzzlePoint($WeaponSlot), %obj.getWorldBoxCenter(), %mask, 0);
                  if (%remeqLOS)
                  {
                     %task.attackInitted = false;
                     %task.engageRemeq = %obj;
                     %client.engageRemeq = %obj;

                     //if we are jammers/cloakers we are smarter
                     if (%client.player.getInventory("CloakingPack") > 0 || %client.player.getInventory("SensorJammerPack") > 0)
                        %task.setWeight($AIWeightDestroyTurret);
                     else if (isObject(%client.objectiveTask) && (%client.objectiveTask.getName() $= "AiTouchObject" ||
                       %client.objectiveTask.description $= "Attack Flag Location for Capper"))
                        %task.setWeight($AIWeightDestroyTurret);
                     else
                        %task.setWeight($AIWeightDetectRemeq);
                  }
               }
            }
         }
      }
   }
}

function AIDetectRemeqTask::monitor(%task, %client)
{
   if (%task.engageRemeq > 0 && isObject(%task.engageRemeq) && AIClientIsAlive(%client) && %client.engageRemeq > 0)
   {
      %client.setEngageTarget(-1);
      %hasMortar = (%client.player.getInventory("Mortar") > 0) && (%client.player.getInventory("MortarAmmo") > 0 && !%client.inWater);

      //shoot first, ask questions later - Lagg...
      if (%hasMortar)
      {
         %client.setTargetObject(%task.engageRemeq, 300, "Mortar");
         %client.stepRangeObject(%task.engageRemeq, "BasicTargeter", 10, 80);
      }
      else
      {
         %client.setTargetObject(%task.engageRemeq, 100, "Destroy");
         %client.stepRangeObject(%task.engageRemeq, "DefaultRepairBeam", 12, 15);
      }  
   }
   //else we are done
   else
   {
      %task.engageRemeq = -1;
      %client.engageRemeq = -1;
      %client.setTargetObject(-1);
      %task.setWeight(0);
   }              
}

//------------------------------------------------------------------------------------
//AICouldUseInventoryTask will cause them to use an inv station if they're low
//on ammo or health.  This is a new task for CTF, Siege, DnD, CnH game types.                 - Lagg... 11-12-2003
//------------------------------------------------------------------------------------

function AICouldUseInventoryTask::init(%task, %client)
{
}

function AICouldUseInventoryTask::assume(%task, %client)
{
	%task.setWeightFreq(35);
	%task.setMonitorFreq(35);

	//mark the current time for the buy inventory state machine
	%task.buyInvTime = getSimTime();
}

function AICouldUseInventoryTask::retire(%task, %client)
{
	//reset the state machine time stamp...
	%task.buyInvTime = getSimTime();
}

function AICouldUseInventoryTask::weight(%task, %client)
{
   //first, see if we are all there
   %player = %client.player;
   if (!isObject(%player))
      return;

   %damage = %player.getDamagePercent();
   %weaponry = AIEngageWeaponRating(%client);


   //first, see if we need equipment or health
	if (%damage < 0.3 && %weaponry >= 40)
	{
		%task.setWeight(0);
		return;
	}

	//don't use inv stations if we're not that badly damaged, and we're in the middle of a fight
	if (%damage < 0.6 && %client.getEngageTarget() > 0 && !AIEngageOutOfAmmo(%client))
	{
		%task.setWeight(0);
		return;
	}

	//if we're already buying, continue
	if (%task.buyInvTime == %client.buyInvTime)
	{
		//set the weight - if our damage is above 0.8 or our we're out of ammo
		if (%damage > 0.8 || AIEngageOutOfAmmo(%client))
			%task.setWeight($AIWeightNeedItemBadly);
		else
			%task.setWeight($AIWeightNeedItem);
		return;
	}

	//we need to search for an inv station near us...
	%result = AIFindClosestInventory(%client, false);
	%closestInv = getWord(%result, 0);
	%closestDist = getWord(%result, 1);

	//only use inv stations if we're right near them...
	if (%closestDist > 150)
	{
		%task.setWeight(0);
                return;
	}

        if (!%closestInv.inBase && %client.inBase)
        {
		%task.setWeight(0);
                return;
	}        

        ////Seiege Game - only use inv that are inBase if we are
        //if (Game.class $= "SiegeGame")
        //{
           //if  (%client.team == Game.offenseTeam && !%closestInv.inBase && %client.inBase)
           //{
		//%task.setWeight(0);
                ////error("AICouldUseInventoryTask::weight - Inventory is outside but we are inBase - " @ getTaggedString(%client.name));
                //return;
           //}
	//}

        //error("AICouldUseInventoryTask::weight - %weaponry = " @ %weaponry @ ", " @ getTaggedString(%client.name));

        //check for LOS - Lagg...
        %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType;
	%closestInvLOS = !containerRayCast(%player.getWorldBoxCenter(), %closestInv.getWorldBoxCenter(), %mask, 0);

	//set the weight...
	%task.closestInv = %closestInv;
	if (%damage > 0.8 || AIEngageOutOfAmmo(%client))
		%task.setWeight($AIWeightNeedItemBadly);
	else if (%closestDist < 100 && (AIEngageWeaponRating(%client) <= 30 || %damage > 0.4))
		%task.setWeight($AIWeightNeedItem);
	else if (%closestInvLOS)
		%task.setWeight($AIWeightFoundItem);
	else
		%task.setWeight(0);
}

function AICouldUseInventoryTask::monitor(%task, %client)
{
	//make sure we still need equipment
	%player = %client.player;
	if (!isObject(%player))
		return;

        //if we are picking up an item or health forget it - Lagg...
        //if (%client.pickUpItem > 0)
                //return;

	%damage = %player.getDamagePercent();
	%weaponry = AIEngageWeaponRating(%client);
	if (%damage < 0.3 && %weaponry >= 40 && !%client.spawnUseInv)
	{
		%task.buyInvTime = getSimTime();
		return;
	}

        //if we got an inventory station make sure we keep it
        if (%player.getInventory("InventoryDeployable"))
           %buySet = "HeavyInventorySet";

        else
        {      
           //pick a random set based on armor...
	   %randNum = getRandom();
	   if (%randNum < 0.4) 
		%buySet = "LightEnergyDefault MediumEnergySet HeavyEnergySet";
	   else if (%randNum < 0.6)
		%buySet = "LightShieldSet MediumShieldSet HeavyShieldOff"; 
	   else if (%randNum < 0.8)
		%buySet = "LightCloakSet MediumMissileSet HeavyAmmoSet";
           else
                %buySet = "LightEnergySniper MediumEnergySet HeavyEnergySet";
        }

	//process the inv buying state machine
   %result = AIBuyInventory(%client, "", %buySet, %task.buyInvTime);
   if (%result $= "InProgress")
      return;

   //if we succeeded or failed, reset the state machine...
   if (%result !$= "InProgress")
      %task.buyInvTime = getSimTime();

        
   //this call works in conjunction with AIEngageTask
   %client.setEngageTarget(%client.shouldEngage);
}

//------------------------------------------------------------------------------------
//New AIDefault Task                       ------                    Lagg... 11-6-2003
//AIDetectVehicleTask is for detecting / destroying enemy vehicles manned or unmanned
//------------------------------------------------------------------------------------

function AIDetectVehicleTask::init(%task, %client)
{

}

function AIDetectVehicleTask::assume(%task, %client)
{
            %task.engageVehicle = -1;
            %client.engageVehicle = -1;
	    %task.setWeightFreq(17);//was 7
	    %task.setMonitorFreq(17);//was 7
}

function AIDetectVehicleTask::retire(%task, %client)
{
	    %task.engageVehicle = -1;
            %client.engageVehicle = -1;
	    %client.setTargetObject(-1);        
}

function AIDetectVehicleTask::weight(%task, %client)
{
   %player = %client.player;
   if (!isObject(%player))
      return;

   //don't do this at lower skill levels
   %skill = %client.getSkillLevel();
   if (%skill <= 0.7)
   {
      %task.setWeight(0);
      return;
   }

   //check for ammo
   %out = AIAttackOutofAmmo(%client);   
   %hasMortar = (%player.getInventory("Mortar") > 0) && (%player.getInventory("MortarAmmo") > 0 && !%client.inWater);
   %hasMissile = (%player.getInventory("MissileLauncher") > 0) && (%player.getInventory("MissileLauncherAmmo") > 0 && !%client.inWater);
   if (!%hasMissile && %out)
   {
      %task.engageVehicle = -1;
      %client.engageVehicle = -1;
      %client.setTargetObject(-1);
      %task.setWeight(0);
      return;
   }

   //if we're already attacking a Vehicle set weight, let monitor have it 
   if (%task.engageVehicle > 0 && isObject(%task.engageVehicle) && %client.engageVehicle > 0)
   {
      //some vehicles are a greater threat than others
      %type = %task.engageVehicle.getDataBlock().getName();
      if (%type $= "MobileBaseVehicle" && %task.engageVehicle.fullyDeployed)// could use ".deployed" here ***
      {
         if (%hasMissile || %hasMortar)
            %task.setWeight($AIWeightDetectVehicle);
         else
            %task.setWeight($AIWeightFoundEnemy + 200);
      }
      else
      {
         if (%hasMissile || %hasMortar)
            %task.setWeight($AIWeightDetectRemeq);
         else
            %task.setWeight($AIWeightFoundEnemy);// + 150);
      }

      return;
   }

   //see if we're within the viscinity of a new (enemy) vehicle
   %task.engageVehicle = -1;
   %client.engageVehicle = -1;

   %search = aiFindClosestEnemyVehicle(%client);//--new function in ai_Vehicle.cs
   %closestVehicle  = getWord(%search, 0);
   %closestDist = getWord(%search, 1);

   //if we found a vehicle to attack
   if (%closestVehicle > 0 && %closestDist < 300)
   {
      //check if a respawning vehicle
      if (%closestVehicle.marker > 0 && VectorDist(%closestVehicle.position, %closestVehicle.marker.position) < 5)
         return;

      //check for LOS
      %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticTSShapeObjectType | $TypeMasks::ForceFieldObjectType;
      %vehicleLOS = !containerRayCast(%player.getMuzzlePoint($WeaponSlot), %closestVehicle.getWorldBoxCenter(), %mask, 0);

      if (%vehicleLOS)
      {
         %task.engageVehicle = %closestVehicle;
         %client.engageVehicle = %closestVehicle;
      }
      else
         %task.setWeight(0);
   }
   else
      %task.setWeight(0);
}

function AIDetectVehicleTask::monitor(%task, %client)
{
   %player = %client.player;
   if (!isObject(%player) || !isObject(%task.engageVehicle))
      return;

   if (%task.engageVehicle > 0 && isObject(%task.engageVehicle) && %client.engageVehicle > 0)
   {
      //check the distance
      %vehicleDist = vectorDist(%client.player.getWorldBoxCenter(), %task.engageVehicle.getWorldBoxCenter());

      if (%vehicleDist > 300)
      {
         %task.engageVehicle = -1;
         %client.engageVehicle = -1;
         %client.setTargetObject(-1);
         %task.setWeight(0);
         return;
      }
      else if (%vehicleDist < 15.0)
         %client.setDangerLocation(%task.engageVehicle.getWorldBoxCenter(), 30);

      //check for ammo   
      %hasMortar = (%player.getInventory("Mortar") > 0) && (%player.getInventory("MortarAmmo") > 0 && !%client.inWater);
      %hasMissile = (%player.getInventory("MissileLauncher") > 0) && (%player.getInventory("MissileLauncherAmmo") > 0 && !%client.inWater);

      if (%vehicleDist > 40 && %hasMissile)
      {
         %client.stepRangeObject(%task.engageVehicle, "BasicTargeter", 80, 250);
         %client.setTargetObject(%task.engageVehicle, 300, "Missile");            
      }
      else if (%hasMortar)
      {
         %client.stepRangeObject(%task.engageVehicle, "BasicTargeter", 80, 250);
         %client.setTargetObject(%task.engageVehicle, 300, "Mortar");
      }
      else
      {
         %client.stepMove(%task.engageVehicle.getWorldBoxCenter(), 20.0);
         %client.setTargetObject(%task.engageVehicle, 150, "Destroy");
      }
   }
   else
   {
      %task.engageVehicle = -1;
      %client.engageVehicle = -1;
      %client.setTargetObject(-1);
      %task.setWeight(0);
   }
}

//-----------------------------------------------------------------------------------------------------------------------
//                                                                                                     AISpamLocationTask
//Heh, heh, heh :)                                                                                   - Lagg... 11-15-2003
//
// this little diddy requires two simgroups, one named "StandLocations" and the other "SpamLocations"
//-----------------------------------------------------------------------------------------------------------------------
function AISpamLocationTask::init(%task, %client)
{
   %task.standLocation = -1;
   %task.locationToSpam = -1;
   %task.targetObject = -1;
}

function AISpamLocationTask::assume(%task, %client)
{
	%task.setWeightFreq(20);
	%task.setMonitorFreq(20);
}

function AISpamLocationTask::retire(%task, %client)
{
}

function AISpamLocationTask::weight(%task, %client)
{
   %player = %client.player;
   if (!isObject(%player))
		return;

   if (!isObject(nameToId(StandLocations)) || !isObject(nameToId(SpamLocations)))
   {
      %task.setWeight(0);
      return;
   }

   //sorry only seige offense team gets this advantage
   if (Game.class $= "SiegeGame")
   {
      if (%client.team != Game.offenseTeam)
      {
         %task.setWeight(0);
         return;
      }
   }

   //don't do this at lower skill levels or newbies leave the game :(
   %skill = %client.getSkillLevel();
   if (%skill <= 0.7)
   {
      %task.setWeight(0);
      return;
   }

   //do we have the spam?
   %hasMortar = (%player.getInventory("Mortar") > 0 && %player.getInventory("MortarAmmo") >= 1);
   //%hasGrenade = (%player.getInventory("GrenadeLauncher") > 0 && %player.getInventory("GrenadeLauncherAmmo") >= 1);
   if (!%hasMortar)
   {
      %task.setWeight(0);
      return;
   }

   //if we are being harrassed by any deployed turrets, take care of them first
   if (isObject(%client.lastDamageTurret) && %client.lastDamageTurret.getDataBlock().getClassName() $= "TurretData" && %client.lastDamageTurret.team != %client.team)
   {
      %task.setWeight(0);
      return;
   }

   //if we are using the repairpack at the moment, forget it for now. More to come for this Lagg ... :)
   if (isObject(%player.getMountedImage($WeaponSlot)) && %player.getMountedImage($WeaponSlot).getName() $= "RepairGunImage" && %player.getImageState($BackpackSlot) $= "activate" && %player.getDamagePercent() > 0)
   {
      %task.setWeight(0);
      return;
   }

   //if we already ran the tests and passed them, set the weight and forget it :)
   if (isObject(%task.standLocation) && %task.standLocation != -1)
   {
      //a quick look see if object still enabled and we are close
      if (%task.targetObject.getDamageState() !$= "Destroyed")
      {
         %dis = vectorDist(%task.standLocation, %player.getWorldBoxCenter());
         if (%dis < 15)
         {

            error("AISpamLocationTask::weight - LOCKED-ON to a " @ %task.targetObject.getDataBlock().getname());
            error("AISpamLocationTask::weight - Set Weight and Forget it! " @ getTaggedString(%client.name));
                      
            //set the weight and let monitor handle it
            %task.setWeight($AIHuntersWeightMustCap);
            return;
         }
      }
   }

   //else if we have not found a suitable location  yet      
   %task.standLocation = -1;
   %task.locationToSpam = -1;
   %task.targetObject = -1;

   //get our position 
   %clientPos = %player.getWorldBoxCenter();

   //since there can be any number of these locations on a map, we have to check our distance from each one

   //get the simgroup, an see if we are close to any of fire positions placed in the simgroup titled "standLocations"
   %group = nameToId(StandLocations);
   %count = %group.getCount();
   for (%i = 0; %i < %count; %i ++)
   {
      //grab a fire position
      %standMarker = %group.getObject(%i);

      //distance check required we have to be close
      %dist = vectorDist(%clientPos, %standMarker.position);
      if (%dist < 15)
      {
        

         //each position to stand has a target associated with it by dynamic field
         %target = nameToId(%standMarker.targetObject);

         error("AISpamLocationTask::weight - WE ARE WITHIN 15 M of LOCATION !!! - " @ getTaggedString(%client.name) @ " Target = " @ %target);

         
         if (%target.getDamageState() !$= "Destroyed")
         {

            %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType | $TypeMasks::ForceFieldObjectType | $TypeMasks::VehicleObjectType;
            %hasLOS = !containerRayCast(%client.player.getMuzzlePoint($WeaponSlot), %standMarker.position, %mask, 0);
            error("AISpamLocationTask::weight - WE HAVE TARGET - NOT DESTROYED !!! - " @ getTaggedString(%client.name));
            
            if (%hasLOS)
            {
               //we have stand location and the target, set the weight
               %task.standLocation = %standMarker;
               %task.targetObject = nameToId(%target);
               break;
            }
         }
      }
   }
   
   //ok if we have something, LOCK-ON to it
   if (%task.standLocation > 0 && %task.targetObject > 0)
      %task.setWeight($AIHuntersWeightMustCap);
   else
      %task.setWeight(0);
}

function AISpamLocationTask::monitor(%task, %client)
{
   //make sure we are all there
   %player = %client.player;
   if (!isObject(%player))
      return;

   //error(" AISpamLocationTask::monitor - CHECK # 1 " @ getTaggedString(%client.name));


   //we made it to the task monitor. Lets find the location to spam
   %group = nameToId(spamLocations);
   %count = %group.getCount();
   for (%i = 0; %i < %count; %i ++)
   {
      //grab an aim location
      %locMarker = %group.getObject(%i);
      %locObject = nameToId(%locMarker.targetObject);

      echo(" ");
      echo(" TARGET OBJECT WEIGHT LOCKED ON = " @ %task.targetObject);
      echo(" FIRE LOCATION TARGET OBJECT is = " @ %locMarker.targetObject);
      echo(" ");

      //check if location marker is set to same target object as the task.standLocation is
      if (%locObject == %task.targetObject)
      {
         if (%task.standLocation.set == %locMarker.set)
         {
            //we found it, then lets go
            %task.locationToSpam = %locMarker.position;

            error(" AISpamLocationTask::monitor - TARGET OBJECT FOUND & LOCKED IN !!! " @ getTaggedString(%client.name));
            break;
         }
      }
   }

   //ok start moving to the exact spot to shoot from
   %client.stepMove(%task.standLocation.position, 0.25);

   //but before we get there we can shoot if we are pretty close and we can see the richocet point.
   //this gives the impression of a personality with intelligence

   %dist = vectorDist(%player.getWorldBoxCenter(), %task.standLocation.position);
   error(" AISpamLocationTask::monitor - Dist to Stand Location = " @ %dist);

   %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType | $TypeMasks::ForceFieldObjectType | $TypeMasks::VehicleObjectType;
   %hasLOS = !containerRayCast(%client.player.getMuzzlePoint($WeaponSlot), %locMarker.position, %mask, 0);

   if (%hasLOS && %dist < 10)
   {
      //we are getting close, now sense the shot by looking at it
      %client.setEngageTarget(-1);      
      %client.aimAt(%task.locationToSpam, 3000);
      %player.use("Mortar");

      //real close, let fly
      if (%dist < 4)
         %client.pressFire(1);      
   }
}

//-----------------------------------------------------------------------------------------------------------------------
//                                                                                                     AISpamLocationTask
//I created this to task to mortar spam people who like to mortar spam my bots :)                       - Lagg... 11-15-2003
//-----------------------------------------------------------------------------------------------------------------------
function AIMortarSpamLocationTask::init(%task, %client)
{
}

function AIMortarSpamLocationTask::assume(%task, %client)
{
	%task.setWeightFreq(6);
	%task.setMonitorFreq(9);
}

function AIMortarSpamLocationTask::retire(%task, %client)
{
}

function AIMortarSpamLocationTask::weight(%task, %client)
{
   %player = %client.player;
	if (!isObject(%player))
		return;

   //don't do this at lower skill levels
   %skill = %client.getSkillLevel();
   if (%skill <= 0.7)
   {
      %task.setWeight(0);
      return;
   }

   //do we have the spam?
   %hasMortar = (%player.getInventory("Mortar") > 0) && (%player.getInventory("MortarAmmo") > 0);
   if (!%hasMortar)
   {
      %task.setWeight(0);
      return;
   }

   %clientPos = %player.getWorldBoxCenter();
   %task.spamlocation = nameToId(spamLocation).position;
   %task.locationToSpam = nameToId(locationToSpam).position;

   //only do this if we are in the vicinity to spam
   if (vectorDist(%clientPos, %task.spamLocation) > 45)
   {
      %task.setWeight(0);
      return;
   }

   //only do this if we got an enemy close to location (cheat ignore LOS)
   %result = AIFindClosestEnemyToLoc(%client, %task.locationToSpam, 17, $AIClientLOSTimeout, true, 70);
   %closestEnemy = getWord(%result, 0);

   if (%closestEnemy <= 0)
   {
      %task.setWeight(0);
      return;
   }

   //error("weight - client = " @ getTaggedString(%client.name));//debug stuffs - Lagg...
   %task.setWeight($AIWeightReturnFire + 100);
}

function AIMortarSpamLocationTask::monitor(%task, %client)
{
   //make sure we are all there
   %player = %client.player;
   if (!isObject(%player))
      return;

   //check for LOS
   %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType |
     $TypeMasks::ForceFieldObjectType | $TypeMasks::VehicleObjectType;
   %hasLOS = !containerRayCast(%player.getWorldBoxCenter(), %task.locationToSpam, %mask, 0);

   if (%hasLOS)
   {
      //%client.stop();
      %client.aimAt(%task.locationToSpam, 3000);
      %player.use("Mortar");
      %client.pressFire(1);
   }
   else
      %task.setWeight(0);
}

//-----------------------------------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//AISiegePatrolTask used to wander around the map looking for something to do...

function AISiegePatrolTask::init(%task, %client)
{
}

function AISiegePatrolTask::assume(%task, %client)
{
	%task.setWeightFreq(43);
	%task.setMonitorFreq(43);
	%task.findLocation = true;
	%task.patrolLocation = "0 0 0";
	%task.idleing = false;
	%task.idleEndTime = 0;
}

function AISiegePatrolTask::retire(%task, %client)
{
}

function AISiegePatrolTask::weight(%task, %client)
{
   
   if (%client.objectiveTask <= 0)      
      %task.setWeight($AIWeightPatrolling);
   else
      %task.setWeight(0);
      
}

function AISiegePatrolTask::monitor(%task, %client)
{
	//this call works in conjunction with AIEngageTask
	%client.setEngageTarget(%client.shouldEngage);

	//see if we're close enough to our patrol point
	if (%task.idleing)
	{
		if (getSimTime() > %task.idleEndTime)
		{
			%task.findLocation = true;
			%task.idleing = false;
		}
	}

	//see if we need to find a place to go...
	else if (%task.findLocation)
	{
		//first, see if we're in need of either health, or ammo
		//note: normally, I'd be tempted to put this kind of "looking for health" code
		//into the AIPickupItemTask, however, that task will be used in CTF, where you
		//don't want people on AIDefendLocation to leave their post to hunt for health, etc...
		//AIPickupItemTask only deals with items within a 30m radius around the bot.
		//AIPatrolTask will move the bot to the vicinity of an item, then AIPickUpItemTask
		//will finish the job...
		%foundItemLocation = false;
		%damage = %client.player.getDamagePercent();
		if (%damage > 0.7)
		{

                        error("AISiegePatrolTask::monitor - we are very damaged - " @ getTaggedString(%client.name));
			//search for a health kit
			%closestHealth = AIFindSafeItem(%client, "Health");
			if (%closestHealth > 0)
			{
				%task.patrolLocation = %closestHealth.getWorldBoxCenter();
				%foundItemLocation = true;
                                error("AISiegePatrolTask::monitor - found a health item, move to it" @ getTaggedString(%client.name));
			}
		}
		else if (AIEngageOutOfAmmo(%client))
		{
			//search for a Ammo or a weapon...
			%closestItem = AIFindSafeItem(%client, "Ammo");
			if (%closestItem > 0)
			{
				%task.patrolLocation = %closestItem.getWorldBoxCenter();
				%foundItemLocation = true;
			}
		}

		//now see if we don't really have good equipment...
		if (!%foundItemLocation && AIEngageWeaponRating(%client) < 20)
		{
			//search for any useful item
			%closestItem = AIFindSafeItem(%client, "Any");
			if (%closestItem > 0)
			{
				%task.patrolLocation = %closestItem.getWorldBoxCenter();
				%foundItemLocation = true;
			}
		}
		//choose a randomish location only if we're not in need of health or ammo
		if (!%foundItemLocation)
		{
			//find a random item/inventory in the map, and pick a spawn point near it...
			%pickGraphNode = false;
			%chooseSet = 0;
			if ($AIInvStationSet.getCount() > 0)
				%chooseSet = $AIInvStationSet;
			else if ($AIWeaponSet.getCount() > 0)
				%chooseSet = $AIWeaponSet;
			else if ($AIItemSet.getCount() > 0)
				%chooseSet = $AIItemSet;

			if (!%chooseSet)
				%pickGraphNode = true;

			//here we pick whether we choose a random map point, or a point based on an item...
                        //if (getRandom() < 0.3) - ah - Lagg...
			else if (getRandom() < 0.3)
				%pickGraphNode = true;

			//here we decide whether we should choose a player location...  a bit of a cheat but
			//it's scaled by the bot skill level
			%pickPlayerLocation = false;
			%skill = %client.getSkillLevel();
			if (%skill < 1.0)
				%skill = %skill / 2.0;
			if (getRandom() < (%skill * %skill) && ClientGroup.getCount() > 1)
			{
				//find a random client
				%count = ClientGroup.getCount();
				%index = (getRandom() * (%count - 0.1));
				%cl = ClientGroup.getObject(%index);
				if (%cl != %client && AIClientIsAlive(%cl))
				{
					%task.patrolLocation = %cl.player.getWorldBoxCenter();
					%pickGraphNode = false;
					%pickPlayerLocation = true;
				}
			}
			
			if (!%pickGraphNode && !%pickPlayerLocation)
			{
				%itemCount = %chooseSet.getCount();
				%item = %chooseSet.getObject(getRandom() * (%itemCount - 0.1));
		      %nodeIndex = navGraph.randNode(%item.getWorldBoxCenter(), 10, true, true);
				if (%nodeIndex <= 0)
					%pickGraphNode = true;
				else
					%task.patrolLocation = navGraph.randNodeLoc(%nodeIndex);
			}

			//see if we failed above or have to pick just a random spot on the graph - use the spawn points...
			if (%pickGraphNode)
			{
			   %task.patrolLocation = Game.pickPlayerSpawn(%client, true);
				if (%task.patrolLocation == -1)
				{
					//%client.stepIdle(%client.player.getWorldBoxCenter());
                                        centerPrintAll("<color:FFFFFF>" @ getTaggedString(%Client.name)
                                        @ "\n<color:BBBBFF> Committed Suicide Because he had nothing better to do.", 0, 3);
					error("AISiegePatrolTask::monitor - CANNOT DO ANYTHING USEFUL SO KILL OURSELF " @ getTaggedString(%client.name));

                                         if( isObject( %client.player ) )
	                                    %client.player.scriptKill($DamageType::Suicide);
                                        return;
				}
			}
		}

		//now that we have a new location - move towards it
		%task.findLocation = false;
		%client.stepMove(%task.patrolLocation, 8.0);
	}

	//else we're on patrol - see if we're close to our destination
	else
	{
		%client.stepMove(%task.patrolLocation, 8.0);
		%distToDest = %client.getPathDistance(%task.patrolLocation);
		if (%distToDest > 0 && %distToDest < 10)
		{
			%task.idleing = true;
			%task.idleEndTime = 4000 + getSimTime() + (getRandom() * 6000);
			%client.stepIdle(%client.player.getWorldBoxCenter());
		}
	}
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------
//                                                                                                     AIDebugTask
//AIDebugTask                                                                                   - Lagg... 11-15-2003
//-----------------------------------------------------------------------------------------------------------------------
function AIDebugTask::init(%task, %client)
{
}

function AIDebugTask::assume(%task, %client)
{
	%task.setWeightFreq(350);
	%task.setMonitorFreq(10);
}

function AIDebugTask::retire(%task, %client)
{
}

function AIDebugTask::weight(%task, %client)
{
   %player = %client.player;
	if (!isObject(%player))
		return;
   if (%client.team != Game.offenseTeam)
   {
      %task.setWeight(0);
      return;
   }

   //if (isObject(%client.objectiveTask) && !%client.inBase)// && %player.getArmorSize() $= "Medium")
      //{
         //%wat = %client.objectiveTask.getName();
         //error(getTaggedString(%client.name));
         //echo("               " @ %wat);
         //echo("   ");
         //echo("   ");
      //}

   if (!isObject(%client.objectiveTask))
      {
        // %wat = %client.objectiveTask.getName();
         error(getTaggedString(%client.name) @ " HAS NO OBJECTIVE");
         //echo("               " @ %wat);
         echo("   ");
         echo("   ");
      }
  
   %task.setWeight(0);
   return;
}

function AIDebugTask::monitor(%task, %client)
{
   //make sure we are all there
   %player = %client.player;
   if (!isObject(%player))
      return;

      if (isObject(%client.objectiveTask))
      {
         %wat = %client.objectiveTask.getName();
         error(getTaggedString(%client.name));
         echo("               " @ %wat);
         echo("   ");
         echo("   ");
      }
}




