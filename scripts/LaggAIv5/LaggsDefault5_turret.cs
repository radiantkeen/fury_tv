//let the turrets fire on all enemy targets 
function TurretData::selectTarget(%this, %turret)
{
   %turretTarg = %turret.getTarget();
   if(%turretTarg == -1)
      return;

   // if the turret isn't on a team, don't fire at anyone
   if(getTargetSensorGroup(%turretTarg) == 0)
   {
      %turret.clearTarget();
      return;
   }

   // stop firing if turret is disabled or if it needs power and isn't powered
   if((!%turret.isPowered()) && (!%turret.needsNoPower))
   {
      %turret.clearTarget();
      return;
   }

   %TargetSearchMask = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType |
                       $TypeMasks::TurretObjectType | $TypeMasks::SensorObjectType |
                       $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType;

   %attackRadius = %turret.getMountedImage(0).attackRadius;

   InitContainerRadiusSearch(%turret.getMuzzlePoint(0),
                             %attackRadius,
                             %TargetSearchMask);

   while ((%potentialTarget = ContainerSearchNext()) != 0)
   {
      %potTargTarg = %potentialTarget.getTarget();

      //a little modificaton here, don't shoot at team 0 stuff - Lagg... 5-19-2005
      //if (%turret.isValidTarget(%potentialTarget) && (getTargetSensorGroup(%turretTarg) != getTargetSensorGroup(%potTargTarg)))
      if (%turret.isValidTarget(%potentialTarget) && (getTargetSensorGroup(%turretTarg) != getTargetSensorGroup(%potTargTarg)) &&
        getTargetSensorGroup(%potTargTarg) != 0)
      {
         %turret.setTargetObject(%potentialTarget);
         return;
      }
   }
}



