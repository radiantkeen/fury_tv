//----------------------------------------------------------------------------------------------- AIO Tank Patrol ---
//Set up simgroups called "T" @ %team @ "TankPath" @ %num
//(example T2TankPath1 or T2TankPath7 / T1TankPath4 or T1TankPath9)
//NOTE: The AIObjective marker must be close to the vpad to use (some maps have multiple vpads per team).
//must state the path # to follow in the "paths" field of the objective marker seperated by spaces. The
//simgroups must contain markers or cameras placed in the order to follow for each path.
//Thats 1 simgroup per path - Lagg... 4-8-2004

function AIOTankPatrol::weight(%this, %client, %level, %minWeight, %inventoryStr)     
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   // For CnH games, we set a target object and set def or offense (can be used for any gameType)
   if (%this.targetObjectId > 0 && %this.clientLevel1 != %client)
   {
      if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden())
         return 0;

      //if offense and we own target object quit
      if (%this.offense > 0)
      {
         if (%this.targetObjectId.team == %client.team || %this.targetObjectId.getDamageState() $= "Destroyed")
         {
            error("AIOTankPatrol::weight - Target was destroyed so we are not taking this objective");//-------------------------- note here
            return 0;
         }
      }
      //if defense and we own it must be powered and not destroyed, or if enemy it must be destroyed or not powered
      else if (%this.defense > 0)
      {
         if (%this.targetObjectId.team == %client.team)
         {
            if (!%this.targetObjectId.isPowered() || %this.targetObjectId.getDamageState() $= "Destroyed")
               return 0;
         }
         else
         { 
            if (%this.targetObjectId.isPowered() && %this.targetObjectId.getDamageState() !$= "Destroyed")
              return 0;
         }
      }        
   }

   //check if flight path exists
   if (Game.class $= "SiegeGame")
      %team = Game.offenseTeam == %client.team ? 1 : 2;
   else
      %team = %client.team;

   if(! isObject(nameToId("T" @ %team @ "TankPath" @ firstWord(%this.paths))))
   {
      error("AIOTankPatrol Error! - No Paths field specified in AIOTankPatrol objective Marker");
      return 0;
   }

   //do a quick check to disqualify this objective if it can't meet the minimum weight
   if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
   {
      if (%this.targetObjectId > 0 && %this.issuedByClientId == %client.controlByHuman)
      {
         if ($AIWeightHumanIssuedCommand < %minWeight)
	    return 0;
      }
      else
         return 0;
   }

   //check for Vehicle station near buy of forget it if we are noy mounted yet - Lagg...
   //would like to add a check for unpiloted vehicles with passengers that need a ride (someday - DONE!) - Lagg... 5-4-2005
   if (!%client.player.isMounted() && !%client.justBought)
   {
      %blockName = AssaultVehicle;

      //check to see if we can steal a ride
      %abV = aiSearchForRide(%client, %blockName, %this.position, 250, %this.mode);

      if (%abv <= 0 && (%this.mode $= "NoBuy" || %this.mode $= "PassengerOnly"))
         return 0;

      if (%abV <= 0)
      {
         %clVs = AIFindClosestVStation(%client, %this.position);
         if (%clVs > 0)
         {
            %closestVs = getWord(%clVs, 0);
            %closestVsDist = getWord(%clVs, 1);
            if (%closestVsDist > 15)
            {
               //error("AIOTankPatrol Error! - Marker More Than 15Meters From A Team Vehicle Station");
               return 0;
            }
            if (VectorDist (%client.player.position, %this.position) > 300)//we are close to VPad or return 0 *
               return 0;
         }
         else
            return 0;

         //see if this vehicle is allowed at the VPad in question
         if (%closestVs.AssaultVehicle $= "Removed")
         {
            error("AIOTankPatrol Error! - Tank Not Allowed At This Vehicle Pad");
            return 0;
         }

         //check if any of vehicle type are availible
         if (!vehicleCheck(%blockName, %client.team))
            return 0;
      }
   }

   %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);

   //never bump a shrike pilot from his ride
   if (%this.clientLevel1 > 0  && %level == 1 && %this.clientLevel1 != %client)
      return 0;
   else if (%this.clientLevel1 == %client)
      %weight = 20000;

   return %weight;
}

function AIOTankPatrol::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AITankPatrol);
   %client.objectiveTask.initFromObjective(%this, %client);
   
   //create the escort objective (require a gunner in this case...)
   %client.escort = new AIObjective(AIOEscortPlayer)
                     {
						      dataBlock = "AIObjectiveMarker";
                        weightLevel1 = $AIWeightVehicleMountedEscort;
                        weightLevel2 = 0;
                        description = "Tank Support for " @ getTaggedString(%client.name);
                        targetClientId = %client;
                        offense = true;
                        chat = "ChatNeedHold";
                        desiredEquipment = "Plasma PlamsaAmmo";
								buyEquipmentSet = "MediumRepairSet";
                     };
   MissionCleanup.add(%client.escort);

   //if is siege game we have to do this right - Lagg... 11-3-2003
   if (Game.class $= "SiegeGame")
   {
      //the objectives in $ObjectiveQ[1] are all offense objectives, $ObjectiveQ[2] defense
      if (%client.team == game.offenseTeam)
         $ObjectiveQ[1].add(%client.escort);
      else
         $ObjectiveQ[2].add(%client.escort);
   }
   else
      $ObjectiveQ[%client.team].add(%client.escort);
}

function AIOTankPatrol::unassignClient(%this, %client)
{
   //kill the escort objective
   if (%client.escort)
   {
      clearObjectiveFromTable(%client.escort);//new function below to clear objective from $objTable[] - Lagg... 1-27-2004
      AIClearObjective(%client.escort);
      %client.escort.delete();
      %client.escort = "";
   }   
   if(%client.pilotVehicle)
      {
         AIDisembarkVehicle(%client);
      }  
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//----------------------------------------------------------------------------------------------- AI Tank Patrol ---

function AITankPatrol::initFromObjective(%task, %objective, %client)
{
   //initialize the task vars from the objective
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;

   //task location is the position of aiomarker
   %task.location = %objective.position;
   %task.mode = %objective.mode;

   %task.equipment = %objective.equipment;
   %task.buyEquipmentSet = %objective.buyEquipmentSet;
   %task.desiredEquipment = %objective.desiredEquipment;
   %task.issuedByClient = %objective.issuedByClientId;
   %task.chat = %objective.chat;

   //initialize other task vars
   %task.sendMsg = true;
   %task.sendMsgTime = 0;
   %task.paths = %objective.paths;

   %client.player.mVehicle = "";// needed if bot just hopped out a vehicle and it was not destroyed/removed yet
}

function AITankPatrol::assume(%task, %client)
{
   //set frequency slow (bots are safe in a tank)
   %task.setWeightFreq(40);//was 50
   %task.setMonitorFreq(40);//was 50
   
   %client.needEquipment = AINeedEquipment(%task.equipment, %client);

   if (%task.mode $= "PassengerOnly")
   {
      if (%client.player.getArmorSize() !$= "Heavy")
         %client.needEquipment = false;
   }
   //even if we don't *need* equipemnt, see if we should buy some... 
   else if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
   {
      //see if we could benefit from inventory
      %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      %closestDist = getWord(%result, 1);
      if (AINeedEquipment(%task.desiredEquipment, %client) && %closestInv > 0)
      {
         if (%closestDist < 100)
	    %client.needEquipment = true;
      }
   }

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();

   //reset the vehicle station wait time
   %client.vsWaitTime = "";

   //set the destination paths for each team and game type
   
   //for siege game type
   if (Game.class $= "SiegeGame")
      %team = Game.offenseTeam == %client.team ? 1 : 2;
   else
      %team = %client.team;

   //first see how many paths we have
   %mx = getWordCount(%task.paths);

   //and which path to use
   %random = mFloor(getRandom(1, %mx));
   %random--;
   %tg = getWord(%task.paths, %random);

   %task.group = nameToId("T" @ %team @ "TankPath" @ %tg);
   %task.count = %task.group.getCount();
   %task.locationIndex = 0;

   %client.needVehicle = true;
   %task.shouldWait = false;

   //set up the waitSpot for escort objectives
   %task.waitSpot = %task.location;
   for (%i = 0; %i < %task.count; %i ++)
   {
      if (%task.group.getObject(%i).waitTime !$= "")
      {
         %task.waitSpot = %task.group.getObject(%i).position;
         %task.shouldWait = true;
         break;
      }
   } 
}

function AITankPatrol::retire(%task, %client)
{
   if (aiClientIsAlive(%client))
   {
      %client.needVehicle = false;
      %client.clearStep();
      if(%client.player.isMounted())
         AIDisembarkVehicle(%client);
   }
}

function AITankPatrol::weight(%task, %client)
{
   //update the task weight...
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;

   //let the monitor decide when to quit :)//--- NEEDED SO IMPORTANT !
   %task.setWeight(%task.baseWeight);

}

function AITankPatrol::monitor(%task, %client)
{   
   //first, buy equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;
      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
         return;
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(50);
	 %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
         if (%client.player.getArmorSize() !$= "Heavy")
         {
            %task.setMonitorFreq(50);
	    %client.needEquipment = false;
         }
         else if (%task == %client.objectiveTask)
	 {
	    AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
            return;
	 }
      }
   }
   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   //chat - send the message
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
	 {
	    if (%task.chat !$= "")
	    {
	       %chatMsg = getWord(%task.chat, 0);
	       %chatTemplate = getWord(%task.chat, 1);
	       if (%chatTemplate !$= "")
		  AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
	       else
		  AIMessageThread(%task.chat, %client, -1);
	    }
         }
      }
   }

   if (%client.needVehicle)
   {
      //If we're in light or medium armor, buy the vehicle - Lagg...
      if (%client.player.getArmorSize() $= "Heavy")
      {
         if (%task == %client.objectiveTask)
         {
            %task.baseWeight = %client.objectiveWeight;
            %task.equipment = "Medium";
            %task.buyEquipmentSet = "MediumMissileSet";
            %client.needEquipment = true;
            return;
         }
      }

      %blockName = AssaultVehicle;

      //check to see if we can steal a ride
      %abV = aiSearchForRide(%client, %blockName, %task.location, 250, %task.mode);

      if (%abV <= 0 && %task.mode !$= "NoBuy" && %task.mode !$= "Passenger")
      {
         %clVs = AIFindClosestVStation(%client, %task.location);
         if (%clVs > 0)
         {
            %closestVs = getWord(%clVs, 0);
            %closestVsDist = getWord(%clVs, 1);
         }
         if (%closestVs > 0 && %closestVsDist < 250 && !isObject(%client.player.mVehicle))
         {
            //If we're in light or medium armor, buy the vehicle - Lagg...
            if (%client.player.getArmorSize() !$= "Heavy")
            {        
               %task.setMonitorFreq(9);
               %buyResult = aiBuyVehicle(%blockName, %client, %closestVs);
            }
            else
            {
               //if ai in heavy armor buy equipment
               if (%task == %client.objectiveTask)
	       {
                  %task.baseWeight = %client.objectiveWeight;
                  %task.equipment = "Medium";
	          %task.buyEquipmentSet = "MediumMissileSet";
                  %client.needEquipment = true;
                  return;
	       }
            }
           
            if (%buyResult $= "InProgress")
            {
               //reset pilot's wait time for passengers
               %task.shouldWaitTime = getSimTime();            
	       return;
            }

            else if (%buyResult $= "Finished")
            {
               //if we are finished buying the vehicle, then we are done
               //this is handled in function Armor::AIonMount - Lagg... 7-9-2004
            }
            else if (%buyResult $= "Failed")
            {
               //if this task is the objective task, choose a new objective
	       if (%task == %client.objectiveTask)
	       {
	          AIUnassignClient(%client);
	          Game.AIChooseGameObjective(%client);
                  return;
	       }
            }
         }
         else if ((%closestVs <= 0 || %closestVsDist >= 300) && !isObject(%client.player.mVehicle))
         {
            error("AITankPatrol::monitor - No V Pad or To Far unassigning");
            if (%task == %client.objectiveTask)
            {
               AIUnassignClient(%client);
	       Game.AIChooseGameObjective(%client);
               return;
            }
         }
      }
      else if (%abV > 0)
      {
         %client.pilotVehicle = true;//needed to let ai mount pilot seat

         //reset pilot's wait time for passengers
         %task.shouldWaitTime = getSimTime();

         //if we are close to vehicle throw packs that won't fit
         if (hasLargePack(%client.player))
            %client.player.throwPack();

         if (%client.inWater)
         {
            %vPos2D = getWord(%abV.getPosition(), 0) SPC getWord(%abV.getPosition(), 1) SPC "0";
            %myPos2D = getWord(%client.player.position, 0) SPC getWord(%client.player.position, 1) SPC "0";
            if (vectorDist(%myPos2D, %vPos2D) < 2.5)
            {
               %client.pressJump();
               %client.stepMove(%abV.getWorldBoxCenter(), 0.25, $AIModeGainHeight);
            }
            else
               %client.stepMove(%abV.position, 0.25, $AIModeExpress);
         }
         else 
            %client.stepMove(getWords(%abv.getTransform(), 0, 2), 0.25, $AIModeMountVehicle);

         return;
      }
      else
      {
         error("AITankPatrol::monitor - BIG FAILURE unassigning " @ getTaggedString(%client.name));

         //this should cause unassignment below
         %client.needVehicle = false;
      }      
   }

   //let set some variables

   //if we managed to get in vehicle then go
   if (%client.player.isMounted() && isObject(%client.player.mVehicle))
   {
      //set low frequency
      %task.setMonitorFreq(50);

      //set a time limit on this task in case he gets stuck somewhere
      %time = getSimTime() - %client.getTaskTime();
      if (%time > $aiNeedARideTaskTime)
      {

         error("AITankPatrol::monitor - $aiNeedARideTaskTime is up so un-assign"@ getTaggedString(%client.name));//--------- note here

         //set the index to the last
         %task.locationIndex = %task.count - 1;

         //eject, eject, eject
         AIDisembarkVehicle(%client);
      }
 
      //get the vehicle and velocity
      %vehicle = %Client.vehicleMounted;
      %mySpd = VectorLen(%vehicle.getVelocity());
      
      //make sure we got in the correct vehicle
      if (%vehicle.getDataBlock().getName() !$= "AssaultVehicle")
      {
         error("AITankPatrol::monitor - opps we got in wrong vehicle");//believe it or not but i have seen everything - Lagg...
         AIDisembarkVehicle(%client); //Hop off...
      }

      //set the destination, speed and pitch
      %location = %task.group.getObject(%task.locationIndex).position;
      %speed = %task.group.getObject(%task.locationIndex).speed;
      if (%speed $= "")
         %speed = 1.0;

      %pos2D = getWord(%client.vehicleMounted.position, 0) SPC getWord(%client.vehicleMounted.position, 1) SPC "0";
      %dest2D = getWord(%location, 0) SPC getWord( %location, 1) SPC "0";

      %client.setPilotPitchRange(-0.2, 0.05, 0.05);

      //---------------------------------------------------------------------------------------------------------------------- Avoidance ---
      if (%mySpd < 0.1)
      { 
         InitContainerRadiusSearch(%vehicle.getWorldBoxCenter(), 6.0, $TypeMasks::InteriorObjectType |
           $TypeMasks::VehicleObjectType |
           $TypeMasks::TSStaticShapeObjectType);
         %avoid = containerSearchNext();

         if (%avoid = %vehicle)
            %avoid = containerSearchNext();

         //if we are moving slow and close to objects we are probably stuck
         if (%avoid)
         {
            %vx = getWord(%vehicle.getWorldBoxCenter(), 0);
            %vy = getWord(%vehicle.getWorldBoxCenter(), 1);
            %vz = getWord(%vehicle.getWorldBoxCenter(), 2);
            %vz += 2.0;
            %vR = getWords(%vehicle.getTransform(), 3, 6);

            %client.setPilotDestination(%location, 1.0);//max speed
            %vehicle.setTransform(%vx SPC %vy SPC %vz SPC %vR);// --- set the Transform Here up a little

            error("AITANK - Avoidance we are stuck - " @ getTaggedString(%client.name));
         }
      }            

      //wait for gunner only if marker.waitTime has a value
      //if (%task.shouldWait)
      //this should make him follow path before a marker with a wait time is located :)
      if (%task.shouldWait && %task.group.getObject(%task.locationIndex).waitTime !$= "")
      {
         //reset pilot's wait time for passengers
         %task.shouldWaitTime = getSimTime();

         // - get the passenger seat: 0 = empty 1 = full
         for(%i = 0; %i < %vehicle.getDataBlock().numMountPoints; %i++)
         {
            %empty = false;
            if (!%vehicle.getMountNodeObject(%i))
            {
               %empty = true;
               break;
            }
         }

         //check if wait timer is up or bomber is full
         if (!%empty || getSimTime() - %task.group.getObject(%task.locationIndex).waitTime >
           %task.shouldWaitTime)
            %task.shouldWait = false;

         //else drive to wait spot
         else if (VectorDist(%myPos, %location) < 10)
         {
            if(%task.group.getObject(%task.locationIndex).waitTime $= "")
            {
               if ((%task.count - 1) > %task.locationIndex)//has to be -1 on the groups count
                  %task.locationIndex++;
               //we are at end of trail
               else
               {
                  if (%client.vehicleMounted )
                  {
                     AIDisembarkVehicle(%client); //Hop off...
                     %client.stepMove(%location, 0.25);
                     return;
                  }
               }
            }
            else
               %client.setPilotDestination(%location, 0); //stop and wait
         }
         else
	    %client.setPilotDestination(%location, 0.5); //move to wait spot, slow
      }
      //or just drive to the next marker
      else 
      {
         //first check turret potential targets
         %vehicle = %Client.vehicleMounted;
         %turret = %vehicle.getMountNodeObject(10);
         if(isObject(%turret))
            %target = %turret.getTargetObject();

         //driver see if anybody close to run down (cheat ignor LOS) :) - Lagg...
         %losTimeout = $AIClientMinLOSTime + ($AIClientLOSTimeout * %client.getSkillLevel());
         %result = AIFindClosestEnemyToLoc(%client, %client.player.getWorldBoxCenter(), 70, %losTimeout, true, false);
         %closestEnemy = getWord(%result, 0);
         %closestdist = getWord(%result, 1);

         //check for obstacles
         %vLoc = %vehicle.getWorldBoxCenter();
         InitContainerRadiusSearch(%vLoc, 30, $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType |
           $TypeMasks::VehicleObjectType);
         %obs = containerSearchNext();
         if (%obs = %vehicle)
            %obs = containerSearchNext();

         //little check to see if anybody around
         if (%target > 0)
            %dist = vectorDist(%vehicle.getWorldBoxCenter(), %target.getWorldBoxCenter());

         //if driver spotted a target
         if (%closestEnemy > 0 && %closestdist < 70 && !%obs)//run em dowm even if gunner has target
         {
            %client.setPilotDestination(%closestEnemy.position, 1.0);//run em down!
            %client.aimAt(%closestEnemy.position, 1000); 
         }
         else if (%closestEnemy > 0 && %target <= 0 && %closestdist < 200 && !%obs)//only move if gunner has no target
            %client.setPilotDestination(%closestEnemy.position, 0.5);//shoot em
            
         //if gunner has target - move to target
         else if (%target > 0 && %dist > 200 && !%obs)
            %client.setPilotDestination(%target.position, 0.5);//if gunner has target move slow
              
         //slow down so gunner can hit something
         else if (%target > 0 && %dist <= 200)//don't check for obstacles, just stop
            %client.setPilotDestination(%vehicle.position, 0); //this stops him

         //stop so gunner can chaingun target
         else if (%target > 0 && %dist < 32)//since we are stopped no check for %obs
            %client.setPilotDestination(%vehicle.position, 0);  //this stops him

         //if there is no enemy, continue on path
         else
         {
            //if a human is gunner lets be nice to him
            if (!%vehicle.getMountNodeObject(1).isAiControlled && %turret.fireTrigger > 0)
               %client.setPilotAim(%location);//this should stop him
            else
            {
               //are we close to location index marker?
	       if (VectorDist(%dest2D, %pos2D) < 25)//25 meters from marker
	       {
	          //if we have another location index
                  if ((%task.count - 1) > %task.locationIndex)//has to be -1 on the groups count
                     %task.locationIndex++;
                  //else we are at end of trail
	          else
                  {
                     if (%task == %client.objectiveTask)
	             {                  
                        if (%client.vehicleMounted )
                        {
                           AIDisembarkVehicle(%client); //Hop off...
                           %client.stepMove(%location, 0.25);
                           return;
                        }
	             }
                  }
               }

               //or just drive to marker location
               %client.setPilotDestination(%location, %speed);
            }
         }
      }
   }
   //else task must be completed
   else
   {
      AIUnassignClient(%client);
      Game.AIChooseGameObjective(%client);
      return;
   }
}
