//improved AI behavior when using flash grenades - Lagg...
function detonateFlashGrenade(%hg)
{
   %maxWhiteout = %hg.getDataBlock().maxWhiteout;
   %thrower = %hg.sourceObject.client;
   %hg.setDamageState(Destroyed);   
   %hgt = %hg.getTransform();
   %plX = firstword(%hgt);
   %plY = getWord(%hgt, 1);
   %plZ = getWord(%hgt, 2);
   %pos = %plX @ " " @ %plY @ " " @ %plZ;
   //all this stuff below ripped from projectiles.cs

   InitContainerRadiusSearch(%pos, 100.0, $TypeMasks::PlayerObjectType |
                                          $TypeMasks::TurretObjectType);

   while ((%damage = containerSearchNext()) != 0)
   {
      %dist = containerSearchCurrDist();

      %eyeXF = %damage.getEyeTransform();
      %epX   = firstword(%eyeXF);
      %epY   = getWord(%eyeXF, 1);
      %epZ   = getWord(%eyeXF, 2);
      %eyePos = %epX @ " " @ %epY @ " " @ %epZ;
      %eyeVec = %damage.getEyeVector();

      // Make sure we can see the thing...
      if (ContainerRayCast(%eyePos, %pos, $TypeMasks::TerrainObjectType |
                                          $TypeMasks::InteriorObjectType |
                                          $TypeMasks::StaticObjectType, %damage) !$= "0")
      {
         continue;
      }

      %distFactor = 1.0;
      if (%dist >= 100)
         %distFactor = 0.0;
      else if (%dist >= 20) {
         %distFactor = 1.0 - ((%dist - 20.0) / 80.0);
      }

      %dif = VectorNormalize(VectorSub(%pos, %eyePos));
      %dot = VectorDot(%eyeVec, %dif);

      %difAcos = mRadToDeg(mAcos(%dot));
      %dotFactor = 1.0;
      if (%difAcos > 60)
         %dotFactor = ((1.0 - ((%difAcos - 60.0) / 120.0)) * 0.2) + 0.3;
      else if (%difAcos > 45)
         %dotFactor = ((1.0 - ((%difAcos - 45.0) / 15.0)) * 0.5) + 0.5;

      %totalFactor = %dotFactor * %distFactor;
              
	  %prevWhiteOut = %damage.getWhiteOut();

		if(!%prevWhiteOut)
			if(!$teamDamage)
			{
				if(%damage.client != %thrower && %damage.client.team == %thrower.team)
					messageClient(%damage.client, 'teamWhiteOut', '\c1You were hit by %1\'s whiteout grenade.', getTaggedString(%thrower.name)); 
			}
		
      %whiteoutVal = %prevWhiteOut + %totalFactor;
      if(%whiteoutVal > %maxWhiteout)
      {
        //error("whitout at max");
        %whiteoutVal = %maxWhiteout;
      }

      //bot cheat! don't blind the thrower - Lagg... 1-8-2004 
      if (%damage.client == %thrower && %thrower.isAIControlled())
         continue;
     
      %damage.setWhiteOut( %whiteoutVal );
   }
   %hg.schedule( 500, "delete" );
}

//-----------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------- ai Teleporter support -----------
//-----------------------------------------------------------------------------------------------------------------------

function findLinkedTeleporter(%client, %clTpTarg)
{
   %closestTel = -1;
   %closestDist = 32767;
   %telCount = $AITeleportSet.getCount();
   for (%i = 0; %i < %telCount; %i++)
   {
      if (%telCount > 0)
      {
         %teleport = $AITeleportSet.getObject(%i);
         if (%teleport.team == %client.team)
         {
            //check if oneway teleporter
            if (!%teleport.oneway)
            {          
               //make sure the teleporter is not destroyed - ZOD
               if (!%teleport.isDisabled())
	       {
                  //make sure the teleporter is getting power - ZOD
                  if (%teleport.isPowered())
                  {
                     //check for the link
                     if (%teleport.trigger.targetBase == %clTpTarg.trigger)
                     {
                        %dist = %client.getPathDistance(%teleport.getTransform(), %client.player.position);
                        //%dist = VectorDist(%client.player.position, %teleport.getTransform());
                        if (%dist > 0 && %dist < %closestDist)
	                {
                           %closestTeh = %teleport;
		           %closestDist = %dist;
                        }
                     }
                  }
               }
            }
         }
      }
   }

   //this function returns the linked teleport to the closest teleport to target
   //and the distance form the client to the linked teleport
   return %closestTeh @ " " @ %closestDist;
}

function findclosestTeleporterToTarget(%client, %target)
{
   %closestTel = -1;
   %closestDist = 32767;
   %telCount = $AITeleportSet.getCount();

   for (%i = 0; %i < %telCount; %i++)
   {
      if (%telCount > 0)
      {
         %teleport = $AITeleportSet.getObject(%i);
         
               //make sure the teleporter is getting power
               if (%teleport.isPowered())
               {
                  %dist = %client.getPathDistance(%target.getWorldBoxCenter(), %teleport.getTransform());
                  //%dist = VectorDist(%target.getWorldBoxCenter(), %teleport.getTransform());
                  if (%dist > 0 && %dist < %closestDist)
	          {
                     %closestTeh = %teleport;
		     %closestDist = %dist;
                  }
               }
      }
   }

   //this function returns the closest teleport to target
   //and the distance from the closest teleport to target
   return %closestTeh @ " " @ %closestDist;
}

//--------------------------------------------------- assume telporter - start -
function aiTeleporterCheck(%client, %target)
{
   //first check for a teleporter close to target
   %result = findclosestTeleporterToTarget(%client, %target);
   %clTpTarg = getWord(%result, 0);
   %clTpTargDist = getWord(%result, 1);
      
   //next find the linked teleporter
   %result = findLinkedTeleporter(%client, %clTpTarg);
   %linkedTel = getWord(%result, 0);
   %linkedTelDist = getWord(%result, 1);
      
   //now check for what would be closer - Lagg... 10-1-2003
   if (%clTpTarg > 0 && %linkedTel > 0)
   {
      %disToTarg = %client.getPathDistance(%target.getTransform(), %client.player.position);
      %distToTpTarg = %client.getPathDistance(%clTpTarg.getTransform(), %client.player.position);

      ///error("aiTeleporterCheck - PATH DIS to Start Teleport              = " @ %linkedTelDist);
      ///error("aiTeleporterCheck - Destination Teleport distance to Target = " @ %clTpTargDist);
      ///echo("aiTeleporterCheck - Client's distance to target              = " @ %disToTarg);
         
      if ((%clTpTargDist + %linkedTelDist) < %disToTarg && %linkedTelDist < %disToTarg && %linkedTelDist < %distToTptarg)
      {
         //error("aiTeleporterCheck - USE TELEPORT = TRUE - " @ getTaggedString(%client.name));
         %useTeleport = true;
      }
      else
         %useTeleport = false;             
   }
   //--------------------------------------------------- assume telporter - end -
   return %useTeleport @ " " @ %linkedTel;
}
