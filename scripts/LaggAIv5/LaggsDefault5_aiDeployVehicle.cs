//new function by Founder (Martin Hoover)
//PosOne needs to be the location of the object, PosTwo is the position you want it to face.
//sub the two positions so we get a vector pointing from the origin in the direction we want our object to face
function pointToPos(%obj, %posTwo)
{      
   %px = getWord(%obj.getWorldBoxCenter(), 0);
   %py = getWord(%obj.getWorldBoxCenter(), 1);
   %pz = getWord(%obj.getWorldBoxCenter(), 2);
   %ps = %px SPC %py SPC %pz;

   %pz += 1;// up a little here - Lagg...

   %vec = VectorSub(%posTwo, %ps);

   // pull the values out of the vector
   %x = firstWord(%vec);
   %y = getWord(%vec, 1);
   %z = getWord(%vec, 2);

   //this finds the distance from origin to our point
   %len = vectorLen(%vec);

   //---------XY----------------- 
   //given the rise and length of our vector this will give us the angle in radians
   %rotAngleXY = mATan( %z, %len );

   //---------Z-----------------
   //get the angle for the z axis
   %rotAngleZ = mATan( %x, %y );

   //create 2 matrices, one for the z rotation, the other for the x rotation
   %matrix = MatrixCreateFromEuler("0 0" SPC %rotAngleZ * -1);
   %matrix2 = MatrixCreateFromEuler(%rotAngleXY SPC "0 0");

   //now multiply them together so we end up with the rotation we want
   %finalMat = MatrixMultiply(%matrix, %matrix2);

   //we&#180;re done, send the proper numbers back
   //return getWords(%finalMat, 3, 6);
   %rt = getWords(%finalMat, 3, 6);

   %obj.setTransform(%px SPC %py SPC %pz SPC %rt);//------------------------ set the Transform Here
}

//-------------------------------------------------------------------------------------------------------------------- AIO Deploy Vehicle

function AIODeployVehicle::weight(%this, %client, %level, %minWeight, %inventoryStr)
{
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   //sorry only one MPB per team for Bots.
   if (firstWord(AIFindDeployedMPB(%client)) > 0)
      return 0;


   // For CnH games, we set a target object and set def or offense (can be used for any gameType)
   if (%this.targetObjectId > 0 && %this.clientLevel1 != %client)
   {
      if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden())
         return 0;

      //if offense and we own target object quit
      if (%this.offense > 0)
      {
         if (%this.targetObjectId.team == %client.team || %this.targetObjectId.getDamageState() $= "Destroyed")
            return 0;
      }
      //if defense and we own it must be powered and not destroyed, or if enemy it must be destroyed or not powered
      else if (%this.defense > 0)
      {
         if (%this.targetObjectId.team == %client.team)
         {
            if (!%this.targetObjectId.isPowered() || %this.targetObjectId.getDamageState() $= "Destroyed")
               return 0;
         }
         else
         { 
            if (%this.targetObjectId.isPowered() && %this.targetObjectId.getDamageState() !$= "Destroyed")
              return 0;
         }
      }        
   }

    //check if flight path exists
   if (Game.class $= "SiegeGame")
      %team = Game.offenseTeam == %client.team ? 1 : 2;
   else
      %team = %client.team;
   
   if(! isObject(nameToId("T" @ %team @ "MPBPath" @ firstWord(%this.paths))))
   {
      error("AIODeployVehicle Error! - No Paths field exists for AIODeployVehicle objective Marker");
      return 0;
   }

   //do a quick check to disqualify this objective if it can't meet the minimum weight
   if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
   {
      if (%this.targetObjectId > 0 && %this.issuedByClientId == %client.controlByHuman)
      {
         if ($AIWeightHumanIssuedCommand < %minWeight)
	    return 0;
      }
      else
         return 0;
   }

   //check for Vehicle station near buy of forget it if we are not mounted yet or didn't just buy - Lagg...
   if (!%client.player.isMounted() && !%client.justBought)
   {
      %clVs = AIFindClosestVStation(%client, %this.position);
      if (%clVs > 0)
      {
         %closestVs = getWord(%clVs, 0);
         %closestVsDist = getWord(%clVs, 1);
         if (%closestVsDist > 15)
         {
            //error("AIODeployVehicle Error! - Marker More Than 15Meters From A Team Vehicle Station");
            return 0;
         }

         if (VectorDist (%client.player.position, %this.position) > 300)//---* close to VPad or return 0 *---
            return 0;
      }
      else
         return 0;

      //see if this vehicle is allowed at the VPad in question
      if (%closestVs.MobileBaseVehicle $= "Removed")
      {
         error("AIODeployVehicle Error! - MPB Not Allowed At This Vehicle Pad");
         return 0;
      }

      //check if any of vehicle type are availible
      %blockName = "MobileBaseVehicle";
      if (!vehicleCheck(%blockName, %client.team))
         return 0;
   }

   %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);

   //never bump a pilot from his task
   if (%this.clientLevel1 > 0  && %level == 1 && %this.clientLevel1 != %client)
      return 0;
   else if (%this.clientLevel1 == %client)
      %weight = 20000;

   return %weight;
}

function AIODeployVehicle::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIDeployVehicle);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIODeployVehicle::unassignClient(%this, %client)
{
   if(%client.pilotVehicle)
      AIDisembarkVehicle(%client);
  
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//-------------------------------------------------------------------------------------

function AIDeployVehicle::initFromObjective(%task, %objective, %client)
{
   //initialize the task vars from the objective
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;
   %task.location = %objective.location;

   %task.equipment = %objective.equipment;
   %task.buyEquipmentSet = %objective.buyEquipmentSet;
   %task.desiredEquipment = %objective.desiredEquipment;
   %task.issuedByClient = %objective.issuedByClientId;
   %task.chat = %objective.chat;

   //initialize other task vars
   %task.sendMsg = true;
   %task.sendMsgTime = 0;
   %task.paths = %objective.paths;

   %client.player.mVehicle = "";// needed if bot just hopped out a vehicle and it was not destroyed/removed yet
}

function AIDeployVehicle::assume(%task, %client)
{
   %task.setWeightFreq(35);
   %task.setMonitorFreq(35);
   
   %client.needEquipment = AINeedEquipment(%task.equipment, %client);

   //even if we don't *need* equipemnt, see if we should buy some... 
   if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
   {
      //see if we could benefit from inventory
      %needArmor = AIMustUseRegularInvStation(%task.desiredEquipment, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      %closestDist = getWord(%result, 1);
      if (AINeedEquipment(%task.desiredEquipment, %client) && %closestInv > 0)
      {
         %result = AIFindClosestEnemy(%client, 100, $AIClientLOSTimeout);
	 %closestEnemy = getWord(%result, 0);
	 %closestEnemydist = getWord(%result, 1);

	 if (%closestEnemy <= 0 || (%closestEnemyDist > %closestDist * 1.5))
	    %client.needEquipment = true;
      }
   }

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();

   //reset the vehicle station wait time
   %client.vsWaitTime = "";

   //clear the target tag
   %task.shouldAttack = -1;

   //set the destination paths for each team and game type
   
   //first see how many paths we have
   //check if flight path exists
   if (Game.class $= "SiegeGame")
      %team = Game.offenseTeam == %client.team ? 1 : 2;
   else
      %team = %client.team;
      
      
   //first see how many paths we have
   %mx = getWordCount(%task.paths);
   %random = mFloor(getRandom(1, %mx));
   %random--;
   %tg = getWord(%task.paths, %random);
   %task.group = nameToId("T" @ %team @ "MPBPath" @ %tg);

   // - set the path simgroup to follow
   %task.count = %task.group.getCount();  
   %task.locationIndex = 0;
   %client.needVehicle = true;
}

function AIDeployVehicle::retire(%task, %client)
{
   if (aiClientIsAlive(%client))
   {
      %client.needVehicle = false;
      %client.clearStep();
      if(%client.player.isMounted())
         AIDisembarkVehicle(%client);
   }
}

function AIDeployVehicle::weight(%task, %client)
{
   //update the task weight...
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;

   //let the monitor decide when to quit :)
   %task.setWeight(%task.baseWeight);
}

//------------------------------------------------------------------------- AIDeployVehicle::monitor ---
function AIDeployVehicle::monitor(%task, %client)
{
   //first, buy equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;
      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
         return;
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(35);
	 %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
         if (%client.player.getArmorSize() $= "Heavy")
         {
            if (%task == %client.objectiveTask)
	    {
	       AIUnassignClient(%client);
	       Game.AIChooseGameObjective(%client);
               return;
	    }
         }
         else
         {
            %task.setMonitorFreq(35);
            %client.needEquipment = false; 
         }   
      }
   }
   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   //chat - send the message
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
	 {
	    if (%task.chat !$= "")
	    {
	       %chatMsg = getWord(%task.chat, 0);
	       %chatTemplate = getWord(%task.chat, 1);
	       if (%chatTemplate !$= "")
		  AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
	       else
		  AIMessageThread(%task.chat, %client, -1);
	    }
         }
      }
   }

   if (%client.needVehicle)
   {
      %clVs = AIFindClosestVStation(%client, %task.location);
      %closestVs = getWord(%clVs, 0);
      %closestVsDist = getWord(%clVs, 1);
      if (%closestVs > 0 && %closestVsDist < 15 && !isObject(%client.player.mVehicle))
      {
         //If we're in light or medium armor, buy the vehicle - Lagg...
         if (%client.player.getArmorSize() !$= "Heavy")
         {        
            %task.setMonitorFreq(9);
            %buyResult = aiBuyVehicle(MobileBaseVehicle, %client, %closestVs);
            //error("BUY RESULT = BUY THE DAM THING !!!");
         }
         else
         {
            //if ai in heavy armor buy equipment
            if (%task == %client.objectiveTask)
	    {
               %task.baseWeight = %client.objectiveWeight;
               %task.equipment = "Medium";
	       %task.buyEquipmentSet = "MediumMissileSet";
               %client.needEquipment = true;
               return;
	    }
         }
           
         if (%buyResult $= "InProgress")
         {
            //clear offensive tags
            %client.lastDamageClient = -1;
            %client.lastDamageTurret = -1;
            %client.shouldEngage = -1;
            %client.setEngageTarget(-1);
            %client.setTargetObject(-1);
            %client.engageRemeq = -1;
            //%client.pickUpItem = -1;
	    return;
         }

         else if (%buyResult $= "Finished")
         {
            //if we are finished buying the vehicle, then we are done
            //this is handled in function Armor::AIonMount - Lagg... 7-9-2004
         }
         else if (%buyResult $= "Failed")
         {
            //if this task is the objective task, choose a new objective
	    if (%task == %client.objectiveTask)
	    {
	       AIUnassignClient(%client);
	       Game.AIChooseGameObjective(%client);
          return;
	    }
         }
      }
      else if ((%closestVs <= 0 || %closestVsDist > 15) && !isObject(%client.player.mVehicle))
      {
         if (%task == %client.objectiveTask)
         {
            AIUnassignClient(%client);
            Game.AIChooseGameObjective(%client);
            return;
         }
      }
   }

   //if we managed to get in vehicle then go
   if (%client.player.isMounted() && isObject(%client.player.mVehicle))
   {
      //update the frequency
      %task.setMonitorFreq(35);

      //set the vehicle data stuffs
      %vehicle = %Client.vehicleMounted;
      %mySpd = vectorLen(%vehicle.getVelocity());

      //set a safety cheat here
      // - set a time limit on this task
      %time = getSimTime() - %client.getTaskTime();
      if (%time > $aiMPBSelfDestructTime)
      {
         AIDisembarkVehicle(%client);
         %task.locationIndex = %task.count - 1;
         %vehicle.setDamageLevel(%vehicle.getDataBlock().destroyedLevel);// - Woo Hoo !         
         return;
      }


      //make sure we got in the correct vehicle
      if (%vehicle.getDataBlock().getName() !$= "MobileBaseVehicle")
      {
         error("AIDeployVehicle::monitor - opps we got in wrong vehicle");//believe it or not but i have seen everything - Lagg...

         AIDisembarkVehicle(%client);
         return;
      }
 
      //set the location and speed
      %location = %task.group.getObject(%task.locationIndex).position;
      %speed = %task.group.getObject(%task.locationIndex).speed;
      if (%speed $= "")
         %speed = 1.0;

      //are we close to location index marker?
      if (VectorDist(%location, %client.vehicleMounted.position) < 25)//25 meters from marker
      {
         //if we have another location index
         if ((%task.count - 1) > %task.locationIndex)//has to be -1 on the %task.group count
            %task.locationIndex++;
               
	 else
         {
            if (%task == %client.objectiveTask)
	    {                  
               if (%client.vehicleMounted )
               {
                  // if vehicle has stopped add a repair objective for it then hop out
                  if (isObject(%vehicle))// && %mySpd <= 0.5)//funny the vehicle never actually has a speed of zero - Lagg... 2-14-2004
		  {

                     AIDisembarkVehicle(%client);
                     %client.stepMove(%location, 0.25);

		     //see if there's a "repairObject" objective for the newly deployed thingy...
		     if (%task == %client.objectiveTask)
		     {

			//search the current objective group and search for a "repair Object" task...
			%objective = %client.objective;

			//delete any previously associated "AIORepairObject" objective
			if (isObject(%objective.repairObjective))
			{
			   AIClearObjective(%objective.repairObjective);
			   %objective.repairObjective.delete();
			   %objective.repairObjective = "";
                           clearObjectiveFromTable(%objective.repairObjective);// - Lagg... 1-27-2004
			}

			//add the repair objective
	                %objective.repairObjective = new AIObjective(AIORepairObject)
		                              {
											      dataBlock = "AIObjectiveMarker";
                                                 position = %vehicle.getWorldBoxCenter();
		                                 weightLevel1 = $AIWeightMortarTurret[1];
		                                 weightLevel2 = $AIWeightRepairGenerator[2];
                                                 location = %vehicle.getWorldBoxCenter();
		                                 description = "Repair the " @ %vehicle.getDataBlock().getName();
                                                 mode = "inField";
						 targetObjectId = %vehicle;
						 issuedByClientId = %client;
		                                 offense = false;
						 defense = true;
		                                 equipment = "RepairPack";
		                                 buyEquipmentSet = "MediumRepairSet";
		                              };
					%objective.repairObjective.group = %objective.group;
                                        MissionCleanup.add(%objective.repairObjective);

                        //if is siege game we have to do this right - Lagg... 11-3-2003
                        if (Game.class $= "SiegeGame")
                        {
                           //the objectives in $ObjectiveQ[1] are all offense objectives, $ObjectiveQ[2] has the defensive ones..
                           if (%client.team == game.offenseTeam)
                              $ObjectiveQ[1].add(%objective.repairObjective);
                           else
                              $ObjectiveQ[2].add(%objective.repairObjective);
                        }
                        else
                           $ObjectiveQ[%client.team].add(%objective.repairObjective);
                     }
                     return;
                  }
                  else
                     %client.setPilotDestination(%task.location, 0);//stop
               }
	    }
         }
      }
      else
      {
         %client.setPilotDestination(%location, %speed);//max speed

         if (%mySpd <= 0.1)
         {
            pointToPos(%vehicle, %location);//cheat here
            applyKick(%vehicle, "foward", 5);//only bounces wheeled vehicles, but helps
         }
      }
   }
   //else task must be completed
   else if (%task == %client.objectiveTask)
   {
      AIUnassignClient(%client);
      Game.AIChooseGameObjective(%client);
      return;
   }
}
