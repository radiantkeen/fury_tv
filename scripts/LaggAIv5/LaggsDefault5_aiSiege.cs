//
// LaggsDefault4_aiSiege.cs
//

function SiegeGame::onAIRespawn(%game, %client)
{
   //add the default tasks
	if (! %client.defaultTasksAdded)
	{
		%client.defaultTasksAdded = true;
	   %client.addTask(AIEngageTask);
	   %client.addTask(AIPickupItemTask);
	   %client.addTask(AITauntCorpseTask);
		%client.addTask(AIEngageTurretTask);
		%client.addtask(AIDetectMineTask);
           %client.addtask(AIDetectRemeqTask);//- Lagg... 3-20-2003
           %client.addtask(AIDetectVehicleTask);//- Lagg... 11-6-2003
           %client.addTask(AICouldUseInventoryTask);//- Lagg... 9-6-2003
           %client.addTask(AISpamLocationTask);//- Lagg... 11-16-2003
	}
}

function SiegeGame::AIInit(%game)
{
   //another mistake fixed here - Lagg...
   //for (%i = 0; %i <= %game.numTeams; %i++)
   for (%i = 1; %i <= %game.numTeams; %i++)
   {
      if (!isObject($ObjectiveQ[%i]))
      {
         $ObjectiveQ[%i] = new AIObjectiveQ();
         MissionCleanup.add($ObjectiveQ[%i]);
      }

      error("team " @ %i @ " objectives load...");
		$ObjectiveQ[%i].clear();
      AIInitObjectives(%i, %game);
   }
   
   //call the default AIInit() function
   AIInit();
}

//fix for spam error messages when team damage is enabled = Lagg... 3-23-2004
function SiegeGame::onAIDamaged(%game, %clVictim, %clAttacker, %damageType, %implement)
{
   if (%clAttacker && %clAttacker != %clVictim && %clAttacker.team == %clVictim.team)
   {
      //schedule(250, %clVictim, "AIPlayAnimSound", %clVictim, %clAttacker.player.getWorldBoxCenter(), "wrn.watchit", -1, -1, 0);
      AIMessageThread("ChatWarnShoot", %clVictim, %clAttacker);
      
      //clear the "lastDamageClient" tag so we don't turn on teammates...  unless it's uberbob!
      %clVictim.lastDamageClient = -1;
   }
}

function SiegeGame::onAIFriendlyFire(%game, %clVictim, %clAttacker, %damageType, %implement)
{
   if (%clAttacker && %clAttacker.team == %clVictim.team && %clAttacker != %clVictim && !%clVictim.isAIControlled())
   {
      // The Bot is only a little sorry:
      if ( getRandom() > 0.4 )
         AIMessageThread("ChatSorry", %clAttacker, %clVictim);
   }
}
