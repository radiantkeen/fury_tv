//-------------------------------------------------------------------------------- fixed ai message thread spam - Lagg... 11-11-2003
function AIMessageThread(%msg, %clSpeaker, %clListener, %force, %index, %threadId)
{
	//abort if AI chat has been disabled
	if ($AIDisableChat)
		return;

	//initialize the params
	if (%index $= "")
		%index = 0;

	if (%clListener $= "")
		%clListener = 0;

	if (%force $= "")
		%force = false;

	//if this is the initial call, see if we're already in a thread, and if we should force this one
	if (%index == 0 && $AIMsgThreadActive && !%force)
	{
                AIEndMessageThread(%threadId);//added here to clear any message errors - Lagg...
		//error("DEBUG msg thread already in progress - aborting: " @ %msg @ " from client: " @ %clSpeaker);
                return;
	}

	//if this is an ongoing thread, make sure it wasn't pre-empted
	if (%index > 0 && %threadId != $AIMsgThreadId)
		return;

	//if this is a new thread, set a new thread id
	if (%index == 0)
	{
		$AIMsgThreadId++;
		$AIMsgThreadActive = true;
		%threadId = $AIMsgThreadId;
	}
	switch$ (%msg)
	{
		//this is an example of how to use the chat system without using the table...
		case "ChatHi":
			serverCmdCannedChat(%clSpeaker, 'ChatHi', true);
			%responsePending = false;
			if (%index == 0)
			{
				if (%clListener < 0)
					%clListener = AIFindCommanderAI(%clSpeaker);
				if (%clListener > 0 && (getRandom() < 0.5))
				{
					%responsePending = true;
               schedule(1000, 0, "AIMessageThread", %msg, %clListener, 0, false, 1, %threadId);
				}
			}
			if (! %responsePending)
				schedule(1000, 0, "AIEndMessageThread", $AIMsgThreadId);

		//this method of using the chat system sets up a table instead...
		//the equivalent is commented out below - same effect - table is much better.
		case "ChatSelfDefendGenerator":
			if (%index == 0)
			{
				//initialize the table
				$AIMsgThreadIndex = 0;
				$AIMsgThreadTable[1] = "";

				%commander = AIFindCommanderAI(%clSpeaker);
				if (%commander > 0)
				{
					$AIMsgThreadTable[0] = %commander @ " ChatCmdDefendBase " @ 1000;
					$AIMsgThreadTable[1] = %clSpeaker @ " ChatCmdAcknowledged " @ 1250;
					$AIMsgThreadTable[2] = %clSpeaker @ " " @ %msg @ " " @ 1000;
					$AIMsgThreadTable[3] = "";
				}
				else
					$AIMsgThreadTable[0] = %clSpeaker @ " " @ %msg @ " " @ 1000;

				//now process the table
				AIProcessMessageTable(%threadId);
			}
		
//		case "ChatSelfDefendGenerator":
//			%responsePending = false;
//			if (%index == 0)
//			{
//				//find the commander
//				%commander = AIFindCommanderAI(%clSpeaker);
//				if (%commander > 0)
//				{
//					%responsePending = true;
//					serverCmdCannedChat(%commander, "ChatCmdDefendBase", true);
//					schedule("AIMessageThread(" @ %msg @ ", " @ %clSpeaker @ ", 0, 1, " @ %type @ ", false, " @ %threadId @ ");", 1000);
//				}
//				else
//					serverCmdCannedChat(%commander, "ChatSelfDefendGenerator", true);
//			}
//			else if (%index == 1)
//			{
//				//make sure the client is still alive
//				if (AIClientIsAlive(%clSpeaker, 1000))
//				{
//					%responsePending = true;
//					serverCmdCannedChat(%clSpeaker, "ChatCmdAcknowledged", true);
//					schedule("AIMessageThread(" @ %msg @ ", " @ %clSpeaker @ ", 0, 2, " @ %type @ ", false, " @ %threadId @ ");", 1000);
//				}
//				else
//					AIEndMessageThread($AIMsgThreadId);
//			}
//			else if (%index == 2)
//			{
//				//make sure the client is still alive
//				if (AIClientIsAlive(%clSpeaker, 1000))
//					serverCmdCannedChat(%clSpeaker, "ChatSelfDefendGenerator", true);
//				else
//					AIEndMessageThread($AIMsgThreadId);
//			}
//			if (! %responsePending)
//				schedule("AIEndMessageThread(" @ $AIMsgThreadId @ ");", 1000);
			
		default:
         %tag = addTaggedString( %msg );
			serverCmdCannedChat( %clSpeaker, %tag, true );
         removeTaggedString( %tag );   // Don't keep incrementing the string ref count...
			schedule( 1500, 0, "AIEndMessageThread", $AIMsgThreadId );
	}
}

//==============================================================================
//Fixes Bots Playing Animations in vehiciles
function AIPlayAnimSound(%client, %location, %sound, %minCel, %maxCel, %index)
{
	//make sure the client is still alive
	if (! AIClientIsAlive(%client, 500))
		return;

	switch (%index)
	{
		case 0:
			//if we can set the client's aim, we can also try the animation
               //-----------------------------------
               //Capto -- 8/8/02
               //Dont play animations if Mounted
			if (%client.aimAt(%location, 2500) && !%client.vehicleMounted)
			   schedule(250, %client, "AIPlayAnimSound", %client, %location, %sound, %minCel, %maxCel, 1);
			else
				schedule(750, %client, "AIPlay3DSound", %client, %sound);

		case 1:
			//play the animation and schedule the next phase
			%randRange = %maxCel - %minCel + 1;
		  	%celNum = %minCel + mFloor(getRandom() * (%randRange - 0.1));
               //-----------------------------------
               //Capto -- 8/8/02
               //Dont play animations if Mounted
			if (%celNum > 0 && !%client.vehicleMounted)
			   %client.player.setActionThread("cel" @ %celNum);
		   schedule(500, %client, "AIPlayAnimSound", %client, %location, %sound, %minCel, %maxCel, 2);

		case 2:
			//say 'hi'
			AIPlay3DSound(%client, %sound);
	}
}

//------------------------------------------------------------------------------------------
//lets try and fix the bot standing in vehicle bug  - Lagg... 12-29-2005
// NOTENOTENOTE: Review
function serverCmdPlayAnim(%client, %anim)
{
   echo("serverCmdPlayAnim");

   //fix here - Lagg...
   if(isObject(%client.objectiveTask.targetClient) && %client.objectiveTask.targetClient.isMounted())
   {
      //error("serverCmdPlayAnim(%client, %anim) - Fix here LAGG...");
      return;
   }

   if( %anim $= "Death1" || %anim $= "Death2" || %anim $= "Death3" || %anim $= "Death4" || %anim $= "Death5" ||
       %anim $= "Death6" || %anim $= "Death7" || %anim $= "Death8" || %anim $= "Death9" || %anim $= "Death10" || %anim $= "Death11" )
      return;

   %player = %client.player;
   // don't play animations if player is in a vehicle
   // ------------------------------------------------------------------
   // z0dd - ZOD, 5/8/02. Console spam fix, check for player object too.
   //if (%player.isMounted())
   //   return;
   if(%player.isMounted() || !isObject(%player))
      return;

   %weapon = ( %player.getMountedImage($WeaponSlot) == 0 ) ? "" : %player.getMountedImage($WeaponSlot).getName().item;
   if(%weapon $= "MissileLauncher" || %weapon $= "SniperRifle")
   {
      %player.animResetWeapon = true;
      %player.lastWeapon = %weapon;  
      %player.unmountImage($WeaponSlot);
      // ----------------------------------------------
      // z0dd - ZOD, 5/8/02. %obj is the wrong varible.
      //%obj.setArmThread(look);
      %player.setArmThread(look);
   }      
   %player.setActionThread(%anim);
}

//------------------------------------------------------------------------------------------
//have to update this function to include all the new aiobjectives :) - Lagg... 11-11-2003
function AIRespondToEvent(%client, %eventTag, %targetClient)
{
	//record the event time
	$EventTagTimeArray[%eventTag] = getSimTime();

	//abort if AI chat has been disabled
	if ($AIDisableChatResponse)
		return;

	%clientPos = %client.player.getWorldBoxCenter();
  	switch$ (%eventTag)
	{
		case 'ChatHi' or 'ChatAnimWave':
			if (AIClientIsAlive(%targetClient) && %targetClient.isAIControlled())
			{
				%setHumanControl = false;
	 			if (!%client.isAIControlled() && %client.controlAI != %targetClient)
					%setHumanControl = aiAttemptHumanControl(%client, %targetClient);
				if (%setHumanControl)
				   schedule(250, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, "gbl.hi", $AIAnimSalute, $AIAnimSalute, 0);
				else
				   schedule(250, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, "gbl.hi", $AIAnimWave, $AIAnimWave, 0);
			}

		case 'ChatBye' or 'ChatTeamDisembark' or 'ChatAnimBye' or 'ChatCmdCompleted':
			if (AIClientIsAlive(%targetClient) && %targetClient.isAIControlled())
			{
			   schedule(250, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, "gbl.bye", $AIAnimWave, $AIAnimWave, 0);

				//see if we need to release the bot
				if (aiHumanHasControl(%client, %targetClient))
				{
					%objective = %targetClient.objective;
					if (%objective.issuedByClientId == %client && %objective.targetClientId == %client && %objective.getName() $= "AIOEscortPlayer")
					{
						AIClearObjective(%objective);
						%objective.delete();
					}
					aiReleaseHumanControl(%client, %targetClient);
				}
			}

		case 'ChatTeamYes' or 'ChatGlobalYes' or 'ChatCheer' or 'ChatAnimDance':
			if (AIClientIsAlive(%targetClient) && %targetClient.isAIControlled())
			{
				//choose the animation range
				%minCel = 5;
				%maxCel = 8;
				if (getRandom() > 0.5)
					%sound = "gbl.thanks";
				else
					%sound = "gbl.awesome";
			   schedule(500, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, %sound, %minCel, %maxCel, 0);
			}

		case 'ChatAnimSalute':
			if (AIClientIsAlive(%targetClient) && %targetClient.isAIControlled())
			   schedule(500, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, "gbl.thanks", $AIAnimSalute, $AIAnimSalute, 0);

		case 'ChatAwesome' or 'ChatGoodGame' or 'ChatGreatShot' or 'ChatNice' or 'ChatYouRock' or 'ChatAnimSpec3':
			if (AIClientIsAlive(%targetClient) && %targetClient.isAIControlled())
			{
				//choose the animation range
				%minCel = 5;
				%maxCel = 8;
				if (getRandom() > 0.5)
					%sound = "gbl.thanks";
				else
					%sound = "gbl.yes";
			   schedule(250, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, %sound, %minCel, %maxCel, 0);
			}

		case 'ChatAww' or 'ChatObnoxious' or 'ChatSarcasm' or 'ChatAnimSpec1' or 'ChatAnimSpec2':
			if (AIClientIsAlive(%targetClient) && %targetClient.isAIControlled())
			{
				if (%targetClient.controlByHuman == %client)
				   schedule(1500, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, "gbl.oops", $AIAnimSalute, $AIAnimSalute, 0);
				else
				   schedule(1500, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, "gbl.quiet", -1, -1, 0);
			}

		case 'ChatBrag' or 'ChatAnimGetSome':
			if (AIClientIsAlive(%targetClient) && %targetClient.isAIControlled())
			{
				if (%targetClient.controlByHuman == %client)
				   schedule(1500, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, "gbl.yes", $AIAnimSalute, $AIAnimSalute, 0);
				else
				   schedule(1500, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, "gbl.no", -1, -1, 0);
			}

		case 'ChatAnimAnnoyed' or 'ChatMove':
			if (AIClientIsAlive(%targetClient) && %targetClient.isAIControlled())
			{
				if (%targetClient.controlByHuman == %client)
				{
				   schedule(750, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, "vqk.sorry", $AIAnimSalute, $AIAnimSalute, 0);
					%targetClient.schedule(2750, "setDangerLocation", %clientPos , 20);
				}
				else
				   schedule(750, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, "gbl.no", -1, -1, 0);
			}

		case 'ChatQuiet':
			if (AIClientIsAlive(%targetClient) && %targetClient.isAIControlled())
			{
				if (%targetClient.controlByHuman == %client)
				   schedule(250, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, "gbl.yes", $AIAnimSalute, $AIAnimSalute, 0);
				else
				   schedule(250, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, "gbl.no", -1, -1, 0);
			}

		case 'ChatWait' or 'ChatShazbot':
			if (AIClientIsAlive(%targetClient) && %targetClient.isAIControlled())
			{
				if (%targetClient.controlByHuman == %client)
				   schedule(250, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, "gbl.yes", $AIAnimSalute, $AIAnimSalute, 0);
				else
				   schedule(250, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, "gbl.no", -1, -1, 0);
			}

		case 'ChatLearn' or 'ChatIsBaseSecure':
		   schedule(750, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, "gbl.dunno", -1, -1, 0);

		case 'ChatWelcome' or 'ChatSorry' or 'ChatAnyTime':
		   schedule(250, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, "gbl.thanks", $AIAnimWave, $AIAnimWave, 0);

		case 'ChatDunno' or 'ChatDontKnow':
		   schedule(250, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, "gbl.yes", -1, -1, 0);

		case 'ChatTeamNo' or 'ChatOops' or 'ChatGlobalNo':
		   schedule(250, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, "gbl.shazbot", -1, -1, 0);

		case 'ChatTeamThanks' or 'ChatThanks':
		   schedule(250, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, "vqk.anytime", $AIAnimWave, $AIAnimWave, 0);

		case 'ChatWarnShoot':
		   schedule(250, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, "vqk.sorry", -1, -1, 0);
		  
		case 'ChatCmdWhat':
			//see if the weight for the escort is higher than what he might otherwise be doing
			if (AIClientIsAlive(%targetClient) && %targetClient.isAIControlled())
			{
				//first, find the chat message
				%objective = %targetClient.objective;
				if (%objective > 0)
				{
					if (%objective.chat)
						%sound = getWord(%objective.chat, 0) @ "Sound";
					else
					{
						%type = %objective.getName();
						switch$ (%type)
						{
							case "AIOAttackLocation" or "AIOMAttackLocation":
								%sound = "att.base";
							case "AIOAttackObject" or "AIOMissileObject" or "AIOMDestroyObject"
                                                          or "AIODestroyObject":
								if (%objective.targetObjectId > 0)
								{
									%objType = %objective.targetObjectId.getDataBlock().getName();
									switch$ (%objType)
									{
										case "GeneratorLarge":
											%sound = "slf.att.generator";
										case "SensorLargePulse":
											%sound = "slf.att.sensors";
										case "SensorMediumPulse":
											%sound = "slf.att.sensors";
										case "TurretBaseLarge":
											%sound = "slf.att.turrets";
										case "StationVehicle":
											%sound = "slf.att.vehicle";
										default:
											%sound = "slf.att.base";
									}
								}
								else
									%sound = "slf.att.base";
							case "AIODefendLocation" or AIODefensePatrol:
								if (%objective.targetObjectId > 0)
								{
									%objType = %objective.targetObjectId.getDataBlock().getName();
									switch$ (%objType)
									{
										case "Flag":
											%sound = "slf.def.flag";
										case "GeneratorLarge":
											%sound = "slf.def.generator";
										case "SensorLargePulse":
											%sound = "slf.def.sensors";
										case "SensorMediumPulse":
											%sound = "slf.def.sensors";
										case "TurretBaseLarge":
											%sound = "slf.def.turrets";
										case "StationVehicle":
											%sound = "slf.def.vehicle";
										default:
											%sound = "slf.def.base";
									}
								}
								else
									%sound = "slf.def.defend";
							case "AIOAttackPlayer":
								%sound = "slf.att.attack";
							case "AIOEscortPlayer":
								%sound = "slf.def.defend";
							case "AIORepairObject":
								if (%objective.targetObjectId > 0)
								{
									%objType = %objective.targetObjectId.getDataBlock().getName();
									switch$ (%objType)
									{
										case "GeneratorLarge":
											%sound = "slf.rep.generator";
										case "SensorLargePulse":
											%sound = "slf.rep.sensors";
										case "SensorMediumPulse":
											%sound = "slf.rep.sensors";
										case "TurretBaseLarge":
											%sound = "slf.rep.turrets";
										case "StationVehicle":
											%sound = "slf.rep.vehicle";
										default:
											%sound = "slf.rep.equipment";
									}
								}
								else
									%sound = "slf.rep.base";
							case "AIOLazeObject":
									%sound = "slf.att.base";
							case "AIOMortarObject":
								if (%objective.targetObjectId > 0)
								{
									%objType = %objective.targetObjectId.getDataBlock().getName();
									switch$ (%objType)
									{
										case "GeneratorLarge":
											%sound = "slf.att.generator";
										case "SensorLargePulse":
											%sound = "slf.att.sensors";
										case "SensorMediumPulse":
											%sound = "slf.att.sensors";
										case "TurretBaseLarge":
											%sound = "slf.att.turrets";
										case "StationVehicle":
											%sound = "slf.att.vehicle";
										default:
											%sound = "slf.att.base";
									}
								}
								else
									%sound = "slf.att.base";
							case "AIOTouchObject":
								if (%objective.mode $= "FlagGrab")
									%sound = "slf.att.flag";
								else if (%objective.mode $= "FlagCapture")
									%sound = "flg.flag";
								else 
									%sound = "slf.att.base";

							case "AIODeployEquipment":
                                                                %equip = %objective.equipment;
                                                                if (%equip $= "InventoryDeployable")
                                                                   %sound = "slf.tsk.remotes";
                                                                else if (%equip $= "PulseSensorDeployable" ||
                                                                  %equip $= "MotionSensorDeployable")
                                                                   %sound = "slf.tsk.sensors";
                                                                else if (%equip $= "TurretOutdoorDeployable" ||
                                                                  %equip $= "TurretIndoorDeployable")
                                                                   %sound = "slf.tsk.turrets";
                                                                else
                                                                   %sound = "slf.tsk.defense";

                                                        case "AIOPlaceCamera":
                                                                        %sound = "slf.tsk.sensors";

                                                        case "AIODeployVehicle" or "AIOEasyRider" or "AIOPilotShrike":
									%sound = "slf.tsk.vehicle";
                                                        case "AIOTankPatrol" or "AIONeedARide" or "AIOBombingRun":
									%sound = "need.support";
								  
							default:
								%sound = "gbl.dunno";
						}
					}
				}
				else
					%sound = "gbl.dunno";

				//now that we have a sound, play it with the salute animation
			   schedule(250, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, %sound, 1, 1, 0);
			}

		//these here are all "self" messages requiring a "thank you"
		case 'ChatRepairBase' or 'ChatSelfAttack' or 'ChatSelfAttackBase' or 'ChatSelfAttackFlag' or 'ChatSelfAttackGenerator' or 'ChatSelfAttackSensors' or 'ChatSelfAttackTurrets' or 'ChatSelfAttackVehicle':
		   schedule(750, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, "gbl.thanks", 1, 1, 0);

		case 'ChatSelfDefendBase' or 'ChatSelfDefend' or 'ChatSelfDefendFlag' or 'ChatSelfDefendGenerator' or 'ChatSelfDefendNexus' or 'ChatSelfDefendSensors' or 'ChatSelfDefendTurrets' or 'ChatSelfDefendVehicle':
		   schedule(750, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, "gbl.thanks", 1, 1, 0);

		case 'ChatSelfRepairBase' or 'ChatSelfRepairEquipment' or 'ChatSelfRepairGenerator' or 'ChatSelfRepair' or 'ChatSelfRepairSensors' or 'ChatSelfRepairTurrets' or 'ChatSelfRepairVehicle':
		   schedule(750, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, "gbl.thanks", 1, 1, 0);

		case 'ChatTaskCover' or 'ChatTaskSetupD' or 'ChatTaskOnIt' or 'ChatTaskSetupRemote' or 'ChatTaskSetupSensors' or 'ChatTaskSetupTurrets' or 'ChatTaskVehicle':
		   schedule(750, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, "gbl.thanks", 1, 1, 0);

		case 'EnemyFlagCaptured':
			//find all the clients within 100 m of the home flag, and get them all to cheer
			%flagPos = %client.player.getWorldBoxCenter();
		   %count = ClientGroup.getCount();
		   for(%i = 0; %i < %count; %i++)
		   {
				%cl = ClientGroup.getObject(%i);

				//make sure the client is alive, and on the same team
				if (%cl.isAIControlled() && %cl.team == %client.team && AIClientIsAlive(%cl))
				{
					//see if they're within 100 m
					%distance = %cl.getPathDistance(%flagPos);
		         if (%distance > 0 && %distance < 100)
		         {
						//choose the animation range
						%minCel = 5;
						%maxCel = 8;
					  	%randTime = mFloor(getRandom() * 1500) + 1;

						//pick a random sound
						if (getRandom() > 0.25)
							%sound = "gbl.awesome";
						else if (getRandom() > 0.5)
							%sound = "gbl.thanks";
						else if (getRandom() > 0.75)
							%sound = "gbl.nice";
						else
							%sound = "gbl.rock";
					   schedule(%randTime, %cl, "AIPlayAnimSound", %cl, %flagPos, %sound, %minCel, %maxCel, 0);
					}
				}
			}

		case 'ChatCmdHunterGiveFlags' or 'ChatCmdGiveMeFlag':
			if (AIClientIsAlive(%targetClient) && %targetClient.isAIControlled())
			{
			   schedule(250, %targetClient, "AIPlayAnimSound", %targetClient, %clientPos, "cmd.acknowledge", $AIAnimSalute, $AIAnimSalute, 0);
			   schedule(750, %targetClient.player, "serverCmdThrowFlag", %targetClient);
			}

	}
}
