//-------------------------------------------------------------------------------------------------------------------------------------
//I would like to extend thanks to Founder (Martin Hoover) from WWW.GarageGames.com for his mathamatics help.
//ZOD, Dev and everybody else at www.tribes2maps.com/forums that helped me with scripts and/or ideas.
//After many months in development this file is still a work in progress, if you make any improvments please
//comment them and send me the upgraded file to JPerricone@nyc.rr.com (A.K.A. Lagg-Alot) 

//---------------------------------------------------------------------------------------------------------- AIOPilotShrike::weight ---
//Using the Tribes 2 Editor make a new Simgroup called "T" @ %team @ "ShrikePath" @ %num
//(example T2ShrikePath1 or T2ShrikePath10 / T1ShrikePath1 or T1ShrikePath6)
//inside each simgroups place markers or cameras in the order to follow for each path.
//You may set up as many simgroups as you like but you must state which paths
//the AIPilot is allowed to follow by adding a DynamicField called "paths" to
//the AIObjectiveMarker, set the "paths" field to the number of the path Bot can follow.
//give this guy plenty of room with the markers, he flys like a wild man :) - Lagg... 4-8-2004
//
//NOTE:All Vehicle AIObjective Markers must be placed within 15 meters to the VPad Bot is to buy from.
//and each AIObjective Marker must have a "paths" field stating the paths to use. May have any amount
//of paths per AIObjective but must have a simgroup named  "T" @ %team @ "ShrikePath" @ %num. Simgroup
//must containing Marker objects to follow. You state the paths and set up marker simgroups, paths are
//randomly chosen (example simgroup named "T2ShrikePath1" or "T2ShrikePath10" / "T1ShrikePath1" or 
//"T1ShrikePath6"). The %num at the end of simgroup name does not have to be consecutive.
// In the AIOObjectiveMarker.Dynamic Field called "paths" just put the numbers of the simgroups to
//follow must be seperated by a space (Dynamic field example: paths = "1 2 4 7 9";) can be in any order.
//
//This way you would place AIOPilotShrike Objective within 15meters of team 1's vehicle Station and set
//paths = "1 2", then make 2 simgroups named T1ShrikePath1 and T1ShrikePath2 and in these simgroups
//you would place markers or cameras in the path you want bot to follow. At a diferent vehicle Station
//on the same team (some maps have more than 1 vehicle pad per team) you could place another AIOPilotShrike
//Objective and state Paths = "3 4"and lay out more markers in the T1ShrikePath3 and T1ShrikePath4 simgroups.
//You now have multiple paths and multiple Vpads to choose from. - Lagg... 5-5-2005

//POSSIBLE MODES = "FlagDive" (means pilot will just fly the path/s you laid out and defend himself of course)

//NEW FEATURE: A new simgroup named "MissileRun" is now required. Place a few markers in it. The points at
//which Shrike will run to if chased by a seeker projectile (Missile). Added Lagg-Alot - 5/2/2009


function AIOPilotShrike::weight(%this, %client, %level, %minWeight, %inventoryStr)
{   
   //make sure the player is still alive!!!!!
   if (! AIClientIsAlive(%client))
      return 0;

   // For CnH games, we set a target object and set def or offense (can be used for any gameType)
   if (%this.targetObjectId > 0 && %this.clientLevel1 != %client)
   {
      if (!isObject(%this.targetObjectId) || %this.targetObjectId.isHidden())
         return 0;

      //if offense and we own target object quit
      if (%this.offense > 0)
      {
         if (%this.targetObjectId.team == %client.team || %this.targetObjectId.getDamageState() $= "Destroyed")
            return 0;
      }
      //if defense and we own it must be powered and not destroyed, or if enemy it must be destroyed or not powered
      else if (%this.defense > 0)
      {
         if (%this.targetObjectId.team == %client.team)
         {
            if (!%this.targetObjectId.isPowered() || %this.targetObjectId.getDamageState() $= "Destroyed")
               return 0;
         }
         else
         { 
            if (%this.targetObjectId.isPowered() && %this.targetObjectId.getDamageState() !$= "Destroyed")
              return 0;
         }
      }        
   }

   //check if flight path exists
   if (Game.class $= "SiegeGame")
      %team = Game.offenseTeam == %client.team ? 1 : 2;
   else
      %team = %client.team;

   if(! isObject(nameToId("T" @ %team @ "ShrikePath" @ firstWord(%this.paths))))
   {
      error("AIOPilotShrike Error! - No Paths field specified in AIOPilotShrike objective Marker");
      return 0;
   }

   //do a quick check to disqualify this objective if it can't meet the minimum weight
   if (!AIODefault::QuickWeight(%this, %client, %level, %minWeight))
   {
      if (%this.targetObjectId > 0 && %this.issuedByClientId == %client.controlByHuman)
      {
         if ($AIWeightHumanIssuedCommand < %minWeight)
	    return 0;
      }
      else
         return 0;
   }

   //check for Vehicle station near buy of forget it if we are not mounted yet or didn't just buy - Lagg...
   //as of right now the Objective Marker must be place with in 75 m of the vpad to buy from - Lagg ... *************

   if (!%client.player.isMounted() && !%client.justBought)
   {
      %clVs = AIFindClosestVStation(%client, %this.position);
      if (%clVs > 0)
      {
         %closestVs = getWord(%clVs, 0);
         %closestVsDist = getWord(%clVs, 1);
         if (%closestVsDist > 15)
         {
            //error("AIOPilotShrike Error! - Marker More Than 15Meters From A Team Vehicle Station");
            return 0;
         }

         if (VectorDist (%client.player.position, %this.position) > 300)//---* close to VPad or return 0 *---
            return 0;
      }
      else
         return 0;

      //see if this vehicle is allowed at the VPad in question
      if (%closestVs.ScoutFlyer $= "Removed")
      {
         error("AIOPilotShrike Error! - ScoutFlyer is not allowed at VPad :(");
         return 0;
      }

      //check if any of vehicle type are availible
      %blockName = "ScoutFlyer";
      if (!vehicleCheck(%blockName, %client.team))
      {
         //error("AIOPilotShrike Error! - NO SHRIKES Left to buy " @ getTaggedString(%client.name));
         return 0;
      }
   }

   %weight = AIODefault::weight(%this, %client, %level, %inventoryStr);       
   
   //never bump a shrike pilot from his ride
   if (%this.clientLevel1 > 0  && %level == 1 && %this.clientLevel1 != %client)
      return 0;
   else if (%this.clientLevel1 == %client)
      %weight = 10000;

   return %weight;
}

function AIOPilotShrike::assignClient(%this, %client)
{
   %client.objectiveTask = %client.addTask(AIPilotShrike);
   %client.objectiveTask.initFromObjective(%this, %client);
}

function AIOPilotShrike::unassignClient(%this, %client)
{
   //error("AIOPilotShrike::unassignClient - " @ getTaggedString(%client.name));

   if(%client.pilotVehicle)
      AIDisembarkVehicle(%client);
 
   %client.removeTask(%client.objectiveTask);
   %client.objectiveTask = "";
}

//-------------------------------------------------------------------------------------

function AIPilotShrike::initFromObjective(%task, %objective, %client)
{
   //initialize the task vars from the objective
   %task.baseWeight = %client.objectiveWeight;
   %task.targetObject = %objective.targetObjectId;
   %task.location = %objective.position;

   %task.equipment = %objective.equipment;
   %task.buyEquipmentSet = %objective.buyEquipmentSet;
   %task.desiredEquipment = %objective.desiredEquipment;
   %task.issuedByClient = %objective.issuedByClientId;
   %task.chat = %objective.chat;
   %task.mode = %objective.mode;

   //initialize other task vars
   %task.sendMsg = true;
   %task.sendMsgTime = 0;
   %task.paths = %objective.paths;

   %client.player.mVehicle = "";// needed if bot just hopped out a vehicle and it was not destroyed/removed yet !
}

function AIPilotShrike::assume(%task, %client)
{
   //frequency according to skill
   %skill = %client.getSkillLevel();// -------------------------------------------- %skill *

   if (%skill < 0.05)
      %freq = 35;
   else if (%skill < 0.20)
      %freq = 30;
   else if (%skill < 0.35)
      %freq = 27;
   else if (%skill < 0.50)
      %freq = 24;
   else if (%skill < 0.65)
      %freq = 21;
   else if (%skill < 0.80)
      %freq = 18;
   else
      %freq = 15;
         
   //set the freqency
   %task.setWeightFreq(%freq);
   %task.setMonitorFreq(15);

   //even if we don't need equipemnt, see if we should buy some...
   if (%task.equipment !$= "")
      %equipmentList = %task.equipment;
   else
      %equipmentList = %task.desiredEquipment;
   %client.needEquipment = AINeedEquipment(%equipmentList, %client);

   if (! %client.needEquipment && %task.buyEquipmentSet !$= "")
   {
      //see if we could benefit from inventory
      %needArmor = AIMustUseRegularInvStation(%equipmentList, %client);
      %result = AIFindClosestInventory(%client, %needArmor);
      %closestInv = getWord(%result, 0);
      %closestDist = getWord(%result, 1);
      if (%closestInv > 0 && %closestDist < 100)
         %client.needEquipment = true;
   }

   //mark the current time for the buy inventory state machine
   %task.buyInvTime = getSimTime();

   //reset the vehicle station wait time
   %client.vsWaitTime = "";

   //clear the target tag
   %task.shouldAttack = -1;

   //set the destination paths for team and game type !

   //for siege game type
   if (Game.class $= "SiegeGame")
      %team = Game.offenseTeam == %client.team ? 1 : 2;
   else
      %team = %client.team;
   
   //first see how many paths we have
   %mx = getWordCount(%task.paths);
   
   //and which path to use
   %random = mFloor(getRandom(1, %mx));
   %random--;
   %tg = getWord(%task.paths, %random);
   %task.group = nameToId("T" @ %team @ "ShrikePath" @ %tg);
   %task.count = %task.group.getCount();
   %task.locationIndex = 0;
   
   %client.needVehicle = true;
   %client.shrikeFire = false;
   %task.firePos = -1;
   %task.searchPos = -1;
   %task.searchTime = -1;
   %task.swch = 1;//need this for firing guns at players, turrets, solar panels and sensors

   %task.run = -1;
}


function AIPilotShrike::retire(%task, %client)
{
   if (aiClientIsAlive(%client))
   {
      %client.needVehicle = false;
      %client.clearStep();
      if(%client.player.isMounted())
         AIDisembarkVehicle(%client);
   }
}

//------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------- lets use the weight to choose the task targets ---
//------------------------------------------------------------------------------------------------------------------------
function AIPilotShrike::weight(%task, %client)
{
   //are we mounted in vehicle ?
   if (%client.player.isMounted() && isObject(%client.player.mVehicle))
   {     
      //get the vehicle data stuff...
      %vehicle = %Client.vehicleMounted;//-------------------------------------------- %vehicle *

      //find the terrain height directly below us 
      %myPos = %vehicle.getWorldBoxCenter();//---------------------------------------- %myPos *
      %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType |
        $TypeMasks::ForceFieldObjectType;
      %downVec = getWord(%myPos, 0) @ " " @ getWord(%myPos, 1) @ "0";
      %altSur = containerRayCast(%myPos, %downVec, %mask, 0);
      %altPos = posfromRayCast(%altSur);
      %terHt = getTerrainHeight(%altPos);//-------------------------------------------- %terHt *
      //%myAlt = getWord(%myPos, 2) - getWord(%altPos, 2);// -------------------------- %myAlt * not used in weight function

      

      //if we have a target, is it to far, dead or destroyed?
      if (isObject(%task.shouldAttack) && %task.shouldAttack > 0)
      {
         %dist = vectorDist(%task.shouldAttack.getWorldBoxCenter(), %myPos);

         if (isObject(%task.shouldAttack) && %task.shouldAttack.getDataBlock().getClassName() $= "PlayerData")
         {
            if (%dist > 400 || %task.shouldAttack.getState() $= "Dead")// --- dist to clear Player Targets is 400m
            {
               %task.shouldAttack = -1;
               %task.searchTime = -1;
               %task.searchPos = -1;
            }
         }
         //else if (%dist > 500 || %task.shouldAttack.getDamageState() $= "Destroyed")// - dist to clear Targets is 500m
         else if (%dist > 450 || %task.shouldAttack.getDamageState() $= "Destroyed")//changed range 5-26-2005
         {
            %task.shouldAttack = -1;
            %task.searchTime = -1;
            %task.searchPos = -1;
         }
      }

      //check if we are hunted by a seeker missile
      %found = false;
      %missileCount = MissileSet.getCount();
      for (%i = 0; %i < %missileCount; %i++)
      {
         %missile = MissileSet.getObject(%i);
         %missileTarg = (%missile.getTargetObject());
         if (%missileTarg == %vehicle)
	 {
            %found = %missile;
            break;
         }
      }

      //if we have a missle chasing us. deal with it. RUN!
      if (isObject(%found))
      {
         //use the Y-axis of the vehicle rotation as the desired direction to check
         //and calculate a point 4m in front of vehicle - Lagg...
         %muzzle = MatrixMulVector("0 0 0 " @ getWords(%vehicle.getTransform(), 3, 6), "0 1 0");
         %muzzle = VectorNormalize(%muzzle);
         %factor = 4; //4m in front of vehicle is the right spot to start
         %muzzlePoint = VectorAdd(%vehLoc,VectorScale(%muzzle, %factor));

         //loop though all safe spots find the ones we are facing
         %set = nameToId("MissileRun");
         %setC = %set.getCount();
         for (%i = 0; %i < %setC; %i++)
         {
            %point = %set.getObject(%i);
            %myD = VectorDist(%myPos, %point.position);
            %mzD = VectorDist(%muzzlePoint, %point.position);

            if (%mzD < %myD)
            {
               %task.run = %point;
               return;//let the monitor deal with the missile - Lagg... 5/2/2009
            }
         }  
      }
      else
         %task.run = -1;     

      %lastAttacker = %vehicle.lastDamagedBy;//---------------------------------------- what damaged us ???
      if (isObject(%lastAttacker))
      {
         if (%lastAttacker.isMounted())
            %lastAttacker = %lastAttacker.getObjectMount();

         %lastClass = %lastAttacker.getDataBlock().getClassName();// --------------- we need the target class to make decisions down below
         %vehicle.lastDamagedBy = -1;// --------------------------------------------- CLEAR THE VEHICLE DAMAGED FLAG
      }
      
      //----------------------------------- if an enemy has fired on our vehicle recently and we have a target already ...

      //- make sure it is new target and alive (should change so last attacker could = current target to reset time check)
      if (isObject(%lastAttacker) && isObject(%task.shouldAttack) && %lastAttacker != %task.shouldAttack)
      { 
         %targPos = %task.shouldAttack.getWorldBoxCenter();
         %curTargDist = vectorDist(%myPos, %targPos);

         //echo("%targPos = " @ %targPos);
         //echo("%curTargDist = " @ %curTargDist);
         //echo("----------------------------------------");

      
         %newTargPos = %lastAttacker.getWorldBoxCenter();
         %newTargDist = vectorDist(%myPos, %newTargPos);

         //echo("%newTargPos = " @ %newTargPos);
         //echo("%newTargDist = " @ %newTargDist);

         %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
         %hasLOS = !containerRayCast(%myPos, %targPos, %mask, 0);

         if (%lastClass $= "FlyingVehicleData" && %hasLOS)// --- was it a Shrike? 1st piority
         {
            if (%newTargDist > 0 && %newTargDist < %curTargDist + 300)
            {
               %task.shouldAttack = %lastAttacker;
               if (%task.searchTime == -1)
                  %task.searchTime = getSimTime();
               %task.searchPos = %task.shouldAttack.getWorldBoxCenter();
            }
         }
         // --- was it a vehicle turret? 2nd piority
         else if (%lastClass $= "TurretData" && %lastAttacker.team != %client.team && %hasLOS)
         { 
            %type = %lastAttacker.getDataBlock().getName();
            //------------------- what's the distance of new attacker???                         
            if (%type $= "TurretBaseLarge" && %newTargDist < %curTargDist + 250)
            {
               %task.shouldAttack = %lastAttacker;
               if (%task.searchTime == -1)
                  %task.searchTime = getSimTime();
            }
         }
         //-------------------------- was it a vehicle (Tank) turret? 3rd piority
         else if (%lastClass $= "HoverVehicleData" && %hasLOS)
         {
            //--------------------- what's the distance of new attacker ???               
            if (%newTargDist > 0 && %newTargDist < %curTargDist + 200)
            {
               %task.shouldAttack = %lastAttacker;
               if (%task.searchTime == -1)
                  %task.searchTime = getSimTime();

               %tx = firstWord(%task.shouldAttack.getTransform());
               %ty = getWord(%task.shouldAttack.getTransform(), 1);
               %tz = getWord(%task.shouldAttack.getTransform(), 2);
               %tz += mFloor(getRandom(2, 5) * 5);//add some altitude to search for targets on the ground
               // --- %task.searchPos used for moving targets, not baseturrets or sensors
               %task.searchPos = %tx SPC %ty SPC %tz;
            }
         }
         //-- was it a Player? 4th piority (check team for this only)
         else if (%lastClass $= "PlayerData" && %lastAttacker.team != %client.team && %hasLOS)
         {
            //see if the new targ is no more than 250 m further
            if (%newTargDist > 0 && %newTargDist < %curTargDist + 250)
            {
               %task.shouldAttack = %lastAttacker;
               if (%task.searchTime == -1)
                  %task.searchTime = getSimTime();
               %tx = firstWord(%task.shouldAttack.getTransform());
               %ty = getWord(%task.shouldAttack.getTransform(), 1);
               %tz = getWord(%task.shouldAttack.getTransform(), 2);
               %tz += mFloor(getRandom(2, 5) * 5);//-------------------------------- add some altitude to search for targets on the ground
               %task.searchPos = %tx SPC %ty SPC %tz;//---------------------------- %task.searchPos used for moving targets, not baseturrets or sensors

            }
         }
         //DON'T THINK I WANT HIM SNOOPING AROUND IF DAMAGED BY A DEPLOYED MPB (hell their deadly to shrikes !)
         //else if (%lastClass $= "WheeledVehicleData" && %hasLOS)
         //{
            //blah, blah, blah;
         //}
      }
      //------------------------------------------------------- if we don't have a target lets see if we can't find one
      else if (%task.shouldAttack <= 0)
      {
         //------------------------------------------------ search for a new vehicle to attack... ---

         %losTimeout = $AIClientMinLOSTime + ($AIClientLOSTimeout * %client.getSkillLevel());
         %result = AIFindClosestEnemyPilot(%client, 400, %losTimeout);
         %pilot = getWord(%result, 0);
         %pilotDist = getWord(%result, 1);

         //------------------------------------------------------------- getClosest enemy Vehicle ---
         //should we engage enemy vehicles?
         %result = aiFindClosestEnemyVehicle(%client);
         %closestEV = getWord(%result, 0);
         %closestEVDist = getWord(%result, 1);    

         //check for LOS
         if (%closestEV > 0 && isObject(%closestEV))
         {
            %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
            %vehicleLOS = !containerRayCast(%myPos, %closestEV.getWorldBoxCenter(), %mask, 0);
         }
         else
            %vehicleLOS = -1;

         //--------------------------------------------------------------------- check for targets ---

         if (%task.mode !$= "FlagDive")
         {
            %result = findClosestEnemyTarget(%client, %vehicle);//new function above, also tied to functions in ai.cs file
            %clTarg = getWord(%result,0);
            %clTargDist = getWord(%result, 1);
         }

         //-------------------------------------------------------------- else look for players on the ground ---

         %result = AIFindClosestEnemyToLoc(%client, %altPos, 250, $AIClientLOSTimeout, false, false);//- we don't look as far for players
         %closestEnemy = getWord(%result, 0);
         %closestEnemydist = getWord(%result, 1);

         //--------------------------------------------------------------------------------------------------- set the newly found target here ---
         if (%closestEnemy > 0 && %vehicleLOS)// we check los the fast way here, not through "AIFindClosestEnemy" above
         {
            %task.shouldAttack = %closestEnemy.player;
            %task.searchPos = %task.shouldAttack.getWorldBoxCenter();//need to UP ^ the Z LEVEL HERE ***
            %task.searchTime = getSimTime();
         }
         else if (%closestEV > 0 && %closestEVDist <= 400 && %vehicleLOS)
         {
            //if we got a respawned vehicle just sitting there ignor it            
            if (%closestEV.marker > 0 && VectorDist(%closestEV.position, %closestEV.marker.position) < 5)
            //if (%closestEV.respawnTime !$= "" && VectorDist(%closestEV.position, %closestEV.marker.position) < 5)
            {
               //error("we got a marker here so skip it");  
            }
            else
            {
               %task.shouldAttack = %closestEV;
               %task.searchPos = %task.shouldAttack.getWorldBoxCenter();//need to UP ^ the Z LEVEL HERE ***
               %task.searchTime = getSimTime();
            }
         }
         //--- else set turrets/sensors as targets (sometimes we get caught behind a hill)
         else if (%clTarg > 0 && %clTargDist < 400 && %vehicleLOS)
         {
            %task.shouldAttack = %clTarg;
            %task.searchPos = %task.shouldAttack.getWorldBoxCenter();//need to UP ^ the Z LEVEL HERE ***
            %task.searchTime = getSimTime();
         }
         else if (AIClientIsAlive(%pilot) && %pilotDist < 400)//- else set Pilot as targets (use default find enemy pilots here)
         {       
            %task.shouldAttack = %pilot.vehicleMounted;
            %task.searchPos = %task.shouldAttack.getWorldBoxCenter();//--------------------- (need to UP ^ the Z LEVEL HERE ***)
            %task.searchTime = getSimTime();
         }
         //else attack our last attacker even if friendly :)
         else if (isObject(%lastAttacker) && (%lastAttacker.team != %client.team || $teamDamage))
         {
            %newTargPos = %lastAttacker.getWorldBoxCenter();
            %newTargDist = vectorDist(%myPos, %newTargPos);
            if (%newTargDist < 400)
            {
               %task.shouldAttack = %lastAttacker;
               %task.searchPos = %task.shouldAttack.getWorldBoxCenter();//need to UP ^ the Z LEVEL HERE ***
               %task.searchTime = getSimTime();
            }
         }
         //--------------------------------------------- reset the searchPos and searchTime for safety (should not be needed though)
         else
         {
            %task.shouldAttack = -1;
            %task.searchTime = -1;
            %task.searchPos = -1;
         }                  
      }
   }

   //------------------------------- could add weight here to fight regularly if we are not mounted yet - Lagg... (*** someday ***)

   //update the task weight... this is the most important part - calls the monitor below 
   if (%task == %client.objectiveTask)
      %task.baseWeight = %client.objectiveWeight;

   //let the monitor decide when to quit :)//--- NEEDED SO IMPORTANT !
   %task.setWeight(%task.baseWeight);
}

//-------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------- AIPilotShrike::monitor ---
//-------------------------------------------------------------------------------------------------------------------------
function AIPilotShrike::monitor(%task, %client)
{
   //first, buy equipment
   if (%client.needEquipment)
   {
      %task.setMonitorFreq(5);
      if (%task.equipment !$= "")
         %equipmentList = %task.equipment;
      else
         %equipmentList = %task.desiredEquipment;
      %result = AIBuyInventory(%client, %equipmentList, %task.buyEquipmentSet, %task.buyInvTime);
      if (%result $= "InProgress")
         return;
      else if (%result $= "Finished")
      {
         %task.setMonitorFreq(%task.freq);
	 %client.needEquipment = false;
      }
      else if (%result $= "Failed")
      {
         //no inventories, heavies can't buy...
         if (%client.player.getArmorSize() $= "Heavy")
         {
            AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
            return;
         }
         else
         {
            %task.setMonitorFreq(%task.freq);
            %client.needEquipment = false;
         }
      }
   }
   //if we made it past the inventory buying, reset the inv time
   %task.buyInvTime = getSimTime();

   //chat - send the message
   if (%task.sendMsg)
   {
      if (%task.sendMsgTime == 0)
         %task.sendMsgTime = getSimTime();
      else if (getSimTime() - %task.sendMsgTime > 7000)
      {
         %task.sendMsg = false;
         if (%client.isAIControlled())
	 {
	    if (%task.chat !$= "")
	    {
	       %chatMsg = getWord(%task.chat, 0);
	       %chatTemplate = getWord(%task.chat, 1);
	       if (%chatTemplate !$= "")
		  AIMessageThreadTemplate(%chatTemplate, %chatMsg, %client, -1);
	       else
		  AIMessageThread(%task.chat, %client, -1);
	    }
         }
      }
   }

   if (%client.needVehicle)
   {
      %clVs = AIFindClosestVStation(%client, %task.location);
      if (%clVs > 0)
      {
         %closestVs = getWord(%clVs, 0);
         %closestVsDist = getWord(%clVs, 1);
      }

      if (%closestVs > 0 && %closestVsDist < 15 && !isObject(%client.player.mVehicle))
      {
         //If we're in light or medium armor, buy the vehicle - Lagg...
         if (%client.player.getArmorSize() !$= "Heavy")
         {        
            %task.setMonitorFreq(9);
            %buyResult = aiBuyVehicle(ScoutFlyer, %client, %closestVs);
         }
         else
         {
            //if ai in heavy armor buy equipment
            if (%task == %client.objectiveTask)
	    {
               %task.baseWeight = %client.objectiveWeight;//- set so can be bumped off objective by aiReassessment fudge
               %task.equipment = "Light Plasma PlasmaAmmo";
	       %task.buyEquipmentSet = "LightCloakSet";
               %client.needEquipment = true;
               return;
	    }
         }
         //- need to add if attacked, Defend yourself or Lower weight so can get bumped and go get em,
         //or set %task.shouldAttack client.lastDamagedClient! (maybe)? ***  
         if (%buyResult $= "InProgress")
	    return;

         else if (%buyResult $= "Finished")
         {
            //if we are finished buying the vehicle, then we are done
            //this is handled in function Armor::AIonMount - Lagg... 7-9-2004
         }
         else if (%buyResult $= "Failed")
         {
            error("AIPilotShrike::monitor - %buyResult failed - " @ getTaggedString(%client.name));
         
            //if this task is the objective task, choose a new objective
	    AIUnassignClient(%client);
	    Game.AIChooseGameObjective(%client);
            return;
         }
      }
      else if ((%closestVs <= 0 || %closestVsDist >= 300) && !isObject(%client.player.mVehicle))
      {
         error("AIPilotShrike::monitor - no vpad or to far " @ getTaggedString(%client.name));

         AIUnassignClient(%client);
	 Game.AIChooseGameObjective(%client);
         return;
      }
   }

   // ------------------------------------------------------------------------ if we managed to get in vehicle then go ---
   if (%client.player.isMounted() && isObject(%client.player.mVehicle))//--------------- are we mounted ???
   {
      //------------------------------------------------------------------------------- set monitor frequency ---
      %task.setMonitorFreq(%task.freq);//set frequency according to skill - Lagg... 4-11-2005

      //------------------------------------------------------------------------- set the variables that we will use ---
      %vehicle = %Client.vehicleMounted;//--------------------- %vehicle *
      %vehData = %vehicle.getDataBlock();// ------------------- %vehData *
      %myZLev = getWord(%vehicle.getTransform(), 2);//--------- %myZLev *
      %mySpd = vectorLen(%vehicle.getVelocity());//------------ %mySpd *
      %skill = %client.getSkillLevel();// --------------------- %skill *      
      %collision = aiCollisionCheck(%vehicle, %mySpd);// ------ %collision *

      //-------------------------------------------------------------------- make sure we got in the correct vehicle ---
      if (%vehicle.getDataBlock().getName() !$= "ScoutFlyer")
      {
         error("AIPilotShrike::monitor - opps we got in wrong vehicle");
         
         AIDisembarkVehicle(%client);
         return;
      }
      //-------------------------------------------------------------------------------------- get our altitude ---
      //find out our altitude 
      %myPos = %vehicle.getWorldBoxCenter();//-------------------------------- %myPos *
      %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType |
        $TypeMasks::ForceFieldObjectType;
      %downVec = getWord(%myPos, 0) @ " " @ getWord(%myPos, 1) @ "0";
      %altSur = containerRayCast(%myPos, %downVec, %mask, 0);
      %altPos = posfromRayCast(%altSur);

      %myAlt = getWord(%myPos, 2) - getWord(%altPos, 2);// -------------------- %myAlt *
      %terHt = getTerrainHeight(%myPos);// ------------------------------------ %terHt *

      %vx = getWord(%myPos, 0);// ------------------------------------------------ %vx *
      %vy = getWord(%myPos, 1);// ------------------------------------------------ %vy *
      //have this already above in %myZLev but still used below
      %vz = getWord(%myPos, 2);// ------------------------------------------------ %vz * 
      %vR = getWords(%vehicle.getTransform(), 3, 6);// --------------------------- %vR *

      //------------------------------------------------------------------------------------------- Avoidance (Stuck) ---
      if (%mySpd < 0.1)
      { 
         InitContainerRadiusSearch(%myPos, 7, $TypeMasks::InteriorObjectType | $TypeMasks::VehicleObjectType
           | $TypeMasks::TerrainObjectType | $TypeMasks::TSStaticShapeObjectType);
         %avoid = containerSearchNext();

         if (%avoid = %vehicle)
            %avoid = containerSearchNext();

         //if we are moving slow and close to objects we are probably stuck
         if (%avoid)
         {
            %tempx = getWord(%vehicle.getWorldBoxCenter(), 0);
            %tempy = getWord(%vehicle.getWorldBoxCenter(), 1);
            %tempz = getWord(%vehicle.getWorldBoxCenter(), 2);
            %tempz += 1.0;
            %tempR = getWords(%vehicle.getTransform(), 3, 6);

            %client.setPilotDestination(%task.location, 1.0);//max speed
            %vehicle.setTransform(%tempx SPC %tempy SPC %tempz SPC %tempR);//--- set the Transform Here up a little
         }
      }

      //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ HERE !
      //if we are locked by a missile, don't check for collision just - RUN!
      if (%task.run > 0)
      {
         //error("AIPilotShrike::monitor - RUN - mySpeed = " @ %mySpd);

         //checkLOSPoint(%pt, %pt1)
         %hasLOS = checkLOSPoint(%myPos, %task.run.position);

         %client.setControlObject(%vehicle);//------ give pilot control of shrike
         %client.shrikeFire = false;//-------------- turn off the gun

         if (%collision)
             %client.setPilotDestination(%task.run.position, 1.0);
         else if (%hasLOS && !%collision)
         {
            %client.setPilotDestination(%task.run.position, 1.0);
            applyKick(%vehicle, "foward", (%skill * 10), true);//------------------ HIT THE GAS!
         } 
         //if we cannot see the run spot nose up and GO!
         else
         {
            %trx = getWord(%task.run.position, 0);
            %try = getWord(%task.run.position, 1);
            %trz = getWord(%task.run.position, 2);

            //%client.setPilotAim(%trx SPC %try SPC %trz + 100);
            %client.setPilotDestination(%trx SPC %try SPC (%trz + 100), 1.0);
         }             
      }                             

      //------------------------------------------------------------- Task Should Attack (do we have a target ?) ---
      else if (isObject(%task.shouldAttack) && %task.shouldAttack > 0)
      {
         //------------------------------------------------------------------ get some target data here ---
         //------------------------------------------------------------------------------------------------

         %targDB = %task.shouldAttack.getDataBlock();//------------------------------- %targDB *
         %targClass = %targDB.getClassName();//--------------------------------------- %targClass *
         %type = %targDB.getName();//------------------------------------------------- %type *
         %targPos = %task.shouldAttack.getWorldBoxCenter();// ------------------------ %targPos *
         %targDist = vectorDist(%myPos, %task.shouldAttack.getWorldBoxCenter());//---- %targDist *
         %targZLev = getWord(%task.shouldAttack.getTransform(), 2);//----------------- %targZLev *
         %targSpd = vectorLen(%task.shouldAttack.getVelocity());//-------------------- %targSpd *
         %compZLev = %myZLev - %targZLev;//------------------------------------------- %compZLev *
         %tx = firstWord(%task.shouldAttack.getTransform());// ----------------------- %tx *
         %ty = getWord(%task.shouldAttack.getTransform(), 1);// ---------------------- %ty *
         %tz = getWord(%task.shouldAttack.getTransform(), 2);// ---------------------- %tz *

         //check for LOS ------------------------------------------------------------------------------ check LOS
         %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
         %hasLOS = !containerRayCast(%vx SPC %vy SPC %vz - 1, %targPos, %mask, 0);//------- %hasLOS *

         if (%hasLOS)
         {
            //---------------------------------------------------------------local %varibles only if has LOS - %firePos
            %x = getWord(%vehicle.getTransform(), 0);// ------------------------- my %x vec
            %y = getWord(%vehicle.getTransform(), 1);// ------------------------- my %y vec
            //%z = getWord(%task.shouldAttack.getTransform(), 2);// ----------- target %z vec
            %z = %tz;// ----------------------------------------------------- target %z vec

            //------------------------------------------------------------- set low to kill players
            if (%targClass $= "PlayerData")
            {
               if (%z < %terHt + 5)
                  %z = %terHt + 5;
               else
                  //%z += 0.7;
                  %z += 1.0;
            }
            //------------------------------------------------------------- set firePos a little higher for land lovers
            else if (%targClass $= "TurretData" || %targClass $= "StaticShapeData" || %targClass $= "HoverVehicleData")
            {
               if (%z < %terHt)
                  %z = %terHt + 25.0;
               else
                  %z += 25.0;
            }

            //set the firing position
            %task.firePos = %vx SPC %vy SPC %z;//------------------------------------------------------- %task.firePos * 
            %z = %tz;//getWord(%task.shouldAttack.getTransform(), 2);
            if (%targClass $= "WheeledVehicleData" || %targClass $= "HoverVehicleData" || %targClass $= "PlayerData")
               %z += mFloor(getRandom(2, 5) * 5);//add some altitude to search for targets on the ground
            %task.searchPos = %tx SPC %ty SPC %z;//---------------------------------------------------- %task.searchPos *
            %task.searchTime = getSimTime();
         }
         else
         {
            //---------------------------------------------------------------- search for targets for 35 sec only ---
            if (getSimTime() - %task.searchTime > 35000)
            {
               %task.shouldAttack = -1;
               %task.searchTime = -1;
               return;
            }
         }

         // -------------------------------------------------------------------------------------------------------------
         //This part sets the vehicle transform to aim at targets, it has to aim differently for each target type.
         //Thanks goes to Martin Hoover(founder) for his help with the mathamatics...

         //sub the two positions so we get a vector pointing from the origin in the direction we want our object to face
         if (%targClass $= "PlayerData" || %targClass $= "TurretData" || %targClass $= "StaticShapeData" ||
           %targClass $= "FlyingVehicleData" || (%targClass $= "HoverVehicleData" && %type $= "ScoutVehicle"))
         {
            //use the x-axis of the players rotation as the desired aiming point,
	    //and calculate a point 1.1 - 1.3 m behind the trigger point - Lagg...

            //(we may be to the side of target, if so we need another variable)//***************************************
            %randK = mFloor(getRandom(0, 9));
            if (%randK < 5)
               %stan = "1 0 0";
            else
               %stan = "0 1 0";

	    //%aprchDirection = MatrixMulVector("0 0 0 " @ getWords(%task.shouldAttack.getTransform(), 3, 6), "1 0 0");
            %aprchDirection = MatrixMulVector("0 0 0 " @ getWords(%task.shouldAttack.getTransform(), 3, 6), %stan);
	    %aprchDirection = VectorNormalize(%aprchDirection);

            //alternate fire so we can hit turrets, sensors, players and solar panels            
            if (%task.swch == 1)
            {
               %task.swch = 2;
               %factor = 2.0;//2.0m to the side of target
            }
            else
            {
               %task.swch = 1;
               %factor = -2.0;//2.0m to the other side of target
            }
            %aprchFromLocation = VectorAdd(%task.shouldAttack.getWorldBoxCenter(),VectorScale(%aprchDirection, %factor));
            %ax = getWord(%aprchFromLocation, 0);
            %ay = getWord(%aprchFromLocation, 1);
            %az = getWord(%aprchFromLocation, 2);


            if (%targClass $= "PlayerData")
               %vec = VectorSub(%aprchFromLocation, %myPos);
            else
               //%vec = VectorSub(%ax SPC %ay SPC %az + 2, %myPos);// ------ up 2 so he hits turrets and solar panels
               %vec = VectorSub(%ax SPC %ay SPC %az + getRandom(0.5, 2.5), %myPos);// --- just spead shots up and down to hit turrets/solar panels
         }
         else if (%targClass $= "HoverVehicleData" || %targClass $= "WheeledVehicleData")
            %vec = VectorSub(%tx SPC %ty SPC %tz + 2.5, %myPos);
         else 
            //%vec = VectorSub(%task.shouldAttack.getWorldBoxCenter(), %myPos);
            // --- up the target point by 1 to counter offset of shrike weapons for big targets only
            %vec = VectorSub(%tx SPC %ty SPC %tz + 1, %myPos);
            

         // pull the values out of the vector
         %x = firstWord(%vec);
         %y = getWord(%vec, 1);
         %z = getWord(%vec, 2);

         %ourTrans = getWords(%vehicle.getTransform(), 3, 6);
         %ourxRot = firstWord(%ourTrans);
         %ourYRot = getWord(%ourTrans, 1);
         %ourzRot = getWord(%ourTrans, 2);
         %ourwRot = getWord(%ourTrans, 3);

         //this finds the distance from origin to our point
         %len = vectorLen(%vec);

         //---------XY-----------------
         //given the rise and length of our vector this will give us the angle in radians
         %rotAngleXY = mATan( %z, %len );

         //---------Z-----------------
         //get the angle for the z axis
         %rotAngleZ = mATan( %x, %y );

         //create 2 matrices, one for the z rotation, the other for the x rotation
         %matrix = MatrixCreateFromEuler("0 0" SPC %rotAngleZ * -1);
         %matrix2 = MatrixCreateFromEuler(%rotAngleXY SPC "0 0");

         //now multiply them together so we end up with the rotation we want
         %finalMat = MatrixMultiply(%matrix, %matrix2);

         //were done, send the proper numbers back
         %xRot = getWord(%finalMat, 3);
         %yRot = getWord(%finalMat, 4);
         %zRot = getWord(%finalMat, 5);
         %wRot = getWord(%finalMat, 6);

         //------------------------------------------------- see if we are facing the enemy target (thanks Martin Hoover "Founder") ---

         //Figuring out if the enemy is in front of the bot shouldn�t be too difficult,
         //just need to compare the bots current rotation to the rotation required to point at the target...

         %vehicleRot = getWords(%vehicle.getTransform(), 3, 6);
         //(since you have T2, you have a function rotFromTransForm() )

         %targetRot = %xRot SPC %yRot SPC %zRot SPC %wRot;//---results of pointToPos;

         // rotation = x y z angle

         //Since an object can have a rotaion described with negative radians you need to multiply
         //the z and angle components to ensure you get the proper compass tick the rotation is specifying.

         %vehicleZangle = getWord(%vehicleRot, 2) * getWord(%vehicleRot, 3);

         //get the angle the bot would have to rotate to to face the target...

         %TargetZAngle = %zRot * %wRot;// --- getWord(%targetRot, 2) * getWord(%targetRot, 3);

         //Now you can simply compare %botZangle and %TargetZAngle to see how similar the angles are.
         //(the angles are in radians, if you want to see them in degrees then do a %degAngle = mRadToDeg(%botZangle) )

         %compare = %vehicleZangle - %TargetZAngle;

         //-------------------------------------------------------------------------------------------------------------
         //since we have 6 different target classes and each one has to be dealt with differently depending upon certain
         //parameters. We will have to check for each different class and in each class we can check for parameters.
         //-------------------------------------------------------------------------------------------------------------

         //--------------------------------------------------------------------------------------------------------------
         //---------------------------------------------------------------------------- TURRET/STATICSHAPE DATA CLASS ---
         //--------------------------------------------------------------------------------------------------------------

         //dealing with turrets can be dangerous, we must keep a safe distance,
         //sensors and solar panels will be somewhat easier to handle...

         if (%targClass $= "TurretData" || %targClass $= "StaticShapeData")
         {
            //--- get some target data ---
            // --------- is turret enabled
            if (%targClass $= "TurretData" && %task.shouldAttack.isEnabled() && %task.shouldAttack.isPowered())
               %safeRange = %task.shouldAttack.getMountedImage(0).attackRadius;// --- set the safeRange
            else
               %safeRange = 50;// --- safe range for sensors/solarPanel

            // ------------------------------------------------ (can't hit a small target at to far a range)
            if (%terHt < %targZLev && %targDist > %safeRange && %targDist < 300 && (%myAlt > 5 || %mySpd < 10))
            {

               //everything good, open fire
               %close = vectorDist(%myPos,  %task.firePos);
               if (%myZLev >= %targZLev && %compZLev < 35.0 && (%targDist / %compZLev > 1.5) &&
                 %compare < 0.65 && %compare > -0.65 && %hasLOS && %myZLev > %trHt + 3 && %close < 5)
               {
                  %client.setControlObject(%client.player);// --- take away pilot's control
                  //%vehicle.setTransform(%myPos SPC %xRot SPC %yRot SPC %zRot SPC %wRot);// --- set the Transform Here
                  // --- set the Transform Here (CHEAT HERE, works better !)
                  %vehicle.setTransform(%task.firePos SPC %xRot SPC %yRot SPC %zRot SPC %wRot);
                  //%vehicle.setVelocity("0 0 0");// --- stop him if we can, so we can kill (has no effect) :(
                  %client.shrikeFire = true;// --- turn ON the Guns
               }
               //else move him into firing position
               else if (%hasLos && checkLOSPoint(%task.firePos, %targPos))// ---------------- We have LOS to Target ---
               {
                  %client.shrikeFire = false;// ---------------------------------- turn off the guns
                  %client.setControlObject(%vehicle);// -------------------------- give pilot control of shrike

                  if (VectorDist(%myPos, %task.firePos) < 8)
                  {
                     if (%myZLev < %targZLev + 1)//set a little higher if we are too low
                     {
                        %vx = getWord(%myPos, 0);
                        %vy = getWord(%myPos, 1);
                        %vz = getWord(%myPos, 2);
                        %vz += 8.0;
                        
                        %client.setPilotPitchRange(-0.025, 0.025, 0.025);// -------- a smooth setting here
                        %client.setPilotDestination(%vx SPC %vy SPC %vz, 0.5);// --- move UP a little, slowly

                        // -- set the Transform Here up a little (not good looking, don't do this)
                        //%vehicle.setTransform(%vx SPC %vy SPC %vz SPC %vR);
                     }
                     else
                        %client.setPilotAim(%aprchFromLocation);// --------------------------------- aim at targetPoint
                  }
                  else
                  {
                     if (%myZLev > (%targZLev + 25) && %targZLev > (%terHt + 10))
                        applyKick(%vehicle, "down", (%skill * 2));
                     else
                     {
                        %client.setPilotPitchRange(-0.025, 0.025, 0.025);// -------- a smooth setting here
                        %client.setPilotDestination(%task.firePos, 1.0);// ----------------- move to the %firePos
                     }
                  }
               }
               //or just forget this target and move along
               else// ----------------------------------------------------------------------- We DO NOT have LOS to Target ---
               {
                  %task.shouldAttack = -1;// ------------------------------------ clear the target so we can aquire a new one
                  %client.shrikeFire = false;// --------------------------------- turn off the guns
                  %client.setControlObject(%vehicle);// ------------------------- give pilot control of shrike

                  //-------------------------------------------------------------- if we can see our target move along flight path
                  if (VectorDist(%myPos, %task.location) < 50)               
	          {
                     //----------------------------------------------------------- if we have another location index
                     if ((%task.count - 1) == %task.locationIndex)
                     {
                        AIDisembarkVehicle(%client); // --------- Hop off...
                        %client.stepMove(%task.location, 0.25);
                     }
                     else//------------------------------------------------------- or go back to last marker, but keep searching
                     {
                        %task.locationIndex++;
                        %task.location = %task.group.getObject(%task.locationIndex).position;
                     }
                  }                  
                  else
                  {

                     //checkLOSPoint(%pt, %pt1)
                     %hasLOS = checkLOSPoint(%myPos, %task.location);

                     //%mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
                     //%hasLOS = !containerRayCast(%myPos, %task.location, %mask, 0);//------- %hasLOS to %task.location ?

                     %client.setPilotPitchRange(-0.025, 0.025, 0.025);// -------- a smooth setting here

                     if (%hasLOS)
                        %client.setPilotDestination(%task.location, 1.0);//max speed
                     else
                        %client.setPilotDestination(%vx SPC %vy SPC %vz + 10, 1.0);// -- if we can't see %task.location move straight up ^ 
                  }
               }
            }
            //else we are to close to safley attack or to far to be effective so MOVE IT ! /// --- *** review this ***
            else
            {
               %client.shrikeFire = false;// ----------------------------------- turn off the guns
               %client.setControlObject(%vehicle);// --------------------------- give pilot control of shrike

               //if we have another location index
               if (VectorDist(%myPos, %task.location) < 50)               
	       {
                  if ((%task.count - 1) == %task.locationIndex)
                  {
                     AIDisembarkVehicle(%client); //--------- Hop off...
                     %client.stepMove(%task.location, 0.25);
                  }
                  else
                  {
                     %task.locationIndex++;
                     %task.location = %task.group.getObject(%task.locationIndex).position;
                  }
               }
               else
               {
                  %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
                  %hasLOS = !containerRayCast(%myPos, %task.location, %mask, 0);//------- %hasLOS to %task.location ?

                  %client.setPilotPitchRange(-0.025, 0.025, 0.025);// -------- a smooth setting here

                  if (%hasLOS)
                     %client.setPilotDestination(%task.location, 1.0);//max speed
                  else
                     %client.setPilotDestination(%vx SPC %vy SPC %vz + 10, 1.0);// -- if we can't see %task.location move straight up ^ 
               }
            }
         }
         //-----------------------------------------------------------------------------------------------------
         //------------------------------------------------------------------------------- PLAYER DATA CLASS ---
         //--------------------------------------------------------------------------------------------

         //- Players we will want to always move toward them and maybe even run them down,
         //- if we lose LOS to target we will want to search for them ---

         else if (%targClass $= "PlayerData" && isObject(%task.shouldAttack))
         {
            %client.setControlObject(%vehicle);//--- give pilot control of shrike and leave it there ---

            //--- get some target data ---

	    %pos2D = getWord(%myPos, 0) SPC getWord(%myPos, 1) SPC "0";
	    %dest2D = getWord(%task.shouldAttack.getWorldBoxCenter(), 0) SPC
              getWord(%task.shouldAttack.getWorldBoxCenter(), 1) SPC "0";

            //check for obsticles near target      ------------------------ check for obsticles near target
            //- look above target for obsticles (interiors, vehicles, FF , trees?)
            %mask = $TypeMasks::InteriorObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::ForceFieldObjectType;
            %noObs = !containerRayCast(%targPos, %tx SPC %ty SPC %tz + 25, %mask, 0);
              
            %mask = $TypeMasks::StaticShapeObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::TurretObjectType |
              $TypeMasks::ForceFieldObjectType | $TypeMasks::TSStaticShapeObjectType;
            InitContainerRadiusSearch(%targPos, 7.0, %mask);
            %obs = containerSearchNext();// != 0;
            if (%obs == %vehicle)
               %obs = containerSearchNext();// != 0;       

            //---------------- find out if we are above or below target... ---
	    if (VectorDist(%pos2D, %dest2D) < 45 && (%compZLev > 8.0 || %compZLev < -8.0))
            {
               %client.shrikeFire = false;//-------------- turn off the gun

               //if we don't have another location index back up
               if (VectorDist(%myPos, %task.location) < 50)               
	       {
                  if ((%task.count - 1) == %task.locationIndex)
                     %task.locationIndex--;
                  else
                     %task.locationIndex++;
               }

               %task.location = %task.group.getObject(%task.locationIndex).position;

               %client.setPilotPitchRange(-0.05, 0.025, 0.025);//------ set pitch range for flying
               %client.setPilotDestination(%task.location, 1.0);
            }
            else if (%hasLOS)//----------------------------------------------------------------- see if we can run em down or should just shoot ---
            {
               if (%compare < 0.65 && %compare > -0.65 && %compZLev < 30.0 && %compZLev > -15.0)
               {
                  if (%noObs && !%obs && !%collision)// -------------------------- if no obsticles try to run em down
                  {
                     %client.setPilotPitchRange(-0.05, 0.025, 0.025);// ---------- set pitch range for fire
                     %client.setPilotDestination(%aprchFromLocation, 1.0);
                     %vehicle.setTransform(%myPos SPC %xRot SPC %yRot SPC %zRot SPC %wRot);// --- set the Transform Here
                  }
                  else// --------------------------------------------------------- if we have obsticles just move to aim
                  {
                     %vehicle.setTransform(%myPos SPC %xRot SPC %yRot SPC %zRot SPC %wRot);// --- set the Transform Here
                     %client.setPilotAim(%aprchFromLocation);// --- aim at targetPoint
                  }

                  %client.shrikeFire = true;// ------------------------------------------------------ turn on the guns
               }
               else
               {
                  %client.shrikeFire = false;//------------------------------------------ turn off the gun
                  %client.setPilotPitchRange(-0.05, 0.025, 0.025);//--------------------- set pitch range for flying

                  if (%noObs && !%obs && !%collision)// || %targDist > 450)//----------- if no obsticles try to run em down
                     %client.setPilotDestination(%targPos, 1.0);//------------------------------------------------ move to the target
                  else
                  {
                     if (VectorDist(%myPos, %task.firePos) < 7.0)
                     {
                        if (%myZLev < %targZLev)//set a little higher if we are too low
                        {
                           %vx = getWord(%myPos, 0);
                           %vy = getWord(%myPos, 1);
                           %vz = getWord(%myPos, 2);
                           %vz += 8.0;

                           %client.setPilotDestination(%vx SPC %vy SPC %tz + 1, 1.0);//------------------------------ move UP a little
                        }
                        else
                           %client.setPilotAim(%aprchFromLocation);//------------------------------------------------ aim at targetPoint
                     }
                     else
                        %client.setPilotDestination(%task.firePos, 1.0);//------------------------------------------- move to %task.firePos
                  }
               }
            }
            else//--- else we can't see the target so start searching for him
            {
               if (%compZLev < 1)//we know where you are, lets adjust if we are lower
               {
                  %x = getWord(%vehicle.getTransform(), 0);//------------------------- my %x vec
                  %y = getWord(%vehicle.getTransform(), 1);//------------------------- my %y vec
                  %client.setPilotPitchRange(-0.025, 0.025, 0.025);//------ set pitch range for flying (smooth)             
                  %client.setPilotDestination(%x SPC %y SPC %vz + 10, 1.0);//----------------------- set speed a little slower ---
               }
               else//else we are searching
               {
                  %client.shrikeFire = false;// ---------------------------- turn off the gun
                  %client.setPilotPitchRange(-0.025, 0.025, 0.025);// ------ set pitch range for flying (smooth)            
                  %client.setPilotDestination(%task.searchPos, 0.7);// --------- set speed a little slower ---
               }
            }
         }

         //----------------------------------------------------------------------------------------------------------- 
         //--------------------------------------------------------------------------- DELOYED WHEEL VEHICLE CLASS ---
         //-----------------------------------------------------------------------------------------------------------

         else if (isObject(%task.shouldAttack) && %targClass $= "WheeledVehicleData" && %task.shouldAttack.deployed)
         {
            if (%hasLOS)// ------------------------------------------------------------ Sec 1 --- Setup Data ---
            {
               // ------------------------------------------------ %firePos ---
               %x = getWord(%vehicle.getTransform(), 0);
               %y = getWord(%vehicle.getTransform(), 1);
               %z = getWord(%task.shouldAttack.getTransform(), 2);
               %c = %z + 3.0;// --- do not go lower than targZLevel ---
               
               if (%z < %terHt)
                  %z = %terHt + (%targDist / 10);// ------- the height we need to fire for what range to target ---
               else
                  %z += (%targDist / 10);
               if (%z < %c)
                  %z = %c;

               %task.firePos = %x SPC %y SPC %z;//------------------------------ %task.firePos --- 

               %tx = firstWord(%task.shouldAttack.getTransform());
               %ty = getWord(%task.shouldAttack.getTransform(), 1);
               %tz = getWord(%task.shouldAttack.getTransform(), 2);
               %tz += mFloor(getRandom(2, 5) * 5);//add some altitude to search for targets on the ground

               %task.searchPos = VectorAdd(%tx SPC %ty SPC %tz, "0 0 5");// -- 5 more ^ ---
               %task.searchTime = getSimTime();
            }
            
            %safeRange = %task.shouldAttack.turret.getMountedImage(0).attackRadius;//---------- set the safeRange

            %pos2D = getWord(%myPos, 0) SPC getWord(%myPos, 1) SPC "0";
	    %dest2D = getWord(%task.shouldAttack.getWorldBoxCenter(), 0) SPC
              getWord(%task.shouldAttack.getWorldBoxCenter(), 1) SPC "0";

            //---------------- find out if we are above or below target... ------------------------- Sec 2 ---
            // ------------------------ Don't fly into danger zone ---

            //--- we don't want to fly over a deployed MPB if we know it is there ---
            if (VectorDist(%pos2D, %dest2D) < (%compZLev * 1.25))
            {
               %client.shrikeFire = false;//-------------- turn off the gun

               //if we don't have another location index give up
               if (VectorDist(%myPos, %task.location) < 50)               
	            {
                  if ((%task.count - 1) == %task.locationIndex)
                  {
                     AIDisembarkVehicle(%client); //--------- Hop off...
                     %client.stepMove(%task.location, 0.25);
                     //return;
                  }
                  else//else move to the next path marker
                     %task.locationIndex++;
               }

               %task.location = %task.group.getObject(%task.locationIndex).position;

               %client.setPilotPitchRange(-0.05, 0.025, 0.025);//------ set pitch range for flying
               %client.setPilotDestination(%task.location, 1.0);
            }
                        //-------------------------------------------------------------------------- Sec 3 --- Safe Kill ---
            else if (%targDist > %safeRange)
            {
               %close = vectorDist(%myPos,  %task.firePos);
               if (%myZLev >= %targZLev && (%targDist / %compZLev > 1.5) && %compare < 0.65 && %compare > -0.65 && %hasLOS && %myZLev > %trHt + 3 && %close < 5)
               {
                  %client.setControlObject(%client.player);//---------------------------------------------------------------- take away pilot's control
                  //%vehicle.setTransform(%myPos SPC %xRot SPC %yRot SPC %zRot SPC %wRot);//--------------------- set the Transform Here - TEST HERE
                  %vehicle.setTransform(%task.firePos SPC %xRot SPC %yRot SPC %zRot SPC %wRot);//------ set the Transform Here (CHEAT HERE, works better)
                  %client.shrikeFire = true;// ----------------------------------------------------------------------------------- turn ON the Guns
               }
               else if (%hasLos && checkLOSPoint(%task.firePos, %targPos))
               {
                  %client.shrikeFire = false;//-------------- turn off the gun
                  %client.setControlObject(%vehicle);//------ give pilot control of shrike

                  if (VectorDist(%myPos, %task.firePos) < 8.0)
                  {
                     if (%myZLev < %targZLev)// --- set a little higher if we are too low ---
                     {
                        %vx = getWord(%myPos, 0);
                        %vy = getWord(%myPos, 1);
                        %vz = getWord(%myPos, 2);
                        %vz += 25.0;
                        %client.setPilotDestination(%vx SPC %vy SPC %vz, 0.5);//--- move UP a little
                     }
                     else//else just aim at target
                        %client.setPilotAim(%task.shouldAttack.getWorldBoxCenter());//--- aim at targetPoint
                  }
                  else//move into firing position
                     %client.setPilotDestination(%task.firePos, 0.5);
               }
               else
               {
                  %task.shouldAttack = -1;//------------ clear the target so we can aquire a new one
                  %client.shrikeFire = false;//-------------- turn off the guns
                  %client.setControlObject(%vehicle);//------ give pilot control of shrike

                  //if we have another location index
                  if (VectorDist(%myPos, %task.location) < 50)               
	          {
                     if ((%task.count - 1) == %task.locationIndex)
                     {
                        AIDisembarkVehicle(%client); //--------- Hop off...
                        %client.stepMove(%task.location, 0.25);
                        //return;
                     }
                     else
                     {
                        %task.locationIndex++;
                        %task.location = %task.group.getObject(%task.locationIndex).position;
                     }
                  }                  
                  else//searching
                     %client.setPilotDestination(%task.searchPos, 0.5);
               }
            }
            else if (%targDist > 15.0 && %targDist <= %safeRange)//--------------------------- we are to close to safley attack so MOVE TO IT !
            //----------------------------------------------------------------------------------------- Sec 4 --- Move in for a close kill ---
            {
               %client.shrikeFire = false;//-------------- turn off the gun
               %client.setControlObject(%vehicle);//------ give pilot control of shrike
               %x = getWord(%vehicle.getTransform(), 0);
               %y = getWord(%vehicle.getTransform(), 1);
               %z = getWord(%task.shouldAttack.getTransform(), 2);
               if (%z < %terHt)
                  %z = %terHt + 6.0;
               else
                  %z += 2.0;
               %task.firePos = %x SPC %y SPC %z;//------------------------------------------------------- %task.firePos --- 

               if (%myZLev >= %targZLev && (%targDist / %compZLev > 0.75) && %compare < 0.65 && %compare > -0.65 && %hasLOS &&
                 %myZLev > %trHt +3 && %targDist > 40.0)
               {
                  if (%collision)
                     Stop(%vehicle, %mySpd, 10);// ------------------------------------------------------------------------ new function to STOP x 10

                  %client.setPilotDestination(%task.shouldAttack.getWorldBoxCenter(), 0.5);
                  %client.setControlObject(%client.player);//------------------------------------------------------- take away pilot's control
                  %vehicle.setTransform(%myPos SPC %xRot SPC %yRot SPC %zRot SPC %wRot);//---------------------------- set the Transform Here
                  %client.shrikeFire = true;//---------------------------------------------------------------------------- turn on the guns
               }

               else if (%myZLev >= %targZLev && (%targDist / %compZLev > 0.75) && %compare < 0.65 && %compare > -0.65 && %hasLOS &&
                 %myZLev > %trHt + 3 && %targDist < 40.0)
               {
                  if (%mySpd > 5)// ---------------------------------------------------------------- STOP if we are flying in for a close kill
                     Stop(%vehicle, %mySpd, 10);//------------------------------------------------------- new function to STOP x 10
                     
                  %client.setControlObject(%client.player);//-------------------------------- take away pilot's control
                  //%vehicle.setTransform(%myPos SPC %xRot SPC %yRot SPC %zRot SPC %wRot);//------------------------ set the Transform Here
                  %vehicle.setTransform(%task.firePos SPC %xRot SPC %yRot SPC %zRot SPC %wRot);//------------------- set the Transform Here (CHEAT HERE)
                  %client.shrikeFire = true;//----------------------------------------------------- turn on the guns
               }

               else if (%targDist < 40)
               {
                  if (%mySpd > 5)// ---------------------------------------------------------- STOP if we are flying in for a close kill
                     Stop(%vehicle, %mySpd, 10);// -------------------------------------------------------- new function to STOP x 10
                     
                  %client.setPilotDestination(%myPos, 0.0);
                  %client.setControlObject(%vehicle);//---------------------------------------------- give pilot control of shrike
                  %client.setPilotAim(%task.shouldAttack.getWorldBoxCenter());
                  %client.shrikeFire = false;//-------------- turn off the gun
               }

               //if we have another location index
               else if (VectorDist(%myPos, %task.location) < 50)               
	       {
                  %client.setControlObject(%vehicle);//------ give pilot control of shrike
                  %client.shrikeFire = false;//-------------- turn off the gun

                  if ((%task.count - 1) == %task.locationIndex)
                  {
                     AIDisembarkVehicle(%client); //--------- Hop off...
                     %client.stepMove(%task.location, 0.25);
                     //return;
                  }
                  else
                  {
                     %task.locationIndex++;
                     %task.location = %task.group.getObject(%task.locationIndex).position;
                  }
               }
               else//else just move towards the target
                  %client.setPilotDestination(%task.shouldAttack.getWorldBoxCenter(), 1.0);
            }
            else//--- we are to close to safley attack so MOVE IT ! --------------------------------------------- Sec 5 --- breaking off ---
            {
               %client.shrikeFire = false;//-------------- turn off the gun
               %client.setControlObject(%vehicle);//------ give pilot control of shrike

               //if we have another location index
               if (VectorDist(%myPos, %task.location) < 50)               
	       {
                  if ((%task.count - 1) == %task.locationIndex)
                  {
                     AIDisembarkVehicle(%client); //--------- Hop off...
                     %client.stepMove(%task.location, 0.25);
                     //return;
                  }
                  else
                  {
                     %task.locationIndex++;
                     %task.location = %task.group.getObject(%task.locationIndex).position;
                  }
               }
               else
               {
                  %client.setPilotPitchRange(-0.05, 0.025, 0.025);//------ set pitch range for flying
                  %client.setPilotDestination(%task.location, 1.0);
               }
            }
         }
         //---------------------------------------------------------------------------------------------------------------
         //----------------------------------------------------------- NON DEPLOYED WHEELED / HOVER VEHICLE DATA CLASS ---
         //---------------------------------------------------------------------------------------------------------------
         else if ((%targClass $= "HoverVehicleData" || %targClass $= "WheeledVehicleData") && isObject(%task.shouldAttack))
         {
            %client.setControlObject(%vehicle);//-------------------- give pilot control of shrike and leave it there ---

	    %pos2D = getWord(%myPos, 0) SPC getWord(%myPos, 1) SPC "0";
	    %dest2D = getWord(%task.shouldAttack.getWorldBoxCenter(), 0) SPC
              getWord(%task.shouldAttack.getWorldBoxCenter(), 1) SPC "0";

            //if we are above or below target just move along...
	    if (VectorDist(%pos2D, %dest2D) < 25 && (%compZLev > 7.0 || %compZLev < -7.0))
            {
               %client.shrikeFire = false;//-------------- turn off the guns

               //if we don't have another location index back up
               if (VectorDist(%myPos, %task.location) < 50)               
	       {
                  if ((%task.count - 1) == %task.locationIndex)
                     %task.locationIndex--;
                  else
                     %task.locationIndex++;
               }

               %task.location = %task.group.getObject(%task.locationIndex).position;

               %client.setPilotPitchRange(-0.025, 0.025, 0.025);//------ set pitch range for flying
               %client.setPilotDestination(%task.location, 1.0);
            }
            //--- close the distance target may have missile stuffs ---
            //else if (%hasLOS && %targDist > 35 && ((%mySpd > 15 && %targDist > 100) || %mySpd < 15))
            else if (%hasLOS && %targDist > 35 && ((%mySpd > 15 && %targDist > 50) || %mySpd < 15))
            {
               if (%compare < 0.65 && %compare > -0.65 && %compZLev < 55.0 && %compZLev > -35.0)
               {
                  //sendEcho(%client, "NON DEPLOYED WHEELED / HOVER VEHICLE DATA CLASS - here is the problem we are not chasing");

                  %client.setControlObject(%client.player);//-------------------------------- take away pilot's control
                  %vehicle.setTransform(%myPos SPC %xRot SPC %yRot SPC %zRot SPC %wRot);//----- set the Transform Here
                  if (%collision)
                     Stop(%vehicle, %mySpd, 10);// --------------------------------- new function to STOP x 10
                  else
                     applyKick(%vehicle, "foward", (%skill * 6), true);//----------------------- give chase (NEW *)

                  %client.shrikeFire = true;//-------------------------- turn on the guns
                  
               }
               else
               {
                  %client.setControlObject(%vehicle);//------ give pilot control of shrike

                  %client.shrikeFire = false;//-------------- turn off the gun
                  %client.setPilotPitchRange(-0.05, 0.025, 0.025);//------ set pitch range for flying
                  
                  if (%trHt < getWord(%task.firePos, 2))//if the terrain height is lower than firepos
                     %client.setPilotDestination(%task.shouldAttack.getWorldBoxCenter(), 1.0);//--- move to the target
                  else
                     %client.setPilotDestination(%task.searchPos, 1.0);//--- move to search

               }
            }
            else if (%hasLos)
            {
               %client.shrikeFire = false;//-------------- turn off the gun
               %client.setControlObject(%vehicle);//------ give pilot control of shrike

               if (VectorDist(%myPos, %task.firePos) < 15.0)
               {
                  if (%myZLev < %targZLev + 5)//set a little higher if we are too low
                  {
                     %vx = getWord(%myPos, 0);
                     %vy = getWord(%myPos, 1);
                     %vz = getWord(%myPos, 2);
                     %vz += 20.0;
                       
                     %client.setPilotDestination(%vx SPC %vy SPC %vz, 1.0);//--- move UP a little
                  }
                  else
                  {
                     %client.setPilotAim(%task.shouldAttack.getWorldBoxCenter());//--- aim at targetPoint
                     %vehicle.setTransform(%myPos SPC %xRot SPC %yRot SPC %zRot SPC %wRot);//-- set the Transform Here
                  }
               }
               else
                  %client.setPilotDestination(%task.firePos, 1.0);
            }
            else
            {
               %client.setControlObject(%vehicle);//---- give pilot control of shrike
               %client.shrikeFire = false;//-------------- turn off the gun
               %client.setPilotPitchRange(-0.05, 0.025, 0.025);//------ set pitch range for flying             
               %client.setPilotDestination(%task.searchPos, 1.0);               
            }
         }
         //--------------------------------------------------------------------------------------------------------
         //-------------------------------------------------------------------------- FLYING VEHICLE DATA CLASS ---
         //--------------------------------------------------------------------------------------------------------

         // ------------------------------------------------------------------------------------------ sec 1 ---
         // ------------------------------------------------------- this sec just updates the task locations ---

         else if (%targClass $= "FlyingVehicleData" && isObject(%task.shouldAttack))
         {
            %client.setControlObject(%vehicle);//----------- give pilot control of shrike and leave it there ---

	    %pos2D = getWord(%myPos, 0) SPC getWord(%myPos, 1) SPC "0";
	    %dest2D = getWord(%task.shouldAttack.getWorldBoxCenter(), 0) SPC
              getWord(%task.shouldAttack.getWorldBoxCenter(), 1) SPC "0";

            //---------------- find out if we are above or below target... ---
	    if (VectorDist(%pos2D, %dest2D) < 45 && (%compZLev > 7.0 || %compZLev < -7.0))
            {
               %client.shrikeFire = false;//-------------- turn off the gun

               //if we don't have another location index back up
               if (VectorDist(%myPos, %task.location) < 50)               
	       {
                  if ((%task.count - 1) == %task.locationIndex)
                     %task.locationIndex--;
                  else
                     %task.locationIndex++;
               }

               %task.location = %task.group.getObject(%task.locationIndex).position;
            }
            //------------------------------------------------------------------------------------------ sec 2 ---
            // ------------------------------------------------------------------ this sec makes LOS decisions ---

            if (%hasLOS)
            {
               //----------------------------------------------------------------------------- check directions
               //use the Y-axis of the vehicle rotation as the desired direction to check
               //and calculate a point 4m in front of vehicle - Lagg...
               %muzzle = MatrixMulVector("0 0 0 " @ getWords(%task.shouldAttack.getTransform(), 3, 6), "0 1 0");
               %muzzle = VectorNormalize(%muzzle);
               //%factor = 4; //4m in front of vehicle is the right spot to start
               %muzzlePoint = VectorAdd(%targPos,VectorScale(%muzzle, 4));
               %tarNoseDis = VectorDist(%muzzlePoint, %myPos);

               //------------------------------------------------------------------------------------------ sec 3 ---
               // ------------------------- No Collision ------------- this sec does the shooting and dogfighting ---

               if (%compare < 0.65 && %compare > -0.65 && %compZLev < 35.0 && %compZLev > -35.0)
               {
                  
                  %client.setPilotPitchRange(-0.05, 0.025, 0.025);// --- set pitch range for flying (smooth)

                  //if (!%collision && %targDist > 125)// --- if no collision move to target
                  if (!%collision && %targDist > 50)
                  {
                     %vehicle.setTransform(%myPos SPC %xRot SPC %yRot SPC %zRot SPC %wRot);// --- set the Transform Here
                     %client.shrikeFire = true;// --- turn on the guns

                     //if (%targDist > 150 && %mySpd < (%vehData.maxForwardSpeed * 3))// --- applyImpluse Foward               < * TEST THIS ALL * >
                     //if (%tarNoseDis > %targDist && %targDist > 150 && %mySpd < (%vehData.maxForwardSpeed * 3))// ---- applyImpluse Foward 
                     if (%tarNoseDis > %targDist && %targDist > 50 && %mySpd < %targSpd)// ---- applyImpluse Foward
                     {
                        %client.setPilotDestination(%targPos, 1.0);// ------------------- keep moving to target

                           applyKick(%vehicle, "foward", (%skill * 12), true);//------------------ give chase
                     }
                     else// ----------------------- if the target is not moving away from us just aim at him (NEW)*
                        %client.setPilotAim(%targPos);

                     // ------------------------------- if targ is pointing at us us we should evade (Dogfighting) ---
                     //if (%targDist >= 25 && %targDist < 75)
                     if (%targDist >= 25 && %targDist < 125 && %tarNoseDis < %targDist && %targSpd > 1 &&
                       %targSpd < 50 && %task.shouldAttack.getMountNodeObject(0) > 0)//need pilot variable %hasPilot (OK)
                     {
                        %mv = getRandom(0, 2);
                        if (%mv < 0.4 && %myZLev < (nameToId("MissionArea").flightCeiling - 20))
                        {
                           applyKick(%vehicle, "up", %skill + 2.5);
                           //sendEcho("sec 3 - Apply RANDOM ^ UP ^ KICK 10 false");
                        }
                        else if (%mv < 0.8)
                        {
                           applyKick(%vehicle, "left", %skill + 7, true);
                           //sendEcho(%client, "sec 3 - Apply RANDOM < LEFT < KICK 15 true");
                        }
                        else if (%mv < 1.2)
                        {
                           applyKick(%vehicle, "right", %skill + 7, true);
                           //sendEcho(%client, "sec 3 - Apply RANDOM > RIGHT > KICK 15 true");
                        }
                        else if (%mv < 1.6 && %myAlt > 100)//*************************** TESTING HERE ********
                        {
                           applyKick(%vehicle, "Down", %skill + 4, true);
                           //sendEcho(%client, "sec 3 - Apply RANDOM !!! Apply DOWN  KICK !!!!!! 5 true");
                        }

                        %client.setPilotAim(%targPos);// -------------------------------- TESTING NEW ***
                     }
                     else if (%targDist < 25)
                        %client.setPilotDestination(%task.location, 1.0);
              
                        
                  }
                  //------------------------------------------------------------------------------------ sec 3a ---
                  // -------------------------- this sec does the stopping and collision avoidance --- Shoot = true

                  else
                  {
                     //---------- set the Transform Here
                     %vehicle.setTransform(%myPos SPC %xRot SPC %yRot SPC %zRot SPC wRot);

                     if (%mySpd < 35)//-------------------- if we are going slow just move to fire position
                        %client.setPilotDestination(%task.firePos, 1.0);

                     %client.setPilotAim(%task.shouldAttack.getWorldBoxCenter());//--- aim at targetPoint
                     if (%collision)//----------------- if we are going to crash (into target) stop !
                     {
                        Stop(%vehicle, %mySpd, 12);// ----------- new function to STOP x 10
                        
                     }
                  }

                  %client.shrikeFire = true;// ------------------------------------------------- turn on the guns
               }

               //----------------------------------------------------------------------------------------- sec 4 ---
               //------------------------------------------- this sec helps move us into a good position to fire ---

               else
               {
                  %client.shrikeFire = false;//-------------- turn off the gun
                  %client.setPilotPitchRange(-0.05, 0.05, 0.025);//------ set pitch range for attacking
                  %client.setPilotDestination(%task.shouldAttack.getWorldBoxCenter(), 1.0);//--- move to the target
                  
                  if (%myZLev > (%targZLev + 10) && %targZLev > (%terHt + 10))
                  {
                     //applyKick(%vehicle, "down", (%skill * 5));old

                     applyKick(%vehicle, "down", %skill);
                     //sendEcho(%client, "sec 4 - FLYING VEHICLE DATA CLASS - We are higher than target Move Down - " @ (%skill * 5) @ " false");
                  }
                  if (%myZLev < (%targZLev - 10) && %targZLev > (%terHt + 10))
                  {
                     //applyKick(%vehicle, "up", 10, true);old

                     applyKick(%vehicle, "up", %skill);
                     //sendEcho(%client, "sec 4 - FLYING VEHICLE DATA CLASS - We are LOWER than target Move ^UP - 10 true");
                  }
               }
            }

            //------------------------------------------------------------------------------------------- sec 5 ---
            //------------------------------------------------ this sec handles if we do not have LOS to target ---

            else
            {
               //sendEcho(%client, "sec 5 - WE DO NOT HAVE LOS To TARGET");
               %client.shrikeFire = false;//-------------- turn off the gun

               %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType |
                 $TypeMasks::TSStaticShapeObjectType;
               %hasLOS = !containerRayCast(%myPos, %task.searchLocation, %mask, 0);//------- %hasLOS to search location

               %client.setPilotPitchRange(-0.025, 0.025, 0.025);// -------- a smooth setting here

               if (%hasLOS)
               {
                  //sendEcho(%client, "sec 5 - FLYING VEHICLE DATA CLASS - move to %task.location - we HAVE los to search location");
                  %client.setPilotDestination(%task.searchLocation, 1.0);//max speed
               }
               else
               {
                  //sendEcho(%client, "sec 5 - FLYING VEHICLE DATA CLASS - MOVE ^UP - we DO NOT have los to search location");
                  %client.setPilotDestination(%vx SPC %vy SPC %vz + 10, 1.0);//max speed
               }               
            }
         }         
        //-------------------------------------------------------------------------------- End Of Target Data Classes ---
      }

      //-----------------------------------------------------------------------------------------------------------------
      //-------------------------------------------------------------------------------------- Following Path Markers ---
      else 
      {
         %task.shouldAttack = -1;//-------------------------- set task no target
         %client.setControlObject(%vehicle);//--------------- give pilot control
         %client.shrikeFire = false;//----------------------- turn off the guns

         //-------------------------------------------------- set the locations
         %task.location = %task.group.getObject(%task.locationIndex).position;
         %speed = %task.group.getObject(%task.locationIndex).speed;
         if (%speed $= "")
            %speed = 1.0;

         //are we close to location index marker?
         //use our speed or 75m (which ever is less) to see how close we are the location marker
	 if (VectorDist(%myPos, %task.location) < (%mySpd || 75))
	 {
	    //if we have another location index
            if ((%task.count - 1) > %task.locationIndex)//has to be -1 on the %task.group count
               %task.locationIndex++;//------------------------------------- set the destination to the next marker
               
	    else//---------------------------------------------------------- else we are at end of trail
            {
               if (%task == %client.objectiveTask)
	       {                  
                  if (%client.vehicleMounted )
                  {
                      //echo("AIPilotShrike::monitor - end of trail - hopping out !!!");

                      AIDisembarkVehicle(%client); //---------------------------------------- Hop off...
                      %client.stepMove(%task.location, 0.25);
                      //AIUnassignClient(%client);
                      //Game.AIChooseGameObjective(%client);
                      //return;
                  }
	       }
            }
         }
         else//---------------------------------------------------------------- else we follow the flight path
         {
            %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
            %hasLOS = !containerRayCast(%myPos, %task.location, %mask, 0);//------- %hasLOS

            if (%hasLOS)//--------------------------------------------------- if we can see the next destination
            {
               //help this cowboy stay more or less on the path
               if (%myZLev < (getWord(%task.location, 2) - 75) && %myZLev < (nameToId("MissionArea").flightCeiling - 20))
               {
                  %client.setPilotPitchRange(-0.005, 0.005, 0.005);// -------- a smooth setting here
                  %client.setPilotDestination(%vx SPC %vy SPC %vz + 10, 1.0);//--- fly straight up a little
               }
               //else if (%myZLev < (getWord(%task.location, 2) - 25) && %myZLev < (nameToId("MissionArea").flightCeiling - 20))
               //{
                  //%client.setPilotPitchRange(-0.005, 0.004, 0.005);// ----------------------- nose it up a lot
                  ////applyKick(%vehicle, "up", (%skill * 2));
                  ////sendEcho(%client, "nose it up a lot !");

                  //applyKick(%vehicle, "up", (%skill + 1));
               //}
               else if (%myZLev < (getWord(%task.location, 2) - 15) && %myZLev < (nameToId("MissionArea").flightCeiling - 20))
               {
                  %client.setPilotPitchRange(-0.005, 0.004, 0.005);// ----------------------- nose it up a little
                  applyKick(%vehicle, "up", (%skill + 0.5));
                  //sendEcho(%client, "nose it up a little !");

                  //applyKick(%vehicle, "up", (%skill + 1));
               }
               else if (%myZLev > (getWord(%task.location, 2) + 25) && !%collision)
               {
                  %client.setPilotPitchRange(-0.005, 0.005, 0.005);// ----------------------- nose it down a little
                  applyKick(%vehicle, "down", (%skill / 2));
                  //sendEcho(%client, "nose it down a little !");

                  //applyKick(%vehicle, "down", (%skill + 0.5));//was 0.5
               }
               else
               {
                  //sendEcho(%client, "fly normally !");              
                  %client.setPilotPitchRange(-0.005, 0.0045, 0.004);// -------------------- a real smooth setting here
               }

               %client.setPilotDestination(%task.location, %speed);// --------------------- move to the destination speed
            }
            // ----------------------------------------- else LOS is blocked to the next destination
            else
            {
               if (%myZLev < (nameToId("MissionArea").flightCeiling - 20))// ---------- don't fly up off the map               
               {
                  %client.setPilotPitchRange(-0.005, 0.0045, 0.005);// -------- a smooth setting here
                  %client.setPilotDestination(%vx SPC %vy SPC %vz + 10, 1.0);// --- fly straight up a little
                  applyKick(%vehicle, "up", (%skill + 2));// ------------------------ help him by skill level
               }
               // ------------------------------------------------------- else just bounce off things like a retard
               else
               {
                  %client.setPilotPitchRange(-0.005, 0.0045, 0.005);// -------- a smooth setting here
                  %client.setPilotDestination(%task.location, 1.0);// -------- max speed
               }              
            }
         }
         
      //%client.setPilotPitchRange(-0.2, 0.05, 0.05);//Dynamix Original Here sucks. max seems to be -0.05, 0.05, 0.05
      }
   }
   //else task must be completed
   else if (%task == %client.objectiveTask)
   {
      AIUnassignClient(%client);
      Game.AIChooseGameObjective(%client);
      return;
   }
}// ------------------------------------------------------------------------------------------------------------ End of AIPilotShrike - Lagg...

// ------------------------------------------------------------------ function send echo
function sendEcho(%client, %mes)
{
   //just use team 1 to test scripts (faster this way)
   if (%client.team == Game.defenseTeam)
      echo("DefTeam - " @ %mes);
}

//used to check if firing postion has los to target
function checkLOSPoint(%pt, %pt1)
{
   //check for LOS
   %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TSStaticShapeObjectType;
   %LOS = !containerRayCast(%pt, %pt1, %mask, 0);

   if (%LOS)
      return true;
   else
      return false;
}

// ------------------------------------------------------------------ function stop - thanks Dev
function Stop(%vehicle, %mySpd, %times)
{
   %Position = %vehicle.getWorldBoxCenter();
   %Velocity = %vehicle.getVelocity();
   %Factor = %mySpd * 3;
   %ForceVector = VectorScale(VectorNormalize(%Velocity), -1 * %Factor);
   for(%i = 0; %i < %times; %i ++)
   {
      %vehicle.ApplyImpulse(%Position, %ForceVector);
   }
}
      
// ------------------------------------------------------------------- function findClosestEnemyTarget
function findClosestEnemyTarget(%client, %vehicle)
{
   //lets find some enemy targets (turrets, solar panels and sensors)
   %clTarg = -1;
   %clDist = 300;	//initialize so only targets within 300 m will be detected

   %tarCount = $AIVehTargSet.getCount();
   for (%i = 0; %i < %tarCount; %i++)
   {
      %pTarg = $AIVehTargSet.getObject(%i);//new $set in ai.cs
      if (%pTarg.team > 0 && %pTarg.team != %client.team)
      {
	 if (%pTarg.getDamageState() !$= "Destroyed")
	 {
            %clTargPos = %pTarg.getWorldBoxCenter();
	    %dist = vectorDist(%vehicle.position, %clTargPos);
	    if (%dist < %clDist)
	    {
               //check for LOS
               %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType |
                 $TypeMasks::StaticTSObjectType | $TypeMasks::ForceFieldObjectType;	    
               %vLOS = !containerRayCast(%vehicle.getWorldBoxCenter(), %clTargPos, %mask, 0);

               if (%vLOS)
               {
	          %clTarg = %pTarg;
                  %clDist = %dist;
               }
	    }
	 }
      }
   }

   return %clTarg @ " " @ %clDist;
}

// ------------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------- some help from Dev to set up the $TypeMasks::
$DamageableMask = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType |
                  $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType |
                  $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType |
                  $TypeMasks::MoveableObjectType | $TypeMasks::DamagableItemObjectType;

$EverythingMask = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType |
                  $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType |
                  $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType |
                  $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType |
                  $TypeMasks::ForceFieldObjectType | $TypeMasks::StaticObjectType |
                  $TypeMasks::MoveableObjectType | $TypeMasks::DamagableItemObjectType;

//-------------------------------------------------------------------------------------------------------------------------
// new function for ai pilots to check for collisions
function aiCollisionCheck(%vehicle, %speed)
{
   %vehPos = %vehicle.getWorldBoxCenter();

   //use the Y-axis of the vehicle rotation as the desired direction to check for collisions
   %muzzle = MatrixMulVector("0 0 0 " @ getWords(%vehicle.getTransform(), 3, 6), "0 1 0");
   %muzzle = VectorNormalize(%muzzle);
   %point = %muzzle;
   %factor = 4; //4m in front of vehicle is the right spot to start                        
   %muzzlePoint = VectorAdd(%vehPos,VectorScale(%muzzle, %factor));
   %rangePoint = VectorAdd(%vehPos,VectorScale(%point, %speed * 5));

   //error("%muzzlePoint = " @ %muzzlePoint);
   //error("%rangePoint = " @ %rangePoint);

   %masks = $TypeMasks::TerrainObjectType |
              $TypeMasks::InteriorObjectType |
              $TypeMasks::ForceFieldObjectType |
              $TypeMasks::StaticObjectType |
              $TypeMasks::MoveableObjectType |
              $TypeMasks::DamagableItemObjectType |
              $TypeMasks::VehicleObjectType |
              $TypeMasks::StaticTSObjectType;



   %collision = ContainerRayCast(%muzzlePoint, %rangePoint, %masks, 0);

   if (%collision)
      return true;

   return false;
}









