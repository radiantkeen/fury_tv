//--------------------------------------------------------------------------
// Damage Types v2.0
//--------------------------------------------------------------------------
// Death Message String Table
$DamageTypeCount = 0;

// More compact way of dealing with death messages
$DamageType::Default = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'default';
$DamageTypeCount++;

/////////////////////////////////////////////////////////////////////////////////////////////////
// %1 = Victim's name                                                                          //
// %2 = Victim's gender (value will be either "him" or "her")                                  //
// %3 = Victim's possessive gender	(value will be either "his" or "her")                      //
// %4 = Killer's name                                                                          //
// %5 = Killer's gender (value will be either "him" or "her")                                  //
// %6 = Killer's possessive gender (value will be either "his" or "her")                       //
// %7 = implement that killed the victim (value is the object number of the bullet, disc, etc) //
/////////////////////////////////////////////////////////////////////////////////////////////////

$DeathMessageCampingCount = 1;
$DeathMessageCamping[0] = '\c0%1 was killed for camping near the Nexus.';

 //Out of Bounds deaths
$DeathMessageOOBCount = 1;
$DeathMessageOOB[0] = '\c0%1 was killed for loitering outside the mission area.';

$DeathMessageLavaCount = 6;
$DeathMessageLava[0] = '\c0%1\'s last thought before falling into the lava : \'Oops\'.';
$DeathMessageLava[1] = '\c0%1 makes the supreme sacrifice to the lava gods.';
$DeathMessageLava[2] = '\c0%1 looks surprised by the lava - but only briefly.';
$DeathMessageLava[3] = '\c0%1 wimps out by jumping into the lava and trying to make it look like an accident.';
$DeathMessageLava[4] = '\c0%1 wanted to experience death by lava first hand.';
$DeathMessageLava[5] = '\c0%1 dives face first into the molten kool-aid... oh wait.';

$DeathMessageLightningCount = 3;
$DeathMessageLightning[0] = '\c0%1 was killed by lightning!';
$DeathMessageLightning[1] = '\c0%1 caught a lightning bolt!';
$DeathMessageLightning[2] = '\c0%1 stuck %3 finger in Mother Nature\'s light socket.';

//these used when a player presses ctrl-k
$DeathMessageSuicideCount = 8;
$DeathMessageSuicide[0] = '\c0%1 blows %3 own head off!';
$DeathMessageSuicide[1] = '\c0%1 ends it all. Cue violin music.';
$DeathMessageSuicide[2] = '\c0%1 kills %2self.';
$DeathMessageSuicide[3] = '\c0%1 goes for the quick and dirty respawn.';
$DeathMessageSuicide[4] = '\c0%1 self-destructs in a fit of ennui.';
$DeathMessageSuicide[5] = '\c0%1 shows off %3 mad dying skills!';
$DeathMessageSuicide[6] = '\c0%1 wanted to make sure %3 gun was loaded.';
$DeathMessageSuicide[7] = '\c0%1 was last heard shouting "death before dishonor!!".';

$DeathMessageLSSuicideCount = 4;
$DeathMessageLSSuicide[0] = '\c0%1 couldn\'t get to a station in time!';
$DeathMessageLSSuicide[1] = '\c0%1 dies from internal organ failure.';
$DeathMessageLSSuicide[2] = '\c0%1\'s life support system lets %3 down.';
$DeathMessageLSSuicide[3] = '\c0%1 shuffles off %3 mortal coil.';

$DeathMessageVehPadCount = 1;
$DeathMessageVehPad[0] = '\c0%1 got caught in a vehicle\'s spawn field.';

$DeathMessageFFPowerupCount = 1;
$DeathMessageFFPowerup[0] = '\c0%1 got caught up in a forcefield during power up.';

$DeathMessageRogueMineCount = 1;
$DeathMessageRogueMine[$DamageType::Mine, 0] = '\c0%1 is all mine.';

//These used when a player is run over by a vehicle
$DeathMessageVehicleCount = 5;
$DeathMessageVehicle[0] = '\c0%4 says to %1: "Hey! You scratched my paint job!".';
$DeathMessageVehicle[1] = '\c0%1 acquires that run-down feeling from %4.';
$DeathMessageVehicle[2] = '\c0%4 shows %1 %6 new ride.';
$DeathMessageVehicle[3] = '\c0%1 makes a painfully close examination of %4\'s front bumper.';
$DeathMessageVehicle[4] = '\c0%1\'s messy death leaves a mark on %4\'s vehicle finish.';

$DeathMessageVehicleFriendlyCount = 3;
$DeathMessageVehicleFriendly[0] = '\c0%1 gets in the way of a friendly vehicle.';
$DeathMessageVehicleFriendly[1] = '\c0Sadly, a friendly vehicle turns %1 into roadkill.';
$DeathMessageVehicleFriendly[2] = '\c0%1 becomes an unsightly ornament on a team vehicle\'s hood.';

$DeathMessageVehicleUnmannedCount = 3;
$DeathMessageVehicleUnmanned[0] = '\c0%1 gets in the way of a runaway vehicle.';
$DeathMessageVehicleUnmanned[1] = '\c0An unmanned vehicle kills the pathetic %1.';
$DeathMessageVehicleUnmanned[2] = '\c0%1 is struck down by an empty vehicle.';

/////////////////////////////////////////////////////////////////////////////////////////////////
// %1 = Victim's Vehicle Name                                                                  //
// %2 = Victim's Controller Name                                                               //
// %3 = Killer's Vehicle Name                                                                  //
// %4 = Killer's name                                                                          //
/////////////////////////////////////////////////////////////////////////////////////////////////

// Pilot Vehicle Kills
$DeathMessagePilotCount = 4;
$DeathMessagePilot[0] = '\c0The %3 piloted by %4 valiantly guns down %2\'s %1".';
$DeathMessagePilot[1] = '\c0%4\'s %3 goes in for the kill against The %1 piloted by %2.';
$DeathMessagePilot[2] = '\c0The %3 has shot down The %1.';
$DeathMessagePilot[3] = '\c0%4 piloting The %3 perfectly strikes The %1, wrecking it into pieces.';

// Gunner Vehicle Kills
$DeathMessageGunnerCount = 3;
$DeathMessageGunner[0] = '\c0%4 gunning for The %3 shoots down %2\'s %1".';
$DeathMessageGunner[1] = '\c0%3\'s turrets bring down The %1.';
$DeathMessageGunner[2] = '\c0%4 takes aim at The %1 in a turret and blows them out of the sky.';

// Unmanned Turret Vehicle Kills
$DeathMessageAutoTurretCount = 3;
$DeathMessageAutoTurret[0] = '\c0%2 piloting The %1 gets caught with their pants down by an automatic turret.';
$DeathMessageAutoTurret[1] = '\c0The %1\'s crew suddenly realizes what all the pretty lights coming from the ground are.';
$DeathMessageAutoTurret[2] = '\c0%2 attempted to fly The %1 past a few enemy turrets, but was shot down.';

// Passenger Vehicle Kills
$DeathMessagePassengerCount = 3;
$DeathMessagePassenger[0] = '\c0Crewman %4 of The %3 shoots down enemy vehicle %1.';
$DeathMessagePassenger[1] = '\c0%4 stares down %2 from a distance before shooting down The %1.';
$DeathMessagePassenger[2] = '\c0%4, while riding aboard The %3 nails a critical point on The %1 and nails them to the ground.';

// Solo player killing a vehicle
$DeathMessageVehPlayerCount = 3;
$DeathMessageVehPlayer[0] = '\c0%4 fatally embarasses the crew of The %1 by shooting it down without a vehicle.';
$DeathMessageVehPlayer[1] = '\c0The crew of The %1 underestimated %4\'s gunnery abilities. Well who\'s laughing now?.';
$DeathMessageVehPlayer[2] = '\c0%4 hulk smashes The %1 out of the sky.';

// Unknown killer destroying a vehicle/nature
$DeathMessageAnonCount = 3;
$DeathMessageAnon[0] = '\c0The %1 comes up all explodey!';
$DeathMessageAnon[1] = '\c0The %1 suffers a mysterious reactor failure and explodes!';
$DeathMessageAnon[2] = '\c0Captain %2 of the %1 noticed the "check engine" light one second too late!';

//These used when a player is killed by a nearby equipment explosion
$DeathMessageExplosionCount = 3;
$DeathMessageExplosion[0] = '\c0%1 was killed by exploding equipment!';
$DeathMessageExplosion[1] = '\c0%1 stood a little too close to the action!';
$DeathMessageExplosion[2] = '\c0%1 learns how to be collateral damage.';

$DeathMessageTurretSelfKillCount = 3;
$DeathMessageTurretSelfKill[0] = '\c0%1 somehow kills %2self with a turret.';
$DeathMessageTurretSelfKill[1] = '\c0%1 apparently didn\'t know the turret was loaded.';
$DeathMessageTurretSelfKill[2] = '\c0%1 helps his team by killing himself with a turret.';

// Used when no other death messages are defined
$DefaultDeathMessageCount = 3;
$DefaultDeathMessage[0] = '\c0%4 kills %1.';
$DefaultDeathMessage[1] = '\c0%4 murders %1.';
$DefaultDeathMessage[2] = '\c0%4 smacks %1 down.';

//these used when a player kills himself (other than by using ctrl - k)
$DeathMessageSelfKillCount = 5;

//used when a player is killed by a teammate
$DeathMessageTeamKillCount = 1;

//used when a player is killed by a teammate controlling a turret
$DeathMessageCTurretTeamKillCount = 1;

//used when a player is killed by an uncontrolled, friendly turret
$DeathMessageCTurretAccdtlKillCount = 1;

//these messages for owned or controlled turrets and vehicles
$DeathMessageCTurretKillCount = 3;

//These used when an automated turret kills an  enemy player
$DeathMessageTurretKillCount = 3;

//these used when a player is killed by an enemy
$DeathMessageCount = 5;

//------------------------------------------------------------------------------
// Death message blocks
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Blaster
$DamageType::Blaster = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'blaster';

$DeathMessageSelfKill[$DamageType::Blaster, 0] = '\c0%1 kills %2self.';
$DeathMessageSelfKill[$DamageType::Blaster, 1] = '\c0%1 makes a note to watch out for ricochets.';
$DeathMessageSelfKill[$DamageType::Blaster, 2] = '\c0%1\'s weapon kills its hapless owner.';
$DeathMessageSelfKill[$DamageType::Blaster, 3] = '\c0%1 deftly guns %2self down.';
$DeathMessageSelfKill[$DamageType::Blaster, 4] = '\c0%1 has a fatal encounter with %3self.';

$DeathMessage[$DamageType::Blaster, 0] = '\c0%4 kills %1 with a disruptor.';
$DeathMessage[$DamageType::Blaster, 1] = '\c0%4 pings %1 to death.';
$DeathMessage[$DamageType::Blaster, 2] = '\c0%1 gets a pointer in disruptor use from %4.';
$DeathMessage[$DamageType::Blaster, 3] = '\c0%4 fatally embarrasses %1 with %6 handheld blaster.';
$DeathMessage[$DamageType::Blaster, 4] = '\c0%4 unleashes a terminal blaster barrage into %1.';

$DeathMessageTeamKill[$DamageType::Blaster, 0] = '\c0%4 TEAMKILLED %1 with a blaster!';
$DeathMessageCTurretTeamKill[$DamageType::Blaster, 0] = '\c0%4 TEAMKILLED %1 with a blaster turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Blaster, 0] = '\c0%1 got in the way of a friendly blaster turret!';

$DeathMessageCTurretKill[$DamageType::Blaster, 0] = '\c0%4 blows away %1 with a blaster turret.';
$DeathMessageCTurretKill[$DamageType::Blaster, 1] = '\c0%4 fries %1 with a blaster turret.';
$DeathMessageCTurretKill[$DamageType::Blaster, 2] = '\c0%4 lights up %1 with a blaster turret.';

$DeathMessageTurretKill[$DamageType::Blaster, 0] = '\c0%1 gets lit up by a blaster turret.';
$DeathMessageTurretKill[$DamageType::Blaster, 1] = '\c0%1 gets rained on by a nearby blaster turret.';
$DeathMessageTurretKill[$DamageType::Blaster, 2] = '\c0%1 finds %2self on the wrong end of a blaster turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Plasma
$DamageType::Plasma = $DamageTypeCount;
$DamageType::PlasmaTurret = $DamageTypeCount; // Legacy compatibility
$DamageTypeText[$DamageTypeCount] = 'Plasma';

$DeathMessageSelfKill[$DamageType::Plasma, 0] = '\c0%1 kills %2self with plasma.';
$DeathMessageSelfKill[$DamageType::Plasma, 1] = '\c0%1 turns %2self into plasma-charred briquettes.';
$DeathMessageSelfKill[$DamageType::Plasma, 2] = '\c0%1 swallows a white-hot mouthful of %3 own plasma.';
$DeathMessageSelfKill[$DamageType::Plasma, 3] = '\c0%1 immolates %2self.';
$DeathMessageSelfKill[$DamageType::Plasma, 4] = '\c0%1 experiences the joy of cooking %2self.';

$DeathMessage[$DamageType::Plasma, 0] = '\c0%4 roasts %1 with the plasma rifle.';
$DeathMessage[$DamageType::Plasma, 1] = '\c0%4 asks %1: "Need a light?"';
$DeathMessage[$DamageType::Plasma, 2] = '\c0%4 entices %1 to try a faceful of plasma.';
$DeathMessage[$DamageType::Plasma, 3] = '\c0%4 introduces %1 to the plasma immolation dance.';
$DeathMessage[$DamageType::Plasma, 4] = '\c0%4 places the Hot Kiss of Death on %1.';

$DeathMessageTeamKill[$DamageType::Plasma, 0] = '\c0%4 TEAMKILLED %1 with Plasma!';
$DeathMessageCTurretTeamKill[$DamageType::Plasma, 0] = '\c0%4 TEAMKILLED %1 with a Plasma turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Plasma, 0] = '\c0%1 got in the way of a friendly Plasma turret!';

$DeathMessageCTurretKill[$DamageType::Plasma, 0] = '\c0%4 converts %1 into a living bonfire.';
$DeathMessageCTurretKill[$DamageType::Plasma, 1] = '\c0%4 gave %1 an ashen look.';
$DeathMessageCTurretKill[$DamageType::Plasma, 2] = '\c0%4 burns and fries %1 with the power of Plasma.';

$DeathMessageTurretKill[$DamageType::Plasma, 0] = '\c0%1 is killed by a plasma turret.';
$DeathMessageTurretKill[$DamageType::Plasma, 1] = '\c0%1\'s body now marks the location of a plasma turret.';
$DeathMessageTurretKill[$DamageType::Plasma, 2] = '\c0%1 is fried by a plasma turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Vulcan
$DamageType::Bullet = $DamageTypeCount; // legacy
$DamageTypeText[$DamageTypeCount] = 'Chaingun';

$DeathMessageSelfKill[$DamageType::Bullet, 0] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::Bullet, 1] = '\c0%1 makes a note to watch out for ricochets.';
$DeathMessageSelfKill[$DamageType::Bullet, 2] = '\c0%1 manages to kill %2self with a reflected bullet.';
$DeathMessageSelfKill[$DamageType::Bullet, 3] = '\c0%1 deftly guns %2self down.';
$DeathMessageSelfKill[$DamageType::Bullet, 4] = '\c0%1 has a fatal encounter with %3self.';

$DeathMessage[$DamageType::Bullet, 0] = '\c0%4 rips %1 up with the chaingun.';
$DeathMessage[$DamageType::Bullet, 1] = '\c0%4 happily chews %1 into pieces with %6 chaingun.';
$DeathMessage[$DamageType::Bullet, 2] = '\c0%4 administers a dose of Vitamin Lead to %1.';
$DeathMessage[$DamageType::Bullet, 3] = '\c0%1 suffers a serious hosing from %4\'s chaingun.';
$DeathMessage[$DamageType::Bullet, 4] = '\c0%4 bestows the blessings of %6 chaingun on %1.';

$DeathMessageTeamKill[$DamageType::Bullet, 0] = '\c0%4 TEAMKILLED %1 with a chaingun!';
$DeathMessageCTurretTeamKill[$DamageType::Bullet, 0] = '\c0%4 TEAMKILLED %1 with a chaingun turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Bullet, 0] = '\c0%1 got in the way of a friendly chaingun turret!';

$DeathMessageCTurretKill[$DamageType::Bullet, 0] = '\c0%1 enjoys the rich, metallic taste of %4\'s chaingun bullet.';
$DeathMessageCTurretKill[$DamageType::Bullet, 1] = '\c0%4\'s chaingun turret plays sweet music all over %1.';
$DeathMessageCTurretKill[$DamageType::Bullet, 2] = '\c0%1 receives a stellar exit wound from %4\'s chaingun bullet.';

$DeathMessageTurretKill[$DamageType::Bullet, 0] = '\c0%1 gets chunked apart by a chaingun turret.';
$DeathMessageTurretKill[$DamageType::Bullet, 1] = '\c0%1 gets rained on by a nearby chaingun turret.';
$DeathMessageTurretKill[$DamageType::Bullet, 2] = '\c0%1 finds %2self on the wrong end of a chaingun turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Disc
$DamageType::Disc = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Disc';

$DeathMessageSelfKill[$DamageType::Disc, 0] = '\c0%1 kills %2self with a disc.';
$DeathMessageSelfKill[$DamageType::Disc, 1] = '\c0%1 catches %3 own disc.';
$DeathMessageSelfKill[$DamageType::Disc, 2] = '\c0%1 heroically falls on %3 own disc.';
$DeathMessageSelfKill[$DamageType::Disc, 3] = '\c0%1 helpfully jumps into %3 own disc\'s explosion.';
$DeathMessageSelfKill[$DamageType::Disc, 4] = '\c0%1 plays Russian roulette with %3 disc launcher.';

$DeathMessage[$DamageType::Disc, 0] = '\c0%4 demolishes %1 with the disc launcher.';
$DeathMessage[$DamageType::Disc, 1] = '\c0%4 serves %1 a blue plate special.';
$DeathMessage[$DamageType::Disc, 2] = '\c0%4 shares a little blue friend with %1.';
$DeathMessage[$DamageType::Disc, 3] = '\c0%4 plays skeet shoot with %1.';
$DeathMessage[$DamageType::Disc, 4] = '\c0%1 becomes one of %4\'s greatest hits.';

$DeathMessageTeamKill[$DamageType::Disc, 0] = '\c0%4 TEAMKILLED %1 with a Disc!';
$DeathMessageCTurretTeamKill[$DamageType::Disc, 0] = '\c0%4 TEAMKILLED %1 with a Disc turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Disc, 0] = '\c0%1 got in the way of a friendly Disc turret!';

$DeathMessageCTurretKill[$DamageType::Disc, 0] = '\c0%4 plays skeet shoot with %1.';
$DeathMessageCTurretKill[$DamageType::Disc, 1] = '\c0%4 serves %1 a blue plate special.';
$DeathMessageCTurretKill[$DamageType::Disc, 2] = '\c0%4 demolishes %1 with the disc launcher.';

$DeathMessageTurretKill[$DamageType::Disc, 0] = '\c0%1 is killed by a disc turret.';
$DeathMessageTurretKill[$DamageType::Disc, 1] = '\c0%1\'s body now marks the location of a disc turret.';
$DeathMessageTurretKill[$DamageType::Disc, 2] = '\c0%1 is blown away by a disc turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Grenade
$DamageType::Grenade = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Grenade';

$DeathMessageSelfKill[$DamageType::Grenade, 0] = '\c0%1 kills %2self with a HE grenade.';
$DeathMessageSelfKill[$DamageType::Grenade, 1] = '\c0%1 catches %3 own \'nade.';
$DeathMessageSelfKill[$DamageType::Grenade, 2] = '\c0%1\'s own grenade turns on %2.';
$DeathMessageSelfKill[$DamageType::Grenade, 3] = '\c0%1 helpfully jumps into %3 own grenade\'s explosion.';
$DeathMessageSelfKill[$DamageType::Grenade, 4] = '\c0%1 pulled the pin a shade early.';

$DeathMessage[$DamageType::Grenade, 0] = '\c0%4 eliminates %1 with a HE grenade.';
$DeathMessage[$DamageType::Grenade, 1] = '\c0%1 swallows %4\'s HE grenade and promptly explodes!';
$DeathMessage[$DamageType::Grenade, 2] = '\c0%1 gets annihilated by %4\'s HE grenade.';
$DeathMessage[$DamageType::Grenade, 3] = '\c0%1 receives a kaboom lesson from %4.';
$DeathMessage[$DamageType::Grenade, 4] = '\c0%4 turns %1 into grenade salad.';

$DeathMessageTeamKill[$DamageType::Grenade, 0] = '\c0%4 TEAMKILLED %1 with a Grenade!';
$DeathMessageCTurretTeamKill[$DamageType::Grenade, 0] = '\c0%4 TEAMKILLED %1 with a grenade launcher turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Grenade, 0] = '\c0%1 got in the way of a friendly grenade launcher turret!';

$DeathMessageCTurretKill[$DamageType::Grenade, 0] = '\c0%4 shoves a HE grenade down %1\'s throat.';
$DeathMessageCTurretKill[$DamageType::Grenade, 1] = '\c0%4 throws %1 a HE grenade and shouts "CATCH"!';
$DeathMessageCTurretKill[$DamageType::Grenade, 2] = '\c0%4 demolishes %1 a grenade launcher turret.';

$DeathMessageTurretKill[$DamageType::Grenade, 0] = '\c0%1 is killed by a flak turret.';
$DeathMessageTurretKill[$DamageType::Grenade, 1] = '\c0%1\'s body now marks the location of a flak turret.';
$DeathMessageTurretKill[$DamageType::Grenade, 2] = '\c0%1 is blown away by a flak turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Laser
$DamageType::Laser = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Laser';

$DeathMessageSelfKill[$DamageType::Laser, 0] = '\c0%1 amazingly bends spacetime to kill %2self with a Laser.';
$DeathMessageSelfKill[$DamageType::Laser, 1] = '\c0%1 amazingly bends spacetime to kill %2self with a Laser.';
$DeathMessageSelfKill[$DamageType::Laser, 2] = '\c0%1 amazingly bends spacetime to kill %2self with a Laser.';
$DeathMessageSelfKill[$DamageType::Laser, 3] = '\c0%1 amazingly bends spacetime to kill %2self with a Laser.';
$DeathMessageSelfKill[$DamageType::Laser, 4] = '\c0%1 amazingly bends spacetime to kill %2self with a Laser.';

$DeathMessage[$DamageType::Laser, 0] = '\c0%4 sets %1 ablaze with a well placed laser pulse.';
$DeathMessage[$DamageType::Laser, 1] = '\c0%4 picks off %1 with %6 laser.';
$DeathMessage[$DamageType::Laser, 2] = '\c0%4 holds %1 down under a magnifying glass under the sun.';
$DeathMessage[$DamageType::Laser, 3] = '\c0%1 finally realized what that red dot was.';
$DeathMessage[$DamageType::Laser, 4] = '\c0%4 plays laser light tag with %1.';

$DeathMessageTeamKill[$DamageType::Laser, 0] = '\c0%4 TEAMKILLED %1 with a Laser!';
$DeathMessageCTurretTeamKill[$DamageType::Laser, 0] = '\c0%4 TEAMKILLED %1 with a Laser turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Laser, 0] = '\c0%1 got in the way of a friendly Laser turret!';

$DeathMessageCTurretKill[$DamageType::Laser, 0] = '\c0%1 is instantly vaporized by %4\'s Laser.';
$DeathMessageCTurretKill[$DamageType::Laser, 1] = '\c0%4\'s deadly aim with the Laser singes %1.';
$DeathMessageCTurretKill[$DamageType::Laser, 2] = '\c0%4\'s Laser torches off %1\'s hair.';

$DeathMessageTurretKill[$DamageType::Laser, 0] = '\c0%1 is killed by a laser turret.';
$DeathMessageTurretKill[$DamageType::Laser, 1] = '\c0%1\'s body now marks the location of a laser turret.';
$DeathMessageTurretKill[$DamageType::Laser, 2] = '\c0%1 is fried by a laser turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// ELF
$DamageType::ELF = $DamageTypeCount;
$DamageType::ELFTurret = $DamageTypeCount; // Legacy
$DamageTypeText[$DamageTypeCount] = 'Arc Welder';

$DeathMessageSelfKill[$DamageType::ELF, 0] = '\c0%1 ELFs %2self with the ELF.';
$DeathMessageSelfKill[$DamageType::ELF, 1] = '\c0%1 ELFs %2self with the ELF.';
$DeathMessageSelfKill[$DamageType::ELF, 2] = '\c0%1 ELFs %2self with the ELF.';
$DeathMessageSelfKill[$DamageType::ELF, 3] = '\c0%1 ELFs %2self with the ELF.';
$DeathMessageSelfKill[$DamageType::ELF, 4] = '\c0%1 ELFs %2self with the ELF.';

$DeathMessage[$DamageType::Elf, 0] = '\c0%4 fries %1 with the ELF.';
$DeathMessage[$DamageType::Elf, 1] = '\c0%4 bug zaps %1 with %6 ELF.';
$DeathMessage[$DamageType::Elf, 2] = '\c0%1 learns the shocking truth about %4\'s ELF skills.';
$DeathMessage[$DamageType::Elf, 3] = '\c0%4 electrocutes %1 without a sponge.';
$DeathMessage[$DamageType::Elf, 4] = '\c0%4\'s arc welder leaves %1 a crispy critter.';

$DeathMessageTeamKill[$DamageType::ELF, 0] = '\c0%4 TEAMKILLED %1 with an ELF!';
$DeathMessageCTurretTeamKill[$DamageType::ELF, 0] = '\c0%4 TEAMKILLED %1 with an ELF turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::ELF, 0] = '\c0%1 got in the way of a friendly ELF turret!';

$DeathMessageCTurretKill[$DamageType::ELF, 0] = '\c0%1 gets zapped by ELF gunner %4.';
$DeathMessageCTurretKill[$DamageType::ELF, 1] = '\c0%1 gets barbecued by ELF gunner %4.';
$DeathMessageCTurretKill[$DamageType::ELF, 2] = '\c0%1 gets shocked by ELF gunner %4.';

$DeathMessageTurretKill[$DamageType::ELF, 0] = '\c0%1 is killed by an ELF turret.';
$DeathMessageTurretKill[$DamageType::ELF, 1] = '\c0%1 is zapped by an ELF turret.';
$DeathMessageTurretKill[$DamageType::ELF, 2] = '\c0%1 is short-circuited by an ELF turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Mortar
$DamageType::Mortar = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Mortar';

$DeathMessageSelfKill[$DamageType::Mortar, 0] = '\c0%1 kills %2self with a mortar!';
$DeathMessageSelfKill[$DamageType::Mortar, 1] = '\c0%1 hugs %3 own big boomie.';
$DeathMessageSelfKill[$DamageType::Mortar, 2] = '\c0%1 mortars %2self all over the map.';
$DeathMessageSelfKill[$DamageType::Mortar, 3] = '\c0%1 experiences %3 mortar\'s payload up close.';
$DeathMessageSelfKill[$DamageType::Mortar, 4] = '\c0%1 suffered the wrath of %3 own mortar.';

$DeathMessage[$DamageType::Mortar, 0] = '\c0%4 obliterates %1 with the mortar.';
$DeathMessage[$DamageType::Mortar, 1] = '\c0%4 drops a mortar round right in %1\'s lap.';
$DeathMessage[$DamageType::Mortar, 2] = '\c0%4 delivers a mortar payload straight to %1.';
$DeathMessage[$DamageType::Mortar, 3] = '\c0%4 offers a little "heavy love" to %1.';
$DeathMessage[$DamageType::Mortar, 4] = '\c0%1 stumbles into %4\'s mortar reticle.';

$DeathMessageTeamKill[$DamageType::Mortar, 0] = '\c0%4 TEAMKILLED %1 with a Mortar!';
$DeathMessageCTurretTeamKill[$DamageType::Mortar, 0] = '\c0%4 TEAMKILLED %1 with a Mortar turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Mortar, 0] = '\c0%1 got in the way of a friendly Mortar turret!';

$DeathMessageCTurretKill[$DamageType::Mortar, 0] = '\c0Whoops! %1 + %4\'s mortar = Dead %1.';
$DeathMessageCTurretKill[$DamageType::Mortar, 1] = '\c0%1 learns the happy explosion dance from %4\'s mortar.';
$DeathMessageCTurretKill[$DamageType::Mortar, 2] = '\c0%4\'s mortar has a blast with %1.';

$DeathMessageTurretKill[$DamageType::Mortar, 0] = '\c0%1 is pureed by a mortar turret.';
$DeathMessageTurretKill[$DamageType::Mortar, 1] = '\c0%1 enjoys a mortar turret\'s attention.';
$DeathMessageTurretKill[$DamageType::Mortar, 2] = '\c0%1 is blown to kibble by a mortar turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Missile
$DamageType::Missile = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Missile';

$DeathMessageSelfKill[$DamageType::Missile, 0] = '\c0%1 kills %2self with a missile!';
$DeathMessageSelfKill[$DamageType::Missile, 1] = '\c0%1 runs a missile up %3 own tailpipe.';
$DeathMessageSelfKill[$DamageType::Missile, 2] = '\c0%1 tests the missile\'s shaped charge on %2self.';
$DeathMessageSelfKill[$DamageType::Missile, 3] = '\c0%1 achieved missile lock on %2self.';
$DeathMessageSelfKill[$DamageType::Missile, 4] = '\c0%1 gracefully smoked %2self with a missile!';

$DeathMessage[$DamageType::Missile, 0] = '\c0%4 intercepts %1 with a missile.';
$DeathMessage[$DamageType::Missile, 1] = '\c0%4 watches %6 missile touch %1 and go boom.';
$DeathMessage[$DamageType::Missile, 2] = '\c0%4 got sweet tone on %1.';
$DeathMessage[$DamageType::Missile, 3] = '\c0By now, %1 has realized %4\'s missile killed %2.';
$DeathMessage[$DamageType::Missile, 4] = '\c0%4\'s missile rains little pieces of %1 all over the ground.';

$DeathMessageTeamKill[$DamageType::Missile, 0] = '\c0%4 TEAMKILLED %1 with a Missile!';
$DeathMessageCTurretTeamKill[$DamageType::Missile, 0] = '\c0%4 TEAMKILLED %1 with a Missile turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Missile, 0] = '\c0%1 got in the way of a friendly Missile turret!';

$DeathMessageCTurretKill[$DamageType::Missile, 0] = '\c0%4 shows %1 a new world of pain with a missile.';
$DeathMessageCTurretKill[$DamageType::Missile, 1] = '\c0%4 pops %1 with a missile.';
$DeathMessageCTurretKill[$DamageType::Missile, 2] = '\c0%4\'s missile lights up %1\'s, uh, ex-life.';

$DeathMessageTurretKill[$DamageType::Missile, 0] = '\c0%1 is killed by a missile turret.';
$DeathMessageTurretKill[$DamageType::Missile, 1] = '\c0%1 is shot down by a missile turret.';
$DeathMessageTurretKill[$DamageType::Missile, 2] = '\c0%1 is blown away by a missile turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// ShockLance
$DamageType::ShockLance = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'ShockLance';

$DeathMessageSelfKill[$DamageType::ShockLance, 0] = '\c0%1 kills %2self with a ShockLance!';
$DeathMessageSelfKill[$DamageType::ShockLance, 1] = '\c0%1 runs a ShockLance up %3 own tailpipe.';
$DeathMessageSelfKill[$DamageType::ShockLance, 2] = '\c0%1 tests the ShockLance\'s shaped charge on %2self.';
$DeathMessageSelfKill[$DamageType::ShockLance, 3] = '\c0%1 achieved ShockLance lock on %2self.';
$DeathMessageSelfKill[$DamageType::ShockLance, 4] = '\c0%1 gracefully smoked %2self with a ShockLance!';

$DeathMessage[$DamageType::Shocklance, 0] = '\c0%4 reaps a harvest of %1 with the Shocklance.';
$DeathMessage[$DamageType::Shocklance, 1] = '\c0%4 feeds %1 the business end of %6 Shocklance.';
$DeathMessage[$DamageType::Shocklance, 2] = '\c0%4 stops %1 dead with the Shocklance.';
$DeathMessage[$DamageType::Shocklance, 3] = '\c0%4 eliminates %1 in close combat.';
$DeathMessage[$DamageType::Shocklance, 4] = '\c0%4 ruins %1\'s day with one zap of a Shocklance.';

$DeathMessageTeamKill[$DamageType::ShockLance, 0] = '\c0%4 TEAMKILLED %1 with a Shocklance!';
$DeathMessageCTurretTeamKill[$DamageType::ShockLance, 0] = '\c0%4 TEAMKILLED %1 with a Shocklance turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::ShockLance, 0] = '\c0%1 got in the way of a friendly Shocklance turret!';

$DeathMessageCTurretKill[$DamageType::ShockLance, 0] = '\c0%4 shows %1 a new world of pain with a Shocklance.';
$DeathMessageCTurretKill[$DamageType::ShockLance, 1] = '\c0%4 pops %1 with a Shocklance.';
$DeathMessageCTurretKill[$DamageType::ShockLance, 2] = '\c0%4\'s Shocklance lights up %1\'s, uh, ex-life.';

$DeathMessageTurretKill[$DamageType::ShockLance, 0] = '\c0%1 is killed by a Shocklance turret.';
$DeathMessageTurretKill[$DamageType::ShockLance, 1] = '\c0%1 is shot down by a Shocklance turret.';
$DeathMessageTurretKill[$DamageType::ShockLance, 2] = '\c0%1 is blown away by a Shocklance   turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// BomberBombs
$DamageType::BomberBombs = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'BomberBombs';

$DeathMessageSelfKill[$DamageType::BomberBombs, 0] = '\c0%1 kills %2self with a freefall bomb!';
$DeathMessageSelfKill[$DamageType::BomberBombs, 1] = '\c0%1 kills %2self with a freefall bomb!';
$DeathMessageSelfKill[$DamageType::BomberBombs, 2] = '\c0%1 kills %2self with a freefall bomb!';
$DeathMessageSelfKill[$DamageType::BomberBombs, 3] = '\c0%1 kills %2self with a freefall bomb!';
$DeathMessageSelfKill[$DamageType::BomberBombs, 4] = '\c0%1 kills %2self with a freefall bomb!';

$DeathMessage[$DamageType::BomberBombs, 0] = '\c0%4 kills %1 with a freefall bomb.';
$DeathMessage[$DamageType::BomberBombs, 1] = '\c0%4 kills %1 with a freefall bomb.';
$DeathMessage[$DamageType::BomberBombs, 2] = '\c0%4 kills %1 with a freefall bomb.';
$DeathMessage[$DamageType::BomberBombs, 3] = '\c0%4 kills %1 with a freefall bomb.';
$DeathMessage[$DamageType::BomberBombs, 4] = '\c0%4 kills %1 with a freefall bomb.';

$DeathMessageTeamKill[$DamageType::BomberBombs, 0] = '\c0%4 TEAMKILLED %1 with a freefall bomb!';
$DeathMessageCTurretTeamKill[$DamageType::BomberBombs, 0] = '\c0%4 TEAMKILLED %1 with a freefall bomb turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::BomberBombs, 0] = '\c0%1 got in the way of a friendly freefall bomb turret!';

$DeathMessageCTurretKill[$DamageType::BomberBombs, 0] = '\c0%1 catches %4\'s bomb in both teeth.';
$DeathMessageCTurretKill[$DamageType::BomberBombs, 1] = '\c0%4 leaves %1 a smoking bomb crater.';
$DeathMessageCTurretKill[$DamageType::BomberBombs, 2] = '\c0%4 bombs %1 back to the 20th century.';

$DeathMessageTurretKill[$DamageType::BomberBombs, 0] = '\c0%1 catches %4\'s bomb in both teeth.';
$DeathMessageTurretKill[$DamageType::BomberBombs, 1] = '\c0%4 leaves %1 a smoking bomb crater.';
$DeathMessageTurretKill[$DamageType::BomberBombs, 2] = '\c0%4 bombs %1 back to the 20th century.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// SatchelCharge
$DamageType::SatchelCharge = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'SatchelCharge';

$DeathMessageSelfKill[$DamageType::SatchelCharge, 0] = '\c0%1 goes out with a bang!';  //applies to most explosion types
$DeathMessageSelfKill[$DamageType::SatchelCharge, 1] = '\c0%1 blows %2self into tiny bits and pieces.';
$DeathMessageSelfKill[$DamageType::SatchelCharge, 2] = '\c0%1 explodes in that fatal kind of way.';
$DeathMessageSelfKill[$DamageType::SatchelCharge, 3] = '\c0%1 experiences explosive decompression!';
$DeathMessageSelfKill[$DamageType::SatchelCharge, 4] = '\c0%1 splashes all over the map.';

$DeathMessage[$DamageType::SatchelCharge, 0] = '\c0%4 buys %1 a ticket to the moon.';  //satchel charge only
$DeathMessage[$DamageType::SatchelCharge, 1] = '\c0%4 blows %1 into tiny bits.';
$DeathMessage[$DamageType::SatchelCharge, 2] = '\c0%4 makes %1 a hugely explosive offer.';
$DeathMessage[$DamageType::SatchelCharge, 3] = '\c0%4 turns %1 into a cloud of satchel-vaporized armor.';
$DeathMessage[$DamageType::SatchelCharge, 4] = '\c0%4\'s satchel charge leaves %1 nothin\' but smokin\' boots.';

$DeathMessageTeamKill[$DamageType::SatchelCharge, 0] = '\c0%4 TEAMKILLED %1 with a satchel charge!';
$DeathMessageCTurretTeamKill[$DamageType::SatchelCharge, 0] = '\c0%4 TEAMKILLED %1 with a satchel charge turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::SatchelCharge, 0] = '\c0%1 got in the way of a friendly satchel charge turret!';

$DeathMessageCTurretKill[$DamageType::SatchelCharge, 0] = '\c0%1 catches %4\'s satchel charge in both teeth.';
$DeathMessageCTurretKill[$DamageType::SatchelCharge, 1] = '\c0%4 leaves %1 a smoking satchel charge crater.';
$DeathMessageCTurretKill[$DamageType::SatchelCharge, 2] = '\c0%4 satchel charge\'s %1 back to the 20th century.';

$DeathMessageTurretKill[$DamageType::SatchelCharge, 0] = '\c0%1 catches %4\'s satchel charge in both teeth.';
$DeathMessageTurretKill[$DamageType::SatchelCharge, 1] = '\c0%4 leaves %1 a smoking satchel charge crater.';
$DeathMessageTurretKill[$DamageType::SatchelCharge, 2] = '\c0%4 satchel charge\'s %1 back to the 20th century.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// EMP
$DamageType::EMP = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'EMP';

$DeathMessageSelfKill[$DamageType::EMP, 0] = '\c0%1 accidentally drops an EMP and attempts to stuff it back in %2 armor.';
$DeathMessageSelfKill[$DamageType::EMP, 1] = '\c0%1 feels the wrath of %3 own electromagnetic pulse.';
$DeathMessageSelfKill[$DamageType::EMP, 2] = '\c0%1 kills %2self with an EMP!';
$DeathMessageSelfKill[$DamageType::EMP, 3] = '\c0%1\'s EMP causes %3 to do the electric slide.';
$DeathMessageSelfKill[$DamageType::EMP, 4] = '\c0%1 attempts to harness the awesome power of electromagnetism... and fails.';

$DeathMessage[$DamageType::EMP, 0] = '\c0%4 fries %1 with an EM pulse.';
$DeathMessage[$DamageType::EMP, 1] = '\c0%4 was last heard screaming "UNLIMITED POWAH!" in %1\s direction.';
$DeathMessage[$DamageType::EMP, 2] = '\c0%1 learns the shocking truth about %4\'s EM Pulse.';
$DeathMessage[$DamageType::EMP, 3] = '\c0%4 blows out %1\'s armor with an EM Pulse.';
$DeathMessage[$DamageType::EMP, 4] = '\c0%4\'s EM Pulse leaves %1 a crispy critter.';

$DeathMessageTeamKill[$DamageType::EMP, 0] = '\c0%4 TEAMKILLED %1 with an EM pulse!';
$DeathMessageCTurretTeamKill[$DamageType::EMP, 0] = '\c0%4 TEAMKILLED %1 with an EM pulse turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::EMP, 0] = '\c0%1 got in the way of a friendly EM pulse turret!';

$DeathMessageCTurretKill[$DamageType::EMP, 0] = '\c0%4 blows out %1\'s armor with an EM Pulse.';
$DeathMessageCTurretKill[$DamageType::EMP, 1] = '\c0%1 learns the shocking truth about %4\'s EM Pulse.';
$DeathMessageCTurretKill[$DamageType::EMP, 2] = '\c0%4\'s EM Pulse leaves %1 a crispy critter.';

$DeathMessageTurretKill[$DamageType::EMP, 0] = '\c0%4 blows out %1\'s armor with an EM Pulse.';
$DeathMessageTurretKill[$DamageType::EMP, 1] = '\c0%1 learns the shocking truth about %4\'s EM Pulse.';
$DeathMessageTurretKill[$DamageType::EMP, 2] = '\c0%4\'s EM Pulse leaves %1 a crispy critter.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Burn
$DamageType::Burn = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Burn';

$DeathMessageSelfKill[$DamageType::Burn, 0] = '\c0%1 shows %2self the awesome power of fire.';
$DeathMessageSelfKill[$DamageType::Burn, 1] = '\c0%1 is now painfully aware of how a plasma fire feels.';
$DeathMessageSelfKill[$DamageType::Burn, 2] = '\c0%1 swallows a white-hot mouthful of %3 own plasma.';
$DeathMessageSelfKill[$DamageType::Burn, 3] = '\c0%1 immolates %2self.';
$DeathMessageSelfKill[$DamageType::Burn, 4] = '\c0%1 experiences the joy of cooking %2self.';

$DeathMessage[$DamageType::Burn, 0] = '\c0%4 converts %1 into a living bonfire.';
$DeathMessage[$DamageType::Burn, 1] = '\c0%1 forgot to put out %4\'s flames.';
$DeathMessage[$DamageType::Burn, 2] = '\c0%4 sets %1 on fire.';
$DeathMessage[$DamageType::Burn, 3] = '\c0%4 roasts some s\'mores over %1\'s burning corpse.';
$DeathMessage[$DamageType::Burn, 4] = '\c0%4 experiences a plasma fire first hand thanks to %1.';

$DeathMessageTeamKill[$DamageType::Burn, 0] = '\c0%4 TEAMKILLED %1 with a plasma fire!';
$DeathMessageCTurretTeamKill[$DamageType::Burn, 0] = '\c0%4 TEAMKILLED %1 with a plasma fire!';
$DeathMessageCTurretAccdtlKill[$DamageType::Burn, 0] = '\c0%1 got in the way of a friendly plasma fire!';

$DeathMessageCTurretKill[$DamageType::Burn, 0] = '\c0%4 experiences a plasma fire first hand thanks to %1.';
$DeathMessageCTurretKill[$DamageType::Burn, 1] = '\c0%4 sets %1 on fire.';
$DeathMessageCTurretKill[$DamageType::Burn, 2] = '\c0%4 roasts some s\'mores over %1\'s burning corpse.';

$DeathMessageTurretKill[$DamageType::Burn, 0] = '\c0%1 is lit on fire by a turret.';
$DeathMessageTurretKill[$DamageType::Burn, 1] = '\c0%1 is lit on fire by a turret.';
$DeathMessageTurretKill[$DamageType::Burn, 2] = '\c0%1 is lit on fire by a turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// AutoCannon
$DamageType::AutoCannon = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Autocannon';

$DeathMessageSelfKill[$DamageType::AutoCannon, 0] = '\c0%1 dives heroically on %3 own auto-shell.';
$DeathMessageSelfKill[$DamageType::AutoCannon, 1] = '\c0%1 dives heroically on %3 own auto-shell.';
$DeathMessageSelfKill[$DamageType::AutoCannon, 2] = '\c0%1 learns that auto-shell go "pop"!';
$DeathMessageSelfKill[$DamageType::AutoCannon, 3] = '\c0%1 dives heroically on %3 own auto-shell.';
$DeathMessageSelfKill[$DamageType::AutoCannon, 4] = '\c0%1 gets up close and personal with %3 own auto-shell.';

$DeathMessage[$DamageType::AutoCannon, 0] = '\c0%4 pops %1 good with an auto-shell.';
$DeathMessage[$DamageType::AutoCannon, 1] = '\c0%4 happily blows %1 into pieces with %6 autocannon.';
$DeathMessage[$DamageType::AutoCannon, 2] = '\c0%4 commands %1 to dance the autocannon dance.';
$DeathMessage[$DamageType::AutoCannon, 3] = '\c0%4 blows out %1\'s... everything with the autocannon.';
$DeathMessage[$DamageType::AutoCannon, 4] = '\c0%4 blows chunks away from %1 with %6 autocannon.';

$DeathMessageTeamKill[$DamageType::AutoCannon, 0] = '\c0%4 TEAMKILLED %1 with an autocannon!';
$DeathMessageCTurretTeamKill[$DamageType::AutoCannon, 0] = '\c0%4 TEAMKILLED %1 with an autocannon turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::AutoCannon, 0] = '\c0%1 got in the way of an friendly autocannon turret!';

$DeathMessageCTurretKill[$DamageType::AutoCannon, 0] = '\c0%4 pops %1 good with an autocannon.';
$DeathMessageCTurretKill[$DamageType::AutoCannon, 1] = '\c0%4 happily blows %1 into pieces with %6 autocannon.';
$DeathMessageCTurretKill[$DamageType::AutoCannon, 2] = '\c0%4 blows chunks away from %1 with %6 autocannon.';

$DeathMessageTurretKill[$DamageType::AutoCannon, 0] = '\c0%1 plays chicken with an autocannon turret and loses.';
$DeathMessageTurretKill[$DamageType::AutoCannon, 1] = '\c0%1 comes apart at the sight of an autocannon turret.';
$DeathMessageTurretKill[$DamageType::AutoCannon, 2] = '\c0%1 gets popped by a nearby autocannon turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Turbocharger
$DamageType::Turbocharger = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Turbocharger';

$DeathMessageSelfKill[$DamageType::Turbocharger, 0] = '\c0%1 is the victim of a sudden turbocharger reactor outburst.';
$DeathMessageSelfKill[$DamageType::Turbocharger, 1] = '\c0%1 is the victim of a sudden turbocharger reactor outburst.';
$DeathMessageSelfKill[$DamageType::Turbocharger, 2] = '\c0%1 is the victim of a sudden turbocharger reactor outburst.';
$DeathMessageSelfKill[$DamageType::Turbocharger, 3] = '\c0%1 is the victim of a sudden turbocharger reactor outburst.';
$DeathMessageSelfKill[$DamageType::Turbocharger, 4] = '\c0%1 is the victim of a sudden turbocharger reactor outburst.';

$DeathMessage[$DamageType::Turbocharger, 0] = '\c0%1 is annihilated by %4\'s explosive overload.';
$DeathMessage[$DamageType::Turbocharger, 1] = '\c0%1 is blown away by %4\'s core breach.';
$DeathMessage[$DamageType::Turbocharger, 2] = '\c0%1 contacts %4\'s exploding turbocharger and goes boom!';
$DeathMessage[$DamageType::Turbocharger, 3] = '\c0%1 gets a fatal booster shot from %4\'s turbocharger explosion.';
$DeathMessage[$DamageType::Turbocharger, 4] = '\c0Ouch! %1 + %4\'s overloaded turbocharger = Dead %1.';

$DeathMessageTeamKill[$DamageType::Turbocharger, 0] = '\c0%4 TEAMKILLED %1 with a turbocharger!';
$DeathMessageCTurretTeamKill[$DamageType::Turbocharger, 0] = '\c0%4 TEAMKILLED %1 with a turbocharger turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Turbocharger, 0] = '\c0%1 got in the way of a turbocharger overload turret!';

$DeathMessageCTurretKill[$DamageType::Turbocharger, 0] = '\c0%4 plants a meteor blast in %1\'s chest.';
$DeathMessageCTurretKill[$DamageType::Turbocharger, 1] = '\c0%1 fails to evade %4\'s deft meteor cannon barrage.';
$DeathMessageCTurretKill[$DamageType::Turbocharger, 2] = '\c0%1 gets knocked into next week from %4\'s meteor blast.';

$DeathMessageTurretKill[$DamageType::Turbocharger, 0] = '\c0%1 somehow figures out how to weaponize an exploding turbocharger.';
$DeathMessageTurretKill[$DamageType::Turbocharger, 1] = '\c0%1 somehow figures out how to weaponize an exploding turbocharger.';
$DeathMessageTurretKill[$DamageType::Turbocharger, 2] = '\c0%1 somehow figures out how to weaponize an exploding turbocharger.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Railgun
$DamageType::Railgun = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Railgun';

$DeathMessageSelfKill[$DamageType::Railgun, 0] = '\c0%1 somehow kills %2self with a railgun.';
$DeathMessageSelfKill[$DamageType::Railgun, 1] = '\c0%1 somehow kills %2self with a railgun.';
$DeathMessageSelfKill[$DamageType::Railgun, 2] = '\c0%1 somehow kills %2self with a railgun.';
$DeathMessageSelfKill[$DamageType::Railgun, 3] = '\c0%1 somehow kills %2self with a railgun.';
$DeathMessageSelfKill[$DamageType::Railgun, 4] = '\c0%1 somehow kills %2self with a railgun.';

$DeathMessage[$DamageType::Railgun, 0] = '\c0%4 wrecks %1 hard with a supersonic rail.';
$DeathMessage[$DamageType::Railgun, 1] = '\c0%1 rides %4\s rails.';
$DeathMessage[$DamageType::Railgun, 2] = '\c0%1 gets extra friendly with %4\'s rails.';
$DeathMessage[$DamageType::Railgun, 3] = '\c0%4\'s railgun gives %1 a hot tungsten injection.';
$DeathMessage[$DamageType::Railgun, 4] = '\c0%1 has a very brief and rough fling with %4\'s railgun.';

$DeathMessageTeamKill[$DamageType::Railgun, 0] = '\c0%4 TEAMKILLED %1 with a railgun!';
$DeathMessageCTurretTeamKill[$DamageType::Railgun, 0] = '\c0%4 TEAMKILLED %1 with a railgun turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Railgun, 0] = '\c0%1 got in the way of a railgun overload turret!';

$DeathMessageCTurretKill[$DamageType::Railgun, 0] = '\c0%4 wrecks %1 hard with a supersonic rail.';
$DeathMessageCTurretKill[$DamageType::Railgun, 1] = '\c0%4\'s railgun gives %1 a hot tungsten injection.';
$DeathMessageCTurretKill[$DamageType::Railgun, 2] = '\c0%1 has a very brief and rough fling with %4\'s railgun.';

$DeathMessageTurretKill[$DamageType::Railgun, 0] = '\c0%1 somehow figures out how to turretize a Railgun.';
$DeathMessageTurretKill[$DamageType::Railgun, 1] = '\c0%1 somehow figures out how to turretize a Railgun.';
$DeathMessageTurretKill[$DamageType::Railgun, 2] = '\c0%1 somehow figures out how to turretize a Railgun.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Bolter
$DamageType::Bolter = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Bolter';

$DeathMessageSelfKill[$DamageType::Bolter, 0] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::Bolter, 1] = '\c0%1 makes a note to watch out for ricochets.';
$DeathMessageSelfKill[$DamageType::Bolter, 2] = '\c0%1 manages to kill %2self with a reflected bolt.';
$DeathMessageSelfKill[$DamageType::Bolter, 3] = '\c0%1 deftly guns %2self down.';
$DeathMessageSelfKill[$DamageType::Bolter, 4] = '\c0%1 has a fatal encounter with %3self.';

$DeathMessage[$DamageType::Bolter, 0] = '\c0%4 rips %1 up with the bolter.';
$DeathMessage[$DamageType::Bolter, 1] = '\c0%4 happily chews %1 into pieces with %6 bolter.';
$DeathMessage[$DamageType::Bolter, 2] = '\c0%4 administers a dose of energy spears to %1.';
$DeathMessage[$DamageType::Bolter, 3] = '\c0%1 suffers a serious hosing from %4\'s bolter.';
$DeathMessage[$DamageType::Bolter, 4] = '\c0%4 bestows the blessings of %6 bolter on %1.';

$DeathMessageTeamKill[$DamageType::Bolter, 0] = '\c0%4 TEAMKILLED %1 with a bolter!';
$DeathMessageCTurretTeamKill[$DamageType::Bolter, 0] = '\c0%4 TEAMKILLED %1 with a bolter turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Bolter, 0] = '\c0%1 got in the way of a friendly bolter turret!';

$DeathMessageCTurretKill[$DamageType::Bolter, 0] = '\c0%1 enjoys the rich, electric taste of %4\'s bolter.';
$DeathMessageCTurretKill[$DamageType::Bolter, 1] = '\c0%4\'s bolter turret plays sweet music all over %1.';
$DeathMessageCTurretKill[$DamageType::Bolter, 2] = '\c0%1 receives a stellar exit wound from %4\'s bolter.';

$DeathMessageTurretKill[$DamageType::Bolter, 0] = '\c0%1 gets chunked apart by a bolter turret.';
$DeathMessageTurretKill[$DamageType::Bolter, 1] = '\c0%1 gets rained on by a nearby bolter turret.';
$DeathMessageTurretKill[$DamageType::Bolter, 2] = '\c0%1 finds %2self on the wrong end of a bolter turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// ShieldPulse
$DamageType::ShieldPulse = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'ShieldPulse';

$DeathMessageSelfKill[$DamageType::ShieldPulse, 0] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::ShieldPulse, 1] = '\c0%1 makes a note to watch out for ricochets.';
$DeathMessageSelfKill[$DamageType::ShieldPulse, 2] = '\c0%1 manages to kill %2self with a reflected blaster.';
$DeathMessageSelfKill[$DamageType::ShieldPulse, 3] = '\c0%1 deftly guns %2self down.';
$DeathMessageSelfKill[$DamageType::ShieldPulse, 4] = '\c0%1 has a fatal encounter with %3self.';

$DeathMessage[$DamageType::ShieldPulse, 0] = '\c0%4 expands and crushes %1.';
$DeathMessage[$DamageType::ShieldPulse, 1] = '\c0%4\'s shield expansion crushes %1.';
$DeathMessage[$DamageType::ShieldPulse, 2] = '\c0%1 gets all 2-dimensional from %4\'s shield expansion.';
$DeathMessage[$DamageType::ShieldPulse, 3] = '\c0%1 turns into a tribal pizza thanks to %4\'s shield expansion.';
$DeathMessage[$DamageType::ShieldPulse, 4] = '\c0%1 couldn\'t take the pressure.';

$DeathMessageTeamKill[$DamageType::ShieldPulse, 0] = '\c0%4 TEAMKILLED %1 with a shield pulse!';
$DeathMessageCTurretTeamKill[$DamageType::ShieldPulse, 0] = '\c0%4 TEAMKILLED %1 with a shield pulse turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::ShieldPulse, 0] = '\c0%1 got in the way of a friendly shield pulse turret!';

$DeathMessageCTurretKill[$DamageType::ShieldPulse, 0] = '\c0%1 turns into a tribal pizza thanks to %4\'s shield expansion.';
$DeathMessageCTurretKill[$DamageType::ShieldPulse, 1] = '\c0%4\'s ShieldPulse turret plays sweet music all over %1.';
$DeathMessageCTurretKill[$DamageType::ShieldPulse, 2] = '\c0%1 receives a stellar exit wound from %4\'s ShieldPulse.';

$DeathMessageTurretKill[$DamageType::ShieldPulse, 0] = '\c0%1 gets blasted by a turret.';
$DeathMessageTurretKill[$DamageType::ShieldPulse, 1] = '\c0%4\'s shield expansion crushes %1.';
$DeathMessageTurretKill[$DamageType::ShieldPulse, 2] = '\c0%1 turns into a tribal pizza thanks to %4\'s shield expansion.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Turret
$DamageType::Turret = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Turret';

$DeathMessageSelfKill[$DamageType::Turret, 0] = '\c0%1 kills %2self with a turret.';
$DeathMessageSelfKill[$DamageType::Turret, 1] = '\c0%1 kills %2self with a turret.';
$DeathMessageSelfKill[$DamageType::Turret, 2] = '\c0%1 kills %2self with a turret.';
$DeathMessageSelfKill[$DamageType::Turret, 3] = '\c0%1 kills %2self with a turret.';
$DeathMessageSelfKill[$DamageType::Turret, 4] = '\c0%1 kills %2self with a turret.';

$DeathMessage[$DamageType::Turret, 0] = '\c0%4 kills %1 with a turret.';
$DeathMessage[$DamageType::Turret, 1] = '\c0%4 kills %1 with a turret.';
$DeathMessage[$DamageType::Turret, 2] = '\c0%4 kills %1 with a turret.';
$DeathMessage[$DamageType::Turret, 3] = '\c0%4 kills %1 with a turret.';
$DeathMessage[$DamageType::Turret, 4] = '\c0%4 kills %1 with a turret.';

$DeathMessageTeamKill[$DamageType::Turret, 0] = '\c0%4 TEAMKILLED %1 with a turret!';
$DeathMessageCTurretTeamKill[$DamageType::Turret, 0] = '\c0%4 TEAMKILLED %1 with a turret!';
$DeathMessageCTurretAccdtlKill[$DamageType::Turret, 0] = '\c0%1 got in the way of a friendly turret!';

$DeathMessageCTurretKill[$DamageType::Turret, 0] = '\c0%4 kills %1 with a turret.';
$DeathMessageCTurretKill[$DamageType::Turret, 1] = '\c0%4 kills %1 with a turret.';
$DeathMessageCTurretKill[$DamageType::Turret, 2] = '\c0%4 kills %1 with a turret.';

$DeathMessageTurretKill[$DamageType::Turret, 0] = '\c0%1 gets killed by a turret.';
$DeathMessageTurretKill[$DamageType::Turret, 1] = '\c0%1 gets killed by a turret.';
$DeathMessageTurretKill[$DamageType::Turret, 2] = '\c0%1 gets killed by a turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Gauss Rifle
$DamageType::Gauss = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Gauss';

$DeathMessageSelfKill[$DamageType::Gauss, 0] = '\c0%1 kills %2self with a spike.';
$DeathMessageSelfKill[$DamageType::Gauss, 1] = '\c0%1 kills %2self with a spike.';
$DeathMessageSelfKill[$DamageType::Gauss, 2] = '\c0%1 kills %2self with a spike.';
$DeathMessageSelfKill[$DamageType::Gauss, 3] = '\c0%1 kills %2self with a spike.';
$DeathMessageSelfKill[$DamageType::Gauss, 4] = '\c0%1 kills %2self with a spike.';

$DeathMessage[$DamageType::Gauss, 0] = '\c0%4\'s gauss spike neatly drills %1.';
$DeathMessage[$DamageType::Gauss, 1] = '\c0%1 dies under %4\'s gauss spike love.';
$DeathMessage[$DamageType::Gauss, 2] = '\c0%1 is chewed up by %4\'s gauss spike.';
$DeathMessage[$DamageType::Gauss, 3] = '\c0%1 feels the burn from %4\'s gauss spike.';
$DeathMessage[$DamageType::Gauss, 4] = '\c0%1 is nailed by %4\'s gauss spike.';

$DeathMessageTeamKill[$DamageType::Gauss, 0] = '\c0%4 TEAMKILLED %1 with a gauss spike!';
$DeathMessageCSpikeTeamKill[$DamageType::Gauss, 0] = '\c0%4 TEAMKILLED %1 with a gauss turret!';
$DeathMessageCSpikeAccdtlKill[$DamageType::Gauss, 0] = '\c0%1 got in the way of a friendly gauss turret!';

$DeathMessageCSpikeKill[$DamageType::Gauss, 0] = '\c0%4\'s gauss spike neatly drills %1.';
$DeathMessageCSpikeKill[$DamageType::Gauss, 1] = '\c0%1 is chewed up by %4\'s gauss spike.';
$DeathMessageCSpikeKill[$DamageType::Gauss, 2] = '\c0%1 is nailed by %4\'s gauss spike.';

$DeathMessageSpikeKill[$DamageType::Gauss, 0] = '\c0%1 is chewed up by a gauss turret.';
$DeathMessageSpikeKill[$DamageType::Gauss, 1] = '\c0%1 is nailed by a gauss turret.';
$DeathMessageSpikeKill[$DamageType::Gauss, 2] = '\c0%1 gets in the crosshairs of a gauss turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// GeneralExplosive
$DamageType::GeneralExplosive = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'General Explosives';

$DeathMessageSelfKill[$DamageType::GeneralExplosive, 0] = '\c0%1 goes out with a bang.';
$DeathMessageSelfKill[$DamageType::GeneralExplosive, 1] = '\c0%1 finds out that this gun explodes.';
$DeathMessageSelfKill[$DamageType::GeneralExplosive, 2] = '\c0%1 blows %s brains out.';
$DeathMessageSelfKill[$DamageType::GeneralExplosive, 3] = '\c0%1 comes apart from their own explosives.';
$DeathMessageSelfKill[$DamageType::GeneralExplosive, 4] = '\c0%1 finds out what explosives do.';

$DeathMessage[$DamageType::GeneralExplosive, 0] = '\c0%4 teaches %1 the happy explosion dance.';
$DeathMessage[$DamageType::GeneralExplosive, 1] = '\c0%4 leaves %1 as a smoking crater.';
$DeathMessage[$DamageType::GeneralExplosive, 2] = '\c0%4 shows %1 a new world of explosive pain.';
$DeathMessage[$DamageType::GeneralExplosive, 3] = '\c0%4\'s explosives makes armored chowder out of %1.';
$DeathMessage[$DamageType::GeneralExplosive, 4] = '\c0%4 buys %1 a ticket to the moon.';

$DeathMessageTeamKill[$DamageType::Turret, 0] = '\c0%4 TEAMKILLED %1 with an explosive effect!';
$DeathMessageCTurretTeamKill[$DamageType::Turret, 0] = '\c0%4 TEAMKILLED %1 with an explosive effect!';
$DeathMessageCTurretAccdtlKill[$DamageType::Turret, 0] = '\c0%1 got in the way of a friendly explosive effect!';

$DeathMessageCTurretKill[$DamageType::GeneralExplosive, 0] = '\c0%4\'s explosives makes armored chowder out of %1.';
$DeathMessageCTurretKill[$DamageType::GeneralExplosive, 1] = '\c0%4 leaves %1 as a smoking crater.';
$DeathMessageCTurretKill[$DamageType::GeneralExplosive, 2] = '\c0%4 shows %1 a new world of explosive pain.';

$DeathMessageTurretKill[$DamageType::GeneralExplosive, 0] = '\c0%1 gets exploded by a turret.';
$DeathMessageTurretKill[$DamageType::GeneralExplosive, 1] = '\c0%1 gets exploded by a turret.';
$DeathMessageTurretKill[$DamageType::GeneralExplosive, 2] = '\c0%1 gets exploded by a turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Flak
$DamageType::Flak = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Flak';

$DeathMessageSelfKill[$DamageType::Flak, 0] = '\c0%1 goes out with a bang!';
$DeathMessageSelfKill[$DamageType::Flak, 1] = '\c0%1 blows %2self into tiny bits and pieces.';
$DeathMessageSelfKill[$DamageType::Flak, 2] = '\c0%1 explodes in that fatal kind of way.';
$DeathMessageSelfKill[$DamageType::Flak, 3] = '\c0%1 caught %2self in fatal flak.';
$DeathMessageSelfKill[$DamageType::Flak, 4] = '\c0%1 flaks %2self all over the map.';

$DeathMessage[$DamageType::Flak, 0] = '\c0%4 intercepts %1 with flak.';
$DeathMessage[$DamageType::Flak, 1] = '\c0%4 watches %6 flak charge blow %1 to pieces.';
$DeathMessage[$DamageType::Flak, 2] = '\c0%1 rides %4\'s flak charge.';
$DeathMessage[$DamageType::Flak, 3] = '\c0%4 delivers a flak payload straight to %1.';
$DeathMessage[$DamageType::Flak, 4] = '\c0%4\'s flak rains little pieces of %1 all over the ground.';

$DeathMessageTeamKill[$DamageType::Flak, 0] = '\c0%4 TEAMKILLED %1 with flak!';
$DeathMessageCTurretTeamKill[$DamageType::Flak, 0] = '\c0%4 TEAMKILLED %1 with flak!';
$DeathMessageCTurretAccdtlKill[$DamageType::Flak, 0] = '\c0%1 got in the way of a friendly flak blasts!';

$DeathMessageCTurretKill[$DamageType::Flak, 0] = '\c0%4 watches %6 flak charge blow %1 to pieces.';
$DeathMessageCTurretKill[$DamageType::Flak, 1] = '\c0%4\'s flak rains little pieces of %1 all over the ground.';
$DeathMessageCTurretKill[$DamageType::Flak, 2] = '\c0%4 delivers a flak payload straight to %1.';

$DeathMessageTurretKill[$DamageType::Flak, 0] = '\c0%1 is killed by a flak turret.';
$DeathMessageTurretKill[$DamageType::Flak, 1] = '\c0%1\'s body now marks the location of a flak turret.';
$DeathMessageTurretKill[$DamageType::Flak, 2] = '\c0%1 is blown away by a flak turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// General
$DamageType::General = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'General';

$DeathMessageSelfKill[$DamageType::General, 0] = '\c0%1 kills %2self.';
$DeathMessageSelfKill[$DamageType::General, 1] = '\c0%1 kills %2self.';
$DeathMessageSelfKill[$DamageType::General, 2] = '\c0%1 kills %2self.';
$DeathMessageSelfKill[$DamageType::General, 3] = '\c0%1 kills %2self.';
$DeathMessageSelfKill[$DamageType::General, 4] = '\c0%1 kills %2self.';

$DeathMessage[$DamageType::General, 0] = '\c0%4 kills %1.';
$DeathMessage[$DamageType::General, 1] = '\c0%4 destroys %1.';
$DeathMessage[$DamageType::General, 2] = '\c0%4 maims %1.';
$DeathMessage[$DamageType::General, 3] = '\c0%4 kills %1.';
$DeathMessage[$DamageType::General, 4] = '\c0%4 destroys %1.';

$DeathMessageTeamKill[$DamageType::General, 0] = '\c0%4 TEAMKILLED %1!';
$DeathMessageCTurretTeamKill[$DamageType::General, 0] = '\c0%4 TEAMKILLED %1!';
$DeathMessageCTurretAccdtlKill[$DamageType::General, 0] = '\c0%1 got in the way of a friendly and was killed!';

$DeathMessageCTurretKill[$DamageType::General, 0] = '\c0%4 kills %1.';
$DeathMessageCTurretKill[$DamageType::General, 1] = '\c0%4 destroys %1.';
$DeathMessageCTurretKill[$DamageType::General, 2] = '\c0%4 maims %1.';

$DeathMessageTurretKill[$DamageType::General, 0] = '\c0%1 is killed by a turret.';
$DeathMessageTurretKill[$DamageType::General, 1] = '\c0%1 is destroyed by a turret.';
$DeathMessageTurretKill[$DamageType::General, 2] = '\c0%1 is maimed by a turret.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// VehicleExplosion
$DamageType::VehicleExplosion = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'Vehicle Explosion';

$DeathMessageSelfKill[$DamageType::VehicleExplosion, 0] = '\c0%1 goes down with the ship.';
$DeathMessageSelfKill[$DamageType::VehicleExplosion, 1] = '\c0%1 goes down with the ship.';
$DeathMessageSelfKill[$DamageType::VehicleExplosion, 2] = '\c0%1 goes down with the ship.';
$DeathMessageSelfKill[$DamageType::VehicleExplosion, 3] = '\c0%1 goes down with the ship.';
$DeathMessageSelfKill[$DamageType::VehicleExplosion, 4] = '\c0%1 goes down with the ship.';

$DeathMessage[$DamageType::VehicleExplosion, 0] = '\c0%4\'s vehicle explosion takes out %1!';
$DeathMessage[$DamageType::VehicleExplosion, 1] = '\c0%4\'s vehicle explosion wrecks %1!';
$DeathMessage[$DamageType::VehicleExplosion, 2] = '\c0%4\'s vehicle explosion vaporizes %1!';
$DeathMessage[$DamageType::VehicleExplosion, 3] = '\c0%4\'s vehicle explosion takes out %1!';
$DeathMessage[$DamageType::VehicleExplosion, 4] = '\c0%4\'s vehicle explosion wrecks %1!';

$DeathMessageTeamKill[$DamageType::VehicleExplosion, 0] = '\c0%1 follows %4 to the grave!';
$DeathMessageCTurretTeamKill[$DamageType::VehicleExplosion, 0] = '\c0%1 follows %4 to the grave!';
$DeathMessageCTurretAccdtlKill[$DamageType::VehicleExplosion, 0] = '\c0%1 goes down with the ship.';

$DeathMessageCTurretKill[$DamageType::VehicleExplosion, 0] = '\c0%4\'s vehicle explosion takes out %1!';
$DeathMessageCTurretKill[$DamageType::VehicleExplosion, 1] = '\c0%4\'s vehicle explosion vaporizes %1!';
$DeathMessageCTurretKill[$DamageType::VehicleExplosion, 2] = '\c0%4\'s vehicle explosion wrecks %1!';

$DeathMessageTurretKill[$DamageType::VehicleExplosion, 0] = '\c0%1 goes down with the ship.';
$DeathMessageTurretKill[$DamageType::VehicleExplosion, 1] = '\c0%1 goes down with the ship.';
$DeathMessageTurretKill[$DamageType::VehicleExplosion, 2] = '\c0%1 goes down with the ship.';

$DamageTypeCount++;

//------------------------------------------------------------------------------
// Misc strings

$DamageType::Mine = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'default';

$DeathMessage[$DamageType::Mine, 0] = '\c0%4 reminds %1 that a mine is a terrible thing to waste.';
$DeathMessage[$DamageType::Mine, 1] = '\c0%1 doesn\'t see %4\'s mine in time.';
$DeathMessage[$DamageType::Mine, 2] = '\c0%4 gives %1 a piece of %6 mine.';
$DeathMessage[$DamageType::Mine, 3] = '\c0%1 puts their foot on %4\'s mine.';
$DeathMessage[$DamageType::Mine, 4] = '\c0%1 stepped on %4\'s toe-popper.';

$DeathMessageSelfKill[$DamageType::Mine, 0] = '\c0%1 kills %2self with a mine!';
$DeathMessageSelfKill[$DamageType::Mine, 1] = '\c0%1\'s mine violently reminds %2 of its existence.';
$DeathMessageSelfKill[$DamageType::Mine, 2] = '\c0%1 plants a decisive foot on %3 own mine!';
$DeathMessageSelfKill[$DamageType::Mine, 3] = '\c0%1 fatally trips on %3 own mine!';
$DeathMessageSelfKill[$DamageType::Mine, 4] = '\c0%1 makes a note not to run over %3 own mines.';

$DeathMessageTeamKill[$DamageType::Mine, 0] = '\c0%4 TEAMKILLED %1 with a mine!';

$DamageTypeCount++;

$DamageType::Explosion = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'defaultexplosion';
$DamageTypeCount++;

$DamageType::Impact = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'run-over';
$DamageTypeCount++;

$DamageType::Ground = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'crash-ground';

$DeathMessageSelfKill[$DamageType::Ground, 0] = '\c0%1 hits the ground and dies.';
$DeathMessageSelfKill[$DamageType::Ground, 1] = '\c0%1 learns the truth about the deadly ground.';
$DeathMessageSelfKill[$DamageType::Ground, 2] = '\c0%1 craters on impact.';
$DeathMessageSelfKill[$DamageType::Ground, 3] = '\c0%1 pancakes upon landing.';
$DeathMessageSelfKill[$DamageType::Ground, 4] = '\c0%1 loses a game of chicken with the ground.';

$DeathMessage[$DamageType::Ground, 0] = '\c0%1 hits the ground and dies.';
$DeathMessage[$DamageType::Ground, 1] = '\c0%1 hits the ground and dies.';
$DeathMessage[$DamageType::Ground, 2] = '\c0%1 hits the ground and dies.';
$DeathMessage[$DamageType::Ground, 3] = '\c0%1 hits the ground and dies.';
$DeathMessage[$DamageType::Ground, 4] = '\c0%1 hits the ground and dies.';

$DamageTypeCount++;

$DamageType::OutOfBounds = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'oob';
$DamageTypeCount++;

$DamageType::Lightning = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'lightning';
$DamageTypeCount++;

$DamageType::VehicleSpawn = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'vehspawn';
$DamageTypeCount++;

$DamageType::ForceFieldPowerup = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'ffpowerup';
$DamageTypeCount++;

$DamageType::Crash = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'crash';
$DamageTypeCount++;

// DMM -- added so MPBs that blow up under water get a message
$DamageType::Water = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'mpbwater';
$DamageTypeCount++;

//Tinman - used in Hunters for cheap bastards  ;)
$DamageType::NexusCamping = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'nexuscamp';
$DamageTypeCount++;

// MES -- added so CTRL-K can get a distinctive message
$DamageType::Suicide = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'suicide';
$DamageTypeCount++;

$DamageType::LifeSupportSuicide = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'lifesupportfail';
$DamageTypeCount++;

// Falling in lava
$DamageType::Lava = $DamageTypeCount;
$DamageTypeText[$DamageTypeCount] = 'lava';
$DamageTypeCount++;

// dependant on being pre-defined

$DeathMessageVehicleCrashCount = 5;
$DeathMessageVehicleCrash[$DamageType::Crash, 0] = '\c0%1 fails to eject in time.';
$DeathMessageVehicleCrash[$DamageType::Crash, 1] = '\c0%1 becomes one with their vehicle dashboard.';
$DeathMessageVehicleCrash[$DamageType::Crash, 2] = '\c0%1 drives under the influence of death.';
$DeathMessageVehicleCrash[$DamageType::Crash, 3] = '\c0%1 makes a perfect three hundred point landing.';
$DeathMessageVehicleCrash[$DamageType::Crash, 4] = '\c0%1 heroically pilots his vehicle into something really, really hard.';

$DeathMessageHeadshotCount = 3;
$DeathMessageHeadshot[$DamageType::Laser, 0] = '\c0%4 drills right through %1\'s braincase with %6 laser.';
$DeathMessageHeadshot[$DamageType::Laser, 1] = '\c0%4 pops %1\'s head like a cheap balloon.';
$DeathMessageHeadshot[$DamageType::Laser, 2] = '\c0%1 loses %3 head over %4\'s laser skill.';

// z0dd - ZOD, 8/25/02. Added Lance rear shot messages
$DeathMessageRearshotCount = 3;
$DeathMessageRearshot[$DamageType::ShockLance, 0] = '\c0%4 delivers a backdoor Lance to %1.';
$DeathMessageRearshot[$DamageType::ShockLance, 1] = '\c0%4 sends high voltage up %1\'s bum.';
$DeathMessageRearshot[$DamageType::ShockLance, 2] = '\c0%1 receives %4\'s rear-entry Lance attack.';
