//------------------------------------------------------------------------------------------
// Description: A task that triggers bots to grab & run the enemy flag.
//------------------------------------------------------------------------------------------
function AIEnhancedFlagCaptureTask::initFromObjective(%task, %objective, %client) { }
function AIEnhancedFlagCaptureTask::assume(%task, %client) { %task.setMonitorFreq(1); }
function AIEnhancedFlagCaptureTask::retire(%task, %client) { }

function AIEnhancedFlagCaptureTask::weight(%task, %client)
{
    if (%client.shouldRunFlag)
    {
        // First, is the enemy flag home?
        %enemyTeam = %client.team == 1 ? 2 : 1;
        %enemyFlag = nameToID("Team" @ %enemyTeam @ "Flag1");

        if (isObject(%enemyFlag) && %enemyFlag.isHome)
        {
            %client.targetCaptureFlag = %enemyFlag;
            %task.setWeight($DXAI::Task::MediumPriority);
        }
    }
    else
        %task.setWeight($DXAI::Task::NoPriority);
}

function AIEnhancedFlagCaptureTask::monitor(%task, %client)
{
    if (!isObject(%client.targetCaptureFlag) && !%client.hasFlag)
      return;

    if (!%client.hasFlag)
        %client.setMoveTarget(%client.targetCaptureFlag.getPosition());
    else
        %client.setMoveTarget(nameToID("Team" @ %client.team @ "Flag1").getPosition());
}
