//------------------------------------------------------------------------------------------
// Description: A task that performs path correction.
//------------------------------------------------------------------------------------------`
function AIEnhancedGunnerTask::initFromObjective(%task, %objective, %client) { }
function AIEnhancedGunnerTask::assume(%task, %client) { %task.setMonitorFreq(2); }
function AIEnhancedGunnerTask::retire(%task, %client) { }

function AIEnhancedGunnerTask::weight(%task, %client)
{
    if (%client.shouldGun)
        %task.setWeight($DXAI::Task::MediumPriority);
    else
        %task.setWeight($DXAI::Task::NoPriority);
}

function AIEnhancedGunnerTask::monitor(%task, %client)
{
    if (isObject(%client.gunnerMountTarget) && !isObject(%client.gunnerMountTarget.getControllingObject()))
    {
        %client.gunnerMountTarget = "";
        %client.gunnerWaitTarget = "";
        %client.gunnerWaitPoint = "";
    }

    if (%client.gunnerWaitPoint $= "" && !isObject(%client.gunnerMountTarget))
        return;

    // Pick a point >= 15m <= 30m from the center of the pad
    if (!isObject(%client.gunnerMountTarget) && vectorDist(%client.player.getWorldBoxCenter(), %client.gunnerWaitPoint) > 5)
        %client.setMoveTarget(%client.gunnerWaitPoint);
    else if (isObject(%client.gunnerMountTarget))
        %client.setMoveTarget(%client.gunnerMountTarget.getWorldBoxCenter());
}

function AIEnhancedGunnerTask::commanderMonitor(%commander, %bots)
{
    // Initially we should stand somewhere near a vehicle station
    %suitableStationCount = 0;

    for (%iteration = 0; %iteration < $AIVehicleStationSet.getCount(); %iteration++)
    {
        %vehiclePad = $AIVehicleStationSet.getObject(%iteration);
        %vehicleStation = %vehiclePad.station;

        if (%vehicleStation.team <= 0 || %vehicleStation.team == %commander.team)
        {
            if (!%vehicleStation.isDisabled() && %vehicleStation.isPowered())
            {
                %suitableStations[%suitableStationCount] = %vehiclePad;
                %suitableStationCount++;
            }
        }
    }

    // Scan for suitable vehicles
    %suitableVehicleCount = 0;

    for (%iteration = 0; %iteration < $AIVehicleSet.getCount(); %iteration++)
    {
        %vehicle = $AIVehicleSet.getObject(%iteration);
        %pilot = %vehicle.getControllingObject();

        if (%pilot.team == %commander.team)
        {
            %suitableVehicles[%suitableVehicleCount] = %vehicle;
            %suitableVehicleCount++;
        }
    }

    // Initialize the counters
    if (%commander.lastAssignedVehicleStationNumber $= "")
        %commander.lastAssignedVehicleStationNumber = 0;
    if (%commander.lastAssignedVehicleNumber $= "")
        %commander.lastAssignedVehicleNumber = 0;

    // Scan for waiting points
    for (%iteration = 0; %iteration < %bots.getCount(); %iteration++)
    {
        %bot = %bots.getObject(%iteration);

        if (!isObject(%bot.player))
            continue;

        if (%suitableStationCount > 0 && !isObject(%bot.gunnerWaitTarget) || %bot.gunnerWaitTarget.isDisabled())
        {
            %bot.gunnerWaitTarget = %suitableStations[%commander.lastAssignedVehicleStationNumber];

            // Generate a random vector from the center of the vehicle pad.
            %vector = getRandomVector(9, 15);
            %bot.gunnerWaitPoint = vectorAdd(%vector, %bot.gunnerWaitTarget.getWorldBoxCenter());
            %commander.lastAssignedVehicleStationNumber = %commander.lastAssignedVehicleStationNumber++ % %suitableStationCount;
            continue;
        }

        // If we're at a good waiting point, start loading into viable friendly vehicles
        if (%suitableVehicleCount > 0 && !isObject(%bot.gunnerMountTarget) && isObject(%bot.gunnerWaitTarget) && vectorDist(%bot.player.getWorldBoxCenter(), %bot.gunnerWaitPoint) > 5)
        {
            %bot.gunnerMountTarget = %suitableVehicles[%commander.lastAssignedVehicleNumber];
            %commander.lastAssignedVehicleNumber = %commander.lastAssignedVehicleNumber++ % %suitableVehicleCount;
        }
    }
}

function AIEnhancedGunnerTask::clientKilled(%this, %client)
{
    %client.gunnerMountTarget = "";
    %client.gunnerWaitTarget = "";
    %client.gunnerWaitPoint = "";
}
