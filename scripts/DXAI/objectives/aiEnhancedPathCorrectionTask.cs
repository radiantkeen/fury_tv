//------------------------------------------------------------------------------------------
// Description: A task that performs path correction.
//------------------------------------------------------------------------------------------`
function AIEnhancedPathCorrectionTask::initFromObjective(%task, %objective, %client) { }
function AIEnhancedPathCorrectionTask::assume(%task, %client) { %task.setMonitorFreq(2); }
function AIEnhancedPathCorrectionTask::retire(%task, %client) { }

function AIEnhancedPathCorrectionTask::weight(%task, %client)
{
    if (%client.isPathCorrecting)
        %task.setWeight($DXAI::Task::VeryHighPriority);
    else
        %task.setWeight($DXAI::Task::NoPriority);
}

function AIEnhancedPathCorrectionTask::monitor(%task, %client)
{
    if (%client.isPathCorrecting)
    {
        if (%client.player.getEnergyPercent() >= 1)
            %client.isPathCorrecting = false;
        else
            %client.setMoveTarget(-1);
    }

}

function AIEnhancedPathCorrectionTask::commanderMonitor(%bots)
{

}
