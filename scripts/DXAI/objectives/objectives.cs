//------------------------------------------------------------------------------------------
// main.cs
// Source file for the DXAI enhanced objective implementations.
// https://github.com/Ragora/T2-DXAI.git
//
// Copyright (c) 2015 Robert MacGregor
// This software is licensed under the MIT license. Refer to LICENSE.txt for more information.
//------------------------------------------------------------------------------------------

$DXAI::Task::NoPriority = 0;
$DXAI::Task::LowPriority = 100;
$DXAI::Task::MediumPriority = 200;
$DXAI::Task::HighPriority = 500;
$DXAI::Task::VeryHighPriority = 1000;
$DXAI::Task::ReservedPriority = 5000;

function ObjectiveNameToVoice(%bot)
{
    %objective = %bot.getTaskName();

    %result = "avo.grunt";
    switch$(%objective)
    {
      case "AIEnhancedReturnFlagTask":
        %result = "slf.def.flag";
      case "AIEnhancedRearmTask":
        %result = "need.cover";
      case "AIEnhancedEngageTarget":
        %result = "slf.att.attack";
      case "AIEnhancedScoutLocation":
        %result = "slf.def.defend";
      case "AIEnhancedDefendLocation":
        switch$(%bot.defenseDescription)
        {
            case "flag":
                %result = "slf.def.flag";
            case "generator":
                %result = "slf.def.generator";
            default:
                %result = "slf.def.defend";
        }
      case "AIEnhancedEscort":
        %result = "slf.tsk.cover";
      case "AIEnhancedGunnerTask":
        if (isObject(%bot.player.getObjectMount()))
            %result = "slf.def.defend";
        else
            %result = "need.ride";
    }

    return %result;
}

exec("scripts/DXAI/objectives/aiEnhancedDefendLocation.cs");
exec("scripts/DXAI/objectives/aiEnhancedEngageTarget.cs");
exec("scripts/DXAI/objectives/aiEnhancedEscort.cs");
exec("scripts/DXAI/objectives/aiEnhancedFlagCaptureTask.cs");
exec("scripts/DXAI/objectives/aiEnhancedFlagReturnTask.cs");
exec("scripts/DXAI/objectives/aiEnhancedPathCorrectionTask.cs");
exec("scripts/DXAI/objectives/aiEnhancedRearmTask.cs");
exec("scripts/DXAI/objectives/aiEnhancedScoutLocation.cs");
exec("scripts/DXAI/objectives/aiEnhancedGunnerTask.cs");
