//------------------------------------------------------------------------------------------
//      +Param %bot.scoutLocation: The X Y Z coordinates of the location that this bot
// must attempt to scout.
//      +Param %bot.scoutDistance: The maximum distance that this bot will attempt to scout
// out around %bot.scoutLocation.
//      +Description: The AIEnhancedScoutLocation does exactly as the name implies. The
// behavior a bot will exhibit with this code is that the bot will pick random nodes from
// the navigation graph that is within %bot.scoutDistance of %bot.scoutLocation and head
// to that chosen node. This produces a bot that will wander around the given location,
// including into and through interiors & other noded obstructions.
//------------------------------------------------------------------------------------------
function AIEnhancedScoutLocation::initFromObjective(%task, %objective, %client) { }
function AIEnhancedScoutLocation::assume(%task, %client) { %task.setMonitorFreq(32); %client.currentNode = -1; }
function AIEnhancedScoutLocation::retire(%task, %client) { }
function AIEnhancedScoutLocation::weight(%task, %client) { %task.setWeight($DXAI::Task::MediumPriority); }

function AIEnhancedScoutLocation::monitor(%task, %client)
{
    if (%client.engageTarget)
        return AIEnhancedScoutLocation::monitorEngage(%task, %client);

    // We can't really work without a NavGraph
    if (!isObject(NavGraph))
        return;

    // We just received the task, so find a node within distance of our scout location
    if (%client.currentNode == -1)
    {
        %client.currentNode = NavGraph.randNode(%client.scoutTargetLocation, %client.scoutDistance, true, true);

        if (%client.currentNode != -1)
            %client.setMoveTarget(NavGraph.nodeLoc(%client.currentNode));
    }
    // We're moving, or are near enough to our target
    else
    {
        %pathDistance = %client.getPathDistance(%client.moveTarget);

        // Don't move if we're close enough to our next node
        if (%pathDistance <= 40 && %client.isMovingToTarget)
        {
            %client.setMoveTarget(-1);
            %client.nextScoutRotation = getRandom(5000, 10000);
            %client.scoutTime += %task.getMonitorTimeMS();
        }
        else if(%client.getPathDistance(%client.moveTarget) > 40)
        {
          //  %client.setMoveTarget(%client.moveTarget);
            %client.scoutTime = 0;
        }
        else
            %client.scoutTime += %task.getMonitorTimeMS();

        // Wait a little bit at each node
        if (%client.scoutTime >= %client.nextScoutRotation)
        {
            %client.scoutTime = 0;
            %client.nextScoutRotation = getRandom(5000, 10000);

            // Pick a new node
            %client.currentNode = NavGraph.randNode(%client.scoutTargetLocation, %client.scoutDistance, true, true);

            // Ensure that we found a node.
            if (%client.currentNode != -1)
               %client.setMoveTarget(NavGraph.nodeLoc(%client.currentNode));
        }
    }
}

function AIEnhancedScoutLocation::monitorEngage(%task, %client)
{
}
