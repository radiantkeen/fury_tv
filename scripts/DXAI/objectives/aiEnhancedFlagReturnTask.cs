//------------------------------------------------------------------------------------------
// Description: A task that actually makes the bots return a flag that's nearby.
//------------------------------------------------------------------------------------------`
function AIEnhancedReturnFlagTask::initFromObjective(%task, %objective, %client) { }
function AIEnhancedReturnFlagTask::assume(%task, %client) { %task.setMonitorFreq(32); }
function AIEnhancedReturnFlagTask::retire(%task, %client) { }

function AIEnhancedReturnFlagTask::weight(%task, %client)
{
    %flag = nameToID("Team" @ %client.team @ "Flag1");
    if (!isObject(%flag) || %flag.isHome)
    {
      //  %client.setMoveTarget(-1);
        %task.setWeight($DXAI::Task::NoPriority);
    }
    else
    {
        // TODO: For now, all the bots go after it! Make this check if the bot is range.
        %task.setWeight($DXAI::Task::HighPriority);
        %client.returnFlagTarget = %flag;
    }
}

function AIEnhancedReturnFlagTask::monitor(%task, %client)
{
    if (!isObject(%client.returnFlagTarget))
        return;

    if (isObject(%client.engageTarget) && %client.engageTarget.getState() $= "Move")
        AIEnhancedReturnFlagTask::monitorEngage(%task, %client);
    else
        %client.setMoveTarget(%client.returnFlagTarget.getPosition());
}

function AIEnhancedReturnFlagTask::monitorEngage(%task, %client)
{
}
