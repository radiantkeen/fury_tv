//------------------------------------------------------------------------------------------
//      +Param %bot.engangeDistance: The maximum distance at which the bot will go out to
// attack a hostile.
//      +Param %bot.engageTarget: A manually assigned engage target to go after.
//      +Description: The AIEnhancedEngageTarget is a better implementation of the base
// AI engage logic.
//------------------------------------------------------------------------------------------`
function AIEnhancedEngageTarget::initFromObjective(%task, %objective, %client) { }
function AIEnhancedEngageTarget::assume(%task, %client) { %task.setMonitorFreq(1); }
function AIEnhancedEngageTarget::retire(%task, %client) { }

function AIEnhancedEngageTarget::weight(%task, %client)
{
    // Blow through seen targets
    %chosenTarget = -1;
    %chosenTargetDistance = 9999;

    %botPosition = %client.player.getPosition();
    for (%iteration = 0; %iteration < %client.visibleHostiles.getCount(); %iteration++)
    {
        %current = %client.visibleHostiles.getObject(%iteration);

        %targetDistance = vectorDist(%current.getPosition(), %botPosition);

        // FIXME: We immediately forget about the asshole here
        if (%targetDistance < %chosenTargetDistance && %client.player.canSeeObject(%current, %client.viewDistance, %client.fieldOfView))
        {
            %chosenTargetDistance = %targetDistance;
            %chosenTarget = %current;
        }
    }

    %client.engageTarget = %chosenTarget;
    if (!isObject(%client.engageTarget) && %client.engageTargetLastPosition $= "")
    {
        %task.setWeight($DXAI::Task::NoPriority);

        // Make sure we disable the pack
        if (%client.player.getPackName() $= "ShieldPack")
        {
            %client.player.setImageTrigger(2, false);
            %client.rechargingEnergy = false;
        }
    }
    else if (!isObject(%client.engageTarget) && %client.engageTargetLastPosition !$= "")
        %task.setWeight($DXAI::Task::LowPriority);
    else
    {
        %task.setWeight($DXAI::Task::VeryHighPriority);

        // If we have a shield pack on, use it
        if (%client.player.getPackName() $= "ShieldPack" && %client.player.getEnergyLevel() >= 30 && !%client.rechargingEnergy)
            %client.player.setImageTrigger(2, true);
        else if (%client.player.getPackName() $= "ShieldPack" && %client.player.getEnergyLevel() >= 70)
        {
            %client.player.setImageTrigger(2, true);
            %client.rechargingEnergy = false;
        }
        else if (%client.player.getPackName() $= "ShieldPack")
        {
            // We ran out of energy, let the pack recharge for a bit
            %client.player.setImageTrigger(2, false);
            %client.rechargingEnergy = true;
        }
    }
}

function AIEnhancedEngageTarget::monitor(%task, %client)
{
    if (isObject(%client.engageTarget))
    {
        if (!(%client.engageTarget.getType() & $TypeMasks::VehicleObjectType) && %client.engageTarget.getState() !$= "Move")
        {
            %client.engageTarget = -1;
            %client.engageTargetLastPosition = "";
            return;
        }

        // Calculate the T between the target and this bot
        %normal = vectorNormalize(vectorSub(%client.engageTarget.getWorldBoxCenter(), %client.player.getWorldBoxCenter()));

        %forwardAngle = mAtan(getWord(%normal, 1), getWord(%normal, 0));
        %horizontalMaxAngle = %forwardAngle + mDegToRad(90);
        %horizontalMinAngle = %forwardAngle - mDegToRad(90);

        %randomAngle = getRandomFloat(%horizontalMaxAngle, %horizontalMinAngle);

        // FIXME: Maintain weapon distance
        %minDist = 20;
        %maxDist = 30;

        %distance = getRandom(%minDist, %maxDist);

        // Calculate a final point
        %normal = mSin(%randomAngle) SPC mCos(%randomAngle);

        %targetPoint = vectorAdd(%client.engageTarget.getWorldBoxCenter(), vectorScale(%normal, %distance));
        %targetPoint = setWord(%targetPoint, 2, getTerrainHeight(%targetPoint));

        %targetPoint = vectorAdd(%targetPoint, "0 0 2");
        %client.engageTargetLastPosition = %client.engageTarget.getWorldBoxCenter();

        if (%client.engageJetTiming $= "")
            %client.engageJetTiming = getRandom(500, 10000);
        else
            %client.engageJetTime += %task.getMonitorTimeMS();

        if (%client.engageJetTime >= %client.engageJetTiming && %client.player.getEnergyPercent() >= 0.5)
        {
            %client.combatJetTiming = getRandom(1000, 1500) + %client.engageJetTiming;
            %client.runJets(%client.combatJetTiming);

            %client.engageJetTiming = "";
            %client.engageJetTime = 0;

            %client.isCombatJetting = true;

            %client.setMoveTarget(vectorScale(%client.engageTarget.getBackwardsVector(), 20));
        }

        if (%client.isCombatJetting && %client.combatJetTime < %client.combatJetTiming && %client.player.getEnergyPercent() >= 0.2)
        {
            %client.combatJetTime += %task.getMonitorTimeMS();
            %client.setMoveTarget(vectorScale(%client.engageTarget.getBackwardsVector(), 20));
        }
        else
        {
            %client.setMoveTarget(%targetPoint);

            %client.combatJetTime = 0;
            %client.isCombatJetting = false;
        }
    }
    else if (%client.engageTargetLastPosition !$= "")
    {
        %client.setMoveTarget(%client.engageTargetLastPosition);

        if (vectorDist(%client.player.getPosition(), %client.engageTargetLastPosition) <= 10)
        {
            %client.engageTargetLastPosition = "";
            %client.setMoveTarget(-1);
        }
    }
}

function AIEnhancedEngageTarget::commanderMonitor(%commander, %bots)
{

}
