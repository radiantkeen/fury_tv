//------------------------------------------------------------------------------------------
//      +Param %bot.defendLocation: The X Y Z coordinates of the location that this bot
// must attempt to defend.
//      +Description: The AIEnhancedDefendLocation does exactly as the name implies. The
// behavior a bot will exhibit with this code is that the bot will attempt to first to
// the location desiginated by %bot.defendLocation. Once the bot is in range, it will
// idly step about near the defense location, performing a sort of short range scouting.
// If the bot were to be knocked too far away, then this logic will simply start all over
// again.
//------------------------------------------------------------------------------------------
function AIEnhancedDefendLocation::initFromObjective(%task, %objective, %client) { }
function AIEnhancedDefendLocation::assume(%task, %client) { %task.setMonitorFreq(32); }
function AIEnhancedDefendLocation::retire(%task, %client) { %client.isMoving = false; }
function AIEnhancedDefendLocation::weight(%task, %client) { %task.setWeight($DXAI::Task::MediumPriority); }

function AIEnhancedDefendLocation::monitor(%task, %client)
{
    if (%client.getPathDistance(%client.defendTargetLocation) <= 40)
    {
        // Pick a random time to move to a nearby location
        if (%client.defendTime == -1)
        {
            %client.nextDefendRotation = getRandom(5000, 10000);
            %client.setMoveTarget(-1);
        }

        // If we're near our random point, just don't move
        if (%client.getPathDistance(%client.moveLocation) <= 10)
            %client.setMoveTarget(-1);

        %client.defendTime += %task.getMonitorTimeMS();
        if (%client.defendTime >= %client.nextDefendRotation)
        {
            %client.defendTime = 0;
            %client.nextDefendRotation = getRandom(5000, 10000);

            %nextPosition = NavGraph.nodeLoc(NavGraph.randNode(%client.player.getPosition(), 40, true, true));

            // If it isn't far enough, just pass on moving. This will help prevent bots jumping up in the air randomly.
            if (vectorDist(%client.player.getPosition(), %nextPosition) > 5)
                %client.setMoveTarget(%nextPosition);
        }
    }
    else
    {
        %client.defendTime = -1;
        %client.setMoveTarget(%client.defendTargetLocation);
    }
}
