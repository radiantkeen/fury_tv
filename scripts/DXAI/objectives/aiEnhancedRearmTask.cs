//------------------------------------------------------------------------------------------
//      +Param %bot.shouldRearm: A boolean representing whether or not this bot should go
// and rearm.
//      +Param %bot.rearmTarget: The ID of the inventory station to rearm at.
//------------------------------------------------------------------------------------------`
function AIEnhancedRearmTask::initFromObjective(%task, %objective, %client) { }
function AIEnhancedRearmTask::assume(%task, %client) { %task.setMonitorFreq(32); }
function AIEnhancedRearmTask::retire(%task, %client) { }

function AIEnhancedRearmTask::weight(%task, %client)
{
    if (%client.shouldRearm)
        %task.setWeight($DXAI::Task::HighPriority);
    else
    {
        %client.rearmTarget = "";
        %task.setWeight($DXAI::Task::NoPriority);
    }

    %task.setMonitorFreq(getRandom(10, 32));
}

function AIEnhancedRearmTask::monitor(%task, %client)
{
    if (!isObject(%client.rearmTarget))
        return;

    // Politely wait if someone is already on it.
    if (vectorDist(%client.rearmTarget.getPosition(), %client.player.getPosition()) <= 7 && isObject(%client.rearmTarget.triggeredBy))
        %client.setMoveTarget(-1);
    else
        %client.setMoveTarget(%client.rearmTarget.getPosition());
}

function AIEnhancedRearmTask::commanderMonitor(%commander, %bots)
{
    // Find all suitable inventory stations
    %suitableStationCount = 0;

    // FIXME: Find closest inventories first and only do striping when things are overloaded
    for (%iteration = 0; %iteration < $AIInvStationSet.getCount(); %iteration++)
    {
        %inventoryStation = $AIInvStationSet.getObject(%iteration);
        if (%inventoryStation.team <= 0 || %inventoryStation.team == %commander.team)
        {
            if (!%inventoryStation.isDisabled() && %inventoryStation.isPowered())
            {
                %suitableStations[%suitableStationCount] = %inventoryStation;
                %suitableStationCount++;
            }
        }
    }

    // Distribute the stations
    if (%suitableStationCount > 0)
    {
        if (%commander.lastAssignedInventoryStationNumber $= "")
            %commander.lastAssignedInventoryStationNumber = 0;

        for (%iteration = 0; %iteration < %bots.getCount(); %iteration++)
        {
            %bot = %bots.getObject(%iteration);

            if (!isObject(%bot.rearmTarget) || %bot.rearmTarget.isDisabled())
            {
                %bot.rearmTarget = %suitableStations[%commander.lastAssignedInventoryStationNumber];
                %commander.lastAssignedInventoryStationNumber = %commander.lastAssignedInventoryStationNumber++ % %suitableStationCount;
            }
        }
    }
}

function AIEnhancedRearmTask::clientKilled(%this, %client)
{
    %client.rearmTarget = "";
}
