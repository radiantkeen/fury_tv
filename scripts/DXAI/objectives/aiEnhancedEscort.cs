//------------------------------------------------------------------------------------------
//      +Param %bot.escortTarget: The ID of the object to escort. This can be literally
// any object that exists in the game world.
//      +Description: The AIEnhancedDefendLocation does exactly as the name implies. The
// behavior a bot will exhibit with this code is that the bot will attempt to first to
// the location desiginated by %bot.defendLocation. Once the bot is in range, it will
// idly step about near the defense location, performing a sort of short range scouting.
// If the bot were to be knocked too far away, then this logic will simply start all over
// again.
//------------------------------------------------------------------------------------------
function AIEnhancedEscort::initFromObjective(%task, %objective, %client) { }
function AIEnhancedEscort::assume(%task, %client) { %task.setMonitorFreq(1); }
function AIEnhancedEscort::retire(%task, %client) { %client.isMoving = false; %client.manualAim = false; }
function AIEnhancedEscort::weight(%task, %client) { %task.setWeight($DXAI::Task::MediumPriority); }

function AIEnhancedEscort::monitor(%task, %client)
{
    // Is our escort object even a thing?
    if (!isObject(%client.escortTarget))
        return;

    %escortLocation = %client.escortTarget.getPosition();

    // Pick a location near the target
    // FIXME: Only update randomly every so often, or perhaps update using the target's move direction & velocity?
    // TODO: Keep a minimum distance from the escort target, prevents crowding and accidental vehicular death.
    %client.isMoving = true;
    %client.manualAim = true;
    %client.aimLocation = %escortLocation;

    %client.setMoveTarget(getRandomPositionOnTerrain(%escortLocation, 40));
}

function AIEnhancedEscort::commanderMonitor(%commander, %bots)
{

}
