//------------------------------------------------------------------------------------------
// loadouts.cs
// Source file declaring usable loadouts for the bots and mapping them to their most
// appropriate tasks.
// https://github.com/Ragora/T2-DXAI.git
//
// Copyright (c) 2015 Robert MacGregor
// This software is licensed under the MIT license. Refer to LICENSE.txt for more information.
//------------------------------------------------------------------------------------------

$DXAI::Loadouts[0, "Name"] = "Light Scout";
$DXAI::Loadouts[0, "Weapon", 0] = Blaster;
$DXAI::Loadouts[0, "Weapon", 1] = Disc;
$DXAI::Loadouts[0, "Weapon", 2] = Plasma;
$DXAI::Loadouts[0, "Pack"] = EnergyPack;
$DXAI::Loadouts[0, "WeaponCount"] = 3;
$DXAI::Loadouts[0, "Armor"] = "Light";

$DXAI::Loadouts[1, "Name"] = "Light Defender";
$DXAI::Loadouts[1, "Weapon", 0] = Chaingun;
$DXAI::Loadouts[1, "Weapon", 1] = Disc;
$DXAI::Loadouts[1, "Weapon", 2] = Plasma;
$DXAI::Loadouts[1, "Pack"] = ShieldPack;
$DXAI::Loadouts[1, "WeaponCount"] = 3;
$DXAI::Loadouts[1, "Armor"] = "Light";

$DXAI::Loadouts[2, "Name"] = "Medium Defender";
$DXAI::Loadouts[2, "Weapon", 0] = Scattergun;
$DXAI::Loadouts[2, "Weapon", 1] = Mortar;
$DXAI::Loadouts[2, "Weapon", 2] = Chaingun;
$DXAI::Loadouts[2, "Weapon", 3] = Plasma;
$DXAI::Loadouts[2, "Pack"] = ShieldPack;
$DXAI::Loadouts[2, "WeaponCount"] = 4;
$DXAI::Loadouts[2, "Armor"] = "Medium";

$DXAI::Loadouts[3, "Name"] = "Heavy Support Gunner";
$DXAI::Loadouts[3, "Weapon", 0] = Bolter;
$DXAI::Loadouts[3, "Weapon", 1] = RocketLauncher;
$DXAI::Loadouts[3, "Weapon", 2] = HeavyBlaster;
$DXAI::Loadouts[3, "Weapon", 3] = Mortar;
$DXAI::Loadouts[3, "Weapon", 4] = Howitzer;
$DXAI::Loadouts[3, "Pack"] = SensorJammerPack;
$DXAI::Loadouts[3, "WeaponCount"] = 5;
$DXAI::Loadouts[3, "Armor"] = "Heavy";

$DXAI::Loadouts[4, "Name"] = "Heavy Defender";
$DXAI::Loadouts[4, "Weapon", 0] = Minigun;
$DXAI::Loadouts[4, "Weapon", 1] = RocketLauncher;
$DXAI::Loadouts[4, "Weapon", 2] = GrenadeLauncher;
$DXAI::Loadouts[4, "Weapon", 3] = Mortar;
$DXAI::Loadouts[4, "Weapon", 4] = PlasmaCannon;
$DXAI::Loadouts[4, "Pack"] = ShieldPack;
$DXAI::Loadouts[4, "WeaponCount"] = 5;
$DXAI::Loadouts[4, "Armor"] = "Heavy";

$DXAI::Loadouts[5, "Name"] = "Cloaked Scout";
$DXAI::Loadouts[5, "Weapon", 0] = Chaingun;
$DXAI::Loadouts[5, "Weapon", 1] = Disc;
$DXAI::Loadouts[5, "Weapon", 2] = Shocklance;
$DXAI::Loadouts[5, "Pack"] = CloakingPack;
$DXAI::Loadouts[5, "WeaponCount"] = 3;
$DXAI::Loadouts[5, "Armor"] = "Light";

$DXAI::Loadouts[6, "Name"] = "Sniper Gunner";
$DXAI::Loadouts[6, "Weapon", 0] = SniperRifle;
$DXAI::Loadouts[6, "Weapon", 1] = Plasma;
$DXAI::Loadouts[6, "Weapon", 2] = Chaingun;
$DXAI::Loadouts[6, "Pack"] = EnergyPack;
$DXAI::Loadouts[6, "WeaponCount"] = 3;
$DXAI::Loadouts[6, "Armor"] = "Light";

$DXAI::Loadouts[7, "Name"] = "Medium Ammo Gunner";
$DXAI::Loadouts[7, "Weapon", 0] = Chaingun;
$DXAI::Loadouts[7, "Weapon", 1] = Plasma;
$DXAI::Loadouts[7, "Weapon", 2] = RocketLauncher;
$DXAI::Loadouts[7, "Weapon", 3] = GaussRifle;
$DXAI::Loadouts[7, "Pack"] = AmmoPack;
$DXAI::Loadouts[7, "WeaponCount"] = 4;
$DXAI::Loadouts[7, "Armor"] = "Medium";

$DXAI::Loadouts[8, "Name"] = "Medium Energy Gunner";
$DXAI::Loadouts[8, "Weapon", 0] = Plasma;
$DXAI::Loadouts[8, "Weapon", 1] = Blaster;
$DXAI::Loadouts[8, "Weapon", 2] = Nullifier;
$DXAI::Loadouts[8, "Weapon", 3] = GaussRifle;
$DXAI::Loadouts[8, "Pack"] = EnergyPack;
$DXAI::Loadouts[8, "WeaponCount"] = 4;
$DXAI::Loadouts[8, "Armor"] = "Medium";

$DXAI::Loadouts[9, "Name"] = "Medium Support Gunner";
$DXAI::Loadouts[9, "Weapon", 0] = Chaingun;
$DXAI::Loadouts[9, "Weapon", 1] = Plasma;
$DXAI::Loadouts[9, "Weapon", 2] = ELFGun;
$DXAI::Loadouts[9, "Weapon", 3] = Fatman;
$DXAI::Loadouts[9, "Pack"] = RepairPack;
$DXAI::Loadouts[9, "WeaponCount"] = 4;
$DXAI::Loadouts[9, "Armor"] = "Medium";

$DXAI::Loadouts[10, "Name"] = "Heavy Ranged Gunner";
$DXAI::Loadouts[10, "Weapon", 0] = Howitzer;
$DXAI::Loadouts[10, "Weapon", 1] = MissileLauncher;
$DXAI::Loadouts[10, "Weapon", 2] = PlasmaCannon;
$DXAI::Loadouts[10, "Weapon", 3] = Bolter;
$DXAI::Loadouts[10, "Weapon", 4] = Mortar;
$DXAI::Loadouts[10, "Pack"] = AmmoPack;
$DXAI::Loadouts[10, "WeaponCount"] = 5;
$DXAI::Loadouts[10, "Armor"] = "Heavy";

$DXAI::Loadouts[11, "Name"] = "Light Support Gunner";
$DXAI::Loadouts[11, "Weapon", 0] = Blaster;
$DXAI::Loadouts[11, "Weapon", 1] = Disc;
$DXAI::Loadouts[11, "Weapon", 2] = ELFGun;
$DXAI::Loadouts[11, "Pack"] = RepairPack;
$DXAI::Loadouts[11, "WeaponCount"] = 3;
$DXAI::Loadouts[11, "Armor"] = "Light";

$DXAI::Loadouts[12, "Name"] = "Heavy Assault Gunner";
$DXAI::Loadouts[12, "Weapon", 0] = PlasmaCannon;
$DXAI::Loadouts[12, "Weapon", 1] = Minigun;
$DXAI::Loadouts[12, "Weapon", 2] = Scattergun;
$DXAI::Loadouts[12, "Weapon", 3] = PulseLaserCannon;
$DXAI::Loadouts[12, "Weapon", 4] = RocketLauncher;
$DXAI::Loadouts[12, "Pack"] = EnergyPack;
$DXAI::Loadouts[12, "WeaponCount"] = 5;
$DXAI::Loadouts[12, "Armor"] = "Heavy";

$DXAI::OptimalLoadouts["AIEnhancedGunnerTask"] = "3 6 7 8 9 10 11 12";
$DXAI::OptimalLoadouts["AIEnhancedFlagCaptureTask"] = "0";
$DXAI::OptimalLoadouts["AIEnhancedScoutLocation"] = "0 5";
$DXAI::OptimalLoadouts["AIEnhancedDefendLocation"] = "1 2 4 6 10 11";

// A default loadout to use when the bot has no objective.
$DXAI::DefaultLoadout = 0;

$DXAI::Loadouts::Count = 13;
$DXAI::Loadouts::Default = 0;
