// Sol Conflict Network Layer

if(!isObject(Net))
{
   System.Net = new ScriptObject(Net)
   {
      class = Net;

      // HTTP network variables
      isStatTracking = false;
      login = "radiantalpha.io";
      authToken = "NULL";

      // Network prefs
      packetRate = 32;
      packetSize = 600;
   };
   
   Net.connections = new SimSet();
   Net.schedule(60000, "checkConnections");
}

$pref::Net::PacketRateToClient = Net.packetRate;
$pref::Net::PacketSize = Net.packetSize;
$pref::Net::PacketRateToServer = Net.packetRate;

// Net Control Messages
// 1xxx control messages
$NET_STATUS_OK = 1000;
$NET_STATUS_LOGIN_SUCCESS = 1001;

// 2xxx data messages
$NET_STATUS_LOGIN = 2000;
$NET_STATUS_PLAYERDATA = 2001;
$NET_STATUS_PLAYERDROP = 2002;
$NET_STATUS_MATCHDATA = 2003;

// 3xxx error messages
$NET_STATUS_LOGINFAIL = 3000;
$NET_STATUS_AUTHFAIL = 3001;
$NET_STATUS_DBFAIL = 3002;
$NET_STATUS_LOGINDISABLE = 3003;

exec("scripts/Core/HTTPClient.cs");

// Garbage collection
function Net::checkConnections(%this)
{
    for(%c = 0; %c < %this.connections.getCount(); %c++)
    {
        %conn = %this.connections.getObject(%c);
        
        if(%conn.marked < 1)
        {
            %conn.marked = 1;
            continue;
        }

        %this.connections.remove(%conn);
        %conn.delete();
    }
    
    %this.schedule(60000, "checkConnections");
    //error("Net::checkConnections tick" SPC getSimTime());
}

function Net::registerObject(%this, %obj)
{
    if(%this.connections.isMember(%obj))
        return;
        
    %this.connections.add(%obj);
}

function Net::get(%this, %addr)
{
    %conn = new ScriptObject() { class = "HTTPClient"; };

    Net.registerObject(%conn);
    %conn.get(%addr);
}

// Generates random socket IDs
function Net::generateSocketID(%this)
{
    %id = "S"@generateRandomString(5);

    if(isObject(%id))
        return %this.generateSocketID();
    else
        return %id;
}

function Net::createConnection(%this, %addr)
{
    %conn = new ScriptObject() { class = "HTTPClient"; };

    %conn.connect(%addr);
    Net.registerObject(%conn);
    
    return %conn;
}

function Net::sendPacket(%this, %conn, %packet)
{
    %conn.sendPacket(%packet);
}

function Net::processPacket(%this, %packet)
{
    %pcmd = getField(%packet.payload, 0);
    Net.registerObject(%packet);

    if(strlen(%pcmd) < 1)
        return;
    
    switch$(%pcmd)
    {
        case $NET_STATUS_OK:
            return;

        case $NET_STATUS_PLAYERDATA:
            onLoadAccount(%packet.payload);

        case $NET_STATUS_LOGIN_SUCCESS:
            %this.authToken = getField(%packet.payload, 1);
            %this.isStatTracking = true;
            error("Logged into Fury: TV Master System.");

        case $NET_STATUS_LOGINFAIL:
            %this.isStatTracking = false;
            error("Login credentials incorrect, server not connected to Fury: TV Master System.");

        case $NET_STATUS_AUTHFAIL:
            error("Authentication failed for last communication with the Fury: TV Master System.");

        case $NET_STATUS_DBFAIL:
            error("Error: Database failure on the Fury: TV Master System, try again in a few moments.");

        case $NET_STATUS_LOGINDISABLE:
            %this.isStatTracking = false;
            error("Fury: TV Master System reports this server's login has been disabled.");

        default:
            echo("Master server message:" SPC %packet.payload);
    }
}
