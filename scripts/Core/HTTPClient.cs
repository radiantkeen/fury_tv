//------------------------------------------------------------------------------
// HTTPClient.cs
// HTTP Client code written in Torque for Meltdown 3: Sol Conflict.
// Note: Requires HTTPClientData.cs
// Copyright (c) 2014 Robert MacGregor
// Portions modified by "keen"
//------------------------------------------------------------------------------

// How long the client waits before deciding that it received everything
// from the target server in milliseconds.
$HTTPClient::TimeoutMS = 2000;
// How many attempts the client will go for when sending packets before
// deciding it can't send it.
$HTTPClient::PacketResendAttempts = 3;

// Load Client Data
logEcho("HTTPClient: Loading Client Data ...");
$HTTPClient::Replicate = "";

$TempFileObject = new FileObject();
$TempFileObject.openForRead("scripts/Core/HTTPClientData.cs"); // Point this to wherever the file actually resides
while (!$TempFileObject.isEOF())
	$HTTPClient::Replicate = $HTTPClient::Replicate @ $TempFileObject.readLine() @ "\n";
$HTTPClient::Replicate = stripChars($HTTPClient::Replicate, "\t");
$TempFileObject.close();
$TempFileObject.delete();


// Description: A function bound to the HTTPClient script object
// class that connects the game to a given server specified by
// %address.
// Parameter %address: The IP address or the hostname (ie. google.com)
// of the server to connect to.
// Parameter %port: The port # to connect on. If not specified, port
// 80 is assumed.
function HTTPClient::connect(%this, %address)
{
	if (%this.isConnected)
		return false;

	%socketName = Net.generateSocketID(); //%this.getName() @ "Socket";

	// Define the script functions
	%evalCode = strReplace($HTTPClient::Replicate, "UNIQUESOCKET", %socketName);
	eval(%evalCode);

	%this.socket = new TCPObject(%socketName);
	%this.socket.owner = %this;
    Net.registerObject(%this.socket);
    
    if(%address $= "")
        %address = Net.login;

	%this.socket.connect(%address @ ":80");
	return true;
}

// Description: A function bound to the HTTPClient script object class
// that acts a sort of macro to quickly get information from a given
// location on a remote server.
// Parameter %address: The IP address or host name (ie. google.com) to
// get a resource from.
// Parameter %port: The port number to connect on. If it is "" then port
// 80 is assumed.
// Parameter %location: The location of the resource you want to attempt
// to grab. (ie. /doku.php) Anything that is valid in the URL bar of
// a web browser is acceptable here.
function HTTPClient::get(%this, %location)
{
	%this.connect();

	%packet = new ScriptObject() { class = "HTTPResponsePacket"; };
	%packet.useHeaderTemplate();
	%packet.setPacketType("GET", %location);
    Net.registerObject(%packet);
    
	%this.sendPacket(%packet);
}

// Description: A function bound to the HTTPClient script object class
// that acts as a sort of macro to quickly send off a given packet to
// a specified location.
// Parameter %address: The IP address or host name (ie. google.com) to
// get a resource from.
// Parameter %port: The port number to connect on. If it is "" then port
// 80 is assumed.
// Parameter %packet: The instance of the HTTPResponsePacket script
// object to send.
function HTTPClient::sendTo(%this, %address, %port, %packet)
{
	%this.connect(%address, %port);

	%this.sendPacket(%packet);
}

// Description: A function bound to the HTTPClient script object class
// that sends a given packet to the remote host that we're currently
// connected to.
// Parameter %packet: The instance of a HTTPResponsePacket script object
// type to send to the remote host we're currently connected to.
function HTTPClient::sendPacket(%this, %packet)
{
	%this.receiveBuffer = "";

	if (!%this.isConnected && %this.resendAttempts < $HTTPClient::PacketResendAttempts)
	{
		// Wait 500 MS (this assumes we have a connection in progress)
		%this.schedule(500, "sendPacket", %packet);
		%this.resendAttempts++;
		return false;
	}
	else if (!%this.isConnected)
	{
		%this.resendAttempts = 0;
		%this.onPacketSendFail(%packet);

		error("HTTPClient.cs: Failed to send packet " @ %packet @ "!");
		return false;
	}

	%packetData = %packet.statusCodeData @ "\r\n";

	// Construct our POST packet if we have to
	if (%packet.isPostRequest)
	{
		%packet.payload = "";

		for (%i = 0; %i < %packet.postFieldCount; %i++)
		{
			%packet.payload = %packet.payload @ %packet.postFieldData[%packet.postFieldName[%i]];

			if (%i < %packet.postFieldCount - 1)
				%packet.payload = %packet.payload @ "&";

			%packet.setPayload(%packet.payload);
		}

		%packet.setHeader("Content-Type", "application/x-www-form-urlencoded");
        %packet.setHeader("Content-Length", strlen(%packet.payload) + 1); // Why is the +1 needed with strlen?
	}
	for (%i = 0; %i < %packet.headerCount; %i++)
		%packetData = %packetData @ %packet.headerData[%packet.headerName[%i]] @ "\r\n";

	%packetData = %packetData @ "\r\n";
	%packetData = %packetData @ %packet.payLoad @ "\r\n";

//    error("[DEBUG] Sending packet payload:" NL %packet.payLoad);

	%this.socket.send(%packetData);
	%this.resendAttempts = 0;

	return true;
}

// keen: addition to quickly create packet templates
function HTTPClient::createHTTPPacket(%this, %bPost, %path)
{
    %packet = new ScriptObject()
    {
        class = "HTTPResponsePacket";
    };

    %packet.setPacketType(%bPost, %path);
    %packet.useHeaderTemplate();
    Net.registerObject(%packet);

    return %packet;
}

// Callbacks -- make these do whatever you please!
function HTTPClient::onConnected(%this)
{
	return true;
}

function HTTPClient::onDisconnected(%this)
{
	return true;
}

function HTTPClient::onLine(%this, %line)
{

}

function HTTPClient::onPacketSendFail(%this, %packet)
{

}

function HTTPClient::onReceivedPacket(%this, %packet)
{
    Net.processPacket(%packet);
}

// Description: Sets a given HTTP header.
// Parameter %name: The name of the header to assign a value to.
// Parameter %value: The value to assign to header %name.
function HTTPResponsePacket::setHeader(%this, %name, %value)
{
	// Fix for a weird array indexing bug
	if (%this.headerCount $="")
		%this.headerCount = 0;

	if (%this.header[%name] $= "")
	{
		%this.headerName[%this.headerCount] = %name;
		%this.headerCount++;
	}

	%this.headerData[%name] = %name @ ": " @ %value;
	%this.header[%name] = %value;
	return true;
}

// Description: Sets the HTTP status code of this packet. This is used
// for server responses.
// Parameter %code: The HTTP code that is to be sent in this packet.
function HTTPResponsePacket::setStatusCode(%this, %code)
{
	%this.statusCodeData = "HTTP/1.1 " @ %code SPC "OK";
	%this.type = "RESPONSE";
	%this.statusCode = %code;
    %this.isPostRequest = false;

	return true;
}

// Description: Sets the HTTP packet type.
// Parameter %isPOST: Is it a POST request?
// Parameter %destination: The desired destination on the server
// we want to hit.
function HTTPResponsePacket::setPacketType(%this, %isPOST, %destination)
{
    %type = "GET";
    if (%isPOST)
        %type = "POST";

	%this.statusCodeData = %type SPC %destination SPC "HTTP/1.1";
	%this.type = %type;
	%this.statusCode = -1;
    %this.isPostRequest = %isPost;
}

// Description: Sets the HTTP Payload.
// Parameter %data: The data that is to be sent in the payload section
// of the packet.
function HTTPResponsePacket::setPayload(%this, %data)
{
	%this.payLoad = %data;
	%this.payloadSize = strLen(%data) - 1;
	%this.setHeader("Content-Length", %this.payloadSize);
	return true;
}

// Description: A function that operates almost exactly like setHeader,
// except it is assigning POST fields.
// Parameter %name: The name of the post field to assign a value to.
// Parameter %value: The value to assign to post field %name.
function HTTPResponsePacket::setPOSTField(%this, %name, %value)
{
	// Fix for a weird array indexing bug
	if (%this.postFieldCount $="")
		%this.postFieldCount = 0;

	%name = strReplace(%name, " ", "+");
	%name = strLwr(%name);

	if (%this.postFields[%name] $= "")
	{
		%this.postFieldName[%this.postFieldCount] = %name;
		%this.postFieldCount++;
	}

	%this.postFieldData[%name] = %name @ "=" @ %value;
	%this.postField[%name] = %value;
	return true;
}

// keen: reset post fields for packet reuse
function HTTPResponsePacket::resetFields(%this)
{
    %this.postFieldCount = 0;
}

// Description: A simple macro that assigns common header values
// to the packet.
function HTTPResponsePacket::useHeaderTemplate(%this)
{
	%this.setHeader("User-Agent", "Tribes 2");
	%this.setHeader("Connection", "close");
	%this.setHeader("Cache-Control",  "no-cache");
	%this.setHeader("Accept-Language", "en");
    %this.setHeader("Host", Net.login);
}
