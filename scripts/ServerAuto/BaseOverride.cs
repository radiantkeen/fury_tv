function StaticShapeData::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType)
{
   if(%data.noDamageInMod == true)
      return;
      
   // if this is a non-team mission type and the object is "protected", don't damage it
   if(%data.noIndividualDamage && Game.allowsProtectedStatics())
      return;

   // if this is a Siege mission and this object shouldn't take damage (e.g. vehicle stations)
   if(%data.noDamageInSiege && Game.class $= "SiegeGame")
      return;

   if(%targetObject.lastHitFlags & $Projectile::RepairProjectile)
   {
       %targetObject.setDamageLevel(%targetObject.getDamageLevel() - %amount);
       zapObject(%targetObject, "FXRedShift");
       return;
   }

   if(%sourceObject && %targetObject.isEnabled())
   {
      if(%sourceObject.client)
      {
        %targetObject.lastDamagedBy = %sourceObject.client;
        %targetObject.lastDamagedByTeam = %sourceObject.client.team;
        %targetObject.damageTimeMS = GetSimTime();
      }
      else
      {
        %targetObject.lastDamagedBy = %sourceObject;
        %targetObject.lastDamagedByTeam = %sourceObject.team;
        %targetObject.damageTimeMS = GetSimTime();
      }
   }

    // ShapeBaseExtension handles shields for staticshapes
   if(%data.isShielded && (%targetObject.lastHitFlags & $Projectile::IgnoreShields) == 0)
   {
      if(%targetObject.shieldSource > 0)
          %amount = %data.onShieldDamage(%targetObject.shieldSource, %position, %amount, %damageType);
      else
          %amount = %data.onShieldDamage(%targetObject, %position, %amount, %damageType);
   }

   if(%targetObject.lastHitFlags & $Projectile::Nullifier)
       %amount = 0;

   if(%targetObject.lastHitFlags & $Projectile::Energy)
       %amount *= 0.75;

   %amount *= %targetObject.damageReduceFactor;

   if(%targetObject.lastHitFlags & $Projectile::ArmorOnly)
       %amount *= 2;
       
//   %damageScale = %data.damageScale[%damageType];
//   if(%damageScale !$= "")
//      %amount *= %damageScale;

    //if team damage is off, cap the amount of damage so as not to disable the object...
    if (!$TeamDamage && !%targetObject.getDataBlock().deployedObject)
    {
       // -------------------------------------
       // z0dd - ZOD, 6/24/02. Console spam fix
       if(isObject(%sourceObject))
       {
          //see if the object is being shot by a friendly
          if(%sourceObject.getDataBlock().catagory $= "Vehicles")
             %attackerTeam = getVehicleAttackerTeam(%sourceObject);
          else
             %attackerTeam = %sourceObject.team;
      }
      if ((%targetObject.getTarget() != -1) && isTargetFriendly(%targetObject.getTarget(), %attackerTeam))
      {
         %curDamage = %targetObject.getDamageLevel();
         %availableDamage = %targetObject.getDataBlock().disabledLevel - %curDamage - 0.05;
         if (%amount > %availableDamage)
            %amount = %availableDamage;
      }
    }

   // if there's still damage to apply
   if (%amount > 0)
      %targetObject.applyDamage(%amount);
}

function stationTrigger::onEnterTrigger(%data, %obj, %colObj)
{
	//make sure it's a player object, and that that object is still alive
   if(!%colObj.isPlayer() || %colObj.getState() $= "Dead")
      return;

   // z0dd - ZOD, 7/13/02 Part of hack to keep people from mounting
   // vehicles in disallowed armors.
   if(%obj.station.getDataBlock().getName() !$= "StationVehicle")
      %colObj.client.inInv = true;

   %colObj.inStation = true;
   commandToClient(%colObj.client,'setStationKeys', true);
   if(Game.stationOnEnterTrigger(%data, %obj, %colObj)) {
      //verify station.team is team associated and isn't on player's team
      if((%obj.mainObj.team != %colObj.client.team) && (%obj.mainObj.team != 0))
      {
         //%obj.station.playAudio(2, StationAccessDeniedSound);
         messageClient(%colObj.client, 'msgStationDenied', '\c2Access Denied -- Wrong team.~wfx/powered/station_denied.wav');
      }
      else if(%obj.disableObj.isDisabled())
      {
         messageClient(%colObj.client, 'msgStationDisabled', '\c2Station is disabled.');
      }
      else if(%obj.station.getDataBlock().getName() !$= "StationVehicle" && !%obj.mainObj.isPowered())
      {
         messageClient(%colObj.client, 'msgStationNoPower', '\c2Station is not powered.');
      }
      else if(%obj.station.notDeployed)
      {
         messageClient(%colObj.client, 'msgStationNotDeployed', '\c2Station is not deployed.');
      }
      else if(%obj.station.triggeredBy $= "")
      {
         if(%obj.station.getDataBlock().setPlayersPosition(%obj.station, %obj, %colObj))
         {
            messageClient(%colObj.client, 'CloseHud', "", 'inventoryScreen');
            commandToClient(%colObj.client, 'TogglePlayHuds', true);
            %obj.station.triggeredBy = %colObj;
            %obj.station.getDataBlock().stationTriggered(%obj.station, 1);
            %colObj.station = %obj.station;
            %colObj.lastWeapon = ( %colObj.getMountedImage($WeaponSlot) == 0 ) ? "" : %colObj.getMountedImage($WeaponSlot).getName().item;
            %colObj.unmountImage($WeaponSlot);
         }
      }
   }
}

function RepairKit::onUse(%data,%obj)
{
   //----------------------------------------------------------------------------
   // z0dd - ZOD, 8/10/02. Let players use repair kit regardless of health status
   // if they choose so via client $pref::
   if (%obj.client.wasteRepKit == 1)
   {
      %obj.decInventory(%data,1);
      messageClient(%obj.client, 'MsgRepairKitUsed', '\c2Repair Kit Used.');
      if (%obj.getDamageLevel() != 0)
      {
         %obj.applyRepair(1.25);
      }
   }
   else
   {
      // Don't use the kit unless we're damaged
      if (%obj.getDamageLevel() != 0)
      {
         %obj.applyRepair(1.25);
         %obj.decInventory(%data,1);
         messageClient(%obj.client, 'MsgRepairKitUsed', '\c2Repair Kit Used.');
      }
   }
}

function DefaultGame::equip(%game, %player)
{
//    echo("New player:" SPC %player);

   for(%i =0; %i<$InventoryHudCount; %i++)
      %player.client.setInventoryHudItem($InventoryHudData[%i, itemDataName], 0, 1);
   %player.client.clearBackpackIcon();

   //%player.setArmor("Light");
   %player.setInventory(RepairKit,999);
   %player.setInventory(Grenade,999);
   %player.setInventory(Blaster,1);
   %player.setInventory(Disc,1);
   %player.setInventory(Chaingun, 1);
   %player.setInventory(ChaingunAmmo, 999);
   %player.setInventory(DiscAmmo, 999);
   %player.setInventory(Beacon, 3);
   %player.setInventory(TargetingLaser, 1);
   %player.setInventory(RepairPack, 1);
   %player.weaponCount = 3;

   %player.use("Blaster");
}

function DefaultGame::gameOver( %game )
{
   //set the bool
   $missionRunning = false;

   CancelCountdown();
   CancelEndCountdown();

   //loop through all the clients, and do any cleanup...
   %count = ClientGroup.getCount();
   for (%i = 0; %i < %count; %i++)
   {
      %client = ClientGroup.getObject(%i);
      %player = %client.player;

        // Copy all points to last* variables for sending to stats server
        %client.lastscore = %client.score;
        %client.lastkills = %client.kills;
        %client.lastdeaths = %client.deaths;
        %client.lastassists = %client.assists;
        %client.lastsuicides = %client.suicides;
        %client.lastteamkills = %client.teamkills;
        %client.lastpilotkills = %client.pilotkills;
        %client.lastgunnerkills = %client.gunnerkills;

      // z0dd - ZOD, 6/13/02. Need to remove this for random teams by Founder (founder@mechina.com).
      if($CurrentMissionType $= TR2) // z0dd - ZOD, 9/17/02. Check for Team Rabbit 2
         %client.lastTeam = %client.team;

      if ( !%client.isAiControlled() )
      {
         %client.endMission();
         messageClient( %client, 'MsgClearDebrief', "" );
         %game.sendDebriefing( %client );

         if(%client.player.isBomber)
            commandToClient(%client, 'endBomberSight');

         //clear the score hud...
         messageClient( %client, 'SetScoreHudHeader', "", "" );
         messageClient( %client, 'SetScoreHudSubheader', "", "");
         messageClient( %client, 'ClearHud', "", 'scoreScreen', 0 );

         // clean up the players' HUDs:
         %client.setWeaponsHudClearAll();
         %client.setInventoryHudClearAll();
      }
   }
   // z0dd - ZOD, 6/22/02. Setup random teams by Founder (founder@mechina.com).
   if($CurrentMissionType !$= TR2) // z0dd - ZOD, 9/17/02. Check for Team Rabbit 2
      %game.setupClientTeams();

   // Default game does nothing...  except lets the AI know the mission is over
   AIMissionEnd();
}

function DefaultGame::displayDeathMessages(%game, %clVictim, %clKiller, %damageType, %implement)
{
   // ----------------------------------------------------------------------------------
   // z0dd - ZOD, 6/18/02. From Panama Jack, send the damageTypeText as the last varible
   // in each death message so client knows what weapon it was that killed them.

   // Force undefined damage types as the general type
   if(%damageType $= "")
      %damageType = $DamageType::General;

   %victimGender = (%clVictim.sex $= "Male" ? 'him' : 'her');
   %victimPoss = (%clVictim.sex $= "Male" ? 'his' : 'her');
   %killerGender = (%clKiller.sex $= "Male" ? 'him' : 'her');
   %killerPoss = (%clKiller.sex $= "Male" ? 'his' : 'her');
   %victimName = %clVictim.name;
   %killerName = %clKiller.name;
   //error("DamageType = " @ %damageType @ ", implement = " @ %implement @ ", implement class = " @ %implement.getClassName() @ ", is controlled = " @ %implement.getControllingClient());

   if(%damageType == $DamageType::Explosion)
   {
      messageAll('msgExplosionKill', $DeathMessageExplosion[mFloor(getRandom() * $DeathMessageExplosionCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") killed by a nearby explosion.");
   }
   else if(%damageType == $DamageType::Suicide)  //player presses cntrl-k
   {
      messageAll('msgSuicide', $DeathMessageSuicide[mFloor(getRandom() * $DeathMessageSuicideCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") committed suicide (CTRL-K)");
   }
	else if(%damageType == $DamageType::VehicleSpawn)
	{
      messageAll('msgVehicleSpawnKill', $DeathMessageVehPad[mFloor(getRandom() * $DeathMessageVehPadCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") killed by vehicle spawn");
	}
	else if(%damageType == $DamageType::ForceFieldPowerup)
	{
      messageAll('msgVehicleSpawnKill', $DeathMessageFFPowerup[mFloor(getRandom() * $DeathMessageFFPowerupCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") killed by Force Field Powerup");
	}
	else if(%damageType == $DamageType::Crash)
	{
      messageAll('msgVehicleCrash', $DeathMessageVehicleCrash[%damageType, mFloor(getRandom() * $DeathMessageVehicleCrashCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") crashes a vehicle.");
	}
	else if(%damageType == $DamageType::Impact) // run down by vehicle
	{
		if( ( %controller = %implement.getControllingClient() ) > 0)
		{
	      %killerGender = (%controller.sex $= "Male" ? 'him' : 'her');
	      %killerPoss = (%controller.sex $= "Male" ? 'his' : 'her');
	      %killerName = %controller.name;
			messageAll('msgVehicleKill', $DeathMessageVehicle[mFloor(getRandom() * $DeathMessageVehicleCount)], %victimName, %victimGender, %victimPoss, %killerName ,%killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
	      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") killed by a vehicle controlled by "@%controller);
		}
		else
		{
			messageAll('msgVehicleKill', $DeathMessageVehicleUnmanned[mFloor(getRandom() * $DeathMessageVehicleUnmannedCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
	      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") killed by a vehicle (unmanned)");
		}
	}
   // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   // z0dd - ZOD, 5/15/02. Added Hover Vehicle so we get proper
   // death messages when killed with Wildcat chaingun
   //else if (isObject(%implement) && (%implement.getClassName() $= "Turret" || %implement.getClassName() $= "VehicleTurret" || %implement.getClassName() $= "FlyingVehicle"))   //player killed by a turret
   else if (isObject(%implement) && (%implement.getClassName() $= "Turret" || %implement.getClassName() $= "VehicleTurret" || %implement.getClassName() $= "FlyingVehicle" || %implement.getClassName() $= "HoverVehicle"))
   {
      if (%implement.getControllingClient() != 0)  //is turret being controlled?
      {
         %controller = %implement.getControllingClient();
         %killerGender = (%controller.sex $= "Male" ? 'him' : 'her');
         %killerPoss = (%controller.sex $= "Male" ? 'his' : 'her');
         %killerName = %controller.name;

         if (%controller == %clVictim)
            messageAll('msgTurretSelfKill', $DeathMessageTurretSelfKill[mFloor(getRandom() * $DeathMessageTurretSelfKillCount)],%victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
         else if (%controller.team == %clVictim.team) //controller TK'd a friendly
            messageAll('msgCTurretKill', $DeathMessageCTurretTeamKill[%damageType, mFloor(getRandom() * $DeathMessageCTurretTeamKillCount)],%victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
         else //controller killed an enemy
            messageAll('msgCTurretKill', $DeathMessageCTurretKill[%damageType, mFloor(getRandom() * $DeathMessageCTurretKillCount)],%victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
         logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") killed by a turret controlled by "@%controller);
      }
      // use the handle associated with the deployed object to verify valid owner
      else if (isObject(%implement.owner))
      {
         %owner = %implement.owner;
         //error("Owner is " @ %owner @ "   Handle is " @ %implement.ownerHandle);
         //error("Turret is still owned");
         //turret is uncontrolled, but is owned - treat the same as controlled.
         %killerGender = (%owner.sex $= "Male" ? 'him' : 'her');
         %killerPoss = (%owner.sex $= "Male" ? 'his' : 'her');
         %killerName = %owner.name;

         if (%owner.team == %clVictim.team)  //player got in the way of a teammates deployed but uncontrolled turret.
            messageAll('msgCTurretKill', $DeathMessageCTurretAccdtlKill[%damageType,mFloor(getRandom() * $DeathMessageCTurretAccdtlKillCount)],%victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
         else  //deployed, uncontrolled turret killed an enemy
            messageAll('msgCTurretKill', $DeathMessageCTurretKill[%damageType,mFloor(getRandom() * $DeathMessageCTurretKillCount)],%victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
         logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") was killed by turret (automated)");
      }
      else  //turret is not a placed (owned) turret (or owner is no longer on it's team), and is not being controlled
      {
         messageAll('msgTurretKill', $DeathMessageTurretKill[%damageType,mFloor(getRandom() * $DeathMessageTurretKillCount)],%victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
         logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") killed by turret");
      }
   }
   else if((%clKiller == %clVictim) || (%damageType == $DamageType::Ground)) //player killed himself or fell to death
   {
      messageAll('msgSelfKill', $DeathMessageSelfKill[%damageType,mFloor(getRandom() * $DeathMessageSelfKillCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      logEcho(%clVictim.nameBase @ "(pl " @ %clVictim.player @ "/cl " @ %clVictim @ ") killed self (" @ getTaggedString($DamageTypeText[%damageType]) @ ")");
   }

   else if (%damageType == $DamageType::OutOfBounds) //killer died due to Out-of-Bounds damage
   {
      messageAll('msgOOBKill', $DeathMessageOOB[mFloor(getRandom() * $DeathMessageOOBCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") killed by out-of-bounds damage");
   }

   else if (%damageType == $DamageType::NexusCamping) //Victim died from camping near the nexus...
   {
      messageAll('msgCampKill', $DeathMessageCamping[mFloor(getRandom() * $DeathMessageCampingCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") killed for nexus camping");
   }

   else if(%clKiller.team == %clVictim.team) //was a TK
   {
      messageAll('msgTeamKill', $DeathMessageTeamKill[%damageType, mFloor(getRandom() * $DeathMessageTeamKillCount)],  %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") teamkilled by "@%clKiller.nameBase@" (pl "@%clKiller.player@"/cl "@%clKiller@")");
   }

   else if (%damageType == $DamageType::Lava)   //player died by falling in lava
   {
      messageAll('msgLavaKill',  $DeathMessageLava[mFloor(getRandom() * $DeathMessageLavaCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") killed by lava");
   }
   else if ( %damageType == $DamageType::Lightning )  // player was struck by lightning
   {
      messageAll('msgLightningKill',  $DeathMessageLightning[mFloor(getRandom() * $DeathMessageLightningCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") killed by lightning");
   }
   else if ( %damageType == $DamageType::Mine && !isObject(%clKiller) )
   {
         error("Mine kill w/o source");
         messageAll('MsgRogueMineKill', $DeathMessageRogueMine[%damageType, mFloor(getRandom() * $DeathMessageRogueMineCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
   }
   else  //was a legitimate enemy kill
   {
      if(%damageType == 6 && (%clVictim.headShot))
      {
         // laser headshot just occurred
         messageAll('MsgHeadshotKill', $DeathMessageHeadshot[%damageType, mFloor(getRandom() * $DeathMessageHeadshotCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      }
      // ----------------------------------------------------
      // z0dd - ZOD, 8/25/02. Rear Lance hits
      else if (%damageType == 10 && (%clVictim.rearshot))
      {
         // shocklance rearshot just occurred
         messageAll('MsgRearshotKill', $DeathMessageRearshot[%damageType, mFloor(getRandom() * $DeathMessageRearshotCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      }
      // ----------------------------------------------------
      else
         messageAll('MsgLegitKill', $DeathMessage[%damageType, mFloor(getRandom() * $DeathMessageCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      logEcho(%clVictim.nameBase @ " (pl " @ %clVictim.player @ "/cl " @ %clVictim @ ") killed by " @ %clKiller.nameBase @ " (pl " @ %clKiller.player @ "/cl " @%clKiller @ ") using " @ getTaggedString($DamageTypeText[%damageType]));
   }
}

function DefaultGame::sendGameVoteMenu( %game, %client, %key )
{
   %isAdmin = ( %client.isAdmin || %client.isSuperAdmin );
   %multipleTeams = %game.numTeams > 1;

   // no one is going anywhere until this thing starts
   if($MatchStarted)
   {
      // Client options:
      if ( %client.team != 0 )
      {
         if ( %multipleTeams )
            if( !$Host::TournamentMode )
               messageClient( %client, 'MsgVoteItem', "", %key, 'ChooseTeam', "", 'Change your Team' );
         messageClient( %client, 'MsgVoteItem', "", %key, 'MakeObserver', "", 'Become an Observer' );
      }
      else
      {
         if(!%multipleTeams && !$Host::TournamentMode)
            messageClient( %client, 'MsgVoteItem', "", %key, 'JoinGame', "", 'Join the Game' );
      }

      //%totalSlots = $Host::maxPlayers - ($HostGamePlayerCount + $HostGameBotCount);
     // if( $HostGameBotCount > 0 && %totalSlots > 0 && %client.isAdmin)
         //messageClient( %client, 'MsgVoteItem', "", %key, 'Addbot', "", 'Add a Bot' );
   }

   if( !%client.canVote && !%isAdmin )
      return;

   // z0dd - ZOD, 9/29/02. Removed T2 demo code from here

   if ( %game.scheduleVote $= "" )
   {
      if(!%client.isAdmin)
      {
         // Actual vote options:
         messageClient( %client, 'MsgVoteItem', "", %key, 'VoteChangeMission', 'change the mission to', 'Vote to Change the Mission' );

         if( $Host::TournamentMode )
         {
            messageClient( %client, 'MsgVoteItem', "", %key, 'VoteFFAMode', 'Change server to Free For All.', 'Vote Free For All Mode' );

            if(!$MatchStarted && !$CountdownStarted)
               messageClient( %client, 'MsgVoteItem', "", %key, 'VoteMatchStart', 'Start Match', 'Vote to Start the Match' );
         }
         else
            messageClient( %client, 'MsgVoteItem', "", %key, 'VoteTournamentMode', 'Change server to Tournament.', 'Vote Tournament Mode' );

         if ( %multipleTeams )
         {
            if(!$MatchStarted && !$Host::TournamentMode)
               messageClient( %client, 'MsgVoteItem', "", %key, 'ChooseTeam', "", 'Change your Team' );

            if ( $teamDamage )
               messageClient( %client, 'MsgVoteItem', "", %key, 'VoteTeamDamage', 'disable team damage', 'Vote to Disable Team Damage' );
            else
               messageClient( %client, 'MsgVoteItem', "", %key, 'VoteTeamDamage', 'enable team damage', 'Vote to Enable Team Damage' );
         }
         
         if($Host::HavocMode)
             messageClient( %client, 'MsgVoteItem', "", %key, 'VoteHavocMode', 'disable Havoc Mode', 'Vote to Disable Havoc Mode' );
         else
             messageClient( %client, 'MsgVoteItem', "", %key, 'VoteHavocMode', 'enable Havoc Mode', 'Vote to Enable Havoc Mode' );
             
         if($Host::GroundIsDeadly)
             messageClient( %client, 'MsgVoteItem', "", %key, 'VoteDeadlyGroundMode', 'disable Deadly Ground Mode', 'Vote to Disable Deadly Ground' );
         else
             messageClient( %client, 'MsgVoteItem', "", %key, 'VoteDeadlyGroundMode', 'enable Deadly Ground Mode', 'Vote to Enable Deadly Ground' );
      }
      else
      {
         // Actual vote options:
         messageClient( %client, 'MsgVoteItem', "", %key, 'VoteChangeMission', 'change the mission to', 'Change the Mission' );

         if( $Host::TournamentMode )
         {
            messageClient( %client, 'MsgVoteItem', "", %key, 'VoteFFAMode', 'Change server to Free For All.', 'Free For All Mode' );

            if(!$MatchStarted && !$CountdownStarted)
               messageClient( %client, 'MsgVoteItem', "", %key, 'VoteMatchStart', 'Start Match', 'Start Match' );
         }
         else
            messageClient( %client, 'MsgVoteItem', "", %key, 'VoteTournamentMode', 'Change server to Tournament.', 'Tournament Mode' );

         if ( %multipleTeams )
         {
            if(!$MatchStarted)
               messageClient( %client, 'MsgVoteItem', "", %key, 'ChooseTeam', "", 'Choose Team' );

            if ( $teamDamage )
               messageClient( %client, 'MsgVoteItem', "", %key, 'VoteTeamDamage', 'disable team damage', 'Disable Team Damage' );
            else
               messageClient( %client, 'MsgVoteItem', "", %key, 'VoteTeamDamage', 'enable team damage', 'Enable Team Damage' );
         }
         
         if($Host::HavocMode)
             messageClient( %client, 'MsgVoteItem', "", %key, 'VoteHavocMode', 'disable Havoc Mode', 'Disable Havoc Mode' );
         else
             messageClient( %client, 'MsgVoteItem', "", %key, 'VoteHavocMode', 'enable Havoc Mode', 'Enable Havoc Mode' );

         if($Host::GroundIsDeadly)
             messageClient( %client, 'MsgVoteItem', "", %key, 'VoteDeadlyGroundMode', 'disable Deadly Ground Mode', 'Disable Deadly Ground' );
         else
             messageClient( %client, 'MsgVoteItem', "", %key, 'VoteDeadlyGroundMode', 'enable Deadly Ground Mode', 'Enable Deadly Ground' );
      }
   }

   // Admin only options:
   if ( %client.isAdmin )
   {
      messageClient( %client, 'MsgVoteItem', "", %key, 'VoteChangeTimeLimit', 'change the time limit', 'Change the Time Limit' );
      messageClient( %client, 'MsgVoteItem', "", %key, 'VoteResetServer', 'reset server defaults', 'Reset the Server' );

      // -----------------------------------------------------------------------------
      // z0dd - ZOD, 5/12/02. Add bot menu for admins
      %totalSlots = $Host::maxPlayers - ($HostGamePlayerCount + $HostGameBotCount);
      if( $HostGameBotCount > 0 && %totalSlots > 0)
         messageClient( %client, 'MsgVoteItem', "", %key, 'Addbot', "", 'Add a Bot' );
      // -----------------------------------------------------------------------------
   }
}

function DefaultGame::evalVote(%game, %typeName, %admin, %arg1, %arg2, %arg3, %arg4)
{
   switch$ (%typeName)
   {
      case "voteChangeMission":
         %game.voteChangeMission(%admin, %arg1, %arg2, %arg3, %arg4);

      case "voteTeamDamage":
         %game.voteTeamDamage(%admin, %arg1, %arg2, %arg3, %arg4);

      case "voteTournamentMode":
         %game.voteTournamentMode(%admin, %arg1, %arg2, %arg3, %arg4);

      case "voteMatchStart":
         %game.voteMatchStart(%admin, %arg1, %arg2, %arg3, %arg4);

      case "voteFFAMode":
         %game.voteFFAMode(%admin, %arg1, %arg2, %arg3, %arg4);

      case "voteChangeTimeLimit":
         %game.voteChangeTimeLimit(%admin, %arg1, %arg2, %arg3, %arg4);

      case "voteResetServer":
         %game.voteResetServer(%admin, %arg1, %arg2, %arg3, %arg4);

      case "voteKickPlayer":
         %game.voteKickPlayer(%admin, %arg1, %arg2, %arg3, %arg4);

      case "voteAdminPlayer":
         %game.voteAdminPlayer(%admin, %arg1, %arg2, %arg3, %arg4);

      case "voteGreedMode":
         %game.voteGreedMode(%admin, %arg1, %arg2, %arg3, %arg4);

      case "voteHoardMode":
         %game.voteHoardMode(%admin, %arg1, %arg2, %arg3, %arg4);
         
      case "VoteHavocMode":
         %game.voteHavocMode(%admin, %arg1, %arg2, %arg3, %arg4);

      case "VoteDeadlyGroundMode":
         %game.voteDeadlyGroundMode(%admin, %arg1, %arg2, %arg3, %arg4);
   }
}

function DefaultGame::VoteHavocMode(%game, %admin)
{
   if(%admin)
   {
      if($Host::HavocMode)
      {
         messageAll('MsgAdminForce', '\c2The Admin has disabled Havoc mode. All vehicles now available.');
         $Host::HavocMode = 0;
      }
      else
      {
         messageAll('MsgAdminForce', '\c2The Admin has enabled Havoc mode. Only multi-passenger vehicles allowed.');
         $Host::HavocMode = 1;
      }
   }
   else
   {
      %totalVotes = %game.totalVotesFor + %game.totalVotesAgainst;
      if(%totalVotes > 0 && (%game.totalVotesFor / (ClientGroup.getCount() - $HostGameBotCount)) > ($Host::VotePasspercent / 100))
      {
         if($Host::HavocMode)
         {
            messageAll('MsgVotePassed', '\c2Havoc mode was disabled by vote. All vehicles now available.');
            $Host::HavocMode = 0;
         }
         else
         {
            messageAll('MsgVotePassed', '\c2Havoc mode was enabled by vote. Only multi-passenger vehicles allowed.');
            $Host::HavocMode = 1;
         }
      }
      else
      {
         if($Host::HavocMode)
            messageAll('MsgVoteFailed', '\c2Disable Havoc mode vote did not pass: %1 percent.', mFloor(%game.totalVotesFor/(ClientGroup.getCount() - $HostGameBotCount) * 100));
         else
            messageAll('MsgVoteFailed', '\c2Enable Havoc mode vote did not pass: %1 percent.', mFloor(%game.totalVotesFor/(ClientGroup.getCount() - $HostGameBotCount) * 100));
      }
   }
}

function DefaultGame::VoteDeadlyGroundMode(%game, %admin)
{
   if(%admin)
   {
      if($Host::GroundIsDeadly)
      {
         messageAll('MsgAdminForce', '\c2The Admin has disabled deadly ground mode. The ground is safe to touch.');
         $Host::GroundIsDeadly = 0;
      }
      else
      {
         messageAll('MsgAdminForce', '\c2The Admin has enabled deadly ground mode. The ground will kill you on touch.');
         $Host::GroundIsDeadly = 1;
      }
   }
   else
   {
      %totalVotes = %game.totalVotesFor + %game.totalVotesAgainst;
      if(%totalVotes > 0 && (%game.totalVotesFor / (ClientGroup.getCount() - $HostGameBotCount)) > ($Host::VotePasspercent / 100))
      {
         if($Host::GroundIsDeadly)
         {
            messageAll('MsgVotePassed', '\c2Deadly ground mode was disabled by vote. The ground is safe to touch.');
            $Host::GroundIsDeadly = 0;
         }
         else
         {
            messageAll('MsgVotePassed', '\c2Deadly ground was enabled by vote. The ground will kill you on touch.');
            $Host::GroundIsDeadly = 1;
         }
      }
      else
      {
         if($Host::GroundIsDeadly)
            messageAll('MsgVoteFailed', '\c2Disable deadly ground mode vote did not pass: %1 percent.', mFloor(%game.totalVotesFor/(ClientGroup.getCount() - $HostGameBotCount) * 100));
         else
            messageAll('MsgVoteFailed', '\c2Enable deadly ground mode vote did not pass: %1 percent.', mFloor(%game.totalVotesFor/(ClientGroup.getCount() - $HostGameBotCount) * 100));
      }
   }
}
