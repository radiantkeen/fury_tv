//=====================================================================================
// scriptSpawn.cs
//
// Author: Robert MacGregor
// License: MIT License
//
// A script reimplementation of the spawn graphing logic in Tribes 2. This aims to
// efficiently replace the broken graphing system for spawn graphs as some maps will
// cause the game to crash and others cause the game to generate apparently invalid
// spawn data in the file.
//=====================================================================================

// Whether or not to always force script spawn to be active even if the current map has valid spawn data
$ScriptSpawn::ForceActive = false;
// The step size between raycasts spinning about the horizontal circle.
$ScriptSpawn::Stepping::HorizontalSize = mDegToRad(10);
// The step size between raycasts spinning about the vertical circle.
$ScriptSpawn::Stepping::VerticalSize = mDegToRad(5);
// The minimum alignment with 0 0 1 that the points should be.
$ScriptSpawn::Alignment::Minimum = 0.5;
// The maximum number of object piercings on a single ray before we stop.
$ScriptSpawn::Piercing::MaxDepth = 50;
// The maximum number of attempts to pierce an object before giving up on a ray.
$ScriptSpawn::Piercing::MaxAttemptCount = 5;
// The amount of distance to move the raycast when testing pierces
$ScriptSpawn::Piercing::NudgeAmount = 0.5;

function SimGroup::getObjectByName(%this, %name)
{
    %name = strLwr(%name);
    for (%iteration = 0; %iteration < %this.getCount(); %iteration++)
    {
        %object = %this.getObject(%iteration);

        if (strLwr(%object.getName()) $= %name)
            return %object;
    }
    return -1;
}

function ScriptSpawn::generateSpawnData(%graphSpawns)
{
    %startTime = getRealTime();

    error("Generating all spawn data " @ %this @ " at " @ formatTimeString("mm/dd/yy hh:nn:ss A"));

    // Locate all viable spawn sphere locations
    %spawnCount = 0;
    for (%teamID = 0; %teamID <= Game.numTeams; %teamID++)
    {
        %teamGroup = "Team" @ %teamID;

        if (isObject(%teamGroup))
        {
            %spawnGroup = %teamGroup.getObjectByName("spawnspheres");

            if (isObject(%spawnGroup))
                for (%iteration = 0; %iteration < %spawnGroup.getCount(); %iteration++)
                {
                    %object = %spawnGroup.getObject(%iteration);

                    if (%object.getClassName() $= "SpawnSphere")
                    {
                        %spawnCount++;
                        %object.generateSpawnData(%graphSpawns);
                    }
                }
        }
    }

    %deltaTime = getRealTime() - %startTime;
    error("Completed graphing " @ %spawnCount @ " spawn spheres in " @ (%deltaTime / 1000) @ " sec");
    error("-----------------------------------------------------------");
}

//====================================================================================
//
function SpawnSphere::chooseLocation(%this)
{
    if (!%this.spawnDataGenerated)
        %this.generateSpawnData();

    %totalSpawns = %this.viableOutdoorPointCount + %this.viableIndoorPointCount;
    %totalWeight = %this.indoorWeight + %this.outdoorWeight;
    %outdoorWeight = %this.outdoorWeight / %totalWeight;

    %randomWeight = getRandom(0, %totalWeight) / %totalWeight;
    %isOutdoor = %randomWeight <= %outdoorWeight;

    %spawnHigh = %isOutdoor ? %this.viableOutdoorPointCount : %this.viableIndoorPointCount;
    %spawnID = getRandom(0, %spawnHigh);

    %currentSpawnCount = 0;
    for (%iteration = 0; %iteration < %totalSpawns; %iteration++)
        if (%this.viablePointsOutdoors[%iteration] == %isOutdoor)
            if (%currentSpawnCount == %spawnID)
                return %isOutdoor SPC %this.viablePoints[%iteration];
            else
                %currentSpawnCount++;
    return -1;
}

//====================================================================================
// Description: Generates spawn data for this spawn sphere. When complete, the
// SpawnSphere object will have the following data available on it:
//
// spawnDataGenerated - A flag used to signal that the data is available for spawn.
// horizontalStepCount - The number of entries for the horizontal
//====================================================================================
function SpawnSphere::generateSpawnData(%this, %graphSpawns)
{
    %this.viableOutdoorPointCount = 0;
    %this.viableIndoorPointCount = 0;
    %this.spawnDataGenerated = false;

    error("-----------------------------------------------------------");
    error("Generating spawn data for sphere " @ %this @ " at " @ formatTimeString("mm/dd/yy hh:nn:ss A"));
    error("Radius - " @ %this.radius);
    error("Indoor Weight - " @ %this.indoorWeight);
    error("Outdoor Weight - " @ %this.outdoorWeight);

    %startTime = getRealTime();

    %maximumHorizontalSteps = mCeil((3.14 * 2) / $ScriptSpawn::Stepping::HorizontalSize);
    %maximumVerticalSteps = mCeil(3.14 / $ScriptSpawn::Stepping::VerticalSize);

    error("Maximum Horizontal Steps: " @ %maximumHorizontalSteps);
    error("Maximum Vertical Steps: " @ %maximumVerticalSteps);

    // Spin about the horizontal circle
    for (%horizontalStep = 0; %horizontalStep <= %maximumHorizontalSteps; %horizontalStep++)
    {
        %horizontalAngle = %horizontalStep * $ScriptSpawn::Stepping::HorizontalSize;
        %horizontalAngle = %horizontalAngle > (3.14 * 2) ? (3.14 * 2) : %horizontalAngle;

        %rayNormalX = mSin(%horizontalAngle);
        %rayNormalY = mCos(%horizontalAngle);

        // Spin about the vertical circle
        for (%verticalStep = -%maximumVerticalSteps; %verticalStep <= %maximumVerticalSteps; %verticalStep++)
        {
            %verticalAngle = %verticalStep * $ScriptSpawn::Stepping::VerticalSize;
            %verticalAngle = %verticalAngle > 3.14 ? 3.14 : %verticalAngle;

            %rayNormalZ = mCos(%verticalAngle);
            %rayNormal = %rayNormalX SPC %rayNormalY SPC %rayNormalZ;

            // Perform the raycast and attempt piercing
            %currentPierceObject = -1;
            %startPosition = %this.getPosition();
            %currentRayPosition = %startPosition;

            // Attempt to loop for each successful pierce
            for (%rayDepth = 0; %rayDepth <= $ScriptSpawn::Piercing::MaxDepth; %rayDepth++)
            {
                // Attempt to perform piercing
                if (isObject(%currentPierceObject))
                {
                    %piercedObject = false;
                    for (%pierceAttempt = 0; %pierceAttempt <= $ScriptSpawn::Piercing::MaxAttemptCount; %pierceAttempt++)
                    {
                        %pierceEndPosition = vectorAdd(%currentRayPosition, vectorScale(%rayNormal, $ScriptSpawn::Piercing::NudgeAmount));
                        %pierceTest = containerRayCast(%currentRayPosition, %pierceEndPosition, -1, -1);

                        %hitObject = getWord(%pierceTest, 0);
                        if (%hitObject == %currentPierceObject)
                            %currentRayPosition = %pierceEndPosition;
                        else
                        {
                            %piercedObject = true;
                            break;
                        }
                    }

                    // Attempt a new raycast or exit
                    %currentPierceObject = -1;
                    if (!%piercedObject)
                    {
                        error("!!! Gave up on a ray due to too many failed pierce attempts!");
                        break;
                    }
                }

                %rayDistance = vectorDist(%currentRayPosition, %startPosition);

                // Break off once we hit the radius
                if (%rayDistance >= %this.radius)
                    break;

                %rayLength = %this.radius - %rayDistance;
                %endPosition = vectorAdd(%currentRayPosition, vectorScale(%rayNormal, %rayLength));

                %rayCast = containerRayCast(%currentRayPosition, %endPosition, -1, -1);
                %hitObject = getWord(%rayCast, 0);

                // If there is no hit object, try a raycast straight downwards to plot ground positions
                if (!isObject(%hitObject))
                {
                    %rayCast = containerRayCast(%endPosition, vectorAdd(%endPosition, vectorScale("0 0 -1", %this.radius)), -1, -1);
                    %hitObject = getWord(%rayCast, 0);

                    %rayDistance = %this.radius;
                    %currentRayPosition = %endPosition;
                    %currentPierceObject = -1;
                }
                else
                    %currentPierceObject = %hitObject;

                // Create packs at each point for now.
                if (isObject(%hitObject))
                {
                    %hitPosition = getWords(%rayCast, 1, 3);
                    %hitNormal = getWords(%rayCast, 4, 6);
                    %currentRayPosition = %hitPosition;

                    // Calculate how aligned this angle is with 0 0 1
                    %alignment = vectorDot(%hitNormal, "0 0 1");

                    if (%alignment >= $ScriptSpawn::Alignment::Minimum)
                    {
                        %totalSpawns = %this.viableOutdoorPointCount + %this.viableIndoorPointCount;
                        %this.viablePoints[%totalSpawns] = %hitPosition;

                        %isOutdoor = %hitObject.getType() & $TypeMasks::TerrainObjectType;
                        %this.viablePointsOutdoors[%totalSpawns] = %isOutdoor != 0;

                        if (%isOutdoor)
                            %this.viableOutdoorPointCount++;
                        else
                            %this.viableIndoorPointCount++;

                        if (%graphSpawns)
                            %pack = new Item()
                            {
                                position = %hitPosition;
                                datablock = RepairPack;
                                static = true;
                                rotate = true;
                            };
                    }
                }

                // If we advanced to the radius because of no objects, end the tunneling
                if (%rayDistance >= %this.radius)
                    break;
            }
        }
    }

    error("Finished generating spawn data for sphere " @ %this @ " at " @ formatTimeString("mm/dd/yy hh:nn:ss A"));

    %deltaTime = getRealTime() - %startTime;
    error("Completed in " @ (%deltaTime / 1000) @ " sec");
    error("-----------------------------------------------------------");

    %this.spawnDataGenerated = true;
}

package SpawnHooks
{
    function DefaultGame::pickTeamSpawn(%game, %team)
    {
        if (navGraphExists() && !$ScriptSpawn::ForceActive)
            return parent::pickTeamSpawn(%game, %team);

        for (%attempt = 0; %attempt < 20; %attempt++)
        {
            %sphere = %game.selectSpawnSphere(%team);
            if (%sphere == -1)
            {
                echo("No spawn spheres found for team " @ %team);
                return -1;
            }

            %radius = %sphere.radius;
            %sphereTrans = %sphere.getTransform();
            %sphereCtr = getWord(%sphereTrans, 0) @ " " @ getWord(%sphereTrans, 1) @ " " @ getWord(%sphereTrans, 2);   //don't need full transform here, just x, y, z

            %avoidThese = $TypeMasks::VehicleObjectType | $TypeMasks::MoveableObjectType |
                            $TypeMasks::PlayerObjectType | $TypeMasks::TurretObjectType;

            %chosenLocation = -1;
            for (%tries = 0; %tries < 10; %tries++)
            {
                %loc = %sphere.chooseLocation();
                %adjUp = VectorAdd(%loc, "0 0 1.0");   // don't go much below

                if (ContainerBoxEmpty(%avoidThese, %adjUp, 2.0))
                {
                    %chosenLocation = %loc;
                    break;
                }
            }

            if (%chosenLocation != -1)
            {
                %zone = getWord(%chosenLocation, 0) ? "outdoor" : "indoor";
                %chosenLocation = getWords(%chosenLocation, 1, 3);
                %rot = %game.selectSpawnFacing(%chosenLocation, %sphereCtr, %zone);
                %spawnLoc = %chosenLocation @ %rot;
                return %spawnLoc;
            }
        }
    }
};
if (!isActivePackage(SpawnHooks))
    activatePackage(SpawnHooks);
