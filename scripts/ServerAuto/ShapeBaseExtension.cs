//-----------------------------------------------------------------------------
// ShapeBase Extension
//--------------------------------------

function ShapeBase::setPowerLevel(%obj, %amount)
{
    %energyPct = %amount / %obj.maxPower;
    %obj.setEnergyLevel(%healthPct * %obj.getDatablock().maxEnergy);
}

function ShapeBase::getPowerLevel(%obj)
{
    return mCeil(%obj.getEnergyLevel() * 100);
}

function ShapeBase::playHitIndicator(%obj, %enemy)
{
    %client = 0;
    
    if(%obj.client > 0)
        %client = %obj.client;
    else if((%cl = %obj.getControllingClient()))
        %client = %cl;
        
    if(%client && !%client.isAIControlled())
    {
        if(%enemy == 2)
        {
            commandToClient(%client, 'ShowDeploySensor', "75 125 255", 384);
            return;
        }
        
        if(%enemy)
            messageClient(%client, 'MsgClientHit', %client.projectileHitWav);

        %client.displayHitIndicator(%enemy);
    }
}

// Legacy code in case it's needed - functionality moved clientside
//function GameConnection::displayHitIndicator(%client, %enemy)
//{
//    if(%client.hitDisplay > 0)
//        cancel(%client.hitDisplay);
//
//    if(%enemy)
//        activateDeploySensorGrn(%client.player);
//    else
//        activateDeploySensorRed(%client.player);
//
//    %client.hitDisplay = %client.schedule(256, "clearHitIndicator");
//}

//function GameConnection::clearHitIndicator(%client)
//{
//    %client.hitDisplay = 0;
//    deactivateDeploySensor(%client.player);
//}

function GameConnection::displayHitIndicator(%client, %enemy)
{
    %color = %enemy ? "255 255 255" : "255 0 0";

    commandToClient(%client, 'ShowDeploySensor', %color, 384);
}

function ShapeBaseData::onAdd(%data, %obj)
{
   Parent::onAdd(%data, %obj);
	// if it's a deployed object, schedule the ambient thread to play in a little while
   if(%data.deployAmbientThread)
	   %obj.schedule(750, "playThread", $AmbientThread, "ambient");
	// check for ambient animation that should always be played
	if(%data.alwaysAmbient)
		%obj.playThread($AmbientThread, "ambient");

    // keen: MD3 initial stuff
    %obj.isWet = false;
    %obj.isFragmented = false;
    
    %obj.damageReduceFactor = 1.0;
    %obj.shieldAbsorbFactor = 1.0;
    %obj.damageBuffFactor = 1.0;
    %obj.energyEfficiencyFactor = 1.0;
    %obj.bonusWeight = 0;
    %obj.spreadFactorBase = 1.0;
    %obj.rateOfFire = 1.0;
    %obj.maxAmmoCapacityFactor = 1.0;
    %obj.rechargeRateFactor = 1.0;
    %obj.shieldRechargeFactor = 1.0;
    %obj.damageReduction = 0;
}

function GameBaseData::onAdd(%data, %obj)
{
   if(%data.targetTypeTag !$= "")
   {
      // use the name given to the object in the mission file
      if(%obj.nameTag !$= "")
      {
         %obj.nameTag = addTaggedString(%obj.nameTag);
         %nameTag = %obj.nameTag;
      }
      else
         %nameTag = %data.targetNameTag;

   	%obj.target = createTarget(%obj, %nameTag, "", "", %data.targetTypeTag, 0, 0);
   }
   else
      %obj.target = -1;
}

function ShapeBase::getObjectSlot(%this, %obj)
{
    for(%i = 0; %i < 16; %i++)
    {
        if(%this.getMountNodeObject(%i) == %obj)
            return %i;
    }
    
    return -1;
}

// Shield damage for both players and staticshapes/turrets/sensors/etc (armor.cs/baseoverride.cs)
function ShapeBaseData::onShieldDamage(%data, %targetObject, %position, %amount, %damageType)
{
    %energy = %targetObject.getEnergyLevel();

    // Passthrough if no shields or if shield ignore
    if(%energy < 1 || %targetObject.lastHitFlags & ($Projectile::IgnoreShields | $Projectile::Phaser))
        return %amount;

    if(%targetObject.lastHitFlags & ($Projectile::ArmorOnly | $Projectile::Corrosive))
       return 0;

    if(%targetObject.lastHitFlags & $Projectile::Nullifier)
        %amount *= 2;

    if(%targetObject.lastHitFlags & $Projectile::PartialShieldPhase)
        %amount *= 0.5;

    if(%targetObject.lastHitFlags & $Projectile::Explosive)
        %amount *= 0.2;

    if(%targetObject.lastHitFlags & $Projectile::Kinetic)
        %amount *= 0.75;

    %amount *= %targetObject.shieldAbsorbFactor;
    %strength = %energy / %data.energyPerDamagePoint;
    %remainder = 0;
    
    if(%amount * %targetObject.shieldAbsorbFactor <= %strength)
    {
       // Shield absorbs all
       %lost = %amount * %targetObject.shieldAbsorbFactor * %data.energyPerDamagePoint;
        
       if(%targetObject.lastHitFlags & $Projectile::PartialShieldPhase)
       {
           %remainder = %lost * 0.5;
           %lost *= 0.5;
       }

       %energy -= %lost;
       
       %targetObject.setEnergyLevel(%energy);
       %targetObject.playShieldEffect("0.0 0.0 1.0");
    }
    else
    {
        // Shield exhausted
        %targetObject.setEnergyLevel(0);

        if(%targetObject.lastHitFlags & $Projectile::Nullifier)
            %remainder = 0;
        else
            %remainder = %amount - %strength / %targetObject.shieldAbsorbFactor;
    }
    
    return %remainder;
}
