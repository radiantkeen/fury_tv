//----------------------------------------------------------------------------
// Score HUD Override

function ScoreScreen::__construct(%this)
{
    ScoreScreen.Version = 1.0;
}

function ScoreScreen::__destruct(%this)
{
   // Thou shalt not spam
}

if(!isObject(ScoreScreen))
   System.addClass(ScoreScreen);

// String table for menus
$Menu::Main = 0;
$MDMenuName[0] = "Main Menu";

$Menu::Score = 1;
$MDMenuName[1] = "Score Menu";

$Menu::VehicleTeleport = 2;
$MDMenuName[2] = "Vehicle Teleport";

$Menu::MainMenu = 3;
$MDMenuName[3] = "Main";

$Menu::InTurret = 4;
$MDMenuName[4] = "Turret Selection";

$MenuState::Default = 0;
$MenuState::Static = 1;

$MHNotStackableBreak = false;

// Gametype overloads
function DefaultGame::updateScoreHud(%game, %client, %tag)
{
     ScoreScreen.updateScoreHud(%game, %client, %tag);
}

function HuntersGame::updateScoreHud(%game, %client, %tag)
{
     ScoreScreen.updateScoreHud(%game, %client, %tag);
}

function TeamHuntersGame::updateScoreHud(%game, %client, %tag)
{
     ScoreScreen.updateScoreHud(%game, %client, %tag);
}

function RabbitGame::updateScoreHud(%game, %client, %tag)
{
     ScoreScreen.updateScoreHud(%game, %client, %tag);
}

function BountyGame::updateScoreHud(%game, %client, %tag)
{
     ScoreScreen.updateScoreHud(%game, %client, %tag);
}

function CnHGame::updateScoreHud(%game, %client, %tag)
{
     ScoreScreen.updateScoreHud(%game, %client, %tag);
}

function DnDGame::updateScoreHud(%game, %client, %tag)
{
     ScoreScreen.updateScoreHud(%game, %client, %tag);
}

function CTFGame::updateScoreHud(%game, %client, %tag)
{
     ScoreScreen.updateScoreHud(%game, %client, %tag);
}

function DMGame::updateScoreHud(%game, %client, %tag)
{
     ScoreScreen.updateScoreHud(%game, %client, %tag);
}

function SiegeGame::updateScoreHud(%game, %client, %tag)
{
     ScoreScreen.updateScoreHud(%game, %client, %tag);
}

function ConstructionGame::updateScoreHud(%game, %client, %tag)
{
     ScoreScreen.updateScoreHud(%game, %client, %tag);
}

function FuryTVGame::updateScoreHud(%game, %client, %tag)
{
     ScoreScreen.updateScoreHud(%game, %client, %tag);
}

// processGameLink
function DefaultGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     ScoreScreen.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

function HuntersGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     ScoreScreen.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

function TeamHuntersGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     ScoreScreen.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

function RabbitGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     ScoreScreen.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

function CTFGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     ScoreScreen.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

function DMGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     ScoreScreen.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

function CnHGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     ScoreScreen.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

function DnDGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     ScoreScreen.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

function SiegeGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     ScoreScreen.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

function ConstructionGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     ScoreScreen.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

function FuryTVGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     ScoreScreen.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

// Code
function ScoreScreen::updateScoreHud(%this, %game, %client, %tag)
{
     if(%client.scoreHudMenuState $= "")
          %client.scoreHudMenuState = $MenuState::Default;

     if(%client.scoreHudMenuState == $MenuState::Static)
          return;

     if(%client.scoreHudMenu $= "")
          if(%client.defaultMenu $= "")
               %client.scoreHudMenu = $Menu::Main;
          else
               %client.scoreHudMenu = %client.defaultMenu;

     %client.menuHudTag = %tag;

     messageClient(%client, 'SetScoreHudHeader', "", "");
     messageClient(%client, 'SetScoreHudHeader', "", '<just:center><a:gamelink\tGL\t3>Guide</a> | <a:gamelink\tGL\t1>Score</a>');
     renderMainMenu(%game, %client, %tag);
}

function ScoreScreen::processGameLink(%this, %game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     %tag = %client.menuHudTag;
     messageClient(%client, 'ClearHud', "", %tag, 0);
     %index = 0;

     if(%arg1 $= "GL")
     {
          %client.scoreHudMenu = %arg2;
          %game.processGameLink(%client, "Main");
          return;
     }

     switch(%client.scoreHudMenu)
     {
          case $Menu::MainMenu:
               renderMainMenu(%game, %client, %tag);
               
          case $Menu::Score:
               scoreProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5);

          case $Menu::VehicleTeleport:
               vehicleTPProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
               
          case $Menu::InTurret:
               inTurretProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5);

          default:
//               echo(%arg1 SPC %arg2 SPC %arg3);
//               %client.scoreHudMenu = %arg2;
//               %game.processGameLink(%client, %arg1);
               commandToClient(%client, 'TogglePlayHuds', true);
               closeModuleHud(%client);
     }
}

function renderMainMenu(%game, %client, %tag)
{
     switch(%client.scoreHudMenu)
     {
          case $Menu::Score:
//               %client.scoreHudMenuState = $MenuState::Default;
               scoreMainMenuRender(%game, %client, %tag);

          default:
               %index = 0;

               messageClient(%client, 'ClearHud', "", %tag, 0);
               messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>Fury: Terminal Velocity v%1 -- Quick Guide (press F2 to close)', System.Version);
               messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>Visit the Radiant Alpha forums at <a:wwwlink\tforums.radiantalpha.com>http://forums.radiantalpha.com</a>');
               %index++;
               messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>for more Fury: Terminal Velocity news, updates, and a full play guide.');
               %index++;
               messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center><color:FFFFFF>Quick Guide');
               %index++;
               messageClient(%client, 'SetLineHud', "", %tag, %index, 'In pilot seat: [Pack] activates vehicle module if you install an active module');
               %index++;
               messageClient(%client , 'SetLineHud' , "" , %tag , %index , '[Grenade] launches missiles, [Mine] near a friendly vehicle station restocks vehicle');
               %index++;
               messageClient(%client , 'SetLineHud' , "" , %tag , %index , "Shields have their own energy pool seperate from engines and weapons");
               %index++;
               messageClient(%client , 'SetLineHud' , "" , %tag , %index , "When shields are depleted, it will recharge ~15-30 sec depending on vehicle");
               %index++;
               messageClient(%client , 'SetLineHud' , "" , %tag , %index , "Explosive weapons (eg. missiles/mortars) do 1/5th their damage to shields");
               %index++;
               messageClient(%client , 'SetLineHud' , "" , %tag , %index , "Some weapons use ammo instead of energy");
               %index++;
               messageClient(%client , 'SetLineHud' , "" , %tag , %index , "All weapons on a vehicle and their turrets share energy and ammo");
               %index++;
               messageClient(%client , 'SetLineHud' , "" , %tag , %index , "Missiles will only be distracted by flares if thrown directly in their path");
               %index++;
               messageClient(%client , 'SetLineHud' , "" , %tag , %index , "Players give bonuses to vehicles when seated");
               %index++;
               messageClient(%client , 'SetLineHud' , "" , %tag , %index , "Light - Damage Reduction: Reduces vehicle's shield damage taken by 5%");
               %index++;
               messageClient(%client , 'SetLineHud' , "" , %tag , %index , "Medium - Damage Bonus: Increases missile and weapon damage by 5%");
               %index++;
               messageClient(%client , 'SetLineHud' , "" , %tag , %index , "Heavy - Energy Efficiency: Reduces energy weapon cost by 5%");
               %index++;
               messageClient(%client , 'SetLineHud' , "" , %tag , %index , "The repair pack's repair gun has now transformed into the Repair Rifle");
               %index++;
               messageClient(%client , 'SetLineHud' , "" , %tag , %index , "The repair rifle heals for 150 HP * number of players in the vehicle");
               %index++;

               messageClient(%client, 'ClearHud', "", %tag, %index);
     }
}

function closeModuleHud(%client)
{
     %tag = %client.menuHudTag;
     %client.scoreHudMenuState = $MenuState::Default;
     %client.scoreHudMenu = %client.defaultMenu;
     serverCmdHideHud(%client, 'scoreScreen');
     
//     if(!isObject(%client.player.getObjectMount()))
//         commandToClient(%client, 'setHudMode', 'Standard', "", 0);
         
     messageClient(%client, 'ClearHud', "", %tag, 0);
     messageClient(%client, 'CloseHud', "", %tag, 0);
}

//------------------------------------------------------------------------------
function scoreMainMenuRender(%game, %client, %tag)
{
   %client.scoreHudMenuState = $MenuState::Default;
   
   if (Game.numTeams > 1)
   {
      // Send header:
//      messageClient( %client, 'SetScoreHudHeader', "", '<tab:15,315>\t%1<rmargin:260><just:right>%2<rmargin:560><just:left>\t%3<just:right>%4',
//            %game.getTeamName(1), $TeamScore[1], %game.getTeamName(2), $TeamScore[2] );

      // Send subheader:
      messageClient( %client, 'SetScoreHudSubheader', "", '<tab:15,315>\t%3 (%1)<rmargin:260><just:right>SCORE: %4<rmargin:560><just:left>\t%5 (%2)<just:right>SCORE: %6',
            $TeamRank[1, count], $TeamRank[2, count], %game.getTeamName(1), $TeamScore[1], %game.getTeamName(2), $TeamScore[2] );

      %index = 0;
      while ( true )
      {
         if ( %index >= $TeamRank[1, count]+2 && %index >= $TeamRank[2, count]+2 )
            break;

         //get the team1 client info
         %team1Client = "";
         %team1ClientScore = "";
         %col1Style = "";
         if ( %index < $TeamRank[1, count] )
         {
            %team1Client = $TeamRank[1, %index];
            %team1ClientScore = %team1Client.score $= "" ? 0 : %team1Client.score;
            %col1Style = %team1Client == %client ? "<color:dcdcdc>" : "";
            %team1playersTotalScore += %team1Client.score;
         }
         else if( %index == $teamRank[1, count] && $teamRank[1, count] != 0 && %game.class $= "CTFGame") // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
         {
            %team1ClientScore = "--------------";
         }
         else if( %index == $teamRank[1, count]+1 && $teamRank[1, count] != 0 && %game.class $= "CTFGame") // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
         {
            %team1ClientScore = %team1playersTotalScore != 0 ? %team1playersTotalScore : 0;
         }
         //get the team2 client info
         %team2Client = "";
         %team2ClientScore = "";
         %col2Style = "";
         if ( %index < $TeamRank[2, count] )
         {
            %team2Client = $TeamRank[2, %index];
            %team2ClientScore = %team2Client.score $= "" ? 0 : %team2Client.score;
            %col2Style = %team2Client == %client ? "<color:dcdcdc>" : "";
            %team2playersTotalScore += %team2Client.score;
         }
         else if( %index == $teamRank[2, count] && $teamRank[2, count] != 0 && %game.class $= "CTFGame") // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
         {
            %team2ClientScore = "--------------";
         }
         else if( %index == $teamRank[2, count]+1 && $teamRank[2, count] != 0 && %game.class $= "CTFGame") // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
         {
            %team2ClientScore = %team2playersTotalScore != 0 ? %team2playersTotalScore : 0;
         }

         //if the client is not an observer, send the message
         if (%client.team != 0)
         {
            messageClient( %client, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush>%5<clip:200>%1</clip><rmargin:260><just:right>%2<spop><rmargin:560><just:left>\t%6<clip:200>%3</clip><just:right>%4',
                  %team1Client.name, %team1ClientScore, %team2Client.name, %team2ClientScore, %col1Style, %col2Style );
         }
         //else for observers, create an anchor around the player name so they can be observed
         else
         {
            messageClient( %client, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush>%5<clip:200><a:gamelink\t%7>%1</a></clip><rmargin:260><just:right>%2<spop><rmargin:560><just:left>\t%6<clip:200><a:gamelink\t%8>%3</a></clip><just:right>%4',
                  %team1Client.name, %team1ClientScore, %team2Client.name, %team2ClientScore, %col1Style, %col2Style, %team1Client, %team2Client );
         }

         %index++;
      }
   }
   else
   {
      //tricky stuff here...  use two columns if we have more than 15 clients...
      %numClients = $TeamRank[0, count];
      if ( %numClients > $ScoreHudMaxVisible )
         %numColumns = 2;

      // Clear header:
//      messageClient( %client, 'SetScoreHudHeader', "", "" );

      // Send header:
      if (%numColumns == 2)
         messageClient(%client, 'SetScoreHudSubheader', "", '<tab:15,315>\tPLAYER<rmargin:270><just:right>SCORE<rmargin:570><just:left>\tPLAYER<just:right>SCORE');
      else
         messageClient(%client, 'SetScoreHudSubheader', "", '<tab:15>\tPLAYER<rmargin:270><just:right>SCORE');

      %countMax = %numClients;
      if ( %countMax > ( 2 * $ScoreHudMaxVisible ) )
      {
         if ( %countMax & 1 )
            %countMax++;
         %countMax = %countMax / 2;
      }
      else if ( %countMax > $ScoreHudMaxVisible )
         %countMax = $ScoreHudMaxVisible;

      for ( %index = 0; %index < %countMax; %index++ )
      {
         //get the client info
         %col1Client = $TeamRank[0, %index];
         %col1ClientScore = %col1Client.score $= "" ? 0 : %col1Client.score;
         %col1Style = %col1Client == %client ? "<color:dcdcdc>" : "";

         //see if we have two columns
         if ( %numColumns == 2 )
         {
            %col2Client = "";
            %col2ClientScore = "";
            %col2Style = "";

            //get the column 2 client info
            %col2Index = %index + %countMax;
            if ( %col2Index < %numClients )
            {
               %col2Client = $TeamRank[0, %col2Index];
               %col2ClientScore = %col2Client.score $= "" ? 0 : %col2Client.score;
               %col2Style = %col2Client == %client ? "<color:dcdcdc>" : "";
            }
         }

         //if the client is not an observer, send the message
         if (%client.team != 0)
         {
            if ( %numColumns == 2 )
               messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25,325>\t<spush>%5<clip:195>%1</clip><rmargin:260><just:right>%2<spop><rmargin:560><just:left>\t%6<clip:195>%3</clip><just:right>%4',
                     %col1Client.name, %col1ClientScore, %col2Client.name, %col2ClientScore, %col1Style, %col2Style );
            else
               messageClient( %client, 'SetLineHud', "", %tag, %index, '<tab:25>\t%3<clip:195>%1</clip><rmargin:260><just:right>%2',
                     %col1Client.name, %col1ClientScore, %col1Style );
         }
         //else for observers, create an anchor around the player name so they can be observed
         else
         {
            if ( %numColumns == 2 )
               messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25,325>\t<spush>%5<clip:195><a:gamelink\t%7>%1</a></clip><rmargin:260><just:right>%2<spop><rmargin:560><just:left>\t%6<clip:195><a:gamelink\t%8>%3</a></clip><just:right>%4',
                     %col1Client.name, %col1ClientScore, %col2Client.name, %col2ClientScore, %col1Style, %col2Style, %col1Client, %col2Client );
            else
               messageClient( %client, 'SetLineHud', "", %tag, %index, '<tab:25>\t%3<clip:195><a:gamelink\t%4>%1</a></clip><rmargin:260><just:right>%2',
                     %col1Client.name, %col1ClientScore, %col1Style, %col1Client );
         }
      }

   }

   // Tack on the list of observers:
   %observerCount = 0;
   for (%i = 0; %i < ClientGroup.getCount(); %i++)
   {
      %cl = ClientGroup.getObject(%i);
      if (%cl.team == 0)
         %observerCount++;
   }

   if (%observerCount > 0)
   {
	   messageClient( %client, 'SetLineHud', "", %tag, %index, "");
      %index++;
		messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:10, 310><spush><font:Univers Condensed:22>\tOBSERVERS (%1)<rmargin:260><just:right>TIME<spop>', %observerCount);
      %index++;
      for (%i = 0; %i < ClientGroup.getCount(); %i++)
      {
         %cl = ClientGroup.getObject(%i);
         //if this is an observer
         if (%cl.team == 0)
         {
            %obsTime = getSimTime() - %cl.observerStartTime;
            %obsTimeStr = %game.formatTime(%obsTime, false);
		      messageClient( %client, 'SetLineHud', "", %tag, %index, '<tab:20, 310>\t<clip:150>%1</clip><rmargin:260><just:right>%2',
		                     %cl.name, %obsTimeStr );
            %index++;
         }
      }
   }

   //clear the rest of Hud so we don't get old lines hanging around...
   messageClient( %client, 'ClearHud', "", %tag, %index );
}

function scoreProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
   //the default behavior when clicking on a game link is to start observing that client
   %targetClient = %arg1;

   if(%arg1 $= "Close")
//   {
        closeModuleHud(%client);
//        return;		// -soph
//   }
   else if( %arg1 $= "Main" )	// +[soph]
        renderMainMenu(%game, %client, %tag);
   else				// +[/soph]
   if ((%client.team == 0) && isObject(%targetClient) && (%targetClient.team != 0))
   {
      %prevObsClient = %client.observeClient;

      // update the observer list for this client
      observerFollowUpdate( %client, %targetClient, %prevObsClient !$= "" );

      serverCmdObserveClient(%client, %targetClient);
      displayObserverHud(%client, %targetClient);

      if (%targetClient != %prevObsClient)
      {
         messageClient(%targetClient, 'Observer', '\c1%1 is now observing you.', %client.name);
         messageClient(%prevObsClient, 'ObserverEnd', '\c1%1 is no longer observing you.', %client.name);
      }
   }
}

//------------------------------------------------------------------------------
function inTurretMainMenuRender(%game, %client, %tag)
{
     %index = 0;
     messageClient(%client, 'ClearHud', "", %tag, 0);
     messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>Turret Weapons Selection');

     messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>Testing');
     %index++;

     messageClient( %client, 'ClearHud', "", %tag, %index);
}

// args:
// 1    menuname
// 2    vehicle
// 3    turretnode
// 4    weaponid
function inTurretProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
    switch$(%arg1)
    {
        case "HardpointSelect":
            %index = 0;
//            %client.scoreHudMenuName = "Select";
            %client.scoreHudMenuState = $MenuState::Static;
            messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>In Turret - Select a weapon');

            // Vehicle initial defs
            %data = %arg2.getDatablock().getName();
            %vid = $VehicleListID[%data];
            %weaponcount = 0;
            
            // Get all available entries on this turret
            for(%i = 0; %i < $VehicleListData[%vid, "hardpoints"]; %i++)
            {
                if($VehicleHardpoints[%vid, %i, "mountPoint"] == %arg3)
                {
                    %weapons[%weaponcount] = %i;
                    %weaponInstalledId[%weaponcount] = 3 + %i;
                    %weaponcount++;
                }
            }
            
            if(%weaponcount < 1)
            {
                messageClient(%client, 'SetLineHud', "", %tag, %index, 'This turret has no installed weapons!', %arg1, %arg2, %arg3, %arg4, %arg5);
                %index++;
                messageClient(%client, 'SetLineHud', "", %tag, %index, "");
                %index++;
                messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tExit>Exit</a>');
                %index++;
                messageClient(%client, 'ClearHud', "", %tag, %index);
                
                return;
            }
            
            for(%i = 0; %i < %weaponcount; %i++)
            {
                %wepid = %weapons[%i];
                %wepInstalled = %weaponInstalledId[%i];
                
                messageClient(%client, 'SetLineHud', "", %tag, %index, '%1:', $VehicleHardpoints[%vid, %wepid, "name"]);
                %index++;

                for(%w = 0; %w < VehiclePart.vehiclePartCount[$VehiclePartType::Weapon]; %w++)
                {
                    %potwep = VehiclePart.vehicleParts[$VehiclePartType::Weapon, %w].getName();

                    if(%potwep $= "" || %potwep $= "XEmptyHardpoint")
                        continue;

                    if(testSlotCanMount(%vid, %wepid, %potwep))
                    {
                        messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><a:gamelink\tInstallWeapon\t%3\t%4\t%5>%1: %2</a></clip>', %potwep.name, %potwep.description, %arg2, %wepid, %potwep);
                        %index++;
                    }
                }
                
                messageClient(%client, 'SetLineHud', "", %tag, %index, "");
                %index++;
            }

//            messageClient(%client, 'SetLineHud', "", %tag, %index, "");
//            %index++;
            messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tExit>Cancel</a>');
            %index++;
            messageClient(%client, 'ClearHud', "", %tag, %index);
            
            return;

        case "InstallWeapon":
            %data = %arg2.getDatablock().getName();
            %vid = $VehicleListID[%data];
            %hardpoint = $VehicleHardpoints[%vid, %arg3, "mountPoint"];
            %turret = %arg2.getMountNodeObject(%hardpoint);
            %origpartid = 3 + %arg3;
            %origpart = %arg2.installedVehiclePart[%origpartid];
            
            // Find installed weapon ID, replace references to origpartid
            %thisweapon = 0;

            for(%i = 0; %i < %turret.wepCount; %i++)
            {
//                echo(%turret.weapon[%i] SPC %origpart);
                
                if(%turret.weapon[%i] $= %origpart)
                {
                    %thisweapon = %i;
                    break;
                }
            }
            
            // Blank out existing images, copy to new turret once created
            %start = $VehicleHardpoints[%vid, %arg3, "imageStart"];
            %run = $VehicleHardpoints[%vid, %arg3, "imageCount"];
            
            for(%i = 0; %i < %run; %i++)
                %blanked[%i] = %start + %i;
            
            for(%i = 1; %i < 8; %i++)
            {
                %skip = false;
                
                for(%j = 0; %j < %run; %j++)
                {
                    if(%blanked[%j] == %i)
                    {
                        %skip = true;
                        break;
                    }
                }
                
                if(%skip)
                    continue;

                %images[%i] = %turret.getMountedImage(%i);
            }

            %client.player.setControlObject(0);
//            %arg2.unmountObject(%turret.onNode);
            %origpart.uninstallPart(%arg2, %arg3);
            %arg2.installedVehiclePart[%origpartid] = %arg4;
            %newTurret = %data.cloneTurret(%arg2, %turret);
            
            // Re-mount each image except the new ones to the turret
            for(%i = 1; %i < 8; %i++)
            {
                if(%images[%i] $= "" || %images[%i] == 0)
                    continue;

                %newTurret.mountImage(%images[%i], %i);
            }
            
            // Schedule player re-integration once turret is created
            %newTurret.weapon[%thisweapon] = %arg4;
            %newTurret.getDatablock().schedule(512, "finishTurretSwap", %newTurret);
            schedule(512, %arg2, "finishVTurretReplace", %arg2, %newTurret, %client.player);

            // Install parts for missing images
            %arg4.installPart(%data, %arg2, %client.player, %arg3);
            %arg4.reArm(%data, %arg2, %client.player, %arg3);

//            %arg2.mountObject(%turret, %turret.onNode);

            messageClient(%client, 'MsgVTurretLoadoutChange', '\c2%1 -> %2', $VehicleHardpoints[%vid, %arg3, "name"], %arg4.name);

            %client.scoreHudMenuState = $MenuState::Default;
            %game.processGameLink(%client, "Exit");
            return;

        default:
            closeModuleHud(%client);
            commandToClient(%client, 'TogglePlayHuds', true);
            return;
    }

    // For anything else - a just in case
    closeModuleHud(%client);
}

function finishVTurretReplace(%vehicle, %turret, %player)
{
    if(%player.getObjectMount() != %vehicle)
        return;

    %player.setControlObject(%turret);
    %player.client.setObjectActiveImage(%turret, 2);
    %player.vehicleTurret = %turret;
    
    serverCmdSetVehicleWeapon(%player.client, 0);
}

//------------------------------------------------------------------------------
function vehicleTPMainMenuRender(%game, %client, %tag)
{
     %index = 0;
     messageClient(%client, 'ClearHud', "", %tag, 0);
     messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>Teleport to Vehicle');

     messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>Testing');
     %index++;

     messageClient( %client, 'ClearHud', "", %tag, %index);
}

// $Menu::VehicleTeleport

// args:
// 1    menuname
// 2    team
function vehicleTPProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
    switch$(%arg1)
    {
        case "HardpointSelect":
            %index = 0;
//            %client.scoreHudMenuName = "Select";
            %client.scoreHudMenuState = $MenuState::Static;
            messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>Teleport to Vehicle');

            messageClient(%client, 'SetLineHud', "", %tag, %index, 'I\'m feeling lucky: Teleport me to a random vehicle', %arg1, %arg2, %arg3, %arg4, %arg5);
            %index++;
            messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><a:gamelink\tFeelingLucky\t1>* Empty Standing Seat</a></clip>');
            %index++;
            messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><a:gamelink\tFeelingLucky\t2>* Empty Turret Seat</a></clip>');
            %index++;

            messageClient(%client, 'SetLineHud', "", %tag, %index, '', %arg1, %arg2, %arg3, %arg4, %arg5);
            %index++;
            messageClient(%client, 'SetLineHud', "", %tag, %index, '', %arg1, %arg2, %arg3, %arg4, %arg5);
            %index++;
            messageClient(%client, 'SetLineHud', "", %tag, %index, 'Available Team Seats', %arg1, %arg2, %arg3, %arg4, %arg5);
            %index++;
            
            // Vehicle initial defs
            %data = %arg2.getDatablock().getName();
            %vid = $VehicleListID[%data];
            %weaponcount = 0;

            // Get all available entries on this turret
            for(%i = 0; %i < $VehicleListData[%vid, "hardpoints"]; %i++)
            {
                if($VehicleHardpoints[%vid, %i, "mountPoint"] == %arg3)
                {
                    %weapons[%weaponcount] = %i;
                    %weaponInstalledId[%weaponcount] = 3 + %i;
                    %weaponcount++;
                }
            }

            if(%weaponcount < 1)
            {
                messageClient(%client, 'SetLineHud', "", %tag, %index, 'This turret has no installed weapons!', %arg1, %arg2, %arg3, %arg4, %arg5);
                %index++;
                messageClient(%client, 'SetLineHud', "", %tag, %index, "");
                %index++;
                messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tExit>Exit</a>');
                %index++;
                messageClient(%client, 'ClearHud', "", %tag, %index);

                return;
            }

            for(%i = 0; %i < %weaponcount; %i++)
            {
                %wepid = %weapons[%i];
                %wepInstalled = %weaponInstalledId[%i];

                messageClient(%client, 'SetLineHud', "", %tag, %index, '%1:', $VehicleHardpoints[%vid, %wepid, "name"]);
                %index++;

                for(%w = 0; %w < VehiclePart.vehiclePartCount[$VehiclePartType::Weapon]; %w++)
                {
                    %potwep = VehiclePart.vehicleParts[$VehiclePartType::Weapon, %w].getName();

                    if(%potwep $= "" || %potwep $= "XEmptyHardpoint")
                        continue;

                    if(testSlotCanMount(%vid, %wepid, %potwep))
                    {
                        messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><a:gamelink\tInstallWeapon\t%3\t%4\t%5>%1: %2</a></clip>', %potwep.name, %potwep.description, %arg2, %wepid, %potwep);
                        %index++;
                    }
                }

                messageClient(%client, 'SetLineHud', "", %tag, %index, "");
                %index++;
            }

//            messageClient(%client, 'SetLineHud', "", %tag, %index, "");
//            %index++;
            messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tExit>Cancel</a>');
            %index++;
            messageClient(%client, 'ClearHud', "", %tag, %index);

            return;

        case "InstallWeapon":
            %data = %arg2.getDatablock().getName();
            %vid = $VehicleListID[%data];
            %hardpoint = $VehicleHardpoints[%vid, %arg3, "mountPoint"];
            %turret = %arg2.getMountNodeObject(%hardpoint);
            %origpartid = 3 + %arg3;
            %origpart = %arg2.installedVehiclePart[%origpartid];

            // Find installed weapon ID, replace references to origpartid
            %thisweapon = 0;

            for(%i = 0; %i < %turret.wepCount; %i++)
            {
//                echo(%turret.weapon[%i] SPC %origpart);

                if(%turret.weapon[%i] $= %origpart)
                {
                    %thisweapon = %i;
                    break;
                }
            }

            // Blank out existing images, copy to new turret once created
            %start = $VehicleHardpoints[%vid, %arg3, "imageStart"];
            %run = $VehicleHardpoints[%vid, %arg3, "imageCount"];

            for(%i = 0; %i < %run; %i++)
                %blanked[%i] = %start + %i;

            for(%i = 1; %i < 8; %i++)
            {
                %skip = false;

                for(%j = 0; %j < %run; %j++)
                {
                    if(%blanked[%j] == %i)
                    {
                        %skip = true;
                        break;
                    }
                }

                if(%skip)
                    continue;

                %images[%i] = %turret.getMountedImage(%i);
            }

            %client.player.setControlObject(0);
//            %arg2.unmountObject(%turret.onNode);
            %origpart.uninstallPart(%arg2, %arg3);
            %arg2.installedVehiclePart[%origpartid] = %arg4;
            %newTurret = %data.cloneTurret(%arg2, %turret);

            // Re-mount each image except the new ones to the turret
            for(%i = 1; %i < 8; %i++)
            {
                if(%images[%i] $= "" || %images[%i] == 0)
                    continue;

                %newTurret.mountImage(%images[%i], %i);
            }

            // Schedule player re-integration once turret is created
            %newTurret.weapon[%thisweapon] = %arg4;
            %newTurret.getDatablock().schedule(512, "finishTurretSwap", %newTurret);
            schedule(512, %arg2, "finishVTurretReplace", %arg2, %newTurret, %client.player);

            // Install parts for missing images
            %arg4.installPart(%data, %arg2, %client.player, %arg3);
            %arg4.reArm(%data, %arg2, %client.player, %arg3);

//            %arg2.mountObject(%turret, %turret.onNode);

            messageClient(%client, 'MsgVTurretLoadoutChange', '\c2%1 -> %2', $VehicleHardpoints[%vid, %arg3, "name"], %arg4.name);

            %client.scoreHudMenuState = $MenuState::Default;
            %game.processGameLink(%client, "Exit");
            return;

        default:
            closeModuleHud(%client);
            commandToClient(%client, 'TogglePlayHuds', true);
            return;
    }

    // For anything else - a just in case
    closeModuleHud(%client);
}
