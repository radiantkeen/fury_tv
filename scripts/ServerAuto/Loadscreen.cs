//--------------------------------------------------------------------------
// Loading GUI
//--------------------------------------

$FuryTVLoadScreenTip[0] = "In pilot seat: [Pack] activates vehicle module if you install an active module";
$FuryTVLoadScreenTip[1] = "[Grenade] launches missiles";
$FuryTVLoadScreenTip[2] = "[Mine] near a friendly vehicle station restocks vehicle";
$FuryTVLoadScreenTip[3] = "Shields have their own energy pool seperate from engines and weapons";
$FuryTVLoadScreenTip[4] = "When shields are depleted, it will recharge ~15-30 sec depending on vehicle";
$FuryTVLoadScreenTip[5] = "Explosive weapons (eg. missiles/mortars) do 1/5th their damage to shields";
$FuryTVLoadScreenTip[6] = "Some weapons use ammo instead of energy";
$FuryTVLoadScreenTip[7] = "All weapons on a vehicle and their turrets share energy and ammo";
$FuryTVLoadScreenTip[8] = "Missiles will only be distracted by flares if thrown directly in their path";
$FuryTVLoadScreenTip[9] = "The repair pack's repair gun has now transformed into the Repair Rifle";
$FuryTVLoadScreenTip[10] = "The repair rifle heals for 150 HP * number of players in the vehicle";
$FuryTVLoadScreenTip[11] = "[In vehicle & stackable] Light - Damage Reduction: Reduces vehicle's shield damage taken by 5%";
$FuryTVLoadScreenTip[12] = "[In vehicle & stackable] Medium - Damage Bonus: Increases missile and weapon damage by 5%";
$FuryTVLoadScreenTip[13] = "[In vehicle & stackable] Heavy - Energy Efficiency: Reduces energy weapon cost by 5%";

$FuryTVLoadScreenTipCount = 14;

//------------------------------------------------------------------------------
function sendLoadInfoToClient(%client)
{
     if(%client.loadScreened)
          return;

     %client.loadScreened = true;
     sendChatInfoToClient(%client);

//          schedule(1000, %client, messageClient, %client, 'MsgGameOver', "");
}

function sendChatInfoToClient(%client)
{
   messageClient(%client, 'MsgGameOver', "");
   messageClient(%client, 'MsgClearDebrief', "");

   %snd[0] = '~wfx/misc/nexus_cap.wav';
   %snd[1] = '~wfx/misc/switch_taken.wav';

   %sndCnt = 1;

   %launchSnd = %snd[getRandom(0, %sndCnt)];
   %tip = $FuryTVLoadScreenTip[mFloor(getRandom() * $FuryTVLoadScreenTipCount)];

   %singlePlayer = $CurrentMissionType $= "SinglePlayer";
   messageClient(%client, 'MsgLoadInfo', "", $CurrentMission, "", "");

   %nmis = "<font:verdana bold:12><color:33CCCC>* Mission: <color:FFFFFF>" SPC $MissionDisplayName SPC "("@$MissionTypeDisplayName@")";

   // Server name
   messageClient(%client, 'MsgDebriefResult', "", '<just:center>%1', $Host::GameName);

   messageClient(%client, 'MsgLoadQuoteLine', %launchSnd, "");
   messageClient(%client, 'MsgDebriefAddLine', "", "<spush><font:sui generis:22><color:EEEE33><just:center>Fury: <color:FFFFFF>Terminal Velocity<spop>");
   messageClient(%client, 'MsgDebriefAddLine', "", "<spush><font:times new roman:20><color:fe2322><just:center>Clientside vehicle air combat mod<spop>");
   messageClient(%client, 'MsgDebriefAddLine', "", "");
   messageClient(%client, 'MsgDebriefAddLine', "", "<spush><font:verdana bold:16><color:33CCCC>Version: <color:FFFFFF>v"@System.Version@" <color:33CCCC>Developer: <color:FFFFFF><a:PLAYER\tKeen>Keen</a><spop>");
   messageClient(%client, 'MsgDebriefAddLine', "", "<spush><font:verdana bold:16><color:33CCCC>With contributions from: <color:FFFFFF>Bahke, DarkDragonDX<spop>");
   messageClient(%client, 'MsgDebriefAddLine', "", "<font:verdana bold:16><color:33CCCC>Clientside mod, download required: <color:ffffff><a:wwwlink\tforums.radiantalpha.com/discussion/9/client-download-link\t1>http://forums.radiantalpha.com/discussion/9/client-download-link</a>.");
//   messageClient(%client, 'MsgDebriefAddLine', "", "<font:verdana bold:16><color:33CCCC>Press F2 to configure your account.");
   messageClient(%client, 'MsgDebriefAddLine', "", "");
   messageClient(%client, 'MsgDebriefAddLine', "", "<font:verdana bold:16><color:33CCCC>Tip: <color:FFFFFF>"@%tip );
   messageClient(%client, 'MsgDebriefAddLine', "", "");
   messageClient(%client, 'MsgDebriefAddLine', "", "");

   // Send server info:
   messageClient(%client, 'MsgDebriefAddLine', "", %nmis);
   messageClient(%client, 'MsgDebriefAddLine', "", $Host::Info);
   messageClient(%client, 'MsgDebriefAddLine', "", "<color:FFFFFF>" @ $Host::LoadScreenMessage);
   messageClient(%client, 'MsgDebriefAddLine', "", "<color:33CCCC>* Time limit: <color:FFFFFF>" @ $Host::TimeLimit, false );
   messageClient(%client, 'MsgDebriefAddLine', "", "<color:33CCCC>* Team damage: <color:FFFFFF>" @ ($TeamDamage ? "On" : "Off"));
   messageClient(%client, 'MsgDebriefAddLine', "", "<color:33CCCC>* Smurfs: <color:FFFFFF>" @ ($Host::NoSmurfs ? "No" : "Yes"));
//   messageClient(%client, 'MsgDebriefAddLine', "", "<color:FFFFFF>Load screen message here.", false ); // @ $Host::LoadScreenMessage
}
