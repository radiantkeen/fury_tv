//------------------------------------------------------------------------------
// Special - 0-1
$VehicleTargeterSlot = 0;
$VehicleWeightSlot = 1;

// Secondary - 2-3

// Primary - 4-7

// Vehicle Missile handling code
datablock AudioProfile(VehicleRestockSound)
{
     filename = "fx/vehicle/vehicle_reload.wav";
     description = AudioExplosion3d;
   preload = true;
};

datablock AudioProfile(LaunchVehicleMissileSound)
{
     filename = "fx/weapon/vehicle_rack_missile_launch.wav";
     description = AudioExplosion3d;
   preload = true;
};

datablock AudioProfile(VMissileLaunchSound)
{
     filename    = "fx/weapon/missile_fire.wav";
     description = AudioAmbient3d;
   preload = true;
};

datablock AudioProfile(VMissileSLaunchSound)
{
     filename    = "fx/weapon/missile_fire.wav";
     description = AudioAmbient3d;
   preload = true;
};

datablock AudioProfile(VMissileMLaunchSound)
{
     filename    = "fx/weapon/large_rocket_launch.wav";
     description = AudioAmbient3d;
   preload = true;
};

datablock AudioProfile(VMissileLLaunchSound)
{
     filename    = "fx/weapon/missile_medium_launch.wav";
     description = AudioAmbient3d;
   preload = true;
};

datablock AudioProfile(VMissileXLLaunchSound)
{
     filename    = "fx/weapon/fatman_fire.wav"; // "fx/Bonuses/high-level4-blazing.wav";
     description = AudioAmbient3d;
   preload = true;
};

function ShapeBase::addMissile(%vehicle, %slot, %image, %deploy, %proj, %refire, %size)
{
    if(%vehicle.missileCount $= "")
        %vehicle.missileCount = 0;

    if(%vehicle.missileReady[%slot] == true)
        return;
        
    %vehicle.missileReady[%slot] = true;
    %vehicle.missileDeploy[%slot] = %deploy;
    %vehicle.missileProj[%slot] = %proj;
    %vehicle.missileRefireTime[%slot] = %refire;
    %vehicle.armedMissiles[%vehicle.missileCount] = %slot;
    %vehicle.missileSize[%slot] = %size;
    %vehicle.missileCount++;
    
    if(%image !$= "")
        %vehicle.mountImage(%image, %slot);
        
    %vehicle.lastMissileFired = getSimTime();
}

function ShapeBase::launchMissile(%vehicle)
{
    %time = getSimTime();
    
    if(%vehicle.missileCount < 1)
    {
        %vehicle.playAudio(2, BomberBombDryFireSound);

        return;
    }
    else if(%time < %vehicle.missileTimeout)
    {
        %vehicle.playAudio(1, MissileReloadSound);

        return;
    }
    
    %slot = %vehicle.armedMissiles[%vehicle.missileCount - 1];
    %player = %vehicle.getMountNodeObject(0);
    %fwdVec = %vehicle.getForwardVector();
    %point = %vehicle.getMuzzlePoint(%slot);
    %p = createProjectile("BombProjectile", %vehicle.missileDeploy[%slot], %fwdVec, %point, %vehicle, %slot, %player);
    %vehicle.missileReady[%slot] = false;
    
    %target = %vehicle.getLockedTarget();
    %p.fwdDir = %fwdVec;

    if(%target)
        %p.target = %target;
    else if(%vehicle.isLocked())
        %p.lockedPos = %vehicle.getLockedPosition();

    %p.missileProj = %vehicle.missileProj[%slot];
    %p.missileSize = %vehicle.missileSize[%slot];

    %p.getDatablock().schedule(800, fireMissile, %p);
    %vehicle.unmountImage(%slot);
    %vehicle.missileCount--;
    %vehicle.missileTimeout = %time + %vehicle.missileRefireTime[%slot];
    
//    commandToClient(%player.client, 'setAmmoHudCount', "Missiles:" SPC %vehicle.missileCount);
    %vehicle.playAudio(0, LaunchVehicleMissileSound);
    
    if(%vehicle.missileCount < 1 && %vehicle.missileCharges > 0)
    {
        %vehicle.missileCharges--;
        messageClient(%player.client, 'MsgVehReloadMissiles', '\c2Missiles depleted, rearming from internal bay.');
        schedule(5000, %vehicle, "missileRackReload", %vehicle);
    }
}

function missileRackReload(%vehicle)
{
    %player = %vehicle.getMountNodeObject(0);
    %vehicle.playAudio(1, VehicleRestockSound);

    for(%i = 0; %i < 6; %i++)
    {
        %part = %vehicle.installedVehiclePart[(%i + 3)];

        if(isObject(%part) && $VehicleHardpoints[%vehicle.vid, %i, "type"] == $VHardpointType::Missile)
            %part.schedule(0, "reArm", %data, %vehicle, %player, %i);
    }
}

function ProjectileData::fireMissile(%data, %proj)
{
    if(%data.bombOnly)
        return;

    %sound = "";
    
    switch(%proj.missileSize)
    {
        case $VHardpointSize::Small:
            %sound = "VMissileSLaunchSound";

        case $VHardpointSize::Medium:
            %sound = "VMissileMLaunchSound";
            
        case $VHardpointSize::Large:
            %sound = "VMissileLLaunchSound";
            
        case $VHardpointSize::XLarge:
            %sound = "VMissileXLLaunchSound";
            
        default:
            %sound = "VMissileLaunchSound";
    }
    
    %proj.play3D(%sound);
    %proj.schedule(32, delete);

    %point = getFXPoint();
    %point.setPosition(%proj.position);

    if(isObject(%proj.target) || %proj.lockedPos !$= "")
    {
       %m = createProjectile("SeekerProjectile", %proj.missileProj, %proj.fwdDir, %proj.position, %point, 0, %proj.instigator);
       MissileSet.add(%m);
    }
    else
       %m = createProjectile("LinearProjectile", %proj.missileProj@"Dumbfire", %proj.fwdDir, %proj.position, %point, 0, %proj.instigator);

    %point.setTransform($g_FXOrigin);

    %m.vehicleObject    = %proj.vehicleObject;
    %m.damageBuffFactor = %proj.damageBuffFactor;

    if(isObject(%proj.target))
    {
        %m.target = %proj.target;
        %m.setObjectTarget(%proj.target);
    }
    else if(%proj.lockedPos !$= "")
        %m.setPositionTarget(%proj.lockedPos);
//    else
//        %m.setNoTarget();
}
//    %data.onBayLaunch(%obj, %slot, "BreacherMissileDeploy", "BreacherMissile", $VHardpointSize::Medium);
function ShapeBaseImageData::onBayLaunch(%data, %obj, %slot, %bomb, %missile, %size)
{
    %vtest = %obj.getObjectMount();
    %drawSource = isObject(%vtest) ? %vtest : %obj;

    if(%drawSource.ammoCache[%data.ammo] < 1)
    {
        %obj.playAudio(2, BomberBombDryFireSound);
        return;
    }

    %p = ShapeBaseImageData::onFire(%data, %obj, %slot);

    if(%p)
    {
        %fwdVec = %drawSource.getForwardVector();
        %dir = %obj.getMuzzleVector(%slot);
        %point = %obj.getMuzzlePoint(%slot);
        %target = %obj.getLockedTarget();

        if(%target)
        {
            %p.target = %target;
            %p.fwdDir = %fwdVec;
        }
        else if(%obj.isLocked())
        {
            %p.lockedPos = %drawSource.getLockedPosition();
            %p.fwdDir = %fwdVec;
        }
        else
            %p.fwdDir = %dir;

        %p.missileProj = %missile;
        %p.missileSize = %size;
        %p.vehicleObject = %drawSource;
        
        %p.getDatablock().schedule(1000, "fireMissile", %p);
//        %p.schedule(1000, "setPosition"); // the above function call schedules a delete 32ms after it executes, but does not execute on this... wat?
    }
    else
        %obj.playAudio(2, BomberBombDryFireSound);
}

datablock AudioProfile(DebrisFXExplosionSound)
{
   filename = "fx/explosion/vehicle_rnd_general_exp.wav";
   description = AudioExplosion3d;
   preload = true;
};

datablock AudioProfile(DebrisFXSparksSound)
{
   filename = "fx/explosion/vehicle_rnd_sparks_exp.wav";
   description = AudioExplosion3d;
   preload = true;
};

datablock AudioProfile(VehicleOnFireSound)
{
     filename = "fx/vehicle/vehicle_onfire.wav";
     description = AudioDefaultLooping3d;
     preload = true;
};

datablock DebrisData(HitDebrisFX)
{
   explodeOnMaxBounce = false;

   elasticity = 0.35;
   friction = 0.5;

   lifetime = 12.0;
   lifetimeVariance = 4.0;

   minSpinSpeed = 20;
   maxSpinSpeed = 800;

   numBounces = 5;
   bounceVariance = 3;

   staticOnMaxBounce = true;
   gravModifier = 1.0;

   useRadiusMass = true;
   baseRadius = 1;

   velocity = 14.0;
   velocityVariance = 7.0;
};

datablock ExplosionData(DebrisFXExplosion)
{
   soundProfile = DebrisFXExplosionSound;
   faceViewer = true;

   explosionShape = "effect_plasma_explosion.dts";
   sizes[0] = "0.2 0.2 0.2";
   sizes[1] = "0.3 0.3 0.3";
};

datablock ParticleData(DebrisEnergySparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 350;
   lifetimeVarianceMS   = 50;
   textureName          = "special/blueSpark";
   colors[0]     = "1.0 1.0 1.0 1.0";
   colors[1]     = "0.4 0.4 1.0 1.0";
   colors[2]     = "0.2 0.1 0.5 0.0";
   sizes[0]      = 2.0;
   sizes[1]      = 1.5;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.7;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(DebrisEnergySparkEmitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 0;
   ejectionVelocity = 32;
   velocityVariance = 8.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 180;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 250;
   particles = "DebrisEnergySparks";
};

datablock ExplosionData(DebrisSparkExplosion)
{
//   explosionShape = "energy_explosion.dts";
   soundProfile   = DebrisFXSparksSound;

   emitter[0] = DebrisEnergySparkEmitter;

   faceViewer           = false;
};

datablock StaticShapeData(DebrisExplosive) : StaticShapeDamageProfile
{
   shapeFile      = "turret_muzzlepoint.dts";
   explosion      = DebrisFXExplosion;

   maxDamage      = 1;
   destroyedLevel = 1;
   disabledLevel  = 1;

   isShielded = false;
   energyPerDamagePoint = 0;
   maxEnergy = 0;
   rechargeRate = 0.0;

   debrisShapeName = "debris_generic.dts";
   debris = HitDebrisFX;
};

datablock StaticShapeData(SparkDebrisExplosive) : StaticShapeDamageProfile
{
   shapeFile      = "turret_muzzlepoint.dts";
   explosion      = DebrisSparkExplosion;

   maxDamage      = 1;
   destroyedLevel = 1;
   disabledLevel  = 1;

   isShielded = false;
   energyPerDamagePoint = 0;
   maxEnergy = 0;
   rechargeRate = 0.0;

//   debrisShapeName = "debris_generic_small.dts";
//   debris = HitDebrisFX;
};

function createHitDebris(%pos)
{
   %debris = getRandom(1) ? "DebrisExplosive" : "SparkDebrisExplosive";

   %d = new StaticShape()
   {
      dataBlock = %debris;
   };

   MissionCleanup.add(%d);

   %d.setPosition(%pos);
   %d.schedule(32, "setDamageState", "Destroyed");
   %d.schedule(512, "delete");
}

function throwVehicleFlare(%obj, %vehicle, %flare)
{
     if(%obj.getInventory(%flare))
        %obj.decInventory(%flare, 1);
     else
        return false;

      %p = new FlareProjectile()
      {
         dataBlock        = FlareGrenadeProj;
         initialDirection = "0 0 -1";
         initialPosition  = %vehicle.position;
         sourceObject     = %obj;
         sourceSlot       = 0;
      };

      FlareSet.add(%p);
      MissionCleanup.add(%p);

      %obj.play3d(GrenadeThrowSound);
      %p.schedule(6000, "delete");

      // miscellaneous grenade-throwing cleanup stuff
      %obj.lastThrowTime[%data] = getSimTime();
      %obj.throwStrength = 0;

      return true;
}

datablock AudioProfile(EngineAlertSound)
{
   filename    = "gui/vote_nopass.WAV";
   description = AudioExplosion3d;
   preload = true;
};

datablock TurretImageData(SeekingParamImage) // ShapeBaseImageData
{
   mountPoint = 4;
   offset = "0 0.1 -0.3";
   rotation = "0 1 0 180";
   shapeFile = "turret_muzzlepoint.dts";

   isSeeker     = true;
   seekRadius   = 300;
   maxSeekAngle = 30;
   seekTime     = 0.85;
   minSeekHeat  = 0.8;
   useTargetAudio = false;
   minTargetingDistance = 15;

   // Turret parameters
   activationMS      = 1000;
   deactivateDelayMS = 1500;
   thinkTimeMS       = 200;
   degPerSecTheta    = 720;
   degPerSecPhi      = 720;
};

datablock TurretImageData(GenericParamImage)
{
   mountPoint = 2;
   shapeFile = "turret_muzzlepoint.dts";

   projectile = AssaultChaingunBullet;
   projectileType = TracerProjectile;

   useCapacitor = true;
   usesEnergy = true;

   // Turret parameters
   activationMS      = 750;
   deactivateDelayMS = 1500;
   thinkTimeMS       = 100;
   degPerSecTheta    = 600;
   degPerSecPhi      = 600;

   attackRadius      = 200;
};

datablock ShapeBaseImageData(VehicleMissileLocker)
{
   shapeFile = "turret_muzzlepoint.dts";
   mountPoint = 0;
   offset = "0 0 0";
   rotation = "0 1 0 0";

   isSeeker     = true;
   seekRadius   = 400;
   maxSeekAngle = 40;
   seekTime     = 0.85;
   minSeekHeat  = 0.8;
   useTargetAudio = false;
   minTargetingDistance = 10;
};

datablock ShapeBaseImageData(VehicleMissileInactive) : VehicleMissileLocker
{
   isSeeker = false;
};

function testVehicleForMount(%player, %obj)
{
   if(isObject(%obj) && %obj.getDamageState() !$= "Destroyed" && !%player.teleporting && !%player.client.inInv)
      %player.getDataBlock().onCollision(%player, %obj, 0);
}

// This handles cycling through available vehicle weapons - was 1-indexed, bumped to 0-indexed
function serverCmdSwitchVehicleWeapon(%client, %dir)
{
   %obj = %client.player.getControlObject();
   %weaponNum = %obj.selectedWeapon;

   if(%dir $= "next")
   {
      if(%weaponNum++ > (%obj.wepCount - 1))
         %weaponNum = 0;
   }
   else
   {
      if(%weaponNum-- < 0)
         %weaponNum = (%obj.wepCount - 1);
   }

   serverCmdSetVehicleWeapon(%client, %weaponNum);
}

function serverCmdSetVehicleWeapon(%client, %num)
{
    %obj = %client.player.getControlObject();
    
    if(!isObject(%obj))
        return;
    
    %data = %obj.getDataBlock();
    %bIsVehicle = %obj.isVehicle();

//    echo("setVehicleWeapon" SPC %client SPC %obj SPC %bIsVehicle SPC %num);
    
    for(%i = 0; %i < 8; %i++)
        %obj.setImageTrigger(%i, false);

    if(%num > (%obj.wepCount - 1) || %num < 0)
        %num = 0;

    %obj.selectedWeapon = %num;
    %obj.selectedWeaponObject = "";
    %wep = %obj.weapon[%num];

    if(isObject(%wep))
    {
        %vid = %bIsVehicle ? %obj.vid : %obj.getObjectMount().vid;
        %image = $VehicleHardpoints[%vid, %obj.hardpointRef[%obj.selectedWeapon], "imageStart"];
        %obj.selectedWeaponObject = %wep;

        %wep.onSwitchWeapon(%obj, %client, %num);

        if(%obj.weapon[%num].reticleTex !$= "")
        {
            commandToClient(%client, 'ChangeReticle', %obj.weapon[%num].reticleTex, %obj.weapon[%num].showReticleFrame);
            commandToClient(%client, 'SetVehHighlight', %image);
        }
        else
        {
            // set the active image on the client's obj - only used when interfacing with the old way of doing things
            %client.setWeaponsHudActive("Blaster");
            %client.setVWeaponsHudActive(%image);
            %client.setObjectActiveImage(%obj, %image);
        }
        
        if(%wep.usesAmmo)
        {
            if(%bIsVehicle)
                commandToClient(%client, 'setAmmoHudCount', %obj.ammoCache[%wep.ammo]);
            else
                commandToClient(%client, 'setAmmoHudCount', %obj.getObjectMount().ammoCache[%wep.ammo]);
        }
        else
            commandToClient(%client, 'setAmmoHudCount', "");
    }
    else
    {
        commandToClient(%client, 'setAmmoHudCount', "");
        commandToClient(%client, 'ChangeReticle', "", false);
        commandToClient(%client, 'SetVehHighlight', 0);
    }
}

//--------------------------------------------------------------
// NUMBER OF PURCHASEABLE VEHICLES PER TEAM
//--------------------------------------------------------------

$VehicleRespawnTime        = 15000;

$VehicleMax[$VehicleGroup::Other]               = 0;
$VehicleMax[$VehicleGroup::Interceptors]        = 3;
$VehicleMax[$VehicleGroup::Fighters]            = 4;
$VehicleMax[$VehicleGroup::Gunships]            = 5;
$VehicleMax[$VehicleGroup::Battleships]         = 4;
$VehicleMax[$VehicleGroup::Carriers]            = 4;
$VehicleMax[$VehicleGroup::Dreadnoughts]        = 1;

function clearVehicleCount(%team)
{
    $VehicleTotalCount[%team, $VehicleGroup::Other]               = 0;
    $VehicleTotalCount[%team, $VehicleGroup::Interceptors]        = 0;
    $VehicleTotalCount[%team, $VehicleGroup::Fighters]            = 0;
    $VehicleTotalCount[%team, $VehicleGroup::Gunships]            = 0;
    $VehicleTotalCount[%team, $VehicleGroup::Battleships]         = 0;
    $VehicleTotalCount[%team, $VehicleGroup::Carriers]            = 0;
    $VehicleTotalCount[%team, $VehicleGroup::Dreadnoughts]        = 0;
}

// todo: switch to bitmask method
function findEmptySeat(%vehicle, %player, %forceNode)
{
   %minNode = 1;
   %node = -1;
   %dataBlock = %vehicle.getDataBlock();
   %dis = %dataBlock.minMountDist;
   %playerPos = getWords(%player.getTransform(), 0, 2);
   %message = "";
   
   if(%player.getDatablock().canPilot == true)
       %minNode = 0;
   else
      %minNode = findFirstHeavyNode(%dataBlock);
      
   if(%forceNode !$= "")
      %node = %forceNode;
   else
   {
      for(%i = 0; %i < %dataBlock.numMountPoints; %i++)
         if(!%vehicle.getMountNodeObject(%i))
         {
            if(%dataBlock.noSitPoint[%i] == true)
                continue;
                
            %seatPos = getWords(%vehicle.getSlotTransform(%i), 0, 2);
            %disTemp = VectorLen(VectorSub(%seatPos, %playerPos));
            if(%disTemp <= %dis)
            {
               %node = %i;
               %dis = %disTemp;
            }
         }
    }
   if(%node != -1 && %node < %minNode)
   {
      if(%message $= "")
      {
         if(%node == 0)
            %message = '\c2Only Scout (Pilot) Armors can pilot this vehicle.~wfx/misc/misc.error.wav';
         else
            %message = '\c2Only Scout (Pilot) Armors can use that position.~wfx/misc/misc.error.wav';
      }

      if(!%player.noSitMessage)
      {
         %player.noSitMessage = true;
         %player.schedule(2000, "resetSitMessage");
         messageClient(%player.client, 'MsgArmorCantMountVehicle', %message);
      }
      %node = -1;
   }
   return %node;
}

function findAIEmptySeat(%vehicle, %player)
{
    %dataBlock = %vehicle.getDataBlock();
    %num = 1;
	%node = -1;
 
	for(%i = %num; %i < %dataBlock.numMountPoints; %i++)
	{
	   if (!%vehicle.getMountNodeObject(%i))
	   {
            if(%dataBlock.noSitPoint[%i] == true)
                continue;
	      //cheap hack - for now, AI's will mount the next available node regardless of where they collided
	      %node = %i;
	      break;
	   }
	}

	//return the empty seat
	return %node;
}

function findFirstHeavyNode(%data)
{
   for(%i = 0; %i < %data.numMountPoints; %i++)
   
      if(%data.mountPose[%i] $= "")
         return %i;
         
   return %data.numMountPoints;
}

/// Vehicle HUD

function VehicleHud::updateHud(%obj, %client, %tag)
{
   %station = %client.player.station;
   %obj.vstag = %tag;
   %menu = %client.currentVehicleMenu;
   %team = %client.getSensorGroup();
   %count = 0;

    if(%menu == 1 && !$Host::HavocMode)
    {
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Skycutter Turbobike", "", ScoutVehicle, vehicleLeft("ScoutVehicle", %team) );
        %count++;
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Airhawk Jetbike", "", Airhawk, vehicleLeft("Airhawk", %team) );
        %count++;
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Wildcat Hoverbike", "", Wildcat, vehicleLeft("Wildcat", %team) );
        %count++;
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Ameri Enforcer", "", Ameri, vehicleLeft("Ameri", %team) );
        %count++;
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Back", "", "B", 1 );
        %count++;
    }
    else if(%menu == 2)
    {
        if(!$Host::HavocMode)
        {
            messageClient( %client, 'SetLineHud', "", %tag, %count, "Shrike Multirole Fighter", "", ScoutFlyer, vehicleLeft("ScoutFlyer", %team) );
            %count++;
            messageClient( %client, 'SetLineHud', "", %tag, %count, "Shrike X Prototype Fighter", "", ShrikeX, vehicleLeft("ShrikeX", %team) );
            %count++;
        }
        
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Super Shrike Strike Fighter", "", SuperShrike, vehicleLeft("SuperShrike", %team) );
        %count++;
        
        if(!$Host::HavocMode)
        {
            messageClient( %client, 'SetLineHud', "", %tag, %count, "Starhawk Assault Fighter", "", Starhawk, vehicleLeft("Starhawk", %team) );
            %count++;
            messageClient( %client, 'SetLineHud', "", %tag, %count, "Firebird Heavy Fighter", "", Firebird, vehicleLeft("Firebird", %team) );
            %count++;
        }
        
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Hummingbird Hover Jeep", "", HoverJeep, vehicleLeft("HoverJeep", %team) );
        %count++;
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Back", "", "B", 1 );
        %count++;
    }
    else if(%menu == 3)
    {
        messageClient( %client, 'SetLineHud', "", %tag, %count, "B-86 Catapult Bomber", "", Catapult, vehicleLeft("Catapult", %team) );
        %count++;
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Tomahawk Attack Chopper", "", Tomahawk, vehicleLeft("Tomahawk", %team) );
        %count++;
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Guillotine Hover Tank", "", AssaultVehicle, vehicleLeft("AssaultVehicle", %team) );
        %count++;
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Bismark Scout Carrier", "", Bismark, vehicleLeft("Bismark", %team) );
        %count++;
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Retaliator Gunship", "", BomberFlyer, vehicleLeft("BomberFlyer", %team) );
        %count++;
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Scorpion Destroyer", "", Scorpion, vehicleLeft("Scorpion", %team) );
        %count++;
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Back", "", "B", 1 );
        %count++;
    }
    else if(%menu == 4)
    {
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Stryker Cruiser", "", Stryker, vehicleLeft("Stryker", %team) );
        %count++;
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Wolfhound Battlecruiser", "", Wolfhound, vehicleLeft("Wolfhound", %team) );
        %count++;
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Rhino Heavy APC", "", HAPCFlyer, vehicleLeft("HAPCFlyer", %team) );
        %count++;
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Annihilator Battleship", "", Annihilator, vehicleLeft("Annihilator", %team) );
        %count++;
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Sky Mistress Troop Carrier", "", SkyMistress, vehicleLeft("SkyMistress", %team) );
        %count++;
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Leviathan Dreadnought", "", Leviathan, vehicleLeft("Leviathan", %team) );
        %count++;
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Back", "", "B", 1 );
        %count++;
    }
    else if(%menu == 5 && $Host::EnableSpecialVehicles)
    {
//        messageClient( %client, 'SetLineHud', "", %tag, %count, "Arwing Interdictor", "", Arwing, vehicleLeft("Arwing", %team) );
//        %count++;
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Viper Interceptor", "", Viper, vehicleLeft("Viper", %team) );
        %count++;
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Pyro-GX Heavy Interceptor", "", PyroGX, vehicleLeft("PyroGX", %team) );
        %count++;
//        messageClient( %client, 'SetLineHud', "", %tag, %count, "Goodship Lollipop TEMP", "", Goodship, vehicleLeft("Goodship", %team) );
//        %count++;
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Back", "", "B", 1 );
        %count++;
    }
    else
    {
        if(!$Host::HavocMode)
        {
            messageClient( %client, 'SetLineHud', "", %tag, %count, "Interceptors", "", "M1", 1337 );
            %count++;
        }
        
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Fighters", "", "M2", 1337 );
        %count++;
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Gunships", "", "M3", 1337 );
        %count++;
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Battleships", "", "M4", 1337 );
        %count++;

        if($Host::EnableSpecialVehicles && !$Host::HavocMode)
        {
            messageClient( %client, 'SetLineHud', "", %tag, %count, "Special", "", "M5", 1337 );
            %count++;
        }
    }
   
   %station.lastCount = %count;
}

function vehicleLeft(%blockName, %team)
{
    %vid = $VehicleListID[%blockName];
    %grp = $VehicleListData[%vid, "group"];

    return $VehicleMax[%grp] - $VehicleTotalCount[%team, %grp];
}

function vehicleCheck(%blockName, %team)
{
   if(vehicleLeft(%blockName, %team) > 0)
      return true;

   return false;
}

function actuallyBuyingVehicle(%client, %blockName)
{
    %wellAreYou = false;
    %menu = 0;

    switch$(%blockName)
    {
        case "B":
            %menu = 0;

        case "M1":
            %menu = 1;

        case "M2":
            %menu = 2;

        case "M3":
            %menu = 3;
            
        case "M4":
            %menu = 4;
            
        case "M5":
            %menu = 5;
            
        case "M6":
            %menu = 6;
            
        case "M7":
            %menu = 7;

        case "M8":
            %menu = 8;
            
        case "M9":
            %menu = 9;
            
        default:
            %wellAreYou = true;
    }

    commandToClient(%client, 'StationVehicleHideHud');
    %client.currentVehicleMenu = %menu;
    
    if(%wellAreYou)
        return true;
    
    commandToClient(%client, 'StationVehicleShowHud');
    
    return false;
}

function serverCmdBuyVehicle(%client, %blockName)
{
    if(!actuallyBuyingVehicle(%client, %blockName))
        return;
   
   %team = %client.getSensorGroup();
   if(vehicleCheck(%blockName, %team))
   {
      %st = %client.player.station;
      %station = %st.pad;
      
      if(%station.ready)
      {
         %trans = %station.getTransform();
         %pos = getWords(%trans, 0, 2);
         %matrix = VectorOrthoBasis(getWords(%trans, 3, 6));
         %yrot = getWords(%matrix, 3, 5);
         %p = vectorAdd(%pos,vectorScale(%yrot, -3));
         %p =  getWords(%p,0, 1) @ " " @ getWord(%p,2) + 4;

//         error(%blockName);
//         error(%blockName.spawnOffset);

         %p = vectorAdd(%p, %blockName.spawnOffset);
         %rot = getWords(%trans, 3, 5);
         %angle = getWord(%trans, 6) + 3.14;
         %mask = $TypeMasks::VehicleObjectType | $TypeMasks::PlayerObjectType |
                 $TypeMasks::StationObjectType | $TypeMasks::TurretObjectType;
	      InitContainerRadiusSearch(%p, %blockName.checkRadius, %mask);

	      %clear = 1;
         for (%x = 0; (%obj = containerSearchNext()) != 0; %x++)
         {
            if((%obj.getType() & $TypeMasks::VehicleObjectType) && (%obj.getDataBlock().checkIfPlayersMounted(%obj)))
            {
               %clear = 0;
               break;
            }
            else if(%obj.isWalker && isObject(%obj.pilotObject))
            {
                MessageClient(%client, "", 'Can\'t create vehicle. A player is on the creation pad.');
                return;
            }
            else
               %removeObjects[%x] = %obj;
         }
         if(%clear)
         {
            %fadeTime = 0;
            for(%i = 0; %i < %x; %i++)
            {
               if(%removeObjects[%i].isPlayer() && !%removeObjects[%i].isWalker)
               {
                  %pData = %removeObjects[%i].getDataBlock();
                  %pData.damageObject(%removeObjects[%i], 0, "0 0 0", 1000, $DamageType::VehicleSpawn);
               }
               else
               {
                  %removeObjects[%i].mountable = 0;
                  %removeObjects[%i].startFade( 1000, 0, true );
                  %removeObjects[%i].schedule(1001, "delete");
                  %fadeTime = 1500;
               }
            }
            schedule(%fadeTime, 0, "createVehicle", %client, %station, %blockName, %team , %p, %rot, %angle);
         }
         else
            MessageClient(%client, "", 'Can\'t create vehicle. A player is on the creation pad.');
      }
      //--------------------------------------------------------------------------------------
      // z0dd - ZOD, 4/25/02. client tried to quick purchase a vehicle that isn't on this map
      else
      {
      	 messageClient(%client, "", "~wfx/misc/misc.error.wav");
      }
      //--------------------------------------------------------------------------------------
   }
   //--------------------------------------------------------------------------------------------------------------
   // z0dd - ZOD, 4/25/02. client tried to quick purchase vehicle when max vehicles of this type are already in use
   else
   {
      messageClient(%client, "", "~wfx/misc/misc.error.wav");
   }
   //--------------------------------------------------------------------------------------------------------------
}

function createVehicle(%client, %station, %blockName, %team, %pos, %rot, %angle)
{
   %create = %blockName;
   
   if((%obj = %create.create(%team)))
   {
      //-----------------------------------------------
      // z0dd - ZOD, 4/25/02. MPB Teleporter.
      // keen: repurpose this for portal exit point
//      if ( %blockName $= "MobileBaseVehicle" )
//      {
//         %station.station.teleporter.MPB = %obj;
//         %obj.teleporter = %station.station.teleporter;
//      }
      //-----------------------------------------------
      %station.ready = false;
      %obj.team = %team;
      %obj.useCreateHeight(true);
      %obj.schedule(5500, "useCreateHeight", false);
      %obj.getDataBlock().isMountable(%obj, false);
      %obj.getDataBlock().schedule(6500, "isMountable", %obj, true);

      %station.playThread($ActivateThread,"activate2");
      %station.playAudio($ActivateSound, ActivateVehiclePadSound);

      vehicleListAdd(%blockName, %obj);
      MissionCleanup.add(%obj);

      %turret = %obj.getMountNodeObject(10);
      if(%turret > 0)
      {
         %turret.setCloaked(true);
         %turret.schedule(4800, "setCloaked", false);
      }

      %obj.setCloaked(true);
      %obj.setTransform(vectorAdd(%pos, "0 0 5") SPC %rot SPC %angle);

      %obj.schedule(3700, "playAudio", 0, VehicleAppearSound);
      %obj.schedule(4800, "setCloaked", false);

      if(%client.player.lastVehicle)
      {
         %client.player.lastVehicle.lastPilot = "";
         vehicleAbandonTimeOut(%client.player.lastVehicle);
         %client.player.lastVehicle = "";
      }
      %client.player.lastVehicle = %obj;
      %obj.lastPilot = %client.player;

      // play the FX
      %fx = new StationFXVehicle()
      {
         dataBlock = VehicleInvFX;
         stationObject = %station;
      };

      if ( %client.isVehicleTeleportEnabled() )
         %obj.getDataBlock().schedule(5000, "mountDriver", %obj, %client.player);
         
       %blockName.installParts(%obj, %client.player);
       
       if(%obj.getTarget() != -1)
          setTargetSensorGroup(%obj.getTarget(), %client.getSensorGroup());
   }
}

function vehicleAbandonTimeOut(%vehicle)
{
   if(%vehicle.getDatablock().cantAbandon $= "" && %vehicle.lastPilot $= "")
   {
//      if(%vehicle.isWalker)
//      {
        // don't fade if someone else captured it
//        if(isObject(%vehicle.pilotObject))
//            return;
            
//        $VehicleTotalCount[%vehicle.originalteam, %vehicle.shortName]--;
//        %vehicle.mountable = 0;
//        %vehicle.startFade(1000, 0, true);
//        %vehicle.schedule(1001, "delete");
//        return;
//      }

      for(%i = 0; %i < %vehicle.getDatablock().numMountPoints; %i++)
         if((%passenger = %vehicle.getMountNodeObject(%i)) && %passenger.isPlayer())
         {
//            %passenger = %vehicle.getMountNodeObject(%i);
            if(%passenger.lastVehicle !$= "")
               schedule(15000, %passenger.lastVehicle,"vehicleAbandonTimeOut", %passenger.lastVehicle);
            %passenger.lastVehicle = %vehicle;
            %vehicle.lastPilot = %passenger;
            return;
         }

      if(%vehicle.respawnTime !$= "")
         %vehicle.marker.schedule = %vehicle.marker.data.schedule(%vehicle.respawnTime, "respawn", %vehicle.marker);
      %vehicle.mountable = 0;
      %vehicle.startFade(1000, 0, true);
      %vehicle.schedule(1001, "delete");
      
      for(%i = 0; %i < 16; %i++)
      {
         if(isObject((%pt = %vehicle.getMountNodeObject(%i))))
         {
            %pt.startFade(1000, 0, true);
            %pt.schedule(1001, "delete");
         }
      }
   }
}

// Used on maps that alter the count of vehicles - default vehicles only
function refreshVehicleCounts()
{
    // not needed anymore
}

// Vehicle function overrides
// -----------------------------------------------------------------

// New Vehicle Object breakdown for data access - important things added in Fury: TV
//
// Class Vehicle
// vid                  Int                         'Vehicle ID, used to access the string table data on the vehicle - ex: $VehicleListData[%obj.vid, "block"] or $VehicleListData[$VehicleID::Retaliator, "block"]'
// partsInstalled       Bool                        'Whether VehicleData::InstallParts() has been run or not
// nameOverride         String                      'Name of the vehicle to show on the vehicle and in death messages'
// installedParts       Int                         'Count of installed parts - should be between 3 and 9'
// installedVehiclePart ScriptObject<VehiclePart>[] 'Array of all vehicle parts installed on the vehicle'
// wepCount             Int                         'Count of weapons installed on the vehicle'
// weapon               ScriptObject<VehiclePart>[] 'Array of weapon objects installed to the vehicle'
// hardpointRef         Int[]                       'Array of what weapon number this weapon object corresponds to'
// installedMissiles    Int                         'Count of how many missile objects are installed to the vehicle'
// missile              ScriptObject<VehiclePart>[] 'Array of missile objects installed to the vehicle'
// turretCount          Int                         'Count of turrets installed on the vehicle'
// turrets              Turret[]                    'Array of turret objects installed to the vehicle'
// selectedWeapon       Int                         'Currently selected weapon index'
// missileCount         Int                         'Count of missiles installed on the vehicle - used for visual display'
// stallState           Bool                        'Whether the selected physics engine on the vehicle has detected the vehicle is stalling (ex. falling to the ground)'

function VehicleData::onAdd(%data, %obj)
{
   %obj.resupplySystem = false;
   %obj.gyrostabilizer = false;
   %obj.missileCharges = 0;
   %obj.emergencyBatteryCharges = 0;
   %obj.reassemblerTicking = false;
   %obj.livingBomb = false;
   %obj.tagYoureIt = false;
   
   Parent::onAdd(%data, %obj);

//   echo("vehicle spawned:" SPC %obj SPC "target:" SPC %obj.getTarget());
   
   if(%obj.getTarget() != -1)
   {
       if(%data.sensorData !$= "")
          setTargetSensorData(%obj.getTarget(), %data.sensorData);

//       setTargetNeverVisMask(%obj.getTarget(), 0);
//       setTargetAlwaysVisMask(%obj.getTarget(), 0xffffffff);
//       setTargetRenderMask(%obj.getTarget(), 0xffffffff);
//       setTargetFriendlyMask(%obj.getTarget(), 0xffffffff);
   }

   if(%obj.disableMove)
      %obj.immobilized = true;

   if(%obj.deployed)
   {
      if($countDownStarted)
      {
         // spawning on mission load
         %data.installParts(%obj, 0);
         %data.schedule(($Host::WarmupTime * 1000) / 2, "vehicleDeploy", %obj, 0, 1);
      }
      else
      {
         $VehiclesDeploy[$NumVehiclesDeploy] = %obj;
         $NumVehiclesDeploy++;
      }
   }

   if(%obj.mountable || %obj.mountable $= "")
      %data.isMountable(%obj, true);
   else
      %data.isMountable(%obj, false);

    %obj.missileCount = 0;

    %data.schedule(32, "startReactor", %obj);
}

function VehicleData::startReactor(%data, %obj)
{
    %obj.setSelfPowered();
    %obj.setRechargeRate(%obj.rechargeTick);
    %obj.setEnergyLevel(%data.maxEnergy);

    %data.onTick(%obj);
}

function VehicleData::onRemove(%this, %obj)
{
   // if there are passengers/driver, kick them out
   for(%i = 0; %i < %obj.getDatablock().numMountPoints; %i++)
   {
      %pl = %obj.getMountNodeObject(%i);

      if(isObject(%pl) && %pl.isPlayer())
         %pl.unmount();
   }

   if(isObject(%obj.frame))
      %obj.frame.delete();

   if(isObject(%obj.sensor))
      %obj.sensor.delete();

   if(isObject(%obj.shieldSource))
      %obj.shieldSource.delete();

    for(%i = 0; %i < 16; %i++)
    {
        %something = %obj.getMountNodeObject(%i);

        if(isObject(%something))
            %something.schedule(1000, "delete");
    }

   %this.deleteAllMounted(%obj);

   vehicleListRemove(%obj.getDataBlock(), %obj);

   if(%obj.lastPilot.lastVehicle == %obj)
      %obj.lastPilot.lastVehicle = "";

   Parent::onRemove(%this, %obj);
}

function vehicleListAdd(%blockName, %obj)
{
    %vid = $VehicleListID[%blockName];
    $VehicleTotalCount[%obj.team, $VehicleListData[%vid, "group"]]++;
    %name = "Team"@%obj.team@"Vehicles";
    %grp = nameToID(%name);

    if(%grp == -1)
    {
        %grp = new SimGroup(%name);
        MissionCleanup.add(%grp);
    }
    
//    %grp.add(%obj);
//    error("group count" SPC %grp SPC %grp.getCount());

//    if(%grp.getCount() < 1)
//        error("vehicle add failure" SPC %grp SPC %name SPC %obj);

    // This is the only solution that worked... there is no reason for the above not to work
    // it just simply refused to work. This is typical of T2 choosing to not execute code.
    // This game must be some sort of rogue AI.
    %grp.schedule(0, "add", %obj);
}

function vehicleListRemove(%data, %obj)
{
    %vid = $VehicleListID[%data.getName()];

    $VehicleTotalCount[%obj.team, $VehicleListData[%vid, "group"]]--;
}

function VehicleData::mountDriver(%data, %obj, %player)
{
   if(isObject(%obj) && %obj.getDamageState() !$= "Destroyed")
   {
      %player.setCloaked(true);
      schedule(1000, 0, "testVehicleForMount", %player, %obj);
      %player.schedule(1500,"setCloaked", false);
   }
}

function VehicleData::updateAmmoCount(%data, %obj, %client, %ammo)
{
     commandToClient(%client, 'setAmmoHudCount', %obj.ammoCache[%ammo]);
}

function TurretData::updateAmmoCount(%data, %obj, %client, %ammo)
{
    commandToClient(%client, 'setAmmoHudCount', %obj.getObjectMount().ammoCache[%ammo]);
}

// todo: modify so that it sets the damage level for all slots (0-15) if type ^ player?
function VehicleData::onDamage(%this,%obj)
{
   %damage = %obj.getDamageLevel();
   if (%damage >= %this.destroyedLevel)
   {
      if(%obj.getDamageState() !$= "Destroyed")
      {
         if(%obj.respawnTime !$= "")
            %obj.marker.schedule = %obj.marker.data.schedule(%obj.respawnTime, "respawn", %obj.marker);
         %obj.setDamageState(Destroyed);
      }
   }
   else
   {
      if(%obj.getDamageState() !$= "Enabled")
         %obj.setDamageState(Enabled);
   }
}

function VehicleData::displayKillMessage(%data, %targetVeh, %sourceObject)
{
    %targetVehName = %targetVeh.nameOverride;
    %targetPilotName = %targetVeh.getControllingClient().name;

    if(!isObject(%sourceObject))
        messageAll('msgAnonVehKill', $DeathMessageAnon[mFloor(getRandom() * $DeathMessageAnonCount)], %targetVehName, %targetPilotName);
    else if(%sourceObject.isVehicle())
    {
        if(%sourceObject.getMountNodeObject(0) != %sourceObject.getControllingClient().player)
            return;

//        echo("deathmsg
        %sourceVehName = %sourceObject.nameOverride;
        %sourcePilotName = %sourceObject.getControllingClient().name;
        
        messageAll('msgVehPilotKill', $DeathMessagePilot[mFloor(getRandom() * $DeathMessagePilotCount)], %targetVehName, %targetPilotName, %sourceVehName, %sourcePilotName);
    }
    else if(%sourceObject.isTurret())
    {
        %sc = %sourceObject.getControllingClient();
        %node = %sourceObject.getObjectSlot(%sc.player);
        
        if(%node != -1)
        {
            if(%sourceObject.team == %targetVeh.team)
                return;
                
            messageAll('msgTurretVehKill', $DeathMessageAutoTurret[mFloor(getRandom() * $DeathMessageAutoTurretCount)], %targetVehName, %targetPilotName, %sourceVehName, %sourcePilotName);
        }
        else
        {
            %sourceVehName = %sourceObject.nameOverride;
            %sourcePilotName = %sourceObject.getControllingClient().name;

            messageAll('msgVehGunnerKill', $DeathMessageGunner[mFloor(getRandom() * $DeathMessageGunnerCount)], %targetVehName, %targetPilotName, %sourceVehName, %sourcePilotName);
        }
    }
    else if(%sourceObject.isPlayer())
    {
        %veh = %sourceObject.getObjectMount();

        if(%veh.isVehicle())
        {
            %sourceVehName = %sourceObject.nameOverride;
            
            messageAll('msgVehPassengerKill', $DeathMessagePassenger[mFloor(getRandom() * $DeathMessagePassengerCount)], %targetVehName, %targetPilotName, %sourceVehName, %sourceObject.client.name);
        }
        else
            messageAll('msgVehPlayerKill', $DeathMessageVehPlayer[mFloor(getRandom() * $DeathMessageVehPlayerCount)], %targetVehName, %targetPilotName, 0, %sourceObject.client.name);
    }
}

function VehicleData::onDestroyed(%data, %obj, %prevState)
{
    %data.displayKillMessage(%obj, %obj.lastDamagedBy);

    if(%obj.lastDamagedBy)
    {
        %destroyer = %obj.lastDamagedBy;
        game.vehicleDestroyed(%obj, %destroyer);
    }

    %tp = %obj.getMountNodeObject(0);

//   if(%obj.turretObject > 0)
//      if(%obj.turretObject.getControllingClient())
//         %obj.turretObject.getDataBlock().playerDismount(%obj.turretObject);

   // todo: combine above code with this code so that if they are controlling a turret, they get playerDismount() called too
   for(%i = 0; %i < %obj.getDatablock().numMountPoints; %i++)
   {
      if ((%flingee = %obj.getMountNodeObject(%i)) && %flingee.isPlayer())
      {
         %flingee.getDataBlock().doDismount(%flingee, true);
         %xVel = 250.0 - (getRandom() * 500.0);
         %yVel = 250.0 - (getRandom() * 500.0);
         %zVel = (getRandom() * 100.0) + 50.0;
         %flingVel = %xVel @ " " @ %yVel @ " " @ %zVel;
         %flingee.applyImpulse(%flingee.getTransform(), %flingVel);
//         echo("got player..." @ %flingee.getClassName());
         %flingee.damage(0, %obj.getPosition(), 125, $DamageType::Crash, 0, $DamageGroupMask::Kinetic);
      }
   }

	radiusVehicleExplosion(%data, %obj, %tp);

   %data.deleteAllMounted(%obj);

   // keen: anti-vehicle run over fix
   %obj.setFrozenState(true);
   %obj.getDatablock().schedule(256, "hideAndSeek", %obj);
}

function VehicleData::hideAndSeek(%data, %obj)
{
    %obj.setPosition(VectorScale(vectorRand(), -10000));
    %obj.schedule(2048, "delete");

    // delete the other objects that might be hanging around on the vehicle
    for(%i = 0; %i < 16; %i++)
    {
        %something = %obj.getMountNodeObject(%i);

        if(isObject(%something))
            %something.schedule(1000, "delete");
    }
}

function radiusVehicleExplosion(%data, %vehicle, %sourceObject)
{
	// this is a modified version of RadiusExplosion() from projectiles.cs
	%position = %vehicle.getPosition();
   InitContainerRadiusSearch(%position, %data.explosionRadius, $TypeMasks::PlayerObjectType      |
                                                 $TypeMasks::VehicleObjectType     |
                                                 $TypeMasks::MoveableObjectType    |
                                                 $TypeMasks::StaticShapeObjectType |
                                                 $TypeMasks::ForceFieldObjectType  |
                                                 $TypeMasks::TurretObjectType      |
                                                 $TypeMasks::ItemObjectType);

   %numTargets = 0;
   while ((%targetObject = containerSearchNext()) != 0)
   {
		if(%targetObject == %vehicle)
			continue;

      %dist = containerSearchCurrRadDamageDist();

      if (%dist > %data.explosionRadius)
         continue;

      if (%targetObject.isMounted())
      {
         %mount = %targetObject.getObjectMount();
			if(%mount == %vehicle)
				continue;

         %found = -1;
         for (%i = 0; %i < %mount.getDataBlock().numMountPoints; %i++)
         {
            if (%mount.getMountNodeObject(%i) == %targetObject)
            {
               %found = %i;
               break;
            }
         }

         if (%found != -1)
         {
            if (%mount.getDataBlock().isProtectedMountPoint[%found] && (%mount != %vehicle))
               continue;
         }
      }

      %targets[%numTargets]     = %targetObject;
      %targetDists[%numTargets] = %dist;
      %numTargets++;
   }

   for (%i = 0; %i < %numTargets; %i++)
   {
      %targetObject = %targets[%i];
      %dist = %targetDists[%i];

      %coverage = calcExplosionCoverage(%position, %targetObject,
                                        ($TypeMasks::InteriorObjectType |
                                         $TypeMasks::TerrainObjectType |
                                         $TypeMasks::ForceFieldObjectType));
      if (%coverage == 0)
         continue;

      %amount = (1.0 - (%dist / %data.explosionRadius)) * %coverage * %data.explosionDamage;
      %targetData = %targetObject.getDataBlock();

      %momVec = "0 0 1";

      if(%amount > 0)
         %targetData.damageObject(%targetObject, %sourceObject, %position, %amount, $DamageType::VehicleExplosion, %momVec);
   }
}

function VehicleData::deleteAllMounted(%obj)
{

}

function VehicleData::onTrigger(%data, %obj, %trigger, %state)
{
    if(%trigger == 0)
    {
        %part = %obj.weapon[%obj.selectedWeapon];

        if(!isObject(%part))
            return;

        %start = $VehicleHardpoints[%obj.vid, %obj.hardpointRef[%obj.selectedWeapon], "imageStart"];
        %run = $VehicleHardpoints[%obj.vid, %obj.hardpointRef[%obj.selectedWeapon], "imageCount"];

        %part.onTrigger(%obj, %state, %start, %run);
    }
    else if(%trigger == 3)
        %obj.bJetState = %state;
    else if(%trigger == 4 && %state)
        %data.triggerGrenade(%obj, %obj.getMountNodeObject(0));
    else if(%trigger == 5)
        %data.triggerMine(%obj, %obj.getMountNodeObject(0), 0, %state);
}

function TurretData::onTrigger(%data, %obj, %trigger, %state)
{
   switch (%trigger)
   {
      case 0:
        %obj.fireTrigger = %state;

        if(%obj.selectedWeapon $= "")
            %obj.selectedWeapon = 0;

        %part = %obj.weapon[%obj.selectedWeapon];

        if(!isObject(%part))
            return;

        %vehicle = %obj.getObjectMount();

        %start = $VehicleHardpoints[%vehicle.vid, %obj.hardpointRef[%obj.selectedWeapon], "imageStart"];
        %run = $VehicleHardpoints[%vehicle.vid, %obj.hardpointRef[%obj.selectedWeapon], "imageCount"];

        %part.onTrigger(%obj, %state, %start, %run);

      case 2:
         if(%state)
         {
            %obj.getDataBlock().playerDismount(%obj);
         }
   }
}

function TurretData::onDamage(%data, %obj)
{
    if(%obj.mountedVehicleTurret != true)
    {
        Parent::onDamage(%data, %obj);
        return;
    }

   %newDamageVal = %obj.getDamageLevel();

   if(%obj.lastDamageVal !$= "")
      if(isObject(%obj.getObjectMount()) && %obj.lastDamageVal > %newDamageVal)
         %obj.getObjectMount().setDamageLevel(%newDamageVal);

   %obj.lastDamageVal = %newDamageVal;
}

function TurretData::damageObject(%this, %targetObject, %sourceObject, %position, %amount, %damageType ,%vec, %client, %projectile)
{
    if(%targetObject.mountedVehicleTurret != true)
    {
        Parent::damageObject(%this, %targetObject, %sourceObject, %position, %amount, %damageType ,%vec, %client, %projectile);
        return;
    }

   //If vehicle turret is hit then apply damage to the vehicle
   %vehicle = %targetObject.getObjectMount();

   if(%vehicle)
      %vehicle.getDataBlock().damageObject(%vehicle, %sourceObject, %position, %amount, %damageType, %vec, %client, %projectile);
}

function TurretData::playerDismount(%data, %obj)
{
   //Passenger Exiting
   %obj.fireTrigger = 0;

   for(%s = 0; %s < 8; %s++)
       %obj.setImageTrigger(%s, false);

   %client = %obj.getControllingClient();
// %client.setControlObject(%client.player);
   %client.player.mountImage(%client.player.lastWeapon, $WeaponSlot);
   %client.player.mountVehicle = false;
//   setTargetSensorGroup(%obj.getTarget(), 0);
//   setTargetNeverVisMask(%obj.getTarget(), 0xffffffff);
//   %client.player.getDataBlock().doDismount(%client.player);
}

function TurretData::doDismount(%data, %obj)
{
    // Do nothing
}

function VehicleData::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType, %momVec, %theClient, %srcProj)
{
   if(%targetObject.lastHitFlags & $Projectile::RepairProjectile)
   {
       %mul = %targetObject.numSeated < 1 ? 1 : %targetObject.numSeated;
       
       %targetObject.setDamageLevel(%targetObject.getDamageLevel() - (%amount * %mul));
//       echo("healed vehicle for" SPC (%amount * %mul));
       zapEffect(%targetObject, "FXRedShift");
       %targetObject.lastHitFlags = 0;
       return;
   }
   
   if(%proj !$= "")
   {
      if(%amount > 0 && %targetObject.lastDamageProj !$= %proj)
      {
         %targetObject.lastDamageProj = %proj;
         %targetObject.lastDamageAmount = %amount;
      }
      else if(%targetObject.lastDamageAmount < %amount)
         %amount = %amount - %targetObject.lastDamageAmount;
      else
         return;
   }

   // check for team damage
   %sourceClient = %sourceObject ? %sourceObject.getOwnerClient() : 0;
   %targetTeam = getTargetSensorGroup(%targetObject.getTarget());

   if(%sourceClient)
      %sourceTeam = %sourceClient.getSensorGroup();
   else if(isObject(%sourceObject) && %sourceObject.getClassName() $= "Turret")
   {
      %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
      %sourceClient = %sourceObject.getControllingClient(); // z0dd - ZOD, 6/10/02. Play a sound to client when they hit a vehicle with a controlled turret
   }
   else
   {
      %sourceTeam = %sourceObject ? getTargetSensorGroup(%sourceObject.getTarget()) : -1;
      // Client is allready defined and this spams console - ZOD
      //%sourceClient = %sourceObject.getControllingClient(); // z0dd - ZOD, 6/10/02. Play a sound to client when they hit a vehicle from a vehicle
   }

    // vehicles no longer obey team damage when TD setting is off
    if(!$teamDamage && (%targetTeam == %sourceTeam))
       return;
    //but we do want to track the destroyer
    if(%sourceObject)
    {
        %targetObject.lastDamagedBy = %sourceObject;
        %targetObject.lastDamageType = %damageType;
    }
    else
        %targetObject.lastDamagedBy = 0;

   // ----------------------------------------------------------------------------------
   // z0dd - ZOD, 6/10/02. Play a sound to client when they hit a vehicle
   // keen: removed as hitsound alreads plays with reticle
//   if(%sourceClient && %sourceClient.vehicleHitSound)
//   {
//      if(%targetTeam != %sourceTeam)
//      {
//         if ((%damageType > 0  && %damageType < 11) ||
//             (%damageType == 13)                    ||
//             (%damageType > 15 && %damageType < 24) ||
//             (%damageType > 25 && %damageType < 32))
//         {
//            messageClient(%sourceClient, 'MsgClientHit', %sourceClient.vehicleHitWav);
//         }
//      }
//   }
   // ----------------------------------------------------------------------------------

   // Scale damage type & include shield calculations...
   %amount *= %targetObject.damageReduceFactor;
   
   if(%targetObject.tagYoureIt)
      %amount *= 1.5;
      
//   %amount *= %data.damageScale[%damageType];
//   %amount *= %targetObject.armorDamageFactor[%element];

   if(%targetObject.isShielded && (%targetObject.lastHitFlags & $Projectile::ArmorOnly) == 0)
       %amount = %targetObject.shieldSource.getDatablock().onShieldDamage(%targetObject.shieldSource, %targetObject, %position, %amount, %damageType); //installedVehiclePart[$VehiclePartType::Shield].onShieldDamage(%targetObject, %position, %amount, %damageType);

   if(%targetObject.lastHitFlags & $Projectile::Nullifier)
       %amount = 0;

   if(%targetObject.lastHitFlags & $Projectile::Energy)
       %amount *= 0.75;
       
   if(%amount == 0)
      return;

   %targetObject.applyDamage(%amount);

   if(getRandom() < (0.2 + ((%targetObject.getDamageLevel() / %data.maxDamage) * 0.1)))
      createHitDebris(%position);

   if(%targetObject.getDamageState() $= "Destroyed" )
   {
      if( %momVec !$= "")
         %targetObject.setMomentumVector(%momVec);
   }
}

// Trigger rerouting to vehicle
function VehicleData::triggerPack(%data, %obj, %player)
{
    if(%obj.installedModule == true)
    {
        %time = getSimTime();
        %module = %obj.installedVehiclePart[$VehiclePartType::Module].getName();

        if(%time < %obj.moduleTimeout[%module])
        {
            %rem = mCeil((%obj.moduleTimeout[%module] - %time) / 1000);
            
            messageClient(%player.client, 'MsgVModCooldown', '\c2%2 cooling down, %1 seconds remaining.', %rem, %module.name);
            return;
        }
        
        %cd = 500;

        if(%module.triggerType == $VMTrigger::Push)
        {
            %cdt = %module.triggerPush(%data, %obj, %player);
            %cd = %cdt > 0 ? %cdt : %module.cooldownTime;
        }
        else if(%module.triggerType == $VMTrigger::Toggle)
        {
            %obj.moduleTriggerState = !%obj.moduleTriggerState;

            %cdt = %module.triggerToggle(%data, %obj, %player, %obj.moduleTriggerState);
            %cd = %cdt > 0 ? %cdt : %module.cooldownTime;
        }

        %obj.moduleTimeout[%module] = %time + %cd;
    }
}

function VehicleData::triggerModuleOff(%data, %obj)
{
    if(%obj.installedModule == true)
    {
        %module = %obj.installedVehiclePart[$VehiclePartType::Module].getName();

        if(%module.triggerType == $VMTrigger::Toggle)
        {
            %obj.moduleTriggerState = false;

            %module.triggerToggle(%data, %obj, %obj.getMountNodeObject(0), %obj.moduleTriggerState);
        }
    }
}

// args:
// 1    menuname
// 2    vehicle
// 3    turretnode
// 4    weaponid
function VehicleData::turretTriggerPack(%data, %obj, %player, %turret)
{
    InitContainerRadiusSearch(%obj.getWorldBoxCenter(), 25, $TypeMasks::StaticShapeObjectType);

    while((%hit = containerSearchNext()) != 0)
    {
        if(%hit.team == %obj.team && %hit.getDatablock().shapeFile $= "vehicle_pad.dts")
        {
            %time = getSimTime();

            if(%time < %turret.nextMenuTime)
                return;

            if(vectorLen(%obj.getVelocity()) > 5)
            {
                messageClient(%player.client, 'MsgTurMTooFast', '\c2Vehicle is moving too fast to change turret loadout, please land before attempting again.');
                return;
            }

            %turret.nextMenuTime = %time + 1000;
            %tag = 'scoreScreen';
            
            serverCmdShowHud(%player.client, 'scoreScreen');
            commandToClient(%player.client, 'TogglePlayHuds', false);
//            messageClient(%player.client, 'CloseHud', "", %tag);
//            messageClient(%player.client, 'OpenHud', "", %tag);
            %player.client.scoreHudMenu = $Menu::InTurret;
            inTurretProcessGameLink(Game, %tag, %player.client, "HardpointSelect", %obj, %turret.onNode);
            
            return;
        }
    }
    
    if(%obj.installedModule == true)
        %obj.installedVehiclePart[$VehiclePartType::Module].onTurretTriggerPack(%obj, %player, %turret);
}

function VehicleData::triggerGrenade(%data, %vehicle, %player)
{
    if(%obj.installedModule == true)
        %obj.installedVehiclePart[$VehiclePartType::Module].onTriggerGrenade(%vehicle, %player);
    else
        %vehicle.launchMissile();
}

function VehicleData::triggerMine(%data, %obj, %player, %slot, %state)
{
    if(%state)
        return;

    if(%slot == 0)
    {
        if(%data.restockVehicle(%obj, %player))
            return;
    }
        
    if(%obj.installedModule == true)
        %obj.installedVehiclePart[$VehiclePartType::Module].onTriggerMine(%obj, %player, %slot);
}

// Used to run vehicle restock script
function VehicleData::restockVehicle(%data, %vehicle, %player)
{
    InitContainerRadiusSearch(%vehicle.getWorldBoxCenter(), 25, $TypeMasks::StaticShapeObjectType);

    while((%hit = containerSearchNext()) != 0)
    {
        if(%hit.team == %vehicle.team && %hit.getDatablock().shapeFile $= "vehicle_pad.dts")
        {
            %time = getSimTime();

            if(%time < %vehicle.nextRestockTime)
            {
                %cdTimeSec = mCeil((%vehicle.nextRestockTime - %time) / 1000);

                messageClient(%player.client, 'MsgVehNoReloadCD', '\c2Vehicle has recently restocked, come back in %1 seconds.', %cdTimeSec);
                return true;
            }

            if(vectorLen(%vehicle.getVelocity()) > 5)
            {
                messageClient(%player.client, 'MsgVehRTooFast', '\c2Vehicle is moving too fast to restock, please land before attempting again.');
                return true;
            }
        
            // Reload vehicle
            %vehicle.play3d(VehicleRestockSound);
            messageClient(%player.client, 'MsgVehReload', '\c2Vehicle landed on pad; ammo restocked, module reset, and hull repaired.');

            // Clear ammo table first so multiple weapons can use the same ammo pool - 0 schedule actually runs code after all other code runs during this tick
            for(%i = 0; %i < 6; %i++)
            {
                %part = %vehicle.installedVehiclePart[(%i + 3)];

                if(isObject(%part))
                {
                    if(%part.ammo !$= "")
                        %vehicle.ammoCache[%part.ammo] = 0;
                        
                    %part.schedule(0, "reArm", %data, %vehicle, %player, %i);
                }
            }
            
            // Repair vehicle and restart shield emitter if depleted
            zapVehicle(%vehicle, FXHealGreen);
            %vehicle.setDamageLevel(0);
            %vehicle.setEnergyLevel(%vehicle.getMaxEnergy());
            %vehicle.shieldSource.getDatablock().schedule(1000, "startRecharge", %vehicle.shieldSource);
            %vehicle.nextRestockTime = %time + 120000;
            %vehicle.installedVehiclePart[$VehiclePartType::Module].reArm(%data, %vehicle, %player, -1);
            
            // Refresh reticle counts of all players either flying the vehicle (pilot) or in a turret position - tested by
            // whether they have a mounted image - pilots and gunners will not while it's 99% likely others on the vehicle will
            for(%i = 0; %i < 16; %i++)
            {
                %pl = %vehicle.getMountNodeObject(%i);

                if(%pl > 0 && %pl.isPlayer())
                {
                    if(%i == 0 && !%pl.client.isAIControlled())
                        schedule(32, %vehicle, "serverCmdSetVehicleWeapon", %pl.client, 0);
                    else if(%pl.client.getControlObject().isTurret())
                        schedule(32, %vehicle, "serverCmdSetVehicleWeapon", %pl.client, 0);
                }
            }
        }
    }
    
    return false;
}

function VehicleData::onTick(%data, %obj)
{
    if(!isObject(%obj))
        return false;
        
    if(%obj.updateFrames == true && (%obj.tickCount % 96 == 0)) // 3 seconds
        %data.processFrameUpdate(%obj);
    
    %pilot = %obj.getMountNodeObject(0);

    if(%obj.tickCount % 8 == 0)
    {
        if(isObject(%pilot))
            %data.updatePilotStatus(%obj, %pilot);
        
        if(%obj.thrusterCount > 0)
            %data.processThrusterUpdate(%obj);
    }

    %data.tickPhysics(%obj);

    %obj.tickCount++;
    %data.schedule($g_TickTime, "onTick", %obj);

    return true;
}

function VehicleData::processThrusterUpdate(%data, %obj)
{
    %speed = vectorLen(%obj.getVelocity());
    
    for(%i = 0; %i < %obj.thrusterCount; %i++)
    {
        %bUpdate = false;

        if(%obj.thrusterData[%i, "speed"] == 0)
            %obj.thrusterData[%i, "state"] = (%obj.bJetState == true);
        else //if(%speed >= %obj.thrusterData[%i, "speed"])
           %obj.thrusterData[%i, "state"] = (%speed >= %obj.thrusterData[%i, "speed"]);
        
        %src = %obj.thrusterData[%i, "bFrame"] ? %obj.frame : %obj;
        %src.setImageTrigger(%obj.thrusterData[%i, "slot"], %obj.thrusterData[%i, "state"]);
    }
}

function VehicleData::registerThruster(%data, %obj, %img, %slot, %speed, %bFrame) // speed 0 == jet thruster
{
    if(%obj.thrusterCount $= "")
        %obj.thrusterCount = 0;
        
    %obj.thrusterData[%obj.thrusterCount, "slot"] = %slot;
    %obj.thrusterData[%obj.thrusterCount, "speed"] = %speed;
    %obj.thrusterData[%obj.thrusterCount, "bFrame"] = %bFrame;
    %obj.thrusterData[%obj.thrusterCount, "state"] = false;

    %obj.thrusterCount++;
    
    %src = %bFrame ? %obj.frame : %obj;
    %src.mountImage(%img, %slot);
}

function VehicleData::processFrameUpdate(%data, %obj)
{
   if(isObject(%obj.frame))
   {
      %obj.mountObject(%obj.frame, %data.frameMountPoint);
      %obj.frame.setDamageLevel(%obj.getDamageLevel());
   }
}

function VehicleData::tickPhysics(%data, %obj)
{
    %pct = %obj.getDamagePct();
    %dangerLevel = %data.damageLevelTolerance[1] > 0 ? %data.damageLevelTolerance[1] : 0.8;

    if(%pct >= %dangerLevel)
    {
        if(!%obj.onFire)
        {
            %obj.onFire = true;
            %obj.playAudio(0, VehicleOnFireSound);
        }
        
        if(%obj.tickCount % 12 == 0)
        {
            if(%obj.tickCount % 48 == 0)
                %obj.playAudio(3, EngineAlertSound);

            %vel = vectorLen(%obj.getVelocity());
            %force = %vel + (%pct * 500);
            %stVec = VectorRand();
            %nVec = vectorScale(%stVec, %force);
            %obj.applyImpulse(%obj.getTransform(), %nVec);

            %dmgFXPos = vectorProject(%obj.position, %stVec, getRandom() * %data.cameraMaxDist * 0.5);
            createHitDebris(%dmgFXPos);
        }
    }
    else if(%obj.onFire)
    {
        %obj.onFire = false;
        %obj.stopAudio(0);
    }
    
    // Don't run physics calculations for bot pilots
    %bp = %obj.getMountNodeObject(0);
    
    if(%bp > 0 && %bp.client.isAIControlled())
        return;
    
    switch(%data.physicsType)
    {
        case $VehiclePhysics::ConstantMomentum:
            %data.tickConstantMomentum(%obj);
            
        case $VehiclePhysics::Aerodynamic:
            %data.tickAerodynamic(%obj);

        case $VehiclePhysics::ForwardsOnly:
            %data.tickForwardsOnly(%obj);
            
        default:
            return;
    }
}

// Physics 1 - Meltdown 3 constant motion/anti backwards physics
// ------------------------------------------------------------
function VehicleData::tickConstantMomentum(%data, %obj)
{
    if(%data.checkMinVelocity > 0)
    {
        %velVec = %obj.getVelocity();
        %vel = vectorLen(%velVec);
//        %fvel = vectorLen(getWords(%velVec, 0, 1) SPC "0");
        %nVec = "0 0 0";
        %doImpulse = false;
        %doStall = false;

        if(%data.isMovingBackwards(%obj))
        {
//            echo("mvBack" SPC %vel);
            if(%vel > %data.checkMinReverseSpeed)
            {
                %force = %vel + (%data.mass * 3.5); // %obj.totalWeight
                %nVec = vectorScale(%obj.getForwardVector(), %force);
                %doImpulse = true;
            }
        }

        if(%vel < %data.checkMinVelocity)
        {
            if(!%data.findGround(%obj))
            {
                %force = %vel + (%data.mass * 3.5); // totalWeight
                %nVec = vectorScale("0 0 -1", %force);
                %doImpulse = true;
                %doStall = true;
            }
        }

        if(%doImpulse)
            %obj.applyImpulse(%obj.getTransform(), %nVec);

        %obj.stallState = %doStall;
    }
}

function VehicleData::findGround(%data, %obj)
{
   %dist = %data.checkMinHeight * -1;
   %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticObjectType | $TypeMasks::WaterObjectType;
   %pos = %obj.position;
   %end = VectorAdd(%pos, "0 0" SPC %dist);
   %result = containerRayCast(%pos, %end, %mask, %obj);
   %target = getWord(%result, 0);

   if(isObject(%target))
      return true;
   else
      return false;
}

function VehicleData::isMovingBackwards(%data, %obj)
{
    %forwardVec = vectorNormalize(%obj.getVelocity());
//    %objPos = %obj.position;
    %dif = %obj.getForwardVector();
    %dot = VectorDot(%dif, %forwardVec);

    if(%dot >= mCos(1.05))
        return false;
    else
        return true;
}

// Physics 2 - Aerodynamics by Ragora, adapted to vehicle tick thread structure
// ------------------------------------------------------------
function VehicleData::tickAerodynamic(%data, %vehicle)
{
    //==========================================================================
    // Gravity/Nose drop Simulation
    //==========================================================================
    %vehicleCenter = %vehicle.getWorldBoxCenter();
    %forwardVector = %vehicle.getForwardVector();
    %velocity = %vehicle.getVelocity();
    %raycast = ContainerRayCast(%vehicleCenter, "0 0 -9999", $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType, %vehicle);
    %hitPosition = getWords(%raycast, 1, 3);
    %height = vectorDist(%vehicleCenter, %hitPosition);
    %length = vectorLen(%velocity);

    if (%height > %data.hoverDisableHeight)
    {
        // Not enough velocity to generate any significant lift
        if (%length < %data.noseDropThreshold)
        {
            %gravity = getGravity() * %data.gravityMultiplier;
            %vehicle.applyImpulse(%vehicleCenter, vectorScale("0 0 1", %gravity));

            %vehicle.applyImpulse(vectorAdd(%vehicleCenter, vectorScale(%forwardVector, %data.noseDistance)), vectorScale("0 0 1", -%data.noseDropStrength));
            %vehicle.stallState = true;
        }
        else
            %vehicle.stallState = false;
    }

    //==========================================================================
    // Lift Simulation
    //==========================================================================

    // Helps prevent precision issues
    if (%length >= 1)
    {
        // Determine how much of the vehicle's current velocity is actually focused forward
        %forwardForce = vectorDot(%forwardVector, %velocity);

        // We're flying backwards
        if (%fowardForce < 0)
            return;

        // Calculate lift force
        %liftForce = (%forwardForce * %data.liftForceRatio) * %data.liftForceMultiplier;

        // Determine the vehicle's angle of inclination (no inclination relative to the horizontal axis = best lift)
        %inclination = mAbs(getWord(%forwardVector, 2));
        %liftForce *= (1 - %inclination);

        // Apply the upward lift
        %vehicle.applyImpulse(%vehicleCenter, vectorScale("0 0 1", %liftForce));
    }
}

// Physics 3 - Meltdown 3 anti backwards physics only
// ------------------------------------------------------------
function VehicleData::tickForwardsOnly(%data, %obj)
{
    if(%data.checkMinVelocity > 0)
    {
        %velVec = %obj.getVelocity();
        %vel = vectorLen(%velVec);
        %fvel = vectorLen(getWords(%velVec, 0, 1) SPC "0");
        %nVec = "0 0 0";
        %doImpulse = false;

        if(%data.isMovingBackwards(%obj))
        {
            if(%vel > %data.checkMinReverseSpeed)
            {
                %force = %vel + (%data.mass * 3.5); // totalWeight
                %nVec = vectorScale(%obj.getForwardVector(), %force);
                %doImpulse = true;
            }
        }

        if(%doImpulse)
            %obj.applyImpulse(%obj.getTransform(), %nVec);
    }
}

function VehicleData::onPilotSeated(%data, %obj, %player)
{
   commandToClient(%player.client, 'SetWeaponryVehicleKeys', true);
   commandToClient(%player.client, 'ShowAmmoCounter');
   
   setTargetSensorGroup(%obj.getTarget(), %obj.team);

   // if there is a turret, set its team as well.
   for(%i = 0; %i < 16; %i++)
   {
      %turret = %obj.getMountNodeObject(%i);

      if(isObject(%turret) && %turret.isTurret())
      {
          setTargetSensorGroup(%turret.getTarget(), %obj.team);
          setTargetNeverVisMask(%turret.getTarget(), 0xffffffff);
      }
   }
   
   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
      
    if(%obj.selectedWeapon $= "")
        %obj.selectedWeapon = 0;

    %wep = %obj.weapon[%obj.selectedWeapon];

    if(!isObject(%wep))
        return;

    serverCmdSetVehicleWeapon(%player.client, %obj.selectedWeapon);
}

function VehicleData::updatePilotStatus(%data, %obj, %pilot)
{
    %hpPct = %obj.getDamageLeftPct();
    %shieldPct = %obj.shieldSource.strength / %obj.shieldSource.maxStrength;
    %hpGraph = createColorGraph("|", 20, %hpPct, "<color:FF0000>", "<color:FFFF00>", "<color:00FF00>", "<color:777777>", 0.3, 0.6);
    %shGraph = createColorGraph("|", 20, %shieldPct, "<color:FF0000>", "<color:FFFF00>", "<color:00FF00>", "<color:777777>", 0.3, 0.6);

    if(%obj.selectedWeapon $= "")
        %obj.selectedWeapon = 0;

    %wep = %obj.weapon[%obj.selectedWeapon];
    %weapon = "None";
    %hasWep = false;
    %headTracking = false;
    
    if(isObject(%wep) && %wep.name !$= "")
    {
        %weapon = %wep.name;
        %hasWep = true;
        %htimg = %obj.getMountedImage(2); 

        if(%htimg !$= "")
            %headTracking = %htimg.pilotHeadTracking == true;
    }

     %module = %obj.installedVehiclePart[$VehiclePartType::Module].name;

//    if(%obj.stallState)
//        commandToClient(%pilot.client, 'BottomPrint', "Hull: "@%hpGraph@" ("@mCeil(%hpPct * 100)@"%) | Shield: "@%shGraph@" ("@mCeil(%shieldPct * 100)@"%)\nSpeed: "@mFloor(vectorLen(%obj.getVelocity()) * 3.6)@" KPH | Missiles: "@%obj.missileCount@" | Weapon: "@%weapon NL "<color:FF6633>>> STALLING, INCREASE SPEED <<", 1, 3);
//    else if(%hasWep && %headTracking)
//        commandToClient(%pilot.client, 'BottomPrint', "Hull: "@%hpGraph@" ("@mCeil(%hpPct * 100)@"%) | Shield: "@%shGraph@" ("@mCeil(%shieldPct * 100)@"%)\nSpeed: "@mFloor(vectorLen(%obj.getVelocity()) * 3.6)@" KPH | Missiles: "@%obj.missileCount@" | Weapon [Mouselook]: "@%weapon NL "Module: "@%module, 1, 3);
//    else
//        commandToClient(%pilot.client, 'BottomPrint', "Hull: "@%hpGraph@" ("@mCeil(%hpPct * 100)@"%) | Shield: "@%shGraph@" ("@mCeil(%shieldPct * 100)@"%)\nSpeed: "@mFloor(vectorLen(%obj.getVelocity()) * 3.6)@" KPH | Missiles: "@%obj.missileCount@" | Weapon: "@%weapon NL "Module: "@%module, 1, 3);

    commandToClient(%pilot.client, 'GraphPilot', %hpPct, %shieldPct, %obj.missileCount, %weapon, %module, (%hasWep && %headTracking), %obj.stallState);
//    messageClient(%pilot.client, 'MsgSPCurrentObjective2', "", %ammoStr);
}

function VehicleData::onGunnerSeated(%data, %obj, %player, %turretNode) // Only called to update turret status, turret is implied here - if not, there's bigger problems to worry about
{
    %turret = %obj.getMountNodeObject(%turretNode);
    %player.vehicleTurret = %turret;
    %player.setTransform("0 0 0 0 0 1 0");
    %player.lastWeapon = %player.getMountedImage($WeaponSlot);
    %player.unmountImage($WeaponSlot);

    if(!%player.client.isAIControlled())
    {
        %player.setControlObject(%turret);
        %player.client.setObjectActiveImage(%turret, 2);
    }

    %turret.turreteer = %player;
    %turret.owner = %player.client;
    %turret.onNode = %turretNode;

    commandToClient(%player.client, 'SetWeaponryVehicleKeys', true);
    //commandToClient(%player.client, 'setHudMode', 'Pilot', "HAPC", %node);
    commandToClient(%player.client, 'setHudMode', 'Pilot', "Assault", %node);
//    commandToClient(%player.client, 'setRepairReticle');

//    %turret = %turrObj > 0 ? %turrObj : %obj.getMountNodeObject(%turretNode);
//    %turrObj = %turrObj > 0 ? %turrObj : %turret;

    serverCmdSetVehicleWeapon(%player.client, %turret.selectedWeapon);

    %wep = %turret.weapon[%turret.selectedWeapon];
    %ammocount = %obj.ammoCache[%wep.ammo] $= "" ? 0 : %obj.ammoCache[%wep.ammo];

    if(isObject(%wep) && %wep.name !$= "")
    {
        if(%wep.ammo !$= "")
            %turret.getDatablock().updateAmmoCount(%turret, %player.client, %wep.ammo);
    }

    %data.updateGunnerStatus(%obj, %player);
    schedule(32, 0, "commandToClient", %player.client, 'ShowAmmoCounter'); // won't show unless it's scheduled... I don't even
}

function VehicleData::updateGunnerStatus(%data, %obj, %player)
{
    if(%player.getObjectMount() != %obj)
        return;
        
    %wep = %player.vehicleTurret.weapon[%player.vehicleTurret.selectedWeapon];
    %weapon = "None";

    if(isObject(%wep) && %wep.name !$= "")
        %weapon = %wep.name;

    %hpPct = %obj.getDamageLeftPct();
    %shieldPct = %obj.shieldSource.strength / %obj.shieldSource.maxStrength;
//    %ammocount = %obj.ammoCache[%wep.ammo] $= "" ? 0 : %obj.ammoCache[%wep.ammo];
    
//    commandToClient(%player.client, 'BottomPrint', "Hull: "@mCeil(%hpPct * 100)@"% | Shield: "@mCeil(%shieldPct * 100)@"% | Missiles: "@%obj.missileCount@"\nCurrent Weapon: "@%weapon, 1, 2);
    
    %module = %obj.installedVehiclePart[$VehiclePartType::Module].name;
    commandToClient(%player.client, 'GraphGunner', %hpPct, %shieldPct, %obj.missileCount, %weapon, %module);
    
    %data.schedule(256, "updateGunnerStatus", %obj, %player);
}

function VehicleData::playerDismounted(%data, %obj, %player)
{
   for(%i = 0; %i < 8; %i++)
      %obj.setImageTrigger(%i, false);

   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, true );
}

function VehicleData::updatePassengerString(%data, %obj)
{
   // build a space-separated string representing passengers
   // 0 = no passenger; 1 = passenger (e.g. "1 0 0 1 1 0 ")
   %passString = buildPassengerString(%obj);

   %obj.numSeated = 0;
   
    if(%obj.bonusFirstTimeSetup $= "")
    {
        %obj.bonusFirstTimeSetup = true;
        %obj.defshieldAbsorbFactor = %obj.shieldAbsorbFactor;
        %obj.defdamageBuffFactor = %obj.damageBuffFactor;
        %obj.defenergyEfficiencyFactor = %obj.energyEfficiencyFactor;
    }
   
    %obj.shieldAbsorbFactor = %obj.defshieldAbsorbFactor;
    %obj.damageBuffFactor = %obj.defdamageBuffFactor;
    %obj.energyEfficiencyFactor = %obj.defenergyEfficiencyFactor;
   
   // send the string of passengers to all mounted players
   // Also recalculate vehicle bonuses here - odd place? maybe...
   for(%i = 0; %i < %data.numMountPoints; %i++)
   {
      %seat = %obj.getMountNodeObject(%i);

      if(%seat > 0 && %seat.isPlayer())
      {
         commandToClient(%seat.client, 'checkPassengers', %passString);
         %obj.numSeated++;
         
         %ptype = %seat.getArmorType();

         if($ArmorType::Light)
            %obj.shieldAbsorbFactor -= 0.05;
         else if($ArmorType::Medium)
            %obj.damageBuffFactor += 0.05;
         else if($ArmorType::Heavy)
            %obj.energyEfficiencyFactor -= 0.05;
      }
   }
}

function VehicleData::installTurret(%data, %obj, %turretData, %slot)
{
   %turret = TurretData::create(%turretData);
   %turret.selectedWeapon = 0;
   %turret.team = %obj.teamBought;
   %turret.mountedVehicleTurret = true;
   %turret.setSelfPowered();
   %turret.setCapacitorRechargeRate(1); //  %turret.getDataBlock().capacitorRechargeRate // turrets draw from main power
   %turret.setAutoFire(false);
   %turret.setCloaked(true);
   %turret.schedule(5500, "setCloaked", false);
   %turret.vehicle = %obj;
   MissionCleanup.add(%turret);

   setTargetSensorGroup(%turret.getTarget(), %turret.team);
   setTargetNeverVisMask(%turret.getTarget(), 0xffffffff);
   %obj.mountObject(%turret, %slot);

   if(%obj.turretCount $= "")
      %obj.turretCount = 0;
      
   %obj.turrets[%obj.turretCount] = %turret;
   %turret.vTurretID = %obj.turretCount;
   %obj.turretCount++;
   
   // necessary because stupidity with T2
   %turret.getDatablock().abandonCheck(%turret);
   
   return %turret;
}

function VehicleData::cloneTurret(%data, %obj, %src)
{
   %turret = TurretData::create(%src.getDatablock());
   %turret.selectedWeapon = %src.selectedWeapon;
   %turret.team = %src.team;
   %turret.mountedVehicleTurret = %src.mountedVehicleTurret;
   %turret.setSelfPowered();
   %turret.setCapacitorRechargeRate(1);
   %turret.setAutoFire(false);
   %turret.setCloaked(true);
   %turret.vehicle = %obj;
   %turret.turreteer = %src.turreteer;
   %turret.owner = %src.owner;
   
   MissionCleanup.add(%turret);
   setTargetSensorGroup(%turret.getTarget(), %turret.team);
   setTargetNeverVisMask(%turret.getTarget(), 0xffffffff);

   %turret.onNode = %src.onNode;

   // necessary because stupidity with T2 - old turret will have vehicle ref
   // removed so it will get removed naturally
   %turret.getDatablock().abandonCheck(%turret);

   // Transfer turret variables over
   %turret.wepCount = %src.wepCount;
   %turret.installedMissiles = %src.installedMissiles;

   for(%i = 0; %i < 6; %i++)
   {
       %turret.weapon[%i] = %src.weapon[%i];
       %turret.hardpointRef[%i] = %src.hardpointRef[%i];
   }
   
   // Do the swap
   %obj.unmountObject(%src.onNode);
   %src.setPosition("0 0 -10000");
   %obj.mountObject(%turret, %src.onNode);
   %vehicle.turrets[%src.vTurretID] = %turret;
   %turret.vTurretID = %src.vTurretID;
   %src.vehicle = 0;
   %src.hide(true);
   
   return %turret;
}

function TurretData::finishTurretSwap(%data, %obj)
{
    if(!%obj.getMountedImage(0))
        %obj.mountImage("GenericParamImage", 0);
        
    for(%i = 0; %i < 8; %i++)
        %obj.setImageAmmo(%i, true);
        
    %obj.play3d("MBLSwitchSound");
    %obj.setCloaked(false);
}

function TurretData::abandonCheck(%data, %obj)
{
    if(isObject(%obj.vehicle))
        %data.schedule(30000, "abandonCheck", %obj);
    else
        %obj.delete();
}

// Base overrides
function ScoutFlyer::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);
   
   %obj.schedule(5500, "playThread", $ActivateThread, "activate");
}

function ScoutFlyer::onTrigger(%data, %obj, %trigger, %state)
{
    Parent::onTrigger(%data, %obj, %trigger, %state);
}

function ScoutFlyer::playerMounted(%data, %obj, %player, %node)
{
   // scout flyer == SUV (single-user vehicle)
   commandToClient(%player.client, 'setHudMode', 'Pilot', "Shrike", %node);

   %data.onPilotSeated(%obj, %player);
}

function ScoutFlyer::playerDismounted(%data, %obj, %player)
{
    Parent::playerDismounted(%data, %obj, %player);
}

function ScoutVehicle::onAdd(%this, %obj)
{
    Parent::onAdd(%this, %obj);
   
   %obj.schedule(5500, "playThread", $ActivateThread, "activate");
}

function ScoutVehicle::onTrigger(%data, %obj, %trigger, %state)
{
    Parent::onTrigger(%data, %obj, %trigger, %state);
}

function ScoutVehicle::playerMounted(%data, %obj, %player, %node)
{
   // scout vehicle == SUV (single-user vehicle)
   commandToClient(%player.client, 'setHudMode', 'Pilot', "Hoverbike", %node);

   %data.onPilotSeated(%obj, %player);
}

function ScoutVehicle::playerDismounted(%data, %obj, %player)
{
    Parent::playerDismounted(%data, %obj, %player);
}

function HAPCFlyer::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);
   
    %obj.mountImage("AnniDecal7", 1);
//    %obj.mountImage("AnniDecal5", 5);

    %this.registerThruster(%obj, "AnniDecal3", 2, 15, false);
    %this.registerThruster(%obj, "AnniDecal4", 3, 15, false);
    %this.registerThruster(%obj, "AnniDecal8", 4, 15, false);
    %this.registerThruster(%obj, "WolfDecal1", 5, 25, false);
}

function HAPCFlyer::playerMounted(%data, %obj, %player, %node)
{
   if(%node == 0)
   {
      commandToClient(%player.client, 'setHudMode', 'Pilot', "HAPC", %node);
      
      %data.onPilotSeated(%obj, %player);
   }
   else
	   commandToClient(%player.client, 'setHudMode', 'Passenger', "HAPC", %node);

   %data.updatePassengerString(%obj);
   
   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}

function BomberFlyer::onAdd(%this, %obj)
{
    Parent::onAdd(%this, %obj);

//    %obj.mountImage(FirestormPilotTurret, 1);

    %turret = %this.installTurret(%obj, BomberTurret, 10);
    %turret.mountImage(AssaultTurretParam, 0); // AIAimingTurretBarrel
    %turret.mountImage(RetaliatorPilotTurret, 1); // amusing workaround
    
    %this.registerThruster(%obj, "AvengerDecal1", 3, 25, false);
    %this.registerThruster(%obj, "AvengerDecal2", 4, 25, false);
    %this.registerThruster(%obj, "AvengerDecal3", 5, 25, false);
    %this.registerThruster(%obj, "AvengerDecal4", 1, 25, false);
}

function BomberFlyer::onTrigger(%data, %obj, %trigger, %state)
{
    Parent::onTrigger(%data, %obj, %trigger, %state);
}

function BomberTurret::onTrigger(%data, %obj, %trigger, %state)
{
    Parent::onTrigger(%data, %obj, %trigger, %state);
}

function BomberFlyer::playerMounted(%data, %obj, %player, %node)
{
   if(%node == 0)
   {
      // pilot position
//      %player.setPilot(true);
      commandToClient(%player.client, 'setHudMode', 'Pilot', "Bomber", %node);
      %data.onPilotSeated(%obj, %player);
   }
   else if(%node == 1)
   {
      // bombardier position
//      %turret = %obj.getMountNodeObject(10);
//      %player.vehicleTurret = %turret;
//      %player.setTransform("0 0 0 0 0 1 0");
//      %player.lastWeapon = %player.getMountedImage($WeaponSlot);
//      %player.unmountImage($WeaponSlot);
      
//      if(!%player.client.isAIControlled())
//      {
//         %player.setControlObject(%turret);
//         %player.client.setObjectActiveImage(%turret, 2);
//      }
          
//      %turret.bomber = %player;
//      $bWeaponActive = 0;
//      %obj.getMountNodeObject(10).selectedWeapon = 1;
//      commandToClient(%player.client,'SetWeaponryVehicleKeys', true);
       %data.onGunnerSeated(%obj, %player, 10);
	   commandToClient(%player.client, 'setHudMode', 'Pilot', "Bomber", %node);
//      if(%obj.isBombing)
//          commandToClient(%player.client, 'startBomberSight');
//      %player.isBomber = true;
   }
   else
   {
      // tail gunner position
	   commandToClient(%player.client, 'setHudMode', 'Passenger', "Bomber", %node);
   }

   %data.updatePassengerString(%obj);

   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}

function BomberFlyer::deleteAllMounted(%data, %obj)
{
   if(isObject(%obj.beacon))
      %obj.beacon.schedule(50, delete);
}

function AssaultVehicle::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);

   %obj.mountImage(TankPilotTurret, 4);

   %turret = %this.installTurret(%obj, AssaultPlasmaTurret, 10);
   %turret.mountImage(AssaultTurretParam, 0); // AIAimingTurretBarrel

   %obj.schedule(6000, "playThread", $ActivateThread, "activate");
}

function AssaultPlasmaTurret::onTrigger(%data, %obj, %trigger, %state)
{
    Parent::onTrigger(%data, %obj, %trigger, %state);
}

function AssaultVehicle::deleteAllMounted(%data, %obj)
{

}

function AssaultVehicle::playerMounted(%data, %obj, %player, %node)
{
   if(%node == 0)
   {
      // driver position
	   commandToClient(%player.client, 'setHudMode', 'Pilot', "Assault", %node);
    
      %data.onPilotSeated(%obj, %player);
   }
   else
   {
       %data.onGunnerSeated(%obj, %player, 10);
	   commandToClient(%player.client, 'setHudMode', 'Pilot', "Assault", %node);
   }

   %data.updatePassengerString(%obj);

   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}

// Vehicle overrides over
//--------------------------------------------------------------------------------

function serverCmdControlObject(%client, %targetId)
{
   // match started:
   if(!$MatchStarted)
   {
      commandToClient(%client, 'ControlObjectResponse', false, "mission has not started.");
      return;
   }

   // object:
   %obj = getTargetObject(%targetId);
   if(%obj == -1)
   {
      commandToClient(%client, 'ControlObjectResponse', false, "failed to find target object.");
      return;
   }

   // shapebase:
   if(!(%obj.getType() & $TypeMasks::ShapeBaseObjectType))
   {
      commandToClient(%client, 'ControlObjectResponse', false, "object cannot be controlled.");
      return;
   }

   // can control:
   if(!%obj.getDataBlock().canControl || %obj.getMountedImage(0).getName() $= "MissileBarrelLarge") // z0dd - ZOD 4/18/02. Prevent missile barrels from being controlled
   {
      commandToClient(%client, 'ControlObjectResponse', false, "object cannot be controlled.");
      return;
   }

   // check damage:
   if(%obj.getDamageState() !$= "Enabled")
   {
      commandToClient(%client, 'ControlObjectResponse', false, "object is " @ %obj.getDamageState());
      return;
   }

   // powered:
   if(!%obj.isPowered())
   {
      commandToClient(%client, 'ControlObjectResponse', false, "object is not powered.");
      return;
   }

   // controlled already:
   %control = %obj.getControllingClient();
   if(%control)
   {
      if(%control == %client)
         commandToClient(%client, 'ControlObjectResponse', false, "you are already controlling that object.");
      else
         commandToClient(%client, 'ControlObjectResponse', false, "someone is already controlling that object.");
      return;
   }

   // same team?
   if(getTargetSensorGroup(%targetId) != %client.getSensorGroup())
   {
      commandToClient(%client, 'ControlObjectResonse', false, "cannot control enemy objects.");
      return;
   }

   // dead?
   if(%client.player == 0)
   {
      commandToClient(%client, 'ControlObjectResponse', false, "dead people cannot control objects.");
      return;
   }

   //mounted in a vehicle?
   if (%client.player.isMounted() || isObject(%client.walker))
   {
      commandToClient(%client, 'ControlObjectResponse', false, "can't control objects while mounted in a vehicle.");
      return;
   }

   %client.setControlObject(%obj);
   commandToClient(%client, 'ControlObjectResponse', true, getControlObjectType(%obj));

   // --------------------------------------------------------------------------------------------------------
   // z0dd - ZOD, 5/12/02. Change turrets name to controllers name.
   if((%obj.getType() & $TypeMasks::TurretObjectType) && (!%client.isAIControlled()))
   {
      // Set this varible on the client so we can reset turret nameTag when client is done.
      %client.TurretControl = %obj;

      if(%obj.nameTag !$= "") // Get the name tag for storage, this is created in the *.mis file.
      {
         %obj.oldTag = getTaggedString(%obj.nameTag); // Store this nameTag in a var on the turret.
         removeTaggedString(%obj.nameTag); // Reset the turrets nameTag.
         %obj.nameTag = "";
      }
      else // This is either a deployed turret or the *.mis file has no nameTag for it.
      {
         %obj.oldTag = ""; // No nameTag to store on turret (paranoia).

         // Reset the turrets targetNameTag. This may cause problems - ZOD
         //removeTaggedString(%obj.getDataBlock().targetNameTag);
         //%obj.getDataBlock().targetNameTag = "";
      }

      // Reset the turrets target
      freeTarget(%obj.getTarget());

      // Set the turrets target and new nameTag.
      %obj.nameTag = addTaggedString(%client.nameBase @ " controlling ");
      %obj.target = createTarget(%obj, %obj.nameTag, "", "", %obj.getDatablock().targetTypeTag, %obj.team, 0);
      setTargetSensorGroup(%obj.target, %obj.team);
   }
   // --------------------------------------------------------------------------------------------------------
}

function resetControlObject(%client)
{
   if( isObject( %client.comCam ) )
      %client.comCam.delete();

   if(isObject(%client.player) && !%client.player.isDestroyed() && $MatchStarted)
   {
      if(isObject(%client.walker))
         %client.setControlObject(%client.walker);
      else
         %client.setControlObject(%client.player);
   }
   else
      %client.setControlObject(%client.camera);

   // -----------------------------------------------------------------------------------------------------------------------
   // z0dd - ZOD, 5/12/02. Reset the turrets nameTag back to its original.
   if(%client.TurretControl !$= "")
      %turret = %client.TurretControl;
   else
      return;

   if(isObject(%turret))
   {
      // Reset the turrets target and nameTag
      removeTaggedString(%turret.nameTag);
      %turret.nameTag = "";
      freeTarget(%turret.getTarget());

      // Set the turrets target and new nameTag
      if(%turret.oldTag !$= "")
         %turret.nameTag = addTaggedString(%turret.oldTag);
      else
         //%turret.nameTag = addTaggedString(getTaggedString(%turret.getDataBlock().targetNameTag));
         %turret.nameTag = %turret.getDataBlock().targetNameTag; // This should allready be a tagged string

      %turret.target = createTarget(%turret, %turret.nameTag, "", "", %turret.getDatablock().targetTypeTag, %turret.team, 0);
      setTargetSensorGroup(%turret.target, %turret.team);

      // Reset the varible set on the client and turret
      %turret.oldTag = "";
      %client.TurretControl = "";
   }
   // -----------------------------------------------------------------------------------------------------------------------
}

// 1-6 keys
function VehicleData::onTriggerKeys(%data, %veh, %pl, %targetBlock)
{
//    echo("onTriggerKeys vehicle" SPC %data SPC %veh SPC %pl SPC %targetBlock);
}

//----------------------------------------------------------------------------
function ShapeBase::cycleWeapon( %this, %data )
{
   if ( %this.weaponSlotCount == 0 )
      return;
      
   %slot = -1;
   if ( %this.getMountedImage($WeaponSlot) != 0 )
   {
      %curWeapon = %this.getMountedImage($WeaponSlot).item.getName();
      for ( %i = 0; %i < %this.weaponSlotCount; %i++ )
      {
         //error("curWeaponName == " @ %curWeaponName);
         if ( %curWeapon $= %this.weaponSlot[%i] )
         {
            %slot = %i;
            break;
         }
      }
   }

   if ( %data $= "prev" )
   {
      // Previous weapon...
      if ( %slot == 0 || %slot == -1 )
      {
         %i = %this.weaponSlotCount - 1;
         %slot = 0;
      }
      else
         %i = %slot - 1;
   }
   else
   {
      // Next weapon...
      if ( %slot == ( %this.weaponSlotCount - 1 ) || %slot == -1 )
      {
         %i = 0;
         %slot = ( %this.weaponSlotCount - 1 );
      }
      else
         %i = %slot + 1;
   }

   %newSlot = -1;
   while ( %i != %slot )
   {
      if ( %this.weaponSlot[%i] !$= ""
        && %this.hasInventory( %this.weaponSlot[%i] )
        && %this.hasAmmo( %this.weaponSlot[%i] ) )
      {
         // player has this weapon and it has ammo or doesn't need ammo
         %newSlot = %i;
         break;
      }

      if ( %data $= "prev" )
      {
         if ( %i == 0 )
            %i = %this.weaponSlotCount - 1;
         else
            %i--;
      }
      else
      {
         if ( %i == ( %this.weaponSlotCount - 1 ) )
            %i = 0;
         else
            %i++;
      }
   }

   if ( %newSlot != -1 )
      %this.use( %this.weaponSlot[%newSlot] );
}

//----------------------------------------------------------------------------
function ShapeBase::selectWeaponSlot( %this, %data )
{
   if ( %data < 0 || %data > %this.weaponSlotCount
     || %this.weaponSlot[%data] $= "" || %this.weaponSlot[%data] $= "TargetingLaser" )
      return;

   %this.use( %this.weaponSlot[%data] );
}
