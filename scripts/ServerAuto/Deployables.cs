// Deployables Core Library

// ScriptObject RBaseCoreController
// 3 simsets: structure (pieces), shields, internals
// special internal: RemoteBaseGenerator - upon destruction will destroy the entire base
// All internals and shields are self powered, any damage done to them will be redirected
// to the RBaseCoreController's HP pool, any healing done will be reflected by every structure object

function DefaultGame::clearDeployableMaxes(%game)
{
   for(%i = 0; %i <= %game.numTeams; %i++)
   {
      $TeamDeployedCount[%i, TurretIndoorDeployable] = 0;
      $TeamDeployedCount[%i, TurretOutdoorDeployable] = 0;
      $TeamDeployedCount[%i, PulseSensorDeployable] = 0;
      $TeamDeployedCount[%i, MotionSensorDeployable] = 0;
      $TeamDeployedCount[%i, InventoryDeployable] = 0;
      $TeamDeployedCount[%i, DeployedCamera] = 0;
      $TeamDeployedCount[%i, MineDeployed] = 0;
      $TeamDeployedCount[%i, TargetBeacon] = 0;
      $TeamDeployedCount[%i, MarkerBeacon] = 0;

      // Fury stuff
   }
}

// Construction Building Datablock Compatibility Library

$NotDeployableReason::EnemyFlagTooClose         =  20;
$NotDeployableReason::FriendlyFlagTooClose      =  21;
$NotDeployableReason::RequiresBaseNode          =  22;
$NotDeployableReason::OutOfBounds               =  23;
$NotDeployableReason::EnemySpawnTooClose        =  24;
$NotDeployableReason::FriendlySpawnTooFar       =  25;
$NotDeployableReason::BaseNodeTooFar            =  26;
$NotDeployableReason::ExtendedDeployFail        =  27;

// Deployable system overrides
function ShapeBaseImageData::testInvalidDeployConditions(%item, %plyr, %slot)
{
   cancel(%plyr.deployCheckThread);
   %disqualified = $NotDeployableReason::None;  //default
   $MaxDeployDistance = %item.maxDeployDis;
   $MinDeployDistance = %item.minDeployDis;

   %surface = Deployables::searchView(%plyr,
                                      $MaxDeployDistance,
                                      ($TypeMasks::TerrainObjectType |
                                       $TypeMasks::InteriorObjectType));
   if (%surface)
   {
      %surfacePt  = posFromRaycast(%surface);
      %surfaceNrm = normalFromRaycast(%surface);

      // Check that point to see if anything is objstructing it...
      %eyeTrans = %plyr.getEyeTransform();
      %eyePos   = posFromTransform(%eyeTrans);

      %searchResult = containerRayCast(%eyePos, %surfacePt, -1, %plyr);
      if (!%searchResult)
      {
         %item.surface = %surface;
         %item.surfacePt = %surfacePt;
         %item.surfaceNrm = %surfaceNrm;
      }
      else
      {
         if(checkPositions(%surfacePT, posFromRaycast(%searchResult)))
         {
            %item.surface = %surface;
            %item.surfacePt = %surfacePt;
            %item.surfaceNrm = %surfaceNrm;
         }
         else
         {
            // Don't set the item
            %disqualified = $NotDeployableReason::ObjectTooClose;
         }
      }
      if(!getTerrainAngle(%surfaceNrm) && %item.flatMaxDeployDis !$= "")
      {
         $MaxDeployDistance = %item.flatMaxDeployDis;
         $MinDeployDistance = %item.flatMinDeployDis;
      }
   }

   if (%item.testMaxDeployed(%plyr))
   {
      %disqualified = $NotDeployableReason::MaxDeployed;
   }
   else if (%item.testNoSurfaceInRange(%plyr))
   {
      %disqualified = $NotDeployableReason::NoSurfaceFound;
   }
   else if (%item.testNoTerrainFound(%surface))
   {
      %disqualified = $NotDeployableReason::NoTerrainFound;
   }
   else if (%item.testNoInteriorFound())
   {
      %disqualified = $NotDeployableReason::NoInteriorFound;
   }
   else if (%item.testSlopeTooGreat(%surface, %surfaceNrm))
   {
      %disqualified = $NotDeployableReason::SlopeTooGreat;
   }
   else if (%item.testSelfTooClose(%plyr, %surfacePt))
   {
      %disqualified = $NotDeployableReason::SelfTooClose;
   }
   else if (%item.testObjectTooClose(%surfacePt))
   {
      %disqualified = $NotDeployableReason::ObjectTooClose;
   }
   else if (%item.testTurretTooClose(%plyr))
   {
      %disqualified = $NotDeployableReason::TurretTooClose;
   }
   else if (%item.testInventoryTooClose(%plyr))
   {
      %disqualified = $NotDeployableReason::InventoryTooClose;
   }
   else if (%item.testTurretSaturation())
   {
      %disqualified = $NotDeployableReason::TurretSaturation;
   }
   else if(%item.extendedDeployChecks(%plyr))
   {
      %disqualified = $NotDeployableReason::ExtendedDeployFail;
   }
   else if(%item.testEnemyFlagTooClose(%plyr))
   {
      %disqualified = $NotDeployableReason::EnemyFlagTooClose;
   }
   else if(%item.testFriendlyFlagTooClose(%plyr))
   {
      %disqualified = $NotDeployableReason::FriendlyFlagTooClose;
   }
   else if(%item.testEnemySpawnTooClose(%plyr))
   {
      %disqualified = $NotDeployableReason::EnemySpawnTooClose;
   }
   else if(%item.testFriendlySpawnTooFar(%plyr))
   {
      %disqualified = $NotDeployableReason::FriendlySpawnTooFar;
   }
   else if(%item.testOutOfBounds(%plyr))
   {
      %disqualified = $NotDeployableReason::OutOfBounds;
   }
   //---------------------------------------------------------------------------------------
   // z0dd - ZOD, 4/18/02. Addresses the exploit of deploying objects inside other objects.
//   else if (%item.testOrganicTooClose(%plyr))
//   {
//      %disqualified = $NotDeployableReason::OrganicTooClose;
//   }
   //---------------------------------------------------------------------------------------
   else if (%disqualified == $NotDeployableReason::None)
   {
      // Test that there are no objstructing objects that this object
      //  will intersect with
      //
      %rot = %item.getInitialRotation(%plyr);
      if(%item.deployed.className $= "DeployedTurret")
      {
         %xform = %item.deployed.getDeployTransform(%item.surfacePt, %item.surfaceNrm);
      }
      else
      {
         %xform = %surfacePt SPC %rot;
      }

      if (!%item.deployed.checkDeployPos(%xform))
      {
         %disqualified = $NotDeployableReason::ObjectTooClose;
      }
      else if (!%item.testHavePurchase(%xform))
      {
         %disqualified = $NotDeployableReason::SurfaceTooNarrow;
      }
   }

   if (%plyr.getMountedImage($BackpackSlot) == %item)  //player still have the item?
   {
      if (%disqualified)
         activateDeploySensorRed(%plyr);
      else
         activateDeploySensorGrn(%plyr);

      if (%plyr.client.deployPack == true)
         %item.attemptDeploy(%plyr, %slot, %disqualified);
      else
      {
         %plyr.deployCheckThread = %item.schedule(25, "testInvalidDeployConditions", %plyr, %slot); //update checks every 50 milliseconds
      }
   }
   else
       deactivateDeploySensor(%plyr);
}

function Deployables::displayErrorMsg(%item, %plyr, %slot, %error)
{
   deactivateDeploySensor(%plyr);

   %errorSnd = '~wfx/misc/misc.error.wav';
   switch (%error)
   {
      case $NotDeployableReason::None:
         %item.onDeploy(%plyr, %slot);
         messageClient(%plyr.client, 'MsgTeamDeploySuccess', "");
         return;

      case $NotDeployableReason::NoSurfaceFound:
         %msg = '\c2Item must be placed within reach.%1';

      case $NotDeployableReason::MaxDeployed:
         %msg = '\c2Your team\'s control network has reached its capacity for this item.%1';

      case $NotDeployableReason::SlopeTooGreat:
         %msg = '\c2Surface is too steep to place this item on.%1';

      case $NotDeployableReason::SelfTooClose:
         %msg = '\c2You are too close to the surface you are trying to place the item on.%1';

      case $NotDeployableReason::ObjectTooClose:
         %msg = '\c2You cannot place this item so close to another object.%1';

      case $NotDeployableReason::NoTerrainFound:
         %msg = '\c2You must place this on outdoor terrain.%1';

      case $NotDeployableReason::NoInteriorFound:
         %msg = '\c2You must place this on a solid surface.%1';

      case $NotDeployableReason::TurretTooClose:
         %msg = '\c2Interference from a nearby turret prevents placement here.%1';

      case $NotDeployableReason::TurretSaturation:
         %msg = '\c2There are too many turrets nearby.%1';

      case $NotDeployableReason::SurfaceTooNarrow:
         %msg = '\c2There is not adequate surface to clamp to here.%1';

      case $NotDeployableReason::InventoryTooClose:
         %msg = '\c2Interference from a nearby inventory prevents placement here.%1';

      case $NotDeployableReason::EnemyFlagTooClose:
         %msg = '\c2Cannot place here, enemy flag is too close.%1';

      case $NotDeployableReason::FriendlyFlagTooClose:
         %msg = '\c2Cannot place here, friendly flag is too close.%1';
         
      case $NotDeployableReason::EnemySpawnTooClose:
         %msg = '\c2Cannot place here, enemy defense in this area is too strong.%1';
         
      case $NotDeployableReason::FriendlySpawnTooFar:
         %msg = '\c2Cannot place here, you are too far from your base\'s network.%1';
         
      case $NotDeployableReason::OutOfBounds:
         %msg = '\c2No, you can\'t deploy outside the mission area.%1';
         
      case $NotDeployableReason::ExtendedDeployFail:
         %msg = "";
         
      // --------------------------------------------------------------------------------------
      // z0dd - ZOD, 4/18/02. Addresses the exploit of deploying objects inside other objects.
//      case $NotDeployableReason::OrganicTooClose:
//         %msg = '\c2You cannot place this item so close to an organic object.%1';
      // --------------------------------------------------------------------------------------

      default:
         %msg = '\c2Deploy failed.';
   }
   
   if(%error == $NotDeployableReason::ExtendedDeployFail)
        messageClient(%plyr.client, 'MsgDeployFailed', '\c2%2%1', %errorSnd, $NotDeployableReason::ExtendedDeployFailReason);
   else
        messageClient(%plyr.client, 'MsgDeployFailed', %msg, %errorSnd);
}

// Deployables

