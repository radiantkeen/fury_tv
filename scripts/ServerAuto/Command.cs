//-----------------------------------------------------------------------------
// Dynamic Command System
//--------------------------------------

function Command::__construct(%this)
{
    Command.commandCount = 0;
    Command.Version = 1.0;
}

function Command::__destruct(%this)
{
   // Thou shalt not spam
}

if(!isObject(Command))
   System.addClass(Command);
   
//--------------------------------------------------------------------------
// String Table
//--------------------------------------

$Command::Player = 0;
$Command::Admin = 1;
$Command::SuperAdmin = 2;

//--------------------------------------------------------------------------
// Helper Functions
//--------------------------------------

function nameToClient(%name)
{
   %count = ClientGroup.getCount();

   for(%i = 0; %i < %count; %i++)
   {
      %obj = ClientGroup.getObject(%i);

      if(%obj.nameBase $= %name)
          return %obj;
   }

   for(%i = 0; %i < %count; %i++)
   {
      if(strstr(strlwr(%obj.nameBase), strlwr(%name)) != -1)
         return %obj;
   }

   return 0;
}

function GUIDToClientID(%id)
{
   %count = ClientGroup.getCount();

   for(%i = 0; %i < %count; %i++)
   {
      %obj = ClientGroup.getObject(%i);
      
      if(%obj.isAIControlled())
        continue;

      if(%obj.guid == %id)
         return %obj;
   }

   return 0;
}

function nameToGUID(%name)
{
   %client = nameToClient(%name);

   return %client ? %client.guid : 0;
}

function nameToIP(%name)
{
   %client = nameToClient(%name);

   return %client ? %client.getAddress() : "127.0.0.1";
}

function getAdminLevel(%client)
{
     if(%client.isAIControlled())
          return $Command::Player;

     if(%client.isAdmin)
     {
          if(%client.isSuperAdmin)
               return $Command::SuperAdmin;
          else
               return $Command::Admin;
     }
     else
          return $Command::Player;
}

function canTorture(%challenger, %target)
{
     return getAdminLevel(%challenger) > getAdminLevel(%target);
}

//--------------------------------------------------------------------------
// Server chat command overload
//--------------------------------------

if($Host::MaxMessageLen < 1) // check
   $Host::MaxMessageLen = 255;

function serverCmdTeamMessageSent(%client, %text)
{
    %name = %client.isAIControlled() ? %client.namebase : %client.rankName;
    
   // Special case handling - won't be able to tell if /me is used in team-only or global context if used in a plugin
   if(getSubStr(%text, 0, 4) $= "/me ")
   {
      chatMessageTeam(%client, %client.team, '\c1* %1 %2', %name, getSubStr(%text, 4, $Host::MaxMessageLen));
      return;
   }

   if(getSubStr(%text, 0, 1) $= "/" && getSubStr(%text, 1, 1) !$= "")
   {
      Command.evalCommand(%client, getSubStr(%text, 1, $Host::MaxMessageLen));

      return;
   }

   if(strlen(%text) >= $Host::MaxMessageLen)
      %text = getSubStr(%text, 0, $Host::MaxMessageLen);

   chatMessageTeam(%client, %client.team, '\c3%1: %2', %name, %text);
}

function serverCmdMessageSent(%client, %text)
{
   if(%client.vehSetName !$= "")
   {
        %client.setVLoadoutName(%client.vehSetName, getSubStr(%text, 0, 32));
        %client.vehSetName = "";

        %client.screenReference.processGameLink(%client, %client.screenNextWindow, %client.screenNextArg);
        BottomPrint(%client, "Loadout name set.", 5, 1);
        return;
   }

   %name = %client.isAIControlled() ? %client.namebase : %client.rankName;
    
   if(getSubStr(%text, 0, 4) $= "/me ")  // Special case handling - won't be able to tell if /me is used in team-only or global context if used in a plugin
   {
      chatMessageAll(%client, '\c1* %1 %2', %name, getSubStr(%text, 4, $Host::MaxMessageLen));
      return;
   }
   else if(getSubStr(%text, 0, 1) $= "/" && getSubStr(%text, 1, 1) !$= "")
   {
      Command.evalCommand(%client, getSubStr(%text, 1, $Host::MaxMessageLen));

      return;
   }

   if(strlen(%text) >= $Host::MaxMessageLen)
      %text = getSubStr(%text, 0, $Host::MaxMessageLen);

   chatMessageAll(%client, '\c4%1: %2', %name, %text);
}

function cannedChatMessageAll( %sender, %msgString, %name, %string, %keys )
{
   if ( ( %msgString $= "" ) || spamAlert( %sender ) )
      return;

   %name = %sender.isAIControlled() ? %name : %sender.rankName;

   %count = ClientGroup.getCount();
   for ( %i = 0; %i < %count; %i++ )
      cannedChatMessageClient( ClientGroup.getObject(%i), %sender, %msgString, %name, %string, %keys );
}

function cannedChatMessageTeam( %sender, %team, %msgString, %name, %string, %keys )
{
   if ( ( %msgString $= "" ) || spamAlert( %sender ) )
      return;

   %name = %sender.isAIControlled() ? %name : %sender.rankName;
   
   %count = ClientGroup.getCount();
   for ( %i = 0; %i < %count; %i++ )
   {
      %obj = ClientGroup.getObject( %i );
      if ( %obj.team == %sender.team )
         cannedChatMessageClient( %obj, %sender, %msgString, %name, %string, %keys );
   }
}

//--------------------------------------------------------------------------
// Commands

// keen: considering using get/setField() to parse tabs and switching to argc/argv for Command commands
function Command::evalCommand(%this, %cl, %msg)
{
    %obj = %cl.player;
    %cmd = firstWord(%msg); // what IS the command?
    %len = (%tmplen = (strlen(%cmd) + 1)) < 0 ? 0 : %tmplen; // how LONG is the command?

    if(Command.isCommand(%cmd))
    {
        if(Command.authCommand(%cl, %cmd))
        {
            // edit by SoLo: allow for eval() to process "'s
            %val = strReplace(getSubStr(%msg, %len, 128), "\"", "\\\"");
            call("Command."@%cmd, %cl, %cmd);
            eval("Command."@%cmd@"("@%cl@", \""@%val@"\");");
            return;
        }
        else
            Command.logProtectedAttempt(%cl, %cmd, "Insufficient access level to use this command.");
    }
    else
        messageClient(%cl, 'MsgCommandError', '\c5Command[Error]: Command does not exist.');
}

function Command::LogProtectedAttempt(%this, %cl, %cmd, %errorcode)
{
    if(%errorcode $= "")
        %errorcode = "Attempted protected function without access.";

    messageClient(%cl, 'MsgCommandInvalidCommand', '\c5Command[Error]: %1', %errorcode);
    return;
}

function Command::AuthCommand(%this, %cl, %command)
{
    if(getAdminLevel(%cl) >= Command.getCommandLevel(%command))
        return true;
    else
        return false;
}

function Command::addCommand(%this, %level, %cmd, %desc)
{
    if(%cmd $= "")
        return;

    if(%desc $= "")
        %desc = "No description.";

    if(%level < 0)
        %level = 1;

    %count = Command.commandCount;

    for(%i = 0; %i < %count; %i++)
        if(%cmd $= Command.commandList[%i])
            return;

    Command.commandList[%count] = %cmd;
    Command.commandLevel[%count] = %level;
    Command.commandDescription[%count] = %desc;

    Command.commandCount++;

    return 0;
}

function Command::listLevelCommands(%this, %cl)
{
    %cmdlist = "";
    %cmdcount = 0;
    %adminLevel = getAdminLevel(%cl);

    for(%i = 0; %i < Command.commandCount; %i++)
    {
        if(%adminLevel >= Command.commandLevel[%i])
        {
             %cmdlist = %cmdlist@" "@Command.commandList[%i];
             %cmdcount++;
        }
    }

    %total = %cmdcount@" "@%cmdlist;

    return %total;
}

function Command::cmdToID(%this, %cmd)
{
    for(%i = 0; %i < Command.commandCount; %i++)
        if(%cmd $= Command.commandList[%i])
            return %i;

    return 0;
}

function Command::isCommand(%this, %cmd)
{
    if(Command.cmdToID(%cmd))
        return true;
    else
        return false;
}

function Command::getCmdDesc(%this, %cmd)
{
    return Command.commandDescription[Command.cmdToID(%cmd)];
}

function Command::getCommandLevel(%this, %cmd)
{
    return Command.commandLevel[Command.cmdToID(%cmd)];
}

//-----------------------------------------------------------------------------
// Server command callbacks for admin system

function serverCmdCommandEval(%cl, %msg)
{
    Command.evalCommand(%cl, %msg);
}

function serverCmdListLevelCommands(%cl)
{
    commandToClient(%cl, 'CommandListAdminCommands', Command.listLevelCommands(%cl));
}

// Load Command modules
//execDir("ChatCommands");
//execDirBase("Modules");
