$ignorethisstring = true;

function reloadpart(%part)
{
    reload("scripts/VehicleParts/"@%part@".cs");
}

function reloadvehicle(%veh)
{
    reload("scripts/vehicles/"@%veh@".cs");
}

function showVehicles()
{
    for(%i = 0; %i < MissionCleanup.getCount(); %i++)
    {
        %obj = MissionCleanup.getObject(%i);
        
        if(%obj.isVehicle())
            echo("Vehicle: " SPC %obj.position SPC %obj.getDamageLevel());
    }
}

function showTurrets()
{
    for(%i = 0; %i < MissionCleanup.getCount(); %i++)
    {
        %obj = MissionCleanup.getObject(%i);

        if(%obj.isTurret())
            echo("Turret:" SPC %obj.position SPC %obj.getDatablock().getName());
    }
}

function showProjectiles()
{
    for(%i = 0; %i < MissionCleanup.getCount(); %i++)
    {
        %obj = MissionCleanup.getObject(%i);

        if(%obj.getType() & $TypeMasks::ProjectileObjectType)
            echo("Projectile:" SPC %obj.getId() SPC %obj.getDatablock().getName());
    }
}

function showMCObjects()
{
    for(%i = 0; %i < MissionCleanup.getCount(); %i++)
    {
        %obj = MissionCleanup.getObject(%i);

        echo(%obj.getId() TAB %obj.getDatablock().getName() TAB %obj.getType());
    }
}

function showVehicleLists()
{
    echo("VehicleID,VehicleName,VehicleMask,Mass,MaxDamage,RechargeRate,ShapeName,Slot1Name,Slot1Mask,Slot1Size,Slot2Name,Slot2Mask,Slot2Size,Slot3Name,Slot3Mask,Slot3Size,Slot4Name,Slot4Mask,Slot4Size,Slot5Name,Slot5Mask,Slot5Size,Slot6Name,Slot6Mask,Slot6Size");

    for(%i = 0; %i < $VehicleCount; %i++)
    {
        %block = $VehicleListData[%i, "block"];
        %len = strlen(%block.shapeFile);
        %blockname = getSubStr(%block.shapeFile, 0, %len - 4);

        %slotdata = "";

        for(%h = 0; %h < 6; %h++) // $VehicleListData[%i, "hardpoints"]
            %slotdata = %slotdata@","@$VehicleHardpoints[%i, %h, "type"]@","@$VehicleHardpoints[%i, %h, "size"]@","@$VehicleHardpoints[%i, %h, "name"];
            
        echo(%i@","@$VehicleListData[%i, "name"]@","@$VehicleListData[%i, "mask"]@","@mFloor(%block.mass * 100)@","@mFloor(%block.maxDamage * 100)@","@(%block.rechargeRate * $g_TickTime)@","@%blockname@%slotdata);
    }
}

function showWeaponsLists()
{
    %numWeapons = VehiclePart.vehiclePartCount[$VehiclePartType::Weapon];

    echo("WeaponID,WeaponName,WearableMask,MountSize");
    
    for(%i = 0; %i < %numWeapons; %i++)
    {
        %vwep = VehiclePart.vehicleParts[$VehiclePartType::Weapon, %i];

        echo(%i@","@%vwep.name@","@%vwep.wearableMask@","@%vwep.mountSize);
    }

    echo("\nModuleID,ModuleName,ModuleType,WearableMask,DeltaMass,DeltaArmor,DeltaShield,DeltaRegen");
    %numModules = VehiclePart.vehiclePartCount[$VehiclePartType::Module];

    for(%i = 0; %i < %numModules; %i++)
    {
        %vmod = VehiclePart.vehicleParts[$VehiclePartType::Module, %i];

        echo(%i@","@%vmod.name@","@"Module"@","@%vmod.wearableMask@","@%vmod.deltaMass@","@%vmod.deltaArmor@","@%vmod.deltaShield@","@%vmod.deltaRegen);
    }

    %numShields = VehiclePart.vehiclePartCount[$VehiclePartType::Shield];

    for(%i = 0; %i < %numShields; %i++)
    {
        %vshield = VehiclePart.vehicleParts[$VehiclePartType::Shield, %i];

        echo(%i@","@%vshield.name@","@"Shield"@","@%vshield.wearableMask@","@%vshield.deltaMass@","@%vshield.deltaArmor@","@%vshield.deltaShield@","@%vshield.deltaRegen);
    }
}

function writeDBs()
{
    %file = new fileObject();
    %file.openForWrite("dbdump.csv");
    
    %c = DataBlockGroup.getCount();
    
    for(%i = 0; %i < %c; %i++)
    {
        %db = DataBlockGroup.getObject(%i);

        %file.writeLine(%db.getName()@","@%db.getClassName());
    }
    
    %file.close();
}

function writeProjectileKicks()
{
    %file = new fileObject();
    %file.openForWrite("projectilekicks.csv");
    %file.writeLine("name,kick");
        
    %c = DataBlockGroup.getCount();

    for(%i = 0; %i < %c; %i++)
    {
        %db = DataBlockGroup.getObject(%i);

        if(%db.kickBackStrength > 0)
            %file.writeLine(%db.getName()@","@%db.kickBackStrength);
    }

    %file.close();
}

function listToText(%type, %value)
{
    if(%type $= "vehicleid")
    {
    }
    else if(%type $= "")
    {
    
    }
}

$HTMLTableStyle = "border: 2px solid black; border-collapse: collapse;";
$HTMLTRStyle = "";
$HTMLTDStyle = "border 1px solid black;";

function htmltag(%tag, %value, %style)
{
    if(%style !$= "")
        return "<"@%tag@" style=\""@%style@"\">"@%value@"</"@%tag@">";
   else
        return "<"@%tag@">"@%value@"</"@%tag@">";
}

function htmlexportvehicles()
{
    %file = new fileObject();
    %file.openForWrite("HTMLTable.txt");

    %c = DataBlockGroup.getCount();

    for(%i = 0; %i < %c; %i++)
    {
        %db = DataBlockGroup.getObject(%i);

        %file.writeLine(%db.getName()@","@%db.getClassName());
    }

    %file.close();
}

function testST(%veh)
{
    %veh.setTarget(%veh.getMountNodeObject(0).getTarget());
}

function unpatchtemp()
{
    //memPatch("6FF373","506A00FF15FCC77D00B8010000");
    memPatch("439DE7","85F67412834E1801E88CFFFFFF89F18B196A01FF53048D65F85E5B5DC3");
}
