// Sound, Projectile, and Explosion blocks

datablock ParticleData( PlasmaVCannonCrescentParticle )
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/crescent3";

   colors[0]     = "0.8 0.43 0.0 1.0";
   colors[1]     = "0.8 0.43 0.0 0.5";
   colors[2]     = "0.8 0.43 0.0 0.0";
   sizes[0]      = 5.0;
   sizes[1]      = 6.0;
   sizes[2]      = 8.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( PlasmaVCannonCrescentEmitter )
{
   ejectionPeriodMS = 20;
   periodVarianceMS = 0;
   ejectionVelocity = 30;
   velocityVariance = 7.5;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 300;
   particles = "PlasmaVCannonCrescentParticle";
};

datablock ParticleData(PlasmaVCannonExplosionParticle)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.2;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 1300;
   lifetimeVarianceMS   = 300;
   textureName          = "particleTest";
   colors[0]     = "0.8 0.43 0.0 1.0";
   colors[1]     = "0.8 0.43 0.0 0.0";
   sizes[0]      = 2;
   sizes[1]      = 4;
};

datablock ParticleEmitterData(PlasmaVCannonExplosionEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 12;
   velocityVariance = 1.75;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "PlasmaVCannonExplosionParticle";
};

datablock ExplosionData(PlasmaVCannonSubExplosion1)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;

   delayMS = 0;

   offset = 0.0;

   playSpeed = 0.7;

   sizes[0] = "1.5 1.5 1.5";
   sizes[1] = "1.5 1.5 1.5";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(PlasmaVCannonSubExplosion2)
{
   explosionShape = "effect_plasma_explosion.dts";
   faceViewer           = true;

   delayMS = 100;

   offset = 3.5;

   playSpeed = 1.0;

   sizes[0] = "3.0 3.0 3.0";
   sizes[1] = "3.0 3.0 3.0";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(PlasmaVCannonSubExplosion3)
{
   explosionShape = "effect_plasma_explosion.dts";
   faceViewer           = true;

   delayMS = 50;

   offset = 3.0;

   playSpeed = 1.5;


   sizes[0] = "3.0 3.0 3.0";
   sizes[1] = "4.0 4.0 4.0";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(PlasmaVCannonBoltExplosion)
{
   soundProfile   = PlasmaVCannonExpSound;
   particleEmitter = PlasmaVCannonExplosionEmitter;
   particleDensity = 250;
   particleRadius = 1.25;
   faceViewer = true;

   emitter[0] = PlasmaVCannonCrescentEmitter;

   subExplosion[0] = PlasmaVCannonSubExplosion1;
   subExplosion[1] = PlasmaVCannonSubExplosion2;
   subExplosion[2] = PlasmaVCannonSubExplosion3;

   shakeCamera = true;
   camShakeFreq = "10.0 9.0 9.0";
   camShakeAmp = "10.0 10.0 10.0";
   camShakeDuration = 0.25;
   camShakeRadius = 15.0;
};

datablock LinearProjectileData(VLPlasmaBolt)
{
   scale = "1.0 1.0 1.0";
   projectileShapeName = "Fireball_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 0;
   directDamageType    = $DamageType::PlasmaCannon;
   hasDamageRadius     = true;
   indirectDamage      = 3.9;
   damageRadius        = 1;
   radiusDamageType    = $DamageType::PlasmaCannon;
   kickBackStrength    = 1500;

   flags               = $Projectile::PlaysHitSound | $Projectile::Plasma;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "PlasmaVCannonBoltExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

//   baseEmitter         = MissileSmokeEmitter;
//   delayEmitter        = WildcatJetEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 350;
   wetVelocity       = 350;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1000;

   sound = "SentryTurretProjectileSound";

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 7.0;
   lightColor  = "0.0 0.4 1.0";
};

// Primary Definition
datablock ShapeBaseImageData(LPlasmaSSH2)
{
   shapeFile = "turret_mortar_large.dts";
   mountPoint = 0;
   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 2, "rotation"];

   projectile = VLPlasmaBolt;
   projectileType = LinearProjectile;
   ammo = "VLPlasmaAmmo";
   updatePilotAmmo = true;
   sharedResourcePool = true;

   usesEnergy = false;
   useMountEnergy = false;
   fireEnergy = 75.0;
   minEnergy = 75.0;
   useCapacitor = true;

   fireTimeout = 1000;

   // State transitions
   stateName[0]                        = "Activate";
   stateTransitionOnNotLoaded[0]       = "Dead";
   stateTransitionOnLoaded[0]          = "ActivateReady";

   stateName[1]                        = "ActivateReady";
   stateSequence[1]                    = "ambient";
//   stateSound[1]                       = AssaultTurretActivateSound;
   stateTimeoutValue[1]                = 1.0;
   stateTransitionOnTimeout[1]         = "Ready";
   stateTransitionOnNotLoaded[1]       = "Deactivate";

   stateName[2]                        = "Ready";
   stateTransitionOnNotLoaded[2]       = "Deactivate";
   stateTransitionOnNoAmmo[2]          = "NoAmmo";
   stateTransitionOnTriggerDown[2]     = "Fire";

   stateName[3]                        = "Fire";
   stateSequence[3]                    = "Fire";
   stateTransitionOnTimeout[3]         = "Reload";
   stateTimeoutValue[3]                = 1.5;
   stateFire[3]                        = true;
   stateRecoil[3]                      = LightRecoil;
   stateAllowImageChange[3]            = false;
   stateSound[3]                       = PlasmaVCannonFireSound;
   stateScript[3]                      = "onFire";

   stateName[4]                        = "Reload";
   stateSequence[4]                    = "Reload";
   stateTimeoutValue[4]                = 0.5;
   stateAllowImageChange[4]            = false;
   stateSound[4]                       = PlasmaReloadSound;
   stateTransitionOnTimeout[4]         = "Ready";
   //stateTransitionOnNoAmmo[4]          = "NoAmmo";
   stateWaitForTimeout[4]              = true;

   stateName[5]                        = "Deactivate";
   stateDirection[5]                   = false;
   stateSequence[5]                    = "Activate";
   stateTimeoutValue[5]                = 1.0;
   stateTransitionOnLoaded[5]          = "ActivateReady";
   stateTransitionOnTimeout[5]         = "Dead";

   stateName[6]                        = "Dead";
   stateTransitionOnLoaded[6]          = "ActivateReady";
   stateTransitionOnTriggerDown[6]     = "DryFire";

   stateName[7]                        = "DryFire";
   stateSound[7]                       = AssaultMortarDryFireSound;
   stateTimeoutValue[7]                = 1.0;
   stateTransitionOnTimeout[7]         = "NoAmmo";

   stateName[8]                        = "NoAmmo";
   stateSequence[8]                    = "NoAmmo";
   stateTransitionOnAmmo[8]            = "Reload";
   stateTransitionOnTriggerDown[8]     = "DryFire";
};

// Inherited Defs
datablock ShapeBaseImageData(LPlasmaSTA2) : LPlasmaSSH2
{
   pilotHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::Starhawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 2, "rotation"];
};

datablock ShapeBaseImageData(LPlasmaAMI2) : LPlasmaSSH2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Ameri, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Ameri, 2, "rotation"];
};

datablock ShapeBaseImageData(LPlasmaGUI2) : LPlasmaSSH2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Guillotine, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Guillotine, 2, "rotation"];
};

datablock ShapeBaseImageData(LPlasmaTUT4) : LPlasmaSSH2
{
   pilotHeadTracking = true;
   
   mountPoint = 1;

   offset = "0 -0.5 0";
   rotation = "0 1 0 180";
};

datablock ShapeBaseImageData(LPlasmaFIB2) : LPlasmaSSH2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 2, "rotation"];
};

datablock ShapeBaseImageData(LPlasmaFIB4) : LPlasmaSSH2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 4, "rotation"];
};

datablock ShapeBaseImageData(LPlasmaFIB5) : LPlasmaSSH2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 5, "rotation"];
};

datablock ShapeBaseImageData(LPlasmaTUS2) : LPlasmaSSH2
{
   offset = "0 0.25 0";
   rotation = "0 1 0 180";
};

datablock ShapeBaseImageData(LPlasmaTOM2) : LPlasmaSSH2
{
   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 2, "rotation"];
};

datablock ShapeBaseImageData(LPlasmaTOM3) : LPlasmaSSH2
{
   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 3, "rotation"];
};

datablock ShapeBaseImageData(LPlasmaRET2) : LPlasmaSSH2
{
   pilotHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::Retaliator, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Retaliator, 2, "rotation"];
};

datablock ShapeBaseImageData(LPlasmaSTR2) : LPlasmaSSH2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Stryker, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Stryker, 2, "rotation"];
};

datablock ShapeBaseImageData(LPlasmaTPD2) : LPlasmaSSH2
{
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 2, "rotation"];
};

datablock ShapeBaseImageData(LPlasmaTPD3) : LPlasmaSSH2
{
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 3, "rotation"];
};

function LPlasma::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = true;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 100;
    %this.ammo = "VLPlasmaAmmo";
    %this.ammoUse = 1;
    %this.fireTimeout = 1000;
    
    // Reticle data
    %this.reticleTex = "gui/RET_Plasma";
    %this.showReticleFrame = true;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LPlasma", "Plasma Cannon", "Heavy hitting explosive ball of fire", $VehicleList::Firebird | $VehicleList::Tomahawk | $VehicleList::Ameri |  $VehicleList::Guillotine | $VehicleList::Leviathan | $VehicleList::SuperShrike | $VehicleList::Starhawk | $VehicleList::Firebird | $VehicleList::Retaliator | $VehicleList::HoverJeep | $VehicleList::Stryker | $VehicleList::Wolfhound | $VehicleList::Annihilator | $VehicleList::Goodship | $VehicleList::Scorpion, $VHardpointSize::Cannon, $VHardpointType::Ballistic);
