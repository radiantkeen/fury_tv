function CMissileRack::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Push;
    %this.cooldownTime = 1000;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = 0;
    %this.deltaRegen = 0;
}

function CMissileRack::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = true;
    %vehicle.missileCharges = 1;
}

function CMissileRack::reArm(%this, %data, %vehicle, %player, %slot)
{
    %vehicle.missileCharges = 1;
}

VehiclePart.registerVehiclePart($VehiclePartType::Module, "CMissileRack", "Missile Storage Bay", "[Passive] Restocks missiles once depleted, one-time use.", $VehicleList::HasMissiles, $VHardpointSize::Internal, $VHardpointType::Omni);
