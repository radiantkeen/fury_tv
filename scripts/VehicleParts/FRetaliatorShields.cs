// Retaliator Shield Defs
$SD::Retaliator::maxStrength = 2450;
$SD::Retaliator::recDelay = 20;
$SD::Retaliator::resTime = 40;
$SD::Retaliator::recRate = 360;

// Definition Objects
function FRetaliatorStandard::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";

    // Shield settings
    %this.maxStrength = $SD::Retaliator::maxStrength;
    %this.recDelay = $SD::Retaliator::recDelay;
    %this.resTime = $SD::Retaliator::resTime;
    %this.recRate = $SD::Retaliator::recRate;
    %this.powerDrainPct = $ShieldDefs::StandardPowerDrain;
    %this.bleedthrough = $ShieldDefs::StandardBleedthrough;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = %this.maxStrength;
    %this.deltaRegen = ($VehicleListData[$VehicleID::Retaliator, "block"].rechargeRate * $g_TickTime) * -(%this.powerDrainPct / 100);
}

function FRetaliatorStandard::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, %this.maxStrength, mFloor(%this.recDelay * 1000), mFloor(%this.resTime * 1000), %this.recRate / $g_TickTime, %this.powerDrainPct / 100, %this.bleedthrough / 100, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "FRetaliatorStandard", "Standard Shields", %this.maxStrength @ " HP, Recharge delay: " @ %this.recDelay @ " sec, Restart delay: " @ %this.resTime @ " sec, Bleedthrough: " @ %this.bleedthrough @ "%, Power Drain: " @ %this.powerDrainPct @ "%", $VehicleList::Retaliator, $VHardpointSize::Internal, $VHardpointType::Anything);

function FRetaliatorLight::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";

    // Shield settings
    %this.maxStrength = mCeil($SD::Retaliator::maxStrength * 0.5);
    %this.recDelay = $SD::Retaliator::recDelay * 0.75;
    %this.resTime = $SD::Retaliator::resTime * 0.5;
    %this.recRate = $SD::Retaliator::recRate * 0.5;
    %this.powerDrainPct = $ShieldDefs::LightPowerDrain;
    %this.bleedthrough = $ShieldDefs::LightBleedthrough;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = %this.maxStrength;
    %this.deltaRegen = ($VehicleListData[$VehicleID::Retaliator, "block"].rechargeRate * $g_TickTime) * -(%this.powerDrainPct / 100);
}

function FRetaliatorLight::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, %this.maxStrength, mFloor(%this.recDelay * 1000), mFloor(%this.resTime * 1000), %this.recRate / $g_TickTime, %this.powerDrainPct / 100, %this.bleedthrough / 100, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "FRetaliatorLight", "Light Shields", %this.maxStrength @ " HP, Recharge delay: " @ %this.recDelay @ " sec, Restart delay: " @ %this.resTime @ " sec, Bleedthrough: " @ %this.bleedthrough @ "%, Power Drain: " @ %this.powerDrainPct @ "%", $VehicleList::Retaliator, $VHardpointSize::Internal, $VHardpointType::Anything);

function FRetaliatorRegenerative::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";

    // Shield settings
    %this.maxStrength = mCeil($SD::Retaliator::maxStrength * 0.333);
    %this.recDelay = $SD::Retaliator::recDelay * 0.5;
    %this.resTime = $SD::Retaliator::resTime * 0.333;
    %this.recRate = $SD::Retaliator::recRate * 0.75;
    %this.powerDrainPct = $ShieldDefs::RegenerativePowerDrain;
    %this.bleedthrough = $ShieldDefs::RegenerativeBleedthrough;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = %this.maxStrength;
    %this.deltaRegen = ($VehicleListData[$VehicleID::Retaliator, "block"].rechargeRate * $g_TickTime) * -(%this.powerDrainPct / 100);
}

function FRetaliatorRegenerative::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, %this.maxStrength, mFloor(%this.recDelay * 1000), mFloor(%this.resTime * 1000), %this.recRate / $g_TickTime, %this.powerDrainPct / 100, %this.bleedthrough / 100, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "FRetaliatorRegenerative", "Regenerative Shields", %this.maxStrength @ " HP, Recharge delay: " @ %this.recDelay @ " sec, Restart delay: " @ %this.resTime @ " sec, Bleedthrough: " @ %this.bleedthrough @ "%, Power Drain: " @ %this.powerDrainPct @ "%", $VehicleList::Retaliator, $VHardpointSize::Internal, $VHardpointType::Anything);

function FRetaliatorHardened::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";

    // Shield settings
    %this.maxStrength = $SD::Retaliator::maxStrength;
    %this.recDelay = $SD::Retaliator::recDelay * 1.5;
    %this.resTime = $SD::Retaliator::resTime * 1.25;
    %this.recRate = $SD::Retaliator::recRate;
    %this.powerDrainPct = $ShieldDefs::HardenedPowerDrain;
    %this.bleedthrough = $ShieldDefs::HardenedBleedthrough;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = %this.maxStrength;
    %this.deltaRegen = ($VehicleListData[$VehicleID::Retaliator, "block"].rechargeRate * $g_TickTime) * -(%this.powerDrainPct / 100);
}

function FRetaliatorHardened::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, %this.maxStrength, mFloor(%this.recDelay * 1000), mFloor(%this.resTime * 1000), %this.recRate / $g_TickTime, %this.powerDrainPct / 100, %this.bleedthrough / 100, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "FRetaliatorHardened", "Hardened Shields", %this.maxStrength @ " HP, Recharge delay: " @ %this.recDelay @ " sec, Restart delay: " @ %this.resTime @ " sec, Bleedthrough: " @ %this.bleedthrough @ "%, Power Drain: " @ %this.powerDrainPct @ "%", $VehicleList::Retaliator, $VHardpointSize::Internal, $VHardpointType::Anything);

function FRetaliatorCovariant::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";

    // Shield settings
    %this.maxStrength = mCeil($SD::Retaliator::maxStrength * 1.5);
    %this.recDelay = $SD::Retaliator::recDelay;
    %this.resTime = $SD::Retaliator::resTime;
    %this.recRate = $SD::Retaliator::recRate;
    %this.powerDrainPct = $ShieldDefs::CovariantPowerDrain;
    %this.bleedthrough = $ShieldDefs::CovariantBleedthrough;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = %this.maxStrength;
    %this.deltaRegen = ($VehicleListData[$VehicleID::Retaliator, "block"].rechargeRate * $g_TickTime) * -(%this.powerDrainPct / 100);
}

function FRetaliatorCovariant::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, %this.maxStrength, mFloor(%this.recDelay * 1000), mFloor(%this.resTime * 1000), %this.recRate / $g_TickTime, %this.powerDrainPct / 100, %this.bleedthrough / 100, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "FRetaliatorCovariant", "Covariant Shields", %this.maxStrength @ " HP, Recharge delay: " @ %this.recDelay @ " sec, Restart delay: " @ %this.resTime @ " sec, Bleedthrough: " @ %this.bleedthrough @ "%, Power Drain: " @ %this.powerDrainPct @ "%", $VehicleList::Retaliator, $VHardpointSize::Internal, $VHardpointType::Anything);
