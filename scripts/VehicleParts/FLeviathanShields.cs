// Leviathan Shield Defs
$SD::Leviathan::maxStrength = 7500;
$SD::Leviathan::recDelay = 60;
$SD::Leviathan::resTime = 60;
$SD::Leviathan::recRate = 500;

// Definition Objects
function FLeviathanStandard::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";

    // Shield settings
    %this.maxStrength = $SD::Leviathan::maxStrength;
    %this.recDelay = $SD::Leviathan::recDelay;
    %this.resTime = $SD::Leviathan::resTime;
    %this.recRate = $SD::Leviathan::recRate;
    %this.powerDrainPct = $ShieldDefs::StandardPowerDrain;
    %this.bleedthrough = $ShieldDefs::StandardBleedthrough;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = %this.maxStrength;
    %this.deltaRegen = ($VehicleListData[$VehicleID::Leviathan, "block"].rechargeRate * $g_TickTime) * -(%this.powerDrainPct / 100);
}

function FLeviathanStandard::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, %this.maxStrength, mFloor(%this.recDelay * 1000), mFloor(%this.resTime * 1000), %this.recRate / $g_TickTime, %this.powerDrainPct / 100, %this.bleedthrough / 100, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "FLeviathanStandard", "Standard Shields", %this.maxStrength @ " HP, Recharge delay: " @ %this.recDelay @ " sec, Restart delay: " @ %this.resTime @ " sec, Bleedthrough: " @ %this.bleedthrough @ "%, Power Drain: " @ %this.powerDrainPct @ "%", $VehicleList::Leviathan, $VHardpointSize::Internal, $VHardpointType::Anything);

function FLeviathanLight::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";

    // Shield settings
    %this.maxStrength = mCeil($SD::Leviathan::maxStrength * 0.5);
    %this.recDelay = $SD::Leviathan::recDelay * 0.75;
    %this.resTime = $SD::Leviathan::resTime * 0.5;
    %this.recRate = $SD::Leviathan::recRate * 0.5;
    %this.powerDrainPct = $ShieldDefs::LightPowerDrain;
    %this.bleedthrough = $ShieldDefs::LightBleedthrough;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = %this.maxStrength;
    %this.deltaRegen = ($VehicleListData[$VehicleID::Leviathan, "block"].rechargeRate * $g_TickTime) * -(%this.powerDrainPct / 100);
}

function FLeviathanLight::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, %this.maxStrength, mFloor(%this.recDelay * 1000), mFloor(%this.resTime * 1000), %this.recRate / $g_TickTime, %this.powerDrainPct / 100, %this.bleedthrough / 100, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "FLeviathanLight", "Light Shields", %this.maxStrength @ " HP, Recharge delay: " @ %this.recDelay @ " sec, Restart delay: " @ %this.resTime @ " sec, Bleedthrough: " @ %this.bleedthrough @ "%, Power Drain: " @ %this.powerDrainPct @ "%", $VehicleList::Leviathan, $VHardpointSize::Internal, $VHardpointType::Anything);

function FLeviathanRegenerative::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";

    // Shield settings
    %this.maxStrength = mCeil($SD::Leviathan::maxStrength * 0.333);
    %this.recDelay = $SD::Leviathan::recDelay * 0.5;
    %this.resTime = $SD::Leviathan::resTime * 0.333;
    %this.recRate = $SD::Leviathan::recRate * 0.75;
    %this.powerDrainPct = $ShieldDefs::RegenerativePowerDrain;
    %this.bleedthrough = $ShieldDefs::RegenerativeBleedthrough;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = %this.maxStrength;
    %this.deltaRegen = ($VehicleListData[$VehicleID::Leviathan, "block"].rechargeRate * $g_TickTime) * -(%this.powerDrainPct / 100);
}

function FLeviathanRegenerative::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, %this.maxStrength, mFloor(%this.recDelay * 1000), mFloor(%this.resTime * 1000), %this.recRate / $g_TickTime, %this.powerDrainPct / 100, %this.bleedthrough / 100, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "FLeviathanRegenerative", "Regenerative Shields", %this.maxStrength @ " HP, Recharge delay: " @ %this.recDelay @ " sec, Restart delay: " @ %this.resTime @ " sec, Bleedthrough: " @ %this.bleedthrough @ "%, Power Drain: " @ %this.powerDrainPct @ "%", $VehicleList::Leviathan, $VHardpointSize::Internal, $VHardpointType::Anything);

function FLeviathanHardened::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";

    // Shield settings
    %this.maxStrength = $SD::Leviathan::maxStrength;
    %this.recDelay = $SD::Leviathan::recDelay * 1.5;
    %this.resTime = $SD::Leviathan::resTime * 1.25;
    %this.recRate = $SD::Leviathan::recRate;
    %this.powerDrainPct = $ShieldDefs::HardenedPowerDrain;
    %this.bleedthrough = $ShieldDefs::HardenedBleedthrough;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = %this.maxStrength;
    %this.deltaRegen = ($VehicleListData[$VehicleID::Leviathan, "block"].rechargeRate * $g_TickTime) * -(%this.powerDrainPct / 100);
}

function FLeviathanHardened::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, %this.maxStrength, mFloor(%this.recDelay * 1000), mFloor(%this.resTime * 1000), %this.recRate / $g_TickTime, %this.powerDrainPct / 100, %this.bleedthrough / 100, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "FLeviathanHardened", "Hardened Shields", %this.maxStrength @ " HP, Recharge delay: " @ %this.recDelay @ " sec, Restart delay: " @ %this.resTime @ " sec, Bleedthrough: " @ %this.bleedthrough @ "%, Power Drain: " @ %this.powerDrainPct @ "%", $VehicleList::Leviathan, $VHardpointSize::Internal, $VHardpointType::Anything);

function FLeviathanCovariant::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";

    // Shield settings
    %this.maxStrength = mCeil($SD::Leviathan::maxStrength * 1.5);
    %this.recDelay = $SD::Leviathan::recDelay;
    %this.resTime = $SD::Leviathan::resTime;
    %this.recRate = $SD::Leviathan::recRate;
    %this.powerDrainPct = $ShieldDefs::CovariantPowerDrain;
    %this.bleedthrough = $ShieldDefs::CovariantBleedthrough;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = %this.maxStrength;
    %this.deltaRegen = ($VehicleListData[$VehicleID::Leviathan, "block"].rechargeRate * $g_TickTime) * -(%this.powerDrainPct / 100);
}

function FLeviathanCovariant::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, %this.maxStrength, mFloor(%this.recDelay * 1000), mFloor(%this.resTime * 1000), %this.recRate / $g_TickTime, %this.powerDrainPct / 100, %this.bleedthrough / 100, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "FLeviathanCovariant", "Covariant Shields", %this.maxStrength @ " HP, Recharge delay: " @ %this.recDelay @ " sec, Restart delay: " @ %this.resTime @ " sec, Bleedthrough: " @ %this.bleedthrough @ "%, Power Drain: " @ %this.powerDrainPct @ "%", $VehicleList::Leviathan, $VHardpointSize::Internal, $VHardpointType::Anything);
