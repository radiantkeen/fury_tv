// Sound, Projectile, and Explosion blocks
datablock AudioProfile(LDiscFireSound)
{
   filename    = "fx/weapons/TR2spinfusor_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(LDiscReloadSound)
{
   filename    = "fx/weapon/heavy_disc_reload.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(LDiscExpSound)
{
   filename    = "fx/explosion/disc_power_exp.wav";
   description = AudioExplosion3d;
   preload = true;
};

datablock ParticleData(DiscExplosionParticle)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.2;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 750;
   lifetimeVarianceMS   = 150;
   textureName          = "particleTest";
   colors[0]     = "0.3 0.3 0.66 1.0";
   colors[1]     = "0.3 0.3 0.66 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 2;
};

datablock ParticleEmitterData(DiscExplosionEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 5;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "DiscExplosionParticle";
};

datablock ShockwaveData(LDiscShockwave)
{
   width = 6.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 10;
   acceleration = 20.0;
   lifetimeMS = 900;
   height = 1.0;
   verticalCurve = 0.5;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.4 0.4 1.0 0.50";
   colors[1] = "0.4 0.4 1.0 0.25";
   colors[2] = "0.4 0.4 1.0 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ExplosionData(LDiscSubExplosion1)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   delayMS = 100;
   offset = 4.0;
   playSpeed = 1.0;

   sizes[0] = "0.75 0.75 0.75";
   sizes[1] = "1.0  1.0  1.0";
   sizes[2] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;
};

datablock ExplosionData(LDiscSubExplosion2)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   delayMS = 50;
   offset = 2.0;
   playSpeed = 0.75;

   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "0.75 0.75 0.75";
   sizes[2] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;
};

datablock ExplosionData(LDiscSubExplosion3)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   delayMS = 0;
   offset = -1.0;
   playSpeed = 0.5;

   sizes[0] = "1.0 1.0 1.0";
   sizes[1] = "1.0 1.0 1.0";
   sizes[2] = "1.5 1.5 1.5";
   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;
};

datablock ExplosionData(LrgDiscExplosion)
{
   soundProfile   = LDiscExpSound;

   shockwave = LDiscShockwave;
   shockwaveOnTerrain = false;

   subExplosion[0] = LDiscSubExplosion1;
   subExplosion[1] = LDiscSubExplosion2;
   subExplosion[2] = LDiscSubExplosion3;

   particleEmitter = DiscExplosionEmitter;
   particleDensity = 400;
   particleRadius = 3.0;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "20.0 20.0 20.0";
   camShakeDuration = 0.5;
   camShakeRadius = 10.0;
};

datablock LinearProjectileData(LDiscProjectile) : DiscProjectile
{
   projectileShapeName = "disc.dts";
   scale               = "2.0 2.0 2.0";
   emitterDelay        = -1;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 4.0;
   damageRadius        = 15.0;
   radiusDamageType    = $DamageType::Disc;
   kickBackStrength    = 2500;  // z0dd - ZOD, 4/24/02. Was 1750

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Explosive;
   ticking             = false;
   headshotMultiplier  = 1.0;

   sound 				= discProjectileSound;
   explosion           = "LrgDiscExplosion";
   underwaterExplosion = "UnderwaterDiscExplosion";
   splash              = DiscSplash;

   dryVelocity       = 250; // z0dd - ZOD, 3/30/02. Slight disc speed up. was 90
   wetVelocity       = 250; // z0dd - ZOD, 3/30/02. Slight disc speed up. was 50
   velInheritFactor  = 1.0; // z0dd - ZOD, 3/30/02. was 0.5
   fizzleTimeMS      = 2000;
   lifetimeMS        = 2000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 15.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 20.0; // z0dd - ZOD, 4/24/02. Was 0.0. 20 degrees skips off water
   fizzleUnderwaterMS        = 2000;

   activateDelayMS = 32;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.175 0.175 0.5";
};

// Primary Definition
datablock ShapeBaseImageData(LDiscSSH2)
{
   shapeFile = "TR2weapon_disc.dts";
   mountPoint = 0;
   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 2, "rotation"];

   projectile = LDiscProjectile;
   projectileType = LinearProjectile;
   ammo = "VLDiscAmmo";
   updatePilotAmmo = true;
   sharedResourcePool = true;

   usesEnergy = false;
   useMountEnergy = false;
   fireEnergy = 75.0;
   minEnergy = 75.0;
   useCapacitor = true;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;
   stateSequence[1]                 = "Activated";
   stateSound[1]                    = DiscSwitchSound;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";
   stateSequence[2]                 = "DiscSpin";
//   stateSound[2]                    = DiscLoopSound;

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 1.5;
   stateFire[3]                     = true;
   stateRecoil[3]                   = LightRecoil;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "Fire";
   stateScript[3]                   = "onFire";
   stateSound[3]                    = LDiscFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 1.25;
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";
   stateSound[4]                    = LDiscReloadSound;

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = DiscDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

// Inherited Defs
datablock ShapeBaseImageData(LDiscSTA2) : LDiscSSH2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Starhawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 2, "rotation"];
};

datablock ShapeBaseImageData(LDiscAMI2) : LDiscSSH2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Ameri, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Ameri, 2, "rotation"];
};

datablock ShapeBaseImageData(LDiscGUI2) : LDiscSSH2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Guillotine, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Guillotine, 2, "rotation"];
};

datablock ShapeBaseImageData(LDiscTUT4) : LDiscSSH2
{
   pilotHeadTracking = true;
   
   mountPoint = 1;

   offset = "0 -0.5 -0.25";
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 4, "rotation"];
};

datablock ShapeBaseImageData(LDiscFIB2) : LDiscSSH2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 2, "rotation"];
};

datablock ShapeBaseImageData(LDiscFIB4) : LDiscSSH2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 4, "rotation"];
};

datablock ShapeBaseImageData(LDiscFIB5) : LDiscSSH2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 5, "rotation"];
};

datablock ShapeBaseImageData(LDiscTUS2) : LDiscSSH2
{
   offset = "0 0.75 -0.25";
   rotation = $VehicleHardpoints[$VehicleID::TurretPole, 2, "rotation"];
};

datablock ShapeBaseImageData(LDiscTOM2) : LDiscSSH2
{
   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 2, "rotation"];
};

datablock ShapeBaseImageData(LDiscTOM3) : LDiscSSH2
{
   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 3, "rotation"];
};

datablock ShapeBaseImageData(LDiscRET2) : LDiscSSH2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Retaliator, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Retaliator, 2, "rotation"];
};

datablock ShapeBaseImageData(LDiscSTR2) : LDiscSSH2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Stryker, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Stryker, 2, "rotation"];
};

datablock ShapeBaseImageData(LDiscTPD2) : LDiscSSH2
{
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 2, "rotation"];
};

datablock ShapeBaseImageData(LDiscTPD3) : LDiscSSH2
{
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 3, "rotation"];
};

function LDisc::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = true;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 50;
    %this.ammo = "VLDiscAmmo";
    %this.ammoUse = 1;
    %this.fireTimeout = 1375;
    
    // Reticle data
    %this.reticleTex = "gui/RET_disc";
    %this.showReticleFrame = true;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LDisc", "Heavy Disc Launcher", "Upscaled version of the Disc Launcher", $VehicleList::Firebird | $VehicleList::Tomahawk | $VehicleList::Ameri | $VehicleList::Guillotine | $VehicleList::Leviathan | $VehicleList::SuperShrike | $VehicleList::Starhawk | $VehicleList::Firebird | $VehicleList::Retaliator | $VehicleList::HoverJeep | $VehicleList::Stryker | $VehicleList::Wolfhound | $VehicleList::Annihilator | $VehicleList::Goodship | $VehicleList::Scorpion, $VHardpointSize::Cannon, $VHardpointType::Ballistic);
