datablock AudioProfile(BreacherMExpSound)
{
   filename = "fx/explosion/missile_large_exp.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock ShockwaveData(BMissileShockwave)
{
   width = 7.0;
   numSegments = 24;
   numVertSegments = 8;
   velocity = 36;
   acceleration = 36.0;
   lifetimeMS = 500;
   height = 1.3;
   verticalCurve = 0.5;

   mapToTerrain = false;
   renderBottom = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1.0 1.0 1.0 1.0";
   colors[1] = "0.8 0.5 0.2 0.75";
   colors[2] = "0.4 0.25 0.05 0.0";
};

datablock ParticleData( BMissileCrescentParticle )
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.357;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -20.0;
   lifetimeMS           = 750;
   lifetimeVarianceMS   = 100;
   textureName          = "special/crescent3";
   colors[0]     = "0.75 0.7 0.2 1.0";
   colors[1]     = "0.75 0.7 0.2 0.5";
   colors[2]     = "0.75 0.7 0.2 0.0";
   sizes[0]      = 2.0;
   sizes[1]      = 4.0;
   sizes[2]      = 8.0;
   times[0]      = 0.25;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( BMissileCrescentEmitter )
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 15;
   velocityVariance = 5.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 180;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 500;
   particles = "BMissileCrescentParticle";
};

datablock ParticleData(BMissileSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 7.5;
   lifetimeMS           = 750;
   lifetimeVarianceMS   = 250;
   textureName          = "special/bigspark";
   colors[0]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 1.0";
   colors[1]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.5";
   colors[2]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.0";
   sizes[0]      = 1;
   sizes[1]      = 2;
   sizes[2]      = 3;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(MissileSparkEmitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 1;
   ejectionVelocity = 32;
   velocityVariance = 8;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 180;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 640;
   particles = "BMissileSparks";
};

datablock ParticleData(BreacherMExplosionSmoke)
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = 0.0;   // rises slowly
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 1250;
   lifetimeVarianceMS   = 500;

//   textureName          = "particleTest";

   useInvAlpha =  false;
   spinRandomMin = -100.0;
   spinRandomMax =  100.0;

   textureName = "special/Smoke/bigSmoke";

   colors[0]     = "1.0 1.0 1.0 1.0";
   colors[1]     = "0.75 0.75 0.75 0.75";
   colors[2]     = "0.5 0.5 0.5 0.5";
   colors[3]     = "0 0 0 0";
   sizes[0]      = 2.0;
   sizes[1]      = 3.0;
   sizes[2]      = 5.0;
   sizes[3]      = 6.0;
   times[0]      = 0.0;
   times[1]      = 0.333;
   times[2]      = 0.666;
   times[3]      = 1.0;
};

datablock ParticleEmitterData(BreacherMExplosionSmokeEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionOffset = 8.0;


   ejectionVelocity = 1.25;
   velocityVariance = 1.2;

   thetaMin         = 0.0;
   thetaMax         = 90.0;

   lifetimeMS       = 500;

   particles = "BreacherMExplosionSmoke";
};

datablock ExplosionData(BMissileSubExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   faceViewer           = true;

   sizes[0] = "4 4 4";
   sizes[1] = "3 3 3";
   times[0] = 0.0;
   times[1] = 1.0;

   delayMS = 75;
   offset = 2.5;
   playSpeed = 0.75;
};

datablock DebrisData(BreacherMDebris)
{
   emitters[0] = MissileSmokeSpikeEmitter;
   explodeOnMaxBounce = true;
   elasticity = 0.4;
   friction = 0.2;
   lifetime = 1.3;
   lifetimeVariance = 0.5;
};

datablock ExplosionData(BreacherMExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   playSpeed = 0.75;
   soundProfile = BreacherMExpSound;
   faceViewer = true;

   sizes[0] = "4 4 4";
   sizes[1] = "3 3 3";
   times[0] = 0.0;
   times[1] = 1.0;

   subExplosion[0] = BMissileSubExplosion;
   emitter[0] = BreacherMExplosionSmokeEmitter;
   emitter[1] = BMissileCrescentEmitter;
   emitter[2] = BMissileSparkEmitter;
   shockwave = BMissileShockwave;

   debris = BreacherMDebris;
   debrisThetaMin = 5;
   debrisThetaMax = 85;
   debrisNum = 8;
   debrisNumVariance = 2;
   debrisVelocity = 30.0;
   debrisVelocityVariance = 5.0;

   shakeCamera = true;
   camShakeFreq = "6.0 7.0 7.0";
   camShakeAmp = "70.0 70.0 70.0";
   camShakeDuration = 1.0;
   camShakeRadius = 7.0;
};

datablock ShapeBaseImageData(MSLAMJEP6) : EngineAPEImage
{
   mountPoint = 0;

   shapeFile = "vehicle_missile_yellow.dts"; // pyro_missile

   offset = $VehicleHardpoints[$VehicleID::HoverJeep, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::HoverJeep, 6, "rotation"];
};

datablock ShapeBaseImageData(MSLAMJEP7) : MSLAMJEP6
{
   offset = $VehicleHardpoints[$VehicleID::HoverJeep, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::HoverJeep, 7, "rotation"];
};

datablock ShapeBaseImageData(MSLAMSSH6) : MSLAMJEP6
{
   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 6, "rotation"];
};

datablock ShapeBaseImageData(MSLAMSSH7) : MSLAMJEP6
{
   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 7, "rotation"];
};

datablock ShapeBaseImageData(MSLAMSTA6) : MSLAMJEP6
{
   offset = $VehicleHardpoints[$VehicleID::Starhawk, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 6, "rotation"];
};

datablock ShapeBaseImageData(MSLAMSTA7) : MSLAMJEP6
{
   offset = $VehicleHardpoints[$VehicleID::Starhawk, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 7, "rotation"];
};

datablock ShapeBaseImageData(MSLAMCAT4) : MSLAMJEP6
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 4, "rotation"];
};

datablock ShapeBaseImageData(MSLAMCAT5) : MSLAMJEP6
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 5, "rotation"];
};

datablock ShapeBaseImageData(MSLAMCAT6) : MSLAMJEP6
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 6, "rotation"];
};

datablock ShapeBaseImageData(MSLAMCAT7) : MSLAMJEP6
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 7, "rotation"];
};

datablock ShapeBaseImageData(MSLAMGUI6) : MSLAMJEP6
{
   offset = $VehicleHardpoints[$VehicleID::Guillotine, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Guillotine, 6, "rotation"];
};

datablock ShapeBaseImageData(MSLAMGUI7) : MSLAMJEP6
{
   offset = $VehicleHardpoints[$VehicleID::Guillotine, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Guillotine, 7, "rotation"];
};

datablock ShapeBaseImageData(MSLAMRET6) : MSLAMJEP6
{
   offset = $VehicleHardpoints[$VehicleID::Retaliator, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Retaliator, 6, "rotation"];
};

datablock ShapeBaseImageData(MSLAMRET7) : MSLAMJEP6
{
   offset = $VehicleHardpoints[$VehicleID::Retaliator, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Retaliator, 7, "rotation"];
};

datablock ShapeBaseImageData(MSLAMWOL6) : MSLAMJEP6
{
   offset = $VehicleHardpoints[$VehicleID::Wolfhound, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Wolfhound, 6, "rotation"];
};

datablock ShapeBaseImageData(MSLAMWOL7) : MSLAMJEP6
{
   offset = $VehicleHardpoints[$VehicleID::Wolfhound, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Wolfhound, 7, "rotation"];
};

datablock BombProjectileData(SLAMMissileDeploy)
{
   projectileShapeName  = "vehicle_missile_yellow.dts";
   emitterDelay         = -1;
   directDamage         = 0.0;
   hasDamageRadius      = true;
   indirectDamage       = 20.0;
   damageRadius         = 30;
   radiusDamageType     = $DamageType::Missile;
   directDamageType     = $DamageType::Missile;
   kickBackStrength     = 2250;  // z0dd - ZOD, 4/25/02. Was 2500

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Explosive;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   explosion            = "BreacherMExplosion";
   velInheritFactor     = 1.0;

   grenadeElasticity    = 0.25;
   grenadeFriction      = 0.4;
   armingDelayMS        = 2000;
   muzzleVelocity       = 15.0;
   drag                 = 0.3;
   gravityMod		    = 1.0;

   minRotSpeed          = "0.0 45.0 0.0";
   maxRotSpeed          = "0.0 45.0 0.0";

   scale                = "1.0 1.0 1.0";

   sound                = "";
   
   missileNameBase      = "SLAMMissile";
};

datablock LinearProjectileData(SLAMMissileDumbfire)
{
//   scale = "1.5 1.5 1.5";
   projectileShapeName = "vehicle_missile_yellow.dts";
   emitterDelay        = -1;
   directDamage        = 0;
   directDamageType    = $DamageType::Missile;
   hasDamageRadius     = true;
   indirectDamage      = 20.0;
   damageRadius        = 30;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 3200;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Explosive;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "BreacherMExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = WildcatJetEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 150;
   wetVelocity       = 50;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 10000;
   lifetimeMS        = 10000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 6000;

   sound = "LargeMissileProjectileSound";
   
   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 7.0;
   lightColor  = "0.4 0.15 0.1";
};

datablock SeekerProjectileData(SLAMMissile) : ShoulderMissile
{
//   scale = "5.0 5.0 5.0";
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "vehicle_missile_yellow.dts";
   directDamage        = 0;
   directDamageType    = $DamageType::Missile;
   hasDamageRadius     = true;
   indirectDamage      = 20;
   damageRadius        = 30;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 3200;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Explosive;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "BreacherMExplosion";
//   underwaterExplosion = UnderwaterHandGrenadeExplosion;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = WildcatJetEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   lifetimeMS          = 10000;
   muzzleVelocity      = 75.0;
   maxVelocity         = 150.0;
   turningSpeed        = 45.0;
   acceleration        = 25.0;

   proximityRadius     = 4;

   flareDistance = 10;
   flareAngle    = 10;

//   sound = ScoutThrustSound;
   sound = "LargeMissileProjectileSound";

   hasLight    = true;
   lightRadius = 7.0;
   lightColor  = "0.4 0.15 0.1";

   useFlechette = false;
   explodeOnWaterImpact = false;

   // Used for vehicle-mounted missile system
   isVehicleMissile = true;
};

function MSLAM::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "";
    %this.rearmTime = 7000;
    
    %this.missileName = "SLAMMissile";
    %this.missileSize = $VHardpointSize::Large;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "MSLAM", "SLAM Missile", "High damage explosive, limited maneuverability, best uses against large slow targets", $VehicleList::SuperShrike | $VehicleList::Starhawk | $VehicleList::HoverJeep | $VehicleList::Catapult | $VehicleList::Guillotine | $VehicleList::Retaliator | $VehicleList::Wolfhound | $VehicleList::Scorpion, $VHardpointSize::Missile, $VHardpointType::Missile);
