datablock ShapeBaseImageData(MTagSHX6) : EngineAPEImage
{
   mountPoint = 0;

   shapeFile = "vehicle_missile_red.dts";

    offset = $VehicleHardpoints[$VehicleID::ShrikeX, 6, "position"];
    rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 6, "rotation"];
};

datablock ShapeBaseImageData(MTagSHX7) : MTagSHX6
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 7, "rotation"];
};

datablock ShapeBaseImageData(MTagSHR6) : MTagSHX6
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 6, "rotation"];
};

datablock ShapeBaseImageData(MTagSHR7) : MTagSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 7, "rotation"];
};

datablock ShapeBaseImageData(MTagAIR7) : MTagSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Airhawk, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Airhawk, 7, "rotation"];
};

datablock ShapeBaseImageData(MTagSSH6) : MTagSHR6
{
   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 6, "rotation"];
};

datablock ShapeBaseImageData(MTagSSH7) : MTagSHR6
{
   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 7, "rotation"];
};

datablock ShapeBaseImageData(MTagSTA6) : MTagSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Starhawk, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 6, "rotation"];
};

datablock ShapeBaseImageData(MTagSTA7) : MTagSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Starhawk, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 7, "rotation"];
};

datablock ShapeBaseImageData(MTagTOM4) : MTagSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 4, "rotation"];
};

datablock ShapeBaseImageData(MTagTOM5) : MTagSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 5, "rotation"];
};

datablock ShapeBaseImageData(MTagTOM6) : MTagSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 6, "rotation"];
};

datablock ShapeBaseImageData(MTagTOM7) : MTagSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 7, "rotation"];
};

datablock ShapeBaseImageData(MTagAMI6) : MTagSHX6
{
   offset = $VehicleHardpoints[$VehicleID::Ameri, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Ameri, 6, "rotation"];
};

datablock ShapeBaseImageData(MTagAMI7) : MTagSHX6
{
   offset = $VehicleHardpoints[$VehicleID::Ameri, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Ameri, 7, "rotation"];
};

datablock ShapeBaseImageData(MTagJEP6) : MTagSHX6
{
   offset = $VehicleHardpoints[$VehicleID::HoverJeep, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::HoverJeep, 6, "rotation"];
};

datablock ShapeBaseImageData(MTagJEP7) : MTagSHX6
{
   offset = $VehicleHardpoints[$VehicleID::HoverJeep, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::HoverJeep, 7, "rotation"];
};

datablock ShapeBaseImageData(MTagCAT4) : MTagSHX6
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 4, "rotation"];
};

datablock ShapeBaseImageData(MTagCAT5) : MTagSHX6
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 5, "rotation"];
};

datablock ShapeBaseImageData(MTagCAT6) : MTagSHX6
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 6, "rotation"];
};

datablock ShapeBaseImageData(MTagCAT7) : MTagSHX6
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 7, "rotation"];
};

datablock ShapeBaseImageData(MTagGUI6) : MTagSHX6
{
   offset = $VehicleHardpoints[$VehicleID::Guillotine, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Guillotine, 6, "rotation"];
};

datablock ShapeBaseImageData(MTagGUI7) : MTagSHX6
{
   offset = $VehicleHardpoints[$VehicleID::Guillotine, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Guillotine, 7, "rotation"];
};

datablock ShapeBaseImageData(MTagRET6) : MTagSHX6
{
   offset = $VehicleHardpoints[$VehicleID::Retaliator, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Retaliator, 6, "rotation"];
};

datablock ShapeBaseImageData(MTagRET7) : MTagSHX6
{
   offset = $VehicleHardpoints[$VehicleID::Retaliator, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Retaliator, 7, "rotation"];
};

datablock BombProjectileData(TagMissileDeploy)
{
   projectileShapeName  = "vehicle_missile_red.dts";
   emitterDelay         = -1;
   directDamage         = 0.0;
   hasDamageRadius      = true;
   indirectDamage       = 0.5;
   damageRadius         = 1;
   radiusDamageType     = $DamageType::Missile;
   directDamageType     = $DamageType::Missile;
   kickBackStrength     = 200;  // z0dd - ZOD, 4/25/02. Was 2500

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::TAGProjectile;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   explosion            = "MissileExplosion";
   velInheritFactor     = 1.0;

   grenadeElasticity    = 0.25;
   grenadeFriction      = 0.4;
   armingDelayMS        = 2000;
   muzzleVelocity       = 15.0;
   drag                 = 0.3;
   gravityMod		    = 1.0;

   minRotSpeed          = "0.0 45.0 0.0";
   maxRotSpeed          = "0.0 45.0 0.0";

   scale                = "1.0 1.0 1.0";

   sound                = "";
   
   missileNameBase      = "TagMissile";
};

datablock LinearProjectileData(TagMissileDumbfire)
{
//   scale = "1.5 1.5 1.5";
   projectileShapeName = "vehicle_missile_red.dts";
   emitterDelay        = -1;
   directDamage        = 0;
   directDamageType    = $DamageType::Missile;
   hasDamageRadius     = true;
   indirectDamage      = 0.5;
   damageRadius        = 1;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 200;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::TAGProjectile;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "ScoutChaingunExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = WildcatJetEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 350;
   wetVelocity       = 200;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 5000;
   lifetimeMS        = 5000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 6000;

   sound = "LargeMissileProjectileSound";
   
   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 7.0;
   lightColor  = "0.4 0.15 0.1";
};

datablock SeekerProjectileData(TagMissile) : ShoulderMissile
{
//   scale = "5.0 5.0 5.0";
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "vehicle_missile_red.dts";
   directDamage        = 0;
   directDamageType    = $DamageType::Missile;
   hasDamageRadius     = true;
   indirectDamage      = 0.5;
   damageRadius        = 1;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 200;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::TAGProjectile;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "ScoutChaingunExplosion";
//   underwaterExplosion = UnderwaterHandGrenadeExplosion;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = WildcatJetEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   lifetimeMS          = 5000;
   muzzleVelocity      = 350.0;
   maxVelocity         = 350.0;
   turningSpeed        = 360.0;
   acceleration        = 350.0;

   proximityRadius     = 4;

   flareDistance = 10;
   flareAngle    = 10;

//   sound = ScoutThrustSound;
   sound = "LargeMissileProjectileSound";

   hasLight    = true;
   lightRadius = 7.0;
   lightColor  = "0.4 0.15 0.1";

   useFlechette = false;
   explodeOnWaterImpact = false;

   // Used for vehicle-mounted missile system
   isVehicleMissile = true;
};

function MTag::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "";
    %this.rearmTime = 5000;

    %this.missileName = "TagMissile";
    %this.missileSize = $VHardpointSize::Small;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "MTag", "Tag Missile", "Beacon that relays sensor info, +50% damage done to the target's armor for 30 seconds", $VehicleList::Wildcat | $VehicleList::Ameri | $VehicleList::ShrikeX | $VehicleList::Airhawk | $VehicleList::Shrike | $VehicleList::SuperShrike | $VehicleList::Starhawk | $VehicleList::Tomahawk | $VehicleList::HoverJeep | $VehicleList::Catapult | $VehicleList::Guillotine | $VehicleList::Retaliator, $VHardpointSize::Missile, $VHardpointType::Missile);
