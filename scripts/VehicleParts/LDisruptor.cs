// Sound, Projectile, and Explosion blocks
datablock AudioProfile(LDisruptorFireSound)
{
   filename    = "fx/weapon/turret_plasma_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock ParticleData(LDisruptorDebrisSmokeParticle)
{
   dragCoeffiecient     = 4.0;
   gravityCoefficient   = -0.01;   // rises slowly
   inheritedVelFactor   = 0.01;

   lifetimeMS           = 1250;
   lifetimeVarianceMS   = 500;   // ...more or less

   textureName          = "flarebase";

   useInvAlpha =     false;

   spinRandomMin = -50.0;
   spinRandomMax = 50.0;

   colors[0]     = "1.0 1.0 1.0 1.0";
   colors[1]     = "0.075 0.1 0.75 0.75";
   colors[2]     = "0.0 0.05 0.5 0.5";
   colors[3]     = "0.0 0.0 0.1 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.625;
   sizes[2]      = 0.7;
   sizes[3]      = 0.49;
   times[0]      = 0.0;
   times[1]      = 0.45;
   times[2]      = 0.7;
   times[3]      = 1.0;
};

datablock ParticleEmitterData(LDisruptorDebrisSmokeEmitter)
{
   ejectionPeriodMS = 16;
   periodVarianceMS = 6;

   ejectionVelocity = 0.5;  // A little oomph at the back end
   velocityVariance = 0.1;

   thetaMin         = 1.0;
   thetaMax         = 8.0;

   particles = "LDisruptorDebrisSmokeParticle";
};

datablock ParticleData(LDisruptorExplosionSmoke)
{
   dragCoeffiecient     = 0.5;
   gravityCoefficient   = 0;   // rises slowly
   inheritedVelFactor   = 0;
   constantAcceleration = -0.35;

   lifetimeMS           = 1750;
   lifetimeVarianceMS   = 350;

   textureName          = "flarebase";

   useInvAlpha =  false;
   spinRandomMin = -100.0;
   spinRandomMax =  100.0;

//   textureName = "special/Smoke/bigSmoke";

   colors[0]     = "1.0 1.0 1.0 0.0";
   colors[1]     = "0.15 0.1 0.95 1.0";
   colors[2]     = "0.1 0.1 0.75 0.75";
   colors[3]     = "0.075 0.15 0.5 0.0";
   colors[4]     = "0.1 0.1 0.1 0.0";
   colors[5]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 4.0;
   sizes[1]      = 5.5;
   sizes[2]      = 6.1;
   sizes[3]      = 8.5;
   sizes[4]      = 10.25;
   sizes[5]      = 12.0;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 0.35547;
   times[3]      = 0.5235;
   times[4]      = 0.83547;
   times[5]      = 1.0;
};

datablock ParticleEmitterData(LDisruptorExplosionSmokeEmitter)
{
   ejectionPeriodMS = 12;
   periodVarianceMS = 4;

   ejectionOffset = 1.0;

   ejectionVelocity = 7;
   velocityVariance = 2;

   thetaMin         = 0.0;
   thetaMax         = 180.0;

   lifetimeMS       = 650;

   particles = "LDisruptorExplosionSmoke";
};

datablock DebrisData(LDisruptorExplosionDebris)
{
   emitters[0] = LDisruptorDebrisSmokeEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 3.0;
   lifetimeVariance = 2.0;

  numBounces = 3;//1; --- variance can't be > num (ST)
   bounceVariance = 2;//3; --- 2/2 = range of 0-4
};

datablock ExplosionData(LDisruptorExplosion)
{
//   explosionShape = "effect_plasma_explosion.dts";
   soundProfile   = LDisruptorExpSound;
//   particleEmitter = PlasmaExplosionEmitter;
//   particleDensity = 150;
//   particleRadius = 3.25;
   faceViewer = true;

   sizes[0] = "1.75 1.75 1.75";
   sizes[1] = "1.75 1.75 1.75";
   times[0] = 0.0;
   times[1] = 1.0;

   debris = LDisruptorExplosionDebris;
   debrisThetaMin = 5;
   debrisThetaMax = 45;
   debrisNum = 6;
   debrisNumVariance = 2;
   debrisVelocity = 15.0;
   debrisVelocityVariance = 5.0;

   emitter[0] = LDisruptorExplosionSmokeEmitter;
};

datablock LinearProjectileData(VLDisruptorBolt)
{
   scale = "3.0 3.0 3.0";
   projectileShapeName = "ironfist.dts";
   emitterDelay        = -1;
   directDamage        = 0;
   directDamageType    = $DamageType::Disruptor;
   hasDamageRadius     = true;
   indirectDamage      = 5;
   damageRadius        = 1;
   radiusDamageType    = $DamageType::Disruptor;
   kickBackStrength    = 500;

   flags               = $Projectile::PlaysHitSound | $Projectile::Plasma;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "LDisruptorExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

//   baseEmitter         = MissileSmokeEmitter;
//   delayEmitter        = WildcatJetEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 200;
   wetVelocity       = 200;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 6000;

   sound = "SentryTurretProjectileSound";

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 7.0;
   lightColor  = "0.0 0.1 0.5";
};

// Primary Definition
datablock ShapeBaseImageData(LDisruptorSTA2)
{
	className = WeaponImage;
   shapeFile = "turret_fusion_large.dts";
//   item = PulseDisruptor;
   projectile = VLDisruptorBolt;
   projectileType = LinearProjectile;
//	armThread = looksn;

   pilotHeadTracking = true;
    usesEnergy = true;
	minEnergy = 90;
    fireEnergy = 80;
//    defaultModeFireSound = "LaserFireSound";

   offset = $VehicleHardpoints[$VehicleID::Starhawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 2, "rotation"];

   fireTimeout = 1000;

   // State transitions
   stateName[0]                  = "Activate";
   stateTransitionOnNotLoaded[0] = "Dead";
   stateTransitionOnLoaded[0]    = "ActivateReady";

   stateName[1]                  = "ActivateReady";
   stateSequence[1]              = "Deploy";
   stateTimeoutValue[1]          = 1;
   stateTransitionOnTimeout[1]   = "Ready";
   stateTransitionOnNotLoaded[1] = "Deactivate";
   stateTransitionOnNoAmmo[1]    = "NoAmmo";

   stateName[2]                    = "Ready";
   stateTransitionOnNotLoaded[2]   = "Deactivate";
   stateTransitionOnTriggerDown[2] = "Fire";
   stateTransitionOnNoAmmo[2]      = "NoAmmo";

   stateName[3]                = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3]        = 0.75;
   stateFire[3]                = true;
   stateRecoil[3]              = LightRecoil;
   stateAllowImageChange[3]    = false;
   stateSequence[3]            = "Fire";
   stateSound[3]               = LDisruptorFireSound;
   stateScript[3]              = "onFire";

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 1.25;
   stateAllowImageChange[4]      = false;
   stateSequence[4]              = "Deploy";
   stateTransitionOnTimeout[4]   = "Ready";
   stateTransitionOnNotLoaded[4] = "Deactivate";
    stateTransitionOnNoAmmo[4]    = "NoAmmo";

   stateName[5]                = "Deactivate";
   stateSequence[5]            = "Activate";
   stateDirection[5]           = false;
   stateTimeoutValue[5]        = 1;
   stateTransitionOnLoaded[5]  = "ActivateReady";
   stateTransitionOnTimeout[5] = "Dead";

   stateName[6]               = "Dead";
   stateTransitionOnLoaded[6] = "ActivateReady";

   stateName[7]             = "NoAmmo";
   stateTransitionOnAmmo[7] = "Reload";
   stateSequence[7]         = "NoAmmo";
};

// Inherited Defs
datablock ShapeBaseImageData(LDisruptorAMI2) : LDisruptorSTA2
{
   pilotHeadTracking = false;

   offset = $VehicleHardpoints[$VehicleID::Ameri, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Ameri, 2, "rotation"];
};

datablock ShapeBaseImageData(LDisruptorSSH2) : LDisruptorSTA2
{
   pilotHeadTracking = false;

   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 2, "rotation"];
};

datablock ShapeBaseImageData(LDisruptorTUT4) : LDisruptorSTA2
{
   pilotHeadTracking = false;

   mountPoint = 1;

   offset = "0 0 0";
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 4, "rotation"];
};

datablock ShapeBaseImageData(LDisruptorTOM2) : LDisruptorSTA2
{
   pilotHeadTracking = false;

   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 2, "rotation"];
};

datablock ShapeBaseImageData(LDisruptorTOM3) : LDisruptorSTA2
{
   pilotHeadTracking = false;

   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 3, "rotation"];
};

datablock ShapeBaseImageData(LDisruptorFIB2) : LDisruptorSTA2
{
   pilotHeadTracking = false;
   
   offset = $VehicleHardpoints[$VehicleID::Firebird, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 2, "rotation"];
};

datablock ShapeBaseImageData(LDisruptorFIB4) : LDisruptorSTA2
{
   pilotHeadTracking = false;
   
   offset = $VehicleHardpoints[$VehicleID::Firebird, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 4, "rotation"];
};

datablock ShapeBaseImageData(LDisruptorFIB5) : LDisruptorSTA2
{
   pilotHeadTracking = false;
   
   offset = $VehicleHardpoints[$VehicleID::Firebird, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 5, "rotation"];
};

datablock ShapeBaseImageData(LDisruptorTUS2) : LDisruptorSTA2
{
   pilotHeadTracking = false;
   
   offset = "0 0.5 0";
   rotation = "0 1 0 180";
};

datablock ShapeBaseImageData(LDisruptorGUI2) : LDisruptorSTA2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Guillotine, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Guillotine, 2, "rotation"];
};

datablock ShapeBaseImageData(LDisruptorRET2) : LDisruptorSTA2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Retaliator, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Retaliator, 2, "rotation"];
};

datablock ShapeBaseImageData(LDisruptorSTR2) : LDisruptorSTA2
{
   pilotHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::Stryker, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Stryker, 2, "rotation"];
};

datablock ShapeBaseImageData(LDisruptorTPD2) : LDisruptorSTA2
{
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 2, "position"];
   rotation = "0 1 0 180";
};

datablock ShapeBaseImageData(LDisruptorTPD3) : LDisruptorSTA2
{
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 3, "position"];
   rotation = "0 1 0 180";
};

function LDisruptor::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "";
    %this.ammoUse = 0;
    %this.fireTimeout = 1000;
    
    // Reticle data
    %this.reticleTex = "gui/hud_ret_tankmortar";
    %this.showReticleFrame = true;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LDisruptor", "Heavy Disruptor Cannon", "Trades Blaster's speed for damage, good vs larger targets", $VehicleList::Firebird | $VehicleList::Tomahawk | $VehicleList::Ameri | $VehicleList::SuperShrike | $VehicleList::Leviathan | $VehicleList::Guillotine | $VehicleList::Starhawk | $VehicleList::Firebird | $VehicleList::Retaliator | $VehicleList::HoverJeep | $VehicleList::Stryker | $VehicleList::Wolfhound | $VehicleList::Annihilator | $VehicleList::Goodship | $VehicleList::Scorpion, $VHardpointSize::Cannon, $VHardpointType::Energy);
