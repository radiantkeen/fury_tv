// Sound, Projectile, and Explosion blocks

// Primary Definition
datablock ShapeBaseImageData(LBreacherBayUBT4)
{
   className = WeaponImage;
   shapeFile = "turret_muzzlepoint.dts";
   mountPoint = 10;

   offset = $VehicleHardpoints[$VehicleID::Underbelly, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Underbelly, 4, "rotation"];

   projectile = BreacherMissileDeploy;
   projectileType = BombProjectile;
   ammo = "VLBreacherBayAmmo";
   updatePilotAmmo = true;
   sharedResourcePool = true;

   usesEnergy = false;
   useMountEnergy = false;
   fireEnergy = 0.0;
   minEnergy = 0.0;
   useCapacitor = false;

   fireTimeout = 1000;

   // State transitions
   stateName[0]                  = "Activate";
   stateTransitionOnNotLoaded[0] = "Dead";
   stateTransitionOnLoaded[0]    = "ActivateReady";

   stateName[1]                  = "ActivateReady";
   stateSequence[1]              = "Deploy";
   stateTimeoutValue[1]          = 1;
   stateTransitionOnTimeout[1]   = "Ready";
   stateTransitionOnNotLoaded[1] = "Deactivate";
   stateTransitionOnNoAmmo[1]    = "NoAmmo";

   stateName[2]                    = "Ready";
   stateTransitionOnNotLoaded[2]   = "Deactivate";
   stateTransitionOnTriggerDown[2] = "Fire";
   stateTransitionOnNoAmmo[2]      = "NoAmmo";

   stateName[3]                = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3]        = 1.0;
   stateFire[3]                = true;
   stateRecoil[3]              = LightRecoil;
   stateAllowImageChange[3]    = false;
   stateSequence[3]            = "Fire";
   stateSound[3]               = BomberBombFireSound;
   stateScript[3]              = "onFire";

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 1.0;
   stateAllowImageChange[4]      = false;
   stateSound[4]                 = "";
   stateTransitionOnTimeout[4]   = "Ready";
   stateTransitionOnNotLoaded[4] = "Deactivate";
    stateTransitionOnNoAmmo[4]    = "NoAmmo";

   stateName[5]                = "Deactivate";
   stateSequence[5]            = "Activate";
   stateDirection[5]           = false;
   stateTimeoutValue[5]        = 1;
   stateTransitionOnLoaded[5]  = "ActivateReady";
   stateTransitionOnTimeout[5] = "Dead";

   stateName[6]               = "Dead";
   stateTransitionOnLoaded[6] = "ActivateReady";

   stateName[7]             = "NoAmmo";
   stateTransitionOnAmmo[7] = "Reload";
   stateSequence[7]         = "NoAmmo";
};

// Inherited Defs

datablock ShapeBaseImageData(LBreacherBayUBT5) : LBreacherBayUBT4
{
   offset = $VehicleHardpoints[$VehicleID::Underbelly, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Underbelly, 5, "rotation"];
};

function LBreacherBayUBT4::onFire(%data, %obj, %slot)
{
    %data.onBayLaunch(%obj, %slot, "BreacherMissileDeploy", "BreacherMissile", $VHardpointSize::Medium);
}

function LBreacherBayUBT5::onFire(%data, %obj, %slot)
{
    %data.onBayLaunch(%obj, %slot, "BreacherMissileDeploy", "BreacherMissile", $VHardpointSize::Medium);
}

function LBreacherBay::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = true;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 10;
    %this.ammo = "VLBreacherBayAmmo";
    %this.ammoUse = 1;
    %this.fireTimeout = 1000;
    
    // Reticle data
    %this.reticleTex = "gui/RET_missile";
    %this.showReticleFrame = false;
}

function LBreacherBay::onTrigger(%this, %obj, %state, %start, %run) // do alternating fire
{
    %this.setAlternateFireTrigger(%obj, %start, %state);
}

function LBreacherBay::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    %slot = $VehicleHardpoints[%vehicle.vid, %hardpoint, "imageStart"];
    %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]);

    %turret.unmountImage(0);
    %turret.mountImage(SeekingTurretParam, 0);
    %turret.mountImage(LBreacherBayUBT4, %slot);
    %turret.mountImage(LBreacherBayUBT5, %slot+1);
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LBreacherBay", "Breacher Launch Bay", "Gunner-lockable stockpile of Breacher missiles", $VehicleList::Stryker | $VehicleList::Retaliator | $VehicleList::Annihilator, $VHardpointSize::Missile, $VHardpointType::Bay);
