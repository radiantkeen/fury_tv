datablock AudioProfile(VRepairLoopSound)
{
     filename = "fx/misc/selfrepair_idle.wav";
     description = AudioDefaultLooping3d;
     preload = true;
};

function CArmorRepair::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Push;
    %this.cooldownTime = 2000;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = 0;
    %this.deltaRegen = 0;
}

function CArmorRepair::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = true;
}

function CArmorRepair::triggerPush(%this, %data, %vehicle, %player)
{
    if(%vehicle.reassemblerTicking)
        return 500;

    if(%vehicle.getDamagePercent() == 0)
    {
        messageClient(%player.client, 'MsgVReassemblerNoNeed', "\c2Hull integrity at 100%.");
        return 2000;
    }

    if(%vehicle.getEnergyPct() < 0.3)
    {
        messageClient(%player.client, 'MsgVReassemblerNotEnoughEn', "\c2Initial injection energy too low, shutting down.");
        return 1000;
    }

    messageClient(%player.client, 'MsgVReassemblerStart', '\c2Engaging repair module.');

    %vehicle.reassemblerTicking = true;
    %vehicle.tickHeals = 0;
    
    %vehicle.useEnergy(%vehicle.getMaxEnergy() * 0.25);
    %vehicle.playAudio(3, VRepairLoopSound);
    %this.tickReassembler(%data, %vehicle, %player);
}

function CArmorRepair::tickReassembler(%this, %data, %vehicle, %player)
{
    if(!isObject(%vehicle) || !%vehicle.reassemblerTicking)
        return;

    if(%vehicle.getDamagePercent() == 0)
    {
        messageClient(%player.client, 'MsgVReassemblerFinished', "\c2Hull integrity at 100%, repairs complete.");
        %vehicle.moduleTimeout[%this.getName()] = getSimTime() + 3000;
        %this.stopReassembler(%vehicle);
        return;
    }
        
    if(%vehicle.getEnergyPct() < 0.06)
    {
        messageClient(%player.client, 'MsgVReassemblerNoEnergy', "\c2Energy too low, repair module shutting down.");
        %vehicle.moduleTimeout[%this.getName()] = getSimTime() + 2000;
        %this.stopReassembler(%vehicle);
        return;
    }
    
    %mul = vectorLen(%vehicle.getVelocity()) > 10 ? 1 : 2;
    
    %vehicle.setDamageLevel(%vehicle.getDamageLevel() - (%vehicle.getMaxDamage() * 0.05 * %mul));
    %vehicle.useEnergy(%vehicle.getMaxEnergy() * 0.05);
    zapEffect(%vehicle, "FXRedShift");

    %vehicle.tickHeals++;

    if(%vehicle.tickHeals < 5)
        %this.schedule(512, "tickReassembler", %data, %vehicle, %player);
    else
    {
        messageClient(%player.client, 'MsgVReassemblerCycled', "\c2Repair cycle complete, nanobots shutting down.");
        %vehicle.moduleTimeout[%this.getName()] = getSimTime() + 3000;
        %this.stopReassembler(%vehicle);
    }
}

function CArmorRepair::stopReassembler(%this, %vehicle)
{
    %vehicle.reassemblerTicking = false;
    %vehicle.stopAudio(3);
}

function CArmorRepair::reArm(%this, %data, %vehicle, %player, %slot)
{
    %vehicle.moduleTimeout[%this.getName()] = 0;
    %vehicle.reassemblerTicking = false;
}

VehiclePart.registerVehiclePart($VehiclePartType::Module, "CArmorRepair", "Repair Module", "[Active] Repairs hull damage over time, activate multiple times while stationary for full effect", $VehicleList::All, $VHardpointSize::Internal, $VHardpointType::Anything);
