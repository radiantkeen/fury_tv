// Sound, Projectile, and Explosion blocks
datablock AudioProfile(ParticleBeamFireSound)
{
   filename    = "fx/weapon/particlebeam_precharge_fire.wav";
   description = AudioAmbient3d;
   preload = true;
};

datablock AudioProfile(ParticleBeamReloadSound)
{
   filename    = "fx/weapon/pbc_mount.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(ParticleBeamChargeSound)
{
   filename    = "fx/misc/tech_ambient_loop.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(ParticleBeamFailSound)
{
   filename    = "fx/weapons/chaingun_off.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(ParticleBeamExpSound)
{
   filename    = "fx/explosion/particlebeam_hit.wav";
   description = AudioAmbient3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Explosion
//--------------------------------------

datablock ShockwaveData(PBWShockwave)
{
   width = 6;
   numSegments = 32;
   numVertSegments = 7;
   velocity = 40;
   acceleration = 35.0;
   lifetimeMS = 999;
   height = 0.5;
   verticalCurve = 0.375;

   mapToTerrain = false;
   renderBottom = true;
   orientToNormal = true;

   texture[0] = "special/shockwave5";
   texture[1] = "special/gradient";
   texWrap = 1.5;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.6 0.6 1.0 1.0";
   colors[1] = "0.6 0.3 1.0 0.5";
   colors[2] = "0.0 0.0 1.0 0.0";
};

datablock ExplosionData(PBWExplosion)
{
   explosionShape = "disc_explosion.dts";
   soundProfile   = ParticleBeamExpSound;
   shockwave      = PBWShockwave;
   faceViewer     = true;
   playSpeed      = 0.3;

   sizes[0] = "2 2 2";
   sizes[1] = "2 2 2";
   times[0]      = 0.0;
   times[1]      = 1.0;

   shakeCamera = true;
   camShakeFreq = "10.0 6.0 9.0";
   camShakeAmp = "15.0 15.0 15.0";
   camShakeDuration = 1.5;
   camShakeRadius = 10.0;
};

//--------------------------------------
// Projectile
//--------------------------------------
datablock SniperProjectileData(ParticleBeam)
{
   directDamage        = 11.25;
   velInheritFactor    = 1.0;
   faceViewer          = true;
//   sound 			   = PBWProjectileSound;
   explosion           = "PBWExplosion";
   splash              = PlasmaSplash;
   directDamageType    = $DamageType::PBC;
   kickBackStrength    = 2250;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Kinetic;
   ticking             = false;
   headshotMultiplier  = 1.25;

   maxRifleRange       = 300;
   rifleHeadMultiplier = 1.25;
   beamColor           = "0.7 0.7 1";
   fadeTime            = 1.0;

   hasFalloff = false;
   optimalRange = 200;
   falloffRange = 300;
   falloffDamagePct = 0.25;

   startBeamWidth		  = 0.5;
   endBeamWidth 	     = 0.025;
   pulseBeamWidth 	  = 0.75;
   beamFlareAngle 	  = 45.0;
   minFlareSize        = 0.0;
   maxFlareSize        = 400.0;
   pulseSpeed          = 15.0;
   pulseLength         = 0.15;

   lightRadius         = 10.0;
   lightColor          = "0.7 0.7 1";

   textureName[0]   = "special/flare";
   textureName[1]   = "special/nonlingradient";
   textureName[2]   = "special/ELFBeam";
   textureName[3]   = "special/ELFLightning";
   textureName[4]   = "special/skyLightning";
   textureName[5]   = "special/ELFBeam";
   textureName[6]   = "special/ELFLightning";
   textureName[7]   = "special/skyLightning";
   textureName[8]   = "special/ELFBeam";
   textureName[9]   = "special/ELFLightning";
   textureName[10]   = "special/skyLightning";
   textureName[11]   = "special/flare";
};

datablock ShockwaveData(PBWFireShockwave)
{
   width = 6;
   numSegments = 24;
   numVertSegments = 7;
   velocity = 1;
   acceleration = -90.0;
   lifetimeMS = 400;
   height = 0.5;
   verticalCurve = 0.375;

   mapToTerrain = false;
   renderBottom = true;
   orientToNormal = true;

   texture[0] = "special/shockwave5";
   texture[1] = "special/gradient";
   texWrap = 1.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1.0 0.3 1.0 1.0";
   colors[1] = "0.5 0.8 0.5 0.5";
   colors[2] = "0.0 1.0 0.0 0.0";
};

datablock ExplosionData(PBCDisplayExplosion)
{
   soundProfile   = PBCShockwaveSound;
   shockwave = PBWFireShockwave;

   faceViewer = true;
};

datablock LinearFlareProjectileData(PBCDisplayCharge)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0;
   hasDamageRadius     = false;
   indirectDamage      = 0.01;
   damageRadius        = 5;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 0;

   explosion           = "PBCDisplayExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

// Primary Definition
datablock ShapeBaseImageData(XLParticleBeamTUT2)
{
   shapeFile = "herc_elf_right.dts"; // turret_tank_barrelmortar
   mountPoint = 0;
   offset = "-0.5 0 0";
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 2, "rotation"];

   projectile = ParticleBeam;
   projectileType = SniperProjectile;
   ammo = "VLParticleBeamAmmo";
   updatePilotAmmo = true;
   sharedResourcePool = true;

   usesEnergy = false;
   useMountEnergy = false;
   fireEnergy = 300.0;
   minEnergy = 350.0;
   useCapacitor = true;
   
   staggerCount = 4;
   staggerDelay = 250;
   isLaser = true;
   laserOpacity = 1.0;
   
   chargingBeginColor = "255 165 0";
   chargingReadyColor = "0 255 0";
   chargingTime = 1000;
   overchargeTime = 600;
   chargeFailRecycleTime = 1000;
   chargeReloadTime = 7000;
   chargeReloadSoundTime = 5500;
   chargeFireSound = "ParticleBeamFireSound";
   chargeReloadSound = "ParticleBeamReloadSound";
   chargeFailSound = "ParticleBeamFailSound";

   stateName[0]                     = "Activate";
   stateTimeoutValue[0]             = 0.5;
   stateSequence[0]                 = "Activated";
   stateTransitionOnTimeout[0]      = "Ready";

   stateName[1]                     = "Ready";
   stateSequence[1]                 = "Reload";
   stateTransitionOnTriggerDown[1]  = "Fire";

   stateName[2]                     = "Fire";
   stateFire[2]                     = true;
   stateAllowImageChange[2]         = false;
   stateSound[2]                    = "ParticleBeamChargeSound";
   stateScript[2]                   = "onFired";
   stateTransitionOnTriggerUp[2]    = "Deconstruction";

   stateName[3]                     = "Deconstruction";
   stateScript[3]                   = "onReleased";
   stateTimeoutValue[3]             = 0.5;
   stateTransitionOnTimeout[3]      = "Ready";
};

// Inherited Defs

function XLParticleBeam::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = true;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 30;
    %this.ammo = "VLParticleBeamAmmo";
    %this.ammoUse = 1;
    %this.fireTimeout = 0;
    
    // Reticle data
    %this.reticleTex = "gui/hud_ret_shocklance";
    %this.showReticleFrame = true;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "XLParticleBeam", "Particle Beam Cannon", "Instant hit beam weapon that deals massive damage over 1 second", $VehicleList::Guillotine | $VehicleList::Leviathan, $VHardpointSize::Siege, $VHardpointType::Standard);
