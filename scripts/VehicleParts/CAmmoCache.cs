function CAmmoCache::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Push;
    
    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = 0;
    %this.deltaRegen = 0;
}

function CAmmoCache::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = true;
//    %vehicle.bonusWeight += %data.mass * 0.15;
    %vehicle.maxAmmoCapacityFactor += 1.0;
}

VehiclePart.registerVehiclePart($VehiclePartType::Module, "CAmmoCache", "Ammo Cache", "[Passive] Doubles ammo capacity", $VehicleList::HasAmmo, $VHardpointSize::Internal, $VHardpointType::Omni);
