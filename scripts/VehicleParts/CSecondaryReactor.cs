function CSecondaryReactor::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Push;
    
    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = 0;
    %this.deltaRegen = 0;
}

function CSecondaryReactor::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = true;
    %vehicle.rechargeTick *= 1.5;
}

VehiclePart.registerVehiclePart($VehiclePartType::Module, "CSecondaryReactor", "Secondary Reactor", "[Passive] Increases recharge rate by 50%", $VehicleList::All, $VHardpointSize::Internal, $VHardpointType::Omni);
