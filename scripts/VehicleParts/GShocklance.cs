// Primary Definition

datablock ShockLanceProjectileData(VBasicShocker) : BasicShocker
{
   directDamage        = 1.5;
   radiusDamageType    = $DamageType::ShockLance;
   kickBackStrength    = 2600; // z0dd - ZOD, 3/30/02. More lance kick. was 2500

   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   impulse = 4000;
   boltLength = 20.0; // z0dd - ZOD, 3/30/02. was 14.0
   extension = 20.0; // script variable indicating distance you can shock people from. // z0dd - ZOD, 3/30/02. Was 14.0
};

datablock ShapeBaseImageData(GShocklanceSKY0)
{
   className = WeaponImage;
   shapeFile = "TR2weapon_shocklance.dts";

   projectileType = ShockLanceProjectile;
   projectile = VBasicShocker;
   mountPoint = 0;

   offset = $VehicleHardpoints[$VehicleID::Airhawk, 0, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Airhawk, 0, "rotation"];

   usesEnergy = true;
   useMountEnergy = true;
   sharedResourcePool = true;
   
   missEnergy = 15;
   hitEnergy  = 50; // z0dd - ZOD, 8/20/02. Was 15.
   minEnergy = 15;
   fireEnergy = 15;
   fireTimeout = 2500;

   // State transitions
   stateName[0]                  = "Activate";
   stateTransitionOnNotLoaded[0] = "Dead";
   stateTransitionOnLoaded[0]    = "ActivateReady";

   stateName[1]                  = "ActivateReady";
   stateSequence[1]              = "Deploy";
   stateTimeoutValue[1]          = 1;
   stateTransitionOnTimeout[1]   = "Ready";
   stateTransitionOnNotLoaded[1] = "Deactivate";
   stateTransitionOnNoAmmo[1]    = "NoAmmo";

   stateName[2]                    = "Ready";
   stateTransitionOnNotLoaded[2]   = "Deactivate";
   stateTransitionOnTriggerDown[2] = "Fire";
   stateTransitionOnNoAmmo[2]      = "NoAmmo";

   stateName[3]                = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3]        = 0.5;
   stateFire[3]                = true;
   stateRecoil[3]              = LightRecoil;
   stateAllowImageChange[3]    = false;
   stateSequence[3]            = "Fire";
   stateSound[3]               = ShockLanceDryFireSound;
   stateScript[3]              = "onFire";

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 2.0;
   stateAllowImageChange[4]      = false;
   stateTransitionOnTimeout[4]   = "Ready";
   stateTransitionOnNotLoaded[4] = "Deactivate";
    stateTransitionOnNoAmmo[4]    = "NoAmmo";
   stateSound[4]               = ShockLanceReloadSound;
   
   stateName[5]                = "Deactivate";
   stateSequence[5]            = "Activate";
   stateDirection[5]           = false;
   stateTimeoutValue[5]        = 1;
   stateTransitionOnLoaded[5]  = "ActivateReady";
   stateTransitionOnTimeout[5] = "Dead";

   stateName[6]               = "Dead";
   stateTransitionOnLoaded[6] = "ActivateReady";

   stateName[7]             = "NoAmmo";
   stateTransitionOnAmmo[7] = "Reload";
   stateSequence[7]         = "NoAmmo";
};

function GShocklanceSKY0::onFire(%this, %obj, %slot)
{
   %muzzlePos = %obj.getMuzzlePoint(%slot);
   %muzzleVec = %obj.getMuzzleVector(%slot);

   %endPos    = VectorAdd(%muzzlePos, VectorScale(%muzzleVec, %this.projectile.extension));

   %damageMasks = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType |
      $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType |
      $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType;

   %everythingElseMask = $TypeMasks::TerrainObjectType |
                         $TypeMasks::InteriorObjectType |
                         $TypeMasks::ForceFieldObjectType |
                         $TypeMasks::StaticObjectType |
                         $TypeMasks::MoveableObjectType |
                         $TypeMasks::DamagableItemObjectType;

   // did I miss anything? players, vehicles, stations, gens, sensors, turrets
   %hit = ContainerRayCast(%muzzlePos, %endPos, %damageMasks | %everythingElseMask, %obj);

   %noDisplay = true;

   if (%hit !$= "0")
   {
      %obj.setEnergyLevel(%obj.getEnergyLevel() - %this.hitEnergy);

      %hitobj = getWord(%hit, 0);
      %hitpos = getWord(%hit, 1) @ " " @ getWord(%hit, 2) @ " " @ getWord(%hit, 3);

      if ( %hitObj.getType() & %damageMasks )
      {
         %impulse = %this.projectile.impulse;
         %smf = %hitObj.getDatablock().shapeMaxForce;

         if(%smf > 0)
            %impulse = %smf * (%impulse / $VImpulseMaxValue);
         else if(%impulse > $VImpulseClampValue)
                %impulse = $VImpulseClampValue;

         %impulseVec = VectorScale(%muzzleVec, %impulse);
         %hitobj.applyImpulse(%hitpos, %impulseVec);
         %obj.playAudio(0, ShockLanceHitSound);

         // This is truly lame, but we need the sourceobject property present...
         %p = new ShockLanceProjectile() {
            dataBlock        = %this.projectile;
            initialDirection = %obj.getMuzzleVector(%slot);
            initialPosition  = %obj.getMuzzlePoint(%slot);
            sourceObject     = %obj;
            sourceSlot       = %slot;
            targetId         = %hit;
         };
         MissionCleanup.add(%p);

         %damageMultiplier = 1.0;

         if(%hitObj.getDataBlock().getClassName() $= "PlayerData")
         {
            // Now we see if we hit from behind...
            %forwardVec = %hitobj.getForwardVector();
            %objDir2D   = getWord(%forwardVec, 0) @ " " @ getWord(%forwardVec,1) @ " " @ "0.0";
            %objPos     = %hitObj.getPosition();
            %dif        = VectorSub(%objPos, %muzzlePos);
            %dif        = getWord(%dif, 0) @ " " @ getWord(%dif, 1) @ " 0";
            %dif        = VectorNormalize(%dif);
            %dot        = VectorDot(%dif, %objDir2D);

            // 120 Deg angle test...
            // 1.05 == 60 degrees in radians
            if (%dot >= mCos(1.05))
            {
               // Rear hit
               %damageMultiplier = 3.0;
               %hitObj.getOwnerClient().rearshot = 1; // z0dd - ZOD, 8/25/02. Added Lance rear shot messages
            }
            // --------------------------------------------------------------
            // z0dd - ZOD, 8/25/02. Added Lance rear shot messages
            else
            {
               %hitObj.getOwnerClient().rearshot = 0;
            }
            // --------------------------------------------------------------
         }

         %totalDamage = %this.Projectile.DirectDamage * %damageMultiplier;
         %hitObj.getDataBlock().damageObject(%hitobj, %p.sourceObject, %hitpos, %totalDamage, $DamageType::ShockLance);

         %noDisplay = false;
      }
   }

   if( %noDisplay )
   {
      // Miss
      %obj.setEnergyLevel(%obj.getEnergyLevel() - %this.missEnergy);
      %obj.playAudio(0, ShockLanceMissSound);

      %p = new ShockLanceProjectile() {
         dataBlock        = %this.projectile;
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
      };
      MissionCleanup.add(%p);

   }
}

// Inherited Defs

function GShocklance::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "";
    %this.ammoUse = 0;
    %this.fireTimeout = 0;
    
    // Reticle data
    %this.reticleTex = "gui/hud_ret_shocklance";
    %this.showReticleFrame = false;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "GShocklance", "Shocklance", "Reach out and torch someone", $VehicleList::Skycutter, $VHardpointSize::Gun, $VHardpointType::Energy);
