// Sound, Projectile, and Explosion blocks
datablock AudioProfile(DevistatorFireSound)
{
   filename    = "fx/weapon/rocketpod_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock LinearProjectileData(DevistatorRocket)
{
//   scale = "1.5 1.5 1.5";
   projectileShapeName = "pyro_missile.dts";
   emitterDelay        = -1;
   directDamage        = 0;
   directDamageType    = $DamageType::Missile;
   hasDamageRadius     = true;
   indirectDamage      = 2.0;
   damageRadius        = 5;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 750;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Explosive;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "RocketExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = WildcatJetEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 300;
   wetVelocity       = 200;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1000;

   sound = "LargeMissileProjectileSound";

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 7.0;
   lightColor  = "0.4 0.15 0.1";
};

// Primary Definition
datablock ShapeBaseImageData(LDevistatorSTA2)
{
   shapeFile = "herc_lrm.dts";
   mountPoint = 0;

   offset = $VehicleHardpoints[$VehicleID::Starhawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 2, "rotation"];

   projectile = DevistatorRocket;
   projectileType = LinearProjectile;
   ammo = "VPDevistatorAmmo";
   updatePilotAmmo = true;
   sharedResourcePool = true;
   pilotHeadTracking = true;
   
   usesEnergy = false;
   useMountEnergy = false;
   fireEnergy = 0.0;
   minEnergy = 0.0;
   useCapacitor = false;
   
   fireTimeout = 250;

   // State transitions
   stateName[0]                        = "Activate";
   stateTransitionOnNotLoaded[0]       = "Dead";
   stateTransitionOnLoaded[0]          = "ActivateReady";

   stateName[1]                        = "ActivateReady";
   stateSequence[1]                    = "Activate";
//   stateSound[1]                       = AssaultTurretActivateSound;
   stateTimeoutValue[1]                = 1.0;
   stateTransitionOnTimeout[1]         = "Ready";
   stateTransitionOnNotLoaded[1]       = "Deactivate";

   stateName[2]                        = "Ready";
   stateTransitionOnNotLoaded[2]       = "Deactivate";
   stateTransitionOnNoAmmo[2]          = "NoAmmo";
   stateTransitionOnTriggerDown[2]     = "Fire";

   stateName[3]                        = "Fire";
   stateSequence[3]                    = "Fire";
   stateTransitionOnTimeout[3]         = "Reload";
   stateTimeoutValue[3]                = 0.25;
   stateFire[3]                        = true;
   stateRecoil[3]                      = LightRecoil;
   stateAllowImageChange[3]            = false;
   stateSound[3]                       = DevistatorFireSound;
   stateScript[3]                      = "onFire";

   stateName[4]                        = "Reload";
   stateSequence[4]                    = "Reload";
   stateTimeoutValue[4]                = 0.25;
   stateAllowImageChange[4]            = false;
   stateSound[4]                       = "";
   stateTransitionOnTimeout[4]         = "Ready";
   //stateTransitionOnNoAmmo[4]          = "NoAmmo";
   stateWaitForTimeout[4]              = true;

   stateName[5]                        = "Deactivate";
   stateDirection[5]                   = false;
   stateSequence[5]                    = "Activate";
   stateTimeoutValue[5]                = 1.0;
   stateTransitionOnLoaded[5]          = "ActivateReady";
   stateTransitionOnTimeout[5]         = "Dead";

   stateName[6]                        = "Dead";
   stateTransitionOnLoaded[6]          = "ActivateReady";
   stateTransitionOnTriggerDown[6]     = "DryFire";

   stateName[7]                        = "DryFire";
   stateSound[7]                       = AssaultMortarDryFireSound;
   stateTimeoutValue[7]                = 1.0;
   stateTransitionOnTimeout[7]         = "NoAmmo";

   stateName[8]                        = "NoAmmo";
   stateSequence[8]                    = "NoAmmo";
   stateTransitionOnAmmo[8]            = "Reload";
   stateTransitionOnTriggerDown[8]     = "DryFire";
};

// Inherited Defs
datablock ShapeBaseImageData(LDevistatorCAT2) : LDevistatorSTA2
{
   shapeFile = "repair_patch.dts";
   pilotHeadTracking = false;
   
   offset = $VehicleHardpoints[$VehicleID::Catapult, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 2, "rotation"];
};

datablock ShapeBaseImageData(LDevistatorCAT3) : LDevistatorSTA2
{
   shapeFile = "repair_patch.dts";
   pilotHeadTracking = false;
   
   offset = $VehicleHardpoints[$VehicleID::Catapult, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 3, "rotation"];
};

function LDevistator::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = true;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 200;
    %this.ammo = "VPDevistatorAmmo";
    %this.ammoUse = 1;
    %this.fireTimeout = 250;
    
    // Reticle data
    %this.reticleTex = "gui/RET_missile";
    %this.showReticleFrame = false;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LDevistator", "Devistator Rocket Pod", "Dumbfire rocket pod, high rate of fire and damage on unshielded targets", $VehicleList::Catapult, $VHardpointSize::Gun, $VHardpointType::Pod);
