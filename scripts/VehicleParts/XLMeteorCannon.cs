// Sound, Projectile, and Explosion blocks
datablock AudioProfile(XLMeteorFireSound)
{
   filename    = "fx/weapon/phasercannon_fire.wav";
   description = AudioAmbient3d;
   preload = true;
};

datablock AudioProfile(XLMeteorReloadSound)
{
   filename    = "fx/misc/defender_designator_tag.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(XLMeteorExpSound)
{
   filename    = "fx/explosion/meteorcannon_exp.wav";
   description = AudioAmbient3d;
   preload = true;
};

datablock ExplosionData(MeteorCannonExplosion) : PlasmaBarrelBoltExplosion
{
   soundProfile   = XLMeteorExpSound;

   shakeCamera = true;
   camShakeFreq = "10.0 9.0 9.0";
   camShakeAmp = "10.0 10.0 10.0";
   camShakeDuration = 0.3;
   camShakeRadius = 7.5;
};

datablock ParticleData(MeteorCannonTrailParticle)
{
    dragCoefficient = 0.0487805;
    gravityCoefficient = -0.25;
    windCoefficient = 0;
    inheritedVelFactor = 0.362903;
    constantAcceleration = 0;
    lifetimeMS = 2177;
    lifetimeVarianceMS = 443;
    useInvAlpha = 0;
    spinRandomMin = -145.161;
    spinRandomMax = 133.065;
    textureName = "flarebase.png";
    times[0] = 0;
    times[1] = 0.354839;
    times[2] = 1;
    colors[0] = "0.149606 0.296000 1.000000 0.451613";
    colors[1] = "0.000000 0.000000 0.000000 0.709677";
    colors[2] = "1.000000 1.000000 1.000000 0.000000";
    sizes[0] = 1.75;
    sizes[1] = 1.35484;
    sizes[2] = 0.8;
};

datablock ParticleEmitterData(MeteorCannonTrailEmitter)
{
    ejectionPeriodMS = 9;
    periodVarianceMS = 0;
    ejectionVelocity = 5.79032;
    velocityVariance = 2.30645;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 13.7903;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "MeteorCannonTrailParticle";
};

datablock LinearFlareProjectileData(VXLMeteorBolt) : BlasterOldBolt
{
   scale             = "3.0 3.0 3.0";
   directDamage        = 20.00;
   directDamageType    = $DamageType::Blaster;

   explosion           = MeteorCannonExplosion;
   splash              = DiscSplash;
   baseEmitter         = MeteorCannonTrailEmitter;
   
   flags               = $Projectile::PlaysHitSound | $Projectile::Energy;
   ticking             = false;
   headshotMultiplier  = 1.0;

   dryVelocity       = 350.0;
   wetVelocity       = 350.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 100;

   numFlares         = 30;
   flareColor        = "0.13 0.41 0.93";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   sound = SentryTurretProjectileSound;

   hasLight    = true;
   lightRadius = 1.5;
   lightColor  = "1.0 0.25 0.25";
};

datablock ShockwaveData(DisplayBlueShockwave)
{
   width = 5.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 15;
   acceleration = -30;
   lifetimeMS = 500;
   height = 0.75;
   verticalCurve = 0.4;
//   is2D = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.3 0.8 1.0 0.75";
   colors[1] = "0.3 0.4 1.0 0.5";
   colors[2] = "0.3 0.2 1.0 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ExplosionData(DisplayBlueExplosion)
{
   soundProfile   = plasmaExpSound;
   shockwave = DisplayBlueShockwave;

   faceViewer = true;
};

datablock LinearFlareProjectileData(DisplayBlueCharge)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.0001;//0.000; --- must be > 0 (ST)
   damageRadius        = 1;
   radiusDamageType    = $DamageType::MB;
   kickBackStrength    = 750;

   explosion           = "DisplayBlueExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

// Primary Definition
datablock ShapeBaseImageData(XLMeteorTUT2)
{
	className = WeaponImage;
   shapeFile = "herc_plasma_right.dts";
   mountPoint = 0;
//   item = PulseBlaster;
   projectile = VXLMeteorBolt;
   projectileType = LinearFlareProjectile;
//	armThread = looksn;

    usesEnergy = true;
	minEnergy = 400; // 300
    fireEnergy = 400;
//    defaultModeFireSound = "LaserFireSound";

   offset = "-0.75 -0.25 -0.25";
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 2, "rotation"];

   fireTimeout = 250;

   // State transitions
   stateName[0]                  = "Activate";
   stateTransitionOnNotLoaded[0] = "Dead";
   stateTransitionOnLoaded[0]    = "ActivateReady";

   stateName[1]                  = "ActivateReady";
   stateSequence[1]              = "ambient";
   stateTimeoutValue[1]          = 1;
   stateTransitionOnTimeout[1]   = "Ready";
   stateTransitionOnNotLoaded[1] = "Deactivate";
   stateTransitionOnNoAmmo[1]    = "NoAmmo";

   stateName[2]                    = "Ready";
   stateTransitionOnNotLoaded[2]   = "Deactivate";
   stateTransitionOnTriggerDown[2] = "Fire";
   stateTransitionOnNoAmmo[2]      = "NoAmmo";

   stateName[3]                = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3]        = 5.5;
   stateFire[3]                = true;
   stateRecoil[3]              = LightRecoil;
   stateAllowImageChange[3]    = false;
   stateSequence[3]            = "Fire";
   stateSound[3]               = XLMeteorFireSound;
   stateScript[3]              = "onFire";

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 0.5;
   stateAllowImageChange[4]      = false;
   stateSound[4]               = XLMeteorReloadSound;
   stateSequence[4]              = "Reload";
   stateTransitionOnTimeout[4]   = "Ready";
   stateTransitionOnNotLoaded[4] = "Deactivate";
    stateTransitionOnNoAmmo[4]    = "NoAmmo";

   stateName[5]                = "Deactivate";
   stateSequence[5]            = "Activate";
   stateDirection[5]           = false;
   stateTimeoutValue[5]        = 1;
   stateTransitionOnLoaded[5]  = "ActivateReady";
   stateTransitionOnTimeout[5] = "Dead";

   stateName[6]               = "Dead";
   stateTransitionOnLoaded[6] = "ActivateReady";

   stateName[7]             = "NoAmmo";
   stateTransitionOnAmmo[7] = "Reload";
   stateSequence[7]         = "NoAmmo";
};

function XLMeteorTUT2::onFire(%data, %obj, %slot)
{
     %p = Parent::onFire(%data, %obj, %slot);

     if(%p)
        createRemoteProjectile("LinearFlareProjectile", "DisplayBlueCharge", %p.initialDirection, %p.position, 0, %p.instigator);
}

// Inherited Defs
function XLMeteor::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "";
    %this.ammoUse = 0;
    %this.fireTimeout = 3000;
    
    // Reticle data
    %this.reticleTex = "gui/RET_Plasma";
    %this.showReticleFrame = true;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "XLMeteor", "Meteor Cannon", "High output energy weapon designed to punch vehicles out of the sky", $VehicleList::Guillotine | $VehicleList::Leviathan, $VHardpointSize::Siege, $VHardpointType::Energy);
