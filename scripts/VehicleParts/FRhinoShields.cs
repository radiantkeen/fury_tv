// Rhino Shield Defs
$SD::Rhino::maxStrength = 5750;
$SD::Rhino::recDelay = 20;
$SD::Rhino::resTime = 40;
$SD::Rhino::recRate = 560;

// Definition Objects
function FRhinoStandard::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";

    // Shield settings
    %this.maxStrength = $SD::Rhino::maxStrength;
    %this.recDelay = $SD::Rhino::recDelay;
    %this.resTime = $SD::Rhino::resTime;
    %this.recRate = $SD::Rhino::recRate;
    %this.powerDrainPct = $ShieldDefs::StandardPowerDrain;
    %this.bleedthrough = $ShieldDefs::StandardBleedthrough;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = %this.maxStrength;
    %this.deltaRegen = ($VehicleListData[$VehicleID::Rhino, "block"].rechargeRate * $g_TickTime) * -(%this.powerDrainPct / 100);
}

function FRhinoStandard::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, %this.maxStrength, mFloor(%this.recDelay * 1000), mFloor(%this.resTime * 1000), %this.recRate / $g_TickTime, %this.powerDrainPct / 100, %this.bleedthrough / 100, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "FRhinoStandard", "Standard Shields", %this.maxStrength @ " HP, Recharge delay: " @ %this.recDelay @ " sec, Restart delay: " @ %this.resTime @ " sec, Bleedthrough: " @ %this.bleedthrough @ "%, Power Drain: " @ %this.powerDrainPct @ "%", $VehicleList::Rhino, $VHardpointSize::Internal, $VHardpointType::Anything);

function FRhinoLight::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";

    // Shield settings
    %this.maxStrength = mCeil($SD::Rhino::maxStrength * 0.5);
    %this.recDelay = $SD::Rhino::recDelay * 0.75;
    %this.resTime = $SD::Rhino::resTime * 0.5;
    %this.recRate = $SD::Rhino::recRate * 0.5;
    %this.powerDrainPct = $ShieldDefs::LightPowerDrain;
    %this.bleedthrough = $ShieldDefs::LightBleedthrough;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = %this.maxStrength;
    %this.deltaRegen = ($VehicleListData[$VehicleID::Rhino, "block"].rechargeRate * $g_TickTime) * -(%this.powerDrainPct / 100);
}

function FRhinoLight::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, %this.maxStrength, mFloor(%this.recDelay * 1000), mFloor(%this.resTime * 1000), %this.recRate / $g_TickTime, %this.powerDrainPct / 100, %this.bleedthrough / 100, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "FRhinoLight", "Light Shields", %this.maxStrength @ " HP, Recharge delay: " @ %this.recDelay @ " sec, Restart delay: " @ %this.resTime @ " sec, Bleedthrough: " @ %this.bleedthrough @ "%, Power Drain: " @ %this.powerDrainPct @ "%", $VehicleList::Rhino, $VHardpointSize::Internal, $VHardpointType::Anything);

function FRhinoRegenerative::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";

    // Shield settings
    %this.maxStrength = mCeil($SD::Rhino::maxStrength * 0.333);
    %this.recDelay = $SD::Rhino::recDelay * 0.5;
    %this.resTime = $SD::Rhino::resTime * 0.333;
    %this.recRate = $SD::Rhino::recRate * 0.75;
    %this.powerDrainPct = $ShieldDefs::RegenerativePowerDrain;
    %this.bleedthrough = $ShieldDefs::RegenerativeBleedthrough;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = %this.maxStrength;
    %this.deltaRegen = ($VehicleListData[$VehicleID::Rhino, "block"].rechargeRate * $g_TickTime) * -(%this.powerDrainPct / 100);
}

function FRhinoRegenerative::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, %this.maxStrength, mFloor(%this.recDelay * 1000), mFloor(%this.resTime * 1000), %this.recRate / $g_TickTime, %this.powerDrainPct / 100, %this.bleedthrough / 100, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "FRhinoRegenerative", "Regenerative Shields", %this.maxStrength @ " HP, Recharge delay: " @ %this.recDelay @ " sec, Restart delay: " @ %this.resTime @ " sec, Bleedthrough: " @ %this.bleedthrough @ "%, Power Drain: " @ %this.powerDrainPct @ "%", $VehicleList::Rhino, $VHardpointSize::Internal, $VHardpointType::Anything);

function FRhinoHardened::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";

    // Shield settings
    %this.maxStrength = $SD::Rhino::maxStrength;
    %this.recDelay = $SD::Rhino::recDelay * 1.5;
    %this.resTime = $SD::Rhino::resTime * 1.25;
    %this.recRate = $SD::Rhino::recRate;
    %this.powerDrainPct = $ShieldDefs::HardenedPowerDrain;
    %this.bleedthrough = $ShieldDefs::HardenedBleedthrough;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = %this.maxStrength;
    %this.deltaRegen = ($VehicleListData[$VehicleID::Rhino, "block"].rechargeRate * $g_TickTime) * -(%this.powerDrainPct / 100);
}

function FRhinoHardened::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, %this.maxStrength, mFloor(%this.recDelay * 1000), mFloor(%this.resTime * 1000), %this.recRate / $g_TickTime, %this.powerDrainPct / 100, %this.bleedthrough / 100, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "FRhinoHardened", "Hardened Shields", %this.maxStrength @ " HP, Recharge delay: " @ %this.recDelay @ " sec, Restart delay: " @ %this.resTime @ " sec, Bleedthrough: " @ %this.bleedthrough @ "%, Power Drain: " @ %this.powerDrainPct @ "%", $VehicleList::Rhino, $VHardpointSize::Internal, $VHardpointType::Anything);

function FRhinoCovariant::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";

    // Shield settings
    %this.maxStrength = mCeil($SD::Rhino::maxStrength * 1.5);
    %this.recDelay = $SD::Rhino::recDelay;
    %this.resTime = $SD::Rhino::resTime;
    %this.recRate = $SD::Rhino::recRate;
    %this.powerDrainPct = $ShieldDefs::CovariantPowerDrain;
    %this.bleedthrough = $ShieldDefs::CovariantBleedthrough;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = %this.maxStrength;
    %this.deltaRegen = ($VehicleListData[$VehicleID::Rhino, "block"].rechargeRate * $g_TickTime) * -(%this.powerDrainPct / 100);
}

function FRhinoCovariant::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, %this.maxStrength, mFloor(%this.recDelay * 1000), mFloor(%this.resTime * 1000), %this.recRate / $g_TickTime, %this.powerDrainPct / 100, %this.bleedthrough / 100, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "FRhinoCovariant", "Covariant Shields", %this.maxStrength @ " HP, Recharge delay: " @ %this.recDelay @ " sec, Restart delay: " @ %this.resTime @ " sec, Bleedthrough: " @ %this.bleedthrough @ "%, Power Drain: " @ %this.powerDrainPct @ "%", $VehicleList::Rhino, $VHardpointSize::Internal, $VHardpointType::Anything);
