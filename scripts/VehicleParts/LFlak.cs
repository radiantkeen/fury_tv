// Sound, Projectile, and Explosion blocks
datablock AudioProfile(LFlakFireSound)
{
   filename    = "fx/weapon/flakturret_fire.wav";
   description = AudioAmbient3d;
   preload = true;
};

datablock ShockwaveData(LFlakShockwave)
{
   width = 5.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 15;
   acceleration = -30;
   lifetimeMS = 500;
   height = 0.75;
   verticalCurve = 0.4;
   is2D = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1.0 0.773 0.493 0.75";
   colors[1] = "1.0 0.773 0.493 0.5";
   colors[2] = "0.5 0.36 0.247 0.0";

   mapToTerrain = false;
   orientToNormal = false;
   renderBottom = true;
};

datablock ExplosionData(LFlakShellExplosion) : FlakShellExplosion
{
   shockwave = LFlakShockwave;
};

datablock LinearFlareProjectileData(LFlakBurst)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 1.0;
   damageRadius        = 30.0;
   radiusDamageType    = $DamageType::Flak;
   kickBackStrength    = 500;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Explosive;
   ticking             = false;
   headshotMultiplier  = 1.25;

   sound               = GrenadeProjectileSound;
   explosion           = "LFlakShellExplosion";
   underwaterExplosion = "UnderwaterGrenadeExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 32;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock LinearProjectileData(LFlakBullet) : FlakBullet
{
   scale = "1.5 1.5 1.5";
   projectileShapeName = "mortar_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 0;
   directDamageType    = $DamageType::Flak;
   hasDamageRadius     = true;
   indirectDamage      = 1.0;
   damageRadius        = 30;
   radiusDamageType    = $DamageType::Flak;
   kickBackStrength    = 500;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Explosive;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "LFlakShellExplosion";

   dryVelocity       = 300;
   wetVelocity       = 200;
   fizzleTimeMS      = 500;
   lifetimeMS        = 600;
};

function LFlakBullet::onTick(%this, %proj)
{
    Parent::onTick(%this, %proj);

    InitContainerRadiusSearch(%proj.position, %this.damageRadius * 0.675, $TypeMasks::VehicleObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::ProjectileObjectType);

    while((%int = ContainerSearchNext()) != 0)
    {
        if(%int.client.team == %proj.instigator.team || %int.team == %proj.instigator.team)
            continue;

        if(%int.getType() & $TypeMasks::ProjectileObjectType)
        {
            if(%int.getDatablock().getName() $= "FlareGrenadeProj")
                return %this.detonate(%proj);
        }
        else
        {
            if(%int.getHeat() > 0.75)
                return %this.detonate(%proj);
        }
    }
}

function LFlakBullet::detonate(%this, %proj)
{
    transformProjectile(%proj, "LinearFlareProjectile", "LFlakBurst", %proj.position, %proj.initialDirection);
}

datablock DebrisData(FlakShellDebris)
{
   shapeName = "grenade_projectile.dts";

   lifetime = 3.0;

   minSpinSpeed = 300.0;
   maxSpinSpeed = 400.0;

   elasticity = 0.5;
   friction = 0.2;

   numBounces = 3;

   fade = true;
   staticOnMaxBounce = true;
   snapOnMaxBounce = true;
};

// Primary Definition
datablock ShapeBaseImageData(LFlakSSH2)
{
   className = WeaponImage;
   shapeFile = "MiniCannon2.dts";
   mountPoint = "0";
   offset = "-0.5 2.25 -1.5";
   rotation = "1 0 0 0";

   ammo = "VLFlakAmmo";
   updatePilotAmmo = true;
   sharedResourcePool = true;
   projectile = LFlakBullet;
   projectileType = LinearProjectile;
   emap = true;

   casing              = FlakShellDebris;
   shellExitDir        = "1.0 0.3 1.0";
   shellExitOffset     = "0.15 -0.56 -0.1";
   shellExitVariance   = 15.0;
   shellVelocity       = 6.0;

   projectileSpread = 12.0;

   fireTimeout = 167;
   
   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   //
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
   stateSequence[4]            = "Fire";
   stateSequenceRandomFlash[4] = true;
   stateSpinThread[4]       = FullSpeed;
   stateSound[4]            = LFlakFireSound;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateEjectShell[4]       = true;
   //
   stateTimeoutValue[4]          = 0.2;
   stateTransitionOnTimeout[4]   = "Reload";

   //--------------------------------------
   stateName[5]       = "Spindown";
//   stateSound[5]      = ChaingunSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 0.5;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = ChaingunSpinDownSound;
   stateSpinThread[6] = SpinDown;
   //
   stateTimeoutValue[6]        = 0.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ChaingunDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateScript[7]           = "onDryFire";
   stateTransitionOnTimeout[7] = "NoAmmo";
   
   //--------------------------------------
   stateName[8]             = "Reload";
   stateSequence[8]            = "";
   stateSpinThread[8]       = FullSpeed;
   stateAllowImageChange[8] = false;
   stateTimeoutValue[8]          = 0.133;
   stateTransitionOnTimeout[8]   = "Fire";
   stateTransitionOnTriggerUp[8] = "Spindown";
   stateTransitionOnNoAmmo[8]    = "EmptySpindown";
};

// Inherited Defs
datablock ShapeBaseImageData(LFlakSTA2) : LFlakSSH2
{
   pilotHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::Starhawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 2, "rotation"];
};

datablock ShapeBaseImageData(LFlakTUS2) : LFlakSSH2
{
   shapeFile = "pyro_fbigbirtha.dts";
   
   offset = "0 1 0";
   rotation = "1 0 0 180";
};

datablock ShapeBaseImageData(LFlakTUT4) : LFlakSSH2
{
   shapeFile = "pyro_fbigbirtha.dts";
   
   mountPoint = 1;
   
   offset = "0 0 0";
   rotation = "1 0 0 180";
};

datablock ShapeBaseImageData(LFlakRET2) : LFlakSSH2
{
   pilotHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::Retaliator, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Retaliator, 2, "rotation"];
};

datablock ShapeBaseImageData(LFlakSTR2) : LFlakSSH2
{
   pilotHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::Stryker, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Stryker, 2, "rotation"];
};

datablock ShapeBaseImageData(LFlakTPD2) : LFlakSSH2
{
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 2, "rotation"];
};

datablock ShapeBaseImageData(LFlakTPD3) : LFlakSSH2
{
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 3, "rotation"];
};

function LFlak::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = true;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 200;
    %this.ammo = "VLFlakAmmo";
    %this.ammoUse = 1;
    %this.fireTimeout = 150;
    
    // Reticle data
    %this.reticleTex = "gui/RET_grenade";
    %this.showReticleFrame = true;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LFlak", "Heavy Flak Cannon", "Explodes near vehicles, low-medium damage, highly concussive blasts", $VehicleList::SuperShrike | $VehicleList::Starhawk | $VehicleList::Retaliator | $VehicleList::HoverJeep | $VehicleList::Stryker | $VehicleList::Wolfhound | $VehicleList::Annihilator | $VehicleList::Goodship | $VehicleList::Leviathan, $VHardpointSize::Cannon, $VHardpointType::Ballistic);
