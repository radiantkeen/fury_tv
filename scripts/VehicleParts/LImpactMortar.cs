// Sound, Projectile, and Explosion blocks
datablock GrenadeProjectileData(ImpactMortarShot) : MortarShot
{
   projectileShapeName = "mortar_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 6.0;
   damageRadius        = 20.0; // z0dd - ZOD, 8/13/02. Was 20.0
   radiusDamageType    = $DamageType::Mortar;
   kickBackStrength    = 5000;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Explosive;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "MortarExplosion";
   underwaterExplosion = "UnderwaterMortarExplosion";
   velInheritFactor    = 1.0;
   splash              = MortarSplash;
   depthTolerance      = 10.0; // depth at which it uses underwater explosion

   baseEmitter         = ImpactMortarSmokeEmitter;
   bubbleEmitter       = MortarBubbleEmitter;

   grenadeElasticity = 0.1;
   grenadeFriction   = 0.4;
   armingDelayMS     = 0; // z0dd - ZOD, 4/14/02. Was 2000

   gravityMod        = 0.9;  // z0dd - ZOD, 5/18/02. Make mortar projectile heavier, less floaty
   muzzleVelocity    = 125; // z0dd - ZOD, 8/13/02. More velocity to compensate for higher gravity. Was 63.7
   drag              = 0.1;
   sound	     = MortarProjectileSound;

   hasLight    = true;
   lightRadius = 4;
   lightColor  = "0.05 0.2 0.05";

   hasLightUnderwaterColor = true;
   underWaterLightColor = "0.05 0.075 0.2";
};

// Primary Definition
datablock ShapeBaseImageData(LImpactMortarSSH2)
{
   className = WeaponImage;
   shapeFile = "turret_mortar_large.dts";
   mountPoint = "0";
   offset = "-0.5 2.25 -1.5";
   rotation = "1 0 0 0";

   ammo = "VLImpactMortarAmmo";
   updatePilotAmmo = true;
   sharedResourcePool = true;
   projectile = ImpactMortarShot;
   projectileType = GrenadeProjectile;
   emap = true;

   fireTimeout = 1000;
   
   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Deploy";
//   stateSound[0] = MortarSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";
   //stateSound[2] = MortarIdleSound;

   stateName[3] = "Fire";
   stateSequence[3] = "Recoil";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 1.2;
   stateFire[3] = true;
   stateRecoil[3] = LightRecoil;
   stateAllowImageChange[3] = false;
   stateScript[3] = "onFire";
   stateSound[3] = MortarFireSound;

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 0.8;
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";
   stateSound[4] = MortarReloadSound;

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
   stateSound[6]      = MortarDryFireSound;
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";
};

// Inherited Defs
datablock ShapeBaseImageData(LImpactMortarSTA2) : LImpactMortarSSH2
{
   pilotHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::Starhawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 2, "rotation"];
};

datablock ShapeBaseImageData(LImpactMortarGUI2) : LImpactMortarSSH2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Guillotine, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Guillotine, 2, "rotation"];
};

datablock ShapeBaseImageData(LImpactMortarTUT4) : LImpactMortarSSH2
{
   pilotHeadTracking = true;

   mountPoint = 1;

   offset = "0 0 0";
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 4, "rotation"];
};

datablock ShapeBaseImageData(LImpactMortarFIB2) : LImpactMortarSSH2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 2, "rotation"];
};

datablock ShapeBaseImageData(LImpactMortarFIB4) : LImpactMortarSSH2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 4, "rotation"];
};

datablock ShapeBaseImageData(LImpactMortarFIB5) : LImpactMortarSSH2
{
//   shapeFile = "herc_ac_left.dts";

   offset = $VehicleHardpoints[$VehicleID::Firebird, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 5, "rotation"];
};

datablock ShapeBaseImageData(LImpactMortarTUS2) : LImpactMortarSSH2
{
//   offset = "-1 1 -0.25";
   offset = $VehicleHardpoints[$VehicleID::TurretPole, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretPole, 2, "rotation"];
};

datablock ShapeBaseImageData(LImpactMortarTOM2) : LImpactMortarSSH2
{
   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 2, "rotation"];
};

datablock ShapeBaseImageData(LImpactMortarTOM3) : LImpactMortarSSH2
{
//   shapeFile = "herc_ac_left.dts";
   
   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 3, "rotation"];
};

datablock ShapeBaseImageData(LImpactMortarRET2) : LImpactMortarSSH2
{
   pilotHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::Retaliator, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Retaliator, 2, "rotation"];
};

datablock ShapeBaseImageData(LImpactMortarSTR2) : LImpactMortarSSH2
{
   pilotHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::Stryker, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Stryker, 2, "rotation"];
};

datablock ShapeBaseImageData(LImpactMortarTPD2) : LImpactMortarSSH2
{
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 2, "rotation"];
};

datablock ShapeBaseImageData(LImpactMortarTPD3) : LImpactMortarSSH2
{
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 3, "rotation"];
};

function LImpactMortar::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = true;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 60;
    %this.ammo = "VLImpactMortarAmmo";
    %this.ammoUse = 1;
    %this.fireTimeout = 1000;
    
    // Reticle data
    %this.reticleTex = "gui/RET_mortor";
    %this.showReticleFrame = true;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LImpactMortar", "Impact Mortar Launcher", "High explosive, high knockback mortar launcher, arcs when fired", $VehicleList::Firebird | $VehicleList::Tomahawk | $VehicleList::Guillotine | $VehicleList::Leviathan | $VehicleList::SuperShrike | $VehicleList::Starhawk | $VehicleList::Firebird | $VehicleList::Retaliator | $VehicleList::HoverJeep | $VehicleList::Stryker | $VehicleList::Wolfhound | $VehicleList::Annihilator | $VehicleList::Goodship | $VehicleList::Scorpion, $VHardpointSize::Cannon, $VHardpointType::Ballistic);
