// Sound, Projectile, and Explosion blocks
datablock AudioProfile(GNullifierFireSound)
{
   filename    = "fx/weapon/phasedblaster_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock LinearFlareProjectileData(GNullifierBolt) : NullifierBolt
{
   damageRadius        = 2.0;
   indirectDamage      = 1.25;
   radiusDamageType  = $DamageType::Phaser;
   kickBackStrength    = 0.0;

   flags               = $Projectile::PlaysHitSound | $Projectile::Nullifier;
   ticking             = false;
   headshotMultiplier  = 1.0;
};

// Primary Definition
datablock ShapeBaseImageData(GNullifierVIP2)
{
   className = WeaponImage;
   shapeFile = "TR2weapon_shocklance.dts";

   projectileType = LinearFlareProjectile;
   projectile = GNullifierBolt;
   mountPoint = 0;

   offset = $VehicleHardpoints[$VehicleID::Viper, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Viper, 2, "rotation"];

   usesEnergy = true;
   useMountEnergy = true;
   sharedResourcePool = true;
   ammo = "";
   updatePilotAmmo = false;

//   projectileSpread = 1.0;
   minEnergy = 75;
   fireEnergy = 70;
   fireTimeout = 250;

   // State transitions
   stateName[0]                  = "Activate";
   stateTransitionOnNotLoaded[0] = "Dead";
   stateTransitionOnLoaded[0]    = "ActivateReady";

   stateName[1]                  = "ActivateReady";
   stateSequence[1]              = "Deploy";
   stateTimeoutValue[1]          = 1;
   stateTransitionOnTimeout[1]   = "Ready";
   stateTransitionOnNotLoaded[1] = "Deactivate";
   stateTransitionOnNoAmmo[1]    = "NoAmmo";

   stateName[2]                    = "Ready";
   stateTransitionOnNotLoaded[2]   = "Deactivate";
   stateTransitionOnTriggerDown[2] = "Fire";
   stateTransitionOnNoAmmo[2]      = "NoAmmo";

   stateName[3]                = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3]        = 0.4;
   stateFire[3]                = true;
   stateRecoil[3]              = LightRecoil;
   stateAllowImageChange[3]    = false;
   stateSequence[3]            = "Fire";
   stateSound[3]               = GNullifierFireSound;
   stateScript[3]              = "onFire";

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 0.1;
   stateAllowImageChange[4]      = false;
   stateTransitionOnTimeout[4]   = "Ready";
   stateTransitionOnNotLoaded[4] = "Deactivate";
   stateTransitionOnNoAmmo[4]    = "NoAmmo";

   stateName[5]                = "Deactivate";
   stateSequence[5]            = "Activate";
   stateDirection[5]           = false;
   stateTimeoutValue[5]        = 1;
   stateTransitionOnLoaded[5]  = "ActivateReady";
   stateTransitionOnTimeout[5] = "Dead";

   stateName[6]               = "Dead";
   stateTransitionOnLoaded[6] = "ActivateReady";

   stateName[7]             = "NoAmmo";
   stateTransitionOnAmmo[7] = "Reload";
   stateSequence[7]         = "NoAmmo";
};

// Inherited Defs
datablock ShapeBaseImageData(GNullifierSKY0) : GNullifierVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Skycutter, 0, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Skycutter, 0, "rotation"];
};

datablock ShapeBaseImageData(GNullifierAIR2) : GNullifierVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Airhawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Airhawk, 2, "rotation"];
};

datablock ShapeBaseImageData(GNullifierAIR3) : GNullifierVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Airhawk, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Airhawk, 3, "rotation"];
};

datablock ShapeBaseImageData(GNullifierTUT4) : GNullifierVIP2
{
   mountPoint = 1;
   
   offset = $VehicleHardpoints[$VehicleID::TankTurret, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 4, "rotation"];
};

datablock ShapeBaseImageData(GNullifierTUT5) : GNullifierVIP2
{
   mountPoint = 1;
   
   offset = $VehicleHardpoints[$VehicleID::TankTurret, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 5, "rotation"];
};

datablock ShapeBaseImageData(GNullifierTUD4) : GNullifierVIP2
{
   offset = $VehicleHardpoints[$VehicleID::TurretPole, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretPole, 4, "rotation"];
};

datablock ShapeBaseImageData(GNullifierTUD5) : GNullifierVIP2
{
   offset = $VehicleHardpoints[$VehicleID::TurretPole, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretPole, 5, "rotation"];
};

datablock ShapeBaseImageData(GNullifierPGX2) : GNullifierVIP2
{
   offset = $VehicleHardpoints[$VehicleID::PyroGX, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::PyroGX, 2, "rotation"];
};

datablock ShapeBaseImageData(GNullifierPGX3) : GNullifierVIP2
{
   offset = $VehicleHardpoints[$VehicleID::PyroGX, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::PyroGX, 3, "rotation"];
};

datablock ShapeBaseImageData(GNullifierSSH4) : GNullifierVIP2
{
   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 4, "rotation"];
};

datablock ShapeBaseImageData(GNullifierSSH5) : GNullifierVIP2
{
   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 5, "rotation"];
};

datablock ShapeBaseImageData(GNullifierSHR2) : GNullifierVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 2, "rotation"];
};

datablock ShapeBaseImageData(GNullifierSHR3) : GNullifierVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 3, "rotation"];
};

datablock ShapeBaseImageData(GNullifierSHR4) : GNullifierVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 4, "rotation"];
};

datablock ShapeBaseImageData(GNullifierSHR5) : GNullifierVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 5, "rotation"];
};

datablock ShapeBaseImageData(GNullifierSHX2) : GNullifierVIP2
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 2, "rotation"];
};

datablock ShapeBaseImageData(GNullifierSHX3) : GNullifierVIP2
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 3, "rotation"];
   
   stateSound[3]            = "";
};

datablock ShapeBaseImageData(GNullifierSHX4) : GNullifierVIP2
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 4, "rotation"];
};

datablock ShapeBaseImageData(GNullifierSHX5) : GNullifierVIP2
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 5, "rotation"];

   stateSound[3]            = "";
};

datablock ShapeBaseImageData(GNullifierSTA4) : GNullifierVIP2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Starhawk, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 4, "rotation"];
};

datablock ShapeBaseImageData(GNullifierSTA5) : GNullifierVIP2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Starhawk, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 5, "rotation"];
};

datablock ShapeBaseImageData(GNullifierFIB6) : GNullifierVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 6, "rotation"];
};

datablock ShapeBaseImageData(GNullifierFIB7) : GNullifierVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 7, "rotation"];
};

datablock ShapeBaseImageData(GNullifierJEP2) : GNullifierVIP2
{
   offset = $VehicleHardpoints[$VehicleID::HoverJeep, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::HoverJeep, 2, "rotation"];
};

datablock ShapeBaseImageData(GNullifierJEP3) : GNullifierVIP2
{
   offset = $VehicleHardpoints[$VehicleID::HoverJeep, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::HoverJeep, 3, "rotation"];
};

datablock ShapeBaseImageData(GNullifierUBT2) : GNullifierVIP2
{
   shapeFile = "weapon_shocklance.dts";
   mountPoint = 2; // L

   offset = "0.0 -0.2 -0.1";
   rotation = $VehicleHardpoints[$VehicleID::Underbelly, 2, "rotation"];
};

datablock ShapeBaseImageData(GNullifierUBT3) : GNullifierVIP2
{
   shapeFile = "weapon_shocklance.dts"; // turret_chopper_barrel
   mountPoint = 3; // R

   offset = "0.0 -0.2 -0.1";
   rotation = $VehicleHardpoints[$VehicleID::Underbelly, 3, "rotation"];
};

datablock ShapeBaseImageData(GNullifierTPQ4) : GNullifierVIP2
{
   scorpionHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 4, "rotation"];
};

datablock ShapeBaseImageData(GNullifierTPQ5) : GNullifierVIP2
{
   scorpionHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 5, "rotation"];
};

datablock ShapeBaseImageData(GNullifierTPQ6) : GNullifierVIP2
{
   scorpionHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 6, "rotation"];
};

datablock ShapeBaseImageData(GNullifierTPQ7) : GNullifierVIP2
{
   scorpionHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 7, "rotation"];
};

function GNullifier::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "";
    %this.ammoUse = 0;
    %this.fireTimeout = 250;
    
    // Reticle data
    %this.reticleTex = "gui/RET_elf";
    %this.showReticleFrame = false;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "GNullifier", "Nullifier", "Deals significant damage to shields, and none to armor", $VehicleList::Guillotine | $VehicleList::Starhawk | $VehicleList::Firebird | $VehicleList::Skycutter | $VehicleList::Wildcat | $VehicleList::Leviathan | $VehicleList::ShrikeX | $VehicleList::PyroGX | $VehicleList::Shrike | $VehicleList::SuperShrike | $VehicleList::HoverJeep | $VehicleList::Tomahawk | $VehicleList::Retaliator | $VehicleList::Stryker | $VehicleList::Annihilator | $VehicleList::Wolfhound | $VehicleList::Airhawk | $VehicleList::HoverJeep | $VehicleList::Stryker | $VehicleList::Leviathan | $VehicleList::Scorpion, $VHardpointSize::Gun, $VHardpointType::Energy);
