// Sound, Projectile, and Explosion blocks
datablock AudioProfile(GDisruptorFireSound)
{
   filename    = "fx/weapon/heavy_blaster_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock ParticleData(GDisruptorExplosionParticle1)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/crescent4";
   colors[0] = "0.9 0.7 0.1 1.0";
   colors[1] = "0.9 0.7 0.1 1.0";
   colors[2] = "0.9 0.0 0.0 0.0";
   sizes[0]      = 2.0;
   sizes[1]      = 2.5;
   sizes[2]      = 3.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(GDisruptorExplosionEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 12;
   velocityVariance = 4;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 135;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "GDisruptorExplosionParticle1";
};

datablock ParticleData(GDisruptorExplosionParticle2)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/blasterHit";
   colors[0] = "0.9 0.65 0.1 1.0";
   colors[1] = "0.9 0.65 0.1 0.5";
   colors[2] = "0.9 0.65 0.0 0.0";
   sizes[0]      = 0.9;
   sizes[1]      = 1.5;
   sizes[2]      = 2.25;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(GDisruptorExplosionEmitter2)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 5;
   velocityVariance = 1;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 90;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = false;
   lifetimeMS       = 200;
   particles = "GDisruptorExplosionParticle2";
};

datablock ExplosionData(GDisruptorExplosion)
{
   soundProfile   = MDisruptorExpSound;
   emitter[0]     = GDisruptorExplosionEmitter;
   emitter[1]     = GDisruptorExplosionEmitter2;
};

datablock LinearFlareProjectileData(VGDisruptorBolt) : BlasterOldBolt
{
   scale               = "2.5 2.5 2.5";
   directDamage        = 2.5;
   directDamageType    = $DamageType::Blaster;

   explosion           = "GDisruptorExplosion";

   flags               = $Projectile::PlaysHitSound | $Projectile::Energy;
   ticking             = false;
   headshotMultiplier  = 1.0;

   dryVelocity       = 250.0;
   wetVelocity       = 250.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 100;

   numFlares         = 15;
   size              = 0.8;
   flareColor        = "0.9 0.7 0.075";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   sound = SentryTurretProjectileSound;

   hasLight    = true;
   lightRadius = 1.5;
   lightColor  = "1.0 0.25 0.25";
};

// Primary Definition
datablock ShapeBaseImageData(GDisruptorVIP2)
{
   className = WeaponImage;
   shapeFile = "weapon_energy1.dts";

   projectileType = LinearFlareProjectile;
   projectile = VGDisruptorBolt;
   mountPoint = 0;

   offset = $VehicleHardpoints[$VehicleID::Viper, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Viper, 2, "rotation"];

   usesEnergy = true;
   useMountEnergy = true;
   sharedResourcePool = true;

//   projectileSpread = 1.0;
   minEnergy = 50;
   fireEnergy = 45;
   fireTimeout = 550;

   // State transitions
   stateName[0]                  = "Activate";
   stateTransitionOnNotLoaded[0] = "Dead";
   stateTransitionOnLoaded[0]    = "ActivateReady";

   stateName[1]                  = "ActivateReady";
   stateSequence[1]              = "Deploy";
   stateTimeoutValue[1]          = 1;
   stateTransitionOnTimeout[1]   = "Ready";
   stateTransitionOnNotLoaded[1] = "Deactivate";
   stateTransitionOnNoAmmo[1]    = "NoAmmo";

   stateName[2]                    = "Ready";
   stateTransitionOnNotLoaded[2]   = "Deactivate";
   stateTransitionOnTriggerDown[2] = "Fire";
   stateTransitionOnNoAmmo[2]      = "NoAmmo";

   stateName[3]                = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3]        = 0.55;
   stateFire[3]                = true;
   stateRecoil[3]              = LightRecoil;
   stateAllowImageChange[3]    = false;
   stateSequence[3]            = "Fire";
   stateSound[3]               = GDisruptorFireSound;
   stateScript[3]              = "onFire";

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 0.55;
   stateAllowImageChange[4]      = false;
   stateTransitionOnTimeout[4]   = "Ready";
   stateTransitionOnNotLoaded[4] = "Deactivate";
   stateTransitionOnNoAmmo[4]    = "NoAmmo";

   stateName[5]                = "Deactivate";
   stateSequence[5]            = "Activate";
   stateDirection[5]           = false;
   stateTimeoutValue[5]        = 1;
   stateTransitionOnLoaded[5]  = "ActivateReady";
   stateTransitionOnTimeout[5] = "Dead";

   stateName[6]               = "Dead";
   stateTransitionOnLoaded[6] = "ActivateReady";

   stateName[7]             = "NoAmmo";
   stateTransitionOnAmmo[7] = "Reload";
   stateSequence[7]         = "NoAmmo";
};

// Inherited Defs
datablock ShapeBaseImageData(GDisruptorSKY0) : GDisruptorVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Skycutter, 0, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Skycutter, 0, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorAIR2) : GDisruptorVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Airhawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Airhawk, 2, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorAIR3) : GDisruptorVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Airhawk, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Airhawk, 3, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorTUT4) : GDisruptorVIP2
{
   shapeFile = "weapon_energy.dts";
   mountPoint = 1;
   
   offset = $VehicleHardpoints[$VehicleID::TankTurret, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 4, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorTUT5) : GDisruptorVIP2
{
   shapeFile = "weapon_energy.dts";
   mountPoint = 1;
   
   offset = $VehicleHardpoints[$VehicleID::TankTurret, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 5, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorTUD4) : GDisruptorVIP2
{
   shapeFile = "weapon_energy.dts";
   
   offset = "0.3 1.2 0.1";
   rotation = $VehicleHardpoints[$VehicleID::TurretPole, 4, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorTUD5) : GDisruptorVIP2
{
   shapeFile = "weapon_energy.dts";
   
   offset = "-0.3 1.2 0.1";
   rotation = $VehicleHardpoints[$VehicleID::TurretPole, 5, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorPGX2) : GDisruptorVIP2
{
   offset = $VehicleHardpoints[$VehicleID::PyroGX, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::PyroGX, 2, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorPGX3) : GDisruptorVIP2
{
   offset = $VehicleHardpoints[$VehicleID::PyroGX, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::PyroGX, 3, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorSHR2) : GDisruptorVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 2, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorSHR3) : GDisruptorVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 3, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorSHR4) : GDisruptorVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 4, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorSHR5) : GDisruptorVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 5, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorSHX2) : GDisruptorVIP2
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 2, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorSHX3) : GDisruptorVIP2
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 3, "rotation"];
   
   stateSound[4]            = "";
};

datablock ShapeBaseImageData(GDisruptorSHX4) : GDisruptorVIP2
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 4, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorSHX5) : GDisruptorVIP2
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 5, "rotation"];

   stateSound[4]            = "";
};

datablock ShapeBaseImageData(GDisruptorSSH4) : GDisruptorVIP2
{
   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 4, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorSSH5) : GDisruptorVIP2
{
   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 5, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorSTA4) : GDisruptorVIP2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Starhawk, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 4, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorSTA5) : GDisruptorVIP2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Starhawk, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 5, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorFIB6) : GDisruptorVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 6, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorFIB7) : GDisruptorVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 7, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorJEP2) : GDisruptorVIP2
{
   offset = $VehicleHardpoints[$VehicleID::HoverJeep, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::HoverJeep, 2, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorJEP3) : GDisruptorVIP2
{
   offset = $VehicleHardpoints[$VehicleID::HoverJeep, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::HoverJeep, 3, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorUBT2) : GDisruptorVIP2
{
   shapeFile = "weapon_energy.dts";
   mountPoint = 2; // L

   offset = "0.0 -0.2 -0.1";
   rotation = $VehicleHardpoints[$VehicleID::Underbelly, 2, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorUBT3) : GDisruptorVIP2
{
   shapeFile = "weapon_energy.dts";
   mountPoint = 3; // R

   offset = "0.0 -0.2 -0.1";
   rotation = $VehicleHardpoints[$VehicleID::Underbelly, 3, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorTPQ4) : GDisruptorVIP2
{
   scorpionHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 4, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorTPQ5) : GDisruptorVIP2
{
   scorpionHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 5, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorTPQ6) : GDisruptorVIP2
{
   scorpionHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 6, "rotation"];
};

datablock ShapeBaseImageData(GDisruptorTPQ7) : GDisruptorVIP2
{
   scorpionHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 7, "rotation"];
};

function GDisruptor::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "";
    %this.ammoUse = 0;
    %this.fireTimeout = 550;
    
    // Reticle data
    %this.reticleTex = "gui/hud_ret_tankmortar";
    %this.showReticleFrame = false;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "GDisruptor", "Disruptor Bolter", "Trades Blaster's speed for damage, good vs larger targets", $VehicleList::Guillotine | $VehicleList::Starhawk | $VehicleList::Firebird | $VehicleList::Skycutter | $VehicleList::Airhawk | $VehicleList::Wildcat | $VehicleList::Leviathan | $VehicleList::ShrikeX | $VehicleList::PyroGX | $VehicleList::Shrike | $VehicleList::SuperShrike | $VehicleList::HoverJeep | $VehicleList::Tomahawk | $VehicleList::Retaliator | $VehicleList::Stryker | $VehicleList::Annihilator | $VehicleList::Wolfhound | $VehicleList::HoverJeep | $VehicleList::Stryker | $VehicleList::Leviathan | $VehicleList::Scorpion, $VHardpointSize::Gun, $VHardpointType::Energy);
