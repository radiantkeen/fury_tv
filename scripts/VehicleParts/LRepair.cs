// Sound, Projectile, and Explosion blocks
datablock AudioProfile(LRepairFireSound)
{
   filename    = "fx/vehicles/bomber_turret_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(LRepairExpSound)
{
   filename    = "fx/explosion/empulse_exp.wav";
   description = AudioExplosion3d;
   preload = true;
};

datablock ShockwaveData(LRepairShockwave)
{
   width = 5;
   numSegments = 32;
   numVertSegments = 7;
   velocity = 4;
   acceleration = 16;
   lifetimeMS = 600;
   height = 0.25;
   verticalCurve = 0.375;

   mapToTerrain = false;
   renderBottom = true;
   orientToNormal = true;


//   texture[0] = "special/crescent4";
   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 3.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1;

   colors[0] = "0.9 0.075 0.247 1.0";
   colors[1] = "0.9 0.075 0.247 1.0";
   colors[2] = "0.9 0.075 0.247 0.0";
};

datablock ExplosionData(LRepairExplosion)
{
   soundProfile   = LRepairExpSound;
   shockwave      = LRepairShockwave;
};

datablock LinearFlareProjectileData(VLRepairBolt)
{
   scale               = "5.0 5.0 5.0";
   directDamage        = 3.0;
   directDamageType    = $DamageType::Blaster;

   flags               = $Projectile::PlaysHitSound | $Projectile::RepairProjectile;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "LRepairExplosion";

   dryVelocity       = 300.0;
   wetVelocity       = 300.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 100;

   numFlares         = 10;
   size              = 0.65;
   flareColor        = "1 0.125 0.125";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "special/redflare"; // flarebase

   sound = SentryTurretProjectileSound;

   hasLight    = true;
   lightRadius = 1.5;
   lightColor  = "1.0 0.25 0.25";
};

// Primary Definition
datablock ShapeBaseImageData(LRepairSTA2)
{
	className = WeaponImage;
   shapeFile = "turret_missile_large.dts";
//   item = PulseDisruptor;
   projectile = VLRepairBolt;
   projectileType = LinearFlareProjectile;
//	armThread = looksn;

   pilotHeadTracking = true;
    usesEnergy = true;
	minEnergy = 75;
    fireEnergy = 75;
//    defaultModeFireSound = "LaserFireSound";

   ammo = "VLRepairAmmo";
   updatePilotAmmo = true;
   sharedResourcePool = true;
   
   offset = $VehicleHardpoints[$VehicleID::Starhawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 2, "rotation"];

   fireTimeout = 1000;

   // State transitions
   stateName[0]                  = "Activate";
   stateTransitionOnNotLoaded[0] = "Dead";
   stateTransitionOnLoaded[0]    = "ActivateReady";

   stateName[1]                  = "ActivateReady";
   stateSequence[1]              = "Deploy";
   stateTimeoutValue[1]          = 1;
   stateTransitionOnTimeout[1]   = "Ready";
   stateTransitionOnNotLoaded[1] = "Deactivate";
   stateTransitionOnNoAmmo[1]    = "NoAmmo";

   stateName[2]                    = "Ready";
   stateTransitionOnNotLoaded[2]   = "Deactivate";
   stateTransitionOnTriggerDown[2] = "Fire";
   stateTransitionOnNoAmmo[2]      = "NoAmmo";

   stateName[3]                = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3]        = 0.75;
   stateFire[3]                = true;
   stateRecoil[3]              = LightRecoil;
   stateAllowImageChange[3]    = false;
   stateSequence[3]            = "Fire";
   stateSound[3]               = LRepairFireSound;
   stateScript[3]              = "onFire";

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 1.25;
   stateAllowImageChange[4]      = false;
   stateSequence[4]              = "Deploy";
   stateTransitionOnTimeout[4]   = "Ready";
   stateTransitionOnNotLoaded[4] = "Deactivate";
    stateTransitionOnNoAmmo[4]    = "NoAmmo";

   stateName[5]                = "Deactivate";
   stateSequence[5]            = "Activate";
   stateDirection[5]           = false;
   stateTimeoutValue[5]        = 1;
   stateTransitionOnLoaded[5]  = "ActivateReady";
   stateTransitionOnTimeout[5] = "Dead";

   stateName[6]               = "Dead";
   stateTransitionOnLoaded[6] = "ActivateReady";

   stateName[7]             = "NoAmmo";
   stateTransitionOnAmmo[7] = "Reload";
   stateSequence[7]         = "NoAmmo";
};

// Inherited Defs
datablock ShapeBaseImageData(LRepairAMI2) : LRepairSTA2
{
   pilotHeadTracking = false;

   offset = $VehicleHardpoints[$VehicleID::Ameri, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Ameri, 2, "rotation"];
};

datablock ShapeBaseImageData(LRepairSSH2) : LRepairSTA2
{
   pilotHeadTracking = false;

   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 2, "rotation"];
};

datablock ShapeBaseImageData(LRepairTUT4) : LRepairSTA2
{
   pilotHeadTracking = false;

   mountPoint = 1;

   offset = "0 0 0";
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 4, "rotation"];
};

datablock ShapeBaseImageData(LRepairTOM2) : LRepairSTA2
{
   pilotHeadTracking = false;

   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 2, "rotation"];
};

datablock ShapeBaseImageData(LRepairTOM3) : LRepairSTA2
{
   pilotHeadTracking = false;

   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 3, "rotation"];
};

datablock ShapeBaseImageData(LRepairFIB2) : LRepairSTA2
{
   pilotHeadTracking = false;

   offset = $VehicleHardpoints[$VehicleID::Firebird, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 2, "rotation"];
};

datablock ShapeBaseImageData(LRepairFIB4) : LRepairSTA2
{
   pilotHeadTracking = false;

   offset = $VehicleHardpoints[$VehicleID::Firebird, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 4, "rotation"];
};

datablock ShapeBaseImageData(LRepairFIB5) : LRepairSTA2
{
   pilotHeadTracking = false;

   offset = $VehicleHardpoints[$VehicleID::Firebird, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 5, "rotation"];
};

datablock ShapeBaseImageData(LRepairTUS2) : LRepairSTA2
{
   pilotHeadTracking = false;

   offset = $VehicleHardpoints[$VehicleID::TurretPole, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretPole, 2, "rotation"];
};

datablock ShapeBaseImageData(LRepairGUI2) : LRepairSTA2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Guillotine, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Guillotine, 2, "rotation"];
};

datablock ShapeBaseImageData(LRepairRET2) : LRepairSTA2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Retaliator, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Retaliator, 2, "rotation"];
};

datablock ShapeBaseImageData(LRepairSTR2) : LRepairSTA2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Stryker, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Stryker, 2, "rotation"];
};

datablock ShapeBaseImageData(LRepairTPD2) : LRepairSTA2
{
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 2, "rotation"];
};

datablock ShapeBaseImageData(LRepairTPD3) : LRepairSTA2
{
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 3, "rotation"];
};

function LRepair::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = true;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 20;
    %this.ammo = "VLRepairAmmo";
    %this.ammoUse = 1;
    %this.fireTimeout = 1000;
    
    // Reticle data
    %this.reticleTex = "gui/hud_ret_tankmortar";
    %this.showReticleFrame = true;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LRepair", "Nanorepair Cannon", "Repairs targets for 300 HP per player", $VehicleList::Firebird | $VehicleList::Tomahawk | $VehicleList::Ameri | $VehicleList::SuperShrike | $VehicleList::Leviathan | $VehicleList::Guillotine | $VehicleList::Starhawk | $VehicleList::Firebird | $VehicleList::Retaliator | $VehicleList::HoverJeep | $VehicleList::Stryker | $VehicleList::Wolfhound | $VehicleList::Annihilator | $VehicleList::Goodship | $VehicleList::Scorpion, $VHardpointSize::Cannon, $VHardpointType::Standard);
