// Primary Definition

datablock ShapeBaseImageData(PHailstormSKY0)
{
   className = WeaponImage;
   shapeFile = "weapon_nadelauncher.dts"; // stackable2s
   mountPoint = "0";
   offset = $VehicleHardpoints[$VehicleID::Skycutter, 0, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Skycutter, 0, "rotation"];

   projectile = HailstormRocket;
   projectileType = LinearProjectile;
   ammo = "VPHailstormAmmo";
   updatePilotAmmo = true;
   sharedResourcePool = true;

   defaultModeFireSound = "HailstormFireSound";
   staggerCount = 3;
   staggerDelay = 200;
   projectileSpread = 1.0;
   projectileOffset = 0.3;
   useForwardVector = true;
   
   usesEnergy = false;
   useMountEnergy = false;
   fireEnergy = 0.0;
   minEnergy = 0.0;
   useCapacitor = false;
   
   fireTimeout = 1000;
   
   // State transitions
   stateName[0]                  = "Activate";
   stateTransitionOnNotLoaded[0] = "Dead";
   stateTransitionOnLoaded[0]    = "ActivateReady";

   stateName[1]                  = "ActivateReady";
   stateSequence[1]              = "Deploy";
   stateTimeoutValue[1]          = 1;
   stateTransitionOnTimeout[1]   = "Ready";
   stateTransitionOnNotLoaded[1] = "Deactivate";
   stateTransitionOnNoAmmo[1]    = "NoAmmo";

   stateName[2]                    = "Ready";
   stateTransitionOnNotLoaded[2]   = "Deactivate";
   stateTransitionOnTriggerDown[2] = "Fire";
   stateTransitionOnNoAmmo[2]      = "NoAmmo";

   stateName[3]                = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3]        = 2.5;
   stateFire[3]                = true;
   stateRecoil[3]              = LightRecoil;
   stateAllowImageChange[3]    = false;
   stateSequence[3]            = "Fire";
   stateSound[3]               = "";
   stateScript[3]              = "onFire";

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 0.5;
   stateSound[4]                 = RocketPodReloadSound;
   stateAllowImageChange[4]      = false;
   stateTransitionOnTimeout[4]   = "Ready";
   stateTransitionOnNotLoaded[4] = "Deactivate";
    stateTransitionOnNoAmmo[4]    = "NoAmmo";

   stateName[5]                = "Deactivate";
   stateSequence[5]            = "Activate";
   stateDirection[5]           = false;
   stateTimeoutValue[5]        = 1;
   stateTransitionOnLoaded[5]  = "ActivateReady";
   stateTransitionOnTimeout[5] = "Dead";

   stateName[6]               = "Dead";
   stateTransitionOnLoaded[6] = "ActivateReady";

   stateName[7]             = "NoAmmo";
   stateTransitionOnAmmo[7] = "Reload";
   stateSequence[7]         = "NoAmmo";
};

// Inherited Defs

datablock ShapeBaseImageData(PHailstormCAT2) : PHailstormSKY0
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 2, "rotation"];
};

datablock ShapeBaseImageData(PHailstormCAT3) : PHailstormSKY0
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 3, "rotation"];
};

datablock ShapeBaseImageData(PHailstormSHR4) : PHailstormSKY0
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 4, "rotation"];
};

datablock ShapeBaseImageData(PHailstormSHR5) : PHailstormSKY0
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 5, "rotation"];
};

datablock ShapeBaseImageData(PHailstormSSH4) : PHailstormSKY0
{
   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 4, "rotation"];
};

datablock ShapeBaseImageData(PHailstormSSH5) : PHailstormSKY0
{
   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 5, "rotation"];
};

datablock ShapeBaseImageData(PHailstormFIB6) : PHailstormSKY0
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 6, "rotation"];
};

datablock ShapeBaseImageData(PHailstormFIB7) : PHailstormSKY0
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 7, "rotation"];
};

datablock ShapeBaseImageData(PHailstormTUD4) : PHailstormSKY0
{
   offset = "0.45 1.5 0";
   rotation = $VehicleHardpoints[$VehicleID::TurretPole, 4, "rotation"];
};

datablock ShapeBaseImageData(PHailstormTUD5) : PHailstormSKY0
{
   offset = "-0.45 1.5 0";
   rotation = $VehicleHardpoints[$VehicleID::TurretPole, 5, "rotation"];
};

datablock ShapeBaseImageData(PHailstormTUT4) : PHailstormSKY0
{
   mountPoint = 1;
   
   offset = $VehicleHardpoints[$VehicleID::TankTurret, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 4, "rotation"];
};

datablock ShapeBaseImageData(PHailstormTUT5) : PHailstormSKY0
{
   mountPoint = 1;
   
   offset = $VehicleHardpoints[$VehicleID::TankTurret, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 5, "rotation"];
};

datablock ShapeBaseImageData(PHailstormUBT2) : PHailstormSKY0
{
   mountPoint = 2; // L
   
   offset = $VehicleHardpoints[$VehicleID::Underbelly, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Underbelly, 2, "rotation"];
};

datablock ShapeBaseImageData(PHailstormUBT3) : PHailstormSKY0
{
   mountPoint = 3; // R
   
   offset = $VehicleHardpoints[$VehicleID::Underbelly, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Underbelly, 3, "rotation"];
};

function PHailstorm::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = true;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 20;
    %this.ammo = "VPHailstormAmmo";
    %this.ammoUse = 1;
    %this.fireTimeout = 0;
    
    // Reticle data
    %this.reticleTex = "gui/RET_missile";
    %this.showReticleFrame = false;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "PHailstorm", "Hailstorm Rocket Pod", "Dumbfire cluster rocket pod, high damage, moderate range", $VehicleList::Guillotine | $VehicleList::Skycutter | $VehicleList::Shrike | $VehicleList::SuperShrike | $VehicleList::Firebird | $VehicleList::Catapult | $VehicleList::Tomahawk | $VehicleList::Wolfhound | $VehicleList::Leviathan, $VHardpointSize::Gun, $VHardpointType::Pod);
