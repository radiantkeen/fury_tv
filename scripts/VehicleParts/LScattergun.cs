// Sound, Projectile, and Explosion blocks
datablock AudioProfile(VScattergunFireSound)
{
   filename    = "fx/weapon/scattergun_turret_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock LinearProjectileData(VLScattergunShot) : BolterBullet
{
   directDamage        = 1.0;
   directDamageType    = $DamageType::Scattergun;
   hasDamageRadius     = false;
   kickBackStrength    = 1000;
   
   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Kinetic;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "ScattergunExplosion";

   dryVelocity       = 400;
   wetVelocity       = 225;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 500;
   lifetimeMS        = 600;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 600;
};

// Primary Definition
datablock ShapeBaseImageData(LScattergunSSH2)
{
   shapeFile = "turret_mortar_large.dts";
   mountPoint = 0;
   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 2, "rotation"];

   projectile = VLScattergunShot;
   projectileType = LinearProjectile;
   ammo = "VLScattergunAmmo";
   updatePilotAmmo = true;
   sharedResourcePool = true;

   defaultModeFireSound = "";
   staggerCount = 12;
   staggerDelay = 0;
   projectileSpread = 16.0;

   usesEnergy = false;
   useMountEnergy = false;
   fireEnergy = 75.0;
   minEnergy = 75.0;
   useCapacitor = true;

   // State transitions
   stateName[0]                        = "Activate";
   stateTransitionOnNotLoaded[0]       = "Dead";
   stateTransitionOnLoaded[0]          = "ActivateReady";

   stateName[1]                        = "ActivateReady";
   stateSequence[1]                    = "Activate";
//   stateSound[1]                       = AssaultTurretActivateSound;
   stateTimeoutValue[1]                = 1.0;
   stateTransitionOnTimeout[1]         = "Ready";
   stateTransitionOnNotLoaded[1]       = "Deactivate";

   stateName[2]                        = "Ready";
   stateTransitionOnNotLoaded[2]       = "Deactivate";
   stateTransitionOnNoAmmo[2]          = "NoAmmo";
   stateTransitionOnTriggerDown[2]     = "Fire";

   stateName[3]                        = "Fire";
   stateSequence[3]                    = "Fire";
   stateTransitionOnTimeout[3]         = "Reload";
   stateTimeoutValue[3]                = 2.5;
   stateFire[3]                        = true;
   stateRecoil[3]                      = LightRecoil;
   stateAllowImageChange[3]            = false;
   stateSound[3]                       = VScattergunFireSound;
   stateScript[3]                      = "onFire";

   stateName[4]                        = "Reload";
   stateSequence[4]                    = "Reload";
   stateTimeoutValue[4]                = 0.5;
   stateAllowImageChange[4]            = false;
   stateSound[4]                       = MortarReloadSound;
   stateTransitionOnTimeout[4]         = "Ready";
   //stateTransitionOnNoAmmo[4]          = "NoAmmo";
   stateWaitForTimeout[4]              = true;

   stateName[5]                        = "Deactivate";
   stateDirection[5]                   = false;
   stateSequence[5]                    = "Activate";
   stateTimeoutValue[5]                = 1.0;
   stateTransitionOnLoaded[5]          = "ActivateReady";
   stateTransitionOnTimeout[5]         = "Dead";

   stateName[6]                        = "Dead";
   stateTransitionOnLoaded[6]          = "ActivateReady";
   stateTransitionOnTriggerDown[6]     = "DryFire";

   stateName[7]                        = "DryFire";
   stateSound[7]                       = AssaultMortarDryFireSound;
   stateTimeoutValue[7]                = 1.0;
   stateTransitionOnTimeout[7]         = "NoAmmo";

   stateName[8]                        = "NoAmmo";
   stateSequence[8]                    = "NoAmmo";
   stateTransitionOnAmmo[8]            = "Reload";
   stateTransitionOnTriggerDown[8]     = "DryFire";
};

// Inherited Defs
datablock ShapeBaseImageData(LScattergunSTA2) : LScattergunSSH2
{
   pilotHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::Starhawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 2, "rotation"];
};

datablock ShapeBaseImageData(LScattergunGUI2) : LScattergunSSH2
{
   pilotHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::Guillotine, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Guillotine, 2, "rotation"];
};

datablock ShapeBaseImageData(LScattergunAMI2) : LScattergunSSH2
{
   offset = $VehicleHardpoints[$VehicleID::Ameri, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Ameri, 2, "rotation"];
};

datablock ShapeBaseImageData(LScattergunTUT4) : LScattergunSSH2
{
   shapeFile = "pyro_fcannon.dts";

   mountPoint = 1;

   offset = "0 0.5 0";
   rotation = "0 0 1 180";
};

datablock ShapeBaseImageData(LScattergunFIB2) : LScattergunSSH2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 2, "rotation"];
};

datablock ShapeBaseImageData(LScattergunFIB4) : LScattergunSSH2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 4, "rotation"];
};

datablock ShapeBaseImageData(LScattergunFIB5) : LScattergunSSH2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 5, "rotation"];
};

datablock ShapeBaseImageData(LScattergunTUS2) : LScattergunSSH2
{
   shapeFile = "pyro_fcannon.dts";
   
   offset = "0 1 0";
   rotation = "0 0 1 180";
};

datablock ShapeBaseImageData(LScattergunTOM2) : LScattergunSSH2
{
   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 2, "rotation"];
};

datablock ShapeBaseImageData(LScattergunTOM3) : LScattergunSSH2
{
   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 3, "rotation"];
};

datablock ShapeBaseImageData(LScattergunRET2) : LScattergunSSH2
{
   pilotHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::Retaliator, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Retaliator, 2, "rotation"];
};

datablock ShapeBaseImageData(LScattergunSTR2) : LScattergunSSH2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Stryker, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Stryker, 2, "rotation"];
};

datablock ShapeBaseImageData(LScattergunTPD2) : LScattergunSSH2
{
   shapeFile = "pyro_fcannon.dts";
   
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 2, "rotation"];
};

datablock ShapeBaseImageData(LScattergunTPD3) : LScattergunSSH2
{
   shapeFile = "pyro_fcannon.dts";
   
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 3, "rotation"];
};

function LScattergun::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = true;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 25;
    %this.ammo = "VLScattergunAmmo";
    %this.ammoUse = 1;
    %this.fireTimeout = 1500;
    
    // Reticle data
    %this.reticleTex = "gui/RET_Plasma";
    %this.showReticleFrame = true;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LScattergun", "Scattergun", "A vehicle-sized sawed off shotgun, damage drastically drops off at range", $VehicleList::Firebird | $VehicleList::Tomahawk | $VehicleList::Ameri | $VehicleList::Guillotine | $VehicleList::Leviathan | $VehicleList::SuperShrike | $VehicleList::Starhawk | $VehicleList::Firebird | $VehicleList::Retaliator | $VehicleList::HoverJeep | $VehicleList::Stryker | $VehicleList::Wolfhound | $VehicleList::Annihilator | $VehicleList::Goodship | $VehicleList::Scorpion, $VHardpointSize::Cannon, $VHardpointType::Ballistic);
