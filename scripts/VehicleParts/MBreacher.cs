datablock AudioProfile(SLAMMExpSound)
{
   filename = "fx/explosion/missile_medium_exp.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock ParticleData( SLAMBGDebrisSmokeParticle )
{
   dragCoeffiecient     = 1.0;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 2000;
   lifetimeVarianceMS   = 500;

   textureName          = "particleTest";

   useInvAlpha =     true;

   spinRandomMin = -60.0;
   spinRandomMax = 60.0;

   colors[0]     = "0.4 0.5 1.0 1.0";
   colors[1]     = "0.3 0.3 1.0 0.5";
   colors[2]     = "0.2 0.2 0.2 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 1.5;
   sizes[2]      = 3.0;
   times[0]      = 0.0;
   times[1]      = 0.75;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( SLAMBGDebrisSmokeEmitter )
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 1;

   ejectionVelocity = 0.0;  // A little oomph at the back end
   velocityVariance = 0.0;

   thetaMin         = 0.0;
   thetaMax         = 10.0;

   particles = "SLAMBGDebrisSmokeParticle";
};

datablock DebrisData( SLAMEffExplosionDebris )
{
   emitters[0] = SLAMBGDebrisSmokeEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 0.5;
   lifetimeVariance = 0.2;

   numBounces = 1;
};

datablock ParticleData(SLAMEffExplosionSmokeParticle)
{
    dragCoefficient = 0;
    gravityCoefficient = 0;
    windCoefficient = 1;
    inheritedVelFactor = 0.0;
    constantAcceleration = 0.0;
    lifetimeMS = 1250;
    lifetimeVarianceMS = 0;
    useInvAlpha = 1;
    spinRandomMin = -200;
    spinRandomMax = 200;
    textureName = "special/Smoke/smoke_001";
    times[0] = 0;
    times[1] = 0.2;
    times[2] = 1;
    colors[0] = "0.244094 0.656000 1.000000 1.000000";
    colors[1] = "0.200000 0.300000 1.000000 1.000000";
    colors[2] = "0.000000 0.100000 1.000000 0.000000";
    sizes[0] = 1.35484;
    sizes[1] = 3;
    sizes[2] = 7;
};

datablock ParticleEmitterData(SLAMEffExplosionSmokeEmitter)
{
    ejectionPeriodMS = 5;
    periodVarianceMS = 1;
    ejectionVelocity = 6;
    velocityVariance = 1;
    ejectionOffset = 1;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
	   lifeTimeMS = 600;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "SLAMEffExplosionSmokeParticle";
};

datablock ParticleData(SLAMEffSparkParticle)
{
    dragCoefficient = 0.658537;
    gravityCoefficient = 0;
    windCoefficient = 1;
    inheritedVelFactor = 0.2;
    constantAcceleration = 0;
    lifetimeMS = 500;
    lifetimeVarianceMS = 350;
    useInvAlpha = 0;
    spinRandomMin = 0;
    spinRandomMax = 0;
    textureName = "special/underwaterSpark.PNG";
    times[0] = 0;
    times[1] = 0.677419;
    times[2] = 1;
    colors[0] = "0.322835 0.704000 1.000000 1.000000";
    colors[1] = "0.173228 0.544000 0.864000 1.000000";
    colors[2] = "0.000000 0.712000 1.000000 0.000000";
    sizes[0] = 0.790323;
    sizes[1] = 0.790323;
    sizes[2] = 0.790323;
};

datablock ParticleEmitterData(SLAMEffSparkEmitter)
{
    ejectionPeriodMS = 2;
    periodVarianceMS = 0;
    ejectionVelocity = 19;
    velocityVariance = 5;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
	   lifeTimeMS = 250;
    orientParticles= 1;
    orientOnVelocity = 1;
    particles = "SLAMEffSparkParticle";
};

datablock ParticleData(SLAMEffCrescentParticle)
{
    dragCoefficient = 1.60976;
    gravityCoefficient = 0;
    windCoefficient = 1;
    inheritedVelFactor = 0.2;
    constantAcceleration = -0;
    lifetimeMS = 600;
    lifetimeVarianceMS = 0;
    useInvAlpha = 0;
    spinRandomMin = 0;
    spinRandomMax = 0;
    textureName = "special/lightFalloffMono.png";
    times[0] = 0;
    times[1] = 0.5;
    times[2] = 1;
    colors[0] = "0.173228 0.296000 1.000000 1.000000";
    colors[1] = "0.251969 0.296000 1.000000 0.500000";
    colors[2] = "0.488189 0.520000 1.000000 0.000000";
    sizes[0] = 4;
    sizes[1] = 8;
    sizes[2] = 9;
};

datablock ParticleEmitterData(SLAMEffCrescentEmitter)
{
    ejectionPeriodMS = 10;
    periodVarianceMS = 0;
    ejectionVelocity = 40;
    velocityVariance = 5;
    ejectionOffset =   0.967742;
    thetaMin = 5;
    thetaMax = 175;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
	   lifeTimeMS = 1000;
    orientParticles= 1;
    orientOnVelocity = 1;
    particles = "SLAMEffCrescentParticle";
};

datablock ExplosionData(SLAMEffMissileExplosion)
{
   explosionShape = "disc_explosion.dts";
   playSpeed = 0.5;
   soundProfile   = SLAMMExpSound;
   faceViewer = true;

//   sizes[0] = "2 2 2";
// sizes[1] = "2 2 2";
//   sizes[2] = "2 2 2";

   emitter[0] = SLAMEffExplosionSmokeEmitter;
   emitter[1] = SLAMEffSparkEmitter;

   debris = SLAMEffExplosionDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 170;
   debrisNum = 8;
   debrisNumVariance = 4;
   debrisVelocity = 32.0;
   debrisVelocityVariance = 8.0;

   shakeCamera = true;
   camShakeFreq = "6.0 7.0 7.0";
   camShakeAmp = "70.0 70.0 70.0";
   camShakeDuration = 0.5;
   camShakeRadius = 7.0;
};

datablock ShapeBaseImageData(MBreacherSHR6) : EngineAPEImage
{
   mountPoint = 0;

   shapeFile = "vehicle_missile_mirv.dts";

   offset = $VehicleHardpoints[$VehicleID::Shrike, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 6, "rotation"];
};

datablock ShapeBaseImageData(MBreacherSHR7) : MBreacherSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 7, "rotation"];
};

datablock ShapeBaseImageData(MBreacherSHX6) : MBreacherSHR6
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 6, "rotation"];
};

datablock ShapeBaseImageData(MBreacherSHX7) : MBreacherSHR6
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 7, "rotation"];
};

datablock ShapeBaseImageData(MBreacherAIR7) : MBreacherSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Airhawk, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Airhawk, 7, "rotation"];
};

datablock ShapeBaseImageData(MBreacherSSH6) : MBreacherSHR6
{
   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 6, "rotation"];
};

datablock ShapeBaseImageData(MBreacherSSH7) : MBreacherSHR6
{
   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 7, "rotation"];
};

datablock ShapeBaseImageData(MBreacherSTA6) : MBreacherSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Starhawk, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 6, "rotation"];
};

datablock ShapeBaseImageData(MBreacherSTA7) : MBreacherSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Starhawk, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 7, "rotation"];
};

datablock ShapeBaseImageData(MBreacherTOM4) : MBreacherSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 4, "rotation"];
};

datablock ShapeBaseImageData(MBreacherTOM5) : MBreacherSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 5, "rotation"];
};

datablock ShapeBaseImageData(MBreacherTOM6) : MBreacherSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 6, "rotation"];
};

datablock ShapeBaseImageData(MBreacherTOM7) : MBreacherSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 7, "rotation"];
};

datablock ShapeBaseImageData(MBreacherJEP6) : MBreacherSHR6
{
   offset = $VehicleHardpoints[$VehicleID::HoverJeep, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::HoverJeep, 6, "rotation"];
};

datablock ShapeBaseImageData(MBreacherJEP7) : MBreacherSHR6
{
   offset = $VehicleHardpoints[$VehicleID::HoverJeep, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::HoverJeep, 7, "rotation"];
};

datablock ShapeBaseImageData(MBreacherCAT4) : MBreacherSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 4, "rotation"];
};

datablock ShapeBaseImageData(MBreacherCAT5) : MBreacherSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 5, "rotation"];
};

datablock ShapeBaseImageData(MBreacherCAT6) : MBreacherSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 6, "rotation"];
};

datablock ShapeBaseImageData(MBreacherCAT7) : MBreacherSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 7, "rotation"];
};

datablock ShapeBaseImageData(MBreacherGUI6) : MBreacherSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Guillotine, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Guillotine, 6, "rotation"];
};

datablock ShapeBaseImageData(MBreacherGUI7) : MBreacherSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Guillotine, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Guillotine, 7, "rotation"];
};

datablock ShapeBaseImageData(MBreacherRET6) : MBreacherSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Retaliator, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Retaliator, 6, "rotation"];
};

datablock ShapeBaseImageData(MBreacherRET7) : MBreacherSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Retaliator, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Retaliator, 7, "rotation"];
};

datablock ShapeBaseImageData(MBreacherWOL6) : MBreacherSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Wolfhound, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Wolfhound, 6, "rotation"];
};

datablock ShapeBaseImageData(MBreacherWOL7) : MBreacherSHR6
{
   offset = $VehicleHardpoints[$VehicleID::Wolfhound, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Wolfhound, 7, "rotation"];
};

datablock BombProjectileData(BreacherMissileDeploy)
{
   projectileShapeName  = "vehicle_missile_mirv.dts";
   emitterDelay         = -1;
   directDamage         = 0.0;
   hasDamageRadius      = true;
   indirectDamage       = 10.0;
   damageRadius         = 20;
   radiusDamageType     = $DamageType::Missile;
   directDamageType     = $DamageType::Missile;
   kickBackStrength     = 2250;  // z0dd - ZOD, 4/25/02. Was 2500

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Explosive;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   explosion            = "SLAMEffMissileExplosion";
   velInheritFactor     = 1.0;

   grenadeElasticity    = 0.25;
   grenadeFriction      = 0.4;
   armingDelayMS        = 2000;
   muzzleVelocity       = 15.0;
   drag                 = 0.3;
   gravityMod		    = 1.0;

   minRotSpeed          = "0.0 45.0 0.0";
   maxRotSpeed          = "0.0 45.0 0.0";

   scale                = "1.0 1.0 1.0";

   sound                = "";
   
   missileNameBase      = "BreacherMissile";
};

datablock LinearProjectileData(BreacherMissileDumbfire)
{
//   scale = "1.5 1.5 1.5";
   projectileShapeName = "vehicle_missile_mirv.dts";
   emitterDelay        = -1;
   directDamage        = 0;
   directDamageType    = $DamageType::Missile;
   hasDamageRadius     = true;
   indirectDamage      = 10.0;
   damageRadius        = 20;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 3200;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Explosive;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "SLAMEffMissileExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = WildcatJetEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 150;
   wetVelocity       = 100;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 7000;
   lifetimeMS        = 7000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 6000;

   sound = "LargeMissileProjectileSound";
   
   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 7.0;
   lightColor  = "0.4 0.15 0.1";
};

datablock SeekerProjectileData(BreacherMissile) : ShoulderMissile
{
//   scale = "5.0 5.0 5.0";
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "vehicle_missile_mirv.dts";
   directDamage        = 0;
   directDamageType    = $DamageType::Missile;
   hasDamageRadius     = true;
   indirectDamage      = 10;
   damageRadius        = 20;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 3200;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Explosive;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "SLAMEffMissileExplosion";
//   underwaterExplosion = UnderwaterHandGrenadeExplosion;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = WildcatJetEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   lifetimeMS          = 7000;
   muzzleVelocity      = 150.0;
   maxVelocity         = 225.0;
   turningSpeed        = 90.0;
   acceleration        = 50.0;

   proximityRadius     = 4;

   flareDistance = 10;
   flareAngle    = 10;

//   sound = ScoutThrustSound;
   sound = "LargeMissileProjectileSound";

   hasLight    = true;
   lightRadius = 7.0;
   lightColor  = "0.4 0.15 0.1";

   useFlechette = false;
   explodeOnWaterImpact = false;

   // Used for vehicle-mounted missile system
   isVehicleMissile = true;
};

function MBreacher::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "";
    %this.rearmTime = 6000;
    
    %this.missileName = "BreacherMissile";
    %this.missileSize = $VHardpointSize::Medium;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "MBreacher", "Breacher Missile", "A good mix of speed, maneuverability, and power for use vs medium sized or larger craft", $VehicleList::ShrikeX | $VehicleList::Airhawk | $VehicleList::Shrike | $VehicleList::SuperShrike | $VehicleList::Starhawk | $VehicleList::Tomahawk | $VehicleList::HoverJeep | $VehicleList::Catapult | $VehicleList::Guillotine | $VehicleList::Retaliator | $VehicleList::Wolfhound | $VehicleList::Scorpion, $VHardpointSize::Missile, $VHardpointType::Missile);
