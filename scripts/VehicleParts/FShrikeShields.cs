// Shrike Shield Defs
$SD::Shrike::maxStrength = 375;
$SD::Shrike::recDelay = 10;
$SD::Shrike::resTime = 30;
$SD::Shrike::recRate = 120;

// Definition Objects
function FShrikeStandard::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";

    // Shield settings
    %this.maxStrength = $SD::Shrike::maxStrength;
    %this.recDelay = $SD::Shrike::recDelay;
    %this.resTime = $SD::Shrike::resTime;
    %this.recRate = $SD::Shrike::recRate;
    %this.powerDrainPct = $ShieldDefs::StandardPowerDrain;
    %this.bleedthrough = $ShieldDefs::StandardBleedthrough;
    
    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = %this.maxStrength;
    %this.deltaRegen = ($VehicleListData[$VehicleID::Shrike, "block"].rechargeRate * $g_TickTime) * -(%this.powerDrainPct / 100);
}

function FShrikeStandard::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, %this.maxStrength, mFloor(%this.recDelay * 1000), mFloor(%this.resTime * 1000), %this.recRate / $g_TickTime, %this.powerDrainPct / 100, %this.bleedthrough / 100, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "FShrikeStandard", "Standard Shields", %this.maxStrength @ " HP, Recharge delay: " @ %this.recDelay @ " sec, Restart delay: " @ %this.resTime @ " sec, Bleedthrough: " @ %this.bleedthrough @ "%, Power Drain: " @ %this.powerDrainPct @ "%", $VehicleList::Shrike, $VHardpointSize::Internal, $VHardpointType::Anything);

function FShrikeLight::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";

    // Shield settings
    %this.maxStrength = mCeil($SD::Shrike::maxStrength * 0.5);
    %this.recDelay = $SD::Shrike::recDelay * 0.75;
    %this.resTime = $SD::Shrike::resTime * 0.5;
    %this.recRate = $SD::Shrike::recRate * 0.5;
    %this.powerDrainPct = $ShieldDefs::LightPowerDrain;
    %this.bleedthrough = $ShieldDefs::LightBleedthrough;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = %this.maxStrength;
    %this.deltaRegen = ($VehicleListData[$VehicleID::Shrike, "block"].rechargeRate * $g_TickTime) * -(%this.powerDrainPct / 100);
}

function FShrikeLight::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, %this.maxStrength, mFloor(%this.recDelay * 1000), mFloor(%this.resTime * 1000), %this.recRate / $g_TickTime, %this.powerDrainPct / 100, %this.bleedthrough / 100, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "FShrikeLight", "Light Shields", %this.maxStrength @ " HP, Recharge delay: " @ %this.recDelay @ " sec, Restart delay: " @ %this.resTime @ " sec, Bleedthrough: " @ %this.bleedthrough @ "%, Power Drain: " @ %this.powerDrainPct @ "%", $VehicleList::Shrike, $VHardpointSize::Internal, $VHardpointType::Anything);

function FShrikeRegenerative::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";

    // Shield settings
    %this.maxStrength = mCeil($SD::Shrike::maxStrength * 0.333);
    %this.recDelay = $SD::Shrike::recDelay * 0.5;
    %this.resTime = $SD::Shrike::resTime * 0.333;
    %this.recRate = $SD::Shrike::recRate * 0.75;
    %this.powerDrainPct = $ShieldDefs::RegenerativePowerDrain;
    %this.bleedthrough = $ShieldDefs::RegenerativeBleedthrough;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = %this.maxStrength;
    %this.deltaRegen = ($VehicleListData[$VehicleID::Shrike, "block"].rechargeRate * $g_TickTime) * -(%this.powerDrainPct / 100);
}

function FShrikeRegenerative::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, %this.maxStrength, mFloor(%this.recDelay * 1000), mFloor(%this.resTime * 1000), %this.recRate / $g_TickTime, %this.powerDrainPct / 100, %this.bleedthrough / 100, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "FShrikeRegenerative", "Regenerative Shields", %this.maxStrength @ " HP, Recharge delay: " @ %this.recDelay @ " sec, Restart delay: " @ %this.resTime @ " sec, Bleedthrough: " @ %this.bleedthrough @ "%, Power Drain: " @ %this.powerDrainPct @ "%", $VehicleList::Shrike, $VHardpointSize::Internal, $VHardpointType::Anything);

function FShrikeHardened::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";

    // Shield settings
    %this.maxStrength = $SD::Shrike::maxStrength;
    %this.recDelay = $SD::Shrike::recDelay * 1.5;
    %this.resTime = $SD::Shrike::resTime * 1.25;
    %this.recRate = $SD::Shrike::recRate;
    %this.powerDrainPct = $ShieldDefs::HardenedPowerDrain;
    %this.bleedthrough = $ShieldDefs::HardenedBleedthrough;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = %this.maxStrength;
    %this.deltaRegen = ($VehicleListData[$VehicleID::Shrike, "block"].rechargeRate * $g_TickTime) * -(%this.powerDrainPct / 100);
}

function FShrikeHardened::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, %this.maxStrength, mFloor(%this.recDelay * 1000), mFloor(%this.resTime * 1000), %this.recRate / $g_TickTime, %this.powerDrainPct / 100, %this.bleedthrough / 100, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "FShrikeHardened", "Hardened Shields", %this.maxStrength @ " HP, Recharge delay: " @ %this.recDelay @ " sec, Restart delay: " @ %this.resTime @ " sec, Bleedthrough: " @ %this.bleedthrough @ "%, Power Drain: " @ %this.powerDrainPct @ "%", $VehicleList::Shrike, $VHardpointSize::Internal, $VHardpointType::Anything);

function FShrikeCovariant::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";

    // Shield settings
    %this.maxStrength = mCeil($SD::Shrike::maxStrength * 1.5);
    %this.recDelay = $SD::Shrike::recDelay;
    %this.resTime = $SD::Shrike::resTime;
    %this.recRate = $SD::Shrike::recRate;
    %this.powerDrainPct = $ShieldDefs::CovariantPowerDrain;
    %this.bleedthrough = $ShieldDefs::CovariantBleedthrough;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = %this.maxStrength;
    %this.deltaRegen = ($VehicleListData[$VehicleID::Shrike, "block"].rechargeRate * $g_TickTime) * -(%this.powerDrainPct / 100);
}

function FShrikeCovariant::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, %this.maxStrength, mFloor(%this.recDelay * 1000), mFloor(%this.resTime * 1000), %this.recRate / $g_TickTime, %this.powerDrainPct / 100, %this.bleedthrough / 100, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "FShrikeCovariant", "Covariant Shields", %this.maxStrength @ " HP, Recharge delay: " @ %this.recDelay @ " sec, Restart delay: " @ %this.resTime @ " sec, Bleedthrough: " @ %this.bleedthrough @ "%, Power Drain: " @ %this.powerDrainPct @ "%", $VehicleList::Shrike, $VHardpointSize::Internal, $VHardpointType::Anything);
