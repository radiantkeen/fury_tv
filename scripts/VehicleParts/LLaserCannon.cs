// Sound, Projectile, and Explosion blocks
datablock SniperProjectileData(VPulseLaserShot) : PulseLaserShot
{
   directDamage        = 0.5;
   hasDamageRadius     = false;
   indirectDamage      = 0.0;
   damageRadius        = 0.0;
   velInheritFactor    = 1.0;
   sound 				  = SniperRifleProjectileSound;
   explosion           = "LaserExplosion";
   splash              = SniperSplash;
   directDamageType    = $DamageType::PulseLaser;

   flags               = $Projectile::PlaysHitSound | $Projectile::Energy;
   ticking             = false;
   headshotMultiplier  = 1.0;

   maxRifleRange       = 150;
   rifleHeadMultiplier = 1.0;
   beamColor           = "1 0.1 0.1";
   fadeTime            = 1.0;

   hasFalloff = true;
   optimalRange = 75;
   falloffRange = 150;
   falloffDamagePct = 0.25;

   startBeamWidth		  = 0.5;
   endBeamWidth 	     = 0.1;
   pulseBeamWidth 	  = 1.0;
   beamFlareAngle 	  = 3.0;
   minFlareSize        = 0.0;
   maxFlareSize        = 400.0;
   pulseSpeed          = 18.0;
   pulseLength         = 0.2;

   lightRadius         = 1.0;
   lightColor          = "0.3 0.0 0.0";

   textureName[0]      = "special/flare";
   textureName[1]      = "special/nonlingradient";
   textureName[2]      = "special/laserrip01";
   textureName[3]      = "special/laserrip02";
   textureName[4]      = "special/laserrip03";
   textureName[5]      = "special/laserrip04";
   textureName[6]      = "special/laserrip05";
   textureName[7]      = "special/laserrip06";
   textureName[8]      = "special/laserrip07";
   textureName[9]      = "special/laserrip08";
   textureName[10]     = "special/laserrip09";
   textureName[11]     = "special/sniper00";
};

// Primary Definition
datablock ShapeBaseImageData(LLaserCannonSTA2)
{
	className = WeaponImage;
   shapeFile = "turret_elf_large.dts"; // herc_complaser_right
//   item = PulseLaserCannon;
   projectile = VPulseLaserShot;
   projectileType = SniperProjectile;
//	armThread = looksn;

    usesEnergy = true;
	minEnergy = 30;
    fireEnergy = 15; // 5 * 10 = 50/sec
//    defaultModeFireSound = "LaserFireSound";
   pilotHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::Starhawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 2, "rotation"];

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSound[0]                    = SniperRifleSwitchSound;
   stateSequence[0]                 = "deploy";
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   stateSound[3]        = LaserSpinupSound;
   //
   stateTimeoutValue[3]          = 1.5;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
//   stateSequence[4]            = "Fire";
//   stateSequenceRandomFlash[4] = true;
   stateSpinThread[4]       = FullSpeed;
   stateSound[4]            = LaserFireSound;
   //stateRecoil[4]           = LightRecoil;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
//   stateEjectShell[4]       = true;
   //
   stateTimeoutValue[4]          = 0.1;
   stateTransitionOnTimeout[4]   = "Fire";
   stateTransitionOnTriggerUp[4] = "Spindown";
   stateTransitionOnNoAmmo[4]    = "EmptySpindown";

   //--------------------------------------
   stateName[5]       = "Spindown";
   stateSound[5]      = LaserSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 2.0;
   stateWaitForTimeout[5]          = true;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = LaserSpinDownSound;
   stateSpinThread[6] = SpinDown;
   //
   stateTimeoutValue[6]        = 0.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = SniperRifleDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateTransitionOnTimeout[7] = "NoAmmo";
};

// Inherited Defs
datablock ShapeBaseImageData(LLaserCannonTUS2) : LLaserCannonSTA2
{
   pilotHeadTracking = false;
   
   offset = $VehicleHardpoints[$VehicleID::TurretPole, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretPole, 2, "rotation"];
};

datablock ShapeBaseImageData(LLaserCannonTUT4) : LLaserCannonSTA2
{
   pilotHeadTracking = true;

   mountPoint = 1;

   offset = "0 0 0";
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 4, "rotation"];
};

datablock ShapeBaseImageData(LLaserCannonSTR2) : LLaserCannonSTA2
{
   pilotHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::Stryker, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Stryker, 2, "rotation"];
};

datablock ShapeBaseImageData(LLaserCannonRET2) : LLaserCannonSTA2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Retaliator, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Retaliator, 2, "rotation"];
};

datablock ShapeBaseImageData(LLaserCannonTPD2) : LLaserCannonSTA2
{
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 2, "rotation"];
};

datablock ShapeBaseImageData(LLaserCannonTPD3) : LLaserCannonSTA2
{
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 3, "rotation"];
};

function LLaserCannon::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "";
    %this.ammoUse = 0;
    %this.fireTimeout = 0;
    
    // Reticle data
    %this.reticleTex = "gui/RET_blaster";
    %this.showReticleFrame = true;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LLaserCannon", "Beam Laser Cannon", "Close range high damage - 75m optimal range, beam damage falls off", $VehicleList::Leviathan | $VehicleList::Retaliator | $VehicleList::HoverJeep | $VehicleList::Stryker | $VehicleList::Wolfhound | $VehicleList::Annihilator | $VehicleList::Goodship, $VHardpointSize::Cannon, $VHardpointType::Energy);
