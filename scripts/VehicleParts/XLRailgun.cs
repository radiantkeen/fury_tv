// Sound, Projectile, and Explosion blocks
datablock AudioProfile(RailgunFireSound)
{
   filename    = "fx/weapon/railgun_fire.wav";
   description = AudioAmbient3d;
   preload = true;
};

datablock AudioProfile(RailgunReloadSound)
{
   filename    = "fx/weapon/railgun_reload.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(RailgunExpSound)
{
   filename    = "fx/explosion/railgun_hit.wav";
   description = AudioAmbient3d;
   preload = true;
};

datablock ExplosionData(RailgunExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   playSpeed = 1.5;
   soundProfile   = RailgunExpSound;
   faceViewer = true;

   sizes[0] = "1.5 1.5 1.5";
   sizes[1] = "1.5 1.5 1.5";
   sizes[2] = "1.5 1.5 1.5";

   emitter[0] = MissileExplosionSmokeEmitter;

   debris = MissileSpikeDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 170;
   debrisNum = 8;
   debrisNumVariance = 6;
   debrisVelocity = 50;
   debrisVelocityVariance = 10;

   shakeCamera = true;
   camShakeFreq = "6.0 7.0 7.0";
   camShakeAmp = "70.0 70.0 70.0";
   camShakeDuration = 0.5;
   camShakeRadius = 7.0;
};

datablock LinearProjectileData(VRailgunShot)
{
   projectileShapeName = "herc_gauss_projectile.dts";
   
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 30.0;
   damageRadius        = 2.0;
   radiusDamageType    = $DamageType::Railgun;
   kickBackStrength    = 5000;
   
   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Kinetic;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "RailgunExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

//   baseEmitter         = RailgunSmokeEmitter;
//   delayEmitter        = MissileFireEmitter;
   bubbleEmitter       = MortarBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 1000;
   wetVelocity       = 1000;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1000;
};

// Primary Definition
datablock ShapeBaseImageData(XLRailgunTUT2)
{
   shapeFile = "tac_turret_tank_barrelmortar.dts";
   mountPoint = 0;
   offset = $VehicleHardpoints[$VehicleID::TankTurret, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 2, "rotation"];

   projectile = VRailgunShot;
   projectileType = LinearProjectile;
   ammo = "VLRailgunAmmo";
   updatePilotAmmo = true;
   sharedResourcePool = true;

   usesEnergy = false;
   useMountEnergy = false;
   fireEnergy = 150.0;
   minEnergy = 150.0;
   useCapacitor = false;
   
   // State transitions
   stateName[0]                        = "Activate";
   stateTransitionOnNotLoaded[0]       = "Dead";
   stateTransitionOnLoaded[0]          = "ActivateReady";

   stateName[1]                        = "ActivateReady";
   stateSequence[1]                    = "ambient";
//   stateSound[1]                       = AssaultTurretActivateSound;
   stateTimeoutValue[1]                = 1.0;
   stateTransitionOnTimeout[1]         = "Ready";
   stateTransitionOnNotLoaded[1]       = "Deactivate";

   stateName[2]                        = "Ready";
   stateTransitionOnNotLoaded[2]       = "Deactivate";
   stateTransitionOnNoAmmo[2]          = "NoAmmo";
   stateTransitionOnTriggerDown[2]     = "Fire";

   stateName[3]                        = "Fire";
   stateSequence[3]                    = "Fire";
   stateTransitionOnTimeout[3]         = "Reload";
   stateTimeoutValue[3]                = 6.0;
   stateFire[3]                        = true;
   stateRecoil[3]                      = LightRecoil;
   stateAllowImageChange[3]            = false;
   stateSound[3]                       = RailgunFireSound;
   stateScript[3]                      = "onFire";

   stateName[4]                        = "Reload";
   stateSequence[4]                    = "Reload";
   stateTimeoutValue[4]                = 1.0;
   stateAllowImageChange[4]            = false;
   stateSound[4]                       = RailgunReloadSound;
   stateTransitionOnTimeout[4]         = "Ready";
   //stateTransitionOnNoAmmo[4]          = "NoAmmo";
   stateWaitForTimeout[4]              = true;

   stateName[5]                        = "Deactivate";
   stateDirection[5]                   = false;
   stateSequence[5]                    = "Activate";
   stateTimeoutValue[5]                = 1.0;
   stateTransitionOnLoaded[5]          = "ActivateReady";
   stateTransitionOnTimeout[5]         = "Dead";

   stateName[6]                        = "Dead";
   stateTransitionOnLoaded[6]          = "ActivateReady";
   stateTransitionOnTriggerDown[6]     = "DryFire";

   stateName[7]                        = "DryFire";
   stateSound[7]                       = AssaultMortarDryFireSound;
   stateTimeoutValue[7]                = 1.0;
   stateTransitionOnTimeout[7]         = "NoAmmo";

   stateName[8]                        = "NoAmmo";
   stateSequence[8]                    = "NoAmmo";
   stateTransitionOnAmmo[8]            = "Reload";
   stateTransitionOnTriggerDown[8]     = "DryFire";
};

// Inherited Defs

function XLRailgun::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = true;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 30;
    %this.ammo = "VLRailgunAmmo";
    %this.ammoUse = 1;
    %this.fireTimeout = 3500;
    
    // Reticle data
    %this.reticleTex = "gui/hud_ret_sniper";
    %this.showReticleFrame = false;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "XLRailgun", "Railgun", "Near-instant projectile that deals significant damage to a vehicle", $VehicleList::Guillotine | $VehicleList::Leviathan, $VHardpointSize::Siege, $VHardpointType::Ballistic);
