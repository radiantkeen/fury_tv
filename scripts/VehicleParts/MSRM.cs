datablock ShapeBaseImageData(MSRMJEP6) : EngineAPEImage
{
   mountPoint = 0;

   shapeFile = "vehicle_missile_yellow.dts"; // pyro_missile

   offset = $VehicleHardpoints[$VehicleID::HoverJeep, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::HoverJeep, 6, "rotation"];
};

datablock ShapeBaseImageData(MSRMJEP7) : MSRMJEP6
{
   offset = $VehicleHardpoints[$VehicleID::HoverJeep, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::HoverJeep, 7, "rotation"];
};

datablock ShapeBaseImageData(MSRMCAT4) : MSRMJEP6
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 4, "rotation"];
};

datablock ShapeBaseImageData(MSRMCAT5) : MSRMJEP6
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 5, "rotation"];
};

datablock ShapeBaseImageData(MSRMCAT6) : MSRMJEP6
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 6, "rotation"];
};

datablock ShapeBaseImageData(MSRMCAT7) : MSRMJEP6
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 7, "rotation"];
};

datablock ShapeBaseImageData(MSRMRET6) : MSRMJEP6
{
   offset = $VehicleHardpoints[$VehicleID::Retaliator, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Retaliator, 6, "rotation"];
};

datablock ShapeBaseImageData(MSRMRET7) : MSRMJEP6
{
   offset = $VehicleHardpoints[$VehicleID::Retaliator, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Retaliator, 7, "rotation"];
};

datablock ShapeBaseImageData(MSRMWOL6) : MSRMJEP6
{
   offset = $VehicleHardpoints[$VehicleID::Wolfhound, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Wolfhound, 6, "rotation"];
};

datablock ShapeBaseImageData(MSRMWOL7) : MSRMJEP6
{
   offset = $VehicleHardpoints[$VehicleID::Wolfhound, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Wolfhound, 7, "rotation"];
};

datablock BombProjectileData(SRMMissileDeploy)
{
   projectileShapeName  = "vehicle_missile_yellow.dts";
   emitterDelay         = -1;
   directDamage         = 0.0;
   hasDamageRadius      = true;
   indirectDamage       = 25.0;
   damageRadius         = 30;
   radiusDamageType     = $DamageType::Missile;
   directDamageType     = $DamageType::Missile;
   kickBackStrength     = 2250;  // z0dd - ZOD, 4/25/02. Was 2500

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Explosive;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   explosion            = "BreacherMExplosion";
   velInheritFactor     = 1.0;

   grenadeElasticity    = 0.25;
   grenadeFriction      = 0.4;
   armingDelayMS        = 2000;
   muzzleVelocity       = 15.0;
   drag                 = 0.3;
   gravityMod		    = 1.0;

   minRotSpeed          = "0.0 45.0 0.0";
   maxRotSpeed          = "0.0 45.0 0.0";

   scale                = "1.0 1.0 1.0";

   sound                = "";
   
   missileNameBase      = "SRMMissile";
};

datablock LinearProjectileData(SRMMissileDumbfire)
{
//   scale = "1.5 1.5 1.5";
   projectileShapeName = "vehicle_missile_yellow.dts";
   emitterDelay        = -1;
   directDamage        = 0;
   directDamageType    = $DamageType::Missile;
   hasDamageRadius     = true;
   indirectDamage      = 25.0;
   damageRadius        = 30;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 3200;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Explosive;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "BreacherMExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = WildcatJetEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 125;
   wetVelocity       = 50;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 3000;
   lifetimeMS        = 3000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 3000;

   sound = "LargeMissileProjectileSound";
   
   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 7.0;
   lightColor  = "0.4 0.15 0.1";
};

datablock SeekerProjectileData(SRMMissile) : ShoulderMissile
{
//   scale = "5.0 5.0 5.0";
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "vehicle_missile_yellow.dts";
   directDamage        = 0;
   directDamageType    = $DamageType::Missile;
   hasDamageRadius     = true;
   indirectDamage      = 25;
   damageRadius        = 30;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 3200;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Explosive;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "BreacherMExplosion";
//   underwaterExplosion = UnderwaterHandGrenadeExplosion;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = WildcatJetEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   lifetimeMS          = 3000;
   muzzleVelocity      = 125.0;
   maxVelocity         = 150.0;
   turningSpeed        = 180.0;
   acceleration        = 50.0;

   proximityRadius     = 4;

   flareDistance = 10;
   flareAngle    = 10;

//   sound = ScoutThrustSound;
   sound = "LargeMissileProjectileSound";

   hasLight    = true;
   lightRadius = 7.0;
   lightColor  = "0.4 0.15 0.1";

   useFlechette = false;
   explodeOnWaterImpact = false;

   // Used for vehicle-mounted missile system
   isVehicleMissile = true;
};

function MSRM::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "";
    %this.rearmTime = 7000;
    
    %this.missileName = "SRMMissile";
    %this.missileSize = $VHardpointSize::Large;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "MSRM", "Short Range Missile", "Very high maneuverability and damage by sacrificing total flight time (3 sec).", $VehicleList::HoverJeep | $VehicleList::Catapult | $VehicleList::Retaliator | $VehicleList::Wolfhound | $VehicleList::Scorpion, $VHardpointSize::Missile, $VHardpointType::Missile);
