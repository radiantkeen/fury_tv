datablock AudioProfile(RotaryRFireSound)
{
   filename    = "fx/weapon/rotary_rocket_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock LinearProjectileData(RotaryRocket)
{
//   scale = "1.5 1.5 1.5";
   projectileShapeName = "vehicle_missile_red.dts";
   emitterDelay        = -1;
   directDamage        = 0;
   directDamageType    = $DamageType::Missile;
   hasDamageRadius     = true;
   indirectDamage      = 1.25;
   damageRadius        = 5;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 750;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Explosive;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "RocketExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = WildcatJetEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 150;
   wetVelocity       = 100;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 3000;
   lifetimeMS        = 3000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 3000;

   sound = "LargeMissileProjectileSound";

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 7.0;
   lightColor  = "0.4 0.15 0.1";
};

// Primary Definition
datablock ShapeBaseImageData(XLRotaryRocketTUT2)
{
   shapeFile = "herc_ac_right.dts";
   mountPoint = 0;
   offset = "-0.75 -0.25 0";
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 2, "rotation"];

   projectile = RotaryRocket;
   projectileType = LinearProjectile;
   ammo = "VLRotaryRocketAmmo";
   updatePilotAmmo = true;
   sharedResourcePool = true;

   projectileSpread = 2.0; // z0dd - ZOD, 8/6/02. Was: 8.0 / 1000.0

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   //
   stateTimeoutValue[3]          = 0.5;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
   stateSequence[4]            = "Fire";
   stateSequenceRandomFlash[4] = true;
   stateSpinThread[4]       = FullSpeed;
   stateSound[4]            = RotaryRFireSound;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateEjectShell[4]       = true;
   //
   stateTimeoutValue[4]          = 0.25;
   stateTransitionOnTimeout[4]   = "Reload";
   stateTransitionOnTriggerUp[4] = "Spindown";
   stateTransitionOnNoAmmo[4]    = "EmptySpindown";

   //--------------------------------------
   stateName[5]       = "Spindown";
//   stateSound[5]      = ChaingunSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 1.0;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
//   stateSound[6]      = ChaingunSpinDownSound;
   stateSpinThread[6] = SpinDown;
   //
   stateTimeoutValue[6]        = 0.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ChaingunDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateScript[7]           = "onDryFire";
   stateTransitionOnTimeout[7] = "NoAmmo";
   
   stateName[8]       = "Reload";
   stateSpinThread[8]       = FullSpeed;
   stateTimeoutValue[8]            = 0.05;
   stateTransitionOnTimeout[8]   = "Fire";
   stateTransitionOnTriggerUp[8] = "Spindown";
   stateTransitionOnNotLoaded[8] = "EmptySpindown";
   stateTransitionOnNoAmmo[8]    = "EmptySpindown";
};

// Inherited Defs

function XLRotaryRocket::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = true;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 250;
    %this.ammo = "VLRotaryRocketAmmo";
    %this.ammoUse = 1;
    %this.fireTimeout = 0;
    
    // Reticle data
    %this.reticleTex = "gui/RET_chaingun";
    %this.showReticleFrame = true;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "XLRotaryRocket", "Rotary Rocket Launcher", "Rapid fires rockets, what more could you ask for?", $VehicleList::Guillotine | $VehicleList::Leviathan, $VHardpointSize::Siege, $VHardpointType::Ballistic);
