function CResupplySystem::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Push;
    
    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = 0;
    %this.deltaRegen = 0;
}

function CResupplySystem::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = true;
    %vehicle.resupplySystem = true;
}

function CResupplySystem::onTurretTriggerPack(%this, %vehicle, %player, %turret)
{
    %time = getSimTime();
    %slot = %vehicle.getObjectSlot(%turret);

    if(%time < %vehicle.slotInvTimeout[%slot])
    {
        %cdTimeSec = mCeil((%vehicle.slotInvTimeout[%slot] - %time) / 1000);

        messageClient(%player.client, 'MsgVMTResupplyFail', '\c2Turret resupply recently used, try again in %1 seconds.', %cdTimeSec);
        %player.playAudio(0, "StationAccessDeniedSound");
        return;
    }

    %vehicle.slotInvTimeout[%slot] = %time + 300000;
    %turret.playAudio(0, "MobileBaseInventoryActivateSound");
    %turret.setCloaked(true);
    %turret.schedule(500, "setCloaked", false);
    
    %data = %vehicle.getDatablock();
    
//    echo("rearming turret" SPC %turret SPC %turret.getName() SPC %turret.getClassName());
    for(%i = 0; %i < %turret.wepCount; %i++)
    {
        %part = %turret.weapon[%i];
        %hdp = %turret.hardpointRef[%i];

        if(%part.ammo !$= "")
            %vehicle.ammoCache[%part.ammo] = 0;

        %part.schedule(0, "reArm", %data, %vehicle, %player, %hdp);
//        echo("rearming" SPC %part);
    }
    
    schedule(32, %turret, "serverCmdSetVehicleWeapon", %player.client, 0);
    messageClient(%player.client, 'MsgTurAmmoActivate', '\c2Turret resupply function activated, 5 minute cooldown.');
}

function CResupplySystem::onTriggerMine(%this, %vehicle, %player, %slot)
{
    if(%slot > 0 && !%player.isWeaponOperator())
    {
        %time = getSimTime();

        if(%time < %vehicle.slotInvTimeout[%slot])
        {
            %cdTimeSec = mCeil((%vehicle.slotInvTimeout[%slot] - %time) / 1000);

            messageClient(%player.client, 'MsgVMResupplyFail', '\c2Inventory station recently used, try again in %1 seconds.', %cdTimeSec);
            %player.playAudio(0, "StationAccessDeniedSound");
            return;
        }

        %vehicle.slotInvTimeout[%slot] = %time + 300000;
        %player.lastWeapon = ( %player.getMountedImage($WeaponSlot) == 0 ) ? "" : %player.getMountedImage($WeaponSlot).item;

        %player.unmountImage($WeaponSlot);
        %player.playAudio(0, "MobileBaseInventoryActivateSound");
        %player.setCloaked(true);
        %player.schedule(500, "setCloaked", false);

        buyFavorites(%player.client, 0);
        messageClient(%player.client, 'MsgVehInvActivate', '\c2Resupply function activated, 5 minute cooldown.');
    }
}

function CResupplySystem::reArm(%this, %data, %vehicle, %player, %slot)
{
    for(%i = 0; %i < 16; %i++)
        %vehicle.slotInvTimeout[%i] = 0;
}

VehiclePart.registerVehiclePart($VehiclePartType::Module, "CResupplySystem", "Resupply System", "[Passive] Mounts an inventory station in each passenger seat and turret - turrets activate pack to use", $VehicleList::HasCrew, $VHardpointSize::Internal, $VHardpointType::Omni);
