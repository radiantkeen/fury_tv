// Sound, Projectile, and Explosion blocks

datablock LinearFlareProjectileData(GPlasmaBolt) : PlasmaCannonBolt
{
   projectileShapeName = "plasmabolt.dts";
   scale               = "4.0 4.0 4.0";
   faceViewer          = true;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 2.4;
   damageRadius        = 8.0;
   kickBackStrength    = 750.0;
   radiusDamageType    = $DamageType::PlasmaCannon;

   explosion           = "PlasmaCannonBoltExplosion";
   splash              = PlasmaSplash;
   
   flags               = $Projectile::PlaysHitSound | $Projectile::Plasma;
   ticking             = false;
   headshotMultiplier  = 1.0;

   dryVelocity       = 250.0; // z0dd - ZOD, 7/20/02. Faster plasma projectile. was 55
   wetVelocity       = -1;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 2000;
   lifetimeMS        = 3000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   //activateDelayMS = 100;
   activateDelayMS = -1;

   size[0]           = 0.2;
   size[1]           = 0.5;
   size[2]           = 0.1;

   baseEmitter         = FireboltFireEmitter;
   delayEmitter        = FireboltSmokeEmitter;

   numFlares         = 35;
   flareColor        = "1 0.75 0.25";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

	sound        = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 3.0;
   lightColor  = "1 0.75 0.25";
};

// Primary Definition
datablock ShapeBaseImageData(GPlasmaVIP2)
{
   className = WeaponImage;
   shapeFile = "weapon_flamer.dts";
   mountPoint = "0";
   offset = $VehicleHardpoints[$VehicleID::Viper, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Viper, 2, "rotation"];

   ammo = "VGPlasmaAmmo";
   updatePilotAmmo = true;
   sharedResourcePool = true;
   projectile = GPlasmaBolt;
   projectileType = LinearFlareProjectile;
   emap = true;

   fireTimeout = 625;

   // State transitions
   stateName[0]                  = "Activate";
   stateTransitionOnNotLoaded[0] = "Dead";
   stateTransitionOnLoaded[0]    = "ActivateReady";

   stateName[1]                  = "ActivateReady";
   stateSequence[1]              = "Deploy";
   stateTimeoutValue[1]          = 1;
   stateTransitionOnTimeout[1]   = "Ready";
   stateTransitionOnNotLoaded[1] = "Deactivate";
   stateTransitionOnNoAmmo[1]    = "NoAmmo";

   stateName[2]                    = "Ready";
   stateTransitionOnNotLoaded[2]   = "Deactivate";
   stateTransitionOnTriggerDown[2] = "Fire";
   stateTransitionOnNoAmmo[2]      = "NoAmmo";

   stateName[3]                = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3]        = 1.0;
   stateFire[3]                = true;
   stateRecoil[3]              = LightRecoil;
   stateAllowImageChange[3]    = false;
   stateSequence[3]            = "Fire";
   stateSound[3]               = PlasmaCannonFireSound;
   stateScript[3]              = "onFire";

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 0.25;
   stateAllowImageChange[4]      = false;
   stateTransitionOnTimeout[4]   = "Ready";
   stateTransitionOnNotLoaded[4] = "Deactivate";
    stateTransitionOnNoAmmo[4]    = "NoAmmo";

   stateName[5]                = "Deactivate";
   stateSequence[5]            = "Activate";
   stateDirection[5]           = false;
   stateTimeoutValue[5]        = 1;
   stateTransitionOnLoaded[5]  = "ActivateReady";
   stateTransitionOnTimeout[5] = "Dead";

   stateName[6]               = "Dead";
   stateTransitionOnLoaded[6] = "ActivateReady";

   stateName[7]             = "NoAmmo";
   stateTransitionOnAmmo[7] = "Reload";
   stateSequence[7]         = "NoAmmo";
};

// Inherited Defs
datablock ShapeBaseImageData(GPlasmaSKY0) : GPlasmaVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Skycutter, 0, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Skycutter, 0, "rotation"];
};

datablock ShapeBaseImageData(GPlasmaAIR2) : GPlasmaVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Airhawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Airhawk, 2, "rotation"];
};

datablock ShapeBaseImageData(GPlasmaAIR3) : GPlasmaVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Airhawk, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Airhawk, 3, "rotation"];
};

datablock ShapeBaseImageData(GPlasmaTUT4) : GPlasmaVIP2
{
   mountPoint = 1;
   
   offset = $VehicleHardpoints[$VehicleID::TankTurret, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 4, "rotation"];
};

datablock ShapeBaseImageData(GPlasmaTUT5) : GPlasmaVIP2
{
   mountPoint = 1;
   
   offset = $VehicleHardpoints[$VehicleID::TankTurret, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 5, "rotation"];
};

datablock ShapeBaseImageData(GPlasmaTUD4) : GPlasmaVIP2
{
   offset = "0.45 0.2 0";
   rotation = $VehicleHardpoints[$VehicleID::TurretPole, 4, "rotation"];
};

datablock ShapeBaseImageData(GPlasmaTUD5) : GPlasmaVIP2
{
   offset = "-0.45 0.2 0";
   rotation = $VehicleHardpoints[$VehicleID::TurretPole, 5, "rotation"];
};

datablock ShapeBaseImageData(GPlasmaSHR4) : GPlasmaVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 4, "rotation"];
};

datablock ShapeBaseImageData(GPlasmaSHR5) : GPlasmaVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 5, "rotation"];
};

datablock ShapeBaseImageData(GPlasmaSHX2) : GPlasmaVIP2
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 2, "rotation"];
};

datablock ShapeBaseImageData(GPlasmaSHX3) : GPlasmaVIP2
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 3, "rotation"];
   
   stateSound[3]            = "";
};

datablock ShapeBaseImageData(GPlasmaSHX4) : GPlasmaVIP2
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 4, "rotation"];
};

datablock ShapeBaseImageData(GPlasmaSHX5) : GPlasmaVIP2
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 5, "rotation"];

   stateSound[3]            = "";
};

datablock ShapeBaseImageData(GPlasmaSSH4) : GPlasmaVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 4, "rotation"];
};

datablock ShapeBaseImageData(GPlasmaSSH5) : GPlasmaVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 5, "rotation"];
};

datablock ShapeBaseImageData(GPlasmaSTA4) : GPlasmaVIP2
{
   pilotHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::Starhawk, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 4, "rotation"];
};

datablock ShapeBaseImageData(GPlasmaSTA5) : GPlasmaVIP2
{
   pilotHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::Starhawk, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 5, "rotation"];
};

datablock ShapeBaseImageData(GPlasmaFIB6) : GPlasmaVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 6, "rotation"];
};

datablock ShapeBaseImageData(GPlasmaFIB7) : GPlasmaVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 7, "rotation"];
};

datablock ShapeBaseImageData(GPlasmaJEP2) : GPlasmaVIP2
{
   offset = $VehicleHardpoints[$VehicleID::HoverJeep, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::HoverJeep, 2, "rotation"];
   
   pilotHeadTracking = true;
};

datablock ShapeBaseImageData(GPlasmaJEP3) : GPlasmaVIP2
{
   offset = $VehicleHardpoints[$VehicleID::HoverJeep, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::HoverJeep, 3, "rotation"];
   
   pilotHeadTracking = true;
};

datablock ShapeBaseImageData(GPlasmaUBT2) : GPlasmaVIP2
{
//   shapeFile = "turret_chopper_barrel.dts";
   mountPoint = 2; // L

   offset = "0.0 -0.2 -0.1";
   rotation = $VehicleHardpoints[$VehicleID::Underbelly, 2, "rotation"];
};

datablock ShapeBaseImageData(GPlasmaUBT3) : GPlasmaVIP2
{
//   shapeFile = "turret_chopper_barrel.dts";
   mountPoint = 3; // L

   offset = "0.0 -0.2 -0.1";
   rotation = $VehicleHardpoints[$VehicleID::Underbelly, 3, "rotation"];
};

datablock ShapeBaseImageData(GPlasmaTPQ4) : GPlasmaVIP2
{
   scorpionHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 4, "rotation"];
};

datablock ShapeBaseImageData(GPlasmaTPQ5) : GPlasmaVIP2
{
   scorpionHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 5, "rotation"];
};

datablock ShapeBaseImageData(GPlasmaTPQ6) : GPlasmaVIP2
{
   scorpionHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 6, "rotation"];
};

datablock ShapeBaseImageData(GPlasmaTPQ7) : GPlasmaVIP2
{
   scorpionHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 7, "rotation"];
};

function GPlasma::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = true;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 150;
    %this.ammo = "VGPlasmaAmmo";
    %this.ammoUse = 1;
    %this.fireTimeout = 625;
    
    // Reticle data
    %this.reticleTex = "gui/RET_Plasma";
    %this.showReticleFrame = false;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "GPlasma", "Plasma Flametongue", "Vehicle-sized plasma weapon shooting large fireballs", $VehicleList::Guillotine | $VehicleList::Starhawk | $VehicleList::Firebird | $VehicleList::Skycutter | $VehicleList::Airhawk | $VehicleList::Wildcat | $VehicleList::Leviathan | $VehicleList::ShrikeX | $VehicleList::Shrike | $VehicleList::SuperShrike | $VehicleList::HoverJeep | $VehicleList::Tomahawk | $VehicleList::Retaliator | $VehicleList::Stryker | $VehicleList::Annihilator | $VehicleList::Wolfhound | $VehicleList::HoverJeep | $VehicleList::Stryker | $VehicleList::Leviathan | $VehicleList::Scorpion, $VHardpointSize::Gun, $VHardpointType::Ballistic);
