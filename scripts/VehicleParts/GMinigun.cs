// Sound, Projectile, and Explosion blocks

// Primary Definition
datablock ShapeBaseImageData(GMinigunVIP2)
{
   className = WeaponImage;
   shapeFile = "Minigun.dts";
   mountPoint = "0";
   offset = $VehicleHardpoints[$VehicleID::Viper, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Viper, 2, "rotation"];

   ammo = "VGMinigunAmmo";
   updatePilotAmmo = true;
   sharedResourcePool = true;
   projectile = BolterBullet;
   projectileType = LinearProjectile;
   emap = true;

   casing              = ScattergunDebris;
   shellExitDir        = "1.0 0.3 1.0";
   shellExitOffset     = "0.15 -0.56 -0.1";
   shellExitVariance   = 15.0;
   shellVelocity       = 6.0;

   projectileSpread = 8.0; // z0dd - ZOD, 8/6/02. Was: 8.0 / 1000.0

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateSound[0]            = ChaingunSwitchSound;
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   stateSound[3]        = MinigunSpinUpSound;
   //
   stateTimeoutValue[3]          = 0.5;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
   stateSequence[4]            = "Fire";
   stateSequenceRandomFlash[4] = true;
   stateSpinThread[4]       = FullSpeed;
   stateSound[4]            = VulcanFireSound;
   //stateRecoil[4]           = LightRecoil;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateEjectShell[4]       = true;
   //
   stateTimeoutValue[4]          = 0.1;
   stateTransitionOnTimeout[4]   = "Fire";
   stateTransitionOnTriggerUp[4] = "Spindown";
   stateTransitionOnNoAmmo[4]    = "EmptySpindown";

   //--------------------------------------
   stateName[5]       = "Spindown";
   stateSound[5]      = MinigunSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 1.0;
   stateWaitForTimeout[5]          = true;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = ChaingunSpinDownSound;
   stateSpinThread[6] = SpinDown;
   //
   stateTimeoutValue[6]        = 0.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ChaingunDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateTransitionOnTimeout[7] = "NoAmmo";
};

// Inherited Defs
datablock ShapeBaseImageData(GMinigunSKY0) : GMinigunVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Skycutter, 0, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Skycutter, 0, "rotation"];
};

datablock ShapeBaseImageData(GMinigunAIR2) : GMinigunVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Airhawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Airhawk, 2, "rotation"];
};

datablock ShapeBaseImageData(GMinigunAIR3) : GMinigunVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Airhawk, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Airhawk, 3, "rotation"];
};

datablock ShapeBaseImageData(GMinigunTUT4) : GMinigunVIP2
{
   mountPoint = 1;
   
   offset = $VehicleHardpoints[$VehicleID::TankTurret, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 4, "rotation"];
};

datablock ShapeBaseImageData(GMinigunTUT5) : GMinigunVIP2
{
   mountPoint = 1;
   
   offset = $VehicleHardpoints[$VehicleID::TankTurret, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 5, "rotation"];
};

datablock ShapeBaseImageData(GMinigunTUD4) : GMinigunVIP2
{
   offset = "0.4 1.25 -0.25";
   rotation = "1 0 0 0";
};

datablock ShapeBaseImageData(GMinigunTUD5) : GMinigunVIP2
{
   offset = "-0.7 1.25 -0.25";
   rotation = "1 0 0 0";
};

datablock ShapeBaseImageData(GMinigunSHR4) : GMinigunVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 4, "rotation"];
};

datablock ShapeBaseImageData(GMinigunSHR5) : GMinigunVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 5, "rotation"];
};

datablock ShapeBaseImageData(GMinigunSSH4) : GMinigunVIP2
{
   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 4, "rotation"];
};

datablock ShapeBaseImageData(GMinigunSSH5) : GMinigunVIP2
{
   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 5, "rotation"];
};

datablock ShapeBaseImageData(GMinigunSHX2) : GMinigunVIP2
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 2, "rotation"];
};

datablock ShapeBaseImageData(GMinigunSHX3) : GMinigunVIP2
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 3, "rotation"];

   stateSound[4]            = "";
};

datablock ShapeBaseImageData(GMinigunSHX4) : GMinigunVIP2
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 4, "rotation"];
};

datablock ShapeBaseImageData(GMinigunSHX5) : GMinigunVIP2
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 5, "rotation"];

   stateSound[4]            = "";
};

datablock ShapeBaseImageData(GMinigunSTA4) : GMinigunVIP2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Starhawk, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 4, "rotation"];
};

datablock ShapeBaseImageData(GMinigunSTA5) : GMinigunVIP2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Starhawk, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 5, "rotation"];
};

datablock ShapeBaseImageData(GMinigunFIB6) : GMinigunVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 6, "rotation"];
};

datablock ShapeBaseImageData(GMinigunFIB7) : GMinigunVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 7, "rotation"];
};

datablock ShapeBaseImageData(GMinigunJEP2) : GMinigunVIP2
{
   offset = $VehicleHardpoints[$VehicleID::HoverJeep, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::HoverJeep, 2, "rotation"];

   pilotHeadTracking = true;
};

datablock ShapeBaseImageData(GMinigunJEP3) : GMinigunVIP2
{
   offset = $VehicleHardpoints[$VehicleID::HoverJeep, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::HoverJeep, 3, "rotation"];

   pilotHeadTracking = true;
};

datablock ShapeBaseImageData(GMinigunUBT2) : GMinigunVIP2
{
//   shapeFile = "weapon_chaingun.dts";
   mountPoint = 2; // 2

   offset = "-0.095 -0.2 -0.1";
   rotation = $VehicleHardpoints[$VehicleID::Underbelly, 2, "rotation"];
};

datablock ShapeBaseImageData(GMinigunUBT3) : GMinigunVIP2
{
//   shapeFile = "weapon_chaingun.dts";
   mountPoint = 3; // 3

   offset = "-0.09 -0.2 -0.1";
   rotation = $VehicleHardpoints[$VehicleID::Underbelly, 3, "rotation"];
};

datablock ShapeBaseImageData(GMinigunTPQ4) : GMinigunVIP2
{
   scorpionHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 4, "rotation"];
};

datablock ShapeBaseImageData(GMinigunTPQ5) : GMinigunVIP2
{
   scorpionHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 5, "rotation"];
};

datablock ShapeBaseImageData(GMinigunTPQ6) : GMinigunVIP2
{
   scorpionHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 6, "rotation"];
};

datablock ShapeBaseImageData(GMinigunTPQ7) : GMinigunVIP2
{
   scorpionHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 7, "rotation"];
};

function GMinigun::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = true;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 500;
    %this.ammo = "VGMinigunAmmo";
    %this.ammoUse = 1;
    %this.fireTimeout = 0;
    
    // Reticle data
    %this.reticleTex = "gui/RET_chaingun";
    %this.showReticleFrame = true;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "GMinigun", "Minigun", "Delivers twice as many bullets as the Vulcan with an 8* spread", $VehicleList::Guillotine | $VehicleList::Starhawk | $VehicleList::Firebird | $VehicleList::Skycutter | $VehicleList::Airhawk | $VehicleList::Wildcat | $VehicleList::Leviathan | $VehicleList::ShrikeX | $VehicleList::Shrike | $VehicleList::SuperShrike | $VehicleList::HoverJeep | $VehicleList::Tomahawk | $VehicleList::Retaliator | $VehicleList::Stryker | $VehicleList::Annihilator | $VehicleList::Wolfhound | $VehicleList::HoverJeep | $VehicleList::Stryker | $VehicleList::Leviathan | $VehicleList::Scorpion, $VHardpointSize::Gun, $VHardpointType::Ballistic);
