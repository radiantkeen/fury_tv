datablock AudioProfile(WarpLoopSound)
{
     filename = "fx/vehicle/warp_active.wav";
     description = AudioDefaultLooping3d;
     preload = true;
};

datablock AudioProfile(WarpInitializeSound)
{
   filename = "fx/vehicle/warp_initiate.wav";
   description = AudioExplosion3d;
   preload = true;
};

datablock AudioProfile(WarpInitialize2Sound)
{
   filename = "fx/vehicle/warp_engage.wav";
   description = AudioExplosion3d;
   preload = true;
};

datablock AudioProfile(WarpPowerDownSound)
{
   filename = "fx/vehicle/warp_disengage.wav";
   description = AudioExplosion3d;
   preload = true;
};

datablock AudioProfile(WarpLaunchSound)
{
   filename = "fx/misc/warp_end_arrive.wav";
   description = AudioExplosion3d;
   preload = true;
};

datablock StaticShapeData(WarpBubble)
{
   shapeFile = "grenade_flare.dts";
};

datablock StaticShapeData(HoleEntry)
{
   shapeFile = "energy_explosion.dts";
};

datablock ShockLanceProjectileData(WarpFXStage1)
{
   directDamage        = 0;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 0;
   velInheritFactor    = 0;
   sound               = "";

   zapDuration = 6;
   impulse = 0;
   boltLength = 14.0;
   extension = 14.0;            // script variable indicating distance you can shock people from
   lightningFreq = 50.0;
   lightningDensity = 18.0;
   lightningAmp = 0.25;
   lightningWidth = 0.05;


   boltSpeed[0] = 2.0;
   boltSpeed[1] = -0.5;

   texWrap[0] = 1.5;
   texWrap[1] = 1.5;

   startWidth[0] = 0.3;
   endWidth[0] = 0.6;
   startWidth[1] = 0.3;
   endWidth[1] = 0.6;

   texture[0] = "special/jammermap";
   texture[1] = "special/Shocklance_effect02";
   texture[2] = "special/cloaktexture";
   texture[3] = "special/jammermap";
};

datablock ShockLanceProjectileData(WarpFXStage2)
{
   directDamage        = 0.0;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 0;
   velInheritFactor    = 0;
   sound               = "";

   zapDuration = 1.0;
   impulse = 0;
   boltLength = 35.0;
   extension = 35.0;            // script variable indicating distance you can shock people from
   lightningFreq = 25.0;
   lightningDensity = 3.0;
   lightningAmp = 0.25;
   lightningWidth = 0.05;

//   shockwave = ShocklanceHit;

   boltSpeed[0] = 2.0;
   boltSpeed[1] = -0.5;

   texWrap[0] = 0.743978;
   texWrap[1] = 2.038738;

   startWidth[0] = 0.3;
   endWidth[0] = 0.6;
   startWidth[1] = 0.3;
   endWidth[1] = 0.6;

   texture[0] = "special/skyLightning"; // cloaktexture
   texture[1] = "special/Shocklance_effect01";
   texture[2] = "special/stationLight2";
   texture[3] = "special/stationLight";
   texture[4] = "special/stationGlow";
   texture[5] = "special/Shocklance_effect02";

//   emitter[0] = BLaParticleEmitter;
};

function CWarpEngine::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Toggle;
    %this.cooldownTime = 500;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = 0;
    %this.deltaRegen = 0;
}

function CWarpEngine::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = true;
}

function CWarpEngine::triggerToggle(%this, %data, %vehicle, %player, %state)
{
    %vehicle.warpTriggerState = %state;
    
    if(%state)
    {
        if(%vehicle.onFire)
        {
            messageClient(%player.client, 'MsgWarpFailHealth', '\c2Warp drive activation fault - low structural integrity!');
            %vehicle.warpTriggerState = false;
            return 500;
        }

        messageClient(%player.client, 'MsgWarpInit', '\c2Warp drive activated - charging warp field, rotate to desired vector now.');
        
        %vehicle.warpPhase = 0;
        %vehicle.warpTicks = 0;
        
        %vehicle.playAudio(3, WarpInitialize2Sound);
        %this.startWarpEngine(%data, %vehicle, %player);
    }
    else
    {
        messageClient(%player.client, 'MsgWarpOff', '\c2Warp drive deactivated.');
        %vehicle.setFrozenState(false);
        %vehicle.stopAudio(3);
        
        if(isObject(%vehicle.wormhole))
            %vehicle.wormhole.delete();

        if(isObject(%vehicle.wormhole2))
            %vehicle.wormhole2.delete();
            
        %vehicle.gyrostabilizer = false;

        if(%vehicle.warpPhase >= 2)
        {
            for(%i = 0; %i < 32; %i++)
                %this.schedule(32 * %i, "stopWarpEngine", %data, %vehicle);
        }
        
        if(%vehicle.warpPhase == 4)
        {
            %vehicle.playAudio(3, WarpLaunchSound);
            %vehicle.play3d("WarpLaunchSound");
            
            return 5000;
//            %vehicle.moduleTimeout[%this.getName()] = getSimTime() + 5000;
        }
        
//        %vehicle.moduleTimeout[%this.getName()] = 0;
        return 0;
    }
}

$CWarp::Phase0 = 32 * 5;
$CWarp::Phase2 = 32 * 5;

function CWarpEngine::stopWarpEngine(%this, %data, %vehicle)
{
    if(!isObject(%vehicle))
        return;
        
    %speed = vectorLen(%vehicle.getVelocity());

    if(%speed < 20)
        return;

    %impulse = VectorScale(%vehicle.getForwardVector(), (%speed + %data.mass) * -15);
        
    %vehicle.applyImpulse(%vehicle.getTransform(), %impulse);
}

function CWarpEngine::startWarpEngine(%this, %data, %vehicle, %player)
{
    if(!isObject(%vehicle))
        return;

    if(%vehicle.warpPhase == 0)
    {
        if(%vehicle.warpTicks >= $CWarp::Phase0)
        {
            %vehicle.warpPhase++;
            %vehicle.warpTicks = 0;
        }
    }
    else if(%vehicle.warpPhase == 1)
    {
        %vehicle.setFrozenState(true);
        %vehicle.play3d("WarpInitializeSound");

        %pos = %vehicle.getWorldBoxCenter();
//        %end = vectorProject(%pos, %vehicle.getForwardVector(), 25);
        
        %wormhole = new StaticShape()
        {
            scale = "20 20 20";
            dataBlock = "WarpBubble";
        };

        MissionCleanup.add(%wormhole);
        %wormhole.setPosition(%pos);
        %wormhole.playThread(0, "throw");
        %wormhole.startfade(1000, 0, false);

        %vehicle.wormhole = %wormhole;
        %end = vectorProject(%pos, %vehicle.getForwardVector(), 25);

        %wormhole2 = new StaticShape()
        {
            scale = "50 50 50";
            dataBlock = "HoleEntry";
        };

        MissionCleanup.add(%wormhole2);
        %wormhole2.setPosition(%end);
        %wormhole2.playThread(0, "ambient");
        %wormhole2.startfade(1000, 0, false);
        %wormhole2.setRotation(%vehicle.getRotation());

        %vehicle.wormhole2 = %wormhole2;
        %vehicle.gyrostabilizer = true;
        %vehicle.warpPhase++;
    }
    else if(%vehicle.warpPhase == 2)
    {
        if(%vehicle.warpTicks % 64 == 0)
            zapVehicle(%vehicle, WarpFXStage1);

        %impulseVec = VectorScale(%vehicle.getForwardVector(), %data.mass * 4);
        %vehicle.applyImpulse(%vehicle.getTransform(), %impulseVec);
            
        if(%vehicle.warpTicks >= $CWarp::Phase2)
        {
            %vehicle.warpPhase++;
            %vehicle.warpTicks = 0;
        }
    }
    else if(%vehicle.warpPhase == 3)
    {
        %vehicle.setFrozenState(false);
        %vehicle.wormhole.startfade(500, 0, true);
        %vehicle.wormhole2.startfade(1500, 0, true);
        %vehicle.wormhole.schedule(500, "delete");
        %vehicle.wormhole2.schedule(1500, "delete");
        %vehicle.playAudio(3, WarpLaunchSound);
        %vehicle.play3d("WarpLaunchSound");
        
        %vehicle.warpPhase++;
    }
    else if(%vehicle.warpPhase == 4)
    {
        if(%vehicle.warpTicks % 16 == 0)
            zapVehicle(%vehicle, WarpFXStage2);
            
        %impulseVec = VectorScale(%vehicle.getForwardVector(), %data.mass * 3.5);
        %vehicle.applyImpulse(%vehicle.getTransform(), %impulseVec);
    }
    
    if(%vehicle.getEnergyLevel() < 32)
    {
        messageClient(%player.client, 'MsgWarpNoEnergy', '\c2Fatal: Warp engine out of energy!');
         %data.triggerModuleOff(%vehicle);
    }
    else
        %vehicle.useEnergy(%vehicle.rechargeTick * 1.25);
    
    if(%vehicle.warpTriggerState)
    {
        %vehicle.warpTicks++;
        %this.schedule($g_TickTime, "startWarpEngine", %data, %vehicle, %player);
    }
}

function CWarpEngine::reArm(%this, %data, %vehicle, %player, %slot)
{
    %vehicle.moduleTimeout[%this.getName()] = 0;
}

VehiclePart.registerVehiclePart($VehiclePartType::Module, "CWarpEngine", "Micro Warp Engine", "[Toggle] Allows you to travel long distances in a straight line very quickly, significant charge time.", $VehicleList::Wolfhound | $VehicleList::Annihilator | $VehicleList::Leviathan | $VehicleList::SkyMistress | $VehicleList::HAPCFlyer, $VHardpointSize::Internal, $VHardpointType::Anything);
