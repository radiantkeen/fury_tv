// SkyMistress Shield Defs
$SD::SkyMistress::maxStrength = 6500;
$SD::SkyMistress::recDelay = 30;
$SD::SkyMistress::resTime = 60;
$SD::SkyMistress::recRate = 525;

// Definition Objects
function FSkyMistressStandard::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";

    // Shield settings
    %this.maxStrength = $SD::SkyMistress::maxStrength;
    %this.recDelay = $SD::SkyMistress::recDelay;
    %this.resTime = $SD::SkyMistress::resTime;
    %this.recRate = $SD::SkyMistress::recRate;
    %this.powerDrainPct = $ShieldDefs::StandardPowerDrain;
    %this.bleedthrough = $ShieldDefs::StandardBleedthrough;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = %this.maxStrength;
    %this.deltaRegen = ($VehicleListData[$VehicleID::SkyMistress, "block"].rechargeRate * $g_TickTime) * -(%this.powerDrainPct / 100);
}

function FSkyMistressStandard::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, %this.maxStrength, mFloor(%this.recDelay * 1000), mFloor(%this.resTime * 1000), %this.recRate / $g_TickTime, %this.powerDrainPct / 100, %this.bleedthrough / 100, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "FSkyMistressStandard", "Standard Shields", %this.maxStrength @ " HP, Recharge delay: " @ %this.recDelay @ " sec, Restart delay: " @ %this.resTime @ " sec, Bleedthrough: " @ %this.bleedthrough @ "%, Power Drain: " @ %this.powerDrainPct @ "%", $VehicleList::SkyMistress, $VHardpointSize::Internal, $VHardpointType::Anything);

function FSkyMistressLight::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";

    // Shield settings
    %this.maxStrength = mCeil($SD::SkyMistress::maxStrength * 0.5);
    %this.recDelay = $SD::SkyMistress::recDelay * 0.75;
    %this.resTime = $SD::SkyMistress::resTime * 0.5;
    %this.recRate = $SD::SkyMistress::recRate * 0.5;
    %this.powerDrainPct = $ShieldDefs::LightPowerDrain;
    %this.bleedthrough = $ShieldDefs::LightBleedthrough;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = %this.maxStrength;
    %this.deltaRegen = ($VehicleListData[$VehicleID::SkyMistress, "block"].rechargeRate * $g_TickTime) * -(%this.powerDrainPct / 100);
}

function FSkyMistressLight::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, %this.maxStrength, mFloor(%this.recDelay * 1000), mFloor(%this.resTime * 1000), %this.recRate / $g_TickTime, %this.powerDrainPct / 100, %this.bleedthrough / 100, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "FSkyMistressLight", "Light Shields", %this.maxStrength @ " HP, Recharge delay: " @ %this.recDelay @ " sec, Restart delay: " @ %this.resTime @ " sec, Bleedthrough: " @ %this.bleedthrough @ "%, Power Drain: " @ %this.powerDrainPct @ "%", $VehicleList::SkyMistress, $VHardpointSize::Internal, $VHardpointType::Anything);

function FSkyMistressRegenerative::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";

    // Shield settings
    %this.maxStrength = mCeil($SD::SkyMistress::maxStrength * 0.333);
    %this.recDelay = $SD::SkyMistress::recDelay * 0.5;
    %this.resTime = $SD::SkyMistress::resTime * 0.333;
    %this.recRate = $SD::SkyMistress::recRate * 0.75;
    %this.powerDrainPct = $ShieldDefs::RegenerativePowerDrain;
    %this.bleedthrough = $ShieldDefs::RegenerativeBleedthrough;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = %this.maxStrength;
    %this.deltaRegen = ($VehicleListData[$VehicleID::SkyMistress, "block"].rechargeRate * $g_TickTime) * -(%this.powerDrainPct / 100);
}

function FSkyMistressRegenerative::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, %this.maxStrength, mFloor(%this.recDelay * 1000), mFloor(%this.resTime * 1000), %this.recRate / $g_TickTime, %this.powerDrainPct / 100, %this.bleedthrough / 100, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "FSkyMistressRegenerative", "Regenerative Shields", %this.maxStrength @ " HP, Recharge delay: " @ %this.recDelay @ " sec, Restart delay: " @ %this.resTime @ " sec, Bleedthrough: " @ %this.bleedthrough @ "%, Power Drain: " @ %this.powerDrainPct @ "%", $VehicleList::SkyMistress, $VHardpointSize::Internal, $VHardpointType::Anything);

function FSkyMistressHardened::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";

    // Shield settings
    %this.maxStrength = $SD::SkyMistress::maxStrength;
    %this.recDelay = $SD::SkyMistress::recDelay * 1.5;
    %this.resTime = $SD::SkyMistress::resTime * 1.25;
    %this.recRate = $SD::SkyMistress::recRate;
    %this.powerDrainPct = $ShieldDefs::HardenedPowerDrain;
    %this.bleedthrough = $ShieldDefs::HardenedBleedthrough;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = %this.maxStrength;
    %this.deltaRegen = ($VehicleListData[$VehicleID::SkyMistress, "block"].rechargeRate * $g_TickTime) * -(%this.powerDrainPct / 100);
}

function FSkyMistressHardened::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, %this.maxStrength, mFloor(%this.recDelay * 1000), mFloor(%this.resTime * 1000), %this.recRate / $g_TickTime, %this.powerDrainPct / 100, %this.bleedthrough / 100, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "FSkyMistressHardened", "Hardened Shields", %this.maxStrength @ " HP, Recharge delay: " @ %this.recDelay @ " sec, Restart delay: " @ %this.resTime @ " sec, Bleedthrough: " @ %this.bleedthrough @ "%, Power Drain: " @ %this.powerDrainPct @ "%", $VehicleList::SkyMistress, $VHardpointSize::Internal, $VHardpointType::Anything);

function FSkyMistressCovariant::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";

    // Shield settings
    %this.maxStrength = mCeil($SD::SkyMistress::maxStrength * 1.5);
    %this.recDelay = $SD::SkyMistress::recDelay;
    %this.resTime = $SD::SkyMistress::resTime;
    %this.recRate = $SD::SkyMistress::recRate;
    %this.powerDrainPct = $ShieldDefs::CovariantPowerDrain;
    %this.bleedthrough = $ShieldDefs::CovariantBleedthrough;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = %this.maxStrength;
    %this.deltaRegen = ($VehicleListData[$VehicleID::SkyMistress, "block"].rechargeRate * $g_TickTime) * -(%this.powerDrainPct / 100);
}

function FSkyMistressCovariant::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, %this.maxStrength, mFloor(%this.recDelay * 1000), mFloor(%this.resTime * 1000), %this.recRate / $g_TickTime, %this.powerDrainPct / 100, %this.bleedthrough / 100, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "FSkyMistressCovariant", "Covariant Shields", %this.maxStrength @ " HP, Recharge delay: " @ %this.recDelay @ " sec, Restart delay: " @ %this.resTime @ " sec, Bleedthrough: " @ %this.bleedthrough @ "%, Power Drain: " @ %this.powerDrainPct @ "%", $VehicleList::SkyMistress, $VHardpointSize::Internal, $VHardpointType::Anything);
