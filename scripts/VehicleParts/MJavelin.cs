datablock ShapeBaseImageData(MJavelinWOL6) : EngineAPEImage
{
   mountPoint = 0;

   shapeFile = "vehicle_grav_scout.dts"; // pyro_bomb200lb

   offset = $VehicleHardpoints[$VehicleID::Wolfhound, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Wolfhound, 6, "rotation"];
};

datablock ShapeBaseImageData(MJavelinWOL7) : MJavelinWOL6
{
   offset = $VehicleHardpoints[$VehicleID::Wolfhound, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Wolfhound, 7, "rotation"];
};

datablock ShapeBaseImageData(MJavelinCAT4) : MJavelinWOL6
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 4, "rotation"];
};

datablock ShapeBaseImageData(MJavelinCAT5) : MJavelinWOL6
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 5, "rotation"];
};

datablock ShapeBaseImageData(MJavelinCAT6) : MJavelinWOL6
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 6, "rotation"];
};

datablock ShapeBaseImageData(MJavelinCAT7) : MJavelinWOL6
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 7, "rotation"];
};

datablock ShapeBaseImageData(MJavelinGUI6) : MJavelinWOL6
{
   offset = $VehicleHardpoints[$VehicleID::Guillotine, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Guillotine, 6, "rotation"];
};

datablock ShapeBaseImageData(MJavelinGUI7) : MJavelinWOL6
{
   offset = $VehicleHardpoints[$VehicleID::Guillotine, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Guillotine, 7, "rotation"];
};

datablock ShapeBaseImageData(MJavelinRET6) : MJavelinWOL6
{
   offset = $VehicleHardpoints[$VehicleID::Retaliator, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Retaliator, 6, "rotation"];
};

datablock ShapeBaseImageData(MJavelinRET7) : MJavelinWOL6
{
   offset = $VehicleHardpoints[$VehicleID::Retaliator, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Retaliator, 7, "rotation"];
};

datablock BombProjectileData(CruiseMissileDeploy)
{
   projectileShapeName  = "vehicle_grav_scout.dts";
   emitterDelay         = -1;
   directDamage         = 0.0;
   hasDamageRadius      = true;
   indirectDamage       = 40.0;
   damageRadius         = 45;
   radiusDamageType     = $DamageType::Missile;
   directDamageType     = $DamageType::Missile;
   kickBackStrength     = 2250;  // z0dd - ZOD, 4/25/02. Was 2500

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Explosive;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   explosion           = "SatchelMainExplosion";
   underwaterExplosion = "UnderwaterSatchelMainExplosion";
   velInheritFactor     = 1.0;

   grenadeElasticity    = 0.25;
   grenadeFriction      = 0.4;
   armingDelayMS        = 2000;
   muzzleVelocity       = 15.0;
   drag                 = 0.3;
   gravityMod		    = 1.0;

   minRotSpeed          = "0.0 45.0 0.0";
   maxRotSpeed          = "0.0 45.0 0.0";

   scale                = "1.0 1.0 1.0";

   sound                = "";
   
   missileNameBase      = "CruiseMissile";
};

datablock LinearProjectileData(CruiseMissileDumbfire)
{
//   scale = "1.5 1.5 1.5";
   projectileShapeName = "vehicle_grav_scout.dts";
   emitterDelay        = -1;
   directDamage        = 0;
   directDamageType    = $DamageType::Missile;
   hasDamageRadius     = true;
   indirectDamage      = 40.0;
   damageRadius        = 45;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 3200;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Explosive;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "SatchelMainExplosion";
   underwaterExplosion = "UnderwaterSatchelMainExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = WildcatJetEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 100;
   wetVelocity       = 50;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 10000;
   lifetimeMS        = 10000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 6000;

   sound = "LargeMissileProjectileSound";
   
   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 7.0;
   lightColor  = "0.4 0.15 0.1";
};

datablock SeekerProjectileData(CruiseMissile) : ShoulderMissile
{
//   scale = "5.0 5.0 5.0";
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "vehicle_grav_scout.dts";
   directDamage        = 0;
   directDamageType    = $DamageType::Missile;
   hasDamageRadius     = true;
   indirectDamage      = 40;
   damageRadius        = 45;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 3200;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Explosive;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "SatchelMainExplosion";
   underwaterExplosion = "UnderwaterSatchelMainExplosion";
//   underwaterExplosion = UnderwaterHandGrenadeExplosion;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = WildcatJetEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   lifetimeMS          = 10000;
   muzzleVelocity      = 100.0;
   maxVelocity         = 125.0;
   turningSpeed        = 20.0;
   acceleration        = 25.0;

   proximityRadius     = 4;

   flareDistance = 6;
   flareAngle    = 5;

//   sound = ScoutThrustSound;
   sound = "LargeMissileProjectileSound";

   hasLight    = true;
   lightRadius = 7.0;
   lightColor  = "0.4 0.15 0.1";

   useFlechette = false;
   explodeOnWaterImpact = false;

   // Used for vehicle-mounted missile system
   isVehicleMissile = true;
};

function MJavelin::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "";
    %this.rearmTime = 10000;
    
    %this.missileName = "CruiseMissile";
    %this.missileSize = $VHardpointSize::XLarge;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "MJavelin", "Javelin Cruise Missile", "Similar to a torpedo explosive yield, with the maneuverability to match", $VehicleList::Catapult | $VehicleList::Guillotine | $VehicleList::Retaliator | $VehicleList::Wolfhound | $VehicleList::Scorpion, $VHardpointSize::Missile, $VHardpointType::Missile);
