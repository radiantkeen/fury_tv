function CGyrostabilizer::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Push;
    
    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = 0;
    %this.deltaRegen = 0;
}

function CGyrostabilizer::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = true;
    %vehicle.gyrostabilizer = true;
}

VehiclePart.registerVehiclePart($VehiclePartType::Module, "CGyrostabilizer", "Gyrostabilizer", "[Passive] Prevents outside forces from affecting the vehicle", $VehicleList::All, $VHardpointSize::Internal, $VHardpointType::Omni);
