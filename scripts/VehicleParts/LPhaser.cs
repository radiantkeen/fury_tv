// Sound, Projectile, and Explosion blocks

datablock LinearFlareProjectileData(VLPhaserBolt)
{
   scale               = "2 2 2";
   faceViewer          = true;
   directDamage        = 0.0;
   directDamageType    = $DamageType::Phaser;
   hasDamageRadius     = true;
   indirectDamage      = 5.0;
   damageRadius        = 1.0;
   radiusDamageType    = $DamageType::Phaser;

   kickBackStrength    = 500.0;

   explosion           = "LNullifierExplosion";
   splash              = PlasmaSplash;
   flags               = $Projectile::PlaysHitSound | $Projectile::Phaser;
   ticking             = false;
   headshotMultiplier  = 1.0;

   dryVelocity       = 250;
   wetVelocity       = 250;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 3000;
   lifetimeMS        = 3000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   activateDelayMS = 1;

   numFlares         = 35;
   flareColor        = "0.333 0.04 0.267";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   size[0]           = 0.5;
   size[1]           = 1.25;
   size[2]           = 2.0;

	sound        = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 3.0;
   lightColor  = "0.333 0.04 0.267";
};

// Primary Definition
datablock ShapeBaseImageData(LPhaserSTA2)
{
	className = WeaponImage;
   shapeFile = "turret_fusion_large.dts";
//   item = PulsePhaser;
   projectile = VLPhaserBolt;
   projectileType = LinearFlareProjectile;
//	armThread = looksn;

    usesEnergy = true;
	minEnergy = 160;
    fireEnergy = 150;
//    defaultModeFireSound = "LaserFireSound";
   pilotHeadTracking = true;
   ammo = "VLPhaserAmmo";
   updatePilotAmmo = true;
   sharedResourcePool = true;
   
   offset = $VehicleHardpoints[$VehicleID::Starhawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 2, "rotation"];

   fireTimeout = 2000;

   // State transitions
   stateName[0]                  = "Activate";
   stateTransitionOnNotLoaded[0] = "Dead";
   stateTransitionOnLoaded[0]    = "ActivateReady";

   stateName[1]                  = "ActivateReady";
   stateSequence[1]              = "Deploy";
   stateTimeoutValue[1]          = 1;
   stateTransitionOnTimeout[1]   = "Ready";
   stateTransitionOnNotLoaded[1] = "Deactivate";
   stateTransitionOnNoAmmo[1]    = "NoAmmo";

   stateName[2]                    = "Ready";
   stateTransitionOnNotLoaded[2]   = "Deactivate";
   stateTransitionOnTriggerDown[2] = "Fire";
   stateTransitionOnNoAmmo[2]      = "NoAmmo";

   stateName[3]                = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3]        = 0.75;
   stateFire[3]                = true;
   stateRecoil[3]              = LightRecoil;
   stateAllowImageChange[3]    = false;
   stateSequence[3]            = "Recoil";
   stateSound[3]               = LNullifierFireSound;
   stateScript[3]              = "onFire";

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 1.25;
   stateAllowImageChange[4]      = false;
   stateSequence[4]              = "Deploy";
   stateTransitionOnTimeout[4]   = "Ready";
   stateTransitionOnNotLoaded[4] = "Deactivate";
    stateTransitionOnNoAmmo[4]    = "NoAmmo";

   stateName[5]                = "Deactivate";
   stateSequence[5]            = "Activate";
   stateDirection[5]           = false;
   stateTimeoutValue[5]        = 1;
   stateTransitionOnLoaded[5]  = "ActivateReady";
   stateTransitionOnTimeout[5] = "Dead";

   stateName[6]               = "Dead";
   stateTransitionOnLoaded[6] = "ActivateReady";

   stateName[7]             = "NoAmmo";
   stateTransitionOnAmmo[7] = "Reload";
   stateSequence[7]         = "NoAmmo";
};

// Inherited Defs
datablock ShapeBaseImageData(LPhaserAMI2) : LPhaserSTA2
{
	minEnergy = 150;
    fireEnergy = 140;
    
   pilotHeadTracking = false;

   offset = $VehicleHardpoints[$VehicleID::Ameri, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Ameri, 2, "rotation"];
};

datablock ShapeBaseImageData(LPhaserSSH2) : LPhaserSTA2
{
   pilotHeadTracking = false;

   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 2, "rotation"];
};

datablock ShapeBaseImageData(LPhaserTUT4) : LPhaserSTA2
{
   pilotHeadTracking = false;

   mountPoint = 1;

   offset = "0 0 0";
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 4, "rotation"];
};

datablock ShapeBaseImageData(LPhaserTUS2) : LPhaserSTA2
{
   shapeFile = "TR2weapon_mortar.dts";
   pilotHeadTracking = false;
   
   offset = "-0.125 0.75 0.0";
   rotation = $VehicleHardpoints[$VehicleID::TurretPole, 2, "rotation"];
};

datablock ShapeBaseImageData(LPhaserGUI2) : LPhaserSTA2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Guillotine, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Guillotine, 2, "rotation"];
};

datablock ShapeBaseImageData(LPhaserRET2) : LPhaserSTA2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Retaliator, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Retaliator, 2, "rotation"];
};

datablock ShapeBaseImageData(LPhaserSTR2) : LPhaserSTA2
{
   pilotHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::Stryker, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Stryker, 2, "rotation"];
};

datablock ShapeBaseImageData(LPhaserTPD2) : LPhaserSTA2
{
   shapeFile = "TR2weapon_mortar.dts";
   
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 2, "rotation"];
};

datablock ShapeBaseImageData(LPhaserTPD3) : LPhaserSTA2
{
   shapeFile = "TR2weapon_mortar.dts";
   
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 3, "rotation"];
};

function LPhaser::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 20;
    %this.ammo = "VLPhaserAmmo";
    %this.ammoUse = 1;
    %this.fireTimeout = 1000;
    
    // Reticle data
    %this.reticleTex = "gui/RET_elf";
    %this.showReticleFrame = true;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LPhaser", "Phaser Cannon", "Ignores shields and deals damage directly to armor", $VehicleList::Leviathan | $VehicleList::SuperShrike | $VehicleList::Ameri | $VehicleList::Guillotine | $VehicleList::Starhawk | $VehicleList::Retaliator | $VehicleList::HoverJeep | $VehicleList::Stryker | $VehicleList::Wolfhound | $VehicleList::Annihilator | $VehicleList::Goodship | $VehicleList::Scorpion, $VHardpointSize::Cannon, $VHardpointType::Standard);
