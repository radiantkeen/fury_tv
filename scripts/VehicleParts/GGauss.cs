// Sound, Projectile, and Explosion blocks

datablock TracerProjectileData(GGaussBolt) : GaussBolt
{
   doDynamicClientHits = true;

   projectileShapeName = "";
   directDamage        = 0.0;
   directDamageType    = $DamageType::Gauss;
   hasDamageRadius     = true;
   indirectDamage      = 2.25;
   damageRadius        = 3.0;
   kickBackStrength    = 2500.0;
   radiusDamageType    = $DamageType::Gauss;
   sound          	   = BlasterProjectileSound;
   explosion           = GaussExplosion;

   flags               = $Projectile::PlaysHitSound | $Projectile::Kinetic;
   ticking             = false;
   headshotMultiplier  = 1.0;

   dryVelocity       = 600.0;
   wetVelocity       = 600.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 2000;
   lifetimeMS        = 2000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   activateDelayMS = -1;

   tracerLength    = 5;
   tracerAlpha     = false;
   tracerMinPixels = 3;
   tracerColor     = "1 0 0 1";
	tracerTex[0]  	 = "special/landSpikeBolt";
	tracerTex[1]  	 = "special/landSpikeBoltCross";
	tracerWidth     = 0.35;
   crossSize       = 0.79;
   crossViewAng    = 0.990;
   renderCross     = true;
   emap = true;
};

// Primary Definition
datablock ShapeBaseImageData(GGaussVIP2)
{
   className = WeaponImage;
   shapeFile = "weapon_plasma.dts";
   mountPoint = "0";
   offset = $VehicleHardpoints[$VehicleID::Viper, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Viper, 2, "rotation"];

   ammo = "VGGaussAmmo";
   updatePilotAmmo = true;
   sharedResourcePool = true;
   projectile = GGaussBolt;
   projectileType = TracerProjectile;
   emap = true;

   fireTimeout = 750;

   // State transitions
   stateName[0]                  = "Activate";
   stateTransitionOnNotLoaded[0] = "Dead";
   stateTransitionOnLoaded[0]    = "ActivateReady";

   stateName[1]                  = "ActivateReady";
   stateSequence[1]              = "Deploy";
   stateTimeoutValue[1]          = 1;
   stateTransitionOnTimeout[1]   = "Ready";
   stateTransitionOnNotLoaded[1] = "Deactivate";
   stateTransitionOnNoAmmo[1]    = "NoAmmo";

   stateName[2]                    = "Ready";
   stateTransitionOnNotLoaded[2]   = "Deactivate";
   stateTransitionOnTriggerDown[2] = "Fire";
   stateTransitionOnNoAmmo[2]      = "NoAmmo";

   stateName[3]                = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3]        = 0.5;
   stateFire[3]                = true;
   stateRecoil[3]              = LightRecoil;
   stateAllowImageChange[3]    = false;
   stateSequence[3]            = "Fire";
   stateSound[3]               = GaussRifleFireSound;
   stateScript[3]              = "onFire";

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 1.0;
   stateAllowImageChange[4]      = false;
   stateTransitionOnTimeout[4]   = "Ready";
   stateTransitionOnNotLoaded[4] = "Deactivate";
    stateTransitionOnNoAmmo[4]    = "NoAmmo";

   stateName[5]                = "Deactivate";
   stateSequence[5]            = "Activate";
   stateDirection[5]           = false;
   stateTimeoutValue[5]        = 1;
   stateTransitionOnLoaded[5]  = "ActivateReady";
   stateTransitionOnTimeout[5] = "Dead";

   stateName[6]               = "Dead";
   stateTransitionOnLoaded[6] = "ActivateReady";

   stateName[7]             = "NoAmmo";
   stateTransitionOnAmmo[7] = "Reload";
   stateSequence[7]         = "NoAmmo";
};

// Inherited Defs
datablock ShapeBaseImageData(GGaussSKY0) : GGaussVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Skycutter, 0, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Skycutter, 0, "rotation"];
};

datablock ShapeBaseImageData(GGaussAIR2) : GGaussVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Airhawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Airhawk, 2, "rotation"];
};

datablock ShapeBaseImageData(GGaussAIR3) : GGaussVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Airhawk, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Airhawk, 3, "rotation"];
};

datablock ShapeBaseImageData(GGaussTUT4) : GGaussVIP2
{
   mountPoint = 1;
   
//   offset = "-0.125 0.1 0";
   offset = $VehicleHardpoints[$VehicleID::TankTurret, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 4, "rotation"];
};

datablock ShapeBaseImageData(GGaussTUT5) : GGaussVIP2
{
   mountPoint = 1;

//   offset = "0.125 0.1 0";
   offset = $VehicleHardpoints[$VehicleID::TankTurret, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 5, "rotation"];
};

datablock ShapeBaseImageData(GGaussTUD4) : GGaussVIP2
{
   offset = "0.4 1.0 0";
   rotation = $VehicleHardpoints[$VehicleID::TurretPole, 4, "rotation"];
};

datablock ShapeBaseImageData(GGaussTUD5) : GGaussVIP2
{
   offset = "-0.4 1.0 0";
   rotation = $VehicleHardpoints[$VehicleID::TurretPole, 5, "rotation"];
};

datablock ShapeBaseImageData(GGaussSHR4) : GGaussVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 4, "rotation"];
};

datablock ShapeBaseImageData(GGaussSHR5) : GGaussVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 5, "rotation"];
};

datablock ShapeBaseImageData(GGaussSHX2) : GGaussVIP2
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 2, "rotation"];
};

datablock ShapeBaseImageData(GGaussSHX3) : GGaussVIP2
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 3, "rotation"];
   
   stateSound[3]            = "";
};

datablock ShapeBaseImageData(GGaussSHX4) : GGaussVIP2
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 4, "rotation"];
};

datablock ShapeBaseImageData(GGaussSHX5) : GGaussVIP2
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 5, "rotation"];

   stateSound[3]            = "";
};

datablock ShapeBaseImageData(GGaussSSH4) : GGaussVIP2
{
   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 4, "rotation"];
};

datablock ShapeBaseImageData(GGaussSSH5) : GGaussVIP2
{
   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 5, "rotation"];
};

datablock ShapeBaseImageData(GGaussFIB6) : GGaussVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 6, "rotation"];
};

datablock ShapeBaseImageData(GGaussFIB7) : GGaussVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 7, "rotation"];
};

datablock ShapeBaseImageData(GGaussSTA4) : GGaussVIP2
{
   pilotHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::Starhawk, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 4, "rotation"];
};

datablock ShapeBaseImageData(GGaussSTA5) : GGaussVIP2
{
   pilotHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::Starhawk, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 5, "rotation"];
};

datablock ShapeBaseImageData(GGaussJEP2) : GGaussVIP2
{
   offset = $VehicleHardpoints[$VehicleID::HoverJeep, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::HoverJeep, 2, "rotation"];
   
   pilotHeadTracking = true;
};

datablock ShapeBaseImageData(GGaussJEP3) : GGaussVIP2
{
   offset = $VehicleHardpoints[$VehicleID::HoverJeep, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::HoverJeep, 3, "rotation"];
   
   pilotHeadTracking = true;
};

datablock ShapeBaseImageData(GGaussUBT2) : GGaussVIP2
{
   shapeFile = "weapon_plasma.dts";
   mountPoint = 2; // L

   offset = "0.0 -0.2 -0.2";
   rotation = $VehicleHardpoints[$VehicleID::Underbelly, 2, "rotation"];
};

datablock ShapeBaseImageData(GGaussUBT3) : GGaussVIP2
{
   shapeFile = "weapon_plasma.dts";
   mountPoint = 3; // L

   offset = "0.0 -0.2 -0.2";
   rotation = $VehicleHardpoints[$VehicleID::Underbelly, 3, "rotation"];
};

datablock ShapeBaseImageData(GGaussTPQ4) : GGaussVIP2
{
   shapeFile = "weapon_plasma.dts";
   scorpionHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 4, "rotation"];
};

datablock ShapeBaseImageData(GGaussTPQ5) : GGaussVIP2
{
   shapeFile = "weapon_plasma.dts";
   scorpionHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 5, "rotation"];
};

datablock ShapeBaseImageData(GGaussTPQ6) : GGaussVIP2
{
   shapeFile = "weapon_plasma.dts";
   scorpionHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 6, "rotation"];
};

datablock ShapeBaseImageData(GGaussTPQ7) : GGaussVIP2
{
   shapeFile = "weapon_plasma.dts";
   scorpionHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 7, "rotation"];
};

function GGauss::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = true;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 100;
    %this.ammo = "VGGaussAmmo";
    %this.ammoUse = 1;
    %this.fireTimeout = 750;
    
    // Reticle data
    %this.reticleTex = "gui/RET_disc";
    %this.showReticleFrame = false;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "GGauss", "Gauss Spike Launcher", "High velocity moderate damage precision weapon, imparts force on impact", $VehicleList::Guillotine | $VehicleList::Starhawk | $VehicleList::Firebird | $VehicleList::Skycutter | $VehicleList::Airhawk | $VehicleList::Wildcat | $VehicleList::Leviathan | $VehicleList::ShrikeX | $VehicleList::Shrike | $VehicleList::SuperShrike | $VehicleList::HoverJeep | $VehicleList::Tomahawk | $VehicleList::Retaliator | $VehicleList::Stryker | $VehicleList::Annihilator | $VehicleList::Wolfhound | $VehicleList::HoverJeep | $VehicleList::Stryker | $VehicleList::Leviathan | $VehicleList::Scorpion, $VHardpointSize::Gun, $VHardpointType::Ballistic);
