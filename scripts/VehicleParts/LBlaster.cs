// Sound, Projectile, and Explosion blocks

// Primary Definition
datablock ShapeBaseImageData(LBlasterSTA2)
{
	className = WeaponImage;
   shapeFile = "turret_elf_large.dts";
//   item = PulseBlaster;
   projectile = VLBlasterBolt;
   projectileType = TracerProjectile;
//	armThread = looksn;

   pilotHeadTracking = true;
    usesEnergy = true;
	minEnergy = 40;
    fireEnergy = 30;
//    defaultModeFireSound = "LaserFireSound";

   offset = $VehicleHardpoints[$VehicleID::Starhawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 2, "rotation"];

   fireTimeout = 250;

   // State transitions
   stateName[0]                  = "Activate";
   stateTransitionOnNotLoaded[0] = "Dead";
   stateTransitionOnLoaded[0]    = "ActivateReady";

   stateName[1]                  = "ActivateReady";
   stateSequence[1]              = "Deploy";
   stateTimeoutValue[1]          = 1;
   stateTransitionOnTimeout[1]   = "Ready";
   stateTransitionOnNotLoaded[1] = "Deactivate";
   stateTransitionOnNoAmmo[1]    = "NoAmmo";

   stateName[2]                    = "Ready";
   stateTransitionOnNotLoaded[2]   = "Deactivate";
   stateTransitionOnTriggerDown[2] = "Fire";
   stateTransitionOnNoAmmo[2]      = "NoAmmo";

   stateName[3]                = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3]        = 0.2;
   stateFire[3]                = true;
   stateRecoil[3]              = LightRecoil;
   stateAllowImageChange[3]    = false;
   stateSequence[3]            = "Fire";
   stateSound[3]               = LBlasterFireSound;
   stateScript[3]              = "onFire";

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 0.3;
   stateAllowImageChange[4]      = false;
   stateSequence[4]              = "Deploy";
   stateTransitionOnTimeout[4]   = "Ready";
   stateTransitionOnNotLoaded[4] = "Deactivate";
    stateTransitionOnNoAmmo[4]    = "NoAmmo";

   stateName[5]                = "Deactivate";
   stateSequence[5]            = "Activate";
   stateDirection[5]           = false;
   stateTimeoutValue[5]        = 1;
   stateTransitionOnLoaded[5]  = "ActivateReady";
   stateTransitionOnTimeout[5] = "Dead";

   stateName[6]               = "Dead";
   stateTransitionOnLoaded[6] = "ActivateReady";

   stateName[7]             = "NoAmmo";
   stateTransitionOnAmmo[7] = "Reload";
   stateSequence[7]         = "NoAmmo";
};

// Inherited Defs
datablock ShapeBaseImageData(LBlasterAMI2) : LBlasterSTA2
{
   pilotHeadTracking = false;

   offset = $VehicleHardpoints[$VehicleID::Ameri, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Ameri, 2, "rotation"];
};

datablock ShapeBaseImageData(LBlasterSSH2) : LBlasterSTA2
{
   pilotHeadTracking = false;

   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 2, "rotation"];
};

datablock ShapeBaseImageData(LBlasterTUT4) : LBlasterSTA2
{
   pilotHeadTracking = false;

   mountPoint = 1;

   offset = "0 0 0";
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 4, "rotation"];
};

datablock ShapeBaseImageData(LBlasterGUI2) : LBlasterSTA2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Guillotine, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Guillotine, 2, "rotation"];
};

datablock ShapeBaseImageData(LBlasterTOM2) : LBlasterSTA2
{
   pilotHeadTracking = false;

   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 2, "rotation"];
};

datablock ShapeBaseImageData(LBlasterTOM3) : LBlasterSTA2
{
   pilotHeadTracking = false;

   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 3, "rotation"];
};

datablock ShapeBaseImageData(LBlasterFIB2) : LBlasterSTA2
{
   pilotHeadTracking = false;
   
   offset = $VehicleHardpoints[$VehicleID::Firebird, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 2, "rotation"];
};

datablock ShapeBaseImageData(LBlasterFIB4) : LBlasterSTA2
{
   pilotHeadTracking = false;
   
   offset = $VehicleHardpoints[$VehicleID::Firebird, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 4, "rotation"];
};

datablock ShapeBaseImageData(LBlasterFIB5) : LBlasterSTA2
{
   pilotHeadTracking = false;
   
   offset = $VehicleHardpoints[$VehicleID::Firebird, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 5, "rotation"];
};

datablock ShapeBaseImageData(LBlasterTUS2) : LBlasterSTA2
{
   pilotHeadTracking = false;
   
   offset = $VehicleHardpoints[$VehicleID::TurretPole, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretPole, 2, "rotation"];
};

datablock ShapeBaseImageData(LBlasterRET2) : LBlasterSTA2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Retaliator, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Retaliator, 2, "rotation"];
};

datablock ShapeBaseImageData(LBlasterSTR2) : LBlasterSTA2
{
   pilotHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::Stryker, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Stryker, 2, "rotation"];
};

datablock ShapeBaseImageData(LBlasterTPD2) : LBlasterSTA2
{
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 2, "rotation"];
};

datablock ShapeBaseImageData(LBlasterTPD3) : LBlasterSTA2
{
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 3, "rotation"];
};

function LBlaster::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "";
    %this.ammoUse = 0;
    %this.fireTimeout = 250;
    
    // Reticle data
    %this.reticleTex = "gui/hud_ret_shrike";
    %this.showReticleFrame = true;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LBlaster", "Heavy Blaster Cannon", "Similar damage and speed to an anti-air turret", $VehicleList::Firebird | $VehicleList::Tomahawk | $VehicleList::Ameri | $VehicleList::SuperShrike | $VehicleList::Leviathan | $VehicleList::Guillotine | $VehicleList::Starhawk | $VehicleList::Firebird | $VehicleList::Retaliator | $VehicleList::HoverJeep | $VehicleList::Stryker | $VehicleList::Wolfhound | $VehicleList::Annihilator | $VehicleList::Goodship | $VehicleList::Scorpion, $VHardpointSize::Cannon, $VHardpointType::Energy);
