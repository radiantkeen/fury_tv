datablock AudioProfile(VShieldBoostSound)
{
     filename = "fx/misc/armorrepair_loop.wav";
     description = AudioDefault3d;
     preload = true;
};

function CShieldBattery::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Push;
    %this.cooldownTime = 2000;

    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = 0;
    %this.deltaRegen = 0;
}

function CShieldBattery::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = true;
    %vehicle.emergencyBatteryCharges = 1;
}

function CShieldBattery::triggerPush(%this, %data, %vehicle, %player)
{
    if(%vehicle.emergencyBatteryCharges < 1)
    {
        messageClient(%player.client, 'MsgCBAFailEmpty', '\c2Emergency shield battery exhausted.');
        return 2000;
    }
    
    if(%vehicle.shieldSource.strength == %vehicle.shieldSource.maxStrength)
    {
        messageClient(%player.client, 'MsgCBAFailFull', '\c2Shields are full.');
        return 1000;
    }

    zapVehicle(%vehicle, FXBlueShift);
    
    %vehicle.shieldSource.strength += %vehicle.shieldSource.maxStrength * 0.5;

    if(%vehicle.shieldSource.strength > %vehicle.shieldSource.maxStrength)
        %vehicle.shieldSource.strength = %vehicle.shieldSource.maxStrength;

    %vehicle.shieldSource.getDatablock().startRecharge(%vehicle.shieldSource);
    %vehicle.emergencyBatteryCharges--;
}

function CShieldBattery::reArm(%this, %data, %vehicle, %player, %slot)
{
    %vehicle.moduleTimeout[%this.getName()] = 0;
    %vehicle.emergencyBatteryCharges = 1;
}

VehiclePart.registerVehiclePart($VehiclePartType::Module, "CShieldBattery", "Emergency Shield Battery", "[Trigger] Restores 50% shield capacity and restarts shield regen if it has stopped, one-time use.", $VehicleList::HasShields, $VHardpointSize::Internal, $VHardpointType::Omni);
