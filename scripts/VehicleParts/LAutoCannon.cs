// Sound, Projectile, and Explosion blocks
datablock AudioProfile(AutoCannonFireSound)
{
   filename    = "fx/weapon/autocannon_turret_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(AutoCannonHitSound)
{
   filename    = "fx/explosion/vulcan_explosive_exp2.wav";
   description = AudioExplosion3d;
   preload = true;
};

datablock ShockwaveData(AutoCannonImpactShockwave)
{
   width = 0.5;
   numSegments = 24;
   numVertSegments = 24;
   velocity = 5;
   acceleration = 5;
   lifetimeMS = 500;
   height = 0.1;
   verticalCurve = 0.5;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 1.0";
   colors[1] = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.5";
   colors[2] = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ParticleData(AutoCannonDebrisParticle)
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.01;   // rises slowly
   inheritedVelFactor   = 0.125;

   lifetimeMS           =  800;
   lifetimeVarianceMS   =  100;
   useInvAlpha          =  true;
   spinRandomMin        = -100.0;
   spinRandomMax        =  100.0;

   animateTexture = false;

   textureName = "special/Smoke/bigSmoke";

   colors[0]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 1.0";
   colors[1]     = "0.8 0.8 0.8 0.8";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 0.225;
   sizes[1]      = 0.3;
   sizes[2]      = 0.325;
   times[0]      = 0.0;
   times[1]      = 0.25;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(AutoCannonDebrisEmitter) : DefaultEmitter
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 1;
   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.2;
   thetaMax         = 40.0;
   particles = "AutoCannonDebrisParticle";
};

datablock DebrisData(AutoCannonDebris)
{
   emitters[0] = AutoCannonDebrisEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 0.75;
   lifetimeVariance = 0.25;

   numBounces = 1;
};

datablock ParticleData(AutoCannonSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 1.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 300;
   lifetimeVarianceMS   = 0;
   textureName          = "special/spark00";
   colors[0]     = "0.56 0.36 0.26 1.0";
   colors[1]     = "0.56 0.36 0.26 1.0";
   colors[2]     = "1.0 0.36 0.26 0.0";
   sizes[0]      = 2.8;
   sizes[1]      = 2.0;
   sizes[2]      = 0.1;
   times[0]      = 0.0;
   times[1]      = 0.8;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(AutoCannonSparkEmitter) : DefaultEmitter
{
   ejectionPeriodMS = 4;
   ejectionVelocity = 16;
   velocityVariance = 4.0;
   thetaMax         = 75;
   orientParticles  = true;
   lifetimeMS       = 175;
   particles = "AutoCannonSparks";
};

datablock ExplosionData(AutoCannonExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   playSpeed = 2.0;
   soundProfile = ChaingunImpact;
   faceViewer = true;

   shockwave = AutoCannonImpactShockwave;
   emitter[0] = AutoCannonSparkEmitter;

   debris = AutoCannonDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 82;
   debrisNum = 3;
   debrisVelocity = 6.0;
   debrisVelocityVariance = 2.0;

   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock LinearProjectileData(AutoCannonBullet)
{
   scale = "4.0 6.0 4.0";
   projectileShapeName = "tag projectile.dts";
   emitterDelay        = -1;
   directDamage        = 0.95;
   directDamageType    = $DamageType::AutoCannon;
   hasDamageRadius     = false;
   kickBackStrength    = 200;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Kinetic;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "AutoCannonExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

//   baseEmitter         = MissileSmokeEmitter;
//   delayEmitter        = WildcatJetEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 300;
   wetVelocity       = 200;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 3000;
   lifetimeMS        = 3000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 3000;

   sound = "ChaingunProjectile";

   activateDelayMS = -1;

   hasLight    = false;
   lightRadius = 7.0;
   lightColor  = "0.4 0.15 0.1";
};

datablock DebrisData(AutoCannonShellDebris)
{
   shapeName = "tag projectile.dts";

   lifetime = 3.0;

   minSpinSpeed = 300.0;
   maxSpinSpeed = 400.0;

   elasticity = 0.5;
   friction = 0.2;

   numBounces = 3;

   fade = true;
   staticOnMaxBounce = true;
   snapOnMaxBounce = true;
};

// Primary Definition
datablock ShapeBaseImageData(LAutoCannonSSH2)
{
   className = WeaponImage;
   shapeFile = "turret_tank_barrelchain.dts";
   mountPoint = "0";
   offset = "0.5 2.25 -1.5";
   rotation = "1 0 0 0";

   ammo = "VLAutoCannonAmmo";
   updatePilotAmmo = true;
   sharedResourcePool = true;
   projectile = AutoCannonBullet;
   projectileType = LinearProjectile;
   emap = true;

   casing              = AutoCannonShellDebris;
   shellExitDir        = "1.0 0.3 1.0";
   shellExitOffset     = "0.15 -0.56 -0.1";
   shellExitVariance   = 15.0;
   shellVelocity       = 6.0;

   projectileSpread = 3.0; // z0dd - ZOD, 8/6/02. Was: 8.0 / 1000.0

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   //
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
   stateSequence[4]            = "Fire"; // Fire
   stateSequenceRandomFlash[4] = true;
   stateSpinThread[4]       = FullSpeed;
   stateSound[4]            = AutoCannonFireSound;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateEjectShell[4]       = true;
   //
   stateTimeoutValue[4]          = 0.2;
   stateTransitionOnTimeout[4]   = "Reload";

   //--------------------------------------
   stateName[5]       = "Spindown";
//   stateSound[5]      = ChaingunSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 1.0;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = ChaingunSpinDownSound;
   stateSpinThread[6] = SpinDown;
   //
   stateTimeoutValue[6]        = 0.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ChaingunDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateScript[7]           = "onDryFire";
   stateTransitionOnTimeout[7] = "NoAmmo";
   
   //--------------------------------------
   stateName[8]             = "Reload";
   stateSequence[8]            = "";
   stateSpinThread[8]       = FullSpeed;
   stateAllowImageChange[8] = false;
   stateTimeoutValue[8]          = 0.05;
   stateTransitionOnTimeout[8]   = "Fire";
   stateTransitionOnTriggerUp[8] = "Spindown";
   stateTransitionOnNoAmmo[8]    = "EmptySpindown";
};

// Inherited Defs
datablock ShapeBaseImageData(LAutoCannonSTA2) : LAutoCannonSSH2
{
   pilotHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::Starhawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 2, "rotation"];
};

datablock ShapeBaseImageData(LAutoCannonFIB2) : LAutoCannonSSH2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 2, "rotation"];
};

datablock ShapeBaseImageData(LAutoCannonFIB4) : LAutoCannonSSH2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 4, "rotation"];
};

datablock ShapeBaseImageData(LAutoCannonFIB5) : LAutoCannonSSH2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 5, "rotation"];
};

datablock ShapeBaseImageData(LAutoCannonGUI2) : LAutoCannonSSH2
{
   offset = $VehicleHardpoints[$VehicleID::Guillotine, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Guillotine, 2, "rotation"];
};

datablock ShapeBaseImageData(LAutoCannonTUS2) : LAutoCannonSSH2
{
   offset = "0 1 0";
   rotation = $VehicleHardpoints[$VehicleID::TurretPole, 2, "rotation"];
};

datablock ShapeBaseImageData(LAutoCannonTUT4) : LAutoCannonSSH2
{
   mountPoint = 1;
   
   offset = "0 0 0";
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 4, "rotation"];
};

datablock ShapeBaseImageData(LAutoCannonTOM2) : LAutoCannonSSH2
{
   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 2, "rotation"];
};

datablock ShapeBaseImageData(LAutoCannonTOM3) : LAutoCannonSSH2
{
   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 3, "rotation"];
};

datablock ShapeBaseImageData(LAutoCannonRET2) : LAutoCannonSSH2
{
   pilotHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::Retaliator, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Retaliator, 2, "rotation"];
};

datablock ShapeBaseImageData(LAutoCannonSTR2) : LAutoCannonSSH2
{
   pilotHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::Stryker, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Stryker, 2, "rotation"];
};

datablock ShapeBaseImageData(LAutoCannonTPD2) : LAutoCannonSSH2
{
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 2, "rotation"];
};

datablock ShapeBaseImageData(LAutoCannonTPD3) : LAutoCannonSSH2
{
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 3, "rotation"];
};

function LAutoCannon::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = true;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 600;
    %this.ammo = "VLAutoCannonAmmo";
    %this.ammoUse = 1;
    %this.fireTimeout = 0;
    
    // Reticle data
    %this.reticleTex = "gui/hud_ret_tankchaingun";
    %this.showReticleFrame = true;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LAutoCannon", "Auto Cannon", "Rotary automatic cannon - high damage, medium RoF, slower projectiles", $VehicleList::Firebird | $VehicleList::Tomahawk | $VehicleList::SuperShrike | $VehicleList::Starhawk | $VehicleList::Firebird | $VehicleList::Guillotine | $VehicleList::Retaliator | $VehicleList::HoverJeep | $VehicleList::Stryker | $VehicleList::Wolfhound | $VehicleList::Annihilator | $VehicleList::Goodship | $VehicleList::Leviathan | $VehicleList::Scorpion, $VHardpointSize::Cannon, $VHardpointType::Ballistic);
