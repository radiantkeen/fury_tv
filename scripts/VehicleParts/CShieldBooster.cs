datablock AudioProfile(VShieldBoostSound)
{
     filename = "fx/misc/armorrepair_loop.wav";
     description = AudioDefault3d;
     preload = true;
};

function CShieldBooster::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Push;
    %this.cooldownTime = 20000;
    
    // Overall module settings
    %this.deltaMass = 0;
    %this.deltaArmor = 0;
    %this.deltaShield = 0;
    %this.deltaRegen = 0;
}

function CShieldBooster::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = true;
}

function CShieldBooster::triggerPush(%this, %data, %vehicle, %player)
{
    if(%vehicle.getEnergyPct() < 0.5)
    {
        messageClient(%player.client, 'MsgCSBFailEnergy', '\c2Not enough energy to ignite shield boost.');
        return 1000;
    }

    if(%vehicle.shieldSource.strength < 1)
    {
        messageClient(%player.client, 'MsgCSBFailShield', '\c2Shields are down, cannot boost.');
        return 1000;
    }

    if(%vehicle.shieldSource.strength == %vehicle.shieldSource.maxStrength)
    {
        messageClient(%player.client, 'MsgCSBFailShieldFull', '\c2Shields are full.');
        return 1000;
    }

    zapVehicle(%vehicle, FXBlueShift);
    %vehicle.useEnergy(%vehicle.getMaxEnergy() * 0.5);
    %vehicle.play3D(VShieldBoostSound);
    %vehicle.shieldSource.strength += %vehicle.shieldSource.maxStrength * 0.333;
        
    if(%vehicle.shieldSource.strength > %vehicle.shieldSource.maxStrength)
        %vehicle.shieldSource.strength = %vehicle.shieldSource.maxStrength;
}

function CShieldBooster::reArm(%this, %data, %vehicle, %player, %slot)
{
    %vehicle.moduleTimeout[%this.getName()] = 0;
}

VehiclePart.registerVehiclePart($VehiclePartType::Module, "CShieldBooster", "Shield Booster", "[Trigger] Drains 50% energy to recharge 33% shields, 20 sec cooldown", $VehicleList::HasShields, $VHardpointSize::Internal, $VHardpointType::Anything);
