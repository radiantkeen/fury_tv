datablock ShapeBaseImageData(MStingerVIP6) : EngineAPEImage
{
   mountPoint = 0;

   shapeFile = "vehicle_missile_maverick.dts";

   offset = $VehicleHardpoints[$VehicleID::Viper, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Viper, 6, "rotation"];
};

datablock ShapeBaseImageData(MStingerVIP7) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::Viper, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Viper, 7, "rotation"];
};

datablock ShapeBaseImageData(MStingerPGX4) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::PyroGX, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::PyroGX, 4, "rotation"];
};

datablock ShapeBaseImageData(MStingerPGX5) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::PyroGX, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::PyroGX, 5, "rotation"];
};

datablock ShapeBaseImageData(MStingerPGX6) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::PyroGX, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::PyroGX, 6, "rotation"];
};

datablock ShapeBaseImageData(MStingerPGX7) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::PyroGX, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::PyroGX, 7, "rotation"];
};

datablock ShapeBaseImageData(MStingerSHX6) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 6, "rotation"];
};

datablock ShapeBaseImageData(MStingerSHX7) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 7, "rotation"];
};

datablock ShapeBaseImageData(MStingerSHR6) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 6, "rotation"];
};

datablock ShapeBaseImageData(MStingerSHR7) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 7, "rotation"];
};

datablock ShapeBaseImageData(MStingerAIR7) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::Airhawk, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Airhawk, 7, "rotation"];
};

datablock ShapeBaseImageData(MStingerSSH6) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 6, "rotation"];
};

datablock ShapeBaseImageData(MStingerSSH7) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 7, "rotation"];
};

datablock ShapeBaseImageData(MStingerSTA6) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::Starhawk, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 6, "rotation"];
};

datablock ShapeBaseImageData(MStingerSTA7) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::Starhawk, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 7, "rotation"];
};

datablock ShapeBaseImageData(MStingerTOM4) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 4, "rotation"];
};

datablock ShapeBaseImageData(MStingerTOM5) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 5, "rotation"];
};

datablock ShapeBaseImageData(MStingerTOM6) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 6, "rotation"];
};

datablock ShapeBaseImageData(MStingerTOM7) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::Tomahawk, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Tomahawk, 7, "rotation"];
};

datablock ShapeBaseImageData(MStingerAMI6) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::Ameri, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Ameri, 6, "rotation"];
};

datablock ShapeBaseImageData(MStingerAMI7) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::Ameri, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Ameri, 7, "rotation"];
};

datablock ShapeBaseImageData(MStingerJEP6) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::HoverJeep, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::HoverJeep, 6, "rotation"];
};

datablock ShapeBaseImageData(MStingerJEP7) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::HoverJeep, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::HoverJeep, 7, "rotation"];
};

datablock ShapeBaseImageData(MStingerCAT4) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 4, "rotation"];
};

datablock ShapeBaseImageData(MStingerCAT5) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 5, "rotation"];
};

datablock ShapeBaseImageData(MStingerCAT6) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 6, "rotation"];
};

datablock ShapeBaseImageData(MStingerCAT7) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::Catapult, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Catapult, 7, "rotation"];
};

datablock ShapeBaseImageData(MStingerGUI6) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::Guillotine, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Guillotine, 6, "rotation"];
};

datablock ShapeBaseImageData(MStingerGUI7) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::Guillotine, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Guillotine, 7, "rotation"];
};

datablock ShapeBaseImageData(MStingerRET6) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::Retaliator, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Retaliator, 6, "rotation"];
};

datablock ShapeBaseImageData(MStingerRET7) : MStingerVIP6
{
   offset = $VehicleHardpoints[$VehicleID::Retaliator, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Retaliator, 7, "rotation"];
};

datablock BombProjectileData(StingerMissileDeploy)
{
   projectileShapeName  = "vehicle_missile_maverick.dts";
   emitterDelay         = -1;
   directDamage         = 0.0;
   hasDamageRadius      = true;
   indirectDamage       = 5.0;
   damageRadius         = 10;
   radiusDamageType     = $DamageType::Missile;
   directDamageType     = $DamageType::Missile;
   kickBackStrength     = 2250;  // z0dd - ZOD, 4/25/02. Was 2500

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Explosive;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   explosion            = "MissileExplosion";
   velInheritFactor     = 1.0;

   grenadeElasticity    = 0.25;
   grenadeFriction      = 0.4;
   armingDelayMS        = 2000;
   muzzleVelocity       = 15.0;
   drag                 = 0.3;
   gravityMod		    = 1.0;

   minRotSpeed          = "0.0 45.0 0.0";
   maxRotSpeed          = "0.0 45.0 0.0";

   scale                = "1.0 1.0 1.0";

   sound                = "";
   
   missileNameBase      = "StingerMissile";
};

datablock LinearProjectileData(StingerMissileDumbfire)
{
//   scale = "1.5 1.5 1.5";
   projectileShapeName = "vehicle_missile_maverick.dts";
   emitterDelay        = -1;
   directDamage        = 0;
   directDamageType    = $DamageType::Missile;
   hasDamageRadius     = true;
   indirectDamage      = 5.0;
   damageRadius        = 10;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 3200;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Explosive;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "MissileExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = WildcatJetEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 350;
   wetVelocity       = 200;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 5000;
   lifetimeMS        = 5000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 6000;

   sound = "LargeMissileProjectileSound";
   
   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 7.0;
   lightColor  = "0.4 0.15 0.1";
};

datablock SeekerProjectileData(StingerMissile) : ShoulderMissile
{
//   scale = "5.0 5.0 5.0";
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "vehicle_missile_maverick.dts";
   directDamage        = 0;
   directDamageType    = $DamageType::Missile;
   hasDamageRadius     = true;
   indirectDamage      = 5;
   damageRadius        = 10;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 3200;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound | $Projectile::Explosive;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "MissileExplosion";
//   underwaterExplosion = UnderwaterHandGrenadeExplosion;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = WildcatJetEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   lifetimeMS          = 5000;
   muzzleVelocity      = 350.0;
   maxVelocity         = 350.0;
   turningSpeed        = 360.0;
   acceleration        = 350.0;

   proximityRadius     = 4;

   flareDistance = 10;
   flareAngle    = 10;

//   sound = ScoutThrustSound;
   sound = "LargeMissileProjectileSound";

   hasLight    = true;
   lightRadius = 7.0;
   lightColor  = "0.4 0.15 0.1";

   useFlechette = false;
   explodeOnWaterImpact = false;

   // Used for vehicle-mounted missile system
   isVehicleMissile = true;
};

function MStinger::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "";
    %this.rearmTime = 5000;
    
    %this.missileName = "StingerMissile";
    %this.missileSize = $VHardpointSize::Small;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "MStinger", "Stinger Missile", "Highly maneuverable, small warhead - these can hit any vehicle it catches", $VehicleList::Wildcat | $VehicleList::Ameri | $VehicleList::Viper | $VehicleList::PyroGX | $VehicleList::ShrikeX | $VehicleList::Airhawk | $VehicleList::Shrike | $VehicleList::SuperShrike | $VehicleList::Starhawk | $VehicleList::Tomahawk | $VehicleList::HoverJeep | $VehicleList::Catapult | $VehicleList::Guillotine | $VehicleList::Retaliator, $VHardpointSize::Missile, $VHardpointType::Missile);
