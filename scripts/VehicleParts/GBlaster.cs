// Sound, Projectile, and Explosion blocks
datablock AudioProfile(GBlasterFireSound)
{
   filename    = "fx/weapon/beamer_bolter_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock ParticleData(GBlasterExplosionParticle1)
{
   dragCoefficient      = 0.65;
   gravityCoefficient   = 0.3;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 250;
   lifetimeVarianceMS   = 75;
   textureName          = "particleTest";
   colors[0]     = "0.56 0.76 0.06 1.0";
   colors[1]     = "0.56 0.76 0.06 0.0";
   sizes[0]      = 0.15;
   sizes[1]      = 0.25;
};

datablock ParticleEmitterData(GBlasterExplosionEmitter)
{
   ejectionPeriodMS = 12;
   periodVarianceMS = 0;
   ejectionVelocity = 2;
   velocityVariance = 0.25;
   ejectionOffset   = 0.02;
   thetaMin         = 0;
   thetaMax         = 135;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "GBlasterExplosionParticle1";
};

datablock ParticleData(GBlasterSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 350;
   lifetimeVarianceMS   = 0;
   textureName          = "special/tracer00";
   colors[0]     = "1.0 1.0 1.0 1.0";
   colors[1]     = "0.6 0.75 0.1 1.0";
   colors[2]     = "0.3 0.5 0.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.3;
   sizes[2]      = 0.1;
   times[0]      = 0.0;
   times[1]      = 0.7;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(GBlasterSparkEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 16;
   velocityVariance = 4.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 50;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 150;
   particles = "GBlasterSparks";
};

datablock ExplosionData(GBlasterExplosion)
{
   explosionShape = "energy_explosion.dts";
   soundProfile   = MBlasterExpSound;

   emitter[0] = GBlasterSparkEmitter;

   particleEmitter = GBlasterExplosionEmitter;
   particleDensity = 120;
   particleRadius = 0.3;

   faceViewer           = false;
};

datablock TracerProjectileData(VGBlasterBolt) : BlasterBolt
{
   doDynamicClientHits = true;

   projectileShapeName = "energy_bolt.dts";
   directDamage        = 0.4;
   kickBackStrength    = 0;
   directDamageType    = $DamageType::Blaster;
   explosion           = "GBlasterExplosion";
   splash              = ChaingunSplash;

   flags               = $Projectile::PlaysHitSound | $Projectile::Energy;
   ticking             = false;
   headshotMultiplier  = 1.0;

   dryVelocity       = 400.0;
   wetVelocity       = 400.5;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 2000;
   lifetimeMS        = 2000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 3000;

   tracerLength    = 22;
   tracerAlpha     = true;
   tracerMinPixels = 3;
   tracerColor     = "0.1 0.1 1.0 0.75";
	tracerTex[0]  	 = "special/tracer00"; //"special/tracer00"; //"special/blueSpark";
	tracerTex[1]  	 = "small_circle";
   tracerWidth     = 0.22;
   crossSize       = 1.2;
   crossViewAng    = 0.7;
   renderCross     = true;
   emap = true;
};

// Primary Definition
datablock ShapeBaseImageData(GBlasterVIP2)
{
   className = WeaponImage;
   shapeFile = "TR2weapon_grenade_launcher.dts";

   projectileType = TracerProjectile;
   projectile = VGBlasterBolt;
   mountPoint = 0;

   offset = $VehicleHardpoints[$VehicleID::Viper, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Viper, 2, "rotation"];

   usesEnergy = true;
   useMountEnergy = true;
   sharedResourcePool = true;

   projectileSpread = 1.0;
   minEnergy = 15;
   fireEnergy = 12;
   fireTimeout = 125;

   // State transitions
   stateName[0]                  = "Activate";
   stateTransitionOnNotLoaded[0] = "Dead";
   stateTransitionOnLoaded[0]    = "ActivateReady";

   stateName[1]                  = "ActivateReady";
   stateSequence[1]              = "Deploy";
   stateTimeoutValue[1]          = 1;
   stateTransitionOnTimeout[1]   = "Ready";
   stateTransitionOnNotLoaded[1] = "Deactivate";
   stateTransitionOnNoAmmo[1]    = "NoAmmo";

   stateName[2]                    = "Ready";
   stateTransitionOnNotLoaded[2]   = "Deactivate";
   stateTransitionOnTriggerDown[2] = "Fire";
   stateTransitionOnNoAmmo[2]      = "NoAmmo";

   stateName[3]                = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3]        = 0.2;
   stateFire[3]                = true;
   stateRecoil[3]              = LightRecoil;
   stateAllowImageChange[3]    = false;
   stateSequence[3]            = "Fire";
   stateSound[3]               = GBlasterFireSound;
   stateScript[3]              = "onFire";

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 0.05;
   stateAllowImageChange[4]      = false;
   stateTransitionOnTimeout[4]   = "Ready";
   stateTransitionOnNotLoaded[4] = "Deactivate";
   stateTransitionOnNoAmmo[4]    = "NoAmmo";

   stateName[5]                = "Deactivate";
   stateSequence[5]            = "Activate";
   stateDirection[5]           = false;
   stateTimeoutValue[5]        = 1;
   stateTransitionOnLoaded[5]  = "ActivateReady";
   stateTransitionOnTimeout[5] = "Dead";

   stateName[6]               = "Dead";
   stateTransitionOnLoaded[6] = "ActivateReady";

   stateName[7]             = "NoAmmo";
   stateTransitionOnAmmo[7] = "Reload";
   stateSequence[7]         = "NoAmmo";
};

// Inherited Defs

datablock ShapeBaseImageData(GBlasterSKY0) : GBlasterVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Skycutter, 0, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Skycutter, 0, "rotation"];
};

datablock ShapeBaseImageData(GBlasterAIR2) : GBlasterVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Airhawk, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Airhawk, 2, "rotation"];
};

datablock ShapeBaseImageData(GBlasterAIR3) : GBlasterVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Airhawk, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Airhawk, 3, "rotation"];
};

datablock ShapeBaseImageData(GBlasterTUT4) : GBlasterVIP2
{
   mountPoint = 1;
   
   offset = $VehicleHardpoints[$VehicleID::TankTurret, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 4, "rotation"];
};

datablock ShapeBaseImageData(GBlasterTUT5) : GBlasterVIP2
{
   mountPoint = 1;
   
   offset = $VehicleHardpoints[$VehicleID::TankTurret, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TankTurret, 5, "rotation"];
};

datablock ShapeBaseImageData(GBlasterTUD4) : GBlasterVIP2
{
   offset = $VehicleHardpoints[$VehicleID::TurretPole, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretPole, 4, "rotation"];
};

datablock ShapeBaseImageData(GBlasterTUD5) : GBlasterVIP2
{
   offset = $VehicleHardpoints[$VehicleID::TurretPole, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretPole, 5, "rotation"];
};

datablock ShapeBaseImageData(GBlasterPGX2) : GBlasterVIP2
{
   offset = $VehicleHardpoints[$VehicleID::PyroGX, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::PyroGX, 2, "rotation"];
};

datablock ShapeBaseImageData(GBlasterPGX3) : GBlasterVIP2
{
   offset = $VehicleHardpoints[$VehicleID::PyroGX, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::PyroGX, 3, "rotation"];
};

datablock ShapeBaseImageData(GBlasterSHR2) : GBlasterVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 2, "rotation"];
};

datablock ShapeBaseImageData(GBlasterSHR3) : GBlasterVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 3, "rotation"];
};

datablock ShapeBaseImageData(GBlasterSHR4) : GBlasterVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 4, "rotation"];
};

datablock ShapeBaseImageData(GBlasterSHR5) : GBlasterVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Shrike, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Shrike, 5, "rotation"];
};

datablock ShapeBaseImageData(GBlasterSHX2) : GBlasterVIP2
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 2, "rotation"];
};

datablock ShapeBaseImageData(GBlasterSHX3) : GBlasterVIP2
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 3, "rotation"];
   
   stateSound[4]            = "";
};

datablock ShapeBaseImageData(GBlasterSHX4) : GBlasterVIP2
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 4, "rotation"];
};

datablock ShapeBaseImageData(GBlasterSHX5) : GBlasterVIP2
{
   offset = $VehicleHardpoints[$VehicleID::ShrikeX, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::ShrikeX, 5, "rotation"];

   stateSound[4]            = "";
};

datablock ShapeBaseImageData(GBlasterSSH4) : GBlasterVIP2
{
   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 4, "rotation"];
};

datablock ShapeBaseImageData(GBlasterSSH5) : GBlasterVIP2
{
   offset = $VehicleHardpoints[$VehicleID::SuperShrike, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::SuperShrike, 5, "rotation"];
};

datablock ShapeBaseImageData(GBlasterSTA4) : GBlasterVIP2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Starhawk, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 4, "rotation"];
};

datablock ShapeBaseImageData(GBlasterSTA5) : GBlasterVIP2
{
   pilotHeadTracking = true;

   offset = $VehicleHardpoints[$VehicleID::Starhawk, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Starhawk, 5, "rotation"];
};

datablock ShapeBaseImageData(GBlasterFIB6) : GBlasterVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 6, "rotation"];
};

datablock ShapeBaseImageData(GBlasterFIB7) : GBlasterVIP2
{
   offset = $VehicleHardpoints[$VehicleID::Firebird, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::Firebird, 7, "rotation"];
};

datablock ShapeBaseImageData(GBlasterJEP2) : GBlasterVIP2
{
   offset = $VehicleHardpoints[$VehicleID::HoverJeep, 2, "position"];
   rotation = $VehicleHardpoints[$VehicleID::HoverJeep, 2, "rotation"];
};

datablock ShapeBaseImageData(GBlasterJEP3) : GBlasterVIP2
{
   offset = $VehicleHardpoints[$VehicleID::HoverJeep, 3, "position"];
   rotation = $VehicleHardpoints[$VehicleID::HoverJeep, 3, "rotation"];
};

datablock ShapeBaseImageData(GBlasterUBT2) : GBlasterVIP2
{
   shapeFile = "Rifle.dts"; // turret_chopper_barrel
   mountPoint = 2; // L

   offset = "0.0 -0.2 -0.1";
   rotation = $VehicleHardpoints[$VehicleID::Underbelly, 2, "rotation"];
};

datablock ShapeBaseImageData(GBlasterUBT3) : GBlasterVIP2
{
   shapeFile = "Rifle.dts";
   mountPoint = 3; // R

   offset = "0.0 -0.2 -0.1";
   rotation = $VehicleHardpoints[$VehicleID::Underbelly, 3, "rotation"];
};

datablock ShapeBaseImageData(GBlasterTPQ4) : GBlasterVIP2
{
   scorpionHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 4, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 4, "rotation"];
};

datablock ShapeBaseImageData(GBlasterTPQ5) : GBlasterVIP2
{
   scorpionHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 5, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 5, "rotation"];
};

datablock ShapeBaseImageData(GBlasterTPQ6) : GBlasterVIP2
{
   scorpionHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 6, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 6, "rotation"];
};

datablock ShapeBaseImageData(GBlasterTPQ7) : GBlasterVIP2
{
   scorpionHeadTracking = true;
   
   offset = $VehicleHardpoints[$VehicleID::TurretDualQuad, 7, "position"];
   rotation = $VehicleHardpoints[$VehicleID::TurretDualQuad, 7, "rotation"];
};

function GBlaster::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "";
    %this.ammoUse = 0;
    %this.fireTimeout = 125;
    
    // Reticle data
    %this.reticleTex = "gui/hud_ret_shrike";
    %this.showReticleFrame = false;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "GBlaster", "Blaster Lancer", "Common energy bolt weapon, very fast travelling", $VehicleList::Guillotine | $VehicleList::Starhawk | $VehicleList::Firebird | $VehicleList::Skycutter | $VehicleList::Airhawk | $VehicleList::Wildcat | $VehicleList::Leviathan | $VehicleList::ShrikeX | $VehicleList::PyroGX | $VehicleList::Shrike | $VehicleList::SuperShrike | $VehicleList::HoverJeep | $VehicleList::Tomahawk | $VehicleList::Retaliator | $VehicleList::Stryker | $VehicleList::Annihilator | $VehicleList::Wolfhound | $VehicleList::HoverJeep | $VehicleList::Leviathan | $VehicleList::Scorpion, $VHardpointSize::Gun, $VHardpointType::Energy);
